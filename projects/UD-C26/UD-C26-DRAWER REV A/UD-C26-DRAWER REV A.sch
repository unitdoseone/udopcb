<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.2">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="yes"/>
<layer number="100" name="Warning" color="59" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DOCFIELD" urn="urn:adsk.eagle:symbol:13864/1" library_version="1">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
<symbol name="DINA4_P" urn="urn:adsk.eagle:symbol:13866/1" library_version="1">
<frame x1="0" y1="0" x2="180.34" y2="264.16" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_P" urn="urn:adsk.eagle:component:13925/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, portrait with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_P" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="78.74" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-logo">
<packages>
<package name="UDO-LOGO-10MM" urn="urn:adsk.eagle:footprint:6649251/1">
<rectangle x1="1.397" y1="0.127" x2="1.82118125" y2="0.21158125" layer="21"/>
<rectangle x1="3.85318125" y1="0.127" x2="4.191" y2="0.21158125" layer="21"/>
<rectangle x1="4.953" y1="0.127" x2="5.03681875" y2="0.21158125" layer="21"/>
<rectangle x1="5.63118125" y1="0.127" x2="5.715" y2="0.21158125" layer="21"/>
<rectangle x1="6.223" y1="0.127" x2="6.30681875" y2="0.21158125" layer="21"/>
<rectangle x1="7.06881875" y1="0.127" x2="7.40918125" y2="0.21158125" layer="21"/>
<rectangle x1="8.84681875" y1="0.127" x2="8.93318125" y2="0.21158125" layer="21"/>
<rectangle x1="1.22681875" y1="0.21158125" x2="1.905" y2="0.29641875" layer="21"/>
<rectangle x1="2.159" y1="0.21158125" x2="2.32918125" y2="0.29641875" layer="21"/>
<rectangle x1="2.75081875" y1="0.21158125" x2="2.83718125" y2="0.29641875" layer="21"/>
<rectangle x1="2.921" y1="0.21158125" x2="3.09118125" y2="0.29641875" layer="21"/>
<rectangle x1="3.34518125" y1="0.21158125" x2="3.59918125" y2="0.29641875" layer="21"/>
<rectangle x1="3.683" y1="0.21158125" x2="4.36118125" y2="0.29641875" layer="21"/>
<rectangle x1="4.78281875" y1="0.21158125" x2="5.207" y2="0.29641875" layer="21"/>
<rectangle x1="5.461" y1="0.21158125" x2="5.79881875" y2="0.29641875" layer="21"/>
<rectangle x1="6.05281875" y1="0.21158125" x2="6.477" y2="0.29641875" layer="21"/>
<rectangle x1="6.90118125" y1="0.21158125" x2="7.57681875" y2="0.29641875" layer="21"/>
<rectangle x1="7.747" y1="0.21158125" x2="7.91718125" y2="0.29641875" layer="21"/>
<rectangle x1="8.33881875" y1="0.21158125" x2="8.509" y2="0.29641875" layer="21"/>
<rectangle x1="8.67918125" y1="0.21158125" x2="9.10081875" y2="0.29641875" layer="21"/>
<rectangle x1="1.22681875" y1="0.29641875" x2="1.397" y2="0.381" layer="21"/>
<rectangle x1="1.82118125" y1="0.29641875" x2="1.98881875" y2="0.381" layer="21"/>
<rectangle x1="2.159" y1="0.29641875" x2="2.32918125" y2="0.381" layer="21"/>
<rectangle x1="2.75081875" y1="0.29641875" x2="2.83718125" y2="0.381" layer="21"/>
<rectangle x1="2.921" y1="0.29641875" x2="3.09118125" y2="0.381" layer="21"/>
<rectangle x1="3.25881875" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="3.683" y1="0.29641875" x2="3.85318125" y2="0.381" layer="21"/>
<rectangle x1="4.27481875" y1="0.29641875" x2="4.445" y2="0.381" layer="21"/>
<rectangle x1="4.699" y1="0.29641875" x2="4.86918125" y2="0.381" layer="21"/>
<rectangle x1="5.12318125" y1="0.29641875" x2="5.29081875" y2="0.381" layer="21"/>
<rectangle x1="5.37718125" y1="0.29641875" x2="5.54481875" y2="0.381" layer="21"/>
<rectangle x1="5.715" y1="0.29641875" x2="5.88518125" y2="0.381" layer="21"/>
<rectangle x1="5.969" y1="0.29641875" x2="6.13918125" y2="0.381" layer="21"/>
<rectangle x1="6.39318125" y1="0.29641875" x2="6.56081875" y2="0.381" layer="21"/>
<rectangle x1="6.81481875" y1="0.29641875" x2="6.985" y2="0.381" layer="21"/>
<rectangle x1="7.493" y1="0.29641875" x2="7.66318125" y2="0.381" layer="21"/>
<rectangle x1="7.747" y1="0.29641875" x2="7.91718125" y2="0.381" layer="21"/>
<rectangle x1="8.33881875" y1="0.29641875" x2="8.509" y2="0.381" layer="21"/>
<rectangle x1="8.59281875" y1="0.29641875" x2="8.763" y2="0.381" layer="21"/>
<rectangle x1="8.93318125" y1="0.29641875" x2="9.18718125" y2="0.381" layer="21"/>
<rectangle x1="1.143" y1="0.381" x2="1.31318125" y2="0.46558125" layer="21"/>
<rectangle x1="1.905" y1="0.381" x2="2.07518125" y2="0.46558125" layer="21"/>
<rectangle x1="2.159" y1="0.381" x2="2.32918125" y2="0.46558125" layer="21"/>
<rectangle x1="2.75081875" y1="0.381" x2="2.83718125" y2="0.46558125" layer="21"/>
<rectangle x1="2.921" y1="0.381" x2="3.09118125" y2="0.46558125" layer="21"/>
<rectangle x1="3.175" y1="0.381" x2="3.34518125" y2="0.46558125" layer="21"/>
<rectangle x1="3.683" y1="0.381" x2="3.85318125" y2="0.46558125" layer="21"/>
<rectangle x1="4.36118125" y1="0.381" x2="4.52881875" y2="0.46558125" layer="21"/>
<rectangle x1="4.699" y1="0.381" x2="4.78281875" y2="0.46558125" layer="21"/>
<rectangle x1="5.207" y1="0.381" x2="5.37718125" y2="0.46558125" layer="21"/>
<rectangle x1="5.715" y1="0.381" x2="5.88518125" y2="0.46558125" layer="21"/>
<rectangle x1="5.969" y1="0.381" x2="6.05281875" y2="0.46558125" layer="21"/>
<rectangle x1="6.477" y1="0.381" x2="6.56081875" y2="0.46558125" layer="21"/>
<rectangle x1="6.81481875" y1="0.381" x2="6.90118125" y2="0.46558125" layer="21"/>
<rectangle x1="7.57681875" y1="0.381" x2="7.66318125" y2="0.46558125" layer="21"/>
<rectangle x1="7.747" y1="0.381" x2="7.91718125" y2="0.46558125" layer="21"/>
<rectangle x1="8.33881875" y1="0.381" x2="8.67918125" y2="0.46558125" layer="21"/>
<rectangle x1="9.017" y1="0.381" x2="9.18718125" y2="0.46558125" layer="21"/>
<rectangle x1="1.143" y1="0.46558125" x2="1.22681875" y2="0.55041875" layer="21"/>
<rectangle x1="1.905" y1="0.46558125" x2="2.07518125" y2="0.55041875" layer="21"/>
<rectangle x1="2.159" y1="0.46558125" x2="2.32918125" y2="0.55041875" layer="21"/>
<rectangle x1="2.75081875" y1="0.46558125" x2="2.83718125" y2="0.55041875" layer="21"/>
<rectangle x1="2.921" y1="0.46558125" x2="3.09118125" y2="0.55041875" layer="21"/>
<rectangle x1="3.175" y1="0.46558125" x2="3.34518125" y2="0.55041875" layer="21"/>
<rectangle x1="3.683" y1="0.46558125" x2="3.85318125" y2="0.55041875" layer="21"/>
<rectangle x1="4.445" y1="0.46558125" x2="4.78281875" y2="0.55041875" layer="21"/>
<rectangle x1="5.207" y1="0.46558125" x2="5.37718125" y2="0.55041875" layer="21"/>
<rectangle x1="5.54481875" y1="0.46558125" x2="5.79881875" y2="0.55041875" layer="21"/>
<rectangle x1="5.88518125" y1="0.46558125" x2="6.64718125" y2="0.55041875" layer="21"/>
<rectangle x1="6.731" y1="0.46558125" x2="6.90118125" y2="0.55041875" layer="21"/>
<rectangle x1="7.57681875" y1="0.46558125" x2="7.91718125" y2="0.55041875" layer="21"/>
<rectangle x1="8.33881875" y1="0.46558125" x2="9.18718125" y2="0.55041875" layer="21"/>
<rectangle x1="1.05918125" y1="0.55041875" x2="1.22681875" y2="0.635" layer="21"/>
<rectangle x1="1.98881875" y1="0.55041875" x2="2.07518125" y2="0.635" layer="21"/>
<rectangle x1="2.159" y1="0.55041875" x2="2.32918125" y2="0.635" layer="21"/>
<rectangle x1="2.75081875" y1="0.55041875" x2="2.83718125" y2="0.635" layer="21"/>
<rectangle x1="2.921" y1="0.55041875" x2="3.09118125" y2="0.635" layer="21"/>
<rectangle x1="3.175" y1="0.55041875" x2="3.34518125" y2="0.635" layer="21"/>
<rectangle x1="3.683" y1="0.55041875" x2="3.85318125" y2="0.635" layer="21"/>
<rectangle x1="4.445" y1="0.55041875" x2="4.78281875" y2="0.635" layer="21"/>
<rectangle x1="5.207" y1="0.55041875" x2="5.715" y2="0.635" layer="21"/>
<rectangle x1="5.88518125" y1="0.55041875" x2="6.05281875" y2="0.635" layer="21"/>
<rectangle x1="6.13918125" y1="0.55041875" x2="6.39318125" y2="0.635" layer="21"/>
<rectangle x1="6.477" y1="0.55041875" x2="6.64718125" y2="0.635" layer="21"/>
<rectangle x1="6.731" y1="0.55041875" x2="6.90118125" y2="0.635" layer="21"/>
<rectangle x1="7.57681875" y1="0.55041875" x2="7.91718125" y2="0.635" layer="21"/>
<rectangle x1="8.33881875" y1="0.55041875" x2="8.67918125" y2="0.635" layer="21"/>
<rectangle x1="8.763" y1="0.55041875" x2="9.18718125" y2="0.635" layer="21"/>
<rectangle x1="1.05918125" y1="0.635" x2="1.22681875" y2="0.71958125" layer="21"/>
<rectangle x1="1.98881875" y1="0.635" x2="2.07518125" y2="0.71958125" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.32918125" y2="0.71958125" layer="21"/>
<rectangle x1="2.667" y1="0.635" x2="2.83718125" y2="0.71958125" layer="21"/>
<rectangle x1="2.921" y1="0.635" x2="3.09118125" y2="0.71958125" layer="21"/>
<rectangle x1="3.175" y1="0.635" x2="3.34518125" y2="0.71958125" layer="21"/>
<rectangle x1="3.683" y1="0.635" x2="3.85318125" y2="0.71958125" layer="21"/>
<rectangle x1="4.445" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="4.699" y1="0.635" x2="4.86918125" y2="0.71958125" layer="21"/>
<rectangle x1="5.12318125" y1="0.635" x2="5.29081875" y2="0.71958125" layer="21"/>
<rectangle x1="5.37718125" y1="0.635" x2="5.54481875" y2="0.71958125" layer="21"/>
<rectangle x1="5.715" y1="0.635" x2="5.88518125" y2="0.71958125" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.13918125" y2="0.71958125" layer="21"/>
<rectangle x1="6.39318125" y1="0.635" x2="6.56081875" y2="0.71958125" layer="21"/>
<rectangle x1="6.731" y1="0.635" x2="6.90118125" y2="0.71958125" layer="21"/>
<rectangle x1="7.57681875" y1="0.635" x2="7.747" y2="0.71958125" layer="21"/>
<rectangle x1="7.83081875" y1="0.635" x2="8.001" y2="0.71958125" layer="21"/>
<rectangle x1="8.255" y1="0.635" x2="8.42518125" y2="0.71958125" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="8.67918125" y2="0.71958125" layer="21"/>
<rectangle x1="9.017" y1="0.635" x2="9.18718125" y2="0.71958125" layer="21"/>
<rectangle x1="1.05918125" y1="0.71958125" x2="1.22681875" y2="0.80441875" layer="21"/>
<rectangle x1="1.98881875" y1="0.71958125" x2="2.07518125" y2="0.80441875" layer="21"/>
<rectangle x1="2.159" y1="0.71958125" x2="2.75081875" y2="0.80441875" layer="21"/>
<rectangle x1="2.921" y1="0.71958125" x2="3.59918125" y2="0.80441875" layer="21"/>
<rectangle x1="3.683" y1="0.71958125" x2="3.85318125" y2="0.80441875" layer="21"/>
<rectangle x1="4.445" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="4.78281875" y1="0.71958125" x2="5.29081875" y2="0.80441875" layer="21"/>
<rectangle x1="5.37718125" y1="0.71958125" x2="5.88518125" y2="0.80441875" layer="21"/>
<rectangle x1="6.05281875" y1="0.71958125" x2="6.56081875" y2="0.80441875" layer="21"/>
<rectangle x1="6.731" y1="0.71958125" x2="6.90118125" y2="0.80441875" layer="21"/>
<rectangle x1="7.57681875" y1="0.71958125" x2="8.33881875" y2="0.80441875" layer="21"/>
<rectangle x1="8.59281875" y1="0.71958125" x2="9.10081875" y2="0.80441875" layer="21"/>
<rectangle x1="1.05918125" y1="0.80441875" x2="1.22681875" y2="0.889" layer="21"/>
<rectangle x1="1.98881875" y1="0.80441875" x2="2.07518125" y2="0.889" layer="21"/>
<rectangle x1="2.159" y1="0.80441875" x2="2.24281875" y2="0.889" layer="21"/>
<rectangle x1="2.413" y1="0.80441875" x2="2.667" y2="0.889" layer="21"/>
<rectangle x1="2.921" y1="0.80441875" x2="3.00481875" y2="0.889" layer="21"/>
<rectangle x1="3.09118125" y1="0.80441875" x2="3.59918125" y2="0.889" layer="21"/>
<rectangle x1="3.683" y1="0.80441875" x2="3.85318125" y2="0.889" layer="21"/>
<rectangle x1="4.445" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="4.86918125" y1="0.80441875" x2="5.12318125" y2="0.889" layer="21"/>
<rectangle x1="5.54481875" y1="0.80441875" x2="5.715" y2="0.889" layer="21"/>
<rectangle x1="6.13918125" y1="0.80441875" x2="6.39318125" y2="0.889" layer="21"/>
<rectangle x1="6.81481875" y1="0.80441875" x2="6.90118125" y2="0.889" layer="21"/>
<rectangle x1="7.57681875" y1="0.80441875" x2="7.66318125" y2="0.889" layer="21"/>
<rectangle x1="8.001" y1="0.80441875" x2="8.255" y2="0.889" layer="21"/>
<rectangle x1="8.67918125" y1="0.80441875" x2="9.017" y2="0.889" layer="21"/>
<rectangle x1="1.05918125" y1="0.889" x2="1.22681875" y2="0.97358125" layer="21"/>
<rectangle x1="1.98881875" y1="0.889" x2="2.07518125" y2="0.97358125" layer="21"/>
<rectangle x1="2.921" y1="0.889" x2="3.09118125" y2="0.97358125" layer="21"/>
<rectangle x1="3.175" y1="0.889" x2="3.34518125" y2="0.97358125" layer="21"/>
<rectangle x1="3.683" y1="0.889" x2="3.85318125" y2="0.97358125" layer="21"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.52881875" y2="0.97358125" layer="21"/>
<rectangle x1="6.81481875" y1="0.889" x2="6.985" y2="0.97358125" layer="21"/>
<rectangle x1="7.493" y1="0.889" x2="7.66318125" y2="0.97358125" layer="21"/>
<rectangle x1="1.05918125" y1="0.97358125" x2="1.22681875" y2="1.05841875" layer="21"/>
<rectangle x1="1.98881875" y1="0.97358125" x2="2.07518125" y2="1.05841875" layer="21"/>
<rectangle x1="2.921" y1="0.97358125" x2="3.00481875" y2="1.05841875" layer="21"/>
<rectangle x1="3.175" y1="0.97358125" x2="3.34518125" y2="1.05841875" layer="21"/>
<rectangle x1="3.683" y1="0.97358125" x2="3.85318125" y2="1.05841875" layer="21"/>
<rectangle x1="4.27481875" y1="0.97358125" x2="4.445" y2="1.05841875" layer="21"/>
<rectangle x1="6.90118125" y1="0.97358125" x2="7.57681875" y2="1.05841875" layer="21"/>
<rectangle x1="1.05918125" y1="1.05841875" x2="1.22681875" y2="1.143" layer="21"/>
<rectangle x1="1.98881875" y1="1.05841875" x2="2.07518125" y2="1.143" layer="21"/>
<rectangle x1="3.175" y1="1.05841875" x2="3.34518125" y2="1.143" layer="21"/>
<rectangle x1="3.683" y1="1.05841875" x2="4.36118125" y2="1.143" layer="21"/>
<rectangle x1="7.06881875" y1="1.05841875" x2="7.40918125" y2="1.143" layer="21"/>
<rectangle x1="3.76681875" y1="1.143" x2="4.191" y2="1.22758125" layer="21"/>
<rectangle x1="1.82118125" y1="1.56641875" x2="8.255" y2="1.651" layer="21"/>
<rectangle x1="1.397" y1="1.651" x2="8.59281875" y2="1.73558125" layer="21"/>
<rectangle x1="1.143" y1="1.73558125" x2="8.84681875" y2="1.82041875" layer="21"/>
<rectangle x1="1.05918125" y1="1.82041875" x2="9.017" y2="1.905" layer="21"/>
<rectangle x1="0.889" y1="1.905" x2="1.651" y2="1.98958125" layer="21"/>
<rectangle x1="4.86918125" y1="1.905" x2="9.10081875" y2="1.98958125" layer="21"/>
<rectangle x1="0.80518125" y1="1.98958125" x2="1.397" y2="2.07441875" layer="21"/>
<rectangle x1="4.78281875" y1="1.98958125" x2="9.271" y2="2.07441875" layer="21"/>
<rectangle x1="0.635" y1="2.07441875" x2="1.22681875" y2="2.159" layer="21"/>
<rectangle x1="4.78281875" y1="2.07441875" x2="9.35481875" y2="2.159" layer="21"/>
<rectangle x1="0.55118125" y1="2.159" x2="1.05918125" y2="2.24358125" layer="21"/>
<rectangle x1="4.699" y1="2.159" x2="9.44118125" y2="2.24358125" layer="21"/>
<rectangle x1="0.46481875" y1="2.24358125" x2="0.97281875" y2="2.32841875" layer="21"/>
<rectangle x1="4.699" y1="2.24358125" x2="9.525" y2="2.32841875" layer="21"/>
<rectangle x1="0.46481875" y1="2.32841875" x2="0.80518125" y2="2.413" layer="21"/>
<rectangle x1="4.699" y1="2.32841875" x2="9.60881875" y2="2.413" layer="21"/>
<rectangle x1="0.381" y1="2.413" x2="0.71881875" y2="2.49758125" layer="21"/>
<rectangle x1="4.61518125" y1="2.413" x2="9.60881875" y2="2.49758125" layer="21"/>
<rectangle x1="0.29718125" y1="2.49758125" x2="0.71881875" y2="2.58241875" layer="21"/>
<rectangle x1="4.61518125" y1="2.49758125" x2="9.69518125" y2="2.58241875" layer="21"/>
<rectangle x1="0.29718125" y1="2.58241875" x2="0.635" y2="2.667" layer="21"/>
<rectangle x1="4.61518125" y1="2.58241875" x2="9.779" y2="2.667" layer="21"/>
<rectangle x1="0.21081875" y1="2.667" x2="0.55118125" y2="2.75158125" layer="21"/>
<rectangle x1="4.52881875" y1="2.667" x2="9.779" y2="2.75158125" layer="21"/>
<rectangle x1="0.21081875" y1="2.75158125" x2="0.55118125" y2="2.83641875" layer="21"/>
<rectangle x1="1.82118125" y1="2.75158125" x2="2.24281875" y2="2.83641875" layer="21"/>
<rectangle x1="2.921" y1="2.75158125" x2="3.51281875" y2="2.83641875" layer="21"/>
<rectangle x1="4.52881875" y1="2.75158125" x2="9.86281875" y2="2.83641875" layer="21"/>
<rectangle x1="0.127" y1="2.83641875" x2="0.46481875" y2="2.921" layer="21"/>
<rectangle x1="1.56718125" y1="2.83641875" x2="2.413" y2="2.921" layer="21"/>
<rectangle x1="2.83718125" y1="2.83641875" x2="3.683" y2="2.921" layer="21"/>
<rectangle x1="4.52881875" y1="2.83641875" x2="5.29081875" y2="2.921" layer="21"/>
<rectangle x1="5.969" y1="2.83641875" x2="6.64718125" y2="2.921" layer="21"/>
<rectangle x1="6.81481875" y1="2.83641875" x2="7.493" y2="2.921" layer="21"/>
<rectangle x1="7.66318125" y1="2.83641875" x2="8.17118125" y2="2.921" layer="21"/>
<rectangle x1="8.67918125" y1="2.83641875" x2="9.86281875" y2="2.921" layer="21"/>
<rectangle x1="0.127" y1="2.921" x2="0.46481875" y2="3.00558125" layer="21"/>
<rectangle x1="1.48081875" y1="2.921" x2="2.58318125" y2="3.00558125" layer="21"/>
<rectangle x1="2.83718125" y1="2.921" x2="3.85318125" y2="3.00558125" layer="21"/>
<rectangle x1="4.52881875" y1="2.921" x2="5.12318125" y2="3.00558125" layer="21"/>
<rectangle x1="6.13918125" y1="2.921" x2="6.64718125" y2="3.00558125" layer="21"/>
<rectangle x1="6.81481875" y1="2.921" x2="7.493" y2="3.00558125" layer="21"/>
<rectangle x1="7.747" y1="2.921" x2="8.001" y2="3.00558125" layer="21"/>
<rectangle x1="8.763" y1="2.921" x2="9.86281875" y2="3.00558125" layer="21"/>
<rectangle x1="0.127" y1="3.00558125" x2="0.381" y2="3.09041875" layer="21"/>
<rectangle x1="1.397" y1="3.00558125" x2="2.667" y2="3.09041875" layer="21"/>
<rectangle x1="2.921" y1="3.00558125" x2="3.937" y2="3.09041875" layer="21"/>
<rectangle x1="4.52881875" y1="3.00558125" x2="5.03681875" y2="3.09041875" layer="21"/>
<rectangle x1="5.54481875" y1="3.00558125" x2="5.715" y2="3.09041875" layer="21"/>
<rectangle x1="6.223" y1="3.00558125" x2="6.64718125" y2="3.09041875" layer="21"/>
<rectangle x1="6.81481875" y1="3.00558125" x2="7.493" y2="3.09041875" layer="21"/>
<rectangle x1="7.747" y1="3.00558125" x2="7.91718125" y2="3.09041875" layer="21"/>
<rectangle x1="8.255" y1="3.00558125" x2="8.59281875" y2="3.09041875" layer="21"/>
<rectangle x1="8.84681875" y1="3.00558125" x2="9.94918125" y2="3.09041875" layer="21"/>
<rectangle x1="0.04318125" y1="3.09041875" x2="0.381" y2="3.175" layer="21"/>
<rectangle x1="1.31318125" y1="3.09041875" x2="1.73481875" y2="3.175" layer="21"/>
<rectangle x1="2.24281875" y1="3.09041875" x2="2.667" y2="3.175" layer="21"/>
<rectangle x1="3.59918125" y1="3.09041875" x2="4.02081875" y2="3.175" layer="21"/>
<rectangle x1="4.52881875" y1="3.09041875" x2="4.953" y2="3.175" layer="21"/>
<rectangle x1="5.29081875" y1="3.09041875" x2="5.969" y2="3.175" layer="21"/>
<rectangle x1="6.30681875" y1="3.09041875" x2="6.64718125" y2="3.175" layer="21"/>
<rectangle x1="6.81481875" y1="3.09041875" x2="7.493" y2="3.175" layer="21"/>
<rectangle x1="7.747" y1="3.09041875" x2="7.91718125" y2="3.175" layer="21"/>
<rectangle x1="8.08481875" y1="3.09041875" x2="8.67918125" y2="3.175" layer="21"/>
<rectangle x1="8.84681875" y1="3.09041875" x2="9.94918125" y2="3.175" layer="21"/>
<rectangle x1="0.04318125" y1="3.175" x2="0.381" y2="3.25958125" layer="21"/>
<rectangle x1="1.31318125" y1="3.175" x2="1.651" y2="3.25958125" layer="21"/>
<rectangle x1="2.413" y1="3.175" x2="2.75081875" y2="3.25958125" layer="21"/>
<rectangle x1="3.683" y1="3.175" x2="4.02081875" y2="3.25958125" layer="21"/>
<rectangle x1="4.52881875" y1="3.175" x2="4.953" y2="3.25958125" layer="21"/>
<rectangle x1="5.207" y1="3.175" x2="6.05281875" y2="3.25958125" layer="21"/>
<rectangle x1="6.30681875" y1="3.175" x2="6.64718125" y2="3.25958125" layer="21"/>
<rectangle x1="6.81481875" y1="3.175" x2="7.493" y2="3.25958125" layer="21"/>
<rectangle x1="7.66318125" y1="3.175" x2="7.83081875" y2="3.25958125" layer="21"/>
<rectangle x1="8.08481875" y1="3.175" x2="9.94918125" y2="3.25958125" layer="21"/>
<rectangle x1="0.04318125" y1="3.25958125" x2="0.29718125" y2="3.34441875" layer="21"/>
<rectangle x1="1.22681875" y1="3.25958125" x2="1.56718125" y2="3.34441875" layer="21"/>
<rectangle x1="2.49681875" y1="3.25958125" x2="2.75081875" y2="3.34441875" layer="21"/>
<rectangle x1="3.76681875" y1="3.25958125" x2="4.10718125" y2="3.34441875" layer="21"/>
<rectangle x1="4.445" y1="3.25958125" x2="4.86918125" y2="3.34441875" layer="21"/>
<rectangle x1="5.12318125" y1="3.25958125" x2="6.13918125" y2="3.34441875" layer="21"/>
<rectangle x1="6.39318125" y1="3.25958125" x2="6.64718125" y2="3.34441875" layer="21"/>
<rectangle x1="6.81481875" y1="3.25958125" x2="7.493" y2="3.34441875" layer="21"/>
<rectangle x1="7.66318125" y1="3.25958125" x2="7.83081875" y2="3.34441875" layer="21"/>
<rectangle x1="8.93318125" y1="3.25958125" x2="9.94918125" y2="3.34441875" layer="21"/>
<rectangle x1="0.04318125" y1="3.34441875" x2="0.29718125" y2="3.429" layer="21"/>
<rectangle x1="1.22681875" y1="3.34441875" x2="1.48081875" y2="3.429" layer="21"/>
<rectangle x1="2.49681875" y1="3.34441875" x2="2.83718125" y2="3.429" layer="21"/>
<rectangle x1="3.76681875" y1="3.34441875" x2="4.10718125" y2="3.429" layer="21"/>
<rectangle x1="4.445" y1="3.34441875" x2="4.86918125" y2="3.429" layer="21"/>
<rectangle x1="5.12318125" y1="3.34441875" x2="6.13918125" y2="3.429" layer="21"/>
<rectangle x1="6.39318125" y1="3.34441875" x2="6.64718125" y2="3.429" layer="21"/>
<rectangle x1="6.81481875" y1="3.34441875" x2="7.493" y2="3.429" layer="21"/>
<rectangle x1="7.66318125" y1="3.34441875" x2="7.83081875" y2="3.429" layer="21"/>
<rectangle x1="8.93318125" y1="3.34441875" x2="9.94918125" y2="3.429" layer="21"/>
<rectangle x1="0.04318125" y1="3.429" x2="0.29718125" y2="3.51358125" layer="21"/>
<rectangle x1="1.143" y1="3.429" x2="1.48081875" y2="3.51358125" layer="21"/>
<rectangle x1="2.49681875" y1="3.429" x2="2.83718125" y2="3.51358125" layer="21"/>
<rectangle x1="3.85318125" y1="3.429" x2="4.10718125" y2="3.51358125" layer="21"/>
<rectangle x1="4.445" y1="3.429" x2="4.78281875" y2="3.51358125" layer="21"/>
<rectangle x1="5.03681875" y1="3.429" x2="6.13918125" y2="3.51358125" layer="21"/>
<rectangle x1="6.39318125" y1="3.429" x2="6.64718125" y2="3.51358125" layer="21"/>
<rectangle x1="6.81481875" y1="3.429" x2="7.493" y2="3.51358125" layer="21"/>
<rectangle x1="7.66318125" y1="3.429" x2="7.83081875" y2="3.51358125" layer="21"/>
<rectangle x1="8.08481875" y1="3.429" x2="8.67918125" y2="3.51358125" layer="21"/>
<rectangle x1="8.93318125" y1="3.429" x2="9.94918125" y2="3.51358125" layer="21"/>
<rectangle x1="0.04318125" y1="3.51358125" x2="0.29718125" y2="3.59841875" layer="21"/>
<rectangle x1="1.143" y1="3.51358125" x2="1.48081875" y2="3.59841875" layer="21"/>
<rectangle x1="2.58318125" y1="3.51358125" x2="2.83718125" y2="3.59841875" layer="21"/>
<rectangle x1="3.85318125" y1="3.51358125" x2="4.191" y2="3.59841875" layer="21"/>
<rectangle x1="4.445" y1="3.51358125" x2="4.78281875" y2="3.59841875" layer="21"/>
<rectangle x1="5.03681875" y1="3.51358125" x2="6.223" y2="3.59841875" layer="21"/>
<rectangle x1="6.477" y1="3.51358125" x2="6.64718125" y2="3.59841875" layer="21"/>
<rectangle x1="6.90118125" y1="3.51358125" x2="7.40918125" y2="3.59841875" layer="21"/>
<rectangle x1="7.66318125" y1="3.51358125" x2="7.91718125" y2="3.59841875" layer="21"/>
<rectangle x1="8.08481875" y1="3.51358125" x2="8.67918125" y2="3.59841875" layer="21"/>
<rectangle x1="8.84681875" y1="3.51358125" x2="9.94918125" y2="3.59841875" layer="21"/>
<rectangle x1="0.04318125" y1="3.59841875" x2="0.29718125" y2="3.683" layer="21"/>
<rectangle x1="1.143" y1="3.59841875" x2="1.48081875" y2="3.683" layer="21"/>
<rectangle x1="2.58318125" y1="3.59841875" x2="2.83718125" y2="3.683" layer="21"/>
<rectangle x1="3.85318125" y1="3.59841875" x2="4.191" y2="3.683" layer="21"/>
<rectangle x1="4.445" y1="3.59841875" x2="4.78281875" y2="3.683" layer="21"/>
<rectangle x1="5.03681875" y1="3.59841875" x2="6.223" y2="3.683" layer="21"/>
<rectangle x1="6.477" y1="3.59841875" x2="6.64718125" y2="3.683" layer="21"/>
<rectangle x1="6.985" y1="3.59841875" x2="7.32281875" y2="3.683" layer="21"/>
<rectangle x1="7.66318125" y1="3.59841875" x2="7.91718125" y2="3.683" layer="21"/>
<rectangle x1="8.17118125" y1="3.59841875" x2="8.59281875" y2="3.683" layer="21"/>
<rectangle x1="8.84681875" y1="3.59841875" x2="9.94918125" y2="3.683" layer="21"/>
<rectangle x1="0.04318125" y1="3.683" x2="0.29718125" y2="3.76758125" layer="21"/>
<rectangle x1="1.143" y1="3.683" x2="1.48081875" y2="3.76758125" layer="21"/>
<rectangle x1="2.58318125" y1="3.683" x2="2.83718125" y2="3.76758125" layer="21"/>
<rectangle x1="3.85318125" y1="3.683" x2="4.10718125" y2="3.76758125" layer="21"/>
<rectangle x1="4.445" y1="3.683" x2="4.78281875" y2="3.76758125" layer="21"/>
<rectangle x1="5.03681875" y1="3.683" x2="6.13918125" y2="3.76758125" layer="21"/>
<rectangle x1="6.39318125" y1="3.683" x2="6.64718125" y2="3.76758125" layer="21"/>
<rectangle x1="7.57681875" y1="3.683" x2="8.001" y2="3.76758125" layer="21"/>
<rectangle x1="8.763" y1="3.683" x2="9.94918125" y2="3.76758125" layer="21"/>
<rectangle x1="0.04318125" y1="3.76758125" x2="0.29718125" y2="3.85241875" layer="21"/>
<rectangle x1="1.143" y1="3.76758125" x2="1.48081875" y2="3.85241875" layer="21"/>
<rectangle x1="2.58318125" y1="3.76758125" x2="2.83718125" y2="3.85241875" layer="21"/>
<rectangle x1="3.85318125" y1="3.76758125" x2="4.10718125" y2="3.85241875" layer="21"/>
<rectangle x1="4.445" y1="3.76758125" x2="4.86918125" y2="3.85241875" layer="21"/>
<rectangle x1="5.12318125" y1="3.76758125" x2="6.13918125" y2="3.85241875" layer="21"/>
<rectangle x1="6.39318125" y1="3.76758125" x2="6.64718125" y2="3.85241875" layer="21"/>
<rectangle x1="6.81481875" y1="3.76758125" x2="6.90118125" y2="3.85241875" layer="21"/>
<rectangle x1="7.40918125" y1="3.76758125" x2="8.08481875" y2="3.85241875" layer="21"/>
<rectangle x1="8.67918125" y1="3.76758125" x2="9.94918125" y2="3.85241875" layer="21"/>
<rectangle x1="0.04318125" y1="3.85241875" x2="0.29718125" y2="3.937" layer="21"/>
<rectangle x1="1.143" y1="3.85241875" x2="1.48081875" y2="3.937" layer="21"/>
<rectangle x1="2.58318125" y1="3.85241875" x2="2.83718125" y2="3.937" layer="21"/>
<rectangle x1="3.76681875" y1="3.85241875" x2="4.10718125" y2="3.937" layer="21"/>
<rectangle x1="4.445" y1="3.85241875" x2="4.86918125" y2="3.937" layer="21"/>
<rectangle x1="5.12318125" y1="3.85241875" x2="6.13918125" y2="3.937" layer="21"/>
<rectangle x1="6.39318125" y1="3.85241875" x2="7.06881875" y2="3.937" layer="21"/>
<rectangle x1="7.239" y1="3.85241875" x2="8.33881875" y2="3.937" layer="21"/>
<rectangle x1="8.42518125" y1="3.85241875" x2="9.94918125" y2="3.937" layer="21"/>
<rectangle x1="0.04318125" y1="3.937" x2="0.381" y2="4.02158125" layer="21"/>
<rectangle x1="1.143" y1="3.937" x2="1.48081875" y2="4.02158125" layer="21"/>
<rectangle x1="2.58318125" y1="3.937" x2="2.83718125" y2="4.02158125" layer="21"/>
<rectangle x1="3.683" y1="3.937" x2="4.02081875" y2="4.02158125" layer="21"/>
<rectangle x1="4.52881875" y1="3.937" x2="4.86918125" y2="4.02158125" layer="21"/>
<rectangle x1="5.207" y1="3.937" x2="6.05281875" y2="4.02158125" layer="21"/>
<rectangle x1="6.30681875" y1="3.937" x2="9.94918125" y2="4.02158125" layer="21"/>
<rectangle x1="0.04318125" y1="4.02158125" x2="0.381" y2="4.10641875" layer="21"/>
<rectangle x1="1.143" y1="4.02158125" x2="1.48081875" y2="4.10641875" layer="21"/>
<rectangle x1="2.58318125" y1="4.02158125" x2="2.83718125" y2="4.10641875" layer="21"/>
<rectangle x1="3.59918125" y1="4.02158125" x2="4.02081875" y2="4.10641875" layer="21"/>
<rectangle x1="4.52881875" y1="4.02158125" x2="4.953" y2="4.10641875" layer="21"/>
<rectangle x1="5.29081875" y1="4.02158125" x2="5.969" y2="4.10641875" layer="21"/>
<rectangle x1="6.30681875" y1="4.02158125" x2="9.94918125" y2="4.10641875" layer="21"/>
<rectangle x1="0.04318125" y1="4.10641875" x2="0.381" y2="4.191" layer="21"/>
<rectangle x1="1.143" y1="4.10641875" x2="1.48081875" y2="4.191" layer="21"/>
<rectangle x1="2.58318125" y1="4.10641875" x2="3.937" y2="4.191" layer="21"/>
<rectangle x1="4.52881875" y1="4.10641875" x2="5.03681875" y2="4.191" layer="21"/>
<rectangle x1="5.461" y1="4.10641875" x2="5.79881875" y2="4.191" layer="21"/>
<rectangle x1="6.223" y1="4.10641875" x2="9.94918125" y2="4.191" layer="21"/>
<rectangle x1="0.127" y1="4.191" x2="0.46481875" y2="4.27558125" layer="21"/>
<rectangle x1="1.143" y1="4.191" x2="1.48081875" y2="4.27558125" layer="21"/>
<rectangle x1="2.58318125" y1="4.191" x2="3.85318125" y2="4.27558125" layer="21"/>
<rectangle x1="4.52881875" y1="4.191" x2="5.12318125" y2="4.27558125" layer="21"/>
<rectangle x1="6.13918125" y1="4.191" x2="9.86281875" y2="4.27558125" layer="21"/>
<rectangle x1="0.127" y1="4.27558125" x2="0.46481875" y2="4.36041875" layer="21"/>
<rectangle x1="1.143" y1="4.27558125" x2="1.48081875" y2="4.36041875" layer="21"/>
<rectangle x1="2.58318125" y1="4.27558125" x2="3.76681875" y2="4.36041875" layer="21"/>
<rectangle x1="4.52881875" y1="4.27558125" x2="5.207" y2="4.36041875" layer="21"/>
<rectangle x1="5.969" y1="4.27558125" x2="9.86281875" y2="4.36041875" layer="21"/>
<rectangle x1="0.21081875" y1="4.36041875" x2="0.46481875" y2="4.445" layer="21"/>
<rectangle x1="1.22681875" y1="4.36041875" x2="1.397" y2="4.445" layer="21"/>
<rectangle x1="2.58318125" y1="4.36041875" x2="3.59918125" y2="4.445" layer="21"/>
<rectangle x1="4.52881875" y1="4.36041875" x2="5.461" y2="4.445" layer="21"/>
<rectangle x1="5.79881875" y1="4.36041875" x2="9.86281875" y2="4.445" layer="21"/>
<rectangle x1="0.21081875" y1="4.445" x2="0.55118125" y2="4.52958125" layer="21"/>
<rectangle x1="4.52881875" y1="4.445" x2="9.779" y2="4.52958125" layer="21"/>
<rectangle x1="0.29718125" y1="4.52958125" x2="0.635" y2="4.61441875" layer="21"/>
<rectangle x1="4.61518125" y1="4.52958125" x2="9.779" y2="4.61441875" layer="21"/>
<rectangle x1="0.29718125" y1="4.61441875" x2="0.635" y2="4.699" layer="21"/>
<rectangle x1="4.61518125" y1="4.61441875" x2="9.69518125" y2="4.699" layer="21"/>
<rectangle x1="0.381" y1="4.699" x2="0.71881875" y2="4.78358125" layer="21"/>
<rectangle x1="4.61518125" y1="4.699" x2="9.60881875" y2="4.78358125" layer="21"/>
<rectangle x1="0.46481875" y1="4.78358125" x2="0.80518125" y2="4.86841875" layer="21"/>
<rectangle x1="4.699" y1="4.78358125" x2="9.60881875" y2="4.86841875" layer="21"/>
<rectangle x1="0.46481875" y1="4.86841875" x2="0.889" y2="4.953" layer="21"/>
<rectangle x1="4.699" y1="4.86841875" x2="9.525" y2="4.953" layer="21"/>
<rectangle x1="0.55118125" y1="4.953" x2="1.05918125" y2="5.03758125" layer="21"/>
<rectangle x1="4.699" y1="4.953" x2="9.44118125" y2="5.03758125" layer="21"/>
<rectangle x1="0.635" y1="5.03758125" x2="1.143" y2="5.12241875" layer="21"/>
<rectangle x1="4.78281875" y1="5.03758125" x2="9.35481875" y2="5.12241875" layer="21"/>
<rectangle x1="0.71881875" y1="5.12241875" x2="1.31318125" y2="5.207" layer="21"/>
<rectangle x1="4.78281875" y1="5.12241875" x2="9.271" y2="5.207" layer="21"/>
<rectangle x1="0.889" y1="5.207" x2="1.56718125" y2="5.29158125" layer="21"/>
<rectangle x1="4.86918125" y1="5.207" x2="9.18718125" y2="5.29158125" layer="21"/>
<rectangle x1="0.97281875" y1="5.29158125" x2="9.017" y2="5.37641875" layer="21"/>
<rectangle x1="1.143" y1="5.37641875" x2="8.84681875" y2="5.461" layer="21"/>
<rectangle x1="1.31318125" y1="5.461" x2="8.67918125" y2="5.54558125" layer="21"/>
<rectangle x1="1.651" y1="5.54558125" x2="8.33881875" y2="5.63041875" layer="21"/>
</package>
<package name="UDO-LOGO-12MM" urn="urn:adsk.eagle:footprint:6649250/1">
<rectangle x1="1.651" y1="0.21158125" x2="2.24281875" y2="0.29641875" layer="21"/>
<rectangle x1="2.667" y1="0.21158125" x2="2.75081875" y2="0.29641875" layer="21"/>
<rectangle x1="3.34518125" y1="0.21158125" x2="3.429" y2="0.29641875" layer="21"/>
<rectangle x1="4.10718125" y1="0.21158125" x2="4.27481875" y2="0.29641875" layer="21"/>
<rectangle x1="4.52881875" y1="0.21158125" x2="5.12318125" y2="0.29641875" layer="21"/>
<rectangle x1="5.88518125" y1="0.21158125" x2="6.13918125" y2="0.29641875" layer="21"/>
<rectangle x1="6.64718125" y1="0.21158125" x2="6.90118125" y2="0.29641875" layer="21"/>
<rectangle x1="7.40918125" y1="0.21158125" x2="7.66318125" y2="0.29641875" layer="21"/>
<rectangle x1="8.42518125" y1="0.21158125" x2="9.017" y2="0.29641875" layer="21"/>
<rectangle x1="10.541" y1="0.21158125" x2="10.795" y2="0.29641875" layer="21"/>
<rectangle x1="1.56718125" y1="0.29641875" x2="1.73481875" y2="0.381" layer="21"/>
<rectangle x1="2.07518125" y1="0.29641875" x2="2.32918125" y2="0.381" layer="21"/>
<rectangle x1="2.58318125" y1="0.29641875" x2="2.75081875" y2="0.381" layer="21"/>
<rectangle x1="3.34518125" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="3.51281875" y1="0.29641875" x2="3.683" y2="0.381" layer="21"/>
<rectangle x1="4.02081875" y1="0.29641875" x2="4.27481875" y2="0.381" layer="21"/>
<rectangle x1="4.52881875" y1="0.29641875" x2="5.29081875" y2="0.381" layer="21"/>
<rectangle x1="5.79881875" y1="0.29641875" x2="6.30681875" y2="0.381" layer="21"/>
<rectangle x1="6.56081875" y1="0.29641875" x2="6.985" y2="0.381" layer="21"/>
<rectangle x1="7.32281875" y1="0.29641875" x2="7.83081875" y2="0.381" layer="21"/>
<rectangle x1="8.33881875" y1="0.29641875" x2="8.509" y2="0.381" layer="21"/>
<rectangle x1="8.93318125" y1="0.29641875" x2="9.10081875" y2="0.381" layer="21"/>
<rectangle x1="9.35481875" y1="0.29641875" x2="9.525" y2="0.381" layer="21"/>
<rectangle x1="10.033" y1="0.29641875" x2="10.20318125" y2="0.381" layer="21"/>
<rectangle x1="10.37081875" y1="0.29641875" x2="10.96518125" y2="0.381" layer="21"/>
<rectangle x1="1.48081875" y1="0.381" x2="1.651" y2="0.46558125" layer="21"/>
<rectangle x1="2.24281875" y1="0.381" x2="2.413" y2="0.46558125" layer="21"/>
<rectangle x1="2.58318125" y1="0.381" x2="2.75081875" y2="0.46558125" layer="21"/>
<rectangle x1="3.34518125" y1="0.381" x2="3.429" y2="0.46558125" layer="21"/>
<rectangle x1="3.51281875" y1="0.381" x2="3.683" y2="0.46558125" layer="21"/>
<rectangle x1="3.937" y1="0.381" x2="4.10718125" y2="0.46558125" layer="21"/>
<rectangle x1="4.52881875" y1="0.381" x2="4.61518125" y2="0.46558125" layer="21"/>
<rectangle x1="5.207" y1="0.381" x2="5.37718125" y2="0.46558125" layer="21"/>
<rectangle x1="5.715" y1="0.381" x2="5.88518125" y2="0.46558125" layer="21"/>
<rectangle x1="6.223" y1="0.381" x2="6.39318125" y2="0.46558125" layer="21"/>
<rectangle x1="6.56081875" y1="0.381" x2="6.64718125" y2="0.46558125" layer="21"/>
<rectangle x1="6.90118125" y1="0.381" x2="7.06881875" y2="0.46558125" layer="21"/>
<rectangle x1="7.239" y1="0.381" x2="7.40918125" y2="0.46558125" layer="21"/>
<rectangle x1="7.747" y1="0.381" x2="7.91718125" y2="0.46558125" layer="21"/>
<rectangle x1="8.255" y1="0.381" x2="8.42518125" y2="0.46558125" layer="21"/>
<rectangle x1="9.017" y1="0.381" x2="9.18718125" y2="0.46558125" layer="21"/>
<rectangle x1="9.35481875" y1="0.381" x2="9.525" y2="0.46558125" layer="21"/>
<rectangle x1="10.033" y1="0.381" x2="10.20318125" y2="0.46558125" layer="21"/>
<rectangle x1="10.37081875" y1="0.381" x2="10.45718125" y2="0.46558125" layer="21"/>
<rectangle x1="10.87881875" y1="0.381" x2="11.049" y2="0.46558125" layer="21"/>
<rectangle x1="1.397" y1="0.46558125" x2="1.56718125" y2="0.55041875" layer="21"/>
<rectangle x1="2.32918125" y1="0.46558125" x2="2.49681875" y2="0.55041875" layer="21"/>
<rectangle x1="2.58318125" y1="0.46558125" x2="2.75081875" y2="0.55041875" layer="21"/>
<rectangle x1="3.34518125" y1="0.46558125" x2="3.429" y2="0.55041875" layer="21"/>
<rectangle x1="3.51281875" y1="0.46558125" x2="3.683" y2="0.55041875" layer="21"/>
<rectangle x1="3.85318125" y1="0.46558125" x2="4.02081875" y2="0.55041875" layer="21"/>
<rectangle x1="4.52881875" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="21"/>
<rectangle x1="5.29081875" y1="0.46558125" x2="5.461" y2="0.55041875" layer="21"/>
<rectangle x1="5.63118125" y1="0.46558125" x2="5.79881875" y2="0.55041875" layer="21"/>
<rectangle x1="6.30681875" y1="0.46558125" x2="6.39318125" y2="0.55041875" layer="21"/>
<rectangle x1="6.90118125" y1="0.46558125" x2="7.06881875" y2="0.55041875" layer="21"/>
<rectangle x1="7.15518125" y1="0.46558125" x2="7.32281875" y2="0.55041875" layer="21"/>
<rectangle x1="7.83081875" y1="0.46558125" x2="7.91718125" y2="0.55041875" layer="21"/>
<rectangle x1="8.17118125" y1="0.46558125" x2="8.33881875" y2="0.55041875" layer="21"/>
<rectangle x1="9.10081875" y1="0.46558125" x2="9.271" y2="0.55041875" layer="21"/>
<rectangle x1="9.35481875" y1="0.46558125" x2="9.525" y2="0.55041875" layer="21"/>
<rectangle x1="10.033" y1="0.46558125" x2="10.20318125" y2="0.55041875" layer="21"/>
<rectangle x1="10.287" y1="0.46558125" x2="10.45718125" y2="0.55041875" layer="21"/>
<rectangle x1="1.397" y1="0.55041875" x2="1.48081875" y2="0.635" layer="21"/>
<rectangle x1="2.32918125" y1="0.55041875" x2="2.49681875" y2="0.635" layer="21"/>
<rectangle x1="2.667" y1="0.55041875" x2="2.75081875" y2="0.635" layer="21"/>
<rectangle x1="3.34518125" y1="0.55041875" x2="3.429" y2="0.635" layer="21"/>
<rectangle x1="3.51281875" y1="0.55041875" x2="3.683" y2="0.635" layer="21"/>
<rectangle x1="3.85318125" y1="0.55041875" x2="4.02081875" y2="0.635" layer="21"/>
<rectangle x1="4.52881875" y1="0.55041875" x2="4.61518125" y2="0.635" layer="21"/>
<rectangle x1="5.37718125" y1="0.55041875" x2="5.461" y2="0.635" layer="21"/>
<rectangle x1="5.63118125" y1="0.55041875" x2="5.715" y2="0.635" layer="21"/>
<rectangle x1="6.30681875" y1="0.55041875" x2="6.39318125" y2="0.635" layer="21"/>
<rectangle x1="6.731" y1="0.55041875" x2="6.985" y2="0.635" layer="21"/>
<rectangle x1="7.15518125" y1="0.55041875" x2="7.91718125" y2="0.635" layer="21"/>
<rectangle x1="8.17118125" y1="0.55041875" x2="8.255" y2="0.635" layer="21"/>
<rectangle x1="9.18718125" y1="0.55041875" x2="9.271" y2="0.635" layer="21"/>
<rectangle x1="9.35481875" y1="0.55041875" x2="9.525" y2="0.635" layer="21"/>
<rectangle x1="10.033" y1="0.55041875" x2="10.20318125" y2="0.635" layer="21"/>
<rectangle x1="10.287" y1="0.55041875" x2="10.96518125" y2="0.635" layer="21"/>
<rectangle x1="1.31318125" y1="0.635" x2="1.48081875" y2="0.71958125" layer="21"/>
<rectangle x1="2.413" y1="0.635" x2="2.49681875" y2="0.71958125" layer="21"/>
<rectangle x1="2.667" y1="0.635" x2="2.75081875" y2="0.71958125" layer="21"/>
<rectangle x1="3.25881875" y1="0.635" x2="3.429" y2="0.71958125" layer="21"/>
<rectangle x1="3.51281875" y1="0.635" x2="3.683" y2="0.71958125" layer="21"/>
<rectangle x1="3.85318125" y1="0.635" x2="4.02081875" y2="0.71958125" layer="21"/>
<rectangle x1="4.52881875" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="5.37718125" y1="0.635" x2="5.54481875" y2="0.71958125" layer="21"/>
<rectangle x1="5.63118125" y1="0.635" x2="5.715" y2="0.71958125" layer="21"/>
<rectangle x1="6.30681875" y1="0.635" x2="6.39318125" y2="0.71958125" layer="21"/>
<rectangle x1="6.56081875" y1="0.635" x2="6.90118125" y2="0.71958125" layer="21"/>
<rectangle x1="7.15518125" y1="0.635" x2="7.91718125" y2="0.71958125" layer="21"/>
<rectangle x1="8.17118125" y1="0.635" x2="8.255" y2="0.71958125" layer="21"/>
<rectangle x1="9.18718125" y1="0.635" x2="9.271" y2="0.71958125" layer="21"/>
<rectangle x1="9.35481875" y1="0.635" x2="9.525" y2="0.71958125" layer="21"/>
<rectangle x1="10.033" y1="0.635" x2="10.20318125" y2="0.71958125" layer="21"/>
<rectangle x1="10.287" y1="0.635" x2="11.049" y2="0.71958125" layer="21"/>
<rectangle x1="1.31318125" y1="0.71958125" x2="1.48081875" y2="0.80441875" layer="21"/>
<rectangle x1="2.413" y1="0.71958125" x2="2.49681875" y2="0.80441875" layer="21"/>
<rectangle x1="2.667" y1="0.71958125" x2="2.75081875" y2="0.80441875" layer="21"/>
<rectangle x1="3.25881875" y1="0.71958125" x2="3.429" y2="0.80441875" layer="21"/>
<rectangle x1="3.51281875" y1="0.71958125" x2="3.683" y2="0.80441875" layer="21"/>
<rectangle x1="3.85318125" y1="0.71958125" x2="4.02081875" y2="0.80441875" layer="21"/>
<rectangle x1="4.52881875" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="5.37718125" y1="0.71958125" x2="5.54481875" y2="0.80441875" layer="21"/>
<rectangle x1="5.63118125" y1="0.71958125" x2="5.79881875" y2="0.80441875" layer="21"/>
<rectangle x1="6.30681875" y1="0.71958125" x2="6.39318125" y2="0.80441875" layer="21"/>
<rectangle x1="6.477" y1="0.71958125" x2="6.64718125" y2="0.80441875" layer="21"/>
<rectangle x1="7.15518125" y1="0.71958125" x2="7.32281875" y2="0.80441875" layer="21"/>
<rectangle x1="7.83081875" y1="0.71958125" x2="7.91718125" y2="0.80441875" layer="21"/>
<rectangle x1="8.08481875" y1="0.71958125" x2="8.255" y2="0.80441875" layer="21"/>
<rectangle x1="9.18718125" y1="0.71958125" x2="9.271" y2="0.80441875" layer="21"/>
<rectangle x1="9.44118125" y1="0.71958125" x2="9.525" y2="0.80441875" layer="21"/>
<rectangle x1="10.033" y1="0.71958125" x2="10.20318125" y2="0.80441875" layer="21"/>
<rectangle x1="10.287" y1="0.71958125" x2="10.37081875" y2="0.80441875" layer="21"/>
<rectangle x1="10.87881875" y1="0.71958125" x2="11.049" y2="0.80441875" layer="21"/>
<rectangle x1="1.31318125" y1="0.80441875" x2="1.48081875" y2="0.889" layer="21"/>
<rectangle x1="2.413" y1="0.80441875" x2="2.49681875" y2="0.889" layer="21"/>
<rectangle x1="2.667" y1="0.80441875" x2="2.83718125" y2="0.889" layer="21"/>
<rectangle x1="3.175" y1="0.80441875" x2="3.34518125" y2="0.889" layer="21"/>
<rectangle x1="3.51281875" y1="0.80441875" x2="3.683" y2="0.889" layer="21"/>
<rectangle x1="3.85318125" y1="0.80441875" x2="4.02081875" y2="0.889" layer="21"/>
<rectangle x1="4.52881875" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="5.37718125" y1="0.80441875" x2="5.54481875" y2="0.889" layer="21"/>
<rectangle x1="5.715" y1="0.80441875" x2="5.79881875" y2="0.889" layer="21"/>
<rectangle x1="6.223" y1="0.80441875" x2="6.39318125" y2="0.889" layer="21"/>
<rectangle x1="6.477" y1="0.80441875" x2="6.64718125" y2="0.889" layer="21"/>
<rectangle x1="6.90118125" y1="0.80441875" x2="7.06881875" y2="0.889" layer="21"/>
<rectangle x1="7.239" y1="0.80441875" x2="7.40918125" y2="0.889" layer="21"/>
<rectangle x1="7.747" y1="0.80441875" x2="7.91718125" y2="0.889" layer="21"/>
<rectangle x1="8.17118125" y1="0.80441875" x2="8.255" y2="0.889" layer="21"/>
<rectangle x1="9.18718125" y1="0.80441875" x2="9.271" y2="0.889" layer="21"/>
<rectangle x1="9.44118125" y1="0.80441875" x2="9.60881875" y2="0.889" layer="21"/>
<rectangle x1="9.94918125" y1="0.80441875" x2="10.11681875" y2="0.889" layer="21"/>
<rectangle x1="10.287" y1="0.80441875" x2="10.45718125" y2="0.889" layer="21"/>
<rectangle x1="10.87881875" y1="0.80441875" x2="11.049" y2="0.889" layer="21"/>
<rectangle x1="1.31318125" y1="0.889" x2="1.48081875" y2="0.97358125" layer="21"/>
<rectangle x1="2.413" y1="0.889" x2="2.49681875" y2="0.97358125" layer="21"/>
<rectangle x1="2.667" y1="0.889" x2="3.25881875" y2="0.97358125" layer="21"/>
<rectangle x1="3.51281875" y1="0.889" x2="3.683" y2="0.97358125" layer="21"/>
<rectangle x1="3.76681875" y1="0.889" x2="4.27481875" y2="0.97358125" layer="21"/>
<rectangle x1="4.52881875" y1="0.889" x2="4.61518125" y2="0.97358125" layer="21"/>
<rectangle x1="5.37718125" y1="0.889" x2="5.54481875" y2="0.97358125" layer="21"/>
<rectangle x1="5.715" y1="0.889" x2="6.30681875" y2="0.97358125" layer="21"/>
<rectangle x1="6.56081875" y1="0.889" x2="6.985" y2="0.97358125" layer="21"/>
<rectangle x1="7.32281875" y1="0.889" x2="7.83081875" y2="0.97358125" layer="21"/>
<rectangle x1="8.17118125" y1="0.889" x2="8.255" y2="0.97358125" layer="21"/>
<rectangle x1="9.18718125" y1="0.889" x2="9.271" y2="0.97358125" layer="21"/>
<rectangle x1="9.35481875" y1="0.889" x2="10.033" y2="0.97358125" layer="21"/>
<rectangle x1="10.37081875" y1="0.889" x2="10.96518125" y2="0.97358125" layer="21"/>
<rectangle x1="1.31318125" y1="0.97358125" x2="1.48081875" y2="1.05841875" layer="21"/>
<rectangle x1="2.413" y1="0.97358125" x2="2.49681875" y2="1.05841875" layer="21"/>
<rectangle x1="2.667" y1="0.97358125" x2="2.75081875" y2="1.05841875" layer="21"/>
<rectangle x1="2.921" y1="0.97358125" x2="3.175" y2="1.05841875" layer="21"/>
<rectangle x1="3.76681875" y1="0.97358125" x2="4.27481875" y2="1.05841875" layer="21"/>
<rectangle x1="4.52881875" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="21"/>
<rectangle x1="5.37718125" y1="0.97358125" x2="5.54481875" y2="1.05841875" layer="21"/>
<rectangle x1="5.88518125" y1="0.97358125" x2="6.13918125" y2="1.05841875" layer="21"/>
<rectangle x1="6.64718125" y1="0.97358125" x2="6.90118125" y2="1.05841875" layer="21"/>
<rectangle x1="7.40918125" y1="0.97358125" x2="7.66318125" y2="1.05841875" layer="21"/>
<rectangle x1="8.17118125" y1="0.97358125" x2="8.33881875" y2="1.05841875" layer="21"/>
<rectangle x1="9.10081875" y1="0.97358125" x2="9.271" y2="1.05841875" layer="21"/>
<rectangle x1="9.60881875" y1="0.97358125" x2="9.94918125" y2="1.05841875" layer="21"/>
<rectangle x1="10.541" y1="0.97358125" x2="10.795" y2="1.05841875" layer="21"/>
<rectangle x1="1.31318125" y1="1.05841875" x2="1.48081875" y2="1.143" layer="21"/>
<rectangle x1="2.413" y1="1.05841875" x2="2.49681875" y2="1.143" layer="21"/>
<rectangle x1="3.51281875" y1="1.05841875" x2="3.683" y2="1.143" layer="21"/>
<rectangle x1="3.85318125" y1="1.05841875" x2="4.02081875" y2="1.143" layer="21"/>
<rectangle x1="4.52881875" y1="1.05841875" x2="4.61518125" y2="1.143" layer="21"/>
<rectangle x1="5.29081875" y1="1.05841875" x2="5.461" y2="1.143" layer="21"/>
<rectangle x1="8.255" y1="1.05841875" x2="8.42518125" y2="1.143" layer="21"/>
<rectangle x1="9.017" y1="1.05841875" x2="9.18718125" y2="1.143" layer="21"/>
<rectangle x1="1.31318125" y1="1.143" x2="1.48081875" y2="1.22758125" layer="21"/>
<rectangle x1="2.413" y1="1.143" x2="2.49681875" y2="1.22758125" layer="21"/>
<rectangle x1="3.51281875" y1="1.143" x2="3.683" y2="1.22758125" layer="21"/>
<rectangle x1="3.85318125" y1="1.143" x2="4.02081875" y2="1.22758125" layer="21"/>
<rectangle x1="4.52881875" y1="1.143" x2="4.61518125" y2="1.22758125" layer="21"/>
<rectangle x1="5.29081875" y1="1.143" x2="5.461" y2="1.22758125" layer="21"/>
<rectangle x1="8.33881875" y1="1.143" x2="8.509" y2="1.22758125" layer="21"/>
<rectangle x1="8.93318125" y1="1.143" x2="9.10081875" y2="1.22758125" layer="21"/>
<rectangle x1="1.31318125" y1="1.22758125" x2="1.48081875" y2="1.31241875" layer="21"/>
<rectangle x1="2.413" y1="1.22758125" x2="2.49681875" y2="1.31241875" layer="21"/>
<rectangle x1="3.85318125" y1="1.22758125" x2="4.02081875" y2="1.31241875" layer="21"/>
<rectangle x1="4.52881875" y1="1.22758125" x2="4.61518125" y2="1.31241875" layer="21"/>
<rectangle x1="5.12318125" y1="1.22758125" x2="5.37718125" y2="1.31241875" layer="21"/>
<rectangle x1="8.42518125" y1="1.22758125" x2="9.017" y2="1.31241875" layer="21"/>
<rectangle x1="4.52881875" y1="1.31241875" x2="5.207" y2="1.397" layer="21"/>
<rectangle x1="8.59281875" y1="1.31241875" x2="8.84681875" y2="1.397" layer="21"/>
<rectangle x1="2.24281875" y1="1.905" x2="9.779" y2="1.98958125" layer="21"/>
<rectangle x1="1.82118125" y1="1.98958125" x2="10.287" y2="2.07441875" layer="21"/>
<rectangle x1="1.56718125" y1="2.07441875" x2="10.541" y2="2.159" layer="21"/>
<rectangle x1="1.31318125" y1="2.159" x2="10.71118125" y2="2.24358125" layer="21"/>
<rectangle x1="1.22681875" y1="2.24358125" x2="2.07518125" y2="2.32841875" layer="21"/>
<rectangle x1="5.88518125" y1="2.24358125" x2="10.87881875" y2="2.32841875" layer="21"/>
<rectangle x1="1.05918125" y1="2.32841875" x2="1.73481875" y2="2.413" layer="21"/>
<rectangle x1="5.88518125" y1="2.32841875" x2="10.96518125" y2="2.413" layer="21"/>
<rectangle x1="0.97281875" y1="2.413" x2="1.56718125" y2="2.49758125" layer="21"/>
<rectangle x1="5.79881875" y1="2.413" x2="11.13281875" y2="2.49758125" layer="21"/>
<rectangle x1="0.889" y1="2.49758125" x2="1.397" y2="2.58241875" layer="21"/>
<rectangle x1="5.79881875" y1="2.49758125" x2="11.21918125" y2="2.58241875" layer="21"/>
<rectangle x1="0.71881875" y1="2.58241875" x2="1.22681875" y2="2.667" layer="21"/>
<rectangle x1="5.715" y1="2.58241875" x2="11.303" y2="2.667" layer="21"/>
<rectangle x1="0.635" y1="2.667" x2="1.143" y2="2.75158125" layer="21"/>
<rectangle x1="5.715" y1="2.667" x2="11.38681875" y2="2.75158125" layer="21"/>
<rectangle x1="0.635" y1="2.75158125" x2="1.05918125" y2="2.83641875" layer="21"/>
<rectangle x1="5.63118125" y1="2.75158125" x2="11.47318125" y2="2.83641875" layer="21"/>
<rectangle x1="0.55118125" y1="2.83641875" x2="0.97281875" y2="2.921" layer="21"/>
<rectangle x1="5.63118125" y1="2.83641875" x2="11.557" y2="2.921" layer="21"/>
<rectangle x1="0.46481875" y1="2.921" x2="0.889" y2="3.00558125" layer="21"/>
<rectangle x1="5.63118125" y1="2.921" x2="11.557" y2="3.00558125" layer="21"/>
<rectangle x1="0.381" y1="3.00558125" x2="0.80518125" y2="3.09041875" layer="21"/>
<rectangle x1="5.54481875" y1="3.00558125" x2="11.64081875" y2="3.09041875" layer="21"/>
<rectangle x1="0.381" y1="3.09041875" x2="0.71881875" y2="3.175" layer="21"/>
<rectangle x1="5.54481875" y1="3.09041875" x2="11.72718125" y2="3.175" layer="21"/>
<rectangle x1="0.29718125" y1="3.175" x2="0.71881875" y2="3.25958125" layer="21"/>
<rectangle x1="5.54481875" y1="3.175" x2="11.72718125" y2="3.25958125" layer="21"/>
<rectangle x1="0.29718125" y1="3.25958125" x2="0.635" y2="3.34441875" layer="21"/>
<rectangle x1="5.54481875" y1="3.25958125" x2="11.811" y2="3.34441875" layer="21"/>
<rectangle x1="0.21081875" y1="3.34441875" x2="0.55118125" y2="3.429" layer="21"/>
<rectangle x1="2.159" y1="3.34441875" x2="2.75081875" y2="3.429" layer="21"/>
<rectangle x1="3.51281875" y1="3.34441875" x2="4.27481875" y2="3.429" layer="21"/>
<rectangle x1="5.54481875" y1="3.34441875" x2="6.477" y2="3.429" layer="21"/>
<rectangle x1="7.06881875" y1="3.34441875" x2="8.001" y2="3.429" layer="21"/>
<rectangle x1="8.17118125" y1="3.34441875" x2="9.10081875" y2="3.429" layer="21"/>
<rectangle x1="9.271" y1="3.34441875" x2="9.86281875" y2="3.429" layer="21"/>
<rectangle x1="10.287" y1="3.34441875" x2="11.811" y2="3.429" layer="21"/>
<rectangle x1="0.21081875" y1="3.429" x2="0.55118125" y2="3.51358125" layer="21"/>
<rectangle x1="1.98881875" y1="3.429" x2="2.921" y2="3.51358125" layer="21"/>
<rectangle x1="3.429" y1="3.429" x2="4.445" y2="3.51358125" layer="21"/>
<rectangle x1="5.461" y1="3.429" x2="6.30681875" y2="3.51358125" layer="21"/>
<rectangle x1="7.32281875" y1="3.429" x2="8.001" y2="3.51358125" layer="21"/>
<rectangle x1="8.255" y1="3.429" x2="9.017" y2="3.51358125" layer="21"/>
<rectangle x1="9.271" y1="3.429" x2="9.69518125" y2="3.51358125" layer="21"/>
<rectangle x1="10.541" y1="3.429" x2="11.89481875" y2="3.51358125" layer="21"/>
<rectangle x1="0.127" y1="3.51358125" x2="0.55118125" y2="3.59841875" layer="21"/>
<rectangle x1="1.82118125" y1="3.51358125" x2="3.00481875" y2="3.59841875" layer="21"/>
<rectangle x1="3.429" y1="3.51358125" x2="4.61518125" y2="3.59841875" layer="21"/>
<rectangle x1="5.461" y1="3.51358125" x2="6.13918125" y2="3.59841875" layer="21"/>
<rectangle x1="7.40918125" y1="3.51358125" x2="7.91718125" y2="3.59841875" layer="21"/>
<rectangle x1="8.255" y1="3.51358125" x2="9.017" y2="3.59841875" layer="21"/>
<rectangle x1="9.271" y1="3.51358125" x2="9.60881875" y2="3.59841875" layer="21"/>
<rectangle x1="10.62481875" y1="3.51358125" x2="11.89481875" y2="3.59841875" layer="21"/>
<rectangle x1="0.127" y1="3.59841875" x2="0.46481875" y2="3.683" layer="21"/>
<rectangle x1="1.73481875" y1="3.59841875" x2="3.175" y2="3.683" layer="21"/>
<rectangle x1="3.51281875" y1="3.59841875" x2="4.699" y2="3.683" layer="21"/>
<rectangle x1="5.461" y1="3.59841875" x2="6.05281875" y2="3.683" layer="21"/>
<rectangle x1="7.493" y1="3.59841875" x2="7.91718125" y2="3.683" layer="21"/>
<rectangle x1="8.255" y1="3.59841875" x2="9.017" y2="3.683" layer="21"/>
<rectangle x1="9.271" y1="3.59841875" x2="9.525" y2="3.683" layer="21"/>
<rectangle x1="10.033" y1="3.59841875" x2="10.20318125" y2="3.683" layer="21"/>
<rectangle x1="10.71118125" y1="3.59841875" x2="11.89481875" y2="3.683" layer="21"/>
<rectangle x1="0.127" y1="3.683" x2="0.46481875" y2="3.76758125" layer="21"/>
<rectangle x1="1.651" y1="3.683" x2="2.159" y2="3.76758125" layer="21"/>
<rectangle x1="2.75081875" y1="3.683" x2="3.175" y2="3.76758125" layer="21"/>
<rectangle x1="4.27481875" y1="3.683" x2="4.78281875" y2="3.76758125" layer="21"/>
<rectangle x1="5.461" y1="3.683" x2="5.969" y2="3.76758125" layer="21"/>
<rectangle x1="6.477" y1="3.683" x2="7.06881875" y2="3.76758125" layer="21"/>
<rectangle x1="7.57681875" y1="3.683" x2="7.91718125" y2="3.76758125" layer="21"/>
<rectangle x1="8.255" y1="3.683" x2="9.017" y2="3.76758125" layer="21"/>
<rectangle x1="9.271" y1="3.683" x2="9.525" y2="3.76758125" layer="21"/>
<rectangle x1="9.86281875" y1="3.683" x2="10.37081875" y2="3.76758125" layer="21"/>
<rectangle x1="10.71118125" y1="3.683" x2="11.98118125" y2="3.76758125" layer="21"/>
<rectangle x1="0.127" y1="3.76758125" x2="0.381" y2="3.85241875" layer="21"/>
<rectangle x1="1.56718125" y1="3.76758125" x2="1.98881875" y2="3.85241875" layer="21"/>
<rectangle x1="2.83718125" y1="3.76758125" x2="3.25881875" y2="3.85241875" layer="21"/>
<rectangle x1="4.445" y1="3.76758125" x2="4.86918125" y2="3.85241875" layer="21"/>
<rectangle x1="5.461" y1="3.76758125" x2="5.969" y2="3.85241875" layer="21"/>
<rectangle x1="6.39318125" y1="3.76758125" x2="7.15518125" y2="3.85241875" layer="21"/>
<rectangle x1="7.66318125" y1="3.76758125" x2="7.91718125" y2="3.85241875" layer="21"/>
<rectangle x1="8.255" y1="3.76758125" x2="9.017" y2="3.85241875" layer="21"/>
<rectangle x1="9.271" y1="3.76758125" x2="9.44118125" y2="3.85241875" layer="21"/>
<rectangle x1="9.779" y1="3.76758125" x2="10.541" y2="3.85241875" layer="21"/>
<rectangle x1="10.62481875" y1="3.76758125" x2="11.98118125" y2="3.85241875" layer="21"/>
<rectangle x1="0.04318125" y1="3.85241875" x2="0.381" y2="3.937" layer="21"/>
<rectangle x1="1.56718125" y1="3.85241875" x2="1.905" y2="3.937" layer="21"/>
<rectangle x1="2.921" y1="3.85241875" x2="3.34518125" y2="3.937" layer="21"/>
<rectangle x1="4.52881875" y1="3.85241875" x2="4.86918125" y2="3.937" layer="21"/>
<rectangle x1="5.461" y1="3.85241875" x2="5.88518125" y2="3.937" layer="21"/>
<rectangle x1="6.30681875" y1="3.85241875" x2="7.239" y2="3.937" layer="21"/>
<rectangle x1="7.66318125" y1="3.85241875" x2="7.91718125" y2="3.937" layer="21"/>
<rectangle x1="8.255" y1="3.85241875" x2="9.017" y2="3.937" layer="21"/>
<rectangle x1="9.271" y1="3.85241875" x2="9.44118125" y2="3.937" layer="21"/>
<rectangle x1="9.779" y1="3.85241875" x2="9.86281875" y2="3.937" layer="21"/>
<rectangle x1="10.45718125" y1="3.85241875" x2="11.98118125" y2="3.937" layer="21"/>
<rectangle x1="0.04318125" y1="3.937" x2="0.381" y2="4.02158125" layer="21"/>
<rectangle x1="1.48081875" y1="3.937" x2="1.905" y2="4.02158125" layer="21"/>
<rectangle x1="3.00481875" y1="3.937" x2="3.34518125" y2="4.02158125" layer="21"/>
<rectangle x1="4.52881875" y1="3.937" x2="4.953" y2="4.02158125" layer="21"/>
<rectangle x1="5.461" y1="3.937" x2="5.88518125" y2="4.02158125" layer="21"/>
<rectangle x1="6.223" y1="3.937" x2="7.32281875" y2="4.02158125" layer="21"/>
<rectangle x1="7.747" y1="3.937" x2="7.91718125" y2="4.02158125" layer="21"/>
<rectangle x1="8.255" y1="3.937" x2="9.017" y2="4.02158125" layer="21"/>
<rectangle x1="9.271" y1="3.937" x2="9.44118125" y2="4.02158125" layer="21"/>
<rectangle x1="10.795" y1="3.937" x2="11.98118125" y2="4.02158125" layer="21"/>
<rectangle x1="0.04318125" y1="4.02158125" x2="0.381" y2="4.10641875" layer="21"/>
<rectangle x1="1.48081875" y1="4.02158125" x2="1.82118125" y2="4.10641875" layer="21"/>
<rectangle x1="3.00481875" y1="4.02158125" x2="3.34518125" y2="4.10641875" layer="21"/>
<rectangle x1="4.61518125" y1="4.02158125" x2="4.953" y2="4.10641875" layer="21"/>
<rectangle x1="5.461" y1="4.02158125" x2="5.79881875" y2="4.10641875" layer="21"/>
<rectangle x1="6.13918125" y1="4.02158125" x2="7.40918125" y2="4.10641875" layer="21"/>
<rectangle x1="7.747" y1="4.02158125" x2="7.91718125" y2="4.10641875" layer="21"/>
<rectangle x1="8.255" y1="4.02158125" x2="9.017" y2="4.10641875" layer="21"/>
<rectangle x1="9.271" y1="4.02158125" x2="9.44118125" y2="4.10641875" layer="21"/>
<rectangle x1="10.795" y1="4.02158125" x2="11.98118125" y2="4.10641875" layer="21"/>
<rectangle x1="0.04318125" y1="4.10641875" x2="0.381" y2="4.191" layer="21"/>
<rectangle x1="1.48081875" y1="4.10641875" x2="1.82118125" y2="4.191" layer="21"/>
<rectangle x1="3.09118125" y1="4.10641875" x2="3.429" y2="4.191" layer="21"/>
<rectangle x1="4.61518125" y1="4.10641875" x2="4.953" y2="4.191" layer="21"/>
<rectangle x1="5.37718125" y1="4.10641875" x2="5.79881875" y2="4.191" layer="21"/>
<rectangle x1="6.13918125" y1="4.10641875" x2="7.40918125" y2="4.191" layer="21"/>
<rectangle x1="7.747" y1="4.10641875" x2="7.91718125" y2="4.191" layer="21"/>
<rectangle x1="8.255" y1="4.10641875" x2="9.017" y2="4.191" layer="21"/>
<rectangle x1="9.271" y1="4.10641875" x2="9.44118125" y2="4.191" layer="21"/>
<rectangle x1="10.795" y1="4.10641875" x2="11.98118125" y2="4.191" layer="21"/>
<rectangle x1="0.04318125" y1="4.191" x2="0.381" y2="4.27558125" layer="21"/>
<rectangle x1="1.48081875" y1="4.191" x2="1.73481875" y2="4.27558125" layer="21"/>
<rectangle x1="3.09118125" y1="4.191" x2="3.429" y2="4.27558125" layer="21"/>
<rectangle x1="4.699" y1="4.191" x2="4.953" y2="4.27558125" layer="21"/>
<rectangle x1="5.37718125" y1="4.191" x2="5.79881875" y2="4.27558125" layer="21"/>
<rectangle x1="6.13918125" y1="4.191" x2="7.40918125" y2="4.27558125" layer="21"/>
<rectangle x1="7.747" y1="4.191" x2="7.91718125" y2="4.27558125" layer="21"/>
<rectangle x1="8.255" y1="4.191" x2="8.93318125" y2="4.27558125" layer="21"/>
<rectangle x1="9.271" y1="4.191" x2="9.44118125" y2="4.27558125" layer="21"/>
<rectangle x1="9.779" y1="4.191" x2="10.45718125" y2="4.27558125" layer="21"/>
<rectangle x1="10.71118125" y1="4.191" x2="11.98118125" y2="4.27558125" layer="21"/>
<rectangle x1="0.04318125" y1="4.27558125" x2="0.381" y2="4.36041875" layer="21"/>
<rectangle x1="1.48081875" y1="4.27558125" x2="1.73481875" y2="4.36041875" layer="21"/>
<rectangle x1="3.09118125" y1="4.27558125" x2="3.429" y2="4.36041875" layer="21"/>
<rectangle x1="4.699" y1="4.27558125" x2="4.953" y2="4.36041875" layer="21"/>
<rectangle x1="5.37718125" y1="4.27558125" x2="5.79881875" y2="4.36041875" layer="21"/>
<rectangle x1="6.13918125" y1="4.27558125" x2="7.40918125" y2="4.36041875" layer="21"/>
<rectangle x1="7.747" y1="4.27558125" x2="7.91718125" y2="4.36041875" layer="21"/>
<rectangle x1="8.33881875" y1="4.27558125" x2="8.93318125" y2="4.36041875" layer="21"/>
<rectangle x1="9.271" y1="4.27558125" x2="9.525" y2="4.36041875" layer="21"/>
<rectangle x1="9.86281875" y1="4.27558125" x2="10.37081875" y2="4.36041875" layer="21"/>
<rectangle x1="10.71118125" y1="4.27558125" x2="11.98118125" y2="4.36041875" layer="21"/>
<rectangle x1="0.04318125" y1="4.36041875" x2="0.381" y2="4.445" layer="21"/>
<rectangle x1="1.48081875" y1="4.36041875" x2="1.73481875" y2="4.445" layer="21"/>
<rectangle x1="3.09118125" y1="4.36041875" x2="3.429" y2="4.445" layer="21"/>
<rectangle x1="4.699" y1="4.36041875" x2="4.953" y2="4.445" layer="21"/>
<rectangle x1="5.37718125" y1="4.36041875" x2="5.79881875" y2="4.445" layer="21"/>
<rectangle x1="6.13918125" y1="4.36041875" x2="7.40918125" y2="4.445" layer="21"/>
<rectangle x1="7.747" y1="4.36041875" x2="7.91718125" y2="4.445" layer="21"/>
<rectangle x1="8.509" y1="4.36041875" x2="8.763" y2="4.445" layer="21"/>
<rectangle x1="9.18718125" y1="4.36041875" x2="9.525" y2="4.445" layer="21"/>
<rectangle x1="9.94918125" y1="4.36041875" x2="10.20318125" y2="4.445" layer="21"/>
<rectangle x1="10.62481875" y1="4.36041875" x2="11.98118125" y2="4.445" layer="21"/>
<rectangle x1="0.04318125" y1="4.445" x2="0.381" y2="4.52958125" layer="21"/>
<rectangle x1="1.48081875" y1="4.445" x2="1.73481875" y2="4.52958125" layer="21"/>
<rectangle x1="3.09118125" y1="4.445" x2="3.429" y2="4.52958125" layer="21"/>
<rectangle x1="4.61518125" y1="4.445" x2="4.953" y2="4.52958125" layer="21"/>
<rectangle x1="5.37718125" y1="4.445" x2="5.79881875" y2="4.52958125" layer="21"/>
<rectangle x1="6.13918125" y1="4.445" x2="7.40918125" y2="4.52958125" layer="21"/>
<rectangle x1="7.747" y1="4.445" x2="7.91718125" y2="4.52958125" layer="21"/>
<rectangle x1="9.10081875" y1="4.445" x2="9.60881875" y2="4.52958125" layer="21"/>
<rectangle x1="10.541" y1="4.445" x2="11.98118125" y2="4.52958125" layer="21"/>
<rectangle x1="0.04318125" y1="4.52958125" x2="0.381" y2="4.61441875" layer="21"/>
<rectangle x1="1.48081875" y1="4.52958125" x2="1.73481875" y2="4.61441875" layer="21"/>
<rectangle x1="3.09118125" y1="4.52958125" x2="3.429" y2="4.61441875" layer="21"/>
<rectangle x1="4.61518125" y1="4.52958125" x2="4.953" y2="4.61441875" layer="21"/>
<rectangle x1="5.461" y1="4.52958125" x2="5.79881875" y2="4.61441875" layer="21"/>
<rectangle x1="6.13918125" y1="4.52958125" x2="7.40918125" y2="4.61441875" layer="21"/>
<rectangle x1="7.747" y1="4.52958125" x2="8.001" y2="4.61441875" layer="21"/>
<rectangle x1="9.017" y1="4.52958125" x2="9.69518125" y2="4.61441875" layer="21"/>
<rectangle x1="10.45718125" y1="4.52958125" x2="11.98118125" y2="4.61441875" layer="21"/>
<rectangle x1="0.04318125" y1="4.61441875" x2="0.381" y2="4.699" layer="21"/>
<rectangle x1="1.48081875" y1="4.61441875" x2="1.73481875" y2="4.699" layer="21"/>
<rectangle x1="3.09118125" y1="4.61441875" x2="3.429" y2="4.699" layer="21"/>
<rectangle x1="4.52881875" y1="4.61441875" x2="4.953" y2="4.699" layer="21"/>
<rectangle x1="5.461" y1="4.61441875" x2="5.79881875" y2="4.699" layer="21"/>
<rectangle x1="6.223" y1="4.61441875" x2="7.32281875" y2="4.699" layer="21"/>
<rectangle x1="7.747" y1="4.61441875" x2="8.001" y2="4.699" layer="21"/>
<rectangle x1="8.17118125" y1="4.61441875" x2="8.42518125" y2="4.699" layer="21"/>
<rectangle x1="8.84681875" y1="4.61441875" x2="9.86281875" y2="4.699" layer="21"/>
<rectangle x1="10.287" y1="4.61441875" x2="11.98118125" y2="4.699" layer="21"/>
<rectangle x1="0.04318125" y1="4.699" x2="0.381" y2="4.78358125" layer="21"/>
<rectangle x1="1.48081875" y1="4.699" x2="1.73481875" y2="4.78358125" layer="21"/>
<rectangle x1="3.09118125" y1="4.699" x2="3.429" y2="4.78358125" layer="21"/>
<rectangle x1="4.52881875" y1="4.699" x2="4.86918125" y2="4.78358125" layer="21"/>
<rectangle x1="5.461" y1="4.699" x2="5.88518125" y2="4.78358125" layer="21"/>
<rectangle x1="6.30681875" y1="4.699" x2="7.32281875" y2="4.78358125" layer="21"/>
<rectangle x1="7.66318125" y1="4.699" x2="11.98118125" y2="4.78358125" layer="21"/>
<rectangle x1="0.127" y1="4.78358125" x2="0.381" y2="4.86841875" layer="21"/>
<rectangle x1="1.48081875" y1="4.78358125" x2="1.73481875" y2="4.86841875" layer="21"/>
<rectangle x1="3.09118125" y1="4.78358125" x2="3.429" y2="4.86841875" layer="21"/>
<rectangle x1="4.445" y1="4.78358125" x2="4.86918125" y2="4.86841875" layer="21"/>
<rectangle x1="5.461" y1="4.78358125" x2="5.88518125" y2="4.86841875" layer="21"/>
<rectangle x1="6.39318125" y1="4.78358125" x2="7.239" y2="4.86841875" layer="21"/>
<rectangle x1="7.66318125" y1="4.78358125" x2="11.98118125" y2="4.86841875" layer="21"/>
<rectangle x1="0.127" y1="4.86841875" x2="0.46481875" y2="4.953" layer="21"/>
<rectangle x1="1.48081875" y1="4.86841875" x2="1.73481875" y2="4.953" layer="21"/>
<rectangle x1="3.09118125" y1="4.86841875" x2="3.429" y2="4.953" layer="21"/>
<rectangle x1="4.27481875" y1="4.86841875" x2="4.78281875" y2="4.953" layer="21"/>
<rectangle x1="5.461" y1="4.86841875" x2="5.969" y2="4.953" layer="21"/>
<rectangle x1="6.477" y1="4.86841875" x2="7.06881875" y2="4.953" layer="21"/>
<rectangle x1="7.57681875" y1="4.86841875" x2="11.98118125" y2="4.953" layer="21"/>
<rectangle x1="0.127" y1="4.953" x2="0.46481875" y2="5.03758125" layer="21"/>
<rectangle x1="1.48081875" y1="4.953" x2="1.73481875" y2="5.03758125" layer="21"/>
<rectangle x1="3.09118125" y1="4.953" x2="4.699" y2="5.03758125" layer="21"/>
<rectangle x1="5.461" y1="4.953" x2="6.05281875" y2="5.03758125" layer="21"/>
<rectangle x1="7.493" y1="4.953" x2="11.89481875" y2="5.03758125" layer="21"/>
<rectangle x1="0.127" y1="5.03758125" x2="0.46481875" y2="5.12241875" layer="21"/>
<rectangle x1="1.48081875" y1="5.03758125" x2="1.73481875" y2="5.12241875" layer="21"/>
<rectangle x1="3.09118125" y1="5.03758125" x2="4.61518125" y2="5.12241875" layer="21"/>
<rectangle x1="5.461" y1="5.03758125" x2="6.13918125" y2="5.12241875" layer="21"/>
<rectangle x1="7.40918125" y1="5.03758125" x2="11.89481875" y2="5.12241875" layer="21"/>
<rectangle x1="0.21081875" y1="5.12241875" x2="0.55118125" y2="5.207" layer="21"/>
<rectangle x1="1.48081875" y1="5.12241875" x2="1.73481875" y2="5.207" layer="21"/>
<rectangle x1="3.09118125" y1="5.12241875" x2="4.52881875" y2="5.207" layer="21"/>
<rectangle x1="5.461" y1="5.12241875" x2="6.223" y2="5.207" layer="21"/>
<rectangle x1="7.32281875" y1="5.12241875" x2="11.89481875" y2="5.207" layer="21"/>
<rectangle x1="0.21081875" y1="5.207" x2="0.55118125" y2="5.29158125" layer="21"/>
<rectangle x1="1.48081875" y1="5.207" x2="1.73481875" y2="5.29158125" layer="21"/>
<rectangle x1="3.175" y1="5.207" x2="4.36118125" y2="5.29158125" layer="21"/>
<rectangle x1="5.461" y1="5.207" x2="6.39318125" y2="5.29158125" layer="21"/>
<rectangle x1="7.15518125" y1="5.207" x2="11.811" y2="5.29158125" layer="21"/>
<rectangle x1="0.29718125" y1="5.29158125" x2="0.635" y2="5.37641875" layer="21"/>
<rectangle x1="5.54481875" y1="5.29158125" x2="11.811" y2="5.37641875" layer="21"/>
<rectangle x1="0.29718125" y1="5.37641875" x2="0.71881875" y2="5.461" layer="21"/>
<rectangle x1="5.54481875" y1="5.37641875" x2="11.72718125" y2="5.461" layer="21"/>
<rectangle x1="0.381" y1="5.461" x2="0.71881875" y2="5.54558125" layer="21"/>
<rectangle x1="5.54481875" y1="5.461" x2="11.72718125" y2="5.54558125" layer="21"/>
<rectangle x1="0.381" y1="5.54558125" x2="0.80518125" y2="5.63041875" layer="21"/>
<rectangle x1="5.54481875" y1="5.54558125" x2="11.64081875" y2="5.63041875" layer="21"/>
<rectangle x1="0.46481875" y1="5.63041875" x2="0.889" y2="5.715" layer="21"/>
<rectangle x1="5.63118125" y1="5.63041875" x2="11.64081875" y2="5.715" layer="21"/>
<rectangle x1="0.55118125" y1="5.715" x2="0.97281875" y2="5.79958125" layer="21"/>
<rectangle x1="5.63118125" y1="5.715" x2="11.557" y2="5.79958125" layer="21"/>
<rectangle x1="0.55118125" y1="5.79958125" x2="1.05918125" y2="5.88441875" layer="21"/>
<rectangle x1="5.63118125" y1="5.79958125" x2="11.47318125" y2="5.88441875" layer="21"/>
<rectangle x1="0.635" y1="5.88441875" x2="1.143" y2="5.969" layer="21"/>
<rectangle x1="5.715" y1="5.88441875" x2="11.38681875" y2="5.969" layer="21"/>
<rectangle x1="0.71881875" y1="5.969" x2="1.22681875" y2="6.05358125" layer="21"/>
<rectangle x1="5.715" y1="5.969" x2="11.303" y2="6.05358125" layer="21"/>
<rectangle x1="0.80518125" y1="6.05358125" x2="1.397" y2="6.13841875" layer="21"/>
<rectangle x1="5.79881875" y1="6.05358125" x2="11.21918125" y2="6.13841875" layer="21"/>
<rectangle x1="0.889" y1="6.13841875" x2="1.56718125" y2="6.223" layer="21"/>
<rectangle x1="5.79881875" y1="6.13841875" x2="11.13281875" y2="6.223" layer="21"/>
<rectangle x1="1.05918125" y1="6.223" x2="1.73481875" y2="6.30758125" layer="21"/>
<rectangle x1="5.88518125" y1="6.223" x2="11.049" y2="6.30758125" layer="21"/>
<rectangle x1="1.143" y1="6.30758125" x2="1.98881875" y2="6.39241875" layer="21"/>
<rectangle x1="5.88518125" y1="6.30758125" x2="10.87881875" y2="6.39241875" layer="21"/>
<rectangle x1="1.31318125" y1="6.39241875" x2="10.71118125" y2="6.477" layer="21"/>
<rectangle x1="1.48081875" y1="6.477" x2="10.541" y2="6.56158125" layer="21"/>
<rectangle x1="1.73481875" y1="6.56158125" x2="10.37081875" y2="6.64641875" layer="21"/>
</package>
<package name="UDO-LOGO-15MM" urn="urn:adsk.eagle:footprint:6649249/1">
<rectangle x1="2.24281875" y1="0.21158125" x2="2.58318125" y2="0.29641875" layer="21"/>
<rectangle x1="10.71118125" y1="0.21158125" x2="11.049" y2="0.29641875" layer="21"/>
<rectangle x1="1.98881875" y1="0.29641875" x2="2.83718125" y2="0.381" layer="21"/>
<rectangle x1="3.25881875" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="4.10718125" y1="0.29641875" x2="4.27481875" y2="0.381" layer="21"/>
<rectangle x1="4.445" y1="0.29641875" x2="4.52881875" y2="0.381" layer="21"/>
<rectangle x1="5.12318125" y1="0.29641875" x2="5.37718125" y2="0.381" layer="21"/>
<rectangle x1="5.63118125" y1="0.29641875" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="7.32281875" y1="0.29641875" x2="7.747" y2="0.381" layer="21"/>
<rectangle x1="8.33881875" y1="0.29641875" x2="8.67918125" y2="0.381" layer="21"/>
<rectangle x1="9.18718125" y1="0.29641875" x2="9.60881875" y2="0.381" layer="21"/>
<rectangle x1="10.45718125" y1="0.29641875" x2="11.303" y2="0.381" layer="21"/>
<rectangle x1="11.72718125" y1="0.29641875" x2="11.89481875" y2="0.381" layer="21"/>
<rectangle x1="12.573" y1="0.29641875" x2="12.74318125" y2="0.381" layer="21"/>
<rectangle x1="13.081" y1="0.29641875" x2="13.50518125" y2="0.381" layer="21"/>
<rectangle x1="1.905" y1="0.381" x2="2.24281875" y2="0.46558125" layer="21"/>
<rectangle x1="2.58318125" y1="0.381" x2="2.921" y2="0.46558125" layer="21"/>
<rectangle x1="3.25881875" y1="0.381" x2="3.429" y2="0.46558125" layer="21"/>
<rectangle x1="4.10718125" y1="0.381" x2="4.27481875" y2="0.46558125" layer="21"/>
<rectangle x1="4.445" y1="0.381" x2="4.61518125" y2="0.46558125" layer="21"/>
<rectangle x1="5.03681875" y1="0.381" x2="5.37718125" y2="0.46558125" layer="21"/>
<rectangle x1="5.63118125" y1="0.381" x2="6.56081875" y2="0.46558125" layer="21"/>
<rectangle x1="7.15518125" y1="0.381" x2="7.83081875" y2="0.46558125" layer="21"/>
<rectangle x1="8.17118125" y1="0.381" x2="8.763" y2="0.46558125" layer="21"/>
<rectangle x1="9.10081875" y1="0.381" x2="9.779" y2="0.46558125" layer="21"/>
<rectangle x1="10.37081875" y1="0.381" x2="10.71118125" y2="0.46558125" layer="21"/>
<rectangle x1="11.049" y1="0.381" x2="11.38681875" y2="0.46558125" layer="21"/>
<rectangle x1="11.72718125" y1="0.381" x2="11.89481875" y2="0.46558125" layer="21"/>
<rectangle x1="12.573" y1="0.381" x2="12.74318125" y2="0.46558125" layer="21"/>
<rectangle x1="12.99718125" y1="0.381" x2="13.67281875" y2="0.46558125" layer="21"/>
<rectangle x1="1.82118125" y1="0.46558125" x2="2.07518125" y2="0.55041875" layer="21"/>
<rectangle x1="2.75081875" y1="0.46558125" x2="3.00481875" y2="0.55041875" layer="21"/>
<rectangle x1="3.25881875" y1="0.46558125" x2="3.429" y2="0.55041875" layer="21"/>
<rectangle x1="4.10718125" y1="0.46558125" x2="4.27481875" y2="0.55041875" layer="21"/>
<rectangle x1="4.445" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="21"/>
<rectangle x1="4.953" y1="0.46558125" x2="5.12318125" y2="0.55041875" layer="21"/>
<rectangle x1="5.63118125" y1="0.46558125" x2="5.79881875" y2="0.55041875" layer="21"/>
<rectangle x1="6.39318125" y1="0.46558125" x2="6.731" y2="0.55041875" layer="21"/>
<rectangle x1="7.06881875" y1="0.46558125" x2="7.32281875" y2="0.55041875" layer="21"/>
<rectangle x1="7.66318125" y1="0.46558125" x2="7.91718125" y2="0.55041875" layer="21"/>
<rectangle x1="8.17118125" y1="0.46558125" x2="8.33881875" y2="0.55041875" layer="21"/>
<rectangle x1="8.59281875" y1="0.46558125" x2="8.84681875" y2="0.55041875" layer="21"/>
<rectangle x1="9.017" y1="0.46558125" x2="9.271" y2="0.55041875" layer="21"/>
<rectangle x1="9.60881875" y1="0.46558125" x2="9.86281875" y2="0.55041875" layer="21"/>
<rectangle x1="10.287" y1="0.46558125" x2="10.541" y2="0.55041875" layer="21"/>
<rectangle x1="11.21918125" y1="0.46558125" x2="11.47318125" y2="0.55041875" layer="21"/>
<rectangle x1="11.72718125" y1="0.46558125" x2="11.89481875" y2="0.55041875" layer="21"/>
<rectangle x1="12.573" y1="0.46558125" x2="12.74318125" y2="0.55041875" layer="21"/>
<rectangle x1="12.91081875" y1="0.46558125" x2="13.16481875" y2="0.55041875" layer="21"/>
<rectangle x1="13.50518125" y1="0.46558125" x2="13.75918125" y2="0.55041875" layer="21"/>
<rectangle x1="1.73481875" y1="0.55041875" x2="1.98881875" y2="0.635" layer="21"/>
<rectangle x1="2.83718125" y1="0.55041875" x2="3.09118125" y2="0.635" layer="21"/>
<rectangle x1="3.25881875" y1="0.55041875" x2="3.429" y2="0.635" layer="21"/>
<rectangle x1="4.10718125" y1="0.55041875" x2="4.27481875" y2="0.635" layer="21"/>
<rectangle x1="4.445" y1="0.55041875" x2="4.61518125" y2="0.635" layer="21"/>
<rectangle x1="4.86918125" y1="0.55041875" x2="5.03681875" y2="0.635" layer="21"/>
<rectangle x1="5.63118125" y1="0.55041875" x2="5.79881875" y2="0.635" layer="21"/>
<rectangle x1="6.56081875" y1="0.55041875" x2="6.81481875" y2="0.635" layer="21"/>
<rectangle x1="7.06881875" y1="0.55041875" x2="7.239" y2="0.635" layer="21"/>
<rectangle x1="7.747" y1="0.55041875" x2="8.001" y2="0.635" layer="21"/>
<rectangle x1="8.17118125" y1="0.55041875" x2="8.255" y2="0.635" layer="21"/>
<rectangle x1="8.67918125" y1="0.55041875" x2="8.84681875" y2="0.635" layer="21"/>
<rectangle x1="8.93318125" y1="0.55041875" x2="9.18718125" y2="0.635" layer="21"/>
<rectangle x1="9.69518125" y1="0.55041875" x2="9.86281875" y2="0.635" layer="21"/>
<rectangle x1="10.20318125" y1="0.55041875" x2="10.45718125" y2="0.635" layer="21"/>
<rectangle x1="11.303" y1="0.55041875" x2="11.557" y2="0.635" layer="21"/>
<rectangle x1="11.72718125" y1="0.55041875" x2="11.89481875" y2="0.635" layer="21"/>
<rectangle x1="12.573" y1="0.55041875" x2="12.74318125" y2="0.635" layer="21"/>
<rectangle x1="12.827" y1="0.55041875" x2="13.081" y2="0.635" layer="21"/>
<rectangle x1="13.589" y1="0.55041875" x2="13.75918125" y2="0.635" layer="21"/>
<rectangle x1="1.73481875" y1="0.635" x2="1.905" y2="0.71958125" layer="21"/>
<rectangle x1="2.921" y1="0.635" x2="3.09118125" y2="0.71958125" layer="21"/>
<rectangle x1="3.25881875" y1="0.635" x2="3.429" y2="0.71958125" layer="21"/>
<rectangle x1="4.10718125" y1="0.635" x2="4.27481875" y2="0.71958125" layer="21"/>
<rectangle x1="4.445" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="4.86918125" y1="0.635" x2="5.03681875" y2="0.71958125" layer="21"/>
<rectangle x1="5.63118125" y1="0.635" x2="5.79881875" y2="0.71958125" layer="21"/>
<rectangle x1="6.64718125" y1="0.635" x2="6.81481875" y2="0.71958125" layer="21"/>
<rectangle x1="6.985" y1="0.635" x2="7.239" y2="0.71958125" layer="21"/>
<rectangle x1="7.83081875" y1="0.635" x2="8.001" y2="0.71958125" layer="21"/>
<rectangle x1="8.59281875" y1="0.635" x2="8.84681875" y2="0.71958125" layer="21"/>
<rectangle x1="8.93318125" y1="0.635" x2="9.10081875" y2="0.71958125" layer="21"/>
<rectangle x1="10.20318125" y1="0.635" x2="10.37081875" y2="0.71958125" layer="21"/>
<rectangle x1="11.38681875" y1="0.635" x2="11.557" y2="0.71958125" layer="21"/>
<rectangle x1="11.72718125" y1="0.635" x2="11.89481875" y2="0.71958125" layer="21"/>
<rectangle x1="12.573" y1="0.635" x2="12.74318125" y2="0.71958125" layer="21"/>
<rectangle x1="12.827" y1="0.635" x2="12.99718125" y2="0.71958125" layer="21"/>
<rectangle x1="1.73481875" y1="0.71958125" x2="1.905" y2="0.80441875" layer="21"/>
<rectangle x1="2.921" y1="0.71958125" x2="3.175" y2="0.80441875" layer="21"/>
<rectangle x1="3.25881875" y1="0.71958125" x2="3.429" y2="0.80441875" layer="21"/>
<rectangle x1="4.10718125" y1="0.71958125" x2="4.27481875" y2="0.80441875" layer="21"/>
<rectangle x1="4.445" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="4.78281875" y1="0.71958125" x2="4.953" y2="0.80441875" layer="21"/>
<rectangle x1="5.63118125" y1="0.71958125" x2="5.79881875" y2="0.80441875" layer="21"/>
<rectangle x1="6.731" y1="0.71958125" x2="6.90118125" y2="0.80441875" layer="21"/>
<rectangle x1="6.985" y1="0.71958125" x2="7.15518125" y2="0.80441875" layer="21"/>
<rectangle x1="7.83081875" y1="0.71958125" x2="8.001" y2="0.80441875" layer="21"/>
<rectangle x1="8.33881875" y1="0.71958125" x2="8.763" y2="0.80441875" layer="21"/>
<rectangle x1="8.93318125" y1="0.71958125" x2="9.94918125" y2="0.80441875" layer="21"/>
<rectangle x1="10.20318125" y1="0.71958125" x2="10.37081875" y2="0.80441875" layer="21"/>
<rectangle x1="11.38681875" y1="0.71958125" x2="11.557" y2="0.80441875" layer="21"/>
<rectangle x1="11.72718125" y1="0.71958125" x2="11.89481875" y2="0.80441875" layer="21"/>
<rectangle x1="12.573" y1="0.71958125" x2="12.74318125" y2="0.80441875" layer="21"/>
<rectangle x1="12.827" y1="0.71958125" x2="13.843" y2="0.80441875" layer="21"/>
<rectangle x1="1.651" y1="0.80441875" x2="1.82118125" y2="0.889" layer="21"/>
<rectangle x1="3.00481875" y1="0.80441875" x2="3.175" y2="0.889" layer="21"/>
<rectangle x1="3.25881875" y1="0.80441875" x2="3.429" y2="0.889" layer="21"/>
<rectangle x1="4.10718125" y1="0.80441875" x2="4.27481875" y2="0.889" layer="21"/>
<rectangle x1="4.445" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="4.78281875" y1="0.80441875" x2="4.953" y2="0.889" layer="21"/>
<rectangle x1="5.63118125" y1="0.80441875" x2="5.79881875" y2="0.889" layer="21"/>
<rectangle x1="6.731" y1="0.80441875" x2="6.90118125" y2="0.889" layer="21"/>
<rectangle x1="6.985" y1="0.80441875" x2="7.15518125" y2="0.889" layer="21"/>
<rectangle x1="7.83081875" y1="0.80441875" x2="8.001" y2="0.889" layer="21"/>
<rectangle x1="8.17118125" y1="0.80441875" x2="8.59281875" y2="0.889" layer="21"/>
<rectangle x1="8.93318125" y1="0.80441875" x2="9.94918125" y2="0.889" layer="21"/>
<rectangle x1="10.11681875" y1="0.80441875" x2="10.287" y2="0.889" layer="21"/>
<rectangle x1="11.47318125" y1="0.80441875" x2="11.64081875" y2="0.889" layer="21"/>
<rectangle x1="11.72718125" y1="0.80441875" x2="11.89481875" y2="0.889" layer="21"/>
<rectangle x1="12.573" y1="0.80441875" x2="12.74318125" y2="0.889" layer="21"/>
<rectangle x1="12.827" y1="0.80441875" x2="13.843" y2="0.889" layer="21"/>
<rectangle x1="1.651" y1="0.889" x2="1.82118125" y2="0.97358125" layer="21"/>
<rectangle x1="3.00481875" y1="0.889" x2="3.175" y2="0.97358125" layer="21"/>
<rectangle x1="3.25881875" y1="0.889" x2="3.51281875" y2="0.97358125" layer="21"/>
<rectangle x1="4.10718125" y1="0.889" x2="4.27481875" y2="0.97358125" layer="21"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.61518125" y2="0.97358125" layer="21"/>
<rectangle x1="4.78281875" y1="0.889" x2="4.953" y2="0.97358125" layer="21"/>
<rectangle x1="5.63118125" y1="0.889" x2="5.79881875" y2="0.97358125" layer="21"/>
<rectangle x1="6.731" y1="0.889" x2="6.90118125" y2="0.97358125" layer="21"/>
<rectangle x1="6.985" y1="0.889" x2="7.239" y2="0.97358125" layer="21"/>
<rectangle x1="7.83081875" y1="0.889" x2="8.001" y2="0.97358125" layer="21"/>
<rectangle x1="8.08481875" y1="0.889" x2="8.42518125" y2="0.97358125" layer="21"/>
<rectangle x1="8.93318125" y1="0.889" x2="9.10081875" y2="0.97358125" layer="21"/>
<rectangle x1="9.779" y1="0.889" x2="9.94918125" y2="0.97358125" layer="21"/>
<rectangle x1="10.11681875" y1="0.889" x2="10.287" y2="0.97358125" layer="21"/>
<rectangle x1="11.47318125" y1="0.889" x2="11.64081875" y2="0.97358125" layer="21"/>
<rectangle x1="11.72718125" y1="0.889" x2="11.89481875" y2="0.97358125" layer="21"/>
<rectangle x1="12.48918125" y1="0.889" x2="12.74318125" y2="0.97358125" layer="21"/>
<rectangle x1="12.827" y1="0.889" x2="12.99718125" y2="0.97358125" layer="21"/>
<rectangle x1="13.589" y1="0.889" x2="13.843" y2="0.97358125" layer="21"/>
<rectangle x1="1.651" y1="0.97358125" x2="1.82118125" y2="1.05841875" layer="21"/>
<rectangle x1="3.00481875" y1="0.97358125" x2="3.175" y2="1.05841875" layer="21"/>
<rectangle x1="3.25881875" y1="0.97358125" x2="3.51281875" y2="1.05841875" layer="21"/>
<rectangle x1="4.02081875" y1="0.97358125" x2="4.27481875" y2="1.05841875" layer="21"/>
<rectangle x1="4.36118125" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="21"/>
<rectangle x1="4.78281875" y1="0.97358125" x2="4.953" y2="1.05841875" layer="21"/>
<rectangle x1="5.63118125" y1="0.97358125" x2="5.79881875" y2="1.05841875" layer="21"/>
<rectangle x1="6.731" y1="0.97358125" x2="6.90118125" y2="1.05841875" layer="21"/>
<rectangle x1="7.06881875" y1="0.97358125" x2="7.239" y2="1.05841875" layer="21"/>
<rectangle x1="7.747" y1="0.97358125" x2="8.001" y2="1.05841875" layer="21"/>
<rectangle x1="8.08481875" y1="0.97358125" x2="8.255" y2="1.05841875" layer="21"/>
<rectangle x1="8.67918125" y1="0.97358125" x2="8.84681875" y2="1.05841875" layer="21"/>
<rectangle x1="8.93318125" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="21"/>
<rectangle x1="9.69518125" y1="0.97358125" x2="9.86281875" y2="1.05841875" layer="21"/>
<rectangle x1="10.11681875" y1="0.97358125" x2="10.287" y2="1.05841875" layer="21"/>
<rectangle x1="11.47318125" y1="0.97358125" x2="11.64081875" y2="1.05841875" layer="21"/>
<rectangle x1="11.72718125" y1="0.97358125" x2="11.98118125" y2="1.05841875" layer="21"/>
<rectangle x1="12.48918125" y1="0.97358125" x2="12.65681875" y2="1.05841875" layer="21"/>
<rectangle x1="12.827" y1="0.97358125" x2="12.99718125" y2="1.05841875" layer="21"/>
<rectangle x1="13.589" y1="0.97358125" x2="13.75918125" y2="1.05841875" layer="21"/>
<rectangle x1="1.651" y1="1.05841875" x2="1.82118125" y2="1.143" layer="21"/>
<rectangle x1="3.00481875" y1="1.05841875" x2="3.175" y2="1.143" layer="21"/>
<rectangle x1="3.25881875" y1="1.05841875" x2="3.59918125" y2="1.143" layer="21"/>
<rectangle x1="3.937" y1="1.05841875" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="4.36118125" y1="1.05841875" x2="4.61518125" y2="1.143" layer="21"/>
<rectangle x1="4.78281875" y1="1.05841875" x2="4.953" y2="1.143" layer="21"/>
<rectangle x1="5.63118125" y1="1.05841875" x2="5.79881875" y2="1.143" layer="21"/>
<rectangle x1="6.731" y1="1.05841875" x2="6.90118125" y2="1.143" layer="21"/>
<rectangle x1="7.06881875" y1="1.05841875" x2="7.32281875" y2="1.143" layer="21"/>
<rectangle x1="7.66318125" y1="1.05841875" x2="7.91718125" y2="1.143" layer="21"/>
<rectangle x1="8.08481875" y1="1.05841875" x2="8.33881875" y2="1.143" layer="21"/>
<rectangle x1="8.59281875" y1="1.05841875" x2="8.84681875" y2="1.143" layer="21"/>
<rectangle x1="9.017" y1="1.05841875" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="9.60881875" y1="1.05841875" x2="9.86281875" y2="1.143" layer="21"/>
<rectangle x1="10.11681875" y1="1.05841875" x2="10.287" y2="1.143" layer="21"/>
<rectangle x1="11.47318125" y1="1.05841875" x2="11.64081875" y2="1.143" layer="21"/>
<rectangle x1="11.72718125" y1="1.05841875" x2="12.065" y2="1.143" layer="21"/>
<rectangle x1="12.40281875" y1="1.05841875" x2="12.65681875" y2="1.143" layer="21"/>
<rectangle x1="12.91081875" y1="1.05841875" x2="13.16481875" y2="1.143" layer="21"/>
<rectangle x1="13.50518125" y1="1.05841875" x2="13.75918125" y2="1.143" layer="21"/>
<rectangle x1="1.651" y1="1.143" x2="1.82118125" y2="1.22758125" layer="21"/>
<rectangle x1="3.00481875" y1="1.143" x2="3.175" y2="1.22758125" layer="21"/>
<rectangle x1="3.25881875" y1="1.143" x2="4.10718125" y2="1.22758125" layer="21"/>
<rectangle x1="4.36118125" y1="1.143" x2="4.61518125" y2="1.22758125" layer="21"/>
<rectangle x1="4.699" y1="1.143" x2="5.37718125" y2="1.22758125" layer="21"/>
<rectangle x1="5.63118125" y1="1.143" x2="5.79881875" y2="1.22758125" layer="21"/>
<rectangle x1="6.731" y1="1.143" x2="6.90118125" y2="1.22758125" layer="21"/>
<rectangle x1="7.15518125" y1="1.143" x2="7.83081875" y2="1.22758125" layer="21"/>
<rectangle x1="8.17118125" y1="1.143" x2="8.763" y2="1.22758125" layer="21"/>
<rectangle x1="9.10081875" y1="1.143" x2="9.779" y2="1.22758125" layer="21"/>
<rectangle x1="10.20318125" y1="1.143" x2="10.37081875" y2="1.22758125" layer="21"/>
<rectangle x1="11.38681875" y1="1.143" x2="11.557" y2="1.22758125" layer="21"/>
<rectangle x1="11.72718125" y1="1.143" x2="12.573" y2="1.22758125" layer="21"/>
<rectangle x1="12.99718125" y1="1.143" x2="13.67281875" y2="1.22758125" layer="21"/>
<rectangle x1="1.651" y1="1.22758125" x2="1.82118125" y2="1.31241875" layer="21"/>
<rectangle x1="3.00481875" y1="1.22758125" x2="3.175" y2="1.31241875" layer="21"/>
<rectangle x1="3.25881875" y1="1.22758125" x2="3.429" y2="1.31241875" layer="21"/>
<rectangle x1="3.59918125" y1="1.22758125" x2="4.02081875" y2="1.31241875" layer="21"/>
<rectangle x1="4.445" y1="1.22758125" x2="4.52881875" y2="1.31241875" layer="21"/>
<rectangle x1="4.699" y1="1.22758125" x2="5.37718125" y2="1.31241875" layer="21"/>
<rectangle x1="5.63118125" y1="1.22758125" x2="5.79881875" y2="1.31241875" layer="21"/>
<rectangle x1="6.731" y1="1.22758125" x2="6.90118125" y2="1.31241875" layer="21"/>
<rectangle x1="7.32281875" y1="1.22758125" x2="7.747" y2="1.31241875" layer="21"/>
<rectangle x1="8.255" y1="1.22758125" x2="8.67918125" y2="1.31241875" layer="21"/>
<rectangle x1="9.18718125" y1="1.22758125" x2="9.60881875" y2="1.31241875" layer="21"/>
<rectangle x1="10.20318125" y1="1.22758125" x2="10.37081875" y2="1.31241875" layer="21"/>
<rectangle x1="11.38681875" y1="1.22758125" x2="11.557" y2="1.31241875" layer="21"/>
<rectangle x1="11.72718125" y1="1.22758125" x2="11.89481875" y2="1.31241875" layer="21"/>
<rectangle x1="11.98118125" y1="1.22758125" x2="12.40281875" y2="1.31241875" layer="21"/>
<rectangle x1="13.081" y1="1.22758125" x2="13.50518125" y2="1.31241875" layer="21"/>
<rectangle x1="1.651" y1="1.31241875" x2="1.82118125" y2="1.397" layer="21"/>
<rectangle x1="3.00481875" y1="1.31241875" x2="3.175" y2="1.397" layer="21"/>
<rectangle x1="4.445" y1="1.31241875" x2="4.52881875" y2="1.397" layer="21"/>
<rectangle x1="4.78281875" y1="1.31241875" x2="4.953" y2="1.397" layer="21"/>
<rectangle x1="5.63118125" y1="1.31241875" x2="5.79881875" y2="1.397" layer="21"/>
<rectangle x1="6.64718125" y1="1.31241875" x2="6.90118125" y2="1.397" layer="21"/>
<rectangle x1="10.20318125" y1="1.31241875" x2="10.45718125" y2="1.397" layer="21"/>
<rectangle x1="11.303" y1="1.31241875" x2="11.557" y2="1.397" layer="21"/>
<rectangle x1="1.651" y1="1.397" x2="1.82118125" y2="1.48158125" layer="21"/>
<rectangle x1="3.00481875" y1="1.397" x2="3.175" y2="1.48158125" layer="21"/>
<rectangle x1="4.36118125" y1="1.397" x2="4.61518125" y2="1.48158125" layer="21"/>
<rectangle x1="4.78281875" y1="1.397" x2="4.953" y2="1.48158125" layer="21"/>
<rectangle x1="5.63118125" y1="1.397" x2="5.79881875" y2="1.48158125" layer="21"/>
<rectangle x1="6.56081875" y1="1.397" x2="6.81481875" y2="1.48158125" layer="21"/>
<rectangle x1="10.287" y1="1.397" x2="10.541" y2="1.48158125" layer="21"/>
<rectangle x1="11.21918125" y1="1.397" x2="11.47318125" y2="1.48158125" layer="21"/>
<rectangle x1="1.651" y1="1.48158125" x2="1.82118125" y2="1.56641875" layer="21"/>
<rectangle x1="3.00481875" y1="1.48158125" x2="3.175" y2="1.56641875" layer="21"/>
<rectangle x1="4.445" y1="1.48158125" x2="4.52881875" y2="1.56641875" layer="21"/>
<rectangle x1="4.78281875" y1="1.48158125" x2="4.953" y2="1.56641875" layer="21"/>
<rectangle x1="5.63118125" y1="1.48158125" x2="5.79881875" y2="1.56641875" layer="21"/>
<rectangle x1="6.477" y1="1.48158125" x2="6.731" y2="1.56641875" layer="21"/>
<rectangle x1="10.37081875" y1="1.48158125" x2="10.71118125" y2="1.56641875" layer="21"/>
<rectangle x1="11.049" y1="1.48158125" x2="11.38681875" y2="1.56641875" layer="21"/>
<rectangle x1="1.651" y1="1.56641875" x2="1.82118125" y2="1.651" layer="21"/>
<rectangle x1="3.00481875" y1="1.56641875" x2="3.175" y2="1.651" layer="21"/>
<rectangle x1="4.78281875" y1="1.56641875" x2="4.953" y2="1.651" layer="21"/>
<rectangle x1="5.63118125" y1="1.56641875" x2="5.79881875" y2="1.651" layer="21"/>
<rectangle x1="5.88518125" y1="1.56641875" x2="6.64718125" y2="1.651" layer="21"/>
<rectangle x1="10.541" y1="1.56641875" x2="11.21918125" y2="1.651" layer="21"/>
<rectangle x1="1.651" y1="1.651" x2="1.82118125" y2="1.73558125" layer="21"/>
<rectangle x1="3.00481875" y1="1.651" x2="3.175" y2="1.73558125" layer="21"/>
<rectangle x1="4.86918125" y1="1.651" x2="4.953" y2="1.73558125" layer="21"/>
<rectangle x1="5.63118125" y1="1.651" x2="6.56081875" y2="1.73558125" layer="21"/>
<rectangle x1="10.71118125" y1="1.651" x2="11.049" y2="1.73558125" layer="21"/>
<rectangle x1="5.715" y1="1.73558125" x2="6.30681875" y2="1.82041875" layer="21"/>
<rectangle x1="2.49681875" y1="2.413" x2="12.573" y2="2.49758125" layer="21"/>
<rectangle x1="2.159" y1="2.49758125" x2="12.91081875" y2="2.58241875" layer="21"/>
<rectangle x1="1.905" y1="2.58241875" x2="13.16481875" y2="2.667" layer="21"/>
<rectangle x1="1.73481875" y1="2.667" x2="13.335" y2="2.75158125" layer="21"/>
<rectangle x1="1.56718125" y1="2.75158125" x2="13.50518125" y2="2.83641875" layer="21"/>
<rectangle x1="1.397" y1="2.83641875" x2="2.667" y2="2.921" layer="21"/>
<rectangle x1="7.32281875" y1="2.83641875" x2="13.589" y2="2.921" layer="21"/>
<rectangle x1="1.31318125" y1="2.921" x2="2.32918125" y2="3.00558125" layer="21"/>
<rectangle x1="7.32281875" y1="2.921" x2="13.75918125" y2="3.00558125" layer="21"/>
<rectangle x1="1.22681875" y1="3.00558125" x2="2.07518125" y2="3.09041875" layer="21"/>
<rectangle x1="7.239" y1="3.00558125" x2="13.843" y2="3.09041875" layer="21"/>
<rectangle x1="1.05918125" y1="3.09041875" x2="1.905" y2="3.175" layer="21"/>
<rectangle x1="7.15518125" y1="3.09041875" x2="13.92681875" y2="3.175" layer="21"/>
<rectangle x1="0.97281875" y1="3.175" x2="1.73481875" y2="3.25958125" layer="21"/>
<rectangle x1="7.15518125" y1="3.175" x2="14.097" y2="3.25958125" layer="21"/>
<rectangle x1="0.889" y1="3.25958125" x2="1.56718125" y2="3.34441875" layer="21"/>
<rectangle x1="7.15518125" y1="3.25958125" x2="14.18081875" y2="3.34441875" layer="21"/>
<rectangle x1="0.80518125" y1="3.34441875" x2="1.48081875" y2="3.429" layer="21"/>
<rectangle x1="7.06881875" y1="3.34441875" x2="14.26718125" y2="3.429" layer="21"/>
<rectangle x1="0.71881875" y1="3.429" x2="1.397" y2="3.51358125" layer="21"/>
<rectangle x1="7.06881875" y1="3.429" x2="14.26718125" y2="3.51358125" layer="21"/>
<rectangle x1="0.71881875" y1="3.51358125" x2="1.31318125" y2="3.59841875" layer="21"/>
<rectangle x1="6.985" y1="3.51358125" x2="14.351" y2="3.59841875" layer="21"/>
<rectangle x1="0.635" y1="3.59841875" x2="1.22681875" y2="3.683" layer="21"/>
<rectangle x1="6.985" y1="3.59841875" x2="14.43481875" y2="3.683" layer="21"/>
<rectangle x1="0.55118125" y1="3.683" x2="1.143" y2="3.76758125" layer="21"/>
<rectangle x1="6.985" y1="3.683" x2="14.52118125" y2="3.76758125" layer="21"/>
<rectangle x1="0.46481875" y1="3.76758125" x2="1.05918125" y2="3.85241875" layer="21"/>
<rectangle x1="6.90118125" y1="3.76758125" x2="14.52118125" y2="3.85241875" layer="21"/>
<rectangle x1="0.46481875" y1="3.85241875" x2="0.97281875" y2="3.937" layer="21"/>
<rectangle x1="6.90118125" y1="3.85241875" x2="14.605" y2="3.937" layer="21"/>
<rectangle x1="0.381" y1="3.937" x2="0.889" y2="4.02158125" layer="21"/>
<rectangle x1="6.90118125" y1="3.937" x2="14.68881875" y2="4.02158125" layer="21"/>
<rectangle x1="0.381" y1="4.02158125" x2="0.889" y2="4.10641875" layer="21"/>
<rectangle x1="6.90118125" y1="4.02158125" x2="14.68881875" y2="4.10641875" layer="21"/>
<rectangle x1="0.29718125" y1="4.10641875" x2="0.80518125" y2="4.191" layer="21"/>
<rectangle x1="6.81481875" y1="4.10641875" x2="14.77518125" y2="4.191" layer="21"/>
<rectangle x1="0.29718125" y1="4.191" x2="0.80518125" y2="4.27558125" layer="21"/>
<rectangle x1="2.58318125" y1="4.191" x2="3.51281875" y2="4.27558125" layer="21"/>
<rectangle x1="4.27481875" y1="4.191" x2="5.461" y2="4.27558125" layer="21"/>
<rectangle x1="6.81481875" y1="4.191" x2="8.17118125" y2="4.27558125" layer="21"/>
<rectangle x1="8.763" y1="4.191" x2="10.033" y2="4.27558125" layer="21"/>
<rectangle x1="10.11681875" y1="4.191" x2="11.38681875" y2="4.27558125" layer="21"/>
<rectangle x1="11.47318125" y1="4.191" x2="12.48918125" y2="4.27558125" layer="21"/>
<rectangle x1="12.65681875" y1="4.191" x2="14.77518125" y2="4.27558125" layer="21"/>
<rectangle x1="0.21081875" y1="4.27558125" x2="0.71881875" y2="4.36041875" layer="21"/>
<rectangle x1="2.413" y1="4.27558125" x2="3.683" y2="4.36041875" layer="21"/>
<rectangle x1="4.27481875" y1="4.27558125" x2="5.63118125" y2="4.36041875" layer="21"/>
<rectangle x1="6.81481875" y1="4.27558125" x2="7.91718125" y2="4.36041875" layer="21"/>
<rectangle x1="9.017" y1="4.27558125" x2="9.94918125" y2="4.36041875" layer="21"/>
<rectangle x1="10.20318125" y1="4.27558125" x2="11.303" y2="4.36041875" layer="21"/>
<rectangle x1="11.557" y1="4.27558125" x2="12.23518125" y2="4.36041875" layer="21"/>
<rectangle x1="12.99718125" y1="4.27558125" x2="14.859" y2="4.36041875" layer="21"/>
<rectangle x1="0.21081875" y1="4.36041875" x2="0.71881875" y2="4.445" layer="21"/>
<rectangle x1="2.32918125" y1="4.36041875" x2="3.76681875" y2="4.445" layer="21"/>
<rectangle x1="4.27481875" y1="4.36041875" x2="5.715" y2="4.445" layer="21"/>
<rectangle x1="6.81481875" y1="4.36041875" x2="7.747" y2="4.445" layer="21"/>
<rectangle x1="9.18718125" y1="4.36041875" x2="9.94918125" y2="4.445" layer="21"/>
<rectangle x1="10.287" y1="4.36041875" x2="11.303" y2="4.445" layer="21"/>
<rectangle x1="11.557" y1="4.36041875" x2="12.065" y2="4.445" layer="21"/>
<rectangle x1="13.16481875" y1="4.36041875" x2="14.859" y2="4.445" layer="21"/>
<rectangle x1="0.21081875" y1="4.445" x2="0.635" y2="4.52958125" layer="21"/>
<rectangle x1="2.159" y1="4.445" x2="3.85318125" y2="4.52958125" layer="21"/>
<rectangle x1="4.27481875" y1="4.445" x2="5.79881875" y2="4.52958125" layer="21"/>
<rectangle x1="6.81481875" y1="4.445" x2="7.66318125" y2="4.52958125" layer="21"/>
<rectangle x1="9.271" y1="4.445" x2="9.94918125" y2="4.52958125" layer="21"/>
<rectangle x1="10.287" y1="4.445" x2="11.303" y2="4.52958125" layer="21"/>
<rectangle x1="11.557" y1="4.445" x2="11.98118125" y2="4.52958125" layer="21"/>
<rectangle x1="13.25118125" y1="4.445" x2="14.859" y2="4.52958125" layer="21"/>
<rectangle x1="0.127" y1="4.52958125" x2="0.635" y2="4.61441875" layer="21"/>
<rectangle x1="2.07518125" y1="4.52958125" x2="3.937" y2="4.61441875" layer="21"/>
<rectangle x1="4.36118125" y1="4.52958125" x2="5.88518125" y2="4.61441875" layer="21"/>
<rectangle x1="6.81481875" y1="4.52958125" x2="7.57681875" y2="4.61441875" layer="21"/>
<rectangle x1="8.33881875" y1="4.52958125" x2="8.59281875" y2="4.61441875" layer="21"/>
<rectangle x1="9.35481875" y1="4.52958125" x2="9.94918125" y2="4.61441875" layer="21"/>
<rectangle x1="10.287" y1="4.52958125" x2="11.303" y2="4.61441875" layer="21"/>
<rectangle x1="11.557" y1="4.52958125" x2="11.89481875" y2="4.61441875" layer="21"/>
<rectangle x1="12.40281875" y1="4.52958125" x2="12.91081875" y2="4.61441875" layer="21"/>
<rectangle x1="13.335" y1="4.52958125" x2="14.94281875" y2="4.61441875" layer="21"/>
<rectangle x1="0.127" y1="4.61441875" x2="0.635" y2="4.699" layer="21"/>
<rectangle x1="2.07518125" y1="4.61441875" x2="2.75081875" y2="4.699" layer="21"/>
<rectangle x1="3.34518125" y1="4.61441875" x2="4.02081875" y2="4.699" layer="21"/>
<rectangle x1="5.29081875" y1="4.61441875" x2="5.969" y2="4.699" layer="21"/>
<rectangle x1="6.81481875" y1="4.61441875" x2="7.493" y2="4.699" layer="21"/>
<rectangle x1="8.08481875" y1="4.61441875" x2="8.84681875" y2="4.699" layer="21"/>
<rectangle x1="9.44118125" y1="4.61441875" x2="9.94918125" y2="4.699" layer="21"/>
<rectangle x1="10.287" y1="4.61441875" x2="11.303" y2="4.699" layer="21"/>
<rectangle x1="11.557" y1="4.61441875" x2="11.89481875" y2="4.699" layer="21"/>
<rectangle x1="12.23518125" y1="4.61441875" x2="12.99718125" y2="4.699" layer="21"/>
<rectangle x1="13.335" y1="4.61441875" x2="14.94281875" y2="4.699" layer="21"/>
<rectangle x1="0.127" y1="4.699" x2="0.55118125" y2="4.78358125" layer="21"/>
<rectangle x1="1.98881875" y1="4.699" x2="2.58318125" y2="4.78358125" layer="21"/>
<rectangle x1="3.51281875" y1="4.699" x2="4.10718125" y2="4.78358125" layer="21"/>
<rectangle x1="5.461" y1="4.699" x2="6.05281875" y2="4.78358125" layer="21"/>
<rectangle x1="6.731" y1="4.699" x2="7.40918125" y2="4.78358125" layer="21"/>
<rectangle x1="7.91718125" y1="4.699" x2="9.017" y2="4.78358125" layer="21"/>
<rectangle x1="9.44118125" y1="4.699" x2="9.94918125" y2="4.78358125" layer="21"/>
<rectangle x1="10.287" y1="4.699" x2="11.303" y2="4.78358125" layer="21"/>
<rectangle x1="11.557" y1="4.699" x2="11.811" y2="4.78358125" layer="21"/>
<rectangle x1="12.14881875" y1="4.699" x2="13.081" y2="4.78358125" layer="21"/>
<rectangle x1="13.25118125" y1="4.699" x2="14.94281875" y2="4.78358125" layer="21"/>
<rectangle x1="0.04318125" y1="4.78358125" x2="0.55118125" y2="4.86841875" layer="21"/>
<rectangle x1="1.905" y1="4.78358125" x2="2.49681875" y2="4.86841875" layer="21"/>
<rectangle x1="3.59918125" y1="4.78358125" x2="4.191" y2="4.86841875" layer="21"/>
<rectangle x1="5.54481875" y1="4.78358125" x2="6.13918125" y2="4.86841875" layer="21"/>
<rectangle x1="6.731" y1="4.78358125" x2="7.40918125" y2="4.86841875" layer="21"/>
<rectangle x1="7.83081875" y1="4.78358125" x2="9.10081875" y2="4.86841875" layer="21"/>
<rectangle x1="9.525" y1="4.78358125" x2="9.94918125" y2="4.86841875" layer="21"/>
<rectangle x1="10.287" y1="4.78358125" x2="11.303" y2="4.86841875" layer="21"/>
<rectangle x1="11.557" y1="4.78358125" x2="11.811" y2="4.86841875" layer="21"/>
<rectangle x1="12.14881875" y1="4.78358125" x2="14.94281875" y2="4.86841875" layer="21"/>
<rectangle x1="0.04318125" y1="4.86841875" x2="0.55118125" y2="4.953" layer="21"/>
<rectangle x1="1.905" y1="4.86841875" x2="2.413" y2="4.953" layer="21"/>
<rectangle x1="3.683" y1="4.86841875" x2="4.191" y2="4.953" layer="21"/>
<rectangle x1="5.63118125" y1="4.86841875" x2="6.13918125" y2="4.953" layer="21"/>
<rectangle x1="6.731" y1="4.86841875" x2="7.32281875" y2="4.953" layer="21"/>
<rectangle x1="7.747" y1="4.86841875" x2="9.10081875" y2="4.953" layer="21"/>
<rectangle x1="9.60881875" y1="4.86841875" x2="9.94918125" y2="4.953" layer="21"/>
<rectangle x1="10.287" y1="4.86841875" x2="11.303" y2="4.953" layer="21"/>
<rectangle x1="11.557" y1="4.86841875" x2="11.811" y2="4.953" layer="21"/>
<rectangle x1="13.335" y1="4.86841875" x2="14.94281875" y2="4.953" layer="21"/>
<rectangle x1="0.04318125" y1="4.953" x2="0.55118125" y2="5.03758125" layer="21"/>
<rectangle x1="1.82118125" y1="4.953" x2="2.32918125" y2="5.03758125" layer="21"/>
<rectangle x1="3.76681875" y1="4.953" x2="4.191" y2="5.03758125" layer="21"/>
<rectangle x1="5.715" y1="4.953" x2="6.13918125" y2="5.03758125" layer="21"/>
<rectangle x1="6.731" y1="4.953" x2="7.32281875" y2="5.03758125" layer="21"/>
<rectangle x1="7.747" y1="4.953" x2="9.18718125" y2="5.03758125" layer="21"/>
<rectangle x1="9.60881875" y1="4.953" x2="9.94918125" y2="5.03758125" layer="21"/>
<rectangle x1="10.287" y1="4.953" x2="11.303" y2="5.03758125" layer="21"/>
<rectangle x1="11.557" y1="4.953" x2="11.811" y2="5.03758125" layer="21"/>
<rectangle x1="13.41881875" y1="4.953" x2="14.94281875" y2="5.03758125" layer="21"/>
<rectangle x1="0.04318125" y1="5.03758125" x2="0.46481875" y2="5.12241875" layer="21"/>
<rectangle x1="1.82118125" y1="5.03758125" x2="2.32918125" y2="5.12241875" layer="21"/>
<rectangle x1="3.76681875" y1="5.03758125" x2="4.27481875" y2="5.12241875" layer="21"/>
<rectangle x1="5.715" y1="5.03758125" x2="6.223" y2="5.12241875" layer="21"/>
<rectangle x1="6.731" y1="5.03758125" x2="7.32281875" y2="5.12241875" layer="21"/>
<rectangle x1="7.66318125" y1="5.03758125" x2="9.271" y2="5.12241875" layer="21"/>
<rectangle x1="9.60881875" y1="5.03758125" x2="9.94918125" y2="5.12241875" layer="21"/>
<rectangle x1="10.287" y1="5.03758125" x2="11.303" y2="5.12241875" layer="21"/>
<rectangle x1="11.557" y1="5.03758125" x2="11.811" y2="5.12241875" layer="21"/>
<rectangle x1="13.41881875" y1="5.03758125" x2="14.94281875" y2="5.12241875" layer="21"/>
<rectangle x1="0.04318125" y1="5.12241875" x2="0.46481875" y2="5.207" layer="21"/>
<rectangle x1="1.82118125" y1="5.12241875" x2="2.24281875" y2="5.207" layer="21"/>
<rectangle x1="3.76681875" y1="5.12241875" x2="4.27481875" y2="5.207" layer="21"/>
<rectangle x1="5.79881875" y1="5.12241875" x2="6.223" y2="5.207" layer="21"/>
<rectangle x1="6.731" y1="5.12241875" x2="7.239" y2="5.207" layer="21"/>
<rectangle x1="7.66318125" y1="5.12241875" x2="9.271" y2="5.207" layer="21"/>
<rectangle x1="9.69518125" y1="5.12241875" x2="9.94918125" y2="5.207" layer="21"/>
<rectangle x1="10.287" y1="5.12241875" x2="11.303" y2="5.207" layer="21"/>
<rectangle x1="11.557" y1="5.12241875" x2="11.811" y2="5.207" layer="21"/>
<rectangle x1="13.41881875" y1="5.12241875" x2="14.94281875" y2="5.207" layer="21"/>
<rectangle x1="0.04318125" y1="5.207" x2="0.46481875" y2="5.29158125" layer="21"/>
<rectangle x1="1.82118125" y1="5.207" x2="2.24281875" y2="5.29158125" layer="21"/>
<rectangle x1="3.85318125" y1="5.207" x2="4.27481875" y2="5.29158125" layer="21"/>
<rectangle x1="5.79881875" y1="5.207" x2="6.223" y2="5.29158125" layer="21"/>
<rectangle x1="6.731" y1="5.207" x2="7.239" y2="5.29158125" layer="21"/>
<rectangle x1="7.66318125" y1="5.207" x2="9.271" y2="5.29158125" layer="21"/>
<rectangle x1="9.69518125" y1="5.207" x2="9.94918125" y2="5.29158125" layer="21"/>
<rectangle x1="10.287" y1="5.207" x2="11.21918125" y2="5.29158125" layer="21"/>
<rectangle x1="11.557" y1="5.207" x2="11.811" y2="5.29158125" layer="21"/>
<rectangle x1="12.14881875" y1="5.207" x2="13.081" y2="5.29158125" layer="21"/>
<rectangle x1="13.335" y1="5.207" x2="14.94281875" y2="5.29158125" layer="21"/>
<rectangle x1="0.04318125" y1="5.29158125" x2="0.46481875" y2="5.37641875" layer="21"/>
<rectangle x1="1.82118125" y1="5.29158125" x2="2.24281875" y2="5.37641875" layer="21"/>
<rectangle x1="3.85318125" y1="5.29158125" x2="4.27481875" y2="5.37641875" layer="21"/>
<rectangle x1="5.79881875" y1="5.29158125" x2="6.223" y2="5.37641875" layer="21"/>
<rectangle x1="6.731" y1="5.29158125" x2="7.239" y2="5.37641875" layer="21"/>
<rectangle x1="7.57681875" y1="5.29158125" x2="9.271" y2="5.37641875" layer="21"/>
<rectangle x1="9.69518125" y1="5.29158125" x2="9.94918125" y2="5.37641875" layer="21"/>
<rectangle x1="10.37081875" y1="5.29158125" x2="11.21918125" y2="5.37641875" layer="21"/>
<rectangle x1="11.557" y1="5.29158125" x2="11.811" y2="5.37641875" layer="21"/>
<rectangle x1="12.14881875" y1="5.29158125" x2="12.99718125" y2="5.37641875" layer="21"/>
<rectangle x1="13.335" y1="5.29158125" x2="14.94281875" y2="5.37641875" layer="21"/>
<rectangle x1="0.04318125" y1="5.37641875" x2="0.46481875" y2="5.461" layer="21"/>
<rectangle x1="1.82118125" y1="5.37641875" x2="2.24281875" y2="5.461" layer="21"/>
<rectangle x1="3.85318125" y1="5.37641875" x2="4.27481875" y2="5.461" layer="21"/>
<rectangle x1="5.79881875" y1="5.37641875" x2="6.223" y2="5.461" layer="21"/>
<rectangle x1="6.731" y1="5.37641875" x2="7.239" y2="5.461" layer="21"/>
<rectangle x1="7.57681875" y1="5.37641875" x2="9.271" y2="5.461" layer="21"/>
<rectangle x1="9.69518125" y1="5.37641875" x2="9.94918125" y2="5.461" layer="21"/>
<rectangle x1="10.45718125" y1="5.37641875" x2="11.13281875" y2="5.461" layer="21"/>
<rectangle x1="11.47318125" y1="5.37641875" x2="11.89481875" y2="5.461" layer="21"/>
<rectangle x1="12.23518125" y1="5.37641875" x2="12.91081875" y2="5.461" layer="21"/>
<rectangle x1="13.335" y1="5.37641875" x2="14.94281875" y2="5.461" layer="21"/>
<rectangle x1="0.04318125" y1="5.461" x2="0.46481875" y2="5.54558125" layer="21"/>
<rectangle x1="1.82118125" y1="5.461" x2="2.24281875" y2="5.54558125" layer="21"/>
<rectangle x1="3.85318125" y1="5.461" x2="4.27481875" y2="5.54558125" layer="21"/>
<rectangle x1="5.79881875" y1="5.461" x2="6.223" y2="5.54558125" layer="21"/>
<rectangle x1="6.731" y1="5.461" x2="7.239" y2="5.54558125" layer="21"/>
<rectangle x1="7.57681875" y1="5.461" x2="9.271" y2="5.54558125" layer="21"/>
<rectangle x1="9.69518125" y1="5.461" x2="9.94918125" y2="5.54558125" layer="21"/>
<rectangle x1="10.541" y1="5.461" x2="10.96518125" y2="5.54558125" layer="21"/>
<rectangle x1="11.38681875" y1="5.461" x2="11.98118125" y2="5.54558125" layer="21"/>
<rectangle x1="12.40281875" y1="5.461" x2="12.827" y2="5.54558125" layer="21"/>
<rectangle x1="13.25118125" y1="5.461" x2="14.94281875" y2="5.54558125" layer="21"/>
<rectangle x1="0.04318125" y1="5.54558125" x2="0.46481875" y2="5.63041875" layer="21"/>
<rectangle x1="1.82118125" y1="5.54558125" x2="2.24281875" y2="5.63041875" layer="21"/>
<rectangle x1="3.85318125" y1="5.54558125" x2="4.27481875" y2="5.63041875" layer="21"/>
<rectangle x1="5.79881875" y1="5.54558125" x2="6.223" y2="5.63041875" layer="21"/>
<rectangle x1="6.731" y1="5.54558125" x2="7.239" y2="5.63041875" layer="21"/>
<rectangle x1="7.66318125" y1="5.54558125" x2="9.271" y2="5.63041875" layer="21"/>
<rectangle x1="9.69518125" y1="5.54558125" x2="9.94918125" y2="5.63041875" layer="21"/>
<rectangle x1="11.38681875" y1="5.54558125" x2="11.98118125" y2="5.63041875" layer="21"/>
<rectangle x1="13.16481875" y1="5.54558125" x2="14.94281875" y2="5.63041875" layer="21"/>
<rectangle x1="0.04318125" y1="5.63041875" x2="0.46481875" y2="5.715" layer="21"/>
<rectangle x1="1.82118125" y1="5.63041875" x2="2.24281875" y2="5.715" layer="21"/>
<rectangle x1="3.85318125" y1="5.63041875" x2="4.27481875" y2="5.715" layer="21"/>
<rectangle x1="5.715" y1="5.63041875" x2="6.223" y2="5.715" layer="21"/>
<rectangle x1="6.731" y1="5.63041875" x2="7.239" y2="5.715" layer="21"/>
<rectangle x1="7.66318125" y1="5.63041875" x2="9.271" y2="5.715" layer="21"/>
<rectangle x1="9.60881875" y1="5.63041875" x2="9.94918125" y2="5.715" layer="21"/>
<rectangle x1="11.21918125" y1="5.63041875" x2="12.14881875" y2="5.715" layer="21"/>
<rectangle x1="13.081" y1="5.63041875" x2="14.94281875" y2="5.715" layer="21"/>
<rectangle x1="0.04318125" y1="5.715" x2="0.55118125" y2="5.79958125" layer="21"/>
<rectangle x1="1.82118125" y1="5.715" x2="2.24281875" y2="5.79958125" layer="21"/>
<rectangle x1="3.85318125" y1="5.715" x2="4.27481875" y2="5.79958125" layer="21"/>
<rectangle x1="5.715" y1="5.715" x2="6.223" y2="5.79958125" layer="21"/>
<rectangle x1="6.731" y1="5.715" x2="7.32281875" y2="5.79958125" layer="21"/>
<rectangle x1="7.66318125" y1="5.715" x2="9.271" y2="5.79958125" layer="21"/>
<rectangle x1="9.60881875" y1="5.715" x2="9.94918125" y2="5.79958125" layer="21"/>
<rectangle x1="10.20318125" y1="5.715" x2="10.45718125" y2="5.79958125" layer="21"/>
<rectangle x1="11.13281875" y1="5.715" x2="12.23518125" y2="5.79958125" layer="21"/>
<rectangle x1="12.91081875" y1="5.715" x2="14.94281875" y2="5.79958125" layer="21"/>
<rectangle x1="0.04318125" y1="5.79958125" x2="0.55118125" y2="5.88441875" layer="21"/>
<rectangle x1="1.82118125" y1="5.79958125" x2="2.24281875" y2="5.88441875" layer="21"/>
<rectangle x1="3.85318125" y1="5.79958125" x2="4.27481875" y2="5.88441875" layer="21"/>
<rectangle x1="5.63118125" y1="5.79958125" x2="6.13918125" y2="5.88441875" layer="21"/>
<rectangle x1="6.731" y1="5.79958125" x2="7.32281875" y2="5.88441875" layer="21"/>
<rectangle x1="7.747" y1="5.79958125" x2="9.18718125" y2="5.88441875" layer="21"/>
<rectangle x1="9.60881875" y1="5.79958125" x2="14.94281875" y2="5.88441875" layer="21"/>
<rectangle x1="0.04318125" y1="5.88441875" x2="0.55118125" y2="5.969" layer="21"/>
<rectangle x1="1.82118125" y1="5.88441875" x2="2.24281875" y2="5.969" layer="21"/>
<rectangle x1="3.85318125" y1="5.88441875" x2="4.27481875" y2="5.969" layer="21"/>
<rectangle x1="5.63118125" y1="5.88441875" x2="6.13918125" y2="5.969" layer="21"/>
<rectangle x1="6.731" y1="5.88441875" x2="7.40918125" y2="5.969" layer="21"/>
<rectangle x1="7.83081875" y1="5.88441875" x2="9.10081875" y2="5.969" layer="21"/>
<rectangle x1="9.525" y1="5.88441875" x2="14.94281875" y2="5.969" layer="21"/>
<rectangle x1="0.127" y1="5.969" x2="0.55118125" y2="6.05358125" layer="21"/>
<rectangle x1="1.82118125" y1="5.969" x2="2.24281875" y2="6.05358125" layer="21"/>
<rectangle x1="3.85318125" y1="5.969" x2="4.27481875" y2="6.05358125" layer="21"/>
<rectangle x1="5.54481875" y1="5.969" x2="6.05281875" y2="6.05358125" layer="21"/>
<rectangle x1="6.731" y1="5.969" x2="7.40918125" y2="6.05358125" layer="21"/>
<rectangle x1="7.91718125" y1="5.969" x2="9.017" y2="6.05358125" layer="21"/>
<rectangle x1="9.525" y1="5.969" x2="14.94281875" y2="6.05358125" layer="21"/>
<rectangle x1="0.127" y1="6.05358125" x2="0.55118125" y2="6.13841875" layer="21"/>
<rectangle x1="1.82118125" y1="6.05358125" x2="2.24281875" y2="6.13841875" layer="21"/>
<rectangle x1="3.85318125" y1="6.05358125" x2="4.27481875" y2="6.13841875" layer="21"/>
<rectangle x1="5.37718125" y1="6.05358125" x2="6.05281875" y2="6.13841875" layer="21"/>
<rectangle x1="6.731" y1="6.05358125" x2="7.493" y2="6.13841875" layer="21"/>
<rectangle x1="8.001" y1="6.05358125" x2="8.93318125" y2="6.13841875" layer="21"/>
<rectangle x1="9.44118125" y1="6.05358125" x2="14.94281875" y2="6.13841875" layer="21"/>
<rectangle x1="0.127" y1="6.13841875" x2="0.635" y2="6.223" layer="21"/>
<rectangle x1="1.82118125" y1="6.13841875" x2="2.24281875" y2="6.223" layer="21"/>
<rectangle x1="3.85318125" y1="6.13841875" x2="4.27481875" y2="6.223" layer="21"/>
<rectangle x1="5.207" y1="6.13841875" x2="5.969" y2="6.223" layer="21"/>
<rectangle x1="6.81481875" y1="6.13841875" x2="7.493" y2="6.223" layer="21"/>
<rectangle x1="8.17118125" y1="6.13841875" x2="8.763" y2="6.223" layer="21"/>
<rectangle x1="9.35481875" y1="6.13841875" x2="14.94281875" y2="6.223" layer="21"/>
<rectangle x1="0.127" y1="6.223" x2="0.635" y2="6.30758125" layer="21"/>
<rectangle x1="1.82118125" y1="6.223" x2="2.24281875" y2="6.30758125" layer="21"/>
<rectangle x1="3.85318125" y1="6.223" x2="5.88518125" y2="6.30758125" layer="21"/>
<rectangle x1="6.81481875" y1="6.223" x2="7.57681875" y2="6.30758125" layer="21"/>
<rectangle x1="9.35481875" y1="6.223" x2="14.859" y2="6.30758125" layer="21"/>
<rectangle x1="0.21081875" y1="6.30758125" x2="0.635" y2="6.39241875" layer="21"/>
<rectangle x1="1.82118125" y1="6.30758125" x2="2.24281875" y2="6.39241875" layer="21"/>
<rectangle x1="3.85318125" y1="6.30758125" x2="5.79881875" y2="6.39241875" layer="21"/>
<rectangle x1="6.81481875" y1="6.30758125" x2="7.66318125" y2="6.39241875" layer="21"/>
<rectangle x1="9.18718125" y1="6.30758125" x2="14.859" y2="6.39241875" layer="21"/>
<rectangle x1="0.21081875" y1="6.39241875" x2="0.71881875" y2="6.477" layer="21"/>
<rectangle x1="1.82118125" y1="6.39241875" x2="2.24281875" y2="6.477" layer="21"/>
<rectangle x1="3.85318125" y1="6.39241875" x2="5.715" y2="6.477" layer="21"/>
<rectangle x1="6.81481875" y1="6.39241875" x2="7.83081875" y2="6.477" layer="21"/>
<rectangle x1="9.10081875" y1="6.39241875" x2="14.859" y2="6.477" layer="21"/>
<rectangle x1="0.21081875" y1="6.477" x2="0.71881875" y2="6.56158125" layer="21"/>
<rectangle x1="1.82118125" y1="6.477" x2="2.24281875" y2="6.56158125" layer="21"/>
<rectangle x1="3.85318125" y1="6.477" x2="5.54481875" y2="6.56158125" layer="21"/>
<rectangle x1="6.81481875" y1="6.477" x2="8.001" y2="6.56158125" layer="21"/>
<rectangle x1="8.93318125" y1="6.477" x2="14.77518125" y2="6.56158125" layer="21"/>
<rectangle x1="0.29718125" y1="6.56158125" x2="0.80518125" y2="6.64641875" layer="21"/>
<rectangle x1="1.905" y1="6.56158125" x2="2.159" y2="6.64641875" layer="21"/>
<rectangle x1="3.937" y1="6.56158125" x2="5.37718125" y2="6.64641875" layer="21"/>
<rectangle x1="6.81481875" y1="6.56158125" x2="8.33881875" y2="6.64641875" layer="21"/>
<rectangle x1="8.59281875" y1="6.56158125" x2="14.77518125" y2="6.64641875" layer="21"/>
<rectangle x1="0.29718125" y1="6.64641875" x2="0.80518125" y2="6.731" layer="21"/>
<rectangle x1="6.90118125" y1="6.64641875" x2="14.77518125" y2="6.731" layer="21"/>
<rectangle x1="0.381" y1="6.731" x2="0.889" y2="6.81558125" layer="21"/>
<rectangle x1="6.90118125" y1="6.731" x2="14.68881875" y2="6.81558125" layer="21"/>
<rectangle x1="0.381" y1="6.81558125" x2="0.97281875" y2="6.90041875" layer="21"/>
<rectangle x1="6.90118125" y1="6.81558125" x2="14.605" y2="6.90041875" layer="21"/>
<rectangle x1="0.46481875" y1="6.90041875" x2="0.97281875" y2="6.985" layer="21"/>
<rectangle x1="6.90118125" y1="6.90041875" x2="14.605" y2="6.985" layer="21"/>
<rectangle x1="0.55118125" y1="6.985" x2="1.05918125" y2="7.06958125" layer="21"/>
<rectangle x1="6.985" y1="6.985" x2="14.52118125" y2="7.06958125" layer="21"/>
<rectangle x1="0.55118125" y1="7.06958125" x2="1.143" y2="7.15441875" layer="21"/>
<rectangle x1="6.985" y1="7.06958125" x2="14.52118125" y2="7.15441875" layer="21"/>
<rectangle x1="0.635" y1="7.15441875" x2="1.22681875" y2="7.239" layer="21"/>
<rectangle x1="6.985" y1="7.15441875" x2="14.43481875" y2="7.239" layer="21"/>
<rectangle x1="0.71881875" y1="7.239" x2="1.31318125" y2="7.32358125" layer="21"/>
<rectangle x1="6.985" y1="7.239" x2="14.351" y2="7.32358125" layer="21"/>
<rectangle x1="0.80518125" y1="7.32358125" x2="1.397" y2="7.40841875" layer="21"/>
<rectangle x1="7.06881875" y1="7.32358125" x2="14.26718125" y2="7.40841875" layer="21"/>
<rectangle x1="0.889" y1="7.40841875" x2="1.48081875" y2="7.493" layer="21"/>
<rectangle x1="7.06881875" y1="7.40841875" x2="14.18081875" y2="7.493" layer="21"/>
<rectangle x1="0.889" y1="7.493" x2="1.651" y2="7.57758125" layer="21"/>
<rectangle x1="7.15518125" y1="7.493" x2="14.097" y2="7.57758125" layer="21"/>
<rectangle x1="1.05918125" y1="7.57758125" x2="1.73481875" y2="7.66241875" layer="21"/>
<rectangle x1="7.15518125" y1="7.57758125" x2="14.01318125" y2="7.66241875" layer="21"/>
<rectangle x1="1.143" y1="7.66241875" x2="1.905" y2="7.747" layer="21"/>
<rectangle x1="7.239" y1="7.66241875" x2="13.92681875" y2="7.747" layer="21"/>
<rectangle x1="1.22681875" y1="7.747" x2="2.159" y2="7.83158125" layer="21"/>
<rectangle x1="7.239" y1="7.747" x2="13.843" y2="7.83158125" layer="21"/>
<rectangle x1="1.31318125" y1="7.83158125" x2="2.413" y2="7.91641875" layer="21"/>
<rectangle x1="7.32281875" y1="7.83158125" x2="13.67281875" y2="7.91641875" layer="21"/>
<rectangle x1="1.48081875" y1="7.91641875" x2="7.239" y2="8.001" layer="21"/>
<rectangle x1="7.32281875" y1="7.91641875" x2="13.589" y2="8.001" layer="21"/>
<rectangle x1="1.651" y1="8.001" x2="13.41881875" y2="8.08558125" layer="21"/>
<rectangle x1="1.82118125" y1="8.08558125" x2="13.25118125" y2="8.17041875" layer="21"/>
<rectangle x1="1.98881875" y1="8.17041875" x2="13.081" y2="8.255" layer="21"/>
<rectangle x1="2.24281875" y1="8.255" x2="12.827" y2="8.33958125" layer="21"/>
</package>
<package name="UDO-LOGO-20MM">
<rectangle x1="3.00481875" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="14.351" y1="0.29641875" x2="14.68881875" y2="0.381" layer="21"/>
<rectangle x1="2.75081875" y1="0.381" x2="3.683" y2="0.46558125" layer="21"/>
<rectangle x1="4.445" y1="0.381" x2="4.52881875" y2="0.46558125" layer="21"/>
<rectangle x1="5.54481875" y1="0.381" x2="5.715" y2="0.46558125" layer="21"/>
<rectangle x1="5.969" y1="0.381" x2="6.05281875" y2="0.46558125" layer="21"/>
<rectangle x1="6.985" y1="0.381" x2="7.15518125" y2="0.46558125" layer="21"/>
<rectangle x1="7.57681875" y1="0.381" x2="8.42518125" y2="0.46558125" layer="21"/>
<rectangle x1="9.94918125" y1="0.381" x2="10.20318125" y2="0.46558125" layer="21"/>
<rectangle x1="11.21918125" y1="0.381" x2="11.38681875" y2="0.46558125" layer="21"/>
<rectangle x1="12.40281875" y1="0.381" x2="12.74318125" y2="0.46558125" layer="21"/>
<rectangle x1="14.097" y1="0.381" x2="14.94281875" y2="0.46558125" layer="21"/>
<rectangle x1="15.70481875" y1="0.381" x2="15.79118125" y2="0.46558125" layer="21"/>
<rectangle x1="16.80718125" y1="0.381" x2="16.891" y2="0.46558125" layer="21"/>
<rectangle x1="17.653" y1="0.381" x2="17.907" y2="0.46558125" layer="21"/>
<rectangle x1="2.667" y1="0.46558125" x2="3.85318125" y2="0.55041875" layer="21"/>
<rectangle x1="4.36118125" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="21"/>
<rectangle x1="5.54481875" y1="0.46558125" x2="5.715" y2="0.55041875" layer="21"/>
<rectangle x1="5.88518125" y1="0.46558125" x2="6.13918125" y2="0.55041875" layer="21"/>
<rectangle x1="6.731" y1="0.46558125" x2="7.239" y2="0.55041875" layer="21"/>
<rectangle x1="7.493" y1="0.46558125" x2="8.67918125" y2="0.55041875" layer="21"/>
<rectangle x1="9.69518125" y1="0.46558125" x2="10.37081875" y2="0.55041875" layer="21"/>
<rectangle x1="11.049" y1="0.46558125" x2="11.64081875" y2="0.55041875" layer="21"/>
<rectangle x1="12.23518125" y1="0.46558125" x2="12.91081875" y2="0.55041875" layer="21"/>
<rectangle x1="13.92681875" y1="0.46558125" x2="15.113" y2="0.55041875" layer="21"/>
<rectangle x1="15.621" y1="0.46558125" x2="15.875" y2="0.55041875" layer="21"/>
<rectangle x1="16.72081875" y1="0.46558125" x2="16.97481875" y2="0.55041875" layer="21"/>
<rectangle x1="17.399" y1="0.46558125" x2="18.07718125" y2="0.55041875" layer="21"/>
<rectangle x1="2.58318125" y1="0.55041875" x2="3.00481875" y2="0.635" layer="21"/>
<rectangle x1="3.51281875" y1="0.55041875" x2="3.937" y2="0.635" layer="21"/>
<rectangle x1="4.36118125" y1="0.55041875" x2="4.61518125" y2="0.635" layer="21"/>
<rectangle x1="5.54481875" y1="0.55041875" x2="5.715" y2="0.635" layer="21"/>
<rectangle x1="5.88518125" y1="0.55041875" x2="6.13918125" y2="0.635" layer="21"/>
<rectangle x1="6.64718125" y1="0.55041875" x2="7.239" y2="0.635" layer="21"/>
<rectangle x1="7.493" y1="0.55041875" x2="8.84681875" y2="0.635" layer="21"/>
<rectangle x1="9.60881875" y1="0.55041875" x2="10.45718125" y2="0.635" layer="21"/>
<rectangle x1="10.96518125" y1="0.55041875" x2="11.72718125" y2="0.635" layer="21"/>
<rectangle x1="12.14881875" y1="0.55041875" x2="12.99718125" y2="0.635" layer="21"/>
<rectangle x1="13.843" y1="0.55041875" x2="14.26718125" y2="0.635" layer="21"/>
<rectangle x1="14.77518125" y1="0.55041875" x2="15.19681875" y2="0.635" layer="21"/>
<rectangle x1="15.621" y1="0.55041875" x2="15.875" y2="0.635" layer="21"/>
<rectangle x1="16.72081875" y1="0.55041875" x2="16.97481875" y2="0.635" layer="21"/>
<rectangle x1="17.31518125" y1="0.55041875" x2="18.24481875" y2="0.635" layer="21"/>
<rectangle x1="2.49681875" y1="0.635" x2="2.83718125" y2="0.71958125" layer="21"/>
<rectangle x1="3.683" y1="0.635" x2="4.02081875" y2="0.71958125" layer="21"/>
<rectangle x1="4.36118125" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="5.54481875" y1="0.635" x2="5.715" y2="0.71958125" layer="21"/>
<rectangle x1="5.88518125" y1="0.635" x2="6.13918125" y2="0.71958125" layer="21"/>
<rectangle x1="6.56081875" y1="0.635" x2="6.90118125" y2="0.71958125" layer="21"/>
<rectangle x1="7.493" y1="0.635" x2="7.747" y2="0.71958125" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="8.93318125" y2="0.71958125" layer="21"/>
<rectangle x1="9.525" y1="0.635" x2="9.86281875" y2="0.71958125" layer="21"/>
<rectangle x1="10.20318125" y1="0.635" x2="10.541" y2="0.71958125" layer="21"/>
<rectangle x1="10.87881875" y1="0.635" x2="11.13281875" y2="0.71958125" layer="21"/>
<rectangle x1="11.47318125" y1="0.635" x2="11.811" y2="0.71958125" layer="21"/>
<rectangle x1="12.065" y1="0.635" x2="12.40281875" y2="0.71958125" layer="21"/>
<rectangle x1="12.74318125" y1="0.635" x2="13.081" y2="0.71958125" layer="21"/>
<rectangle x1="13.75918125" y1="0.635" x2="14.097" y2="0.71958125" layer="21"/>
<rectangle x1="14.94281875" y1="0.635" x2="15.28318125" y2="0.71958125" layer="21"/>
<rectangle x1="15.621" y1="0.635" x2="15.875" y2="0.71958125" layer="21"/>
<rectangle x1="16.72081875" y1="0.635" x2="16.97481875" y2="0.71958125" layer="21"/>
<rectangle x1="17.22881875" y1="0.635" x2="17.56918125" y2="0.71958125" layer="21"/>
<rectangle x1="17.907" y1="0.635" x2="18.33118125" y2="0.71958125" layer="21"/>
<rectangle x1="2.413" y1="0.71958125" x2="2.667" y2="0.80441875" layer="21"/>
<rectangle x1="3.76681875" y1="0.71958125" x2="4.02081875" y2="0.80441875" layer="21"/>
<rectangle x1="4.36118125" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="5.54481875" y1="0.71958125" x2="5.715" y2="0.80441875" layer="21"/>
<rectangle x1="5.88518125" y1="0.71958125" x2="6.13918125" y2="0.80441875" layer="21"/>
<rectangle x1="6.56081875" y1="0.71958125" x2="6.81481875" y2="0.80441875" layer="21"/>
<rectangle x1="7.493" y1="0.71958125" x2="7.747" y2="0.80441875" layer="21"/>
<rectangle x1="8.67918125" y1="0.71958125" x2="9.017" y2="0.80441875" layer="21"/>
<rectangle x1="9.44118125" y1="0.71958125" x2="9.69518125" y2="0.80441875" layer="21"/>
<rectangle x1="10.37081875" y1="0.71958125" x2="10.62481875" y2="0.80441875" layer="21"/>
<rectangle x1="10.87881875" y1="0.71958125" x2="11.049" y2="0.80441875" layer="21"/>
<rectangle x1="11.557" y1="0.71958125" x2="11.811" y2="0.80441875" layer="21"/>
<rectangle x1="11.98118125" y1="0.71958125" x2="12.23518125" y2="0.80441875" layer="21"/>
<rectangle x1="12.91081875" y1="0.71958125" x2="13.16481875" y2="0.80441875" layer="21"/>
<rectangle x1="13.67281875" y1="0.71958125" x2="14.01318125" y2="0.80441875" layer="21"/>
<rectangle x1="15.02918125" y1="0.71958125" x2="15.367" y2="0.80441875" layer="21"/>
<rectangle x1="15.621" y1="0.71958125" x2="15.875" y2="0.80441875" layer="21"/>
<rectangle x1="16.72081875" y1="0.71958125" x2="16.97481875" y2="0.80441875" layer="21"/>
<rectangle x1="17.145" y1="0.71958125" x2="17.399" y2="0.80441875" layer="21"/>
<rectangle x1="18.07718125" y1="0.71958125" x2="18.33118125" y2="0.80441875" layer="21"/>
<rectangle x1="2.32918125" y1="0.80441875" x2="2.58318125" y2="0.889" layer="21"/>
<rectangle x1="3.85318125" y1="0.80441875" x2="4.10718125" y2="0.889" layer="21"/>
<rectangle x1="4.36118125" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="5.54481875" y1="0.80441875" x2="5.715" y2="0.889" layer="21"/>
<rectangle x1="5.88518125" y1="0.80441875" x2="6.13918125" y2="0.889" layer="21"/>
<rectangle x1="6.477" y1="0.80441875" x2="6.731" y2="0.889" layer="21"/>
<rectangle x1="7.493" y1="0.80441875" x2="7.747" y2="0.889" layer="21"/>
<rectangle x1="8.763" y1="0.80441875" x2="9.10081875" y2="0.889" layer="21"/>
<rectangle x1="9.44118125" y1="0.80441875" x2="9.69518125" y2="0.889" layer="21"/>
<rectangle x1="10.45718125" y1="0.80441875" x2="10.71118125" y2="0.889" layer="21"/>
<rectangle x1="11.557" y1="0.80441875" x2="11.811" y2="0.889" layer="21"/>
<rectangle x1="11.98118125" y1="0.80441875" x2="12.14881875" y2="0.889" layer="21"/>
<rectangle x1="12.99718125" y1="0.80441875" x2="13.16481875" y2="0.889" layer="21"/>
<rectangle x1="13.67281875" y1="0.80441875" x2="13.92681875" y2="0.889" layer="21"/>
<rectangle x1="15.113" y1="0.80441875" x2="15.367" y2="0.889" layer="21"/>
<rectangle x1="15.621" y1="0.80441875" x2="15.875" y2="0.889" layer="21"/>
<rectangle x1="16.72081875" y1="0.80441875" x2="16.97481875" y2="0.889" layer="21"/>
<rectangle x1="17.145" y1="0.80441875" x2="17.399" y2="0.889" layer="21"/>
<rectangle x1="18.161" y1="0.80441875" x2="18.33118125" y2="0.889" layer="21"/>
<rectangle x1="2.32918125" y1="0.889" x2="2.58318125" y2="0.97358125" layer="21"/>
<rectangle x1="3.937" y1="0.889" x2="4.191" y2="0.97358125" layer="21"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.61518125" y2="0.97358125" layer="21"/>
<rectangle x1="5.54481875" y1="0.889" x2="5.715" y2="0.97358125" layer="21"/>
<rectangle x1="5.88518125" y1="0.889" x2="6.13918125" y2="0.97358125" layer="21"/>
<rectangle x1="6.477" y1="0.889" x2="6.731" y2="0.97358125" layer="21"/>
<rectangle x1="7.493" y1="0.889" x2="7.747" y2="0.97358125" layer="21"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.10081875" y2="0.97358125" layer="21"/>
<rectangle x1="9.35481875" y1="0.889" x2="9.60881875" y2="0.97358125" layer="21"/>
<rectangle x1="10.45718125" y1="0.889" x2="10.71118125" y2="0.97358125" layer="21"/>
<rectangle x1="11.47318125" y1="0.889" x2="11.72718125" y2="0.97358125" layer="21"/>
<rectangle x1="11.89481875" y1="0.889" x2="12.14881875" y2="0.97358125" layer="21"/>
<rectangle x1="13.589" y1="0.889" x2="13.843" y2="0.97358125" layer="21"/>
<rectangle x1="15.19681875" y1="0.889" x2="15.45081875" y2="0.97358125" layer="21"/>
<rectangle x1="15.621" y1="0.889" x2="15.875" y2="0.97358125" layer="21"/>
<rectangle x1="16.72081875" y1="0.889" x2="16.97481875" y2="0.97358125" layer="21"/>
<rectangle x1="17.06118125" y1="0.889" x2="17.31518125" y2="0.97358125" layer="21"/>
<rectangle x1="2.32918125" y1="0.97358125" x2="2.49681875" y2="1.05841875" layer="21"/>
<rectangle x1="3.937" y1="0.97358125" x2="4.191" y2="1.05841875" layer="21"/>
<rectangle x1="4.36118125" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="21"/>
<rectangle x1="5.54481875" y1="0.97358125" x2="5.715" y2="1.05841875" layer="21"/>
<rectangle x1="5.88518125" y1="0.97358125" x2="6.13918125" y2="1.05841875" layer="21"/>
<rectangle x1="6.477" y1="0.97358125" x2="6.64718125" y2="1.05841875" layer="21"/>
<rectangle x1="7.493" y1="0.97358125" x2="7.747" y2="1.05841875" layer="21"/>
<rectangle x1="8.93318125" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="21"/>
<rectangle x1="9.35481875" y1="0.97358125" x2="9.60881875" y2="1.05841875" layer="21"/>
<rectangle x1="10.45718125" y1="0.97358125" x2="10.71118125" y2="1.05841875" layer="21"/>
<rectangle x1="11.21918125" y1="0.97358125" x2="11.72718125" y2="1.05841875" layer="21"/>
<rectangle x1="11.89481875" y1="0.97358125" x2="13.25118125" y2="1.05841875" layer="21"/>
<rectangle x1="13.589" y1="0.97358125" x2="13.843" y2="1.05841875" layer="21"/>
<rectangle x1="15.19681875" y1="0.97358125" x2="15.45081875" y2="1.05841875" layer="21"/>
<rectangle x1="15.621" y1="0.97358125" x2="15.875" y2="1.05841875" layer="21"/>
<rectangle x1="16.72081875" y1="0.97358125" x2="16.97481875" y2="1.05841875" layer="21"/>
<rectangle x1="17.06118125" y1="0.97358125" x2="18.415" y2="1.05841875" layer="21"/>
<rectangle x1="2.24281875" y1="1.05841875" x2="2.49681875" y2="1.143" layer="21"/>
<rectangle x1="3.937" y1="1.05841875" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="4.36118125" y1="1.05841875" x2="4.61518125" y2="1.143" layer="21"/>
<rectangle x1="5.54481875" y1="1.05841875" x2="5.715" y2="1.143" layer="21"/>
<rectangle x1="5.88518125" y1="1.05841875" x2="6.13918125" y2="1.143" layer="21"/>
<rectangle x1="6.477" y1="1.05841875" x2="6.64718125" y2="1.143" layer="21"/>
<rectangle x1="7.493" y1="1.05841875" x2="7.747" y2="1.143" layer="21"/>
<rectangle x1="8.93318125" y1="1.05841875" x2="9.18718125" y2="1.143" layer="21"/>
<rectangle x1="9.35481875" y1="1.05841875" x2="9.60881875" y2="1.143" layer="21"/>
<rectangle x1="10.45718125" y1="1.05841875" x2="10.71118125" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="1.05841875" x2="11.557" y2="1.143" layer="21"/>
<rectangle x1="11.89481875" y1="1.05841875" x2="13.25118125" y2="1.143" layer="21"/>
<rectangle x1="13.589" y1="1.05841875" x2="13.75918125" y2="1.143" layer="21"/>
<rectangle x1="15.28318125" y1="1.05841875" x2="15.53718125" y2="1.143" layer="21"/>
<rectangle x1="15.621" y1="1.05841875" x2="15.875" y2="1.143" layer="21"/>
<rectangle x1="16.72081875" y1="1.05841875" x2="16.97481875" y2="1.143" layer="21"/>
<rectangle x1="17.06118125" y1="1.05841875" x2="18.415" y2="1.143" layer="21"/>
<rectangle x1="2.24281875" y1="1.143" x2="2.49681875" y2="1.22758125" layer="21"/>
<rectangle x1="4.02081875" y1="1.143" x2="4.191" y2="1.22758125" layer="21"/>
<rectangle x1="4.36118125" y1="1.143" x2="4.61518125" y2="1.22758125" layer="21"/>
<rectangle x1="5.54481875" y1="1.143" x2="5.715" y2="1.22758125" layer="21"/>
<rectangle x1="5.88518125" y1="1.143" x2="6.13918125" y2="1.22758125" layer="21"/>
<rectangle x1="6.477" y1="1.143" x2="6.64718125" y2="1.22758125" layer="21"/>
<rectangle x1="7.493" y1="1.143" x2="7.747" y2="1.22758125" layer="21"/>
<rectangle x1="9.017" y1="1.143" x2="9.271" y2="1.22758125" layer="21"/>
<rectangle x1="9.35481875" y1="1.143" x2="9.60881875" y2="1.22758125" layer="21"/>
<rectangle x1="10.45718125" y1="1.143" x2="10.71118125" y2="1.22758125" layer="21"/>
<rectangle x1="10.87881875" y1="1.143" x2="11.38681875" y2="1.22758125" layer="21"/>
<rectangle x1="11.89481875" y1="1.143" x2="13.25118125" y2="1.22758125" layer="21"/>
<rectangle x1="13.50518125" y1="1.143" x2="13.75918125" y2="1.22758125" layer="21"/>
<rectangle x1="15.28318125" y1="1.143" x2="15.53718125" y2="1.22758125" layer="21"/>
<rectangle x1="15.621" y1="1.143" x2="15.875" y2="1.22758125" layer="21"/>
<rectangle x1="16.72081875" y1="1.143" x2="16.97481875" y2="1.22758125" layer="21"/>
<rectangle x1="17.06118125" y1="1.143" x2="18.415" y2="1.22758125" layer="21"/>
<rectangle x1="2.24281875" y1="1.22758125" x2="2.49681875" y2="1.31241875" layer="21"/>
<rectangle x1="4.02081875" y1="1.22758125" x2="4.191" y2="1.31241875" layer="21"/>
<rectangle x1="4.36118125" y1="1.22758125" x2="4.61518125" y2="1.31241875" layer="21"/>
<rectangle x1="5.461" y1="1.22758125" x2="5.715" y2="1.31241875" layer="21"/>
<rectangle x1="5.88518125" y1="1.22758125" x2="6.13918125" y2="1.31241875" layer="21"/>
<rectangle x1="6.477" y1="1.22758125" x2="6.64718125" y2="1.31241875" layer="21"/>
<rectangle x1="7.493" y1="1.22758125" x2="7.747" y2="1.31241875" layer="21"/>
<rectangle x1="9.017" y1="1.22758125" x2="9.271" y2="1.31241875" layer="21"/>
<rectangle x1="9.35481875" y1="1.22758125" x2="9.60881875" y2="1.31241875" layer="21"/>
<rectangle x1="10.45718125" y1="1.22758125" x2="10.71118125" y2="1.31241875" layer="21"/>
<rectangle x1="10.795" y1="1.22758125" x2="11.21918125" y2="1.31241875" layer="21"/>
<rectangle x1="11.89481875" y1="1.22758125" x2="12.14881875" y2="1.31241875" layer="21"/>
<rectangle x1="12.99718125" y1="1.22758125" x2="13.25118125" y2="1.31241875" layer="21"/>
<rectangle x1="13.50518125" y1="1.22758125" x2="13.75918125" y2="1.31241875" layer="21"/>
<rectangle x1="15.28318125" y1="1.22758125" x2="15.53718125" y2="1.31241875" layer="21"/>
<rectangle x1="15.621" y1="1.22758125" x2="15.875" y2="1.31241875" layer="21"/>
<rectangle x1="16.72081875" y1="1.22758125" x2="16.97481875" y2="1.31241875" layer="21"/>
<rectangle x1="17.06118125" y1="1.22758125" x2="17.31518125" y2="1.31241875" layer="21"/>
<rectangle x1="18.161" y1="1.22758125" x2="18.415" y2="1.31241875" layer="21"/>
<rectangle x1="2.24281875" y1="1.31241875" x2="2.49681875" y2="1.397" layer="21"/>
<rectangle x1="4.02081875" y1="1.31241875" x2="4.191" y2="1.397" layer="21"/>
<rectangle x1="4.36118125" y1="1.31241875" x2="4.699" y2="1.397" layer="21"/>
<rectangle x1="5.461" y1="1.31241875" x2="5.715" y2="1.397" layer="21"/>
<rectangle x1="5.88518125" y1="1.31241875" x2="6.13918125" y2="1.397" layer="21"/>
<rectangle x1="6.477" y1="1.31241875" x2="6.64718125" y2="1.397" layer="21"/>
<rectangle x1="7.493" y1="1.31241875" x2="7.747" y2="1.397" layer="21"/>
<rectangle x1="9.017" y1="1.31241875" x2="9.271" y2="1.397" layer="21"/>
<rectangle x1="9.44118125" y1="1.31241875" x2="9.69518125" y2="1.397" layer="21"/>
<rectangle x1="10.45718125" y1="1.31241875" x2="10.62481875" y2="1.397" layer="21"/>
<rectangle x1="10.795" y1="1.31241875" x2="11.049" y2="1.397" layer="21"/>
<rectangle x1="11.64081875" y1="1.31241875" x2="11.72718125" y2="1.397" layer="21"/>
<rectangle x1="11.98118125" y1="1.31241875" x2="12.23518125" y2="1.397" layer="21"/>
<rectangle x1="12.99718125" y1="1.31241875" x2="13.16481875" y2="1.397" layer="21"/>
<rectangle x1="13.50518125" y1="1.31241875" x2="13.75918125" y2="1.397" layer="21"/>
<rectangle x1="15.28318125" y1="1.31241875" x2="15.53718125" y2="1.397" layer="21"/>
<rectangle x1="15.621" y1="1.31241875" x2="15.875" y2="1.397" layer="21"/>
<rectangle x1="16.637" y1="1.31241875" x2="16.891" y2="1.397" layer="21"/>
<rectangle x1="17.145" y1="1.31241875" x2="17.399" y2="1.397" layer="21"/>
<rectangle x1="18.161" y1="1.31241875" x2="18.415" y2="1.397" layer="21"/>
<rectangle x1="2.24281875" y1="1.397" x2="2.49681875" y2="1.48158125" layer="21"/>
<rectangle x1="4.02081875" y1="1.397" x2="4.191" y2="1.48158125" layer="21"/>
<rectangle x1="4.36118125" y1="1.397" x2="4.78281875" y2="1.48158125" layer="21"/>
<rectangle x1="5.37718125" y1="1.397" x2="5.63118125" y2="1.48158125" layer="21"/>
<rectangle x1="5.88518125" y1="1.397" x2="6.13918125" y2="1.48158125" layer="21"/>
<rectangle x1="6.477" y1="1.397" x2="6.64718125" y2="1.48158125" layer="21"/>
<rectangle x1="7.493" y1="1.397" x2="7.747" y2="1.48158125" layer="21"/>
<rectangle x1="9.017" y1="1.397" x2="9.271" y2="1.48158125" layer="21"/>
<rectangle x1="9.44118125" y1="1.397" x2="9.69518125" y2="1.48158125" layer="21"/>
<rectangle x1="10.37081875" y1="1.397" x2="10.62481875" y2="1.48158125" layer="21"/>
<rectangle x1="10.795" y1="1.397" x2="11.049" y2="1.48158125" layer="21"/>
<rectangle x1="11.557" y1="1.397" x2="11.811" y2="1.48158125" layer="21"/>
<rectangle x1="11.98118125" y1="1.397" x2="12.319" y2="1.48158125" layer="21"/>
<rectangle x1="12.91081875" y1="1.397" x2="13.16481875" y2="1.48158125" layer="21"/>
<rectangle x1="13.50518125" y1="1.397" x2="13.75918125" y2="1.48158125" layer="21"/>
<rectangle x1="15.28318125" y1="1.397" x2="15.53718125" y2="1.48158125" layer="21"/>
<rectangle x1="15.621" y1="1.397" x2="15.95881875" y2="1.48158125" layer="21"/>
<rectangle x1="16.637" y1="1.397" x2="16.891" y2="1.48158125" layer="21"/>
<rectangle x1="17.145" y1="1.397" x2="17.48281875" y2="1.48158125" layer="21"/>
<rectangle x1="18.07718125" y1="1.397" x2="18.33118125" y2="1.48158125" layer="21"/>
<rectangle x1="2.24281875" y1="1.48158125" x2="2.49681875" y2="1.56641875" layer="21"/>
<rectangle x1="4.02081875" y1="1.48158125" x2="4.191" y2="1.56641875" layer="21"/>
<rectangle x1="4.36118125" y1="1.48158125" x2="4.953" y2="1.56641875" layer="21"/>
<rectangle x1="5.207" y1="1.48158125" x2="5.63118125" y2="1.56641875" layer="21"/>
<rectangle x1="5.88518125" y1="1.48158125" x2="6.13918125" y2="1.56641875" layer="21"/>
<rectangle x1="6.477" y1="1.48158125" x2="6.64718125" y2="1.56641875" layer="21"/>
<rectangle x1="7.493" y1="1.48158125" x2="7.747" y2="1.56641875" layer="21"/>
<rectangle x1="9.017" y1="1.48158125" x2="9.271" y2="1.56641875" layer="21"/>
<rectangle x1="9.525" y1="1.48158125" x2="9.86281875" y2="1.56641875" layer="21"/>
<rectangle x1="10.20318125" y1="1.48158125" x2="10.541" y2="1.56641875" layer="21"/>
<rectangle x1="10.87881875" y1="1.48158125" x2="11.21918125" y2="1.56641875" layer="21"/>
<rectangle x1="11.47318125" y1="1.48158125" x2="11.72718125" y2="1.56641875" layer="21"/>
<rectangle x1="12.065" y1="1.48158125" x2="12.40281875" y2="1.56641875" layer="21"/>
<rectangle x1="12.74318125" y1="1.48158125" x2="13.081" y2="1.56641875" layer="21"/>
<rectangle x1="13.589" y1="1.48158125" x2="13.75918125" y2="1.56641875" layer="21"/>
<rectangle x1="15.28318125" y1="1.48158125" x2="15.53718125" y2="1.56641875" layer="21"/>
<rectangle x1="15.621" y1="1.48158125" x2="16.129" y2="1.56641875" layer="21"/>
<rectangle x1="16.46681875" y1="1.48158125" x2="16.80718125" y2="1.56641875" layer="21"/>
<rectangle x1="17.22881875" y1="1.48158125" x2="17.653" y2="1.56641875" layer="21"/>
<rectangle x1="17.907" y1="1.48158125" x2="18.24481875" y2="1.56641875" layer="21"/>
<rectangle x1="2.24281875" y1="1.56641875" x2="2.49681875" y2="1.651" layer="21"/>
<rectangle x1="4.02081875" y1="1.56641875" x2="4.191" y2="1.651" layer="21"/>
<rectangle x1="4.36118125" y1="1.56641875" x2="5.54481875" y2="1.651" layer="21"/>
<rectangle x1="5.88518125" y1="1.56641875" x2="6.13918125" y2="1.651" layer="21"/>
<rectangle x1="6.223" y1="1.56641875" x2="7.239" y2="1.651" layer="21"/>
<rectangle x1="7.493" y1="1.56641875" x2="7.747" y2="1.651" layer="21"/>
<rectangle x1="9.017" y1="1.56641875" x2="9.271" y2="1.651" layer="21"/>
<rectangle x1="9.60881875" y1="1.56641875" x2="10.45718125" y2="1.651" layer="21"/>
<rectangle x1="10.87881875" y1="1.56641875" x2="11.72718125" y2="1.651" layer="21"/>
<rectangle x1="12.14881875" y1="1.56641875" x2="12.99718125" y2="1.651" layer="21"/>
<rectangle x1="13.589" y1="1.56641875" x2="13.843" y2="1.651" layer="21"/>
<rectangle x1="15.19681875" y1="1.56641875" x2="15.45081875" y2="1.651" layer="21"/>
<rectangle x1="15.621" y1="1.56641875" x2="16.72081875" y2="1.651" layer="21"/>
<rectangle x1="17.31518125" y1="1.56641875" x2="18.161" y2="1.651" layer="21"/>
<rectangle x1="2.24281875" y1="1.651" x2="2.49681875" y2="1.73558125" layer="21"/>
<rectangle x1="4.02081875" y1="1.651" x2="4.191" y2="1.73558125" layer="21"/>
<rectangle x1="4.36118125" y1="1.651" x2="4.61518125" y2="1.73558125" layer="21"/>
<rectangle x1="4.78281875" y1="1.651" x2="5.37718125" y2="1.73558125" layer="21"/>
<rectangle x1="5.88518125" y1="1.651" x2="6.13918125" y2="1.73558125" layer="21"/>
<rectangle x1="6.223" y1="1.651" x2="7.239" y2="1.73558125" layer="21"/>
<rectangle x1="7.493" y1="1.651" x2="7.747" y2="1.73558125" layer="21"/>
<rectangle x1="9.017" y1="1.651" x2="9.18718125" y2="1.73558125" layer="21"/>
<rectangle x1="9.69518125" y1="1.651" x2="10.37081875" y2="1.73558125" layer="21"/>
<rectangle x1="11.049" y1="1.651" x2="11.557" y2="1.73558125" layer="21"/>
<rectangle x1="12.23518125" y1="1.651" x2="12.91081875" y2="1.73558125" layer="21"/>
<rectangle x1="13.589" y1="1.651" x2="13.843" y2="1.73558125" layer="21"/>
<rectangle x1="15.19681875" y1="1.651" x2="15.45081875" y2="1.73558125" layer="21"/>
<rectangle x1="15.621" y1="1.651" x2="15.79118125" y2="1.73558125" layer="21"/>
<rectangle x1="15.95881875" y1="1.651" x2="16.637" y2="1.73558125" layer="21"/>
<rectangle x1="17.399" y1="1.651" x2="18.07718125" y2="1.73558125" layer="21"/>
<rectangle x1="2.24281875" y1="1.73558125" x2="2.49681875" y2="1.82041875" layer="21"/>
<rectangle x1="4.02081875" y1="1.73558125" x2="4.191" y2="1.82041875" layer="21"/>
<rectangle x1="4.445" y1="1.73558125" x2="4.52881875" y2="1.82041875" layer="21"/>
<rectangle x1="4.953" y1="1.73558125" x2="5.12318125" y2="1.82041875" layer="21"/>
<rectangle x1="5.969" y1="1.73558125" x2="6.05281875" y2="1.82041875" layer="21"/>
<rectangle x1="6.30681875" y1="1.73558125" x2="7.15518125" y2="1.82041875" layer="21"/>
<rectangle x1="7.493" y1="1.73558125" x2="7.747" y2="1.82041875" layer="21"/>
<rectangle x1="8.93318125" y1="1.73558125" x2="9.18718125" y2="1.82041875" layer="21"/>
<rectangle x1="9.94918125" y1="1.73558125" x2="10.11681875" y2="1.82041875" layer="21"/>
<rectangle x1="11.21918125" y1="1.73558125" x2="11.38681875" y2="1.82041875" layer="21"/>
<rectangle x1="12.48918125" y1="1.73558125" x2="12.65681875" y2="1.82041875" layer="21"/>
<rectangle x1="13.67281875" y1="1.73558125" x2="13.92681875" y2="1.82041875" layer="21"/>
<rectangle x1="15.113" y1="1.73558125" x2="15.367" y2="1.82041875" layer="21"/>
<rectangle x1="16.21281875" y1="1.73558125" x2="16.383" y2="1.82041875" layer="21"/>
<rectangle x1="17.653" y1="1.73558125" x2="17.82318125" y2="1.82041875" layer="21"/>
<rectangle x1="2.24281875" y1="1.82041875" x2="2.49681875" y2="1.905" layer="21"/>
<rectangle x1="4.02081875" y1="1.82041875" x2="4.191" y2="1.905" layer="21"/>
<rectangle x1="5.88518125" y1="1.82041875" x2="6.13918125" y2="1.905" layer="21"/>
<rectangle x1="6.477" y1="1.82041875" x2="6.64718125" y2="1.905" layer="21"/>
<rectangle x1="7.493" y1="1.82041875" x2="7.747" y2="1.905" layer="21"/>
<rectangle x1="8.84681875" y1="1.82041875" x2="9.18718125" y2="1.905" layer="21"/>
<rectangle x1="13.67281875" y1="1.82041875" x2="14.01318125" y2="1.905" layer="21"/>
<rectangle x1="15.02918125" y1="1.82041875" x2="15.367" y2="1.905" layer="21"/>
<rectangle x1="2.24281875" y1="1.905" x2="2.49681875" y2="1.98958125" layer="21"/>
<rectangle x1="4.02081875" y1="1.905" x2="4.191" y2="1.98958125" layer="21"/>
<rectangle x1="5.88518125" y1="1.905" x2="6.13918125" y2="1.98958125" layer="21"/>
<rectangle x1="6.477" y1="1.905" x2="6.64718125" y2="1.98958125" layer="21"/>
<rectangle x1="7.493" y1="1.905" x2="7.747" y2="1.98958125" layer="21"/>
<rectangle x1="8.84681875" y1="1.905" x2="9.10081875" y2="1.98958125" layer="21"/>
<rectangle x1="13.75918125" y1="1.905" x2="14.097" y2="1.98958125" layer="21"/>
<rectangle x1="14.94281875" y1="1.905" x2="15.28318125" y2="1.98958125" layer="21"/>
<rectangle x1="2.24281875" y1="1.98958125" x2="2.49681875" y2="2.07441875" layer="21"/>
<rectangle x1="4.02081875" y1="1.98958125" x2="4.27481875" y2="2.07441875" layer="21"/>
<rectangle x1="5.88518125" y1="1.98958125" x2="6.05281875" y2="2.07441875" layer="21"/>
<rectangle x1="6.477" y1="1.98958125" x2="6.64718125" y2="2.07441875" layer="21"/>
<rectangle x1="7.493" y1="1.98958125" x2="7.747" y2="2.07441875" layer="21"/>
<rectangle x1="8.67918125" y1="1.98958125" x2="9.017" y2="2.07441875" layer="21"/>
<rectangle x1="13.843" y1="1.98958125" x2="14.26718125" y2="2.07441875" layer="21"/>
<rectangle x1="14.77518125" y1="1.98958125" x2="15.19681875" y2="2.07441875" layer="21"/>
<rectangle x1="2.24281875" y1="2.07441875" x2="2.49681875" y2="2.159" layer="21"/>
<rectangle x1="4.02081875" y1="2.07441875" x2="4.27481875" y2="2.159" layer="21"/>
<rectangle x1="6.477" y1="2.07441875" x2="6.64718125" y2="2.159" layer="21"/>
<rectangle x1="7.493" y1="2.07441875" x2="7.747" y2="2.159" layer="21"/>
<rectangle x1="8.59281875" y1="2.07441875" x2="8.93318125" y2="2.159" layer="21"/>
<rectangle x1="13.92681875" y1="2.07441875" x2="15.113" y2="2.159" layer="21"/>
<rectangle x1="2.24281875" y1="2.159" x2="2.49681875" y2="2.24358125" layer="21"/>
<rectangle x1="4.02081875" y1="2.159" x2="4.191" y2="2.24358125" layer="21"/>
<rectangle x1="6.477" y1="2.159" x2="6.64718125" y2="2.24358125" layer="21"/>
<rectangle x1="7.493" y1="2.159" x2="8.84681875" y2="2.24358125" layer="21"/>
<rectangle x1="14.097" y1="2.159" x2="14.94281875" y2="2.24358125" layer="21"/>
<rectangle x1="2.24281875" y1="2.24358125" x2="2.413" y2="2.32841875" layer="21"/>
<rectangle x1="4.02081875" y1="2.24358125" x2="4.191" y2="2.32841875" layer="21"/>
<rectangle x1="6.477" y1="2.24358125" x2="6.64718125" y2="2.32841875" layer="21"/>
<rectangle x1="7.493" y1="2.24358125" x2="8.763" y2="2.32841875" layer="21"/>
<rectangle x1="14.26718125" y1="2.24358125" x2="14.77518125" y2="2.32841875" layer="21"/>
<rectangle x1="7.493" y1="2.32841875" x2="8.509" y2="2.413" layer="21"/>
<rectangle x1="3.34518125" y1="3.25958125" x2="16.80718125" y2="3.34441875" layer="21"/>
<rectangle x1="3.00481875" y1="3.34441875" x2="17.145" y2="3.429" layer="21"/>
<rectangle x1="2.667" y1="3.429" x2="17.399" y2="3.51358125" layer="21"/>
<rectangle x1="2.49681875" y1="3.51358125" x2="17.56918125" y2="3.59841875" layer="21"/>
<rectangle x1="2.32918125" y1="3.59841875" x2="17.82318125" y2="3.683" layer="21"/>
<rectangle x1="2.159" y1="3.683" x2="17.99081875" y2="3.76758125" layer="21"/>
<rectangle x1="1.98881875" y1="3.76758125" x2="18.07718125" y2="3.85241875" layer="21"/>
<rectangle x1="1.905" y1="3.85241875" x2="3.51281875" y2="3.937" layer="21"/>
<rectangle x1="9.779" y1="3.85241875" x2="18.24481875" y2="3.937" layer="21"/>
<rectangle x1="1.73481875" y1="3.937" x2="3.175" y2="4.02158125" layer="21"/>
<rectangle x1="9.779" y1="3.937" x2="18.33118125" y2="4.02158125" layer="21"/>
<rectangle x1="1.651" y1="4.02158125" x2="2.83718125" y2="4.10641875" layer="21"/>
<rectangle x1="9.69518125" y1="4.02158125" x2="18.49881875" y2="4.10641875" layer="21"/>
<rectangle x1="1.56718125" y1="4.10641875" x2="2.667" y2="4.191" layer="21"/>
<rectangle x1="9.60881875" y1="4.10641875" x2="18.58518125" y2="4.191" layer="21"/>
<rectangle x1="1.397" y1="4.191" x2="2.49681875" y2="4.27558125" layer="21"/>
<rectangle x1="9.60881875" y1="4.191" x2="18.669" y2="4.27558125" layer="21"/>
<rectangle x1="1.31318125" y1="4.27558125" x2="2.32918125" y2="4.36041875" layer="21"/>
<rectangle x1="9.525" y1="4.27558125" x2="18.75281875" y2="4.36041875" layer="21"/>
<rectangle x1="1.22681875" y1="4.36041875" x2="2.159" y2="4.445" layer="21"/>
<rectangle x1="9.525" y1="4.36041875" x2="18.83918125" y2="4.445" layer="21"/>
<rectangle x1="1.143" y1="4.445" x2="2.07518125" y2="4.52958125" layer="21"/>
<rectangle x1="9.44118125" y1="4.445" x2="18.923" y2="4.52958125" layer="21"/>
<rectangle x1="1.05918125" y1="4.52958125" x2="1.98881875" y2="4.61441875" layer="21"/>
<rectangle x1="9.44118125" y1="4.52958125" x2="19.00681875" y2="4.61441875" layer="21"/>
<rectangle x1="0.97281875" y1="4.61441875" x2="1.905" y2="4.699" layer="21"/>
<rectangle x1="9.44118125" y1="4.61441875" x2="19.09318125" y2="4.699" layer="21"/>
<rectangle x1="0.97281875" y1="4.699" x2="1.73481875" y2="4.78358125" layer="21"/>
<rectangle x1="9.35481875" y1="4.699" x2="19.177" y2="4.78358125" layer="21"/>
<rectangle x1="0.889" y1="4.78358125" x2="1.651" y2="4.86841875" layer="21"/>
<rectangle x1="9.35481875" y1="4.78358125" x2="19.26081875" y2="4.86841875" layer="21"/>
<rectangle x1="0.80518125" y1="4.86841875" x2="1.56718125" y2="4.953" layer="21"/>
<rectangle x1="9.35481875" y1="4.86841875" x2="19.26081875" y2="4.953" layer="21"/>
<rectangle x1="0.71881875" y1="4.953" x2="1.48081875" y2="5.03758125" layer="21"/>
<rectangle x1="9.271" y1="4.953" x2="19.34718125" y2="5.03758125" layer="21"/>
<rectangle x1="0.71881875" y1="5.03758125" x2="1.48081875" y2="5.12241875" layer="21"/>
<rectangle x1="9.271" y1="5.03758125" x2="19.431" y2="5.12241875" layer="21"/>
<rectangle x1="0.635" y1="5.12241875" x2="1.397" y2="5.207" layer="21"/>
<rectangle x1="9.271" y1="5.12241875" x2="19.431" y2="5.207" layer="21"/>
<rectangle x1="0.55118125" y1="5.207" x2="1.31318125" y2="5.29158125" layer="21"/>
<rectangle x1="9.271" y1="5.207" x2="19.51481875" y2="5.29158125" layer="21"/>
<rectangle x1="0.55118125" y1="5.29158125" x2="1.22681875" y2="5.37641875" layer="21"/>
<rectangle x1="9.18718125" y1="5.29158125" x2="19.51481875" y2="5.37641875" layer="21"/>
<rectangle x1="0.46481875" y1="5.37641875" x2="1.22681875" y2="5.461" layer="21"/>
<rectangle x1="9.18718125" y1="5.37641875" x2="19.60118125" y2="5.461" layer="21"/>
<rectangle x1="0.46481875" y1="5.461" x2="1.143" y2="5.54558125" layer="21"/>
<rectangle x1="9.18718125" y1="5.461" x2="19.685" y2="5.54558125" layer="21"/>
<rectangle x1="0.381" y1="5.54558125" x2="1.05918125" y2="5.63041875" layer="21"/>
<rectangle x1="3.85318125" y1="5.54558125" x2="4.27481875" y2="5.63041875" layer="21"/>
<rectangle x1="5.88518125" y1="5.54558125" x2="6.81481875" y2="5.63041875" layer="21"/>
<rectangle x1="9.18718125" y1="5.54558125" x2="19.685" y2="5.63041875" layer="21"/>
<rectangle x1="0.381" y1="5.63041875" x2="1.05918125" y2="5.715" layer="21"/>
<rectangle x1="3.51281875" y1="5.63041875" x2="4.61518125" y2="5.715" layer="21"/>
<rectangle x1="5.79881875" y1="5.63041875" x2="7.239" y2="5.715" layer="21"/>
<rectangle x1="9.10081875" y1="5.63041875" x2="10.96518125" y2="5.715" layer="21"/>
<rectangle x1="11.64081875" y1="5.63041875" x2="19.685" y2="5.715" layer="21"/>
<rectangle x1="0.381" y1="5.715" x2="0.97281875" y2="5.79958125" layer="21"/>
<rectangle x1="3.34518125" y1="5.715" x2="4.78281875" y2="5.79958125" layer="21"/>
<rectangle x1="5.715" y1="5.715" x2="7.40918125" y2="5.79958125" layer="21"/>
<rectangle x1="9.10081875" y1="5.715" x2="10.71118125" y2="5.79958125" layer="21"/>
<rectangle x1="11.89481875" y1="5.715" x2="13.335" y2="5.79958125" layer="21"/>
<rectangle x1="13.67281875" y1="5.715" x2="15.113" y2="5.79958125" layer="21"/>
<rectangle x1="15.367" y1="5.715" x2="16.383" y2="5.79958125" layer="21"/>
<rectangle x1="17.22881875" y1="5.715" x2="19.76881875" y2="5.79958125" layer="21"/>
<rectangle x1="0.29718125" y1="5.79958125" x2="0.97281875" y2="5.88441875" layer="21"/>
<rectangle x1="3.175" y1="5.79958125" x2="4.953" y2="5.88441875" layer="21"/>
<rectangle x1="5.715" y1="5.79958125" x2="7.57681875" y2="5.88441875" layer="21"/>
<rectangle x1="9.10081875" y1="5.79958125" x2="10.541" y2="5.88441875" layer="21"/>
<rectangle x1="12.065" y1="5.79958125" x2="13.335" y2="5.88441875" layer="21"/>
<rectangle x1="13.67281875" y1="5.79958125" x2="15.113" y2="5.88441875" layer="21"/>
<rectangle x1="15.45081875" y1="5.79958125" x2="16.21281875" y2="5.88441875" layer="21"/>
<rectangle x1="17.48281875" y1="5.79958125" x2="19.76881875" y2="5.88441875" layer="21"/>
<rectangle x1="0.29718125" y1="5.88441875" x2="0.889" y2="5.969" layer="21"/>
<rectangle x1="3.09118125" y1="5.88441875" x2="5.12318125" y2="5.969" layer="21"/>
<rectangle x1="5.715" y1="5.88441875" x2="7.66318125" y2="5.969" layer="21"/>
<rectangle x1="9.10081875" y1="5.88441875" x2="10.37081875" y2="5.969" layer="21"/>
<rectangle x1="12.23518125" y1="5.88441875" x2="13.335" y2="5.969" layer="21"/>
<rectangle x1="13.67281875" y1="5.88441875" x2="15.113" y2="5.969" layer="21"/>
<rectangle x1="15.45081875" y1="5.88441875" x2="16.129" y2="5.969" layer="21"/>
<rectangle x1="17.56918125" y1="5.88441875" x2="19.85518125" y2="5.969" layer="21"/>
<rectangle x1="0.21081875" y1="5.969" x2="0.889" y2="6.05358125" layer="21"/>
<rectangle x1="2.921" y1="5.969" x2="5.207" y2="6.05358125" layer="21"/>
<rectangle x1="5.715" y1="5.969" x2="7.747" y2="6.05358125" layer="21"/>
<rectangle x1="9.10081875" y1="5.969" x2="10.287" y2="6.05358125" layer="21"/>
<rectangle x1="12.319" y1="5.969" x2="13.335" y2="6.05358125" layer="21"/>
<rectangle x1="13.67281875" y1="5.969" x2="15.113" y2="6.05358125" layer="21"/>
<rectangle x1="15.45081875" y1="5.969" x2="16.04518125" y2="6.05358125" layer="21"/>
<rectangle x1="17.653" y1="5.969" x2="19.85518125" y2="6.05358125" layer="21"/>
<rectangle x1="0.21081875" y1="6.05358125" x2="0.889" y2="6.13841875" layer="21"/>
<rectangle x1="2.83718125" y1="6.05358125" x2="5.29081875" y2="6.13841875" layer="21"/>
<rectangle x1="5.79881875" y1="6.05358125" x2="7.83081875" y2="6.13841875" layer="21"/>
<rectangle x1="9.10081875" y1="6.05358125" x2="10.11681875" y2="6.13841875" layer="21"/>
<rectangle x1="12.40281875" y1="6.05358125" x2="13.335" y2="6.13841875" layer="21"/>
<rectangle x1="13.67281875" y1="6.05358125" x2="15.113" y2="6.13841875" layer="21"/>
<rectangle x1="15.45081875" y1="6.05358125" x2="15.95881875" y2="6.13841875" layer="21"/>
<rectangle x1="16.55318125" y1="6.05358125" x2="17.06118125" y2="6.13841875" layer="21"/>
<rectangle x1="17.73681875" y1="6.05358125" x2="19.85518125" y2="6.13841875" layer="21"/>
<rectangle x1="0.21081875" y1="6.13841875" x2="0.80518125" y2="6.223" layer="21"/>
<rectangle x1="2.75081875" y1="6.13841875" x2="5.37718125" y2="6.223" layer="21"/>
<rectangle x1="5.88518125" y1="6.13841875" x2="7.91718125" y2="6.223" layer="21"/>
<rectangle x1="9.017" y1="6.13841875" x2="10.11681875" y2="6.223" layer="21"/>
<rectangle x1="10.96518125" y1="6.13841875" x2="11.64081875" y2="6.223" layer="21"/>
<rectangle x1="12.48918125" y1="6.13841875" x2="13.335" y2="6.223" layer="21"/>
<rectangle x1="13.67281875" y1="6.13841875" x2="15.02918125" y2="6.223" layer="21"/>
<rectangle x1="15.45081875" y1="6.13841875" x2="15.875" y2="6.223" layer="21"/>
<rectangle x1="16.46681875" y1="6.13841875" x2="17.22881875" y2="6.223" layer="21"/>
<rectangle x1="17.73681875" y1="6.13841875" x2="19.939" y2="6.223" layer="21"/>
<rectangle x1="0.21081875" y1="6.223" x2="0.80518125" y2="6.30758125" layer="21"/>
<rectangle x1="2.75081875" y1="6.223" x2="3.683" y2="6.30758125" layer="21"/>
<rectangle x1="4.445" y1="6.223" x2="5.37718125" y2="6.30758125" layer="21"/>
<rectangle x1="7.06881875" y1="6.223" x2="8.001" y2="6.30758125" layer="21"/>
<rectangle x1="9.017" y1="6.223" x2="10.033" y2="6.30758125" layer="21"/>
<rectangle x1="10.795" y1="6.223" x2="11.811" y2="6.30758125" layer="21"/>
<rectangle x1="12.573" y1="6.223" x2="13.335" y2="6.30758125" layer="21"/>
<rectangle x1="13.67281875" y1="6.223" x2="15.02918125" y2="6.30758125" layer="21"/>
<rectangle x1="15.45081875" y1="6.223" x2="15.875" y2="6.30758125" layer="21"/>
<rectangle x1="16.29918125" y1="6.223" x2="17.399" y2="6.30758125" layer="21"/>
<rectangle x1="17.73681875" y1="6.223" x2="19.939" y2="6.30758125" layer="21"/>
<rectangle x1="0.127" y1="6.30758125" x2="0.80518125" y2="6.39241875" layer="21"/>
<rectangle x1="2.667" y1="6.30758125" x2="3.51281875" y2="6.39241875" layer="21"/>
<rectangle x1="4.61518125" y1="6.30758125" x2="5.461" y2="6.39241875" layer="21"/>
<rectangle x1="7.239" y1="6.30758125" x2="8.08481875" y2="6.39241875" layer="21"/>
<rectangle x1="9.017" y1="6.30758125" x2="9.94918125" y2="6.39241875" layer="21"/>
<rectangle x1="10.62481875" y1="6.30758125" x2="11.98118125" y2="6.39241875" layer="21"/>
<rectangle x1="12.65681875" y1="6.30758125" x2="13.335" y2="6.39241875" layer="21"/>
<rectangle x1="13.67281875" y1="6.30758125" x2="15.113" y2="6.39241875" layer="21"/>
<rectangle x1="15.45081875" y1="6.30758125" x2="15.79118125" y2="6.39241875" layer="21"/>
<rectangle x1="16.21281875" y1="6.30758125" x2="17.48281875" y2="6.39241875" layer="21"/>
<rectangle x1="17.653" y1="6.30758125" x2="19.939" y2="6.39241875" layer="21"/>
<rectangle x1="0.127" y1="6.39241875" x2="0.71881875" y2="6.477" layer="21"/>
<rectangle x1="2.58318125" y1="6.39241875" x2="3.34518125" y2="6.477" layer="21"/>
<rectangle x1="4.78281875" y1="6.39241875" x2="5.54481875" y2="6.477" layer="21"/>
<rectangle x1="7.32281875" y1="6.39241875" x2="8.17118125" y2="6.477" layer="21"/>
<rectangle x1="9.017" y1="6.39241875" x2="9.86281875" y2="6.477" layer="21"/>
<rectangle x1="10.541" y1="6.39241875" x2="12.065" y2="6.477" layer="21"/>
<rectangle x1="12.65681875" y1="6.39241875" x2="13.335" y2="6.477" layer="21"/>
<rectangle x1="13.67281875" y1="6.39241875" x2="15.113" y2="6.477" layer="21"/>
<rectangle x1="15.45081875" y1="6.39241875" x2="15.79118125" y2="6.477" layer="21"/>
<rectangle x1="16.21281875" y1="6.39241875" x2="19.939" y2="6.477" layer="21"/>
<rectangle x1="0.127" y1="6.477" x2="0.71881875" y2="6.56158125" layer="21"/>
<rectangle x1="2.58318125" y1="6.477" x2="3.25881875" y2="6.56158125" layer="21"/>
<rectangle x1="4.86918125" y1="6.477" x2="5.54481875" y2="6.56158125" layer="21"/>
<rectangle x1="7.493" y1="6.477" x2="8.17118125" y2="6.56158125" layer="21"/>
<rectangle x1="9.017" y1="6.477" x2="9.86281875" y2="6.56158125" layer="21"/>
<rectangle x1="10.45718125" y1="6.477" x2="12.14881875" y2="6.56158125" layer="21"/>
<rectangle x1="12.74318125" y1="6.477" x2="13.335" y2="6.56158125" layer="21"/>
<rectangle x1="13.67281875" y1="6.477" x2="15.113" y2="6.56158125" layer="21"/>
<rectangle x1="15.45081875" y1="6.477" x2="15.79118125" y2="6.56158125" layer="21"/>
<rectangle x1="16.21281875" y1="6.477" x2="19.939" y2="6.56158125" layer="21"/>
<rectangle x1="0.127" y1="6.56158125" x2="0.71881875" y2="6.64641875" layer="21"/>
<rectangle x1="2.49681875" y1="6.56158125" x2="3.175" y2="6.64641875" layer="21"/>
<rectangle x1="4.953" y1="6.56158125" x2="5.63118125" y2="6.64641875" layer="21"/>
<rectangle x1="7.493" y1="6.56158125" x2="8.255" y2="6.64641875" layer="21"/>
<rectangle x1="9.017" y1="6.56158125" x2="9.779" y2="6.64641875" layer="21"/>
<rectangle x1="10.37081875" y1="6.56158125" x2="12.23518125" y2="6.64641875" layer="21"/>
<rectangle x1="12.74318125" y1="6.56158125" x2="13.335" y2="6.64641875" layer="21"/>
<rectangle x1="13.67281875" y1="6.56158125" x2="15.02918125" y2="6.64641875" layer="21"/>
<rectangle x1="15.45081875" y1="6.56158125" x2="15.79118125" y2="6.64641875" layer="21"/>
<rectangle x1="17.82318125" y1="6.56158125" x2="19.939" y2="6.64641875" layer="21"/>
<rectangle x1="0.127" y1="6.64641875" x2="0.71881875" y2="6.731" layer="21"/>
<rectangle x1="2.49681875" y1="6.64641875" x2="3.175" y2="6.731" layer="21"/>
<rectangle x1="4.953" y1="6.64641875" x2="5.63118125" y2="6.731" layer="21"/>
<rectangle x1="7.57681875" y1="6.64641875" x2="8.255" y2="6.731" layer="21"/>
<rectangle x1="9.017" y1="6.64641875" x2="9.779" y2="6.731" layer="21"/>
<rectangle x1="10.287" y1="6.64641875" x2="12.319" y2="6.731" layer="21"/>
<rectangle x1="12.827" y1="6.64641875" x2="13.335" y2="6.731" layer="21"/>
<rectangle x1="13.67281875" y1="6.64641875" x2="15.02918125" y2="6.731" layer="21"/>
<rectangle x1="15.45081875" y1="6.64641875" x2="15.70481875" y2="6.731" layer="21"/>
<rectangle x1="17.907" y1="6.64641875" x2="19.939" y2="6.731" layer="21"/>
<rectangle x1="0.04318125" y1="6.731" x2="0.71881875" y2="6.81558125" layer="21"/>
<rectangle x1="2.49681875" y1="6.731" x2="3.09118125" y2="6.81558125" layer="21"/>
<rectangle x1="5.03681875" y1="6.731" x2="5.715" y2="6.81558125" layer="21"/>
<rectangle x1="7.66318125" y1="6.731" x2="8.255" y2="6.81558125" layer="21"/>
<rectangle x1="9.017" y1="6.731" x2="9.779" y2="6.81558125" layer="21"/>
<rectangle x1="10.287" y1="6.731" x2="12.319" y2="6.81558125" layer="21"/>
<rectangle x1="12.827" y1="6.731" x2="13.335" y2="6.81558125" layer="21"/>
<rectangle x1="13.67281875" y1="6.731" x2="15.113" y2="6.81558125" layer="21"/>
<rectangle x1="15.45081875" y1="6.731" x2="15.70481875" y2="6.81558125" layer="21"/>
<rectangle x1="17.907" y1="6.731" x2="19.939" y2="6.81558125" layer="21"/>
<rectangle x1="0.04318125" y1="6.81558125" x2="0.71881875" y2="6.90041875" layer="21"/>
<rectangle x1="2.413" y1="6.81558125" x2="3.09118125" y2="6.90041875" layer="21"/>
<rectangle x1="5.03681875" y1="6.81558125" x2="5.715" y2="6.90041875" layer="21"/>
<rectangle x1="7.66318125" y1="6.81558125" x2="8.33881875" y2="6.90041875" layer="21"/>
<rectangle x1="9.017" y1="6.81558125" x2="9.69518125" y2="6.90041875" layer="21"/>
<rectangle x1="10.20318125" y1="6.81558125" x2="12.319" y2="6.90041875" layer="21"/>
<rectangle x1="12.827" y1="6.81558125" x2="13.335" y2="6.90041875" layer="21"/>
<rectangle x1="13.67281875" y1="6.81558125" x2="15.02918125" y2="6.90041875" layer="21"/>
<rectangle x1="15.45081875" y1="6.81558125" x2="15.70481875" y2="6.90041875" layer="21"/>
<rectangle x1="17.907" y1="6.81558125" x2="19.939" y2="6.90041875" layer="21"/>
<rectangle x1="0.04318125" y1="6.90041875" x2="0.635" y2="6.985" layer="21"/>
<rectangle x1="2.413" y1="6.90041875" x2="3.00481875" y2="6.985" layer="21"/>
<rectangle x1="5.12318125" y1="6.90041875" x2="5.715" y2="6.985" layer="21"/>
<rectangle x1="7.66318125" y1="6.90041875" x2="8.33881875" y2="6.985" layer="21"/>
<rectangle x1="9.017" y1="6.90041875" x2="9.69518125" y2="6.985" layer="21"/>
<rectangle x1="10.20318125" y1="6.90041875" x2="12.40281875" y2="6.985" layer="21"/>
<rectangle x1="12.91081875" y1="6.90041875" x2="13.335" y2="6.985" layer="21"/>
<rectangle x1="13.67281875" y1="6.90041875" x2="15.02918125" y2="6.985" layer="21"/>
<rectangle x1="15.45081875" y1="6.90041875" x2="15.79118125" y2="6.985" layer="21"/>
<rectangle x1="16.21281875" y1="6.90041875" x2="17.399" y2="6.985" layer="21"/>
<rectangle x1="17.907" y1="6.90041875" x2="19.939" y2="6.985" layer="21"/>
<rectangle x1="0.04318125" y1="6.985" x2="0.635" y2="7.06958125" layer="21"/>
<rectangle x1="2.413" y1="6.985" x2="3.00481875" y2="7.06958125" layer="21"/>
<rectangle x1="5.12318125" y1="6.985" x2="5.715" y2="7.06958125" layer="21"/>
<rectangle x1="7.747" y1="6.985" x2="8.33881875" y2="7.06958125" layer="21"/>
<rectangle x1="9.017" y1="6.985" x2="9.69518125" y2="7.06958125" layer="21"/>
<rectangle x1="10.20318125" y1="6.985" x2="12.40281875" y2="7.06958125" layer="21"/>
<rectangle x1="12.91081875" y1="6.985" x2="13.335" y2="7.06958125" layer="21"/>
<rectangle x1="13.75918125" y1="6.985" x2="15.02918125" y2="7.06958125" layer="21"/>
<rectangle x1="15.45081875" y1="6.985" x2="15.79118125" y2="7.06958125" layer="21"/>
<rectangle x1="16.21281875" y1="6.985" x2="17.48281875" y2="7.06958125" layer="21"/>
<rectangle x1="17.82318125" y1="6.985" x2="19.939" y2="7.06958125" layer="21"/>
<rectangle x1="0.04318125" y1="7.06958125" x2="0.635" y2="7.15441875" layer="21"/>
<rectangle x1="2.413" y1="7.06958125" x2="3.00481875" y2="7.15441875" layer="21"/>
<rectangle x1="5.12318125" y1="7.06958125" x2="5.715" y2="7.15441875" layer="21"/>
<rectangle x1="7.747" y1="7.06958125" x2="8.33881875" y2="7.15441875" layer="21"/>
<rectangle x1="9.017" y1="7.06958125" x2="9.69518125" y2="7.15441875" layer="21"/>
<rectangle x1="10.20318125" y1="7.06958125" x2="12.40281875" y2="7.15441875" layer="21"/>
<rectangle x1="12.91081875" y1="7.06958125" x2="13.335" y2="7.15441875" layer="21"/>
<rectangle x1="13.75918125" y1="7.06958125" x2="14.94281875" y2="7.15441875" layer="21"/>
<rectangle x1="15.367" y1="7.06958125" x2="15.79118125" y2="7.15441875" layer="21"/>
<rectangle x1="16.21281875" y1="7.06958125" x2="17.399" y2="7.15441875" layer="21"/>
<rectangle x1="17.82318125" y1="7.06958125" x2="19.939" y2="7.15441875" layer="21"/>
<rectangle x1="0.04318125" y1="7.15441875" x2="0.635" y2="7.239" layer="21"/>
<rectangle x1="2.413" y1="7.15441875" x2="3.00481875" y2="7.239" layer="21"/>
<rectangle x1="5.12318125" y1="7.15441875" x2="5.715" y2="7.239" layer="21"/>
<rectangle x1="7.747" y1="7.15441875" x2="8.33881875" y2="7.239" layer="21"/>
<rectangle x1="9.017" y1="7.15441875" x2="9.69518125" y2="7.239" layer="21"/>
<rectangle x1="10.20318125" y1="7.15441875" x2="12.40281875" y2="7.239" layer="21"/>
<rectangle x1="12.91081875" y1="7.15441875" x2="13.335" y2="7.239" layer="21"/>
<rectangle x1="13.843" y1="7.15441875" x2="14.94281875" y2="7.239" layer="21"/>
<rectangle x1="15.367" y1="7.15441875" x2="15.875" y2="7.239" layer="21"/>
<rectangle x1="16.29918125" y1="7.15441875" x2="17.31518125" y2="7.239" layer="21"/>
<rectangle x1="17.82318125" y1="7.15441875" x2="19.939" y2="7.239" layer="21"/>
<rectangle x1="0.04318125" y1="7.239" x2="0.635" y2="7.32358125" layer="21"/>
<rectangle x1="2.413" y1="7.239" x2="3.00481875" y2="7.32358125" layer="21"/>
<rectangle x1="5.12318125" y1="7.239" x2="5.715" y2="7.32358125" layer="21"/>
<rectangle x1="7.747" y1="7.239" x2="8.33881875" y2="7.32358125" layer="21"/>
<rectangle x1="9.017" y1="7.239" x2="9.69518125" y2="7.32358125" layer="21"/>
<rectangle x1="10.20318125" y1="7.239" x2="12.40281875" y2="7.32358125" layer="21"/>
<rectangle x1="12.91081875" y1="7.239" x2="13.335" y2="7.32358125" layer="21"/>
<rectangle x1="13.92681875" y1="7.239" x2="14.859" y2="7.32358125" layer="21"/>
<rectangle x1="15.28318125" y1="7.239" x2="15.875" y2="7.32358125" layer="21"/>
<rectangle x1="16.383" y1="7.239" x2="17.22881875" y2="7.32358125" layer="21"/>
<rectangle x1="17.73681875" y1="7.239" x2="19.939" y2="7.32358125" layer="21"/>
<rectangle x1="0.04318125" y1="7.32358125" x2="0.635" y2="7.40841875" layer="21"/>
<rectangle x1="2.413" y1="7.32358125" x2="3.00481875" y2="7.40841875" layer="21"/>
<rectangle x1="5.12318125" y1="7.32358125" x2="5.715" y2="7.40841875" layer="21"/>
<rectangle x1="7.747" y1="7.32358125" x2="8.33881875" y2="7.40841875" layer="21"/>
<rectangle x1="9.017" y1="7.32358125" x2="9.69518125" y2="7.40841875" layer="21"/>
<rectangle x1="10.20318125" y1="7.32358125" x2="12.40281875" y2="7.40841875" layer="21"/>
<rectangle x1="12.91081875" y1="7.32358125" x2="13.335" y2="7.40841875" layer="21"/>
<rectangle x1="14.01318125" y1="7.32358125" x2="14.68881875" y2="7.40841875" layer="21"/>
<rectangle x1="15.28318125" y1="7.32358125" x2="15.95881875" y2="7.40841875" layer="21"/>
<rectangle x1="16.46681875" y1="7.32358125" x2="17.145" y2="7.40841875" layer="21"/>
<rectangle x1="17.73681875" y1="7.32358125" x2="19.939" y2="7.40841875" layer="21"/>
<rectangle x1="0.04318125" y1="7.40841875" x2="0.635" y2="7.493" layer="21"/>
<rectangle x1="2.413" y1="7.40841875" x2="3.00481875" y2="7.493" layer="21"/>
<rectangle x1="5.12318125" y1="7.40841875" x2="5.715" y2="7.493" layer="21"/>
<rectangle x1="7.747" y1="7.40841875" x2="8.33881875" y2="7.493" layer="21"/>
<rectangle x1="9.017" y1="7.40841875" x2="9.69518125" y2="7.493" layer="21"/>
<rectangle x1="10.20318125" y1="7.40841875" x2="12.40281875" y2="7.493" layer="21"/>
<rectangle x1="12.91081875" y1="7.40841875" x2="13.335" y2="7.493" layer="21"/>
<rectangle x1="14.26718125" y1="7.40841875" x2="14.43481875" y2="7.493" layer="21"/>
<rectangle x1="15.19681875" y1="7.40841875" x2="15.95881875" y2="7.493" layer="21"/>
<rectangle x1="16.72081875" y1="7.40841875" x2="16.891" y2="7.493" layer="21"/>
<rectangle x1="17.653" y1="7.40841875" x2="19.939" y2="7.493" layer="21"/>
<rectangle x1="0.04318125" y1="7.493" x2="0.635" y2="7.57758125" layer="21"/>
<rectangle x1="2.413" y1="7.493" x2="3.00481875" y2="7.57758125" layer="21"/>
<rectangle x1="5.12318125" y1="7.493" x2="5.715" y2="7.57758125" layer="21"/>
<rectangle x1="7.66318125" y1="7.493" x2="8.33881875" y2="7.57758125" layer="21"/>
<rectangle x1="9.017" y1="7.493" x2="9.69518125" y2="7.57758125" layer="21"/>
<rectangle x1="10.20318125" y1="7.493" x2="12.40281875" y2="7.57758125" layer="21"/>
<rectangle x1="12.91081875" y1="7.493" x2="13.335" y2="7.57758125" layer="21"/>
<rectangle x1="15.113" y1="7.493" x2="16.04518125" y2="7.57758125" layer="21"/>
<rectangle x1="17.56918125" y1="7.493" x2="19.939" y2="7.57758125" layer="21"/>
<rectangle x1="0.04318125" y1="7.57758125" x2="0.71881875" y2="7.66241875" layer="21"/>
<rectangle x1="2.413" y1="7.57758125" x2="3.00481875" y2="7.66241875" layer="21"/>
<rectangle x1="5.12318125" y1="7.57758125" x2="5.715" y2="7.66241875" layer="21"/>
<rectangle x1="7.66318125" y1="7.57758125" x2="8.33881875" y2="7.66241875" layer="21"/>
<rectangle x1="9.017" y1="7.57758125" x2="9.69518125" y2="7.66241875" layer="21"/>
<rectangle x1="10.20318125" y1="7.57758125" x2="12.40281875" y2="7.66241875" layer="21"/>
<rectangle x1="12.91081875" y1="7.57758125" x2="13.335" y2="7.66241875" layer="21"/>
<rectangle x1="15.02918125" y1="7.57758125" x2="16.21281875" y2="7.66241875" layer="21"/>
<rectangle x1="17.48281875" y1="7.57758125" x2="19.939" y2="7.66241875" layer="21"/>
<rectangle x1="0.04318125" y1="7.66241875" x2="0.71881875" y2="7.747" layer="21"/>
<rectangle x1="2.413" y1="7.66241875" x2="3.00481875" y2="7.747" layer="21"/>
<rectangle x1="5.12318125" y1="7.66241875" x2="5.715" y2="7.747" layer="21"/>
<rectangle x1="7.66318125" y1="7.66241875" x2="8.255" y2="7.747" layer="21"/>
<rectangle x1="9.017" y1="7.66241875" x2="9.779" y2="7.747" layer="21"/>
<rectangle x1="10.287" y1="7.66241875" x2="12.319" y2="7.747" layer="21"/>
<rectangle x1="12.827" y1="7.66241875" x2="13.335" y2="7.747" layer="21"/>
<rectangle x1="13.67281875" y1="7.66241875" x2="13.843" y2="7.747" layer="21"/>
<rectangle x1="14.859" y1="7.66241875" x2="16.29918125" y2="7.747" layer="21"/>
<rectangle x1="17.31518125" y1="7.66241875" x2="19.939" y2="7.747" layer="21"/>
<rectangle x1="0.127" y1="7.747" x2="0.71881875" y2="7.83158125" layer="21"/>
<rectangle x1="2.413" y1="7.747" x2="3.00481875" y2="7.83158125" layer="21"/>
<rectangle x1="5.12318125" y1="7.747" x2="5.715" y2="7.83158125" layer="21"/>
<rectangle x1="7.57681875" y1="7.747" x2="8.255" y2="7.83158125" layer="21"/>
<rectangle x1="9.017" y1="7.747" x2="9.779" y2="7.83158125" layer="21"/>
<rectangle x1="10.287" y1="7.747" x2="12.319" y2="7.83158125" layer="21"/>
<rectangle x1="12.827" y1="7.747" x2="13.335" y2="7.83158125" layer="21"/>
<rectangle x1="13.589" y1="7.747" x2="14.097" y2="7.83158125" layer="21"/>
<rectangle x1="14.68881875" y1="7.747" x2="16.55318125" y2="7.83158125" layer="21"/>
<rectangle x1="17.06118125" y1="7.747" x2="19.939" y2="7.83158125" layer="21"/>
<rectangle x1="0.127" y1="7.83158125" x2="0.71881875" y2="7.91641875" layer="21"/>
<rectangle x1="2.413" y1="7.83158125" x2="3.00481875" y2="7.91641875" layer="21"/>
<rectangle x1="5.12318125" y1="7.83158125" x2="5.715" y2="7.91641875" layer="21"/>
<rectangle x1="7.57681875" y1="7.83158125" x2="8.255" y2="7.91641875" layer="21"/>
<rectangle x1="9.017" y1="7.83158125" x2="9.779" y2="7.91641875" layer="21"/>
<rectangle x1="10.37081875" y1="7.83158125" x2="12.23518125" y2="7.91641875" layer="21"/>
<rectangle x1="12.827" y1="7.83158125" x2="19.939" y2="7.91641875" layer="21"/>
<rectangle x1="0.127" y1="7.91641875" x2="0.71881875" y2="8.001" layer="21"/>
<rectangle x1="2.413" y1="7.91641875" x2="3.00481875" y2="8.001" layer="21"/>
<rectangle x1="5.12318125" y1="7.91641875" x2="5.715" y2="8.001" layer="21"/>
<rectangle x1="7.493" y1="7.91641875" x2="8.17118125" y2="8.001" layer="21"/>
<rectangle x1="9.017" y1="7.91641875" x2="9.86281875" y2="8.001" layer="21"/>
<rectangle x1="10.45718125" y1="7.91641875" x2="12.14881875" y2="8.001" layer="21"/>
<rectangle x1="12.74318125" y1="7.91641875" x2="19.939" y2="8.001" layer="21"/>
<rectangle x1="0.127" y1="8.001" x2="0.71881875" y2="8.08558125" layer="21"/>
<rectangle x1="2.413" y1="8.001" x2="3.00481875" y2="8.08558125" layer="21"/>
<rectangle x1="5.12318125" y1="8.001" x2="5.715" y2="8.08558125" layer="21"/>
<rectangle x1="7.40918125" y1="8.001" x2="8.17118125" y2="8.08558125" layer="21"/>
<rectangle x1="9.017" y1="8.001" x2="9.86281875" y2="8.08558125" layer="21"/>
<rectangle x1="10.45718125" y1="8.001" x2="12.065" y2="8.08558125" layer="21"/>
<rectangle x1="12.74318125" y1="8.001" x2="19.939" y2="8.08558125" layer="21"/>
<rectangle x1="0.127" y1="8.08558125" x2="0.80518125" y2="8.17041875" layer="21"/>
<rectangle x1="2.413" y1="8.08558125" x2="3.00481875" y2="8.17041875" layer="21"/>
<rectangle x1="5.12318125" y1="8.08558125" x2="5.715" y2="8.17041875" layer="21"/>
<rectangle x1="7.32281875" y1="8.08558125" x2="8.08481875" y2="8.17041875" layer="21"/>
<rectangle x1="9.017" y1="8.08558125" x2="9.94918125" y2="8.17041875" layer="21"/>
<rectangle x1="10.62481875" y1="8.08558125" x2="11.98118125" y2="8.17041875" layer="21"/>
<rectangle x1="12.65681875" y1="8.08558125" x2="19.939" y2="8.17041875" layer="21"/>
<rectangle x1="0.127" y1="8.17041875" x2="0.80518125" y2="8.255" layer="21"/>
<rectangle x1="2.413" y1="8.17041875" x2="3.00481875" y2="8.255" layer="21"/>
<rectangle x1="5.12318125" y1="8.17041875" x2="5.715" y2="8.255" layer="21"/>
<rectangle x1="7.15518125" y1="8.17041875" x2="8.001" y2="8.255" layer="21"/>
<rectangle x1="9.017" y1="8.17041875" x2="9.94918125" y2="8.255" layer="21"/>
<rectangle x1="10.71118125" y1="8.17041875" x2="11.89481875" y2="8.255" layer="21"/>
<rectangle x1="12.573" y1="8.17041875" x2="19.939" y2="8.255" layer="21"/>
<rectangle x1="0.21081875" y1="8.255" x2="0.80518125" y2="8.33958125" layer="21"/>
<rectangle x1="2.413" y1="8.255" x2="3.00481875" y2="8.33958125" layer="21"/>
<rectangle x1="5.12318125" y1="8.255" x2="5.715" y2="8.33958125" layer="21"/>
<rectangle x1="6.90118125" y1="8.255" x2="8.001" y2="8.33958125" layer="21"/>
<rectangle x1="9.017" y1="8.255" x2="10.033" y2="8.33958125" layer="21"/>
<rectangle x1="10.87881875" y1="8.255" x2="11.72718125" y2="8.33958125" layer="21"/>
<rectangle x1="12.573" y1="8.255" x2="19.939" y2="8.33958125" layer="21"/>
<rectangle x1="0.21081875" y1="8.33958125" x2="0.889" y2="8.42441875" layer="21"/>
<rectangle x1="2.413" y1="8.33958125" x2="3.00481875" y2="8.42441875" layer="21"/>
<rectangle x1="5.12318125" y1="8.33958125" x2="7.91718125" y2="8.42441875" layer="21"/>
<rectangle x1="9.10081875" y1="8.33958125" x2="10.11681875" y2="8.42441875" layer="21"/>
<rectangle x1="11.21918125" y1="8.33958125" x2="11.38681875" y2="8.42441875" layer="21"/>
<rectangle x1="12.48918125" y1="8.33958125" x2="19.85518125" y2="8.42441875" layer="21"/>
<rectangle x1="0.21081875" y1="8.42441875" x2="0.889" y2="8.509" layer="21"/>
<rectangle x1="2.413" y1="8.42441875" x2="3.00481875" y2="8.509" layer="21"/>
<rectangle x1="5.12318125" y1="8.42441875" x2="7.83081875" y2="8.509" layer="21"/>
<rectangle x1="9.10081875" y1="8.42441875" x2="10.20318125" y2="8.509" layer="21"/>
<rectangle x1="12.40281875" y1="8.42441875" x2="19.85518125" y2="8.509" layer="21"/>
<rectangle x1="0.29718125" y1="8.509" x2="0.889" y2="8.59358125" layer="21"/>
<rectangle x1="2.413" y1="8.509" x2="3.00481875" y2="8.59358125" layer="21"/>
<rectangle x1="5.12318125" y1="8.509" x2="7.747" y2="8.59358125" layer="21"/>
<rectangle x1="9.10081875" y1="8.509" x2="10.287" y2="8.59358125" layer="21"/>
<rectangle x1="12.23518125" y1="8.509" x2="19.85518125" y2="8.59358125" layer="21"/>
<rectangle x1="0.29718125" y1="8.59358125" x2="0.97281875" y2="8.67841875" layer="21"/>
<rectangle x1="2.413" y1="8.59358125" x2="3.00481875" y2="8.67841875" layer="21"/>
<rectangle x1="5.12318125" y1="8.59358125" x2="7.57681875" y2="8.67841875" layer="21"/>
<rectangle x1="9.10081875" y1="8.59358125" x2="10.45718125" y2="8.67841875" layer="21"/>
<rectangle x1="12.14881875" y1="8.59358125" x2="19.76881875" y2="8.67841875" layer="21"/>
<rectangle x1="0.29718125" y1="8.67841875" x2="0.97281875" y2="8.763" layer="21"/>
<rectangle x1="2.413" y1="8.67841875" x2="3.00481875" y2="8.763" layer="21"/>
<rectangle x1="5.12318125" y1="8.67841875" x2="7.493" y2="8.763" layer="21"/>
<rectangle x1="9.10081875" y1="8.67841875" x2="10.62481875" y2="8.763" layer="21"/>
<rectangle x1="11.98118125" y1="8.67841875" x2="19.76881875" y2="8.763" layer="21"/>
<rectangle x1="0.381" y1="8.763" x2="1.05918125" y2="8.84758125" layer="21"/>
<rectangle x1="2.413" y1="8.763" x2="2.921" y2="8.84758125" layer="21"/>
<rectangle x1="5.207" y1="8.763" x2="7.32281875" y2="8.84758125" layer="21"/>
<rectangle x1="9.10081875" y1="8.763" x2="10.795" y2="8.84758125" layer="21"/>
<rectangle x1="11.72718125" y1="8.763" x2="19.76881875" y2="8.84758125" layer="21"/>
<rectangle x1="0.381" y1="8.84758125" x2="1.05918125" y2="8.93241875" layer="21"/>
<rectangle x1="2.58318125" y1="8.84758125" x2="2.83718125" y2="8.93241875" layer="21"/>
<rectangle x1="5.29081875" y1="8.84758125" x2="7.06881875" y2="8.93241875" layer="21"/>
<rectangle x1="9.18718125" y1="8.84758125" x2="19.685" y2="8.93241875" layer="21"/>
<rectangle x1="0.46481875" y1="8.93241875" x2="1.143" y2="9.017" layer="21"/>
<rectangle x1="9.18718125" y1="8.93241875" x2="19.685" y2="9.017" layer="21"/>
<rectangle x1="0.46481875" y1="9.017" x2="1.143" y2="9.10158125" layer="21"/>
<rectangle x1="9.18718125" y1="9.017" x2="19.60118125" y2="9.10158125" layer="21"/>
<rectangle x1="0.55118125" y1="9.10158125" x2="1.22681875" y2="9.18641875" layer="21"/>
<rectangle x1="9.18718125" y1="9.10158125" x2="19.60118125" y2="9.18641875" layer="21"/>
<rectangle x1="0.55118125" y1="9.18641875" x2="1.31318125" y2="9.271" layer="21"/>
<rectangle x1="9.18718125" y1="9.18641875" x2="19.51481875" y2="9.271" layer="21"/>
<rectangle x1="0.635" y1="9.271" x2="1.397" y2="9.35558125" layer="21"/>
<rectangle x1="9.271" y1="9.271" x2="19.51481875" y2="9.35558125" layer="21"/>
<rectangle x1="0.71881875" y1="9.35558125" x2="1.397" y2="9.44041875" layer="21"/>
<rectangle x1="9.271" y1="9.35558125" x2="19.431" y2="9.44041875" layer="21"/>
<rectangle x1="0.71881875" y1="9.44041875" x2="1.48081875" y2="9.525" layer="21"/>
<rectangle x1="9.271" y1="9.44041875" x2="19.34718125" y2="9.525" layer="21"/>
<rectangle x1="0.80518125" y1="9.525" x2="1.56718125" y2="9.60958125" layer="21"/>
<rectangle x1="9.35481875" y1="9.525" x2="19.34718125" y2="9.60958125" layer="21"/>
<rectangle x1="0.889" y1="9.60958125" x2="1.651" y2="9.69441875" layer="21"/>
<rectangle x1="9.35481875" y1="9.60958125" x2="19.26081875" y2="9.69441875" layer="21"/>
<rectangle x1="0.889" y1="9.69441875" x2="1.73481875" y2="9.779" layer="21"/>
<rectangle x1="9.35481875" y1="9.69441875" x2="19.177" y2="9.779" layer="21"/>
<rectangle x1="0.97281875" y1="9.779" x2="1.82118125" y2="9.86358125" layer="21"/>
<rectangle x1="9.44118125" y1="9.779" x2="19.09318125" y2="9.86358125" layer="21"/>
<rectangle x1="1.05918125" y1="9.86358125" x2="1.905" y2="9.94841875" layer="21"/>
<rectangle x1="9.44118125" y1="9.86358125" x2="19.00681875" y2="9.94841875" layer="21"/>
<rectangle x1="1.143" y1="9.94841875" x2="2.07518125" y2="10.033" layer="21"/>
<rectangle x1="9.44118125" y1="9.94841875" x2="18.923" y2="10.033" layer="21"/>
<rectangle x1="1.22681875" y1="10.033" x2="2.159" y2="10.11758125" layer="21"/>
<rectangle x1="9.525" y1="10.033" x2="18.923" y2="10.11758125" layer="21"/>
<rectangle x1="1.31318125" y1="10.11758125" x2="2.24281875" y2="10.20241875" layer="21"/>
<rectangle x1="9.525" y1="10.11758125" x2="18.75281875" y2="10.20241875" layer="21"/>
<rectangle x1="1.397" y1="10.20241875" x2="2.413" y2="10.287" layer="21"/>
<rectangle x1="9.60881875" y1="10.20241875" x2="18.669" y2="10.287" layer="21"/>
<rectangle x1="1.48081875" y1="10.287" x2="2.58318125" y2="10.37158125" layer="21"/>
<rectangle x1="9.60881875" y1="10.287" x2="18.58518125" y2="10.37158125" layer="21"/>
<rectangle x1="1.56718125" y1="10.37158125" x2="2.75081875" y2="10.45641875" layer="21"/>
<rectangle x1="9.69518125" y1="10.37158125" x2="18.49881875" y2="10.45641875" layer="21"/>
<rectangle x1="1.73481875" y1="10.45641875" x2="3.00481875" y2="10.541" layer="21"/>
<rectangle x1="9.69518125" y1="10.45641875" x2="18.415" y2="10.541" layer="21"/>
<rectangle x1="1.82118125" y1="10.541" x2="3.34518125" y2="10.62558125" layer="21"/>
<rectangle x1="9.779" y1="10.541" x2="18.24481875" y2="10.62558125" layer="21"/>
<rectangle x1="1.98881875" y1="10.62558125" x2="9.69518125" y2="10.71041875" layer="21"/>
<rectangle x1="9.779" y1="10.62558125" x2="18.161" y2="10.71041875" layer="21"/>
<rectangle x1="2.07518125" y1="10.71041875" x2="17.99081875" y2="10.795" layer="21"/>
<rectangle x1="2.24281875" y1="10.795" x2="17.82318125" y2="10.87958125" layer="21"/>
<rectangle x1="2.413" y1="10.87958125" x2="17.653" y2="10.96441875" layer="21"/>
<rectangle x1="2.58318125" y1="10.96441875" x2="17.48281875" y2="11.049" layer="21"/>
<rectangle x1="2.83718125" y1="11.049" x2="17.22881875" y2="11.13358125" layer="21"/>
<rectangle x1="3.175" y1="11.13358125" x2="16.891" y2="11.21841875" layer="21"/>
</package>
<package name="UDO-LOGO-30MM">
<rectangle x1="4.36118125" y1="0.55041875" x2="5.29081875" y2="0.635" layer="21"/>
<rectangle x1="21.29281875" y1="0.55041875" x2="22.30881875" y2="0.635" layer="21"/>
<rectangle x1="4.191" y1="0.635" x2="5.54481875" y2="0.71958125" layer="21"/>
<rectangle x1="6.731" y1="0.635" x2="6.81481875" y2="0.71958125" layer="21"/>
<rectangle x1="8.42518125" y1="0.635" x2="8.509" y2="0.71958125" layer="21"/>
<rectangle x1="8.93318125" y1="0.635" x2="9.10081875" y2="0.71958125" layer="21"/>
<rectangle x1="10.541" y1="0.635" x2="10.795" y2="0.71958125" layer="21"/>
<rectangle x1="11.38681875" y1="0.635" x2="12.65681875" y2="0.71958125" layer="21"/>
<rectangle x1="14.859" y1="0.635" x2="15.28318125" y2="0.71958125" layer="21"/>
<rectangle x1="16.80718125" y1="0.635" x2="17.145" y2="0.71958125" layer="21"/>
<rectangle x1="18.669" y1="0.635" x2="19.09318125" y2="0.71958125" layer="21"/>
<rectangle x1="21.12518125" y1="0.635" x2="22.479" y2="0.71958125" layer="21"/>
<rectangle x1="23.495" y1="0.635" x2="23.66518125" y2="0.71958125" layer="21"/>
<rectangle x1="25.273" y1="0.635" x2="25.35681875" y2="0.71958125" layer="21"/>
<rectangle x1="26.45918125" y1="0.635" x2="26.797" y2="0.71958125" layer="21"/>
<rectangle x1="4.10718125" y1="0.71958125" x2="5.715" y2="0.80441875" layer="21"/>
<rectangle x1="6.64718125" y1="0.71958125" x2="6.90118125" y2="0.80441875" layer="21"/>
<rectangle x1="8.33881875" y1="0.71958125" x2="8.59281875" y2="0.80441875" layer="21"/>
<rectangle x1="8.84681875" y1="0.71958125" x2="9.18718125" y2="0.80441875" layer="21"/>
<rectangle x1="10.287" y1="0.71958125" x2="10.87881875" y2="0.80441875" layer="21"/>
<rectangle x1="11.303" y1="0.71958125" x2="12.91081875" y2="0.80441875" layer="21"/>
<rectangle x1="14.605" y1="0.71958125" x2="15.53718125" y2="0.80441875" layer="21"/>
<rectangle x1="16.637" y1="0.71958125" x2="17.399" y2="0.80441875" layer="21"/>
<rectangle x1="18.415" y1="0.71958125" x2="19.34718125" y2="0.80441875" layer="21"/>
<rectangle x1="20.955" y1="0.71958125" x2="22.64918125" y2="0.80441875" layer="21"/>
<rectangle x1="23.495" y1="0.71958125" x2="23.749" y2="0.80441875" layer="21"/>
<rectangle x1="25.18918125" y1="0.71958125" x2="25.44318125" y2="0.80441875" layer="21"/>
<rectangle x1="26.20518125" y1="0.71958125" x2="27.13481875" y2="0.80441875" layer="21"/>
<rectangle x1="3.937" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="5.12318125" y1="0.80441875" x2="5.79881875" y2="0.889" layer="21"/>
<rectangle x1="6.64718125" y1="0.80441875" x2="6.90118125" y2="0.889" layer="21"/>
<rectangle x1="8.33881875" y1="0.80441875" x2="8.59281875" y2="0.889" layer="21"/>
<rectangle x1="8.84681875" y1="0.80441875" x2="9.18718125" y2="0.889" layer="21"/>
<rectangle x1="10.11681875" y1="0.80441875" x2="10.87881875" y2="0.889" layer="21"/>
<rectangle x1="11.303" y1="0.80441875" x2="13.081" y2="0.889" layer="21"/>
<rectangle x1="14.52118125" y1="0.80441875" x2="15.621" y2="0.889" layer="21"/>
<rectangle x1="16.46681875" y1="0.80441875" x2="17.48281875" y2="0.889" layer="21"/>
<rectangle x1="18.33118125" y1="0.80441875" x2="19.431" y2="0.889" layer="21"/>
<rectangle x1="20.87118125" y1="0.80441875" x2="21.54681875" y2="0.889" layer="21"/>
<rectangle x1="22.05481875" y1="0.80441875" x2="22.733" y2="0.889" layer="21"/>
<rectangle x1="23.495" y1="0.80441875" x2="23.749" y2="0.889" layer="21"/>
<rectangle x1="25.18918125" y1="0.80441875" x2="25.44318125" y2="0.889" layer="21"/>
<rectangle x1="26.035" y1="0.80441875" x2="27.22118125" y2="0.889" layer="21"/>
<rectangle x1="3.85318125" y1="0.889" x2="4.36118125" y2="0.97358125" layer="21"/>
<rectangle x1="5.37718125" y1="0.889" x2="5.88518125" y2="0.97358125" layer="21"/>
<rectangle x1="6.64718125" y1="0.889" x2="6.90118125" y2="0.97358125" layer="21"/>
<rectangle x1="8.33881875" y1="0.889" x2="8.59281875" y2="0.97358125" layer="21"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.18718125" y2="0.97358125" layer="21"/>
<rectangle x1="10.033" y1="0.889" x2="10.795" y2="0.97358125" layer="21"/>
<rectangle x1="11.303" y1="0.889" x2="13.25118125" y2="0.97358125" layer="21"/>
<rectangle x1="14.351" y1="0.889" x2="15.79118125" y2="0.97358125" layer="21"/>
<rectangle x1="16.383" y1="0.889" x2="17.56918125" y2="0.97358125" layer="21"/>
<rectangle x1="18.24481875" y1="0.889" x2="19.60118125" y2="0.97358125" layer="21"/>
<rectangle x1="20.78481875" y1="0.889" x2="21.29281875" y2="0.97358125" layer="21"/>
<rectangle x1="22.30881875" y1="0.889" x2="22.81681875" y2="0.97358125" layer="21"/>
<rectangle x1="23.495" y1="0.889" x2="23.749" y2="0.97358125" layer="21"/>
<rectangle x1="25.18918125" y1="0.889" x2="25.44318125" y2="0.97358125" layer="21"/>
<rectangle x1="25.95118125" y1="0.889" x2="27.305" y2="0.97358125" layer="21"/>
<rectangle x1="3.76681875" y1="0.97358125" x2="4.191" y2="1.05841875" layer="21"/>
<rectangle x1="5.54481875" y1="0.97358125" x2="5.969" y2="1.05841875" layer="21"/>
<rectangle x1="6.64718125" y1="0.97358125" x2="6.90118125" y2="1.05841875" layer="21"/>
<rectangle x1="8.33881875" y1="0.97358125" x2="8.59281875" y2="1.05841875" layer="21"/>
<rectangle x1="8.84681875" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="21"/>
<rectangle x1="9.94918125" y1="0.97358125" x2="10.45718125" y2="1.05841875" layer="21"/>
<rectangle x1="11.303" y1="0.97358125" x2="11.557" y2="1.05841875" layer="21"/>
<rectangle x1="12.827" y1="0.97358125" x2="13.335" y2="1.05841875" layer="21"/>
<rectangle x1="14.351" y1="0.97358125" x2="14.859" y2="1.05841875" layer="21"/>
<rectangle x1="15.367" y1="0.97358125" x2="15.875" y2="1.05841875" layer="21"/>
<rectangle x1="16.383" y1="0.97358125" x2="16.80718125" y2="1.05841875" layer="21"/>
<rectangle x1="17.22881875" y1="0.97358125" x2="17.653" y2="1.05841875" layer="21"/>
<rectangle x1="18.161" y1="0.97358125" x2="18.58518125" y2="1.05841875" layer="21"/>
<rectangle x1="19.177" y1="0.97358125" x2="19.685" y2="1.05841875" layer="21"/>
<rectangle x1="20.701" y1="0.97358125" x2="21.12518125" y2="1.05841875" layer="21"/>
<rectangle x1="22.479" y1="0.97358125" x2="22.90318125" y2="1.05841875" layer="21"/>
<rectangle x1="23.495" y1="0.97358125" x2="23.749" y2="1.05841875" layer="21"/>
<rectangle x1="25.18918125" y1="0.97358125" x2="25.44318125" y2="1.05841875" layer="21"/>
<rectangle x1="25.86481875" y1="0.97358125" x2="26.37281875" y2="1.05841875" layer="21"/>
<rectangle x1="26.96718125" y1="0.97358125" x2="27.38881875" y2="1.05841875" layer="21"/>
<rectangle x1="3.683" y1="1.05841875" x2="4.10718125" y2="1.143" layer="21"/>
<rectangle x1="5.63118125" y1="1.05841875" x2="6.05281875" y2="1.143" layer="21"/>
<rectangle x1="6.64718125" y1="1.05841875" x2="6.90118125" y2="1.143" layer="21"/>
<rectangle x1="8.33881875" y1="1.05841875" x2="8.59281875" y2="1.143" layer="21"/>
<rectangle x1="8.84681875" y1="1.05841875" x2="9.18718125" y2="1.143" layer="21"/>
<rectangle x1="9.86281875" y1="1.05841875" x2="10.287" y2="1.143" layer="21"/>
<rectangle x1="11.303" y1="1.05841875" x2="11.557" y2="1.143" layer="21"/>
<rectangle x1="12.99718125" y1="1.05841875" x2="13.41881875" y2="1.143" layer="21"/>
<rectangle x1="14.26718125" y1="1.05841875" x2="14.68881875" y2="1.143" layer="21"/>
<rectangle x1="15.53718125" y1="1.05841875" x2="15.875" y2="1.143" layer="21"/>
<rectangle x1="16.29918125" y1="1.05841875" x2="16.637" y2="1.143" layer="21"/>
<rectangle x1="17.399" y1="1.05841875" x2="17.653" y2="1.143" layer="21"/>
<rectangle x1="18.07718125" y1="1.05841875" x2="18.49881875" y2="1.143" layer="21"/>
<rectangle x1="19.34718125" y1="1.05841875" x2="19.685" y2="1.143" layer="21"/>
<rectangle x1="20.61718125" y1="1.05841875" x2="21.03881875" y2="1.143" layer="21"/>
<rectangle x1="22.56281875" y1="1.05841875" x2="22.987" y2="1.143" layer="21"/>
<rectangle x1="23.495" y1="1.05841875" x2="23.749" y2="1.143" layer="21"/>
<rectangle x1="25.18918125" y1="1.05841875" x2="25.44318125" y2="1.143" layer="21"/>
<rectangle x1="25.86481875" y1="1.05841875" x2="26.20518125" y2="1.143" layer="21"/>
<rectangle x1="27.051" y1="1.05841875" x2="27.47518125" y2="1.143" layer="21"/>
<rectangle x1="3.683" y1="1.143" x2="4.02081875" y2="1.22758125" layer="21"/>
<rectangle x1="5.715" y1="1.143" x2="6.13918125" y2="1.22758125" layer="21"/>
<rectangle x1="6.64718125" y1="1.143" x2="6.90118125" y2="1.22758125" layer="21"/>
<rectangle x1="8.33881875" y1="1.143" x2="8.59281875" y2="1.22758125" layer="21"/>
<rectangle x1="8.84681875" y1="1.143" x2="9.18718125" y2="1.22758125" layer="21"/>
<rectangle x1="9.779" y1="1.143" x2="10.20318125" y2="1.22758125" layer="21"/>
<rectangle x1="11.303" y1="1.143" x2="11.557" y2="1.22758125" layer="21"/>
<rectangle x1="13.081" y1="1.143" x2="13.50518125" y2="1.22758125" layer="21"/>
<rectangle x1="14.18081875" y1="1.143" x2="14.52118125" y2="1.22758125" layer="21"/>
<rectangle x1="15.621" y1="1.143" x2="15.95881875" y2="1.22758125" layer="21"/>
<rectangle x1="16.383" y1="1.143" x2="16.637" y2="1.22758125" layer="21"/>
<rectangle x1="17.399" y1="1.143" x2="17.73681875" y2="1.22758125" layer="21"/>
<rectangle x1="17.99081875" y1="1.143" x2="18.33118125" y2="1.22758125" layer="21"/>
<rectangle x1="19.431" y1="1.143" x2="19.76881875" y2="1.22758125" layer="21"/>
<rectangle x1="20.61718125" y1="1.143" x2="20.955" y2="1.22758125" layer="21"/>
<rectangle x1="22.64918125" y1="1.143" x2="23.07081875" y2="1.22758125" layer="21"/>
<rectangle x1="23.495" y1="1.143" x2="23.749" y2="1.22758125" layer="21"/>
<rectangle x1="25.18918125" y1="1.143" x2="25.44318125" y2="1.22758125" layer="21"/>
<rectangle x1="25.781" y1="1.143" x2="26.11881875" y2="1.22758125" layer="21"/>
<rectangle x1="27.22118125" y1="1.143" x2="27.559" y2="1.22758125" layer="21"/>
<rectangle x1="3.59918125" y1="1.22758125" x2="3.937" y2="1.31241875" layer="21"/>
<rectangle x1="5.79881875" y1="1.22758125" x2="6.13918125" y2="1.31241875" layer="21"/>
<rectangle x1="6.64718125" y1="1.22758125" x2="6.90118125" y2="1.31241875" layer="21"/>
<rectangle x1="8.33881875" y1="1.22758125" x2="8.59281875" y2="1.31241875" layer="21"/>
<rectangle x1="8.84681875" y1="1.22758125" x2="9.18718125" y2="1.31241875" layer="21"/>
<rectangle x1="9.779" y1="1.22758125" x2="10.11681875" y2="1.31241875" layer="21"/>
<rectangle x1="11.303" y1="1.22758125" x2="11.557" y2="1.31241875" layer="21"/>
<rectangle x1="13.25118125" y1="1.22758125" x2="13.589" y2="1.31241875" layer="21"/>
<rectangle x1="14.18081875" y1="1.22758125" x2="14.52118125" y2="1.31241875" layer="21"/>
<rectangle x1="15.70481875" y1="1.22758125" x2="15.95881875" y2="1.31241875" layer="21"/>
<rectangle x1="17.399" y1="1.22758125" x2="17.653" y2="1.31241875" layer="21"/>
<rectangle x1="17.99081875" y1="1.22758125" x2="18.33118125" y2="1.31241875" layer="21"/>
<rectangle x1="19.51481875" y1="1.22758125" x2="19.76881875" y2="1.31241875" layer="21"/>
<rectangle x1="20.53081875" y1="1.22758125" x2="20.87118125" y2="1.31241875" layer="21"/>
<rectangle x1="22.733" y1="1.22758125" x2="23.07081875" y2="1.31241875" layer="21"/>
<rectangle x1="23.495" y1="1.22758125" x2="23.749" y2="1.31241875" layer="21"/>
<rectangle x1="25.18918125" y1="1.22758125" x2="25.44318125" y2="1.31241875" layer="21"/>
<rectangle x1="25.69718125" y1="1.22758125" x2="26.035" y2="1.31241875" layer="21"/>
<rectangle x1="27.22118125" y1="1.22758125" x2="27.559" y2="1.31241875" layer="21"/>
<rectangle x1="3.51281875" y1="1.31241875" x2="3.85318125" y2="1.397" layer="21"/>
<rectangle x1="5.88518125" y1="1.31241875" x2="6.223" y2="1.397" layer="21"/>
<rectangle x1="6.64718125" y1="1.31241875" x2="6.90118125" y2="1.397" layer="21"/>
<rectangle x1="8.33881875" y1="1.31241875" x2="8.59281875" y2="1.397" layer="21"/>
<rectangle x1="8.84681875" y1="1.31241875" x2="9.18718125" y2="1.397" layer="21"/>
<rectangle x1="9.779" y1="1.31241875" x2="10.033" y2="1.397" layer="21"/>
<rectangle x1="11.303" y1="1.31241875" x2="11.557" y2="1.397" layer="21"/>
<rectangle x1="13.335" y1="1.31241875" x2="13.67281875" y2="1.397" layer="21"/>
<rectangle x1="14.097" y1="1.31241875" x2="14.43481875" y2="1.397" layer="21"/>
<rectangle x1="15.70481875" y1="1.31241875" x2="16.04518125" y2="1.397" layer="21"/>
<rectangle x1="17.31518125" y1="1.31241875" x2="17.653" y2="1.397" layer="21"/>
<rectangle x1="17.907" y1="1.31241875" x2="18.24481875" y2="1.397" layer="21"/>
<rectangle x1="19.51481875" y1="1.31241875" x2="19.76881875" y2="1.397" layer="21"/>
<rectangle x1="20.447" y1="1.31241875" x2="20.78481875" y2="1.397" layer="21"/>
<rectangle x1="22.81681875" y1="1.31241875" x2="23.15718125" y2="1.397" layer="21"/>
<rectangle x1="23.495" y1="1.31241875" x2="23.749" y2="1.397" layer="21"/>
<rectangle x1="25.18918125" y1="1.31241875" x2="25.44318125" y2="1.397" layer="21"/>
<rectangle x1="25.69718125" y1="1.31241875" x2="26.035" y2="1.397" layer="21"/>
<rectangle x1="27.305" y1="1.31241875" x2="27.47518125" y2="1.397" layer="21"/>
<rectangle x1="3.51281875" y1="1.397" x2="3.85318125" y2="1.48158125" layer="21"/>
<rectangle x1="5.88518125" y1="1.397" x2="6.223" y2="1.48158125" layer="21"/>
<rectangle x1="6.64718125" y1="1.397" x2="6.90118125" y2="1.48158125" layer="21"/>
<rectangle x1="8.33881875" y1="1.397" x2="8.59281875" y2="1.48158125" layer="21"/>
<rectangle x1="8.84681875" y1="1.397" x2="9.18718125" y2="1.48158125" layer="21"/>
<rectangle x1="9.69518125" y1="1.397" x2="10.033" y2="1.48158125" layer="21"/>
<rectangle x1="11.303" y1="1.397" x2="11.557" y2="1.48158125" layer="21"/>
<rectangle x1="13.335" y1="1.397" x2="13.67281875" y2="1.48158125" layer="21"/>
<rectangle x1="14.097" y1="1.397" x2="14.43481875" y2="1.48158125" layer="21"/>
<rectangle x1="15.79118125" y1="1.397" x2="16.04518125" y2="1.48158125" layer="21"/>
<rectangle x1="17.22881875" y1="1.397" x2="17.653" y2="1.48158125" layer="21"/>
<rectangle x1="17.907" y1="1.397" x2="18.24481875" y2="1.48158125" layer="21"/>
<rectangle x1="20.447" y1="1.397" x2="20.78481875" y2="1.48158125" layer="21"/>
<rectangle x1="22.81681875" y1="1.397" x2="23.15718125" y2="1.48158125" layer="21"/>
<rectangle x1="23.495" y1="1.397" x2="23.749" y2="1.48158125" layer="21"/>
<rectangle x1="25.18918125" y1="1.397" x2="25.44318125" y2="1.48158125" layer="21"/>
<rectangle x1="25.69718125" y1="1.397" x2="25.95118125" y2="1.48158125" layer="21"/>
<rectangle x1="3.51281875" y1="1.48158125" x2="3.76681875" y2="1.56641875" layer="21"/>
<rectangle x1="5.969" y1="1.48158125" x2="6.30681875" y2="1.56641875" layer="21"/>
<rectangle x1="6.64718125" y1="1.48158125" x2="6.90118125" y2="1.56641875" layer="21"/>
<rectangle x1="8.33881875" y1="1.48158125" x2="8.59281875" y2="1.56641875" layer="21"/>
<rectangle x1="8.84681875" y1="1.48158125" x2="9.18718125" y2="1.56641875" layer="21"/>
<rectangle x1="9.69518125" y1="1.48158125" x2="10.033" y2="1.56641875" layer="21"/>
<rectangle x1="11.303" y1="1.48158125" x2="11.557" y2="1.56641875" layer="21"/>
<rectangle x1="13.41881875" y1="1.48158125" x2="13.75918125" y2="1.56641875" layer="21"/>
<rectangle x1="14.097" y1="1.48158125" x2="14.351" y2="1.56641875" layer="21"/>
<rectangle x1="15.79118125" y1="1.48158125" x2="16.04518125" y2="1.56641875" layer="21"/>
<rectangle x1="16.97481875" y1="1.48158125" x2="17.56918125" y2="1.56641875" layer="21"/>
<rectangle x1="17.907" y1="1.48158125" x2="19.76881875" y2="1.56641875" layer="21"/>
<rectangle x1="20.447" y1="1.48158125" x2="20.701" y2="1.56641875" layer="21"/>
<rectangle x1="22.90318125" y1="1.48158125" x2="23.241" y2="1.56641875" layer="21"/>
<rectangle x1="23.495" y1="1.48158125" x2="23.749" y2="1.56641875" layer="21"/>
<rectangle x1="25.18918125" y1="1.48158125" x2="25.44318125" y2="1.56641875" layer="21"/>
<rectangle x1="25.69718125" y1="1.48158125" x2="27.559" y2="1.56641875" layer="21"/>
<rectangle x1="3.429" y1="1.56641875" x2="3.76681875" y2="1.651" layer="21"/>
<rectangle x1="5.969" y1="1.56641875" x2="6.30681875" y2="1.651" layer="21"/>
<rectangle x1="6.64718125" y1="1.56641875" x2="6.90118125" y2="1.651" layer="21"/>
<rectangle x1="8.33881875" y1="1.56641875" x2="8.59281875" y2="1.651" layer="21"/>
<rectangle x1="8.84681875" y1="1.56641875" x2="9.18718125" y2="1.651" layer="21"/>
<rectangle x1="9.69518125" y1="1.56641875" x2="10.033" y2="1.651" layer="21"/>
<rectangle x1="11.303" y1="1.56641875" x2="11.557" y2="1.651" layer="21"/>
<rectangle x1="13.50518125" y1="1.56641875" x2="13.75918125" y2="1.651" layer="21"/>
<rectangle x1="14.097" y1="1.56641875" x2="14.351" y2="1.651" layer="21"/>
<rectangle x1="15.79118125" y1="1.56641875" x2="16.04518125" y2="1.651" layer="21"/>
<rectangle x1="16.72081875" y1="1.56641875" x2="17.48281875" y2="1.651" layer="21"/>
<rectangle x1="17.907" y1="1.56641875" x2="19.85518125" y2="1.651" layer="21"/>
<rectangle x1="20.36318125" y1="1.56641875" x2="20.701" y2="1.651" layer="21"/>
<rectangle x1="22.90318125" y1="1.56641875" x2="23.241" y2="1.651" layer="21"/>
<rectangle x1="23.495" y1="1.56641875" x2="23.749" y2="1.651" layer="21"/>
<rectangle x1="25.18918125" y1="1.56641875" x2="25.44318125" y2="1.651" layer="21"/>
<rectangle x1="25.61081875" y1="1.56641875" x2="27.64281875" y2="1.651" layer="21"/>
<rectangle x1="3.429" y1="1.651" x2="3.76681875" y2="1.73558125" layer="21"/>
<rectangle x1="5.969" y1="1.651" x2="6.30681875" y2="1.73558125" layer="21"/>
<rectangle x1="6.64718125" y1="1.651" x2="6.90118125" y2="1.73558125" layer="21"/>
<rectangle x1="8.33881875" y1="1.651" x2="8.59281875" y2="1.73558125" layer="21"/>
<rectangle x1="8.84681875" y1="1.651" x2="9.18718125" y2="1.73558125" layer="21"/>
<rectangle x1="9.69518125" y1="1.651" x2="10.033" y2="1.73558125" layer="21"/>
<rectangle x1="11.303" y1="1.651" x2="11.557" y2="1.73558125" layer="21"/>
<rectangle x1="13.50518125" y1="1.651" x2="13.843" y2="1.73558125" layer="21"/>
<rectangle x1="14.097" y1="1.651" x2="14.351" y2="1.73558125" layer="21"/>
<rectangle x1="15.79118125" y1="1.651" x2="16.04518125" y2="1.73558125" layer="21"/>
<rectangle x1="16.55318125" y1="1.651" x2="17.31518125" y2="1.73558125" layer="21"/>
<rectangle x1="17.907" y1="1.651" x2="19.939" y2="1.73558125" layer="21"/>
<rectangle x1="20.36318125" y1="1.651" x2="20.701" y2="1.73558125" layer="21"/>
<rectangle x1="22.90318125" y1="1.651" x2="23.241" y2="1.73558125" layer="21"/>
<rectangle x1="23.495" y1="1.651" x2="23.749" y2="1.73558125" layer="21"/>
<rectangle x1="25.18918125" y1="1.651" x2="25.44318125" y2="1.73558125" layer="21"/>
<rectangle x1="25.61081875" y1="1.651" x2="27.64281875" y2="1.73558125" layer="21"/>
<rectangle x1="3.429" y1="1.73558125" x2="3.683" y2="1.82041875" layer="21"/>
<rectangle x1="6.05281875" y1="1.73558125" x2="6.30681875" y2="1.82041875" layer="21"/>
<rectangle x1="6.64718125" y1="1.73558125" x2="6.90118125" y2="1.82041875" layer="21"/>
<rectangle x1="8.33881875" y1="1.73558125" x2="8.59281875" y2="1.82041875" layer="21"/>
<rectangle x1="8.84681875" y1="1.73558125" x2="9.18718125" y2="1.82041875" layer="21"/>
<rectangle x1="9.69518125" y1="1.73558125" x2="10.033" y2="1.82041875" layer="21"/>
<rectangle x1="11.303" y1="1.73558125" x2="11.557" y2="1.82041875" layer="21"/>
<rectangle x1="13.50518125" y1="1.73558125" x2="13.843" y2="1.82041875" layer="21"/>
<rectangle x1="14.097" y1="1.73558125" x2="14.351" y2="1.82041875" layer="21"/>
<rectangle x1="15.79118125" y1="1.73558125" x2="16.04518125" y2="1.82041875" layer="21"/>
<rectangle x1="16.383" y1="1.73558125" x2="17.145" y2="1.82041875" layer="21"/>
<rectangle x1="17.907" y1="1.73558125" x2="19.85518125" y2="1.82041875" layer="21"/>
<rectangle x1="20.36318125" y1="1.73558125" x2="20.61718125" y2="1.82041875" layer="21"/>
<rectangle x1="22.987" y1="1.73558125" x2="23.241" y2="1.82041875" layer="21"/>
<rectangle x1="23.495" y1="1.73558125" x2="23.749" y2="1.82041875" layer="21"/>
<rectangle x1="25.18918125" y1="1.73558125" x2="25.44318125" y2="1.82041875" layer="21"/>
<rectangle x1="25.69718125" y1="1.73558125" x2="27.64281875" y2="1.82041875" layer="21"/>
<rectangle x1="3.429" y1="1.82041875" x2="3.683" y2="1.905" layer="21"/>
<rectangle x1="6.05281875" y1="1.82041875" x2="6.30681875" y2="1.905" layer="21"/>
<rectangle x1="6.64718125" y1="1.82041875" x2="6.90118125" y2="1.905" layer="21"/>
<rectangle x1="8.33881875" y1="1.82041875" x2="8.59281875" y2="1.905" layer="21"/>
<rectangle x1="8.84681875" y1="1.82041875" x2="9.18718125" y2="1.905" layer="21"/>
<rectangle x1="9.69518125" y1="1.82041875" x2="10.033" y2="1.905" layer="21"/>
<rectangle x1="11.303" y1="1.82041875" x2="11.557" y2="1.905" layer="21"/>
<rectangle x1="13.589" y1="1.82041875" x2="13.843" y2="1.905" layer="21"/>
<rectangle x1="14.097" y1="1.82041875" x2="14.43481875" y2="1.905" layer="21"/>
<rectangle x1="15.79118125" y1="1.82041875" x2="16.04518125" y2="1.905" layer="21"/>
<rectangle x1="16.383" y1="1.82041875" x2="16.891" y2="1.905" layer="21"/>
<rectangle x1="17.907" y1="1.82041875" x2="18.24481875" y2="1.905" layer="21"/>
<rectangle x1="19.60118125" y1="1.82041875" x2="19.85518125" y2="1.905" layer="21"/>
<rectangle x1="20.36318125" y1="1.82041875" x2="20.61718125" y2="1.905" layer="21"/>
<rectangle x1="22.987" y1="1.82041875" x2="23.241" y2="1.905" layer="21"/>
<rectangle x1="23.495" y1="1.82041875" x2="23.749" y2="1.905" layer="21"/>
<rectangle x1="25.10281875" y1="1.82041875" x2="25.44318125" y2="1.905" layer="21"/>
<rectangle x1="25.69718125" y1="1.82041875" x2="25.95118125" y2="1.905" layer="21"/>
<rectangle x1="27.305" y1="1.82041875" x2="27.64281875" y2="1.905" layer="21"/>
<rectangle x1="3.429" y1="1.905" x2="3.683" y2="1.98958125" layer="21"/>
<rectangle x1="6.05281875" y1="1.905" x2="6.30681875" y2="1.98958125" layer="21"/>
<rectangle x1="6.64718125" y1="1.905" x2="6.985" y2="1.98958125" layer="21"/>
<rectangle x1="8.255" y1="1.905" x2="8.59281875" y2="1.98958125" layer="21"/>
<rectangle x1="8.84681875" y1="1.905" x2="9.18718125" y2="1.98958125" layer="21"/>
<rectangle x1="9.69518125" y1="1.905" x2="10.033" y2="1.98958125" layer="21"/>
<rectangle x1="11.303" y1="1.905" x2="11.557" y2="1.98958125" layer="21"/>
<rectangle x1="13.589" y1="1.905" x2="13.843" y2="1.98958125" layer="21"/>
<rectangle x1="14.097" y1="1.905" x2="14.43481875" y2="1.98958125" layer="21"/>
<rectangle x1="15.70481875" y1="1.905" x2="16.04518125" y2="1.98958125" layer="21"/>
<rectangle x1="16.29918125" y1="1.905" x2="16.72081875" y2="1.98958125" layer="21"/>
<rectangle x1="17.907" y1="1.905" x2="18.24481875" y2="1.98958125" layer="21"/>
<rectangle x1="19.51481875" y1="1.905" x2="19.85518125" y2="1.98958125" layer="21"/>
<rectangle x1="20.36318125" y1="1.905" x2="20.61718125" y2="1.98958125" layer="21"/>
<rectangle x1="22.987" y1="1.905" x2="23.241" y2="1.98958125" layer="21"/>
<rectangle x1="23.495" y1="1.905" x2="23.83281875" y2="1.98958125" layer="21"/>
<rectangle x1="25.10281875" y1="1.905" x2="25.44318125" y2="1.98958125" layer="21"/>
<rectangle x1="25.69718125" y1="1.905" x2="26.035" y2="1.98958125" layer="21"/>
<rectangle x1="27.305" y1="1.905" x2="27.64281875" y2="1.98958125" layer="21"/>
<rectangle x1="3.429" y1="1.98958125" x2="3.683" y2="2.07441875" layer="21"/>
<rectangle x1="6.05281875" y1="1.98958125" x2="6.30681875" y2="2.07441875" layer="21"/>
<rectangle x1="6.64718125" y1="1.98958125" x2="6.985" y2="2.07441875" layer="21"/>
<rectangle x1="8.255" y1="1.98958125" x2="8.509" y2="2.07441875" layer="21"/>
<rectangle x1="8.84681875" y1="1.98958125" x2="9.18718125" y2="2.07441875" layer="21"/>
<rectangle x1="9.69518125" y1="1.98958125" x2="10.033" y2="2.07441875" layer="21"/>
<rectangle x1="11.303" y1="1.98958125" x2="11.557" y2="2.07441875" layer="21"/>
<rectangle x1="13.589" y1="1.98958125" x2="13.843" y2="2.07441875" layer="21"/>
<rectangle x1="14.18081875" y1="1.98958125" x2="14.43481875" y2="2.07441875" layer="21"/>
<rectangle x1="15.70481875" y1="1.98958125" x2="16.04518125" y2="2.07441875" layer="21"/>
<rectangle x1="16.29918125" y1="1.98958125" x2="16.55318125" y2="2.07441875" layer="21"/>
<rectangle x1="17.99081875" y1="1.98958125" x2="18.24481875" y2="2.07441875" layer="21"/>
<rectangle x1="19.51481875" y1="1.98958125" x2="19.85518125" y2="2.07441875" layer="21"/>
<rectangle x1="20.36318125" y1="1.98958125" x2="20.61718125" y2="2.07441875" layer="21"/>
<rectangle x1="22.987" y1="1.98958125" x2="23.241" y2="2.07441875" layer="21"/>
<rectangle x1="23.495" y1="1.98958125" x2="23.83281875" y2="2.07441875" layer="21"/>
<rectangle x1="25.019" y1="1.98958125" x2="25.35681875" y2="2.07441875" layer="21"/>
<rectangle x1="25.69718125" y1="1.98958125" x2="26.035" y2="2.07441875" layer="21"/>
<rectangle x1="27.22118125" y1="1.98958125" x2="27.559" y2="2.07441875" layer="21"/>
<rectangle x1="3.429" y1="2.07441875" x2="3.683" y2="2.159" layer="21"/>
<rectangle x1="6.05281875" y1="2.07441875" x2="6.30681875" y2="2.159" layer="21"/>
<rectangle x1="6.64718125" y1="2.07441875" x2="7.06881875" y2="2.159" layer="21"/>
<rectangle x1="8.17118125" y1="2.07441875" x2="8.509" y2="2.159" layer="21"/>
<rectangle x1="8.84681875" y1="2.07441875" x2="9.18718125" y2="2.159" layer="21"/>
<rectangle x1="9.69518125" y1="2.07441875" x2="10.033" y2="2.159" layer="21"/>
<rectangle x1="11.303" y1="2.07441875" x2="11.557" y2="2.159" layer="21"/>
<rectangle x1="13.589" y1="2.07441875" x2="13.92681875" y2="2.159" layer="21"/>
<rectangle x1="14.18081875" y1="2.07441875" x2="14.52118125" y2="2.159" layer="21"/>
<rectangle x1="15.621" y1="2.07441875" x2="15.95881875" y2="2.159" layer="21"/>
<rectangle x1="16.29918125" y1="2.07441875" x2="16.55318125" y2="2.159" layer="21"/>
<rectangle x1="17.399" y1="2.07441875" x2="17.653" y2="2.159" layer="21"/>
<rectangle x1="17.99081875" y1="2.07441875" x2="18.33118125" y2="2.159" layer="21"/>
<rectangle x1="19.431" y1="2.07441875" x2="19.76881875" y2="2.159" layer="21"/>
<rectangle x1="20.36318125" y1="2.07441875" x2="20.61718125" y2="2.159" layer="21"/>
<rectangle x1="22.987" y1="2.07441875" x2="23.241" y2="2.159" layer="21"/>
<rectangle x1="23.495" y1="2.07441875" x2="23.91918125" y2="2.159" layer="21"/>
<rectangle x1="25.019" y1="2.07441875" x2="25.35681875" y2="2.159" layer="21"/>
<rectangle x1="25.781" y1="2.07441875" x2="26.11881875" y2="2.159" layer="21"/>
<rectangle x1="27.22118125" y1="2.07441875" x2="27.559" y2="2.159" layer="21"/>
<rectangle x1="3.429" y1="2.159" x2="3.683" y2="2.24358125" layer="21"/>
<rectangle x1="6.05281875" y1="2.159" x2="6.30681875" y2="2.24358125" layer="21"/>
<rectangle x1="6.64718125" y1="2.159" x2="7.15518125" y2="2.24358125" layer="21"/>
<rectangle x1="8.08481875" y1="2.159" x2="8.42518125" y2="2.24358125" layer="21"/>
<rectangle x1="8.84681875" y1="2.159" x2="9.18718125" y2="2.24358125" layer="21"/>
<rectangle x1="9.69518125" y1="2.159" x2="10.033" y2="2.24358125" layer="21"/>
<rectangle x1="11.303" y1="2.159" x2="11.557" y2="2.24358125" layer="21"/>
<rectangle x1="13.589" y1="2.159" x2="13.92681875" y2="2.24358125" layer="21"/>
<rectangle x1="14.26718125" y1="2.159" x2="14.605" y2="2.24358125" layer="21"/>
<rectangle x1="15.53718125" y1="2.159" x2="15.95881875" y2="2.24358125" layer="21"/>
<rectangle x1="16.29918125" y1="2.159" x2="16.637" y2="2.24358125" layer="21"/>
<rectangle x1="17.31518125" y1="2.159" x2="17.653" y2="2.24358125" layer="21"/>
<rectangle x1="18.07718125" y1="2.159" x2="18.415" y2="2.24358125" layer="21"/>
<rectangle x1="19.34718125" y1="2.159" x2="19.76881875" y2="2.24358125" layer="21"/>
<rectangle x1="20.36318125" y1="2.159" x2="20.61718125" y2="2.24358125" layer="21"/>
<rectangle x1="22.987" y1="2.159" x2="23.241" y2="2.24358125" layer="21"/>
<rectangle x1="23.495" y1="2.159" x2="24.003" y2="2.24358125" layer="21"/>
<rectangle x1="24.93518125" y1="2.159" x2="25.273" y2="2.24358125" layer="21"/>
<rectangle x1="25.781" y1="2.159" x2="26.20518125" y2="2.24358125" layer="21"/>
<rectangle x1="27.13481875" y1="2.159" x2="27.47518125" y2="2.24358125" layer="21"/>
<rectangle x1="3.429" y1="2.24358125" x2="3.683" y2="2.32841875" layer="21"/>
<rectangle x1="6.05281875" y1="2.24358125" x2="6.30681875" y2="2.32841875" layer="21"/>
<rectangle x1="6.64718125" y1="2.24358125" x2="7.32281875" y2="2.32841875" layer="21"/>
<rectangle x1="7.91718125" y1="2.24358125" x2="8.42518125" y2="2.32841875" layer="21"/>
<rectangle x1="8.84681875" y1="2.24358125" x2="9.18718125" y2="2.32841875" layer="21"/>
<rectangle x1="9.69518125" y1="2.24358125" x2="10.033" y2="2.32841875" layer="21"/>
<rectangle x1="11.303" y1="2.24358125" x2="11.557" y2="2.32841875" layer="21"/>
<rectangle x1="13.589" y1="2.24358125" x2="13.843" y2="2.32841875" layer="21"/>
<rectangle x1="14.26718125" y1="2.24358125" x2="14.77518125" y2="2.32841875" layer="21"/>
<rectangle x1="15.45081875" y1="2.24358125" x2="15.875" y2="2.32841875" layer="21"/>
<rectangle x1="16.29918125" y1="2.24358125" x2="16.72081875" y2="2.32841875" layer="21"/>
<rectangle x1="17.22881875" y1="2.24358125" x2="17.653" y2="2.32841875" layer="21"/>
<rectangle x1="18.07718125" y1="2.24358125" x2="18.58518125" y2="2.32841875" layer="21"/>
<rectangle x1="19.177" y1="2.24358125" x2="19.685" y2="2.32841875" layer="21"/>
<rectangle x1="20.36318125" y1="2.24358125" x2="20.701" y2="2.32841875" layer="21"/>
<rectangle x1="22.90318125" y1="2.24358125" x2="23.241" y2="2.32841875" layer="21"/>
<rectangle x1="23.495" y1="2.24358125" x2="24.17318125" y2="2.32841875" layer="21"/>
<rectangle x1="24.765" y1="2.24358125" x2="25.273" y2="2.32841875" layer="21"/>
<rectangle x1="25.86481875" y1="2.24358125" x2="26.289" y2="2.32841875" layer="21"/>
<rectangle x1="26.96718125" y1="2.24358125" x2="27.47518125" y2="2.32841875" layer="21"/>
<rectangle x1="3.429" y1="2.32841875" x2="3.683" y2="2.413" layer="21"/>
<rectangle x1="6.05281875" y1="2.32841875" x2="6.30681875" y2="2.413" layer="21"/>
<rectangle x1="6.64718125" y1="2.32841875" x2="7.493" y2="2.413" layer="21"/>
<rectangle x1="7.66318125" y1="2.32841875" x2="8.33881875" y2="2.413" layer="21"/>
<rectangle x1="8.84681875" y1="2.32841875" x2="9.18718125" y2="2.413" layer="21"/>
<rectangle x1="9.69518125" y1="2.32841875" x2="10.033" y2="2.413" layer="21"/>
<rectangle x1="11.303" y1="2.32841875" x2="11.557" y2="2.413" layer="21"/>
<rectangle x1="13.589" y1="2.32841875" x2="13.843" y2="2.413" layer="21"/>
<rectangle x1="14.351" y1="2.32841875" x2="15.02918125" y2="2.413" layer="21"/>
<rectangle x1="15.19681875" y1="2.32841875" x2="15.79118125" y2="2.413" layer="21"/>
<rectangle x1="16.383" y1="2.32841875" x2="16.891" y2="2.413" layer="21"/>
<rectangle x1="17.06118125" y1="2.32841875" x2="17.56918125" y2="2.413" layer="21"/>
<rectangle x1="18.161" y1="2.32841875" x2="18.83918125" y2="2.413" layer="21"/>
<rectangle x1="19.00681875" y1="2.32841875" x2="19.60118125" y2="2.413" layer="21"/>
<rectangle x1="20.36318125" y1="2.32841875" x2="20.701" y2="2.413" layer="21"/>
<rectangle x1="22.90318125" y1="2.32841875" x2="23.241" y2="2.413" layer="21"/>
<rectangle x1="23.495" y1="2.32841875" x2="24.34081875" y2="2.413" layer="21"/>
<rectangle x1="24.511" y1="2.32841875" x2="25.18918125" y2="2.413" layer="21"/>
<rectangle x1="25.95118125" y1="2.32841875" x2="26.543" y2="2.413" layer="21"/>
<rectangle x1="26.71318125" y1="2.32841875" x2="27.38881875" y2="2.413" layer="21"/>
<rectangle x1="3.429" y1="2.413" x2="3.683" y2="2.49758125" layer="21"/>
<rectangle x1="6.05281875" y1="2.413" x2="6.30681875" y2="2.49758125" layer="21"/>
<rectangle x1="6.64718125" y1="2.413" x2="6.90118125" y2="2.49758125" layer="21"/>
<rectangle x1="6.985" y1="2.413" x2="8.255" y2="2.49758125" layer="21"/>
<rectangle x1="8.84681875" y1="2.413" x2="9.18718125" y2="2.49758125" layer="21"/>
<rectangle x1="9.35481875" y1="2.413" x2="10.795" y2="2.49758125" layer="21"/>
<rectangle x1="11.303" y1="2.413" x2="11.557" y2="2.49758125" layer="21"/>
<rectangle x1="13.50518125" y1="2.413" x2="13.843" y2="2.49758125" layer="21"/>
<rectangle x1="14.43481875" y1="2.413" x2="15.70481875" y2="2.49758125" layer="21"/>
<rectangle x1="16.46681875" y1="2.413" x2="17.48281875" y2="2.49758125" layer="21"/>
<rectangle x1="18.24481875" y1="2.413" x2="19.51481875" y2="2.49758125" layer="21"/>
<rectangle x1="20.447" y1="2.413" x2="20.701" y2="2.49758125" layer="21"/>
<rectangle x1="22.90318125" y1="2.413" x2="23.241" y2="2.49758125" layer="21"/>
<rectangle x1="23.495" y1="2.413" x2="23.749" y2="2.49758125" layer="21"/>
<rectangle x1="23.83281875" y1="2.413" x2="25.10281875" y2="2.49758125" layer="21"/>
<rectangle x1="26.035" y1="2.413" x2="27.305" y2="2.49758125" layer="21"/>
<rectangle x1="3.429" y1="2.49758125" x2="3.683" y2="2.58241875" layer="21"/>
<rectangle x1="6.05281875" y1="2.49758125" x2="6.30681875" y2="2.58241875" layer="21"/>
<rectangle x1="6.64718125" y1="2.49758125" x2="6.90118125" y2="2.58241875" layer="21"/>
<rectangle x1="7.15518125" y1="2.49758125" x2="8.08481875" y2="2.58241875" layer="21"/>
<rectangle x1="8.84681875" y1="2.49758125" x2="9.18718125" y2="2.58241875" layer="21"/>
<rectangle x1="9.35481875" y1="2.49758125" x2="10.87881875" y2="2.58241875" layer="21"/>
<rectangle x1="11.303" y1="2.49758125" x2="11.557" y2="2.58241875" layer="21"/>
<rectangle x1="13.50518125" y1="2.49758125" x2="13.843" y2="2.58241875" layer="21"/>
<rectangle x1="14.605" y1="2.49758125" x2="15.53718125" y2="2.58241875" layer="21"/>
<rectangle x1="16.55318125" y1="2.49758125" x2="17.399" y2="2.58241875" layer="21"/>
<rectangle x1="18.415" y1="2.49758125" x2="19.34718125" y2="2.58241875" layer="21"/>
<rectangle x1="20.447" y1="2.49758125" x2="20.78481875" y2="2.58241875" layer="21"/>
<rectangle x1="22.81681875" y1="2.49758125" x2="23.15718125" y2="2.58241875" layer="21"/>
<rectangle x1="23.495" y1="2.49758125" x2="23.749" y2="2.58241875" layer="21"/>
<rectangle x1="24.003" y1="2.49758125" x2="24.93518125" y2="2.58241875" layer="21"/>
<rectangle x1="26.20518125" y1="2.49758125" x2="27.13481875" y2="2.58241875" layer="21"/>
<rectangle x1="3.429" y1="2.58241875" x2="3.683" y2="2.667" layer="21"/>
<rectangle x1="6.05281875" y1="2.58241875" x2="6.30681875" y2="2.667" layer="21"/>
<rectangle x1="6.64718125" y1="2.58241875" x2="6.81481875" y2="2.667" layer="21"/>
<rectangle x1="7.32281875" y1="2.58241875" x2="7.91718125" y2="2.667" layer="21"/>
<rectangle x1="8.93318125" y1="2.58241875" x2="9.10081875" y2="2.667" layer="21"/>
<rectangle x1="9.44118125" y1="2.58241875" x2="10.795" y2="2.667" layer="21"/>
<rectangle x1="11.303" y1="2.58241875" x2="11.557" y2="2.667" layer="21"/>
<rectangle x1="13.50518125" y1="2.58241875" x2="13.843" y2="2.667" layer="21"/>
<rectangle x1="14.77518125" y1="2.58241875" x2="15.367" y2="2.667" layer="21"/>
<rectangle x1="16.72081875" y1="2.58241875" x2="17.22881875" y2="2.667" layer="21"/>
<rectangle x1="18.58518125" y1="2.58241875" x2="19.177" y2="2.667" layer="21"/>
<rectangle x1="20.447" y1="2.58241875" x2="20.78481875" y2="2.667" layer="21"/>
<rectangle x1="22.81681875" y1="2.58241875" x2="23.15718125" y2="2.667" layer="21"/>
<rectangle x1="23.495" y1="2.58241875" x2="23.66518125" y2="2.667" layer="21"/>
<rectangle x1="24.17318125" y1="2.58241875" x2="24.765" y2="2.667" layer="21"/>
<rectangle x1="26.37281875" y1="2.58241875" x2="26.96718125" y2="2.667" layer="21"/>
<rectangle x1="3.429" y1="2.667" x2="3.683" y2="2.75158125" layer="21"/>
<rectangle x1="6.05281875" y1="2.667" x2="6.30681875" y2="2.75158125" layer="21"/>
<rectangle x1="9.69518125" y1="2.667" x2="10.033" y2="2.75158125" layer="21"/>
<rectangle x1="11.303" y1="2.667" x2="11.557" y2="2.75158125" layer="21"/>
<rectangle x1="13.41881875" y1="2.667" x2="13.75918125" y2="2.75158125" layer="21"/>
<rectangle x1="20.53081875" y1="2.667" x2="20.87118125" y2="2.75158125" layer="21"/>
<rectangle x1="22.733" y1="2.667" x2="23.07081875" y2="2.75158125" layer="21"/>
<rectangle x1="3.429" y1="2.75158125" x2="3.683" y2="2.83641875" layer="21"/>
<rectangle x1="6.05281875" y1="2.75158125" x2="6.30681875" y2="2.83641875" layer="21"/>
<rectangle x1="8.93318125" y1="2.75158125" x2="9.10081875" y2="2.83641875" layer="21"/>
<rectangle x1="9.69518125" y1="2.75158125" x2="10.033" y2="2.83641875" layer="21"/>
<rectangle x1="11.303" y1="2.75158125" x2="11.557" y2="2.83641875" layer="21"/>
<rectangle x1="13.41881875" y1="2.75158125" x2="13.75918125" y2="2.83641875" layer="21"/>
<rectangle x1="20.61718125" y1="2.75158125" x2="20.955" y2="2.83641875" layer="21"/>
<rectangle x1="22.64918125" y1="2.75158125" x2="23.07081875" y2="2.83641875" layer="21"/>
<rectangle x1="3.429" y1="2.83641875" x2="3.683" y2="2.921" layer="21"/>
<rectangle x1="6.05281875" y1="2.83641875" x2="6.30681875" y2="2.921" layer="21"/>
<rectangle x1="8.84681875" y1="2.83641875" x2="9.18718125" y2="2.921" layer="21"/>
<rectangle x1="9.69518125" y1="2.83641875" x2="10.033" y2="2.921" layer="21"/>
<rectangle x1="11.303" y1="2.83641875" x2="11.557" y2="2.921" layer="21"/>
<rectangle x1="13.335" y1="2.83641875" x2="13.67281875" y2="2.921" layer="21"/>
<rectangle x1="20.61718125" y1="2.83641875" x2="21.03881875" y2="2.921" layer="21"/>
<rectangle x1="22.56281875" y1="2.83641875" x2="22.987" y2="2.921" layer="21"/>
<rectangle x1="3.429" y1="2.921" x2="3.683" y2="3.00558125" layer="21"/>
<rectangle x1="6.05281875" y1="2.921" x2="6.30681875" y2="3.00558125" layer="21"/>
<rectangle x1="8.84681875" y1="2.921" x2="9.18718125" y2="3.00558125" layer="21"/>
<rectangle x1="9.69518125" y1="2.921" x2="10.033" y2="3.00558125" layer="21"/>
<rectangle x1="11.303" y1="2.921" x2="11.557" y2="3.00558125" layer="21"/>
<rectangle x1="13.25118125" y1="2.921" x2="13.589" y2="3.00558125" layer="21"/>
<rectangle x1="20.701" y1="2.921" x2="21.12518125" y2="3.00558125" layer="21"/>
<rectangle x1="22.479" y1="2.921" x2="22.90318125" y2="3.00558125" layer="21"/>
<rectangle x1="3.429" y1="3.00558125" x2="3.683" y2="3.09041875" layer="21"/>
<rectangle x1="6.05281875" y1="3.00558125" x2="6.30681875" y2="3.09041875" layer="21"/>
<rectangle x1="8.93318125" y1="3.00558125" x2="9.18718125" y2="3.09041875" layer="21"/>
<rectangle x1="9.69518125" y1="3.00558125" x2="10.033" y2="3.09041875" layer="21"/>
<rectangle x1="11.303" y1="3.00558125" x2="11.557" y2="3.09041875" layer="21"/>
<rectangle x1="13.16481875" y1="3.00558125" x2="13.589" y2="3.09041875" layer="21"/>
<rectangle x1="20.78481875" y1="3.00558125" x2="21.29281875" y2="3.09041875" layer="21"/>
<rectangle x1="22.30881875" y1="3.00558125" x2="22.81681875" y2="3.09041875" layer="21"/>
<rectangle x1="3.429" y1="3.09041875" x2="3.683" y2="3.175" layer="21"/>
<rectangle x1="6.05281875" y1="3.09041875" x2="6.39318125" y2="3.175" layer="21"/>
<rectangle x1="9.69518125" y1="3.09041875" x2="10.033" y2="3.175" layer="21"/>
<rectangle x1="11.303" y1="3.09041875" x2="11.557" y2="3.175" layer="21"/>
<rectangle x1="12.99718125" y1="3.09041875" x2="13.50518125" y2="3.175" layer="21"/>
<rectangle x1="20.87118125" y1="3.09041875" x2="21.54681875" y2="3.175" layer="21"/>
<rectangle x1="22.05481875" y1="3.09041875" x2="22.733" y2="3.175" layer="21"/>
<rectangle x1="3.429" y1="3.175" x2="3.683" y2="3.25958125" layer="21"/>
<rectangle x1="6.05281875" y1="3.175" x2="6.30681875" y2="3.25958125" layer="21"/>
<rectangle x1="9.69518125" y1="3.175" x2="10.033" y2="3.25958125" layer="21"/>
<rectangle x1="11.303" y1="3.175" x2="11.557" y2="3.25958125" layer="21"/>
<rectangle x1="12.827" y1="3.175" x2="13.41881875" y2="3.25958125" layer="21"/>
<rectangle x1="20.955" y1="3.175" x2="22.64918125" y2="3.25958125" layer="21"/>
<rectangle x1="3.429" y1="3.25958125" x2="3.683" y2="3.34441875" layer="21"/>
<rectangle x1="6.05281875" y1="3.25958125" x2="6.30681875" y2="3.34441875" layer="21"/>
<rectangle x1="9.69518125" y1="3.25958125" x2="10.033" y2="3.34441875" layer="21"/>
<rectangle x1="11.303" y1="3.25958125" x2="13.335" y2="3.34441875" layer="21"/>
<rectangle x1="21.12518125" y1="3.25958125" x2="22.479" y2="3.34441875" layer="21"/>
<rectangle x1="3.429" y1="3.34441875" x2="3.683" y2="3.429" layer="21"/>
<rectangle x1="6.05281875" y1="3.34441875" x2="6.30681875" y2="3.429" layer="21"/>
<rectangle x1="9.69518125" y1="3.34441875" x2="9.94918125" y2="3.429" layer="21"/>
<rectangle x1="11.303" y1="3.34441875" x2="13.16481875" y2="3.429" layer="21"/>
<rectangle x1="21.29281875" y1="3.34441875" x2="22.225" y2="3.429" layer="21"/>
<rectangle x1="11.303" y1="3.429" x2="12.99718125" y2="3.51358125" layer="21"/>
<rectangle x1="11.303" y1="3.51358125" x2="12.827" y2="3.59841875" layer="21"/>
<rectangle x1="5.63118125" y1="4.86841875" x2="24.59481875" y2="4.953" layer="21"/>
<rectangle x1="5.03681875" y1="4.953" x2="25.18918125" y2="5.03758125" layer="21"/>
<rectangle x1="4.699" y1="5.03758125" x2="25.527" y2="5.12241875" layer="21"/>
<rectangle x1="4.36118125" y1="5.12241875" x2="25.86481875" y2="5.207" layer="21"/>
<rectangle x1="4.10718125" y1="5.207" x2="26.11881875" y2="5.29158125" layer="21"/>
<rectangle x1="3.85318125" y1="5.29158125" x2="26.289" y2="5.37641875" layer="21"/>
<rectangle x1="3.683" y1="5.37641875" x2="26.543" y2="5.461" layer="21"/>
<rectangle x1="3.51281875" y1="5.461" x2="26.71318125" y2="5.54558125" layer="21"/>
<rectangle x1="3.34518125" y1="5.54558125" x2="26.88081875" y2="5.63041875" layer="21"/>
<rectangle x1="3.175" y1="5.63041875" x2="27.051" y2="5.715" layer="21"/>
<rectangle x1="3.00481875" y1="5.715" x2="27.13481875" y2="5.79958125" layer="21"/>
<rectangle x1="2.921" y1="5.79958125" x2="5.29081875" y2="5.88441875" layer="21"/>
<rectangle x1="14.77518125" y1="5.79958125" x2="27.305" y2="5.88441875" layer="21"/>
<rectangle x1="2.75081875" y1="5.88441875" x2="4.86918125" y2="5.969" layer="21"/>
<rectangle x1="14.68881875" y1="5.88441875" x2="27.38881875" y2="5.969" layer="21"/>
<rectangle x1="2.667" y1="5.969" x2="4.52881875" y2="6.05358125" layer="21"/>
<rectangle x1="14.605" y1="5.969" x2="27.559" y2="6.05358125" layer="21"/>
<rectangle x1="2.49681875" y1="6.05358125" x2="4.36118125" y2="6.13841875" layer="21"/>
<rectangle x1="14.605" y1="6.05358125" x2="27.64281875" y2="6.13841875" layer="21"/>
<rectangle x1="2.413" y1="6.13841875" x2="4.10718125" y2="6.223" layer="21"/>
<rectangle x1="14.52118125" y1="6.13841875" x2="27.72918125" y2="6.223" layer="21"/>
<rectangle x1="2.32918125" y1="6.223" x2="3.937" y2="6.30758125" layer="21"/>
<rectangle x1="14.52118125" y1="6.223" x2="27.89681875" y2="6.30758125" layer="21"/>
<rectangle x1="2.24281875" y1="6.30758125" x2="3.76681875" y2="6.39241875" layer="21"/>
<rectangle x1="14.43481875" y1="6.30758125" x2="27.98318125" y2="6.39241875" layer="21"/>
<rectangle x1="2.159" y1="6.39241875" x2="3.59918125" y2="6.477" layer="21"/>
<rectangle x1="14.43481875" y1="6.39241875" x2="28.067" y2="6.477" layer="21"/>
<rectangle x1="2.07518125" y1="6.477" x2="3.429" y2="6.56158125" layer="21"/>
<rectangle x1="14.351" y1="6.477" x2="28.15081875" y2="6.56158125" layer="21"/>
<rectangle x1="1.905" y1="6.56158125" x2="3.34518125" y2="6.64641875" layer="21"/>
<rectangle x1="14.351" y1="6.56158125" x2="28.23718125" y2="6.64641875" layer="21"/>
<rectangle x1="1.905" y1="6.64641875" x2="3.175" y2="6.731" layer="21"/>
<rectangle x1="14.26718125" y1="6.64641875" x2="28.321" y2="6.731" layer="21"/>
<rectangle x1="1.73481875" y1="6.731" x2="3.09118125" y2="6.81558125" layer="21"/>
<rectangle x1="14.26718125" y1="6.731" x2="28.40481875" y2="6.81558125" layer="21"/>
<rectangle x1="1.73481875" y1="6.81558125" x2="3.00481875" y2="6.90041875" layer="21"/>
<rectangle x1="14.18081875" y1="6.81558125" x2="28.49118125" y2="6.90041875" layer="21"/>
<rectangle x1="1.651" y1="6.90041875" x2="2.83718125" y2="6.985" layer="21"/>
<rectangle x1="14.18081875" y1="6.90041875" x2="28.575" y2="6.985" layer="21"/>
<rectangle x1="1.56718125" y1="6.985" x2="2.75081875" y2="7.06958125" layer="21"/>
<rectangle x1="14.18081875" y1="6.985" x2="28.65881875" y2="7.06958125" layer="21"/>
<rectangle x1="1.48081875" y1="7.06958125" x2="2.667" y2="7.15441875" layer="21"/>
<rectangle x1="14.097" y1="7.06958125" x2="28.74518125" y2="7.15441875" layer="21"/>
<rectangle x1="1.397" y1="7.15441875" x2="2.58318125" y2="7.239" layer="21"/>
<rectangle x1="14.097" y1="7.15441875" x2="28.74518125" y2="7.239" layer="21"/>
<rectangle x1="1.31318125" y1="7.239" x2="2.49681875" y2="7.32358125" layer="21"/>
<rectangle x1="14.097" y1="7.239" x2="28.829" y2="7.32358125" layer="21"/>
<rectangle x1="1.31318125" y1="7.32358125" x2="2.413" y2="7.40841875" layer="21"/>
<rectangle x1="14.01318125" y1="7.32358125" x2="28.91281875" y2="7.40841875" layer="21"/>
<rectangle x1="1.22681875" y1="7.40841875" x2="2.32918125" y2="7.493" layer="21"/>
<rectangle x1="14.01318125" y1="7.40841875" x2="28.99918125" y2="7.493" layer="21"/>
<rectangle x1="1.143" y1="7.493" x2="2.24281875" y2="7.57758125" layer="21"/>
<rectangle x1="14.01318125" y1="7.493" x2="28.99918125" y2="7.57758125" layer="21"/>
<rectangle x1="1.143" y1="7.57758125" x2="2.159" y2="7.66241875" layer="21"/>
<rectangle x1="13.92681875" y1="7.57758125" x2="29.083" y2="7.66241875" layer="21"/>
<rectangle x1="1.05918125" y1="7.66241875" x2="2.07518125" y2="7.747" layer="21"/>
<rectangle x1="13.92681875" y1="7.66241875" x2="29.16681875" y2="7.747" layer="21"/>
<rectangle x1="0.97281875" y1="7.747" x2="2.07518125" y2="7.83158125" layer="21"/>
<rectangle x1="13.92681875" y1="7.747" x2="29.16681875" y2="7.83158125" layer="21"/>
<rectangle x1="0.97281875" y1="7.83158125" x2="1.98881875" y2="7.91641875" layer="21"/>
<rectangle x1="13.92681875" y1="7.83158125" x2="29.25318125" y2="7.91641875" layer="21"/>
<rectangle x1="0.889" y1="7.91641875" x2="1.905" y2="8.001" layer="21"/>
<rectangle x1="13.843" y1="7.91641875" x2="29.337" y2="8.001" layer="21"/>
<rectangle x1="0.889" y1="8.001" x2="1.82118125" y2="8.08558125" layer="21"/>
<rectangle x1="13.843" y1="8.001" x2="29.337" y2="8.08558125" layer="21"/>
<rectangle x1="0.80518125" y1="8.08558125" x2="1.82118125" y2="8.17041875" layer="21"/>
<rectangle x1="13.843" y1="8.08558125" x2="29.42081875" y2="8.17041875" layer="21"/>
<rectangle x1="0.71881875" y1="8.17041875" x2="1.73481875" y2="8.255" layer="21"/>
<rectangle x1="13.843" y1="8.17041875" x2="29.42081875" y2="8.255" layer="21"/>
<rectangle x1="0.71881875" y1="8.255" x2="1.73481875" y2="8.33958125" layer="21"/>
<rectangle x1="13.75918125" y1="8.255" x2="29.50718125" y2="8.33958125" layer="21"/>
<rectangle x1="0.71881875" y1="8.33958125" x2="1.651" y2="8.42441875" layer="21"/>
<rectangle x1="13.75918125" y1="8.33958125" x2="29.50718125" y2="8.42441875" layer="21"/>
<rectangle x1="0.635" y1="8.42441875" x2="1.651" y2="8.509" layer="21"/>
<rectangle x1="5.63118125" y1="8.42441875" x2="6.64718125" y2="8.509" layer="21"/>
<rectangle x1="8.763" y1="8.42441875" x2="10.541" y2="8.509" layer="21"/>
<rectangle x1="13.75918125" y1="8.42441875" x2="16.80718125" y2="8.509" layer="21"/>
<rectangle x1="17.145" y1="8.42441875" x2="29.591" y2="8.509" layer="21"/>
<rectangle x1="0.635" y1="8.509" x2="1.56718125" y2="8.59358125" layer="21"/>
<rectangle x1="5.29081875" y1="8.509" x2="6.90118125" y2="8.59358125" layer="21"/>
<rectangle x1="8.67918125" y1="8.509" x2="10.87881875" y2="8.59358125" layer="21"/>
<rectangle x1="13.75918125" y1="8.509" x2="16.29918125" y2="8.59358125" layer="21"/>
<rectangle x1="17.653" y1="8.509" x2="20.10918125" y2="8.59358125" layer="21"/>
<rectangle x1="20.447" y1="8.509" x2="22.733" y2="8.59358125" layer="21"/>
<rectangle x1="23.07081875" y1="8.509" x2="24.84881875" y2="8.59358125" layer="21"/>
<rectangle x1="25.61081875" y1="8.509" x2="29.591" y2="8.59358125" layer="21"/>
<rectangle x1="0.55118125" y1="8.59358125" x2="1.48081875" y2="8.67841875" layer="21"/>
<rectangle x1="5.12318125" y1="8.59358125" x2="7.15518125" y2="8.67841875" layer="21"/>
<rectangle x1="8.59281875" y1="8.59358125" x2="11.049" y2="8.67841875" layer="21"/>
<rectangle x1="13.75918125" y1="8.59358125" x2="16.04518125" y2="8.67841875" layer="21"/>
<rectangle x1="17.907" y1="8.59358125" x2="20.02281875" y2="8.67841875" layer="21"/>
<rectangle x1="20.53081875" y1="8.59358125" x2="22.64918125" y2="8.67841875" layer="21"/>
<rectangle x1="23.15718125" y1="8.59358125" x2="24.59481875" y2="8.67841875" layer="21"/>
<rectangle x1="25.86481875" y1="8.59358125" x2="29.591" y2="8.67841875" layer="21"/>
<rectangle x1="0.55118125" y1="8.67841875" x2="1.48081875" y2="8.763" layer="21"/>
<rectangle x1="4.953" y1="8.67841875" x2="7.32281875" y2="8.763" layer="21"/>
<rectangle x1="8.59281875" y1="8.67841875" x2="11.21918125" y2="8.763" layer="21"/>
<rectangle x1="13.67281875" y1="8.67841875" x2="15.875" y2="8.763" layer="21"/>
<rectangle x1="18.07718125" y1="8.67841875" x2="19.939" y2="8.763" layer="21"/>
<rectangle x1="20.53081875" y1="8.67841875" x2="22.64918125" y2="8.763" layer="21"/>
<rectangle x1="23.15718125" y1="8.67841875" x2="24.42718125" y2="8.763" layer="21"/>
<rectangle x1="26.11881875" y1="8.67841875" x2="29.67481875" y2="8.763" layer="21"/>
<rectangle x1="0.46481875" y1="8.763" x2="1.397" y2="8.84758125" layer="21"/>
<rectangle x1="4.78281875" y1="8.763" x2="7.493" y2="8.84758125" layer="21"/>
<rectangle x1="8.59281875" y1="8.763" x2="11.38681875" y2="8.84758125" layer="21"/>
<rectangle x1="13.67281875" y1="8.763" x2="15.70481875" y2="8.84758125" layer="21"/>
<rectangle x1="18.24481875" y1="8.763" x2="19.939" y2="8.84758125" layer="21"/>
<rectangle x1="20.53081875" y1="8.763" x2="22.64918125" y2="8.84758125" layer="21"/>
<rectangle x1="23.241" y1="8.763" x2="24.34081875" y2="8.84758125" layer="21"/>
<rectangle x1="26.289" y1="8.763" x2="29.67481875" y2="8.84758125" layer="21"/>
<rectangle x1="0.46481875" y1="8.84758125" x2="1.397" y2="8.93241875" layer="21"/>
<rectangle x1="4.699" y1="8.84758125" x2="7.57681875" y2="8.93241875" layer="21"/>
<rectangle x1="8.59281875" y1="8.84758125" x2="11.47318125" y2="8.93241875" layer="21"/>
<rectangle x1="13.67281875" y1="8.84758125" x2="15.621" y2="8.93241875" layer="21"/>
<rectangle x1="18.33118125" y1="8.84758125" x2="19.939" y2="8.93241875" layer="21"/>
<rectangle x1="20.53081875" y1="8.84758125" x2="22.56281875" y2="8.93241875" layer="21"/>
<rectangle x1="23.241" y1="8.84758125" x2="24.17318125" y2="8.93241875" layer="21"/>
<rectangle x1="26.37281875" y1="8.84758125" x2="29.76118125" y2="8.93241875" layer="21"/>
<rectangle x1="0.46481875" y1="8.93241875" x2="1.397" y2="9.017" layer="21"/>
<rectangle x1="4.52881875" y1="8.93241875" x2="7.66318125" y2="9.017" layer="21"/>
<rectangle x1="8.59281875" y1="8.93241875" x2="11.557" y2="9.017" layer="21"/>
<rectangle x1="13.67281875" y1="8.93241875" x2="15.45081875" y2="9.017" layer="21"/>
<rectangle x1="18.49881875" y1="8.93241875" x2="19.939" y2="9.017" layer="21"/>
<rectangle x1="20.53081875" y1="8.93241875" x2="22.56281875" y2="9.017" layer="21"/>
<rectangle x1="23.241" y1="8.93241875" x2="24.08681875" y2="9.017" layer="21"/>
<rectangle x1="26.45918125" y1="8.93241875" x2="29.76118125" y2="9.017" layer="21"/>
<rectangle x1="0.381" y1="9.017" x2="1.31318125" y2="9.10158125" layer="21"/>
<rectangle x1="4.445" y1="9.017" x2="7.83081875" y2="9.10158125" layer="21"/>
<rectangle x1="8.59281875" y1="9.017" x2="11.72718125" y2="9.10158125" layer="21"/>
<rectangle x1="13.67281875" y1="9.017" x2="15.367" y2="9.10158125" layer="21"/>
<rectangle x1="18.58518125" y1="9.017" x2="19.939" y2="9.10158125" layer="21"/>
<rectangle x1="20.53081875" y1="9.017" x2="22.56281875" y2="9.10158125" layer="21"/>
<rectangle x1="23.241" y1="9.017" x2="24.003" y2="9.10158125" layer="21"/>
<rectangle x1="26.543" y1="9.017" x2="29.76118125" y2="9.10158125" layer="21"/>
<rectangle x1="0.381" y1="9.10158125" x2="1.31318125" y2="9.18641875" layer="21"/>
<rectangle x1="4.36118125" y1="9.10158125" x2="7.91718125" y2="9.18641875" layer="21"/>
<rectangle x1="8.67918125" y1="9.10158125" x2="11.811" y2="9.18641875" layer="21"/>
<rectangle x1="13.67281875" y1="9.10158125" x2="15.28318125" y2="9.18641875" layer="21"/>
<rectangle x1="18.669" y1="9.10158125" x2="19.939" y2="9.18641875" layer="21"/>
<rectangle x1="20.53081875" y1="9.10158125" x2="22.56281875" y2="9.18641875" layer="21"/>
<rectangle x1="23.241" y1="9.10158125" x2="23.91918125" y2="9.18641875" layer="21"/>
<rectangle x1="25.019" y1="9.10158125" x2="25.44318125" y2="9.18641875" layer="21"/>
<rectangle x1="26.62681875" y1="9.10158125" x2="29.845" y2="9.18641875" layer="21"/>
<rectangle x1="0.381" y1="9.18641875" x2="1.31318125" y2="9.271" layer="21"/>
<rectangle x1="4.27481875" y1="9.18641875" x2="8.001" y2="9.271" layer="21"/>
<rectangle x1="8.763" y1="9.18641875" x2="11.89481875" y2="9.271" layer="21"/>
<rectangle x1="13.67281875" y1="9.18641875" x2="15.19681875" y2="9.271" layer="21"/>
<rectangle x1="16.891" y1="9.18641875" x2="17.06118125" y2="9.271" layer="21"/>
<rectangle x1="18.75281875" y1="9.18641875" x2="19.939" y2="9.271" layer="21"/>
<rectangle x1="20.53081875" y1="9.18641875" x2="22.56281875" y2="9.271" layer="21"/>
<rectangle x1="23.241" y1="9.18641875" x2="23.91918125" y2="9.271" layer="21"/>
<rectangle x1="24.765" y1="9.18641875" x2="25.781" y2="9.271" layer="21"/>
<rectangle x1="26.71318125" y1="9.18641875" x2="29.845" y2="9.271" layer="21"/>
<rectangle x1="0.29718125" y1="9.271" x2="1.22681875" y2="9.35558125" layer="21"/>
<rectangle x1="4.191" y1="9.271" x2="5.79881875" y2="9.35558125" layer="21"/>
<rectangle x1="6.477" y1="9.271" x2="8.08481875" y2="9.35558125" layer="21"/>
<rectangle x1="10.287" y1="9.271" x2="11.89481875" y2="9.35558125" layer="21"/>
<rectangle x1="13.589" y1="9.271" x2="15.113" y2="9.35558125" layer="21"/>
<rectangle x1="16.46681875" y1="9.271" x2="17.48281875" y2="9.35558125" layer="21"/>
<rectangle x1="18.83918125" y1="9.271" x2="19.939" y2="9.35558125" layer="21"/>
<rectangle x1="20.53081875" y1="9.271" x2="22.56281875" y2="9.35558125" layer="21"/>
<rectangle x1="23.241" y1="9.271" x2="23.83281875" y2="9.35558125" layer="21"/>
<rectangle x1="24.68118125" y1="9.271" x2="25.95118125" y2="9.35558125" layer="21"/>
<rectangle x1="26.71318125" y1="9.271" x2="29.845" y2="9.35558125" layer="21"/>
<rectangle x1="0.29718125" y1="9.35558125" x2="1.22681875" y2="9.44041875" layer="21"/>
<rectangle x1="4.10718125" y1="9.35558125" x2="5.54481875" y2="9.44041875" layer="21"/>
<rectangle x1="6.731" y1="9.35558125" x2="8.08481875" y2="9.44041875" layer="21"/>
<rectangle x1="10.62481875" y1="9.35558125" x2="11.98118125" y2="9.44041875" layer="21"/>
<rectangle x1="13.589" y1="9.35558125" x2="15.02918125" y2="9.44041875" layer="21"/>
<rectangle x1="16.21281875" y1="9.35558125" x2="17.653" y2="9.44041875" layer="21"/>
<rectangle x1="18.923" y1="9.35558125" x2="19.939" y2="9.44041875" layer="21"/>
<rectangle x1="20.53081875" y1="9.35558125" x2="22.56281875" y2="9.44041875" layer="21"/>
<rectangle x1="23.241" y1="9.35558125" x2="23.749" y2="9.44041875" layer="21"/>
<rectangle x1="24.59481875" y1="9.35558125" x2="26.035" y2="9.44041875" layer="21"/>
<rectangle x1="26.71318125" y1="9.35558125" x2="29.845" y2="9.44041875" layer="21"/>
<rectangle x1="0.29718125" y1="9.44041875" x2="1.22681875" y2="9.525" layer="21"/>
<rectangle x1="4.10718125" y1="9.44041875" x2="5.37718125" y2="9.525" layer="21"/>
<rectangle x1="6.90118125" y1="9.44041875" x2="8.17118125" y2="9.525" layer="21"/>
<rectangle x1="10.795" y1="9.44041875" x2="12.065" y2="9.525" layer="21"/>
<rectangle x1="13.589" y1="9.44041875" x2="14.94281875" y2="9.525" layer="21"/>
<rectangle x1="16.129" y1="9.44041875" x2="17.82318125" y2="9.525" layer="21"/>
<rectangle x1="18.923" y1="9.44041875" x2="19.939" y2="9.525" layer="21"/>
<rectangle x1="20.53081875" y1="9.44041875" x2="22.56281875" y2="9.525" layer="21"/>
<rectangle x1="23.241" y1="9.44041875" x2="23.749" y2="9.525" layer="21"/>
<rectangle x1="24.511" y1="9.44041875" x2="26.11881875" y2="9.525" layer="21"/>
<rectangle x1="26.62681875" y1="9.44041875" x2="29.92881875" y2="9.525" layer="21"/>
<rectangle x1="0.29718125" y1="9.525" x2="1.143" y2="9.60958125" layer="21"/>
<rectangle x1="4.02081875" y1="9.525" x2="5.207" y2="9.60958125" layer="21"/>
<rectangle x1="7.06881875" y1="9.525" x2="8.255" y2="9.60958125" layer="21"/>
<rectangle x1="10.96518125" y1="9.525" x2="12.14881875" y2="9.60958125" layer="21"/>
<rectangle x1="13.589" y1="9.525" x2="14.94281875" y2="9.60958125" layer="21"/>
<rectangle x1="15.95881875" y1="9.525" x2="17.99081875" y2="9.60958125" layer="21"/>
<rectangle x1="19.00681875" y1="9.525" x2="19.939" y2="9.60958125" layer="21"/>
<rectangle x1="20.53081875" y1="9.525" x2="22.56281875" y2="9.60958125" layer="21"/>
<rectangle x1="23.241" y1="9.525" x2="23.749" y2="9.60958125" layer="21"/>
<rectangle x1="24.42718125" y1="9.525" x2="26.20518125" y2="9.60958125" layer="21"/>
<rectangle x1="26.543" y1="9.525" x2="29.92881875" y2="9.60958125" layer="21"/>
<rectangle x1="0.21081875" y1="9.60958125" x2="1.143" y2="9.69441875" layer="21"/>
<rectangle x1="3.937" y1="9.60958125" x2="5.12318125" y2="9.69441875" layer="21"/>
<rectangle x1="7.15518125" y1="9.60958125" x2="8.255" y2="9.69441875" layer="21"/>
<rectangle x1="11.049" y1="9.60958125" x2="12.14881875" y2="9.69441875" layer="21"/>
<rectangle x1="13.589" y1="9.60958125" x2="14.859" y2="9.69441875" layer="21"/>
<rectangle x1="15.875" y1="9.60958125" x2="18.07718125" y2="9.69441875" layer="21"/>
<rectangle x1="19.09318125" y1="9.60958125" x2="19.939" y2="9.69441875" layer="21"/>
<rectangle x1="20.53081875" y1="9.60958125" x2="22.56281875" y2="9.69441875" layer="21"/>
<rectangle x1="23.241" y1="9.60958125" x2="23.66518125" y2="9.69441875" layer="21"/>
<rectangle x1="24.34081875" y1="9.60958125" x2="29.92881875" y2="9.69441875" layer="21"/>
<rectangle x1="0.21081875" y1="9.69441875" x2="1.143" y2="9.779" layer="21"/>
<rectangle x1="3.937" y1="9.69441875" x2="4.953" y2="9.779" layer="21"/>
<rectangle x1="7.239" y1="9.69441875" x2="8.33881875" y2="9.779" layer="21"/>
<rectangle x1="11.13281875" y1="9.69441875" x2="12.23518125" y2="9.779" layer="21"/>
<rectangle x1="13.589" y1="9.69441875" x2="14.77518125" y2="9.779" layer="21"/>
<rectangle x1="15.79118125" y1="9.69441875" x2="18.161" y2="9.779" layer="21"/>
<rectangle x1="19.09318125" y1="9.69441875" x2="19.939" y2="9.779" layer="21"/>
<rectangle x1="20.53081875" y1="9.69441875" x2="22.56281875" y2="9.779" layer="21"/>
<rectangle x1="23.241" y1="9.69441875" x2="23.66518125" y2="9.779" layer="21"/>
<rectangle x1="24.257" y1="9.69441875" x2="29.92881875" y2="9.779" layer="21"/>
<rectangle x1="0.21081875" y1="9.779" x2="1.143" y2="9.86358125" layer="21"/>
<rectangle x1="3.85318125" y1="9.779" x2="4.86918125" y2="9.86358125" layer="21"/>
<rectangle x1="7.32281875" y1="9.779" x2="8.33881875" y2="9.86358125" layer="21"/>
<rectangle x1="11.21918125" y1="9.779" x2="12.23518125" y2="9.86358125" layer="21"/>
<rectangle x1="13.589" y1="9.779" x2="14.77518125" y2="9.86358125" layer="21"/>
<rectangle x1="15.70481875" y1="9.779" x2="18.24481875" y2="9.86358125" layer="21"/>
<rectangle x1="19.177" y1="9.779" x2="19.939" y2="9.86358125" layer="21"/>
<rectangle x1="20.53081875" y1="9.779" x2="22.56281875" y2="9.86358125" layer="21"/>
<rectangle x1="23.241" y1="9.779" x2="23.66518125" y2="9.86358125" layer="21"/>
<rectangle x1="24.34081875" y1="9.779" x2="29.92881875" y2="9.86358125" layer="21"/>
<rectangle x1="0.21081875" y1="9.86358125" x2="1.05918125" y2="9.94841875" layer="21"/>
<rectangle x1="3.85318125" y1="9.86358125" x2="4.86918125" y2="9.94841875" layer="21"/>
<rectangle x1="7.40918125" y1="9.86358125" x2="8.42518125" y2="9.94841875" layer="21"/>
<rectangle x1="11.303" y1="9.86358125" x2="12.319" y2="9.94841875" layer="21"/>
<rectangle x1="13.589" y1="9.86358125" x2="14.68881875" y2="9.94841875" layer="21"/>
<rectangle x1="15.621" y1="9.86358125" x2="18.33118125" y2="9.94841875" layer="21"/>
<rectangle x1="19.177" y1="9.86358125" x2="19.939" y2="9.94841875" layer="21"/>
<rectangle x1="20.53081875" y1="9.86358125" x2="22.56281875" y2="9.94841875" layer="21"/>
<rectangle x1="23.241" y1="9.86358125" x2="23.66518125" y2="9.94841875" layer="21"/>
<rectangle x1="26.71318125" y1="9.86358125" x2="29.92881875" y2="9.94841875" layer="21"/>
<rectangle x1="0.21081875" y1="9.94841875" x2="1.05918125" y2="10.033" layer="21"/>
<rectangle x1="3.76681875" y1="9.94841875" x2="4.78281875" y2="10.033" layer="21"/>
<rectangle x1="7.493" y1="9.94841875" x2="8.42518125" y2="10.033" layer="21"/>
<rectangle x1="11.38681875" y1="9.94841875" x2="12.319" y2="10.033" layer="21"/>
<rectangle x1="13.589" y1="9.94841875" x2="14.68881875" y2="10.033" layer="21"/>
<rectangle x1="15.53718125" y1="9.94841875" x2="18.33118125" y2="10.033" layer="21"/>
<rectangle x1="19.26081875" y1="9.94841875" x2="19.939" y2="10.033" layer="21"/>
<rectangle x1="20.53081875" y1="9.94841875" x2="22.56281875" y2="10.033" layer="21"/>
<rectangle x1="23.241" y1="9.94841875" x2="23.57881875" y2="10.033" layer="21"/>
<rectangle x1="26.797" y1="9.94841875" x2="29.92881875" y2="10.033" layer="21"/>
<rectangle x1="0.21081875" y1="10.033" x2="1.05918125" y2="10.11758125" layer="21"/>
<rectangle x1="3.76681875" y1="10.033" x2="4.699" y2="10.11758125" layer="21"/>
<rectangle x1="7.493" y1="10.033" x2="8.509" y2="10.11758125" layer="21"/>
<rectangle x1="11.38681875" y1="10.033" x2="12.40281875" y2="10.11758125" layer="21"/>
<rectangle x1="13.589" y1="10.033" x2="14.68881875" y2="10.11758125" layer="21"/>
<rectangle x1="15.53718125" y1="10.033" x2="18.415" y2="10.11758125" layer="21"/>
<rectangle x1="19.26081875" y1="10.033" x2="19.939" y2="10.11758125" layer="21"/>
<rectangle x1="20.53081875" y1="10.033" x2="22.56281875" y2="10.11758125" layer="21"/>
<rectangle x1="23.241" y1="10.033" x2="23.57881875" y2="10.11758125" layer="21"/>
<rectangle x1="26.88081875" y1="10.033" x2="29.92881875" y2="10.11758125" layer="21"/>
<rectangle x1="0.127" y1="10.11758125" x2="1.05918125" y2="10.20241875" layer="21"/>
<rectangle x1="3.76681875" y1="10.11758125" x2="4.699" y2="10.20241875" layer="21"/>
<rectangle x1="7.57681875" y1="10.11758125" x2="8.509" y2="10.20241875" layer="21"/>
<rectangle x1="11.47318125" y1="10.11758125" x2="12.40281875" y2="10.20241875" layer="21"/>
<rectangle x1="13.589" y1="10.11758125" x2="14.605" y2="10.20241875" layer="21"/>
<rectangle x1="15.45081875" y1="10.11758125" x2="18.49881875" y2="10.20241875" layer="21"/>
<rectangle x1="19.34718125" y1="10.11758125" x2="19.939" y2="10.20241875" layer="21"/>
<rectangle x1="20.53081875" y1="10.11758125" x2="22.56281875" y2="10.20241875" layer="21"/>
<rectangle x1="23.241" y1="10.11758125" x2="23.57881875" y2="10.20241875" layer="21"/>
<rectangle x1="26.88081875" y1="10.11758125" x2="29.92881875" y2="10.20241875" layer="21"/>
<rectangle x1="0.127" y1="10.20241875" x2="1.05918125" y2="10.287" layer="21"/>
<rectangle x1="3.683" y1="10.20241875" x2="4.61518125" y2="10.287" layer="21"/>
<rectangle x1="7.57681875" y1="10.20241875" x2="8.509" y2="10.287" layer="21"/>
<rectangle x1="11.557" y1="10.20241875" x2="12.40281875" y2="10.287" layer="21"/>
<rectangle x1="13.50518125" y1="10.20241875" x2="14.605" y2="10.287" layer="21"/>
<rectangle x1="15.45081875" y1="10.20241875" x2="18.49881875" y2="10.287" layer="21"/>
<rectangle x1="19.34718125" y1="10.20241875" x2="19.939" y2="10.287" layer="21"/>
<rectangle x1="20.53081875" y1="10.20241875" x2="22.56281875" y2="10.287" layer="21"/>
<rectangle x1="23.241" y1="10.20241875" x2="23.57881875" y2="10.287" layer="21"/>
<rectangle x1="26.88081875" y1="10.20241875" x2="29.92881875" y2="10.287" layer="21"/>
<rectangle x1="0.127" y1="10.287" x2="0.97281875" y2="10.37158125" layer="21"/>
<rectangle x1="3.683" y1="10.287" x2="4.61518125" y2="10.37158125" layer="21"/>
<rectangle x1="7.66318125" y1="10.287" x2="8.509" y2="10.37158125" layer="21"/>
<rectangle x1="11.557" y1="10.287" x2="12.48918125" y2="10.37158125" layer="21"/>
<rectangle x1="13.50518125" y1="10.287" x2="14.605" y2="10.37158125" layer="21"/>
<rectangle x1="15.367" y1="10.287" x2="18.49881875" y2="10.37158125" layer="21"/>
<rectangle x1="19.34718125" y1="10.287" x2="19.939" y2="10.37158125" layer="21"/>
<rectangle x1="20.53081875" y1="10.287" x2="22.56281875" y2="10.37158125" layer="21"/>
<rectangle x1="23.241" y1="10.287" x2="23.57881875" y2="10.37158125" layer="21"/>
<rectangle x1="26.88081875" y1="10.287" x2="29.92881875" y2="10.37158125" layer="21"/>
<rectangle x1="0.127" y1="10.37158125" x2="0.97281875" y2="10.45641875" layer="21"/>
<rectangle x1="3.683" y1="10.37158125" x2="4.61518125" y2="10.45641875" layer="21"/>
<rectangle x1="7.66318125" y1="10.37158125" x2="8.59281875" y2="10.45641875" layer="21"/>
<rectangle x1="11.557" y1="10.37158125" x2="12.48918125" y2="10.45641875" layer="21"/>
<rectangle x1="13.50518125" y1="10.37158125" x2="14.605" y2="10.45641875" layer="21"/>
<rectangle x1="15.367" y1="10.37158125" x2="18.58518125" y2="10.45641875" layer="21"/>
<rectangle x1="19.34718125" y1="10.37158125" x2="19.939" y2="10.45641875" layer="21"/>
<rectangle x1="20.61718125" y1="10.37158125" x2="22.56281875" y2="10.45641875" layer="21"/>
<rectangle x1="23.15718125" y1="10.37158125" x2="23.66518125" y2="10.45641875" layer="21"/>
<rectangle x1="26.88081875" y1="10.37158125" x2="29.92881875" y2="10.45641875" layer="21"/>
<rectangle x1="0.127" y1="10.45641875" x2="0.97281875" y2="10.541" layer="21"/>
<rectangle x1="3.683" y1="10.45641875" x2="4.52881875" y2="10.541" layer="21"/>
<rectangle x1="7.747" y1="10.45641875" x2="8.59281875" y2="10.541" layer="21"/>
<rectangle x1="11.64081875" y1="10.45641875" x2="12.48918125" y2="10.541" layer="21"/>
<rectangle x1="13.50518125" y1="10.45641875" x2="14.52118125" y2="10.541" layer="21"/>
<rectangle x1="15.367" y1="10.45641875" x2="18.58518125" y2="10.541" layer="21"/>
<rectangle x1="19.34718125" y1="10.45641875" x2="19.939" y2="10.541" layer="21"/>
<rectangle x1="20.61718125" y1="10.45641875" x2="22.56281875" y2="10.541" layer="21"/>
<rectangle x1="23.15718125" y1="10.45641875" x2="23.66518125" y2="10.541" layer="21"/>
<rectangle x1="24.34081875" y1="10.45641875" x2="26.20518125" y2="10.541" layer="21"/>
<rectangle x1="26.797" y1="10.45641875" x2="29.92881875" y2="10.541" layer="21"/>
<rectangle x1="0.127" y1="10.541" x2="0.97281875" y2="10.62558125" layer="21"/>
<rectangle x1="3.683" y1="10.541" x2="4.52881875" y2="10.62558125" layer="21"/>
<rectangle x1="7.747" y1="10.541" x2="8.59281875" y2="10.62558125" layer="21"/>
<rectangle x1="11.64081875" y1="10.541" x2="12.48918125" y2="10.62558125" layer="21"/>
<rectangle x1="13.50518125" y1="10.541" x2="14.52118125" y2="10.62558125" layer="21"/>
<rectangle x1="15.28318125" y1="10.541" x2="18.58518125" y2="10.62558125" layer="21"/>
<rectangle x1="19.431" y1="10.541" x2="19.939" y2="10.62558125" layer="21"/>
<rectangle x1="20.61718125" y1="10.541" x2="22.479" y2="10.62558125" layer="21"/>
<rectangle x1="23.15718125" y1="10.541" x2="23.66518125" y2="10.62558125" layer="21"/>
<rectangle x1="24.34081875" y1="10.541" x2="26.20518125" y2="10.62558125" layer="21"/>
<rectangle x1="26.797" y1="10.541" x2="29.92881875" y2="10.62558125" layer="21"/>
<rectangle x1="0.127" y1="10.62558125" x2="0.97281875" y2="10.71041875" layer="21"/>
<rectangle x1="3.59918125" y1="10.62558125" x2="4.52881875" y2="10.71041875" layer="21"/>
<rectangle x1="7.747" y1="10.62558125" x2="8.59281875" y2="10.71041875" layer="21"/>
<rectangle x1="11.64081875" y1="10.62558125" x2="12.48918125" y2="10.71041875" layer="21"/>
<rectangle x1="13.50518125" y1="10.62558125" x2="14.52118125" y2="10.71041875" layer="21"/>
<rectangle x1="15.28318125" y1="10.62558125" x2="18.669" y2="10.71041875" layer="21"/>
<rectangle x1="19.431" y1="10.62558125" x2="19.939" y2="10.71041875" layer="21"/>
<rectangle x1="20.701" y1="10.62558125" x2="22.479" y2="10.71041875" layer="21"/>
<rectangle x1="23.15718125" y1="10.62558125" x2="23.66518125" y2="10.71041875" layer="21"/>
<rectangle x1="24.34081875" y1="10.62558125" x2="26.11881875" y2="10.71041875" layer="21"/>
<rectangle x1="26.797" y1="10.62558125" x2="29.92881875" y2="10.71041875" layer="21"/>
<rectangle x1="0.127" y1="10.71041875" x2="0.97281875" y2="10.795" layer="21"/>
<rectangle x1="3.59918125" y1="10.71041875" x2="4.52881875" y2="10.795" layer="21"/>
<rectangle x1="7.747" y1="10.71041875" x2="8.59281875" y2="10.795" layer="21"/>
<rectangle x1="11.64081875" y1="10.71041875" x2="12.48918125" y2="10.795" layer="21"/>
<rectangle x1="13.50518125" y1="10.71041875" x2="14.52118125" y2="10.795" layer="21"/>
<rectangle x1="15.28318125" y1="10.71041875" x2="18.669" y2="10.795" layer="21"/>
<rectangle x1="19.431" y1="10.71041875" x2="19.939" y2="10.795" layer="21"/>
<rectangle x1="20.78481875" y1="10.71041875" x2="22.39518125" y2="10.795" layer="21"/>
<rectangle x1="23.07081875" y1="10.71041875" x2="23.749" y2="10.795" layer="21"/>
<rectangle x1="24.42718125" y1="10.71041875" x2="26.035" y2="10.795" layer="21"/>
<rectangle x1="26.797" y1="10.71041875" x2="29.92881875" y2="10.795" layer="21"/>
<rectangle x1="0.127" y1="10.795" x2="0.97281875" y2="10.87958125" layer="21"/>
<rectangle x1="3.59918125" y1="10.795" x2="4.52881875" y2="10.87958125" layer="21"/>
<rectangle x1="7.747" y1="10.795" x2="8.59281875" y2="10.87958125" layer="21"/>
<rectangle x1="11.64081875" y1="10.795" x2="12.48918125" y2="10.87958125" layer="21"/>
<rectangle x1="13.50518125" y1="10.795" x2="14.52118125" y2="10.87958125" layer="21"/>
<rectangle x1="15.28318125" y1="10.795" x2="18.669" y2="10.87958125" layer="21"/>
<rectangle x1="19.431" y1="10.795" x2="19.939" y2="10.87958125" layer="21"/>
<rectangle x1="20.78481875" y1="10.795" x2="22.30881875" y2="10.87958125" layer="21"/>
<rectangle x1="23.07081875" y1="10.795" x2="23.749" y2="10.87958125" layer="21"/>
<rectangle x1="24.511" y1="10.795" x2="26.035" y2="10.87958125" layer="21"/>
<rectangle x1="26.71318125" y1="10.795" x2="29.92881875" y2="10.87958125" layer="21"/>
<rectangle x1="0.127" y1="10.87958125" x2="0.97281875" y2="10.96441875" layer="21"/>
<rectangle x1="3.59918125" y1="10.87958125" x2="4.52881875" y2="10.96441875" layer="21"/>
<rectangle x1="7.747" y1="10.87958125" x2="8.59281875" y2="10.96441875" layer="21"/>
<rectangle x1="11.64081875" y1="10.87958125" x2="12.48918125" y2="10.96441875" layer="21"/>
<rectangle x1="13.50518125" y1="10.87958125" x2="14.52118125" y2="10.96441875" layer="21"/>
<rectangle x1="15.28318125" y1="10.87958125" x2="18.669" y2="10.96441875" layer="21"/>
<rectangle x1="19.431" y1="10.87958125" x2="19.939" y2="10.96441875" layer="21"/>
<rectangle x1="20.87118125" y1="10.87958125" x2="22.225" y2="10.96441875" layer="21"/>
<rectangle x1="22.987" y1="10.87958125" x2="23.83281875" y2="10.96441875" layer="21"/>
<rectangle x1="24.59481875" y1="10.87958125" x2="25.95118125" y2="10.96441875" layer="21"/>
<rectangle x1="26.71318125" y1="10.87958125" x2="29.92881875" y2="10.96441875" layer="21"/>
<rectangle x1="0.127" y1="10.96441875" x2="0.97281875" y2="11.049" layer="21"/>
<rectangle x1="3.59918125" y1="10.96441875" x2="4.52881875" y2="11.049" layer="21"/>
<rectangle x1="7.747" y1="10.96441875" x2="8.59281875" y2="11.049" layer="21"/>
<rectangle x1="11.64081875" y1="10.96441875" x2="12.48918125" y2="11.049" layer="21"/>
<rectangle x1="13.50518125" y1="10.96441875" x2="14.52118125" y2="11.049" layer="21"/>
<rectangle x1="15.28318125" y1="10.96441875" x2="18.669" y2="11.049" layer="21"/>
<rectangle x1="19.431" y1="10.96441875" x2="19.939" y2="11.049" layer="21"/>
<rectangle x1="21.03881875" y1="10.96441875" x2="22.14118125" y2="11.049" layer="21"/>
<rectangle x1="22.987" y1="10.96441875" x2="23.83281875" y2="11.049" layer="21"/>
<rectangle x1="24.68118125" y1="10.96441875" x2="25.781" y2="11.049" layer="21"/>
<rectangle x1="26.62681875" y1="10.96441875" x2="29.92881875" y2="11.049" layer="21"/>
<rectangle x1="0.127" y1="11.049" x2="0.97281875" y2="11.13358125" layer="21"/>
<rectangle x1="3.59918125" y1="11.049" x2="4.52881875" y2="11.13358125" layer="21"/>
<rectangle x1="7.747" y1="11.049" x2="8.59281875" y2="11.13358125" layer="21"/>
<rectangle x1="11.64081875" y1="11.049" x2="12.48918125" y2="11.13358125" layer="21"/>
<rectangle x1="13.50518125" y1="11.049" x2="14.52118125" y2="11.13358125" layer="21"/>
<rectangle x1="15.28318125" y1="11.049" x2="18.669" y2="11.13358125" layer="21"/>
<rectangle x1="19.431" y1="11.049" x2="19.939" y2="11.13358125" layer="21"/>
<rectangle x1="21.209" y1="11.049" x2="21.971" y2="11.13358125" layer="21"/>
<rectangle x1="22.90318125" y1="11.049" x2="23.91918125" y2="11.13358125" layer="21"/>
<rectangle x1="24.84881875" y1="11.049" x2="25.61081875" y2="11.13358125" layer="21"/>
<rectangle x1="26.543" y1="11.049" x2="29.92881875" y2="11.13358125" layer="21"/>
<rectangle x1="0.127" y1="11.13358125" x2="0.97281875" y2="11.21841875" layer="21"/>
<rectangle x1="3.59918125" y1="11.13358125" x2="4.52881875" y2="11.21841875" layer="21"/>
<rectangle x1="7.747" y1="11.13358125" x2="8.59281875" y2="11.21841875" layer="21"/>
<rectangle x1="11.64081875" y1="11.13358125" x2="12.48918125" y2="11.21841875" layer="21"/>
<rectangle x1="13.50518125" y1="11.13358125" x2="14.52118125" y2="11.21841875" layer="21"/>
<rectangle x1="15.28318125" y1="11.13358125" x2="18.58518125" y2="11.21841875" layer="21"/>
<rectangle x1="19.431" y1="11.13358125" x2="19.939" y2="11.21841875" layer="21"/>
<rectangle x1="21.463" y1="11.13358125" x2="21.63318125" y2="11.21841875" layer="21"/>
<rectangle x1="22.81681875" y1="11.13358125" x2="24.003" y2="11.21841875" layer="21"/>
<rectangle x1="25.18918125" y1="11.13358125" x2="25.273" y2="11.21841875" layer="21"/>
<rectangle x1="26.543" y1="11.13358125" x2="29.92881875" y2="11.21841875" layer="21"/>
<rectangle x1="0.127" y1="11.21841875" x2="0.97281875" y2="11.303" layer="21"/>
<rectangle x1="3.59918125" y1="11.21841875" x2="4.52881875" y2="11.303" layer="21"/>
<rectangle x1="7.747" y1="11.21841875" x2="8.59281875" y2="11.303" layer="21"/>
<rectangle x1="11.64081875" y1="11.21841875" x2="12.48918125" y2="11.303" layer="21"/>
<rectangle x1="13.50518125" y1="11.21841875" x2="14.52118125" y2="11.303" layer="21"/>
<rectangle x1="15.28318125" y1="11.21841875" x2="18.58518125" y2="11.303" layer="21"/>
<rectangle x1="19.34718125" y1="11.21841875" x2="19.939" y2="11.303" layer="21"/>
<rectangle x1="22.81681875" y1="11.21841875" x2="24.003" y2="11.303" layer="21"/>
<rectangle x1="26.45918125" y1="11.21841875" x2="29.92881875" y2="11.303" layer="21"/>
<rectangle x1="0.127" y1="11.303" x2="0.97281875" y2="11.38758125" layer="21"/>
<rectangle x1="3.59918125" y1="11.303" x2="4.52881875" y2="11.38758125" layer="21"/>
<rectangle x1="7.747" y1="11.303" x2="8.59281875" y2="11.38758125" layer="21"/>
<rectangle x1="11.557" y1="11.303" x2="12.48918125" y2="11.38758125" layer="21"/>
<rectangle x1="13.50518125" y1="11.303" x2="14.52118125" y2="11.38758125" layer="21"/>
<rectangle x1="15.367" y1="11.303" x2="18.58518125" y2="11.38758125" layer="21"/>
<rectangle x1="19.34718125" y1="11.303" x2="19.939" y2="11.38758125" layer="21"/>
<rectangle x1="22.64918125" y1="11.303" x2="24.08681875" y2="11.38758125" layer="21"/>
<rectangle x1="26.37281875" y1="11.303" x2="29.92881875" y2="11.38758125" layer="21"/>
<rectangle x1="0.127" y1="11.38758125" x2="0.97281875" y2="11.47241875" layer="21"/>
<rectangle x1="3.59918125" y1="11.38758125" x2="4.52881875" y2="11.47241875" layer="21"/>
<rectangle x1="7.747" y1="11.38758125" x2="8.59281875" y2="11.47241875" layer="21"/>
<rectangle x1="11.557" y1="11.38758125" x2="12.48918125" y2="11.47241875" layer="21"/>
<rectangle x1="13.50518125" y1="11.38758125" x2="14.605" y2="11.47241875" layer="21"/>
<rectangle x1="15.367" y1="11.38758125" x2="18.58518125" y2="11.47241875" layer="21"/>
<rectangle x1="19.34718125" y1="11.38758125" x2="19.939" y2="11.47241875" layer="21"/>
<rectangle x1="22.56281875" y1="11.38758125" x2="24.257" y2="11.47241875" layer="21"/>
<rectangle x1="26.289" y1="11.38758125" x2="29.92881875" y2="11.47241875" layer="21"/>
<rectangle x1="0.127" y1="11.47241875" x2="1.05918125" y2="11.557" layer="21"/>
<rectangle x1="3.59918125" y1="11.47241875" x2="4.52881875" y2="11.557" layer="21"/>
<rectangle x1="7.747" y1="11.47241875" x2="8.59281875" y2="11.557" layer="21"/>
<rectangle x1="11.557" y1="11.47241875" x2="12.40281875" y2="11.557" layer="21"/>
<rectangle x1="13.50518125" y1="11.47241875" x2="14.605" y2="11.557" layer="21"/>
<rectangle x1="15.45081875" y1="11.47241875" x2="18.49881875" y2="11.557" layer="21"/>
<rectangle x1="19.34718125" y1="11.47241875" x2="19.939" y2="11.557" layer="21"/>
<rectangle x1="20.53081875" y1="11.47241875" x2="20.701" y2="11.557" layer="21"/>
<rectangle x1="22.479" y1="11.47241875" x2="24.34081875" y2="11.557" layer="21"/>
<rectangle x1="26.11881875" y1="11.47241875" x2="29.92881875" y2="11.557" layer="21"/>
<rectangle x1="0.127" y1="11.557" x2="1.05918125" y2="11.64158125" layer="21"/>
<rectangle x1="3.59918125" y1="11.557" x2="4.52881875" y2="11.64158125" layer="21"/>
<rectangle x1="7.747" y1="11.557" x2="8.59281875" y2="11.64158125" layer="21"/>
<rectangle x1="11.47318125" y1="11.557" x2="12.40281875" y2="11.64158125" layer="21"/>
<rectangle x1="13.50518125" y1="11.557" x2="14.605" y2="11.64158125" layer="21"/>
<rectangle x1="15.45081875" y1="11.557" x2="18.49881875" y2="11.64158125" layer="21"/>
<rectangle x1="19.34718125" y1="11.557" x2="19.939" y2="11.64158125" layer="21"/>
<rectangle x1="20.53081875" y1="11.557" x2="20.78481875" y2="11.64158125" layer="21"/>
<rectangle x1="22.30881875" y1="11.557" x2="24.511" y2="11.64158125" layer="21"/>
<rectangle x1="26.035" y1="11.557" x2="29.92881875" y2="11.64158125" layer="21"/>
<rectangle x1="0.21081875" y1="11.64158125" x2="1.05918125" y2="11.72641875" layer="21"/>
<rectangle x1="3.59918125" y1="11.64158125" x2="4.52881875" y2="11.72641875" layer="21"/>
<rectangle x1="7.747" y1="11.64158125" x2="8.59281875" y2="11.72641875" layer="21"/>
<rectangle x1="11.47318125" y1="11.64158125" x2="12.40281875" y2="11.72641875" layer="21"/>
<rectangle x1="13.589" y1="11.64158125" x2="14.605" y2="11.72641875" layer="21"/>
<rectangle x1="15.45081875" y1="11.64158125" x2="18.415" y2="11.72641875" layer="21"/>
<rectangle x1="19.26081875" y1="11.64158125" x2="20.02281875" y2="11.72641875" layer="21"/>
<rectangle x1="20.447" y1="11.64158125" x2="21.03881875" y2="11.72641875" layer="21"/>
<rectangle x1="22.14118125" y1="11.64158125" x2="24.68118125" y2="11.72641875" layer="21"/>
<rectangle x1="25.781" y1="11.64158125" x2="29.92881875" y2="11.72641875" layer="21"/>
<rectangle x1="0.21081875" y1="11.72641875" x2="1.05918125" y2="11.811" layer="21"/>
<rectangle x1="3.59918125" y1="11.72641875" x2="4.52881875" y2="11.811" layer="21"/>
<rectangle x1="7.747" y1="11.72641875" x2="8.59281875" y2="11.811" layer="21"/>
<rectangle x1="11.38681875" y1="11.72641875" x2="12.40281875" y2="11.811" layer="21"/>
<rectangle x1="13.589" y1="11.72641875" x2="14.68881875" y2="11.811" layer="21"/>
<rectangle x1="15.53718125" y1="11.72641875" x2="18.415" y2="11.811" layer="21"/>
<rectangle x1="19.26081875" y1="11.72641875" x2="20.10918125" y2="11.811" layer="21"/>
<rectangle x1="20.36318125" y1="11.72641875" x2="21.29281875" y2="11.811" layer="21"/>
<rectangle x1="21.88718125" y1="11.72641875" x2="24.93518125" y2="11.811" layer="21"/>
<rectangle x1="25.527" y1="11.72641875" x2="29.92881875" y2="11.811" layer="21"/>
<rectangle x1="0.21081875" y1="11.811" x2="1.05918125" y2="11.89558125" layer="21"/>
<rectangle x1="3.59918125" y1="11.811" x2="4.52881875" y2="11.89558125" layer="21"/>
<rectangle x1="7.747" y1="11.811" x2="8.59281875" y2="11.89558125" layer="21"/>
<rectangle x1="11.38681875" y1="11.811" x2="12.319" y2="11.89558125" layer="21"/>
<rectangle x1="13.589" y1="11.811" x2="14.68881875" y2="11.89558125" layer="21"/>
<rectangle x1="15.621" y1="11.811" x2="18.33118125" y2="11.89558125" layer="21"/>
<rectangle x1="19.26081875" y1="11.811" x2="29.92881875" y2="11.89558125" layer="21"/>
<rectangle x1="0.21081875" y1="11.89558125" x2="1.05918125" y2="11.98041875" layer="21"/>
<rectangle x1="3.59918125" y1="11.89558125" x2="4.52881875" y2="11.98041875" layer="21"/>
<rectangle x1="7.747" y1="11.89558125" x2="8.59281875" y2="11.98041875" layer="21"/>
<rectangle x1="11.303" y1="11.89558125" x2="12.319" y2="11.98041875" layer="21"/>
<rectangle x1="13.589" y1="11.89558125" x2="14.77518125" y2="11.98041875" layer="21"/>
<rectangle x1="15.621" y1="11.89558125" x2="18.24481875" y2="11.98041875" layer="21"/>
<rectangle x1="19.177" y1="11.89558125" x2="29.92881875" y2="11.98041875" layer="21"/>
<rectangle x1="0.21081875" y1="11.98041875" x2="1.143" y2="12.065" layer="21"/>
<rectangle x1="3.59918125" y1="11.98041875" x2="4.52881875" y2="12.065" layer="21"/>
<rectangle x1="7.747" y1="11.98041875" x2="8.59281875" y2="12.065" layer="21"/>
<rectangle x1="11.21918125" y1="11.98041875" x2="12.23518125" y2="12.065" layer="21"/>
<rectangle x1="13.589" y1="11.98041875" x2="14.77518125" y2="12.065" layer="21"/>
<rectangle x1="15.70481875" y1="11.98041875" x2="18.161" y2="12.065" layer="21"/>
<rectangle x1="19.177" y1="11.98041875" x2="29.92881875" y2="12.065" layer="21"/>
<rectangle x1="0.21081875" y1="12.065" x2="1.143" y2="12.14958125" layer="21"/>
<rectangle x1="3.59918125" y1="12.065" x2="4.52881875" y2="12.14958125" layer="21"/>
<rectangle x1="7.747" y1="12.065" x2="8.59281875" y2="12.14958125" layer="21"/>
<rectangle x1="11.13281875" y1="12.065" x2="12.23518125" y2="12.14958125" layer="21"/>
<rectangle x1="13.589" y1="12.065" x2="14.859" y2="12.14958125" layer="21"/>
<rectangle x1="15.79118125" y1="12.065" x2="18.07718125" y2="12.14958125" layer="21"/>
<rectangle x1="19.09318125" y1="12.065" x2="29.92881875" y2="12.14958125" layer="21"/>
<rectangle x1="0.29718125" y1="12.14958125" x2="1.143" y2="12.23441875" layer="21"/>
<rectangle x1="3.59918125" y1="12.14958125" x2="4.52881875" y2="12.23441875" layer="21"/>
<rectangle x1="7.747" y1="12.14958125" x2="8.59281875" y2="12.23441875" layer="21"/>
<rectangle x1="11.049" y1="12.14958125" x2="12.14881875" y2="12.23441875" layer="21"/>
<rectangle x1="13.589" y1="12.14958125" x2="14.859" y2="12.23441875" layer="21"/>
<rectangle x1="15.875" y1="12.14958125" x2="17.99081875" y2="12.23441875" layer="21"/>
<rectangle x1="19.09318125" y1="12.14958125" x2="29.92881875" y2="12.23441875" layer="21"/>
<rectangle x1="0.29718125" y1="12.23441875" x2="1.143" y2="12.319" layer="21"/>
<rectangle x1="3.59918125" y1="12.23441875" x2="4.52881875" y2="12.319" layer="21"/>
<rectangle x1="7.747" y1="12.23441875" x2="8.59281875" y2="12.319" layer="21"/>
<rectangle x1="10.87881875" y1="12.23441875" x2="12.065" y2="12.319" layer="21"/>
<rectangle x1="13.589" y1="12.23441875" x2="14.94281875" y2="12.319" layer="21"/>
<rectangle x1="16.04518125" y1="12.23441875" x2="17.907" y2="12.319" layer="21"/>
<rectangle x1="19.00681875" y1="12.23441875" x2="29.92881875" y2="12.319" layer="21"/>
<rectangle x1="0.29718125" y1="12.319" x2="1.22681875" y2="12.40358125" layer="21"/>
<rectangle x1="3.59918125" y1="12.319" x2="4.52881875" y2="12.40358125" layer="21"/>
<rectangle x1="7.747" y1="12.319" x2="8.59281875" y2="12.40358125" layer="21"/>
<rectangle x1="10.71118125" y1="12.319" x2="12.065" y2="12.40358125" layer="21"/>
<rectangle x1="13.589" y1="12.319" x2="15.02918125" y2="12.40358125" layer="21"/>
<rectangle x1="16.129" y1="12.319" x2="17.82318125" y2="12.40358125" layer="21"/>
<rectangle x1="18.923" y1="12.319" x2="29.92881875" y2="12.40358125" layer="21"/>
<rectangle x1="0.29718125" y1="12.40358125" x2="1.22681875" y2="12.48841875" layer="21"/>
<rectangle x1="3.59918125" y1="12.40358125" x2="4.52881875" y2="12.48841875" layer="21"/>
<rectangle x1="7.747" y1="12.40358125" x2="8.59281875" y2="12.48841875" layer="21"/>
<rectangle x1="10.541" y1="12.40358125" x2="11.98118125" y2="12.48841875" layer="21"/>
<rectangle x1="13.589" y1="12.40358125" x2="15.02918125" y2="12.48841875" layer="21"/>
<rectangle x1="16.29918125" y1="12.40358125" x2="17.653" y2="12.48841875" layer="21"/>
<rectangle x1="18.83918125" y1="12.40358125" x2="29.845" y2="12.48841875" layer="21"/>
<rectangle x1="0.381" y1="12.48841875" x2="1.22681875" y2="12.573" layer="21"/>
<rectangle x1="3.59918125" y1="12.48841875" x2="4.52881875" y2="12.573" layer="21"/>
<rectangle x1="7.747" y1="12.48841875" x2="11.89481875" y2="12.573" layer="21"/>
<rectangle x1="13.67281875" y1="12.48841875" x2="15.113" y2="12.573" layer="21"/>
<rectangle x1="16.55318125" y1="12.48841875" x2="17.31518125" y2="12.573" layer="21"/>
<rectangle x1="18.83918125" y1="12.48841875" x2="29.845" y2="12.573" layer="21"/>
<rectangle x1="0.381" y1="12.573" x2="1.31318125" y2="12.65758125" layer="21"/>
<rectangle x1="3.59918125" y1="12.573" x2="4.52881875" y2="12.65758125" layer="21"/>
<rectangle x1="7.747" y1="12.573" x2="11.811" y2="12.65758125" layer="21"/>
<rectangle x1="13.67281875" y1="12.573" x2="15.19681875" y2="12.65758125" layer="21"/>
<rectangle x1="18.75281875" y1="12.573" x2="29.845" y2="12.65758125" layer="21"/>
<rectangle x1="0.381" y1="12.65758125" x2="1.31318125" y2="12.74241875" layer="21"/>
<rectangle x1="3.59918125" y1="12.65758125" x2="4.52881875" y2="12.74241875" layer="21"/>
<rectangle x1="7.747" y1="12.65758125" x2="11.72718125" y2="12.74241875" layer="21"/>
<rectangle x1="13.67281875" y1="12.65758125" x2="15.28318125" y2="12.74241875" layer="21"/>
<rectangle x1="18.669" y1="12.65758125" x2="29.76118125" y2="12.74241875" layer="21"/>
<rectangle x1="0.381" y1="12.74241875" x2="1.31318125" y2="12.827" layer="21"/>
<rectangle x1="3.59918125" y1="12.74241875" x2="4.52881875" y2="12.827" layer="21"/>
<rectangle x1="7.747" y1="12.74241875" x2="11.64081875" y2="12.827" layer="21"/>
<rectangle x1="13.67281875" y1="12.74241875" x2="15.367" y2="12.827" layer="21"/>
<rectangle x1="18.49881875" y1="12.74241875" x2="29.76118125" y2="12.827" layer="21"/>
<rectangle x1="0.46481875" y1="12.827" x2="1.397" y2="12.91158125" layer="21"/>
<rectangle x1="3.59918125" y1="12.827" x2="4.52881875" y2="12.91158125" layer="21"/>
<rectangle x1="7.747" y1="12.827" x2="11.557" y2="12.91158125" layer="21"/>
<rectangle x1="13.67281875" y1="12.827" x2="15.53718125" y2="12.91158125" layer="21"/>
<rectangle x1="18.415" y1="12.827" x2="29.76118125" y2="12.91158125" layer="21"/>
<rectangle x1="0.46481875" y1="12.91158125" x2="1.397" y2="12.99641875" layer="21"/>
<rectangle x1="3.59918125" y1="12.91158125" x2="4.52881875" y2="12.99641875" layer="21"/>
<rectangle x1="7.747" y1="12.91158125" x2="11.38681875" y2="12.99641875" layer="21"/>
<rectangle x1="13.67281875" y1="12.91158125" x2="15.621" y2="12.99641875" layer="21"/>
<rectangle x1="18.33118125" y1="12.91158125" x2="29.67481875" y2="12.99641875" layer="21"/>
<rectangle x1="0.55118125" y1="12.99641875" x2="1.48081875" y2="13.081" layer="21"/>
<rectangle x1="3.59918125" y1="12.99641875" x2="4.445" y2="13.081" layer="21"/>
<rectangle x1="7.747" y1="12.99641875" x2="11.303" y2="13.081" layer="21"/>
<rectangle x1="13.67281875" y1="12.99641875" x2="15.79118125" y2="13.081" layer="21"/>
<rectangle x1="18.161" y1="12.99641875" x2="29.67481875" y2="13.081" layer="21"/>
<rectangle x1="0.55118125" y1="13.081" x2="1.48081875" y2="13.16558125" layer="21"/>
<rectangle x1="3.683" y1="13.081" x2="4.445" y2="13.16558125" layer="21"/>
<rectangle x1="7.83081875" y1="13.081" x2="11.13281875" y2="13.16558125" layer="21"/>
<rectangle x1="13.75918125" y1="13.081" x2="15.95881875" y2="13.16558125" layer="21"/>
<rectangle x1="17.99081875" y1="13.081" x2="29.67481875" y2="13.16558125" layer="21"/>
<rectangle x1="0.55118125" y1="13.16558125" x2="1.56718125" y2="13.25041875" layer="21"/>
<rectangle x1="3.683" y1="13.16558125" x2="4.445" y2="13.25041875" layer="21"/>
<rectangle x1="7.83081875" y1="13.16558125" x2="10.96518125" y2="13.25041875" layer="21"/>
<rectangle x1="13.75918125" y1="13.16558125" x2="16.129" y2="13.25041875" layer="21"/>
<rectangle x1="17.82318125" y1="13.16558125" x2="29.591" y2="13.25041875" layer="21"/>
<rectangle x1="0.635" y1="13.25041875" x2="1.56718125" y2="13.335" layer="21"/>
<rectangle x1="3.76681875" y1="13.25041875" x2="4.36118125" y2="13.335" layer="21"/>
<rectangle x1="7.91718125" y1="13.25041875" x2="10.71118125" y2="13.335" layer="21"/>
<rectangle x1="13.75918125" y1="13.25041875" x2="16.46681875" y2="13.335" layer="21"/>
<rectangle x1="17.48281875" y1="13.25041875" x2="29.591" y2="13.335" layer="21"/>
<rectangle x1="0.635" y1="13.335" x2="1.651" y2="13.41958125" layer="21"/>
<rectangle x1="3.937" y1="13.335" x2="4.191" y2="13.41958125" layer="21"/>
<rectangle x1="8.08481875" y1="13.335" x2="10.37081875" y2="13.41958125" layer="21"/>
<rectangle x1="13.75918125" y1="13.335" x2="29.50718125" y2="13.41958125" layer="21"/>
<rectangle x1="0.71881875" y1="13.41958125" x2="1.651" y2="13.50441875" layer="21"/>
<rectangle x1="13.75918125" y1="13.41958125" x2="29.50718125" y2="13.50441875" layer="21"/>
<rectangle x1="0.71881875" y1="13.50441875" x2="1.73481875" y2="13.589" layer="21"/>
<rectangle x1="13.843" y1="13.50441875" x2="29.42081875" y2="13.589" layer="21"/>
<rectangle x1="0.80518125" y1="13.589" x2="1.82118125" y2="13.67358125" layer="21"/>
<rectangle x1="13.843" y1="13.589" x2="29.42081875" y2="13.67358125" layer="21"/>
<rectangle x1="0.80518125" y1="13.67358125" x2="1.82118125" y2="13.75841875" layer="21"/>
<rectangle x1="13.843" y1="13.67358125" x2="29.337" y2="13.75841875" layer="21"/>
<rectangle x1="0.889" y1="13.75841875" x2="1.905" y2="13.843" layer="21"/>
<rectangle x1="13.843" y1="13.75841875" x2="29.337" y2="13.843" layer="21"/>
<rectangle x1="0.889" y1="13.843" x2="1.905" y2="13.92758125" layer="21"/>
<rectangle x1="13.92681875" y1="13.843" x2="29.25318125" y2="13.92758125" layer="21"/>
<rectangle x1="0.97281875" y1="13.92758125" x2="1.98881875" y2="14.01241875" layer="21"/>
<rectangle x1="13.92681875" y1="13.92758125" x2="29.25318125" y2="14.01241875" layer="21"/>
<rectangle x1="1.05918125" y1="14.01241875" x2="2.07518125" y2="14.097" layer="21"/>
<rectangle x1="13.92681875" y1="14.01241875" x2="29.16681875" y2="14.097" layer="21"/>
<rectangle x1="1.05918125" y1="14.097" x2="2.159" y2="14.18158125" layer="21"/>
<rectangle x1="13.92681875" y1="14.097" x2="29.083" y2="14.18158125" layer="21"/>
<rectangle x1="1.143" y1="14.18158125" x2="2.24281875" y2="14.26641875" layer="21"/>
<rectangle x1="13.92681875" y1="14.18158125" x2="29.083" y2="14.26641875" layer="21"/>
<rectangle x1="1.143" y1="14.26641875" x2="2.24281875" y2="14.351" layer="21"/>
<rectangle x1="14.01318125" y1="14.26641875" x2="28.99918125" y2="14.351" layer="21"/>
<rectangle x1="1.22681875" y1="14.351" x2="2.32918125" y2="14.43558125" layer="21"/>
<rectangle x1="14.01318125" y1="14.351" x2="28.91281875" y2="14.43558125" layer="21"/>
<rectangle x1="1.31318125" y1="14.43558125" x2="2.413" y2="14.52041875" layer="21"/>
<rectangle x1="14.01318125" y1="14.43558125" x2="28.91281875" y2="14.52041875" layer="21"/>
<rectangle x1="1.397" y1="14.52041875" x2="2.49681875" y2="14.605" layer="21"/>
<rectangle x1="14.097" y1="14.52041875" x2="28.829" y2="14.605" layer="21"/>
<rectangle x1="1.48081875" y1="14.605" x2="2.58318125" y2="14.68958125" layer="21"/>
<rectangle x1="14.097" y1="14.605" x2="28.74518125" y2="14.68958125" layer="21"/>
<rectangle x1="1.48081875" y1="14.68958125" x2="2.667" y2="14.77441875" layer="21"/>
<rectangle x1="14.18081875" y1="14.68958125" x2="28.65881875" y2="14.77441875" layer="21"/>
<rectangle x1="1.56718125" y1="14.77441875" x2="2.83718125" y2="14.859" layer="21"/>
<rectangle x1="14.18081875" y1="14.77441875" x2="28.575" y2="14.859" layer="21"/>
<rectangle x1="1.651" y1="14.859" x2="2.921" y2="14.94358125" layer="21"/>
<rectangle x1="14.18081875" y1="14.859" x2="28.49118125" y2="14.94358125" layer="21"/>
<rectangle x1="1.73481875" y1="14.94358125" x2="3.00481875" y2="15.02841875" layer="21"/>
<rectangle x1="14.26718125" y1="14.94358125" x2="28.49118125" y2="15.02841875" layer="21"/>
<rectangle x1="1.82118125" y1="15.02841875" x2="3.09118125" y2="15.113" layer="21"/>
<rectangle x1="14.26718125" y1="15.02841875" x2="28.40481875" y2="15.113" layer="21"/>
<rectangle x1="1.905" y1="15.113" x2="3.25881875" y2="15.19758125" layer="21"/>
<rectangle x1="14.26718125" y1="15.113" x2="28.321" y2="15.19758125" layer="21"/>
<rectangle x1="1.98881875" y1="15.19758125" x2="3.34518125" y2="15.28241875" layer="21"/>
<rectangle x1="14.351" y1="15.19758125" x2="28.23718125" y2="15.28241875" layer="21"/>
<rectangle x1="2.07518125" y1="15.28241875" x2="3.51281875" y2="15.367" layer="21"/>
<rectangle x1="14.351" y1="15.28241875" x2="28.15081875" y2="15.367" layer="21"/>
<rectangle x1="2.159" y1="15.367" x2="3.683" y2="15.45158125" layer="21"/>
<rectangle x1="14.43481875" y1="15.367" x2="28.067" y2="15.45158125" layer="21"/>
<rectangle x1="2.24281875" y1="15.45158125" x2="3.76681875" y2="15.53641875" layer="21"/>
<rectangle x1="14.43481875" y1="15.45158125" x2="27.89681875" y2="15.53641875" layer="21"/>
<rectangle x1="2.32918125" y1="15.53641875" x2="4.02081875" y2="15.621" layer="21"/>
<rectangle x1="14.52118125" y1="15.53641875" x2="27.813" y2="15.621" layer="21"/>
<rectangle x1="2.49681875" y1="15.621" x2="4.191" y2="15.70558125" layer="21"/>
<rectangle x1="14.52118125" y1="15.621" x2="27.72918125" y2="15.70558125" layer="21"/>
<rectangle x1="2.58318125" y1="15.70558125" x2="4.445" y2="15.79041875" layer="21"/>
<rectangle x1="14.605" y1="15.70558125" x2="27.64281875" y2="15.79041875" layer="21"/>
<rectangle x1="2.75081875" y1="15.79041875" x2="4.61518125" y2="15.875" layer="21"/>
<rectangle x1="14.68881875" y1="15.79041875" x2="27.47518125" y2="15.875" layer="21"/>
<rectangle x1="2.83718125" y1="15.875" x2="5.03681875" y2="15.95958125" layer="21"/>
<rectangle x1="14.68881875" y1="15.875" x2="27.38881875" y2="15.95958125" layer="21"/>
<rectangle x1="3.00481875" y1="15.95958125" x2="5.461" y2="16.04441875" layer="21"/>
<rectangle x1="14.77518125" y1="15.95958125" x2="27.22118125" y2="16.04441875" layer="21"/>
<rectangle x1="3.09118125" y1="16.04441875" x2="27.13481875" y2="16.129" layer="21"/>
<rectangle x1="3.25881875" y1="16.129" x2="26.96718125" y2="16.21358125" layer="21"/>
<rectangle x1="3.429" y1="16.21358125" x2="26.797" y2="16.29841875" layer="21"/>
<rectangle x1="3.59918125" y1="16.29841875" x2="26.62681875" y2="16.383" layer="21"/>
<rectangle x1="3.76681875" y1="16.383" x2="26.45918125" y2="16.46758125" layer="21"/>
<rectangle x1="3.937" y1="16.46758125" x2="26.20518125" y2="16.55241875" layer="21"/>
<rectangle x1="4.191" y1="16.55241875" x2="26.035" y2="16.637" layer="21"/>
<rectangle x1="4.445" y1="16.637" x2="25.69718125" y2="16.72158125" layer="21"/>
<rectangle x1="4.78281875" y1="16.72158125" x2="25.35681875" y2="16.80641875" layer="21"/>
<rectangle x1="5.207" y1="16.80641875" x2="25.019" y2="16.891" layer="21"/>
</package>
<package name="UDO-LOGO-40MM">
<rectangle x1="5.969" y1="0.71958125" x2="6.81481875" y2="0.80441875" layer="21"/>
<rectangle x1="28.321" y1="0.71958125" x2="29.083" y2="0.80441875" layer="21"/>
<rectangle x1="5.715" y1="0.80441875" x2="7.06881875" y2="0.889" layer="21"/>
<rectangle x1="28.067" y1="0.80441875" x2="29.42081875" y2="0.889" layer="21"/>
<rectangle x1="5.54481875" y1="0.889" x2="7.32281875" y2="0.97358125" layer="21"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.017" y2="0.97358125" layer="21"/>
<rectangle x1="11.049" y1="0.889" x2="11.303" y2="0.97358125" layer="21"/>
<rectangle x1="11.811" y1="0.889" x2="11.98118125" y2="0.97358125" layer="21"/>
<rectangle x1="13.843" y1="0.889" x2="14.18081875" y2="0.97358125" layer="21"/>
<rectangle x1="14.94281875" y1="0.889" x2="16.72081875" y2="0.97358125" layer="21"/>
<rectangle x1="19.60118125" y1="0.889" x2="20.10918125" y2="0.97358125" layer="21"/>
<rectangle x1="22.14118125" y1="0.889" x2="22.56281875" y2="0.97358125" layer="21"/>
<rectangle x1="24.59481875" y1="0.889" x2="25.10281875" y2="0.97358125" layer="21"/>
<rectangle x1="27.813" y1="0.889" x2="29.591" y2="0.97358125" layer="21"/>
<rectangle x1="30.94481875" y1="0.889" x2="31.19881875" y2="0.97358125" layer="21"/>
<rectangle x1="33.23081875" y1="0.889" x2="33.401" y2="0.97358125" layer="21"/>
<rectangle x1="34.84118125" y1="0.889" x2="35.34918125" y2="0.97358125" layer="21"/>
<rectangle x1="5.37718125" y1="0.97358125" x2="7.493" y2="1.05841875" layer="21"/>
<rectangle x1="8.763" y1="0.97358125" x2="9.10081875" y2="1.05841875" layer="21"/>
<rectangle x1="10.96518125" y1="0.97358125" x2="11.303" y2="1.05841875" layer="21"/>
<rectangle x1="11.72718125" y1="0.97358125" x2="12.065" y2="1.05841875" layer="21"/>
<rectangle x1="13.589" y1="0.97358125" x2="14.26718125" y2="1.05841875" layer="21"/>
<rectangle x1="14.94281875" y1="0.97358125" x2="16.97481875" y2="1.05841875" layer="21"/>
<rectangle x1="19.34718125" y1="0.97358125" x2="20.36318125" y2="1.05841875" layer="21"/>
<rectangle x1="21.971" y1="0.97358125" x2="22.90318125" y2="1.05841875" layer="21"/>
<rectangle x1="24.34081875" y1="0.97358125" x2="25.44318125" y2="1.05841875" layer="21"/>
<rectangle x1="27.64281875" y1="0.97358125" x2="29.76118125" y2="1.05841875" layer="21"/>
<rectangle x1="30.94481875" y1="0.97358125" x2="31.28518125" y2="1.05841875" layer="21"/>
<rectangle x1="33.147" y1="0.97358125" x2="33.48481875" y2="1.05841875" layer="21"/>
<rectangle x1="34.58718125" y1="0.97358125" x2="35.60318125" y2="1.05841875" layer="21"/>
<rectangle x1="5.29081875" y1="1.05841875" x2="6.05281875" y2="1.143" layer="21"/>
<rectangle x1="6.81481875" y1="1.05841875" x2="7.57681875" y2="1.143" layer="21"/>
<rectangle x1="8.763" y1="1.05841875" x2="9.10081875" y2="1.143" layer="21"/>
<rectangle x1="10.96518125" y1="1.05841875" x2="11.303" y2="1.143" layer="21"/>
<rectangle x1="11.72718125" y1="1.05841875" x2="12.065" y2="1.143" layer="21"/>
<rectangle x1="13.41881875" y1="1.05841875" x2="14.26718125" y2="1.143" layer="21"/>
<rectangle x1="14.859" y1="1.05841875" x2="17.22881875" y2="1.143" layer="21"/>
<rectangle x1="19.177" y1="1.05841875" x2="20.53081875" y2="1.143" layer="21"/>
<rectangle x1="21.80081875" y1="1.05841875" x2="23.07081875" y2="1.143" layer="21"/>
<rectangle x1="24.17318125" y1="1.05841875" x2="25.61081875" y2="1.143" layer="21"/>
<rectangle x1="27.559" y1="1.05841875" x2="28.40481875" y2="1.143" layer="21"/>
<rectangle x1="29.083" y1="1.05841875" x2="29.845" y2="1.143" layer="21"/>
<rectangle x1="30.94481875" y1="1.05841875" x2="31.28518125" y2="1.143" layer="21"/>
<rectangle x1="33.147" y1="1.05841875" x2="33.48481875" y2="1.143" layer="21"/>
<rectangle x1="34.417" y1="1.05841875" x2="35.77081875" y2="1.143" layer="21"/>
<rectangle x1="5.207" y1="1.143" x2="5.79881875" y2="1.22758125" layer="21"/>
<rectangle x1="6.985" y1="1.143" x2="7.66318125" y2="1.22758125" layer="21"/>
<rectangle x1="8.763" y1="1.143" x2="9.10081875" y2="1.22758125" layer="21"/>
<rectangle x1="10.96518125" y1="1.143" x2="11.303" y2="1.22758125" layer="21"/>
<rectangle x1="11.72718125" y1="1.143" x2="12.065" y2="1.22758125" layer="21"/>
<rectangle x1="13.25118125" y1="1.143" x2="14.26718125" y2="1.22758125" layer="21"/>
<rectangle x1="14.859" y1="1.143" x2="17.31518125" y2="1.22758125" layer="21"/>
<rectangle x1="19.09318125" y1="1.143" x2="20.701" y2="1.22758125" layer="21"/>
<rectangle x1="21.717" y1="1.143" x2="23.07081875" y2="1.22758125" layer="21"/>
<rectangle x1="24.08681875" y1="1.143" x2="25.69718125" y2="1.22758125" layer="21"/>
<rectangle x1="27.47518125" y1="1.143" x2="28.067" y2="1.22758125" layer="21"/>
<rectangle x1="29.337" y1="1.143" x2="29.92881875" y2="1.22758125" layer="21"/>
<rectangle x1="30.94481875" y1="1.143" x2="31.28518125" y2="1.22758125" layer="21"/>
<rectangle x1="33.147" y1="1.143" x2="33.48481875" y2="1.22758125" layer="21"/>
<rectangle x1="34.33318125" y1="1.143" x2="35.941" y2="1.22758125" layer="21"/>
<rectangle x1="5.03681875" y1="1.22758125" x2="5.63118125" y2="1.31241875" layer="21"/>
<rectangle x1="7.239" y1="1.22758125" x2="7.747" y2="1.31241875" layer="21"/>
<rectangle x1="8.763" y1="1.22758125" x2="9.10081875" y2="1.31241875" layer="21"/>
<rectangle x1="10.96518125" y1="1.22758125" x2="11.303" y2="1.31241875" layer="21"/>
<rectangle x1="11.72718125" y1="1.22758125" x2="12.065" y2="1.31241875" layer="21"/>
<rectangle x1="13.16481875" y1="1.22758125" x2="13.843" y2="1.31241875" layer="21"/>
<rectangle x1="14.859" y1="1.22758125" x2="15.28318125" y2="1.31241875" layer="21"/>
<rectangle x1="16.637" y1="1.22758125" x2="17.48281875" y2="1.31241875" layer="21"/>
<rectangle x1="18.923" y1="1.22758125" x2="19.685" y2="1.31241875" layer="21"/>
<rectangle x1="20.02281875" y1="1.22758125" x2="20.78481875" y2="1.31241875" layer="21"/>
<rectangle x1="21.63318125" y1="1.22758125" x2="22.225" y2="1.31241875" layer="21"/>
<rectangle x1="22.56281875" y1="1.22758125" x2="23.15718125" y2="1.31241875" layer="21"/>
<rectangle x1="24.003" y1="1.22758125" x2="24.68118125" y2="1.31241875" layer="21"/>
<rectangle x1="25.10281875" y1="1.22758125" x2="25.781" y2="1.31241875" layer="21"/>
<rectangle x1="27.38881875" y1="1.22758125" x2="27.98318125" y2="1.31241875" layer="21"/>
<rectangle x1="29.50718125" y1="1.22758125" x2="30.01518125" y2="1.31241875" layer="21"/>
<rectangle x1="30.94481875" y1="1.22758125" x2="31.28518125" y2="1.31241875" layer="21"/>
<rectangle x1="33.147" y1="1.22758125" x2="33.48481875" y2="1.31241875" layer="21"/>
<rectangle x1="34.163" y1="1.22758125" x2="34.925" y2="1.31241875" layer="21"/>
<rectangle x1="35.26281875" y1="1.22758125" x2="36.02481875" y2="1.31241875" layer="21"/>
<rectangle x1="5.03681875" y1="1.31241875" x2="5.54481875" y2="1.397" layer="21"/>
<rectangle x1="7.32281875" y1="1.31241875" x2="7.83081875" y2="1.397" layer="21"/>
<rectangle x1="8.763" y1="1.31241875" x2="9.10081875" y2="1.397" layer="21"/>
<rectangle x1="10.96518125" y1="1.31241875" x2="11.303" y2="1.397" layer="21"/>
<rectangle x1="11.72718125" y1="1.31241875" x2="12.065" y2="1.397" layer="21"/>
<rectangle x1="13.081" y1="1.31241875" x2="13.67281875" y2="1.397" layer="21"/>
<rectangle x1="14.94281875" y1="1.31241875" x2="15.19681875" y2="1.397" layer="21"/>
<rectangle x1="16.891" y1="1.31241875" x2="17.56918125" y2="1.397" layer="21"/>
<rectangle x1="18.923" y1="1.31241875" x2="19.431" y2="1.397" layer="21"/>
<rectangle x1="20.27681875" y1="1.31241875" x2="20.87118125" y2="1.397" layer="21"/>
<rectangle x1="21.54681875" y1="1.31241875" x2="22.05481875" y2="1.397" layer="21"/>
<rectangle x1="22.733" y1="1.31241875" x2="23.241" y2="1.397" layer="21"/>
<rectangle x1="23.91918125" y1="1.31241875" x2="24.42718125" y2="1.397" layer="21"/>
<rectangle x1="25.273" y1="1.31241875" x2="25.86481875" y2="1.397" layer="21"/>
<rectangle x1="27.305" y1="1.31241875" x2="27.813" y2="1.397" layer="21"/>
<rectangle x1="29.591" y1="1.31241875" x2="30.099" y2="1.397" layer="21"/>
<rectangle x1="30.94481875" y1="1.31241875" x2="31.28518125" y2="1.397" layer="21"/>
<rectangle x1="33.147" y1="1.31241875" x2="33.48481875" y2="1.397" layer="21"/>
<rectangle x1="34.163" y1="1.31241875" x2="34.671" y2="1.397" layer="21"/>
<rectangle x1="35.51681875" y1="1.31241875" x2="36.11118125" y2="1.397" layer="21"/>
<rectangle x1="4.953" y1="1.397" x2="5.37718125" y2="1.48158125" layer="21"/>
<rectangle x1="7.40918125" y1="1.397" x2="7.91718125" y2="1.48158125" layer="21"/>
<rectangle x1="8.763" y1="1.397" x2="9.10081875" y2="1.48158125" layer="21"/>
<rectangle x1="10.96518125" y1="1.397" x2="11.303" y2="1.48158125" layer="21"/>
<rectangle x1="11.72718125" y1="1.397" x2="12.065" y2="1.48158125" layer="21"/>
<rectangle x1="13.081" y1="1.397" x2="13.50518125" y2="1.48158125" layer="21"/>
<rectangle x1="14.859" y1="1.397" x2="15.19681875" y2="1.48158125" layer="21"/>
<rectangle x1="17.06118125" y1="1.397" x2="17.653" y2="1.48158125" layer="21"/>
<rectangle x1="18.83918125" y1="1.397" x2="19.26081875" y2="1.48158125" layer="21"/>
<rectangle x1="20.447" y1="1.397" x2="20.87118125" y2="1.48158125" layer="21"/>
<rectangle x1="21.54681875" y1="1.397" x2="21.971" y2="1.48158125" layer="21"/>
<rectangle x1="22.90318125" y1="1.397" x2="23.241" y2="1.48158125" layer="21"/>
<rectangle x1="23.83281875" y1="1.397" x2="24.34081875" y2="1.48158125" layer="21"/>
<rectangle x1="25.44318125" y1="1.397" x2="25.95118125" y2="1.48158125" layer="21"/>
<rectangle x1="27.22118125" y1="1.397" x2="27.72918125" y2="1.48158125" layer="21"/>
<rectangle x1="29.76118125" y1="1.397" x2="30.18281875" y2="1.48158125" layer="21"/>
<rectangle x1="30.94481875" y1="1.397" x2="31.28518125" y2="1.48158125" layer="21"/>
<rectangle x1="33.147" y1="1.397" x2="33.48481875" y2="1.48158125" layer="21"/>
<rectangle x1="34.07918125" y1="1.397" x2="34.50081875" y2="1.48158125" layer="21"/>
<rectangle x1="35.687" y1="1.397" x2="36.11118125" y2="1.48158125" layer="21"/>
<rectangle x1="4.86918125" y1="1.48158125" x2="5.29081875" y2="1.56641875" layer="21"/>
<rectangle x1="7.57681875" y1="1.48158125" x2="8.001" y2="1.56641875" layer="21"/>
<rectangle x1="8.763" y1="1.48158125" x2="9.10081875" y2="1.56641875" layer="21"/>
<rectangle x1="10.96518125" y1="1.48158125" x2="11.303" y2="1.56641875" layer="21"/>
<rectangle x1="11.72718125" y1="1.48158125" x2="12.065" y2="1.56641875" layer="21"/>
<rectangle x1="12.99718125" y1="1.48158125" x2="13.41881875" y2="1.56641875" layer="21"/>
<rectangle x1="14.859" y1="1.48158125" x2="15.28318125" y2="1.56641875" layer="21"/>
<rectangle x1="17.22881875" y1="1.48158125" x2="17.73681875" y2="1.56641875" layer="21"/>
<rectangle x1="18.75281875" y1="1.48158125" x2="19.177" y2="1.56641875" layer="21"/>
<rectangle x1="20.53081875" y1="1.48158125" x2="20.955" y2="1.56641875" layer="21"/>
<rectangle x1="21.54681875" y1="1.48158125" x2="21.88718125" y2="1.56641875" layer="21"/>
<rectangle x1="22.90318125" y1="1.48158125" x2="23.241" y2="1.56641875" layer="21"/>
<rectangle x1="23.749" y1="1.48158125" x2="24.257" y2="1.56641875" layer="21"/>
<rectangle x1="25.527" y1="1.48158125" x2="25.95118125" y2="1.56641875" layer="21"/>
<rectangle x1="27.13481875" y1="1.48158125" x2="27.64281875" y2="1.56641875" layer="21"/>
<rectangle x1="29.845" y1="1.48158125" x2="30.26918125" y2="1.56641875" layer="21"/>
<rectangle x1="30.94481875" y1="1.48158125" x2="31.28518125" y2="1.56641875" layer="21"/>
<rectangle x1="33.147" y1="1.48158125" x2="33.48481875" y2="1.56641875" layer="21"/>
<rectangle x1="33.99281875" y1="1.48158125" x2="34.417" y2="1.56641875" layer="21"/>
<rectangle x1="35.77081875" y1="1.48158125" x2="36.195" y2="1.56641875" layer="21"/>
<rectangle x1="4.78281875" y1="1.56641875" x2="5.207" y2="1.651" layer="21"/>
<rectangle x1="7.57681875" y1="1.56641875" x2="8.001" y2="1.651" layer="21"/>
<rectangle x1="8.763" y1="1.56641875" x2="9.10081875" y2="1.651" layer="21"/>
<rectangle x1="10.96518125" y1="1.56641875" x2="11.303" y2="1.651" layer="21"/>
<rectangle x1="11.72718125" y1="1.56641875" x2="12.065" y2="1.651" layer="21"/>
<rectangle x1="12.91081875" y1="1.56641875" x2="13.335" y2="1.651" layer="21"/>
<rectangle x1="14.859" y1="1.56641875" x2="15.28318125" y2="1.651" layer="21"/>
<rectangle x1="17.31518125" y1="1.56641875" x2="17.82318125" y2="1.651" layer="21"/>
<rectangle x1="18.669" y1="1.56641875" x2="19.09318125" y2="1.651" layer="21"/>
<rectangle x1="20.61718125" y1="1.56641875" x2="21.03881875" y2="1.651" layer="21"/>
<rectangle x1="21.54681875" y1="1.56641875" x2="21.80081875" y2="1.651" layer="21"/>
<rectangle x1="22.987" y1="1.56641875" x2="23.241" y2="1.651" layer="21"/>
<rectangle x1="23.749" y1="1.56641875" x2="24.17318125" y2="1.651" layer="21"/>
<rectangle x1="25.61081875" y1="1.56641875" x2="26.035" y2="1.651" layer="21"/>
<rectangle x1="27.13481875" y1="1.56641875" x2="27.47518125" y2="1.651" layer="21"/>
<rectangle x1="29.92881875" y1="1.56641875" x2="30.353" y2="1.651" layer="21"/>
<rectangle x1="30.94481875" y1="1.56641875" x2="31.28518125" y2="1.651" layer="21"/>
<rectangle x1="33.147" y1="1.56641875" x2="33.48481875" y2="1.651" layer="21"/>
<rectangle x1="33.99281875" y1="1.56641875" x2="34.33318125" y2="1.651" layer="21"/>
<rectangle x1="35.85718125" y1="1.56641875" x2="36.27881875" y2="1.651" layer="21"/>
<rectangle x1="4.78281875" y1="1.651" x2="5.207" y2="1.73558125" layer="21"/>
<rectangle x1="7.66318125" y1="1.651" x2="8.08481875" y2="1.73558125" layer="21"/>
<rectangle x1="8.763" y1="1.651" x2="9.10081875" y2="1.73558125" layer="21"/>
<rectangle x1="10.96518125" y1="1.651" x2="11.303" y2="1.73558125" layer="21"/>
<rectangle x1="11.72718125" y1="1.651" x2="12.065" y2="1.73558125" layer="21"/>
<rectangle x1="12.91081875" y1="1.651" x2="13.25118125" y2="1.73558125" layer="21"/>
<rectangle x1="14.859" y1="1.651" x2="15.19681875" y2="1.73558125" layer="21"/>
<rectangle x1="17.48281875" y1="1.651" x2="17.907" y2="1.73558125" layer="21"/>
<rectangle x1="18.669" y1="1.651" x2="19.09318125" y2="1.73558125" layer="21"/>
<rectangle x1="20.701" y1="1.651" x2="21.03881875" y2="1.73558125" layer="21"/>
<rectangle x1="22.90318125" y1="1.651" x2="23.241" y2="1.73558125" layer="21"/>
<rectangle x1="23.66518125" y1="1.651" x2="24.08681875" y2="1.73558125" layer="21"/>
<rectangle x1="25.69718125" y1="1.651" x2="26.035" y2="1.73558125" layer="21"/>
<rectangle x1="27.051" y1="1.651" x2="27.47518125" y2="1.73558125" layer="21"/>
<rectangle x1="30.01518125" y1="1.651" x2="30.353" y2="1.73558125" layer="21"/>
<rectangle x1="30.94481875" y1="1.651" x2="31.28518125" y2="1.73558125" layer="21"/>
<rectangle x1="33.147" y1="1.651" x2="33.48481875" y2="1.73558125" layer="21"/>
<rectangle x1="33.909" y1="1.651" x2="34.33318125" y2="1.73558125" layer="21"/>
<rectangle x1="35.941" y1="1.651" x2="36.27881875" y2="1.73558125" layer="21"/>
<rectangle x1="4.699" y1="1.73558125" x2="5.12318125" y2="1.82041875" layer="21"/>
<rectangle x1="7.747" y1="1.73558125" x2="8.17118125" y2="1.82041875" layer="21"/>
<rectangle x1="8.763" y1="1.73558125" x2="9.10081875" y2="1.82041875" layer="21"/>
<rectangle x1="10.96518125" y1="1.73558125" x2="11.303" y2="1.82041875" layer="21"/>
<rectangle x1="11.72718125" y1="1.73558125" x2="12.065" y2="1.82041875" layer="21"/>
<rectangle x1="12.91081875" y1="1.73558125" x2="13.25118125" y2="1.82041875" layer="21"/>
<rectangle x1="14.859" y1="1.73558125" x2="15.28318125" y2="1.82041875" layer="21"/>
<rectangle x1="17.56918125" y1="1.73558125" x2="17.99081875" y2="1.82041875" layer="21"/>
<rectangle x1="18.669" y1="1.73558125" x2="19.00681875" y2="1.82041875" layer="21"/>
<rectangle x1="20.701" y1="1.73558125" x2="21.12518125" y2="1.82041875" layer="21"/>
<rectangle x1="22.90318125" y1="1.73558125" x2="23.241" y2="1.82041875" layer="21"/>
<rectangle x1="23.66518125" y1="1.73558125" x2="24.003" y2="1.82041875" layer="21"/>
<rectangle x1="25.781" y1="1.73558125" x2="25.95118125" y2="1.82041875" layer="21"/>
<rectangle x1="26.96718125" y1="1.73558125" x2="27.38881875" y2="1.82041875" layer="21"/>
<rectangle x1="30.01518125" y1="1.73558125" x2="30.43681875" y2="1.82041875" layer="21"/>
<rectangle x1="30.94481875" y1="1.73558125" x2="31.28518125" y2="1.82041875" layer="21"/>
<rectangle x1="33.147" y1="1.73558125" x2="33.48481875" y2="1.82041875" layer="21"/>
<rectangle x1="33.909" y1="1.73558125" x2="34.24681875" y2="1.82041875" layer="21"/>
<rectangle x1="35.941" y1="1.73558125" x2="36.195" y2="1.82041875" layer="21"/>
<rectangle x1="4.699" y1="1.82041875" x2="5.03681875" y2="1.905" layer="21"/>
<rectangle x1="7.83081875" y1="1.82041875" x2="8.17118125" y2="1.905" layer="21"/>
<rectangle x1="8.763" y1="1.82041875" x2="9.10081875" y2="1.905" layer="21"/>
<rectangle x1="10.96518125" y1="1.82041875" x2="11.303" y2="1.905" layer="21"/>
<rectangle x1="11.72718125" y1="1.82041875" x2="12.065" y2="1.905" layer="21"/>
<rectangle x1="12.827" y1="1.82041875" x2="13.16481875" y2="1.905" layer="21"/>
<rectangle x1="14.859" y1="1.82041875" x2="15.28318125" y2="1.905" layer="21"/>
<rectangle x1="17.56918125" y1="1.82041875" x2="17.99081875" y2="1.905" layer="21"/>
<rectangle x1="18.58518125" y1="1.82041875" x2="19.00681875" y2="1.905" layer="21"/>
<rectangle x1="20.78481875" y1="1.82041875" x2="21.12518125" y2="1.905" layer="21"/>
<rectangle x1="22.733" y1="1.82041875" x2="23.241" y2="1.905" layer="21"/>
<rectangle x1="23.66518125" y1="1.82041875" x2="24.003" y2="1.905" layer="21"/>
<rectangle x1="26.96718125" y1="1.82041875" x2="27.305" y2="1.905" layer="21"/>
<rectangle x1="30.099" y1="1.82041875" x2="30.43681875" y2="1.905" layer="21"/>
<rectangle x1="30.94481875" y1="1.82041875" x2="31.28518125" y2="1.905" layer="21"/>
<rectangle x1="33.147" y1="1.82041875" x2="33.48481875" y2="1.905" layer="21"/>
<rectangle x1="33.82518125" y1="1.82041875" x2="34.163" y2="1.905" layer="21"/>
<rectangle x1="4.61518125" y1="1.905" x2="5.03681875" y2="1.98958125" layer="21"/>
<rectangle x1="7.83081875" y1="1.905" x2="8.255" y2="1.98958125" layer="21"/>
<rectangle x1="8.763" y1="1.905" x2="9.10081875" y2="1.98958125" layer="21"/>
<rectangle x1="10.96518125" y1="1.905" x2="11.303" y2="1.98958125" layer="21"/>
<rectangle x1="11.72718125" y1="1.905" x2="12.065" y2="1.98958125" layer="21"/>
<rectangle x1="12.827" y1="1.905" x2="13.16481875" y2="1.98958125" layer="21"/>
<rectangle x1="14.859" y1="1.905" x2="15.28318125" y2="1.98958125" layer="21"/>
<rectangle x1="17.653" y1="1.905" x2="18.07718125" y2="1.98958125" layer="21"/>
<rectangle x1="18.58518125" y1="1.905" x2="18.923" y2="1.98958125" layer="21"/>
<rectangle x1="20.78481875" y1="1.905" x2="21.12518125" y2="1.98958125" layer="21"/>
<rectangle x1="22.64918125" y1="1.905" x2="23.15718125" y2="1.98958125" layer="21"/>
<rectangle x1="23.57881875" y1="1.905" x2="23.91918125" y2="1.98958125" layer="21"/>
<rectangle x1="26.96718125" y1="1.905" x2="27.305" y2="1.98958125" layer="21"/>
<rectangle x1="30.099" y1="1.905" x2="30.52318125" y2="1.98958125" layer="21"/>
<rectangle x1="30.94481875" y1="1.905" x2="31.28518125" y2="1.98958125" layer="21"/>
<rectangle x1="33.147" y1="1.905" x2="33.48481875" y2="1.98958125" layer="21"/>
<rectangle x1="33.82518125" y1="1.905" x2="34.163" y2="1.98958125" layer="21"/>
<rectangle x1="4.61518125" y1="1.98958125" x2="4.953" y2="2.07441875" layer="21"/>
<rectangle x1="7.91718125" y1="1.98958125" x2="8.255" y2="2.07441875" layer="21"/>
<rectangle x1="8.763" y1="1.98958125" x2="9.10081875" y2="2.07441875" layer="21"/>
<rectangle x1="10.96518125" y1="1.98958125" x2="11.303" y2="2.07441875" layer="21"/>
<rectangle x1="11.72718125" y1="1.98958125" x2="12.065" y2="2.07441875" layer="21"/>
<rectangle x1="12.827" y1="1.98958125" x2="13.16481875" y2="2.07441875" layer="21"/>
<rectangle x1="14.859" y1="1.98958125" x2="15.19681875" y2="2.07441875" layer="21"/>
<rectangle x1="17.73681875" y1="1.98958125" x2="18.07718125" y2="2.07441875" layer="21"/>
<rectangle x1="18.58518125" y1="1.98958125" x2="18.923" y2="2.07441875" layer="21"/>
<rectangle x1="20.78481875" y1="1.98958125" x2="21.12518125" y2="2.07441875" layer="21"/>
<rectangle x1="22.39518125" y1="1.98958125" x2="23.07081875" y2="2.07441875" layer="21"/>
<rectangle x1="23.57881875" y1="1.98958125" x2="25.86481875" y2="2.07441875" layer="21"/>
<rectangle x1="26.88081875" y1="1.98958125" x2="27.22118125" y2="2.07441875" layer="21"/>
<rectangle x1="30.18281875" y1="1.98958125" x2="30.52318125" y2="2.07441875" layer="21"/>
<rectangle x1="30.94481875" y1="1.98958125" x2="31.28518125" y2="2.07441875" layer="21"/>
<rectangle x1="33.147" y1="1.98958125" x2="33.48481875" y2="2.07441875" layer="21"/>
<rectangle x1="33.82518125" y1="1.98958125" x2="34.163" y2="2.07441875" layer="21"/>
<rectangle x1="34.33318125" y1="1.98958125" x2="35.941" y2="2.07441875" layer="21"/>
<rectangle x1="36.02481875" y1="1.98958125" x2="36.27881875" y2="2.07441875" layer="21"/>
<rectangle x1="4.61518125" y1="2.07441875" x2="4.953" y2="2.159" layer="21"/>
<rectangle x1="7.91718125" y1="2.07441875" x2="8.255" y2="2.159" layer="21"/>
<rectangle x1="8.763" y1="2.07441875" x2="9.10081875" y2="2.159" layer="21"/>
<rectangle x1="10.96518125" y1="2.07441875" x2="11.303" y2="2.159" layer="21"/>
<rectangle x1="11.72718125" y1="2.07441875" x2="12.065" y2="2.159" layer="21"/>
<rectangle x1="12.827" y1="2.07441875" x2="13.16481875" y2="2.159" layer="21"/>
<rectangle x1="14.859" y1="2.07441875" x2="15.19681875" y2="2.159" layer="21"/>
<rectangle x1="17.73681875" y1="2.07441875" x2="18.161" y2="2.159" layer="21"/>
<rectangle x1="18.58518125" y1="2.07441875" x2="18.923" y2="2.159" layer="21"/>
<rectangle x1="20.78481875" y1="2.07441875" x2="21.12518125" y2="2.159" layer="21"/>
<rectangle x1="22.14118125" y1="2.07441875" x2="22.987" y2="2.159" layer="21"/>
<rectangle x1="23.57881875" y1="2.07441875" x2="26.11881875" y2="2.159" layer="21"/>
<rectangle x1="26.88081875" y1="2.07441875" x2="27.22118125" y2="2.159" layer="21"/>
<rectangle x1="30.18281875" y1="2.07441875" x2="30.52318125" y2="2.159" layer="21"/>
<rectangle x1="30.94481875" y1="2.07441875" x2="31.28518125" y2="2.159" layer="21"/>
<rectangle x1="33.147" y1="2.07441875" x2="33.48481875" y2="2.159" layer="21"/>
<rectangle x1="33.82518125" y1="2.07441875" x2="36.36518125" y2="2.159" layer="21"/>
<rectangle x1="4.52881875" y1="2.159" x2="4.953" y2="2.24358125" layer="21"/>
<rectangle x1="7.91718125" y1="2.159" x2="8.255" y2="2.24358125" layer="21"/>
<rectangle x1="8.763" y1="2.159" x2="9.10081875" y2="2.24358125" layer="21"/>
<rectangle x1="10.96518125" y1="2.159" x2="11.303" y2="2.24358125" layer="21"/>
<rectangle x1="11.72718125" y1="2.159" x2="12.065" y2="2.24358125" layer="21"/>
<rectangle x1="12.827" y1="2.159" x2="13.16481875" y2="2.24358125" layer="21"/>
<rectangle x1="14.859" y1="2.159" x2="15.19681875" y2="2.24358125" layer="21"/>
<rectangle x1="17.82318125" y1="2.159" x2="18.161" y2="2.24358125" layer="21"/>
<rectangle x1="18.58518125" y1="2.159" x2="18.923" y2="2.24358125" layer="21"/>
<rectangle x1="20.78481875" y1="2.159" x2="21.12518125" y2="2.24358125" layer="21"/>
<rectangle x1="21.971" y1="2.159" x2="22.90318125" y2="2.24358125" layer="21"/>
<rectangle x1="23.57881875" y1="2.159" x2="26.20518125" y2="2.24358125" layer="21"/>
<rectangle x1="26.88081875" y1="2.159" x2="27.22118125" y2="2.24358125" layer="21"/>
<rectangle x1="30.18281875" y1="2.159" x2="30.607" y2="2.24358125" layer="21"/>
<rectangle x1="30.94481875" y1="2.159" x2="31.28518125" y2="2.24358125" layer="21"/>
<rectangle x1="33.147" y1="2.159" x2="33.48481875" y2="2.24358125" layer="21"/>
<rectangle x1="33.82518125" y1="2.159" x2="36.36518125" y2="2.24358125" layer="21"/>
<rectangle x1="4.52881875" y1="2.24358125" x2="4.86918125" y2="2.32841875" layer="21"/>
<rectangle x1="7.91718125" y1="2.24358125" x2="8.33881875" y2="2.32841875" layer="21"/>
<rectangle x1="8.763" y1="2.24358125" x2="9.10081875" y2="2.32841875" layer="21"/>
<rectangle x1="10.96518125" y1="2.24358125" x2="11.303" y2="2.32841875" layer="21"/>
<rectangle x1="11.72718125" y1="2.24358125" x2="12.065" y2="2.32841875" layer="21"/>
<rectangle x1="12.827" y1="2.24358125" x2="13.16481875" y2="2.32841875" layer="21"/>
<rectangle x1="14.859" y1="2.24358125" x2="15.19681875" y2="2.32841875" layer="21"/>
<rectangle x1="17.82318125" y1="2.24358125" x2="18.161" y2="2.32841875" layer="21"/>
<rectangle x1="18.58518125" y1="2.24358125" x2="18.923" y2="2.32841875" layer="21"/>
<rectangle x1="20.78481875" y1="2.24358125" x2="21.12518125" y2="2.32841875" layer="21"/>
<rectangle x1="21.717" y1="2.24358125" x2="22.64918125" y2="2.32841875" layer="21"/>
<rectangle x1="23.57881875" y1="2.24358125" x2="26.20518125" y2="2.32841875" layer="21"/>
<rectangle x1="26.797" y1="2.24358125" x2="27.22118125" y2="2.32841875" layer="21"/>
<rectangle x1="30.26918125" y1="2.24358125" x2="30.607" y2="2.32841875" layer="21"/>
<rectangle x1="30.94481875" y1="2.24358125" x2="31.28518125" y2="2.32841875" layer="21"/>
<rectangle x1="33.147" y1="2.24358125" x2="33.48481875" y2="2.32841875" layer="21"/>
<rectangle x1="33.82518125" y1="2.24358125" x2="36.36518125" y2="2.32841875" layer="21"/>
<rectangle x1="4.52881875" y1="2.32841875" x2="4.86918125" y2="2.413" layer="21"/>
<rectangle x1="8.001" y1="2.32841875" x2="8.33881875" y2="2.413" layer="21"/>
<rectangle x1="8.763" y1="2.32841875" x2="9.10081875" y2="2.413" layer="21"/>
<rectangle x1="10.96518125" y1="2.32841875" x2="11.303" y2="2.413" layer="21"/>
<rectangle x1="11.72718125" y1="2.32841875" x2="12.065" y2="2.413" layer="21"/>
<rectangle x1="12.827" y1="2.32841875" x2="13.16481875" y2="2.413" layer="21"/>
<rectangle x1="14.859" y1="2.32841875" x2="15.19681875" y2="2.413" layer="21"/>
<rectangle x1="17.82318125" y1="2.32841875" x2="18.24481875" y2="2.413" layer="21"/>
<rectangle x1="18.58518125" y1="2.32841875" x2="18.923" y2="2.413" layer="21"/>
<rectangle x1="20.78481875" y1="2.32841875" x2="21.12518125" y2="2.413" layer="21"/>
<rectangle x1="21.63318125" y1="2.32841875" x2="22.479" y2="2.413" layer="21"/>
<rectangle x1="23.57881875" y1="2.32841875" x2="24.257" y2="2.413" layer="21"/>
<rectangle x1="24.34081875" y1="2.32841875" x2="26.20518125" y2="2.413" layer="21"/>
<rectangle x1="26.797" y1="2.32841875" x2="27.13481875" y2="2.413" layer="21"/>
<rectangle x1="30.26918125" y1="2.32841875" x2="30.607" y2="2.413" layer="21"/>
<rectangle x1="30.94481875" y1="2.32841875" x2="31.28518125" y2="2.413" layer="21"/>
<rectangle x1="33.147" y1="2.32841875" x2="33.48481875" y2="2.413" layer="21"/>
<rectangle x1="33.82518125" y1="2.32841875" x2="35.179" y2="2.413" layer="21"/>
<rectangle x1="35.34918125" y1="2.32841875" x2="35.60318125" y2="2.413" layer="21"/>
<rectangle x1="35.77081875" y1="2.32841875" x2="35.941" y2="2.413" layer="21"/>
<rectangle x1="36.02481875" y1="2.32841875" x2="36.36518125" y2="2.413" layer="21"/>
<rectangle x1="4.52881875" y1="2.413" x2="4.86918125" y2="2.49758125" layer="21"/>
<rectangle x1="8.001" y1="2.413" x2="8.33881875" y2="2.49758125" layer="21"/>
<rectangle x1="8.763" y1="2.413" x2="9.10081875" y2="2.49758125" layer="21"/>
<rectangle x1="10.96518125" y1="2.413" x2="11.303" y2="2.49758125" layer="21"/>
<rectangle x1="11.72718125" y1="2.413" x2="12.065" y2="2.49758125" layer="21"/>
<rectangle x1="12.827" y1="2.413" x2="13.16481875" y2="2.49758125" layer="21"/>
<rectangle x1="14.859" y1="2.413" x2="15.19681875" y2="2.49758125" layer="21"/>
<rectangle x1="17.907" y1="2.413" x2="18.24481875" y2="2.49758125" layer="21"/>
<rectangle x1="18.58518125" y1="2.413" x2="18.923" y2="2.49758125" layer="21"/>
<rectangle x1="20.78481875" y1="2.413" x2="21.12518125" y2="2.49758125" layer="21"/>
<rectangle x1="21.54681875" y1="2.413" x2="22.225" y2="2.49758125" layer="21"/>
<rectangle x1="23.57881875" y1="2.413" x2="23.91918125" y2="2.49758125" layer="21"/>
<rectangle x1="25.781" y1="2.413" x2="26.11881875" y2="2.49758125" layer="21"/>
<rectangle x1="26.797" y1="2.413" x2="27.13481875" y2="2.49758125" layer="21"/>
<rectangle x1="30.26918125" y1="2.413" x2="30.607" y2="2.49758125" layer="21"/>
<rectangle x1="30.94481875" y1="2.413" x2="31.28518125" y2="2.49758125" layer="21"/>
<rectangle x1="33.147" y1="2.413" x2="33.48481875" y2="2.49758125" layer="21"/>
<rectangle x1="33.82518125" y1="2.413" x2="34.163" y2="2.49758125" layer="21"/>
<rectangle x1="36.02481875" y1="2.413" x2="36.36518125" y2="2.49758125" layer="21"/>
<rectangle x1="4.52881875" y1="2.49758125" x2="4.86918125" y2="2.58241875" layer="21"/>
<rectangle x1="8.001" y1="2.49758125" x2="8.33881875" y2="2.58241875" layer="21"/>
<rectangle x1="8.763" y1="2.49758125" x2="9.18718125" y2="2.58241875" layer="21"/>
<rectangle x1="10.96518125" y1="2.49758125" x2="11.303" y2="2.58241875" layer="21"/>
<rectangle x1="11.72718125" y1="2.49758125" x2="12.065" y2="2.58241875" layer="21"/>
<rectangle x1="12.827" y1="2.49758125" x2="13.16481875" y2="2.58241875" layer="21"/>
<rectangle x1="14.859" y1="2.49758125" x2="15.19681875" y2="2.58241875" layer="21"/>
<rectangle x1="17.907" y1="2.49758125" x2="18.24481875" y2="2.58241875" layer="21"/>
<rectangle x1="18.58518125" y1="2.49758125" x2="19.00681875" y2="2.58241875" layer="21"/>
<rectangle x1="20.78481875" y1="2.49758125" x2="21.12518125" y2="2.58241875" layer="21"/>
<rectangle x1="21.463" y1="2.49758125" x2="21.971" y2="2.58241875" layer="21"/>
<rectangle x1="23.66518125" y1="2.49758125" x2="24.003" y2="2.58241875" layer="21"/>
<rectangle x1="25.781" y1="2.49758125" x2="26.11881875" y2="2.58241875" layer="21"/>
<rectangle x1="26.797" y1="2.49758125" x2="27.13481875" y2="2.58241875" layer="21"/>
<rectangle x1="30.26918125" y1="2.49758125" x2="30.607" y2="2.58241875" layer="21"/>
<rectangle x1="30.94481875" y1="2.49758125" x2="31.369" y2="2.58241875" layer="21"/>
<rectangle x1="33.06318125" y1="2.49758125" x2="33.48481875" y2="2.58241875" layer="21"/>
<rectangle x1="33.82518125" y1="2.49758125" x2="34.24681875" y2="2.58241875" layer="21"/>
<rectangle x1="36.02481875" y1="2.49758125" x2="36.36518125" y2="2.58241875" layer="21"/>
<rectangle x1="4.52881875" y1="2.58241875" x2="4.86918125" y2="2.667" layer="21"/>
<rectangle x1="8.001" y1="2.58241875" x2="8.33881875" y2="2.667" layer="21"/>
<rectangle x1="8.763" y1="2.58241875" x2="9.18718125" y2="2.667" layer="21"/>
<rectangle x1="10.87881875" y1="2.58241875" x2="11.303" y2="2.667" layer="21"/>
<rectangle x1="11.72718125" y1="2.58241875" x2="12.065" y2="2.667" layer="21"/>
<rectangle x1="12.827" y1="2.58241875" x2="13.16481875" y2="2.667" layer="21"/>
<rectangle x1="14.859" y1="2.58241875" x2="15.19681875" y2="2.667" layer="21"/>
<rectangle x1="17.907" y1="2.58241875" x2="18.24481875" y2="2.667" layer="21"/>
<rectangle x1="18.669" y1="2.58241875" x2="19.00681875" y2="2.667" layer="21"/>
<rectangle x1="20.701" y1="2.58241875" x2="21.12518125" y2="2.667" layer="21"/>
<rectangle x1="21.463" y1="2.58241875" x2="21.88718125" y2="2.667" layer="21"/>
<rectangle x1="23.66518125" y1="2.58241875" x2="24.003" y2="2.667" layer="21"/>
<rectangle x1="25.69718125" y1="2.58241875" x2="26.11881875" y2="2.667" layer="21"/>
<rectangle x1="26.797" y1="2.58241875" x2="27.13481875" y2="2.667" layer="21"/>
<rectangle x1="30.26918125" y1="2.58241875" x2="30.607" y2="2.667" layer="21"/>
<rectangle x1="30.94481875" y1="2.58241875" x2="31.369" y2="2.667" layer="21"/>
<rectangle x1="33.06318125" y1="2.58241875" x2="33.401" y2="2.667" layer="21"/>
<rectangle x1="33.909" y1="2.58241875" x2="34.24681875" y2="2.667" layer="21"/>
<rectangle x1="35.941" y1="2.58241875" x2="36.36518125" y2="2.667" layer="21"/>
<rectangle x1="4.52881875" y1="2.667" x2="4.86918125" y2="2.75158125" layer="21"/>
<rectangle x1="8.001" y1="2.667" x2="8.33881875" y2="2.75158125" layer="21"/>
<rectangle x1="8.763" y1="2.667" x2="9.271" y2="2.75158125" layer="21"/>
<rectangle x1="10.87881875" y1="2.667" x2="11.21918125" y2="2.75158125" layer="21"/>
<rectangle x1="11.72718125" y1="2.667" x2="12.065" y2="2.75158125" layer="21"/>
<rectangle x1="12.827" y1="2.667" x2="13.16481875" y2="2.75158125" layer="21"/>
<rectangle x1="14.859" y1="2.667" x2="15.19681875" y2="2.75158125" layer="21"/>
<rectangle x1="17.907" y1="2.667" x2="18.24481875" y2="2.75158125" layer="21"/>
<rectangle x1="18.669" y1="2.667" x2="19.09318125" y2="2.75158125" layer="21"/>
<rectangle x1="20.701" y1="2.667" x2="21.03881875" y2="2.75158125" layer="21"/>
<rectangle x1="21.463" y1="2.667" x2="21.80081875" y2="2.75158125" layer="21"/>
<rectangle x1="23.66518125" y1="2.667" x2="24.08681875" y2="2.75158125" layer="21"/>
<rectangle x1="25.69718125" y1="2.667" x2="26.035" y2="2.75158125" layer="21"/>
<rectangle x1="26.797" y1="2.667" x2="27.13481875" y2="2.75158125" layer="21"/>
<rectangle x1="30.26918125" y1="2.667" x2="30.607" y2="2.75158125" layer="21"/>
<rectangle x1="30.94481875" y1="2.667" x2="31.45281875" y2="2.75158125" layer="21"/>
<rectangle x1="32.97681875" y1="2.667" x2="33.401" y2="2.75158125" layer="21"/>
<rectangle x1="33.909" y1="2.667" x2="34.33318125" y2="2.75158125" layer="21"/>
<rectangle x1="35.941" y1="2.667" x2="36.27881875" y2="2.75158125" layer="21"/>
<rectangle x1="4.52881875" y1="2.75158125" x2="4.86918125" y2="2.83641875" layer="21"/>
<rectangle x1="8.001" y1="2.75158125" x2="8.33881875" y2="2.83641875" layer="21"/>
<rectangle x1="8.763" y1="2.75158125" x2="9.271" y2="2.83641875" layer="21"/>
<rectangle x1="10.795" y1="2.75158125" x2="11.21918125" y2="2.83641875" layer="21"/>
<rectangle x1="11.72718125" y1="2.75158125" x2="12.065" y2="2.83641875" layer="21"/>
<rectangle x1="12.827" y1="2.75158125" x2="13.16481875" y2="2.83641875" layer="21"/>
<rectangle x1="14.859" y1="2.75158125" x2="15.19681875" y2="2.83641875" layer="21"/>
<rectangle x1="17.907" y1="2.75158125" x2="18.24481875" y2="2.83641875" layer="21"/>
<rectangle x1="18.75281875" y1="2.75158125" x2="19.09318125" y2="2.83641875" layer="21"/>
<rectangle x1="20.61718125" y1="2.75158125" x2="21.03881875" y2="2.83641875" layer="21"/>
<rectangle x1="21.463" y1="2.75158125" x2="21.80081875" y2="2.83641875" layer="21"/>
<rectangle x1="22.987" y1="2.75158125" x2="23.15718125" y2="2.83641875" layer="21"/>
<rectangle x1="23.749" y1="2.75158125" x2="24.17318125" y2="2.83641875" layer="21"/>
<rectangle x1="25.61081875" y1="2.75158125" x2="26.035" y2="2.83641875" layer="21"/>
<rectangle x1="26.797" y1="2.75158125" x2="27.13481875" y2="2.83641875" layer="21"/>
<rectangle x1="30.26918125" y1="2.75158125" x2="30.607" y2="2.83641875" layer="21"/>
<rectangle x1="30.94481875" y1="2.75158125" x2="31.45281875" y2="2.83641875" layer="21"/>
<rectangle x1="32.97681875" y1="2.75158125" x2="33.401" y2="2.83641875" layer="21"/>
<rectangle x1="33.909" y1="2.75158125" x2="34.33318125" y2="2.83641875" layer="21"/>
<rectangle x1="35.85718125" y1="2.75158125" x2="36.27881875" y2="2.83641875" layer="21"/>
<rectangle x1="4.52881875" y1="2.83641875" x2="4.86918125" y2="2.921" layer="21"/>
<rectangle x1="8.001" y1="2.83641875" x2="8.33881875" y2="2.921" layer="21"/>
<rectangle x1="8.763" y1="2.83641875" x2="9.35481875" y2="2.921" layer="21"/>
<rectangle x1="10.71118125" y1="2.83641875" x2="11.13281875" y2="2.921" layer="21"/>
<rectangle x1="11.72718125" y1="2.83641875" x2="12.065" y2="2.921" layer="21"/>
<rectangle x1="12.827" y1="2.83641875" x2="13.16481875" y2="2.921" layer="21"/>
<rectangle x1="14.859" y1="2.83641875" x2="15.19681875" y2="2.921" layer="21"/>
<rectangle x1="17.907" y1="2.83641875" x2="18.24481875" y2="2.921" layer="21"/>
<rectangle x1="18.75281875" y1="2.83641875" x2="19.177" y2="2.921" layer="21"/>
<rectangle x1="20.53081875" y1="2.83641875" x2="20.955" y2="2.921" layer="21"/>
<rectangle x1="21.463" y1="2.83641875" x2="21.80081875" y2="2.921" layer="21"/>
<rectangle x1="22.90318125" y1="2.83641875" x2="23.241" y2="2.921" layer="21"/>
<rectangle x1="23.749" y1="2.83641875" x2="24.257" y2="2.921" layer="21"/>
<rectangle x1="25.527" y1="2.83641875" x2="25.95118125" y2="2.921" layer="21"/>
<rectangle x1="26.797" y1="2.83641875" x2="27.13481875" y2="2.921" layer="21"/>
<rectangle x1="30.26918125" y1="2.83641875" x2="30.607" y2="2.921" layer="21"/>
<rectangle x1="30.94481875" y1="2.83641875" x2="31.53918125" y2="2.921" layer="21"/>
<rectangle x1="32.893" y1="2.83641875" x2="33.31718125" y2="2.921" layer="21"/>
<rectangle x1="33.99281875" y1="2.83641875" x2="34.417" y2="2.921" layer="21"/>
<rectangle x1="35.77081875" y1="2.83641875" x2="36.195" y2="2.921" layer="21"/>
<rectangle x1="4.52881875" y1="2.921" x2="4.86918125" y2="3.00558125" layer="21"/>
<rectangle x1="8.001" y1="2.921" x2="8.33881875" y2="3.00558125" layer="21"/>
<rectangle x1="8.763" y1="2.921" x2="9.44118125" y2="3.00558125" layer="21"/>
<rectangle x1="10.62481875" y1="2.921" x2="11.049" y2="3.00558125" layer="21"/>
<rectangle x1="11.72718125" y1="2.921" x2="12.065" y2="3.00558125" layer="21"/>
<rectangle x1="12.827" y1="2.921" x2="13.16481875" y2="3.00558125" layer="21"/>
<rectangle x1="14.859" y1="2.921" x2="15.19681875" y2="3.00558125" layer="21"/>
<rectangle x1="17.907" y1="2.921" x2="18.24481875" y2="3.00558125" layer="21"/>
<rectangle x1="18.83918125" y1="2.921" x2="19.26081875" y2="3.00558125" layer="21"/>
<rectangle x1="20.447" y1="2.921" x2="20.87118125" y2="3.00558125" layer="21"/>
<rectangle x1="21.463" y1="2.921" x2="21.88718125" y2="3.00558125" layer="21"/>
<rectangle x1="22.81681875" y1="2.921" x2="23.241" y2="3.00558125" layer="21"/>
<rectangle x1="23.83281875" y1="2.921" x2="24.34081875" y2="3.00558125" layer="21"/>
<rectangle x1="25.44318125" y1="2.921" x2="25.95118125" y2="3.00558125" layer="21"/>
<rectangle x1="26.797" y1="2.921" x2="27.22118125" y2="3.00558125" layer="21"/>
<rectangle x1="30.26918125" y1="2.921" x2="30.607" y2="3.00558125" layer="21"/>
<rectangle x1="30.94481875" y1="2.921" x2="31.623" y2="3.00558125" layer="21"/>
<rectangle x1="32.80918125" y1="2.921" x2="33.23081875" y2="3.00558125" layer="21"/>
<rectangle x1="34.07918125" y1="2.921" x2="34.50081875" y2="3.00558125" layer="21"/>
<rectangle x1="35.687" y1="2.921" x2="36.195" y2="3.00558125" layer="21"/>
<rectangle x1="4.52881875" y1="3.00558125" x2="4.86918125" y2="3.09041875" layer="21"/>
<rectangle x1="8.001" y1="3.00558125" x2="8.33881875" y2="3.09041875" layer="21"/>
<rectangle x1="8.763" y1="3.00558125" x2="9.60881875" y2="3.09041875" layer="21"/>
<rectangle x1="10.541" y1="3.00558125" x2="11.049" y2="3.09041875" layer="21"/>
<rectangle x1="11.72718125" y1="3.00558125" x2="12.065" y2="3.09041875" layer="21"/>
<rectangle x1="12.827" y1="3.00558125" x2="13.16481875" y2="3.09041875" layer="21"/>
<rectangle x1="14.859" y1="3.00558125" x2="15.19681875" y2="3.09041875" layer="21"/>
<rectangle x1="17.907" y1="3.00558125" x2="18.24481875" y2="3.09041875" layer="21"/>
<rectangle x1="18.83918125" y1="3.00558125" x2="19.431" y2="3.09041875" layer="21"/>
<rectangle x1="20.27681875" y1="3.00558125" x2="20.87118125" y2="3.09041875" layer="21"/>
<rectangle x1="21.54681875" y1="3.00558125" x2="21.971" y2="3.09041875" layer="21"/>
<rectangle x1="22.733" y1="3.00558125" x2="23.15718125" y2="3.09041875" layer="21"/>
<rectangle x1="23.91918125" y1="3.00558125" x2="24.42718125" y2="3.09041875" layer="21"/>
<rectangle x1="25.273" y1="3.00558125" x2="25.86481875" y2="3.09041875" layer="21"/>
<rectangle x1="26.88081875" y1="3.00558125" x2="27.22118125" y2="3.09041875" layer="21"/>
<rectangle x1="30.18281875" y1="3.00558125" x2="30.607" y2="3.09041875" layer="21"/>
<rectangle x1="30.94481875" y1="3.00558125" x2="31.79318125" y2="3.09041875" layer="21"/>
<rectangle x1="32.639" y1="3.00558125" x2="33.23081875" y2="3.09041875" layer="21"/>
<rectangle x1="34.163" y1="3.00558125" x2="34.671" y2="3.09041875" layer="21"/>
<rectangle x1="35.60318125" y1="3.00558125" x2="36.11118125" y2="3.09041875" layer="21"/>
<rectangle x1="4.52881875" y1="3.09041875" x2="4.86918125" y2="3.175" layer="21"/>
<rectangle x1="8.001" y1="3.09041875" x2="8.33881875" y2="3.175" layer="21"/>
<rectangle x1="8.763" y1="3.09041875" x2="9.10081875" y2="3.175" layer="21"/>
<rectangle x1="9.18718125" y1="3.09041875" x2="9.86281875" y2="3.175" layer="21"/>
<rectangle x1="10.20318125" y1="3.09041875" x2="10.96518125" y2="3.175" layer="21"/>
<rectangle x1="11.72718125" y1="3.09041875" x2="12.065" y2="3.175" layer="21"/>
<rectangle x1="12.827" y1="3.09041875" x2="13.16481875" y2="3.175" layer="21"/>
<rectangle x1="14.859" y1="3.09041875" x2="15.19681875" y2="3.175" layer="21"/>
<rectangle x1="17.907" y1="3.09041875" x2="18.24481875" y2="3.175" layer="21"/>
<rectangle x1="19.00681875" y1="3.09041875" x2="19.685" y2="3.175" layer="21"/>
<rectangle x1="20.02281875" y1="3.09041875" x2="20.78481875" y2="3.175" layer="21"/>
<rectangle x1="21.54681875" y1="3.09041875" x2="22.225" y2="3.175" layer="21"/>
<rectangle x1="22.56281875" y1="3.09041875" x2="23.15718125" y2="3.175" layer="21"/>
<rectangle x1="24.003" y1="3.09041875" x2="24.68118125" y2="3.175" layer="21"/>
<rectangle x1="25.10281875" y1="3.09041875" x2="25.781" y2="3.175" layer="21"/>
<rectangle x1="26.88081875" y1="3.09041875" x2="27.22118125" y2="3.175" layer="21"/>
<rectangle x1="30.18281875" y1="3.09041875" x2="30.52318125" y2="3.175" layer="21"/>
<rectangle x1="30.94481875" y1="3.09041875" x2="32.04718125" y2="3.175" layer="21"/>
<rectangle x1="32.385" y1="3.09041875" x2="33.06318125" y2="3.175" layer="21"/>
<rectangle x1="34.24681875" y1="3.09041875" x2="34.925" y2="3.175" layer="21"/>
<rectangle x1="35.26281875" y1="3.09041875" x2="36.02481875" y2="3.175" layer="21"/>
<rectangle x1="4.52881875" y1="3.175" x2="4.86918125" y2="3.25958125" layer="21"/>
<rectangle x1="8.001" y1="3.175" x2="8.33881875" y2="3.25958125" layer="21"/>
<rectangle x1="8.763" y1="3.175" x2="9.10081875" y2="3.25958125" layer="21"/>
<rectangle x1="9.271" y1="3.175" x2="10.87881875" y2="3.25958125" layer="21"/>
<rectangle x1="11.72718125" y1="3.175" x2="12.065" y2="3.25958125" layer="21"/>
<rectangle x1="12.40281875" y1="3.175" x2="14.26718125" y2="3.25958125" layer="21"/>
<rectangle x1="14.859" y1="3.175" x2="15.19681875" y2="3.25958125" layer="21"/>
<rectangle x1="17.907" y1="3.175" x2="18.24481875" y2="3.25958125" layer="21"/>
<rectangle x1="19.09318125" y1="3.175" x2="20.701" y2="3.25958125" layer="21"/>
<rectangle x1="21.63318125" y1="3.175" x2="23.07081875" y2="3.25958125" layer="21"/>
<rectangle x1="24.08681875" y1="3.175" x2="25.69718125" y2="3.25958125" layer="21"/>
<rectangle x1="26.88081875" y1="3.175" x2="27.22118125" y2="3.25958125" layer="21"/>
<rectangle x1="30.18281875" y1="3.175" x2="30.52318125" y2="3.25958125" layer="21"/>
<rectangle x1="30.94481875" y1="3.175" x2="31.28518125" y2="3.25958125" layer="21"/>
<rectangle x1="31.45281875" y1="3.175" x2="33.06318125" y2="3.25958125" layer="21"/>
<rectangle x1="34.33318125" y1="3.175" x2="35.941" y2="3.25958125" layer="21"/>
<rectangle x1="4.52881875" y1="3.25958125" x2="4.86918125" y2="3.34441875" layer="21"/>
<rectangle x1="8.001" y1="3.25958125" x2="8.33881875" y2="3.34441875" layer="21"/>
<rectangle x1="8.763" y1="3.25958125" x2="9.10081875" y2="3.34441875" layer="21"/>
<rectangle x1="9.35481875" y1="3.25958125" x2="10.71118125" y2="3.34441875" layer="21"/>
<rectangle x1="11.72718125" y1="3.25958125" x2="12.065" y2="3.34441875" layer="21"/>
<rectangle x1="12.319" y1="3.25958125" x2="14.26718125" y2="3.34441875" layer="21"/>
<rectangle x1="14.859" y1="3.25958125" x2="15.19681875" y2="3.34441875" layer="21"/>
<rectangle x1="17.82318125" y1="3.25958125" x2="18.161" y2="3.34441875" layer="21"/>
<rectangle x1="19.177" y1="3.25958125" x2="20.53081875" y2="3.34441875" layer="21"/>
<rectangle x1="21.717" y1="3.25958125" x2="22.987" y2="3.34441875" layer="21"/>
<rectangle x1="24.17318125" y1="3.25958125" x2="25.61081875" y2="3.34441875" layer="21"/>
<rectangle x1="26.88081875" y1="3.25958125" x2="27.305" y2="3.34441875" layer="21"/>
<rectangle x1="30.099" y1="3.25958125" x2="30.52318125" y2="3.34441875" layer="21"/>
<rectangle x1="30.94481875" y1="3.25958125" x2="31.28518125" y2="3.34441875" layer="21"/>
<rectangle x1="31.53918125" y1="3.25958125" x2="32.893" y2="3.34441875" layer="21"/>
<rectangle x1="34.417" y1="3.25958125" x2="35.77081875" y2="3.34441875" layer="21"/>
<rectangle x1="4.52881875" y1="3.34441875" x2="4.86918125" y2="3.429" layer="21"/>
<rectangle x1="8.001" y1="3.34441875" x2="8.33881875" y2="3.429" layer="21"/>
<rectangle x1="8.763" y1="3.34441875" x2="9.10081875" y2="3.429" layer="21"/>
<rectangle x1="9.525" y1="3.34441875" x2="10.541" y2="3.429" layer="21"/>
<rectangle x1="11.72718125" y1="3.34441875" x2="12.065" y2="3.429" layer="21"/>
<rectangle x1="12.40281875" y1="3.34441875" x2="14.26718125" y2="3.429" layer="21"/>
<rectangle x1="14.859" y1="3.34441875" x2="15.19681875" y2="3.429" layer="21"/>
<rectangle x1="17.82318125" y1="3.34441875" x2="18.161" y2="3.429" layer="21"/>
<rectangle x1="19.34718125" y1="3.34441875" x2="20.36318125" y2="3.429" layer="21"/>
<rectangle x1="21.88718125" y1="3.34441875" x2="22.81681875" y2="3.429" layer="21"/>
<rectangle x1="24.34081875" y1="3.34441875" x2="25.35681875" y2="3.429" layer="21"/>
<rectangle x1="26.96718125" y1="3.34441875" x2="27.305" y2="3.429" layer="21"/>
<rectangle x1="30.099" y1="3.34441875" x2="30.43681875" y2="3.429" layer="21"/>
<rectangle x1="30.94481875" y1="3.34441875" x2="31.28518125" y2="3.429" layer="21"/>
<rectangle x1="31.70681875" y1="3.34441875" x2="32.72281875" y2="3.429" layer="21"/>
<rectangle x1="34.58718125" y1="3.34441875" x2="35.60318125" y2="3.429" layer="21"/>
<rectangle x1="4.52881875" y1="3.429" x2="4.86918125" y2="3.51358125" layer="21"/>
<rectangle x1="8.001" y1="3.429" x2="8.33881875" y2="3.51358125" layer="21"/>
<rectangle x1="8.84681875" y1="3.429" x2="9.017" y2="3.51358125" layer="21"/>
<rectangle x1="9.779" y1="3.429" x2="10.287" y2="3.51358125" layer="21"/>
<rectangle x1="11.811" y1="3.429" x2="11.98118125" y2="3.51358125" layer="21"/>
<rectangle x1="12.40281875" y1="3.429" x2="14.18081875" y2="3.51358125" layer="21"/>
<rectangle x1="14.859" y1="3.429" x2="15.19681875" y2="3.51358125" layer="21"/>
<rectangle x1="17.82318125" y1="3.429" x2="18.161" y2="3.51358125" layer="21"/>
<rectangle x1="19.60118125" y1="3.429" x2="20.10918125" y2="3.51358125" layer="21"/>
<rectangle x1="22.14118125" y1="3.429" x2="22.56281875" y2="3.51358125" layer="21"/>
<rectangle x1="24.59481875" y1="3.429" x2="25.10281875" y2="3.51358125" layer="21"/>
<rectangle x1="26.96718125" y1="3.429" x2="27.38881875" y2="3.51358125" layer="21"/>
<rectangle x1="30.01518125" y1="3.429" x2="30.43681875" y2="3.51358125" layer="21"/>
<rectangle x1="31.03118125" y1="3.429" x2="31.19881875" y2="3.51358125" layer="21"/>
<rectangle x1="31.96081875" y1="3.429" x2="32.46881875" y2="3.51358125" layer="21"/>
<rectangle x1="34.84118125" y1="3.429" x2="35.34918125" y2="3.51358125" layer="21"/>
<rectangle x1="4.52881875" y1="3.51358125" x2="4.86918125" y2="3.59841875" layer="21"/>
<rectangle x1="8.001" y1="3.51358125" x2="8.33881875" y2="3.59841875" layer="21"/>
<rectangle x1="12.827" y1="3.51358125" x2="13.16481875" y2="3.59841875" layer="21"/>
<rectangle x1="14.859" y1="3.51358125" x2="15.19681875" y2="3.59841875" layer="21"/>
<rectangle x1="17.73681875" y1="3.51358125" x2="18.07718125" y2="3.59841875" layer="21"/>
<rectangle x1="27.051" y1="3.51358125" x2="27.47518125" y2="3.59841875" layer="21"/>
<rectangle x1="30.01518125" y1="3.51358125" x2="30.353" y2="3.59841875" layer="21"/>
<rectangle x1="4.52881875" y1="3.59841875" x2="4.86918125" y2="3.683" layer="21"/>
<rectangle x1="8.001" y1="3.59841875" x2="8.33881875" y2="3.683" layer="21"/>
<rectangle x1="11.811" y1="3.59841875" x2="11.98118125" y2="3.683" layer="21"/>
<rectangle x1="12.827" y1="3.59841875" x2="13.16481875" y2="3.683" layer="21"/>
<rectangle x1="14.859" y1="3.59841875" x2="15.19681875" y2="3.683" layer="21"/>
<rectangle x1="17.653" y1="3.59841875" x2="18.07718125" y2="3.683" layer="21"/>
<rectangle x1="27.13481875" y1="3.59841875" x2="27.47518125" y2="3.683" layer="21"/>
<rectangle x1="29.92881875" y1="3.59841875" x2="30.353" y2="3.683" layer="21"/>
<rectangle x1="4.52881875" y1="3.683" x2="4.86918125" y2="3.76758125" layer="21"/>
<rectangle x1="8.001" y1="3.683" x2="8.33881875" y2="3.76758125" layer="21"/>
<rectangle x1="11.72718125" y1="3.683" x2="12.065" y2="3.76758125" layer="21"/>
<rectangle x1="12.827" y1="3.683" x2="13.16481875" y2="3.76758125" layer="21"/>
<rectangle x1="14.859" y1="3.683" x2="15.19681875" y2="3.76758125" layer="21"/>
<rectangle x1="17.653" y1="3.683" x2="18.07718125" y2="3.76758125" layer="21"/>
<rectangle x1="27.13481875" y1="3.683" x2="27.559" y2="3.76758125" layer="21"/>
<rectangle x1="29.845" y1="3.683" x2="30.26918125" y2="3.76758125" layer="21"/>
<rectangle x1="4.52881875" y1="3.76758125" x2="4.86918125" y2="3.85241875" layer="21"/>
<rectangle x1="8.001" y1="3.76758125" x2="8.33881875" y2="3.85241875" layer="21"/>
<rectangle x1="11.72718125" y1="3.76758125" x2="12.065" y2="3.85241875" layer="21"/>
<rectangle x1="12.827" y1="3.76758125" x2="13.16481875" y2="3.85241875" layer="21"/>
<rectangle x1="14.859" y1="3.76758125" x2="15.19681875" y2="3.85241875" layer="21"/>
<rectangle x1="17.56918125" y1="3.76758125" x2="17.99081875" y2="3.85241875" layer="21"/>
<rectangle x1="27.22118125" y1="3.76758125" x2="27.72918125" y2="3.85241875" layer="21"/>
<rectangle x1="29.76118125" y1="3.76758125" x2="30.18281875" y2="3.85241875" layer="21"/>
<rectangle x1="4.52881875" y1="3.85241875" x2="4.86918125" y2="3.937" layer="21"/>
<rectangle x1="8.001" y1="3.85241875" x2="8.33881875" y2="3.937" layer="21"/>
<rectangle x1="11.72718125" y1="3.85241875" x2="12.065" y2="3.937" layer="21"/>
<rectangle x1="12.827" y1="3.85241875" x2="13.16481875" y2="3.937" layer="21"/>
<rectangle x1="14.859" y1="3.85241875" x2="15.19681875" y2="3.937" layer="21"/>
<rectangle x1="17.48281875" y1="3.85241875" x2="17.907" y2="3.937" layer="21"/>
<rectangle x1="27.305" y1="3.85241875" x2="27.813" y2="3.937" layer="21"/>
<rectangle x1="29.591" y1="3.85241875" x2="30.099" y2="3.937" layer="21"/>
<rectangle x1="4.52881875" y1="3.937" x2="4.86918125" y2="4.02158125" layer="21"/>
<rectangle x1="8.001" y1="3.937" x2="8.33881875" y2="4.02158125" layer="21"/>
<rectangle x1="11.72718125" y1="3.937" x2="12.065" y2="4.02158125" layer="21"/>
<rectangle x1="12.827" y1="3.937" x2="13.16481875" y2="4.02158125" layer="21"/>
<rectangle x1="14.859" y1="3.937" x2="15.19681875" y2="4.02158125" layer="21"/>
<rectangle x1="17.399" y1="3.937" x2="17.907" y2="4.02158125" layer="21"/>
<rectangle x1="27.38881875" y1="3.937" x2="27.89681875" y2="4.02158125" layer="21"/>
<rectangle x1="29.50718125" y1="3.937" x2="30.099" y2="4.02158125" layer="21"/>
<rectangle x1="4.52881875" y1="4.02158125" x2="4.86918125" y2="4.10641875" layer="21"/>
<rectangle x1="8.001" y1="4.02158125" x2="8.33881875" y2="4.10641875" layer="21"/>
<rectangle x1="11.811" y1="4.02158125" x2="11.98118125" y2="4.10641875" layer="21"/>
<rectangle x1="12.827" y1="4.02158125" x2="13.16481875" y2="4.10641875" layer="21"/>
<rectangle x1="14.859" y1="4.02158125" x2="15.19681875" y2="4.10641875" layer="21"/>
<rectangle x1="17.31518125" y1="4.02158125" x2="17.82318125" y2="4.10641875" layer="21"/>
<rectangle x1="27.47518125" y1="4.02158125" x2="28.067" y2="4.10641875" layer="21"/>
<rectangle x1="29.337" y1="4.02158125" x2="30.01518125" y2="4.10641875" layer="21"/>
<rectangle x1="4.52881875" y1="4.10641875" x2="4.86918125" y2="4.191" layer="21"/>
<rectangle x1="8.001" y1="4.10641875" x2="8.33881875" y2="4.191" layer="21"/>
<rectangle x1="12.827" y1="4.10641875" x2="13.16481875" y2="4.191" layer="21"/>
<rectangle x1="14.859" y1="4.10641875" x2="15.19681875" y2="4.191" layer="21"/>
<rectangle x1="17.22881875" y1="4.10641875" x2="17.73681875" y2="4.191" layer="21"/>
<rectangle x1="27.559" y1="4.10641875" x2="28.321" y2="4.191" layer="21"/>
<rectangle x1="29.083" y1="4.10641875" x2="29.845" y2="4.191" layer="21"/>
<rectangle x1="4.52881875" y1="4.191" x2="4.86918125" y2="4.27558125" layer="21"/>
<rectangle x1="8.001" y1="4.191" x2="8.33881875" y2="4.27558125" layer="21"/>
<rectangle x1="12.827" y1="4.191" x2="13.16481875" y2="4.27558125" layer="21"/>
<rectangle x1="14.859" y1="4.191" x2="15.19681875" y2="4.27558125" layer="21"/>
<rectangle x1="17.06118125" y1="4.191" x2="17.653" y2="4.27558125" layer="21"/>
<rectangle x1="27.64281875" y1="4.191" x2="29.76118125" y2="4.27558125" layer="21"/>
<rectangle x1="4.52881875" y1="4.27558125" x2="4.86918125" y2="4.36041875" layer="21"/>
<rectangle x1="8.001" y1="4.27558125" x2="8.33881875" y2="4.36041875" layer="21"/>
<rectangle x1="12.827" y1="4.27558125" x2="13.16481875" y2="4.36041875" layer="21"/>
<rectangle x1="14.94281875" y1="4.27558125" x2="15.28318125" y2="4.36041875" layer="21"/>
<rectangle x1="16.80718125" y1="4.27558125" x2="17.56918125" y2="4.36041875" layer="21"/>
<rectangle x1="27.813" y1="4.27558125" x2="29.591" y2="4.36041875" layer="21"/>
<rectangle x1="4.52881875" y1="4.36041875" x2="4.86918125" y2="4.445" layer="21"/>
<rectangle x1="8.001" y1="4.36041875" x2="8.33881875" y2="4.445" layer="21"/>
<rectangle x1="12.827" y1="4.36041875" x2="13.16481875" y2="4.445" layer="21"/>
<rectangle x1="14.859" y1="4.36041875" x2="17.399" y2="4.445" layer="21"/>
<rectangle x1="27.98318125" y1="4.36041875" x2="29.42081875" y2="4.445" layer="21"/>
<rectangle x1="4.52881875" y1="4.445" x2="4.86918125" y2="4.52958125" layer="21"/>
<rectangle x1="8.001" y1="4.445" x2="8.255" y2="4.52958125" layer="21"/>
<rectangle x1="12.827" y1="4.445" x2="13.081" y2="4.52958125" layer="21"/>
<rectangle x1="14.859" y1="4.445" x2="17.31518125" y2="4.52958125" layer="21"/>
<rectangle x1="28.321" y1="4.445" x2="29.16681875" y2="4.52958125" layer="21"/>
<rectangle x1="14.859" y1="4.52958125" x2="17.145" y2="4.61441875" layer="21"/>
<rectangle x1="14.94281875" y1="4.61441875" x2="16.891" y2="4.699" layer="21"/>
<rectangle x1="7.239" y1="6.477" x2="32.46881875" y2="6.56158125" layer="21"/>
<rectangle x1="6.64718125" y1="6.56158125" x2="33.147" y2="6.64641875" layer="21"/>
<rectangle x1="6.30681875" y1="6.64641875" x2="33.48481875" y2="6.731" layer="21"/>
<rectangle x1="5.969" y1="6.731" x2="33.82518125" y2="6.81558125" layer="21"/>
<rectangle x1="5.63118125" y1="6.81558125" x2="34.07918125" y2="6.90041875" layer="21"/>
<rectangle x1="5.37718125" y1="6.90041875" x2="34.33318125" y2="6.985" layer="21"/>
<rectangle x1="5.207" y1="6.985" x2="34.58718125" y2="7.06958125" layer="21"/>
<rectangle x1="4.953" y1="7.06958125" x2="34.75481875" y2="7.15441875" layer="21"/>
<rectangle x1="4.78281875" y1="7.15441875" x2="34.925" y2="7.239" layer="21"/>
<rectangle x1="4.61518125" y1="7.239" x2="35.179" y2="7.32358125" layer="21"/>
<rectangle x1="4.445" y1="7.32358125" x2="35.34918125" y2="7.40841875" layer="21"/>
<rectangle x1="4.27481875" y1="7.40841875" x2="35.51681875" y2="7.493" layer="21"/>
<rectangle x1="4.10718125" y1="7.493" x2="35.60318125" y2="7.57758125" layer="21"/>
<rectangle x1="4.02081875" y1="7.57758125" x2="7.15518125" y2="7.66241875" layer="21"/>
<rectangle x1="19.51481875" y1="7.57758125" x2="35.77081875" y2="7.66241875" layer="21"/>
<rectangle x1="3.85318125" y1="7.66241875" x2="6.64718125" y2="7.747" layer="21"/>
<rectangle x1="19.431" y1="7.66241875" x2="35.941" y2="7.747" layer="21"/>
<rectangle x1="3.76681875" y1="7.747" x2="6.30681875" y2="7.83158125" layer="21"/>
<rectangle x1="19.431" y1="7.747" x2="36.02481875" y2="7.83158125" layer="21"/>
<rectangle x1="3.59918125" y1="7.83158125" x2="6.05281875" y2="7.91641875" layer="21"/>
<rectangle x1="19.34718125" y1="7.83158125" x2="36.195" y2="7.91641875" layer="21"/>
<rectangle x1="3.51281875" y1="7.91641875" x2="5.79881875" y2="8.001" layer="21"/>
<rectangle x1="19.26081875" y1="7.91641875" x2="36.27881875" y2="8.001" layer="21"/>
<rectangle x1="3.34518125" y1="8.001" x2="5.54481875" y2="8.08558125" layer="21"/>
<rectangle x1="19.26081875" y1="8.001" x2="36.36518125" y2="8.08558125" layer="21"/>
<rectangle x1="3.25881875" y1="8.08558125" x2="5.37718125" y2="8.17041875" layer="21"/>
<rectangle x1="19.177" y1="8.08558125" x2="36.53281875" y2="8.17041875" layer="21"/>
<rectangle x1="3.175" y1="8.17041875" x2="5.207" y2="8.255" layer="21"/>
<rectangle x1="19.177" y1="8.17041875" x2="36.61918125" y2="8.255" layer="21"/>
<rectangle x1="3.00481875" y1="8.255" x2="5.03681875" y2="8.33958125" layer="21"/>
<rectangle x1="19.09318125" y1="8.255" x2="36.703" y2="8.33958125" layer="21"/>
<rectangle x1="3.00481875" y1="8.33958125" x2="4.86918125" y2="8.42441875" layer="21"/>
<rectangle x1="19.09318125" y1="8.33958125" x2="36.78681875" y2="8.42441875" layer="21"/>
<rectangle x1="2.83718125" y1="8.42441875" x2="4.699" y2="8.509" layer="21"/>
<rectangle x1="19.00681875" y1="8.42441875" x2="36.957" y2="8.509" layer="21"/>
<rectangle x1="2.75081875" y1="8.509" x2="4.61518125" y2="8.59358125" layer="21"/>
<rectangle x1="19.00681875" y1="8.509" x2="37.04081875" y2="8.59358125" layer="21"/>
<rectangle x1="2.667" y1="8.59358125" x2="4.445" y2="8.67841875" layer="21"/>
<rectangle x1="18.923" y1="8.59358125" x2="37.12718125" y2="8.67841875" layer="21"/>
<rectangle x1="2.58318125" y1="8.67841875" x2="4.27481875" y2="8.763" layer="21"/>
<rectangle x1="18.923" y1="8.67841875" x2="37.211" y2="8.763" layer="21"/>
<rectangle x1="2.49681875" y1="8.763" x2="4.191" y2="8.84758125" layer="21"/>
<rectangle x1="18.83918125" y1="8.763" x2="37.29481875" y2="8.84758125" layer="21"/>
<rectangle x1="2.413" y1="8.84758125" x2="4.02081875" y2="8.93241875" layer="21"/>
<rectangle x1="18.83918125" y1="8.84758125" x2="37.38118125" y2="8.93241875" layer="21"/>
<rectangle x1="2.32918125" y1="8.93241875" x2="3.937" y2="9.017" layer="21"/>
<rectangle x1="18.75281875" y1="8.93241875" x2="37.465" y2="9.017" layer="21"/>
<rectangle x1="2.24281875" y1="9.017" x2="3.85318125" y2="9.10158125" layer="21"/>
<rectangle x1="18.75281875" y1="9.017" x2="37.54881875" y2="9.10158125" layer="21"/>
<rectangle x1="2.159" y1="9.10158125" x2="3.76681875" y2="9.18641875" layer="21"/>
<rectangle x1="18.75281875" y1="9.10158125" x2="37.63518125" y2="9.18641875" layer="21"/>
<rectangle x1="2.07518125" y1="9.18641875" x2="3.683" y2="9.271" layer="21"/>
<rectangle x1="18.669" y1="9.18641875" x2="37.63518125" y2="9.271" layer="21"/>
<rectangle x1="2.07518125" y1="9.271" x2="3.51281875" y2="9.35558125" layer="21"/>
<rectangle x1="18.669" y1="9.271" x2="37.719" y2="9.35558125" layer="21"/>
<rectangle x1="1.98881875" y1="9.35558125" x2="3.429" y2="9.44041875" layer="21"/>
<rectangle x1="18.669" y1="9.35558125" x2="37.80281875" y2="9.44041875" layer="21"/>
<rectangle x1="1.905" y1="9.44041875" x2="3.34518125" y2="9.525" layer="21"/>
<rectangle x1="18.58518125" y1="9.44041875" x2="37.88918125" y2="9.525" layer="21"/>
<rectangle x1="1.82118125" y1="9.525" x2="3.25881875" y2="9.60958125" layer="21"/>
<rectangle x1="18.58518125" y1="9.525" x2="37.973" y2="9.60958125" layer="21"/>
<rectangle x1="1.73481875" y1="9.60958125" x2="3.175" y2="9.69441875" layer="21"/>
<rectangle x1="18.49881875" y1="9.60958125" x2="37.973" y2="9.69441875" layer="21"/>
<rectangle x1="1.73481875" y1="9.69441875" x2="3.09118125" y2="9.779" layer="21"/>
<rectangle x1="18.49881875" y1="9.69441875" x2="38.05681875" y2="9.779" layer="21"/>
<rectangle x1="1.651" y1="9.779" x2="3.00481875" y2="9.86358125" layer="21"/>
<rectangle x1="18.49881875" y1="9.779" x2="38.14318125" y2="9.86358125" layer="21"/>
<rectangle x1="1.56718125" y1="9.86358125" x2="2.921" y2="9.94841875" layer="21"/>
<rectangle x1="18.415" y1="9.86358125" x2="38.227" y2="9.94841875" layer="21"/>
<rectangle x1="1.56718125" y1="9.94841875" x2="2.921" y2="10.033" layer="21"/>
<rectangle x1="18.415" y1="9.94841875" x2="38.227" y2="10.033" layer="21"/>
<rectangle x1="1.48081875" y1="10.033" x2="2.83718125" y2="10.11758125" layer="21"/>
<rectangle x1="18.415" y1="10.033" x2="38.31081875" y2="10.11758125" layer="21"/>
<rectangle x1="1.397" y1="10.11758125" x2="2.75081875" y2="10.20241875" layer="21"/>
<rectangle x1="18.415" y1="10.11758125" x2="38.39718125" y2="10.20241875" layer="21"/>
<rectangle x1="1.397" y1="10.20241875" x2="2.667" y2="10.287" layer="21"/>
<rectangle x1="18.33118125" y1="10.20241875" x2="38.39718125" y2="10.287" layer="21"/>
<rectangle x1="1.31318125" y1="10.287" x2="2.58318125" y2="10.37158125" layer="21"/>
<rectangle x1="18.33118125" y1="10.287" x2="38.481" y2="10.37158125" layer="21"/>
<rectangle x1="1.22681875" y1="10.37158125" x2="2.58318125" y2="10.45641875" layer="21"/>
<rectangle x1="18.33118125" y1="10.37158125" x2="38.481" y2="10.45641875" layer="21"/>
<rectangle x1="1.22681875" y1="10.45641875" x2="2.49681875" y2="10.541" layer="21"/>
<rectangle x1="18.33118125" y1="10.45641875" x2="38.56481875" y2="10.541" layer="21"/>
<rectangle x1="1.143" y1="10.541" x2="2.413" y2="10.62558125" layer="21"/>
<rectangle x1="18.24481875" y1="10.541" x2="38.56481875" y2="10.62558125" layer="21"/>
<rectangle x1="1.143" y1="10.62558125" x2="2.413" y2="10.71041875" layer="21"/>
<rectangle x1="18.24481875" y1="10.62558125" x2="38.65118125" y2="10.71041875" layer="21"/>
<rectangle x1="1.05918125" y1="10.71041875" x2="2.32918125" y2="10.795" layer="21"/>
<rectangle x1="18.24481875" y1="10.71041875" x2="38.65118125" y2="10.795" layer="21"/>
<rectangle x1="1.05918125" y1="10.795" x2="2.24281875" y2="10.87958125" layer="21"/>
<rectangle x1="18.24481875" y1="10.795" x2="38.735" y2="10.87958125" layer="21"/>
<rectangle x1="0.97281875" y1="10.87958125" x2="2.24281875" y2="10.96441875" layer="21"/>
<rectangle x1="18.24481875" y1="10.87958125" x2="38.81881875" y2="10.96441875" layer="21"/>
<rectangle x1="0.97281875" y1="10.96441875" x2="2.159" y2="11.049" layer="21"/>
<rectangle x1="18.161" y1="10.96441875" x2="38.81881875" y2="11.049" layer="21"/>
<rectangle x1="0.889" y1="11.049" x2="2.159" y2="11.13358125" layer="21"/>
<rectangle x1="18.161" y1="11.049" x2="38.90518125" y2="11.13358125" layer="21"/>
<rectangle x1="0.889" y1="11.13358125" x2="2.07518125" y2="11.21841875" layer="21"/>
<rectangle x1="7.40918125" y1="11.13358125" x2="8.763" y2="11.21841875" layer="21"/>
<rectangle x1="11.557" y1="11.13358125" x2="13.92681875" y2="11.21841875" layer="21"/>
<rectangle x1="18.161" y1="11.13358125" x2="21.63318125" y2="11.21841875" layer="21"/>
<rectangle x1="23.07081875" y1="11.13358125" x2="33.06318125" y2="11.21841875" layer="21"/>
<rectangle x1="33.147" y1="11.13358125" x2="38.90518125" y2="11.21841875" layer="21"/>
<rectangle x1="0.80518125" y1="11.21841875" x2="2.07518125" y2="11.303" layer="21"/>
<rectangle x1="7.06881875" y1="11.21841875" x2="9.10081875" y2="11.303" layer="21"/>
<rectangle x1="11.47318125" y1="11.21841875" x2="14.18081875" y2="11.303" layer="21"/>
<rectangle x1="18.161" y1="11.21841875" x2="21.29281875" y2="11.303" layer="21"/>
<rectangle x1="23.32481875" y1="11.21841875" x2="26.45918125" y2="11.303" layer="21"/>
<rectangle x1="26.88081875" y1="11.21841875" x2="29.92881875" y2="11.303" layer="21"/>
<rectangle x1="30.43681875" y1="11.21841875" x2="32.639" y2="11.303" layer="21"/>
<rectangle x1="33.82518125" y1="11.21841875" x2="38.90518125" y2="11.303" layer="21"/>
<rectangle x1="0.80518125" y1="11.303" x2="1.98881875" y2="11.38758125" layer="21"/>
<rectangle x1="6.81481875" y1="11.303" x2="9.35481875" y2="11.38758125" layer="21"/>
<rectangle x1="11.38681875" y1="11.303" x2="14.43481875" y2="11.38758125" layer="21"/>
<rectangle x1="18.161" y1="11.303" x2="21.12518125" y2="11.38758125" layer="21"/>
<rectangle x1="23.57881875" y1="11.303" x2="26.37281875" y2="11.38758125" layer="21"/>
<rectangle x1="27.051" y1="11.303" x2="29.845" y2="11.38758125" layer="21"/>
<rectangle x1="30.52318125" y1="11.303" x2="32.385" y2="11.38758125" layer="21"/>
<rectangle x1="34.163" y1="11.303" x2="38.989" y2="11.38758125" layer="21"/>
<rectangle x1="0.80518125" y1="11.38758125" x2="1.98881875" y2="11.47241875" layer="21"/>
<rectangle x1="6.64718125" y1="11.38758125" x2="9.525" y2="11.47241875" layer="21"/>
<rectangle x1="11.38681875" y1="11.38758125" x2="14.605" y2="11.47241875" layer="21"/>
<rectangle x1="18.07718125" y1="11.38758125" x2="20.955" y2="11.47241875" layer="21"/>
<rectangle x1="23.749" y1="11.38758125" x2="26.289" y2="11.47241875" layer="21"/>
<rectangle x1="27.051" y1="11.38758125" x2="29.76118125" y2="11.47241875" layer="21"/>
<rectangle x1="30.52318125" y1="11.38758125" x2="32.21481875" y2="11.47241875" layer="21"/>
<rectangle x1="34.33318125" y1="11.38758125" x2="38.989" y2="11.47241875" layer="21"/>
<rectangle x1="0.71881875" y1="11.47241875" x2="1.905" y2="11.557" layer="21"/>
<rectangle x1="6.477" y1="11.47241875" x2="9.69518125" y2="11.557" layer="21"/>
<rectangle x1="11.303" y1="11.47241875" x2="14.859" y2="11.557" layer="21"/>
<rectangle x1="18.07718125" y1="11.47241875" x2="20.78481875" y2="11.557" layer="21"/>
<rectangle x1="23.91918125" y1="11.47241875" x2="26.20518125" y2="11.557" layer="21"/>
<rectangle x1="27.13481875" y1="11.47241875" x2="29.76118125" y2="11.557" layer="21"/>
<rectangle x1="30.607" y1="11.47241875" x2="32.04718125" y2="11.557" layer="21"/>
<rectangle x1="34.50081875" y1="11.47241875" x2="39.07281875" y2="11.557" layer="21"/>
<rectangle x1="0.71881875" y1="11.557" x2="1.905" y2="11.64158125" layer="21"/>
<rectangle x1="6.39318125" y1="11.557" x2="9.779" y2="11.64158125" layer="21"/>
<rectangle x1="11.303" y1="11.557" x2="14.94281875" y2="11.64158125" layer="21"/>
<rectangle x1="18.07718125" y1="11.557" x2="20.61718125" y2="11.64158125" layer="21"/>
<rectangle x1="24.08681875" y1="11.557" x2="26.20518125" y2="11.64158125" layer="21"/>
<rectangle x1="27.13481875" y1="11.557" x2="29.76118125" y2="11.64158125" layer="21"/>
<rectangle x1="30.607" y1="11.557" x2="31.96081875" y2="11.64158125" layer="21"/>
<rectangle x1="34.671" y1="11.557" x2="39.07281875" y2="11.64158125" layer="21"/>
<rectangle x1="0.635" y1="11.64158125" x2="1.82118125" y2="11.72641875" layer="21"/>
<rectangle x1="6.223" y1="11.64158125" x2="9.94918125" y2="11.72641875" layer="21"/>
<rectangle x1="11.303" y1="11.64158125" x2="15.02918125" y2="11.72641875" layer="21"/>
<rectangle x1="18.07718125" y1="11.64158125" x2="20.53081875" y2="11.72641875" layer="21"/>
<rectangle x1="24.17318125" y1="11.64158125" x2="26.20518125" y2="11.72641875" layer="21"/>
<rectangle x1="27.13481875" y1="11.64158125" x2="29.76118125" y2="11.72641875" layer="21"/>
<rectangle x1="30.607" y1="11.64158125" x2="31.877" y2="11.72641875" layer="21"/>
<rectangle x1="34.75481875" y1="11.64158125" x2="39.07281875" y2="11.72641875" layer="21"/>
<rectangle x1="0.635" y1="11.72641875" x2="1.82118125" y2="11.811" layer="21"/>
<rectangle x1="6.13918125" y1="11.72641875" x2="10.033" y2="11.811" layer="21"/>
<rectangle x1="11.303" y1="11.72641875" x2="15.19681875" y2="11.811" layer="21"/>
<rectangle x1="18.07718125" y1="11.72641875" x2="20.36318125" y2="11.811" layer="21"/>
<rectangle x1="24.34081875" y1="11.72641875" x2="26.20518125" y2="11.811" layer="21"/>
<rectangle x1="27.13481875" y1="11.72641875" x2="29.76118125" y2="11.811" layer="21"/>
<rectangle x1="30.607" y1="11.72641875" x2="31.79318125" y2="11.811" layer="21"/>
<rectangle x1="34.84118125" y1="11.72641875" x2="39.15918125" y2="11.811" layer="21"/>
<rectangle x1="0.635" y1="11.811" x2="1.73481875" y2="11.89558125" layer="21"/>
<rectangle x1="5.969" y1="11.811" x2="10.11681875" y2="11.89558125" layer="21"/>
<rectangle x1="11.38681875" y1="11.811" x2="15.28318125" y2="11.89558125" layer="21"/>
<rectangle x1="18.07718125" y1="11.811" x2="20.27681875" y2="11.89558125" layer="21"/>
<rectangle x1="24.42718125" y1="11.811" x2="26.20518125" y2="11.89558125" layer="21"/>
<rectangle x1="27.13481875" y1="11.811" x2="29.76118125" y2="11.89558125" layer="21"/>
<rectangle x1="30.607" y1="11.811" x2="31.623" y2="11.89558125" layer="21"/>
<rectangle x1="35.00881875" y1="11.811" x2="39.15918125" y2="11.89558125" layer="21"/>
<rectangle x1="0.55118125" y1="11.89558125" x2="1.73481875" y2="11.98041875" layer="21"/>
<rectangle x1="5.88518125" y1="11.89558125" x2="10.287" y2="11.98041875" layer="21"/>
<rectangle x1="11.38681875" y1="11.89558125" x2="15.367" y2="11.98041875" layer="21"/>
<rectangle x1="17.99081875" y1="11.89558125" x2="20.193" y2="11.98041875" layer="21"/>
<rectangle x1="24.511" y1="11.89558125" x2="26.20518125" y2="11.98041875" layer="21"/>
<rectangle x1="27.13481875" y1="11.89558125" x2="29.76118125" y2="11.98041875" layer="21"/>
<rectangle x1="30.607" y1="11.89558125" x2="31.623" y2="11.98041875" layer="21"/>
<rectangle x1="35.00881875" y1="11.89558125" x2="39.15918125" y2="11.98041875" layer="21"/>
<rectangle x1="0.55118125" y1="11.98041875" x2="1.73481875" y2="12.065" layer="21"/>
<rectangle x1="5.79881875" y1="11.98041875" x2="10.37081875" y2="12.065" layer="21"/>
<rectangle x1="11.47318125" y1="11.98041875" x2="15.45081875" y2="12.065" layer="21"/>
<rectangle x1="17.99081875" y1="11.98041875" x2="20.10918125" y2="12.065" layer="21"/>
<rectangle x1="24.59481875" y1="11.98041875" x2="26.20518125" y2="12.065" layer="21"/>
<rectangle x1="27.13481875" y1="11.98041875" x2="29.76118125" y2="12.065" layer="21"/>
<rectangle x1="30.607" y1="11.98041875" x2="31.53918125" y2="12.065" layer="21"/>
<rectangle x1="35.09518125" y1="11.98041875" x2="39.243" y2="12.065" layer="21"/>
<rectangle x1="0.55118125" y1="12.065" x2="1.651" y2="12.14958125" layer="21"/>
<rectangle x1="5.715" y1="12.065" x2="10.45718125" y2="12.14958125" layer="21"/>
<rectangle x1="11.557" y1="12.065" x2="15.53718125" y2="12.14958125" layer="21"/>
<rectangle x1="17.99081875" y1="12.065" x2="20.02281875" y2="12.14958125" layer="21"/>
<rectangle x1="24.68118125" y1="12.065" x2="26.20518125" y2="12.14958125" layer="21"/>
<rectangle x1="27.13481875" y1="12.065" x2="29.76118125" y2="12.14958125" layer="21"/>
<rectangle x1="30.607" y1="12.065" x2="31.45281875" y2="12.14958125" layer="21"/>
<rectangle x1="32.80918125" y1="12.065" x2="33.655" y2="12.14958125" layer="21"/>
<rectangle x1="35.179" y1="12.065" x2="39.243" y2="12.14958125" layer="21"/>
<rectangle x1="0.46481875" y1="12.14958125" x2="1.651" y2="12.23441875" layer="21"/>
<rectangle x1="5.63118125" y1="12.14958125" x2="7.91718125" y2="12.23441875" layer="21"/>
<rectangle x1="8.255" y1="12.14958125" x2="10.541" y2="12.23441875" layer="21"/>
<rectangle x1="13.335" y1="12.14958125" x2="15.621" y2="12.23441875" layer="21"/>
<rectangle x1="17.99081875" y1="12.14958125" x2="19.939" y2="12.23441875" layer="21"/>
<rectangle x1="22.225" y1="12.14958125" x2="22.56281875" y2="12.23441875" layer="21"/>
<rectangle x1="24.765" y1="12.14958125" x2="26.20518125" y2="12.23441875" layer="21"/>
<rectangle x1="27.13481875" y1="12.14958125" x2="29.76118125" y2="12.23441875" layer="21"/>
<rectangle x1="30.607" y1="12.14958125" x2="31.369" y2="12.23441875" layer="21"/>
<rectangle x1="32.639" y1="12.14958125" x2="33.909" y2="12.23441875" layer="21"/>
<rectangle x1="35.179" y1="12.14958125" x2="39.243" y2="12.23441875" layer="21"/>
<rectangle x1="0.46481875" y1="12.23441875" x2="1.651" y2="12.319" layer="21"/>
<rectangle x1="5.54481875" y1="12.23441875" x2="7.40918125" y2="12.319" layer="21"/>
<rectangle x1="8.763" y1="12.23441875" x2="10.62481875" y2="12.319" layer="21"/>
<rectangle x1="13.843" y1="12.23441875" x2="15.70481875" y2="12.319" layer="21"/>
<rectangle x1="17.99081875" y1="12.23441875" x2="19.85518125" y2="12.319" layer="21"/>
<rectangle x1="21.717" y1="12.23441875" x2="22.987" y2="12.319" layer="21"/>
<rectangle x1="24.84881875" y1="12.23441875" x2="26.20518125" y2="12.319" layer="21"/>
<rectangle x1="27.13481875" y1="12.23441875" x2="29.76118125" y2="12.319" layer="21"/>
<rectangle x1="30.607" y1="12.23441875" x2="31.369" y2="12.319" layer="21"/>
<rectangle x1="32.46881875" y1="12.23441875" x2="34.07918125" y2="12.319" layer="21"/>
<rectangle x1="35.179" y1="12.23441875" x2="39.32681875" y2="12.319" layer="21"/>
<rectangle x1="0.46481875" y1="12.319" x2="1.56718125" y2="12.40358125" layer="21"/>
<rectangle x1="5.54481875" y1="12.319" x2="7.239" y2="12.40358125" layer="21"/>
<rectangle x1="8.93318125" y1="12.319" x2="10.62481875" y2="12.40358125" layer="21"/>
<rectangle x1="14.097" y1="12.319" x2="15.79118125" y2="12.40358125" layer="21"/>
<rectangle x1="17.99081875" y1="12.319" x2="19.76881875" y2="12.40358125" layer="21"/>
<rectangle x1="21.463" y1="12.319" x2="23.241" y2="12.40358125" layer="21"/>
<rectangle x1="24.93518125" y1="12.319" x2="26.20518125" y2="12.40358125" layer="21"/>
<rectangle x1="27.13481875" y1="12.319" x2="29.76118125" y2="12.40358125" layer="21"/>
<rectangle x1="30.607" y1="12.319" x2="31.28518125" y2="12.40358125" layer="21"/>
<rectangle x1="32.385" y1="12.319" x2="34.24681875" y2="12.40358125" layer="21"/>
<rectangle x1="35.179" y1="12.319" x2="39.32681875" y2="12.40358125" layer="21"/>
<rectangle x1="0.46481875" y1="12.40358125" x2="1.56718125" y2="12.48841875" layer="21"/>
<rectangle x1="5.461" y1="12.40358125" x2="7.06881875" y2="12.48841875" layer="21"/>
<rectangle x1="9.10081875" y1="12.40358125" x2="10.71118125" y2="12.48841875" layer="21"/>
<rectangle x1="14.26718125" y1="12.40358125" x2="15.875" y2="12.48841875" layer="21"/>
<rectangle x1="17.99081875" y1="12.40358125" x2="19.685" y2="12.48841875" layer="21"/>
<rectangle x1="21.29281875" y1="12.40358125" x2="23.41118125" y2="12.48841875" layer="21"/>
<rectangle x1="25.019" y1="12.40358125" x2="26.20518125" y2="12.48841875" layer="21"/>
<rectangle x1="27.13481875" y1="12.40358125" x2="29.76118125" y2="12.48841875" layer="21"/>
<rectangle x1="30.607" y1="12.40358125" x2="31.28518125" y2="12.48841875" layer="21"/>
<rectangle x1="32.30118125" y1="12.40358125" x2="34.33318125" y2="12.48841875" layer="21"/>
<rectangle x1="35.09518125" y1="12.40358125" x2="39.32681875" y2="12.48841875" layer="21"/>
<rectangle x1="0.46481875" y1="12.48841875" x2="1.56718125" y2="12.573" layer="21"/>
<rectangle x1="5.37718125" y1="12.48841875" x2="6.90118125" y2="12.573" layer="21"/>
<rectangle x1="9.271" y1="12.48841875" x2="10.795" y2="12.573" layer="21"/>
<rectangle x1="14.351" y1="12.48841875" x2="15.875" y2="12.573" layer="21"/>
<rectangle x1="17.907" y1="12.48841875" x2="19.685" y2="12.573" layer="21"/>
<rectangle x1="21.209" y1="12.48841875" x2="23.495" y2="12.573" layer="21"/>
<rectangle x1="25.019" y1="12.48841875" x2="26.20518125" y2="12.573" layer="21"/>
<rectangle x1="27.13481875" y1="12.48841875" x2="29.76118125" y2="12.573" layer="21"/>
<rectangle x1="30.607" y1="12.48841875" x2="31.19881875" y2="12.573" layer="21"/>
<rectangle x1="32.21481875" y1="12.48841875" x2="34.417" y2="12.573" layer="21"/>
<rectangle x1="35.09518125" y1="12.48841875" x2="39.32681875" y2="12.573" layer="21"/>
<rectangle x1="0.381" y1="12.573" x2="1.48081875" y2="12.65758125" layer="21"/>
<rectangle x1="5.29081875" y1="12.573" x2="6.731" y2="12.65758125" layer="21"/>
<rectangle x1="9.35481875" y1="12.573" x2="10.87881875" y2="12.65758125" layer="21"/>
<rectangle x1="14.52118125" y1="12.573" x2="15.95881875" y2="12.65758125" layer="21"/>
<rectangle x1="17.907" y1="12.573" x2="19.60118125" y2="12.65758125" layer="21"/>
<rectangle x1="21.03881875" y1="12.573" x2="23.66518125" y2="12.65758125" layer="21"/>
<rectangle x1="25.10281875" y1="12.573" x2="26.20518125" y2="12.65758125" layer="21"/>
<rectangle x1="27.13481875" y1="12.573" x2="29.76118125" y2="12.65758125" layer="21"/>
<rectangle x1="30.607" y1="12.573" x2="31.19881875" y2="12.65758125" layer="21"/>
<rectangle x1="32.131" y1="12.573" x2="34.50081875" y2="12.65758125" layer="21"/>
<rectangle x1="35.00881875" y1="12.573" x2="39.32681875" y2="12.65758125" layer="21"/>
<rectangle x1="0.381" y1="12.65758125" x2="1.48081875" y2="12.74241875" layer="21"/>
<rectangle x1="5.29081875" y1="12.65758125" x2="6.64718125" y2="12.74241875" layer="21"/>
<rectangle x1="9.525" y1="12.65758125" x2="10.87881875" y2="12.74241875" layer="21"/>
<rectangle x1="14.605" y1="12.65758125" x2="16.04518125" y2="12.74241875" layer="21"/>
<rectangle x1="17.907" y1="12.65758125" x2="19.51481875" y2="12.74241875" layer="21"/>
<rectangle x1="20.955" y1="12.65758125" x2="23.749" y2="12.74241875" layer="21"/>
<rectangle x1="25.18918125" y1="12.65758125" x2="26.20518125" y2="12.74241875" layer="21"/>
<rectangle x1="27.13481875" y1="12.65758125" x2="29.76118125" y2="12.74241875" layer="21"/>
<rectangle x1="30.607" y1="12.65758125" x2="31.19881875" y2="12.74241875" layer="21"/>
<rectangle x1="32.131" y1="12.65758125" x2="39.41318125" y2="12.74241875" layer="21"/>
<rectangle x1="0.381" y1="12.74241875" x2="1.48081875" y2="12.827" layer="21"/>
<rectangle x1="5.207" y1="12.74241875" x2="6.56081875" y2="12.827" layer="21"/>
<rectangle x1="9.60881875" y1="12.74241875" x2="10.87881875" y2="12.827" layer="21"/>
<rectangle x1="14.68881875" y1="12.74241875" x2="16.04518125" y2="12.827" layer="21"/>
<rectangle x1="17.907" y1="12.74241875" x2="19.51481875" y2="12.827" layer="21"/>
<rectangle x1="20.87118125" y1="12.74241875" x2="23.83281875" y2="12.827" layer="21"/>
<rectangle x1="25.18918125" y1="12.74241875" x2="26.20518125" y2="12.827" layer="21"/>
<rectangle x1="27.13481875" y1="12.74241875" x2="29.76118125" y2="12.827" layer="21"/>
<rectangle x1="30.607" y1="12.74241875" x2="31.115" y2="12.827" layer="21"/>
<rectangle x1="32.04718125" y1="12.74241875" x2="39.41318125" y2="12.827" layer="21"/>
<rectangle x1="0.381" y1="12.827" x2="1.48081875" y2="12.91158125" layer="21"/>
<rectangle x1="5.207" y1="12.827" x2="6.477" y2="12.91158125" layer="21"/>
<rectangle x1="9.69518125" y1="12.827" x2="10.96518125" y2="12.91158125" layer="21"/>
<rectangle x1="14.77518125" y1="12.827" x2="16.129" y2="12.91158125" layer="21"/>
<rectangle x1="17.907" y1="12.827" x2="19.431" y2="12.91158125" layer="21"/>
<rectangle x1="20.701" y1="12.827" x2="23.91918125" y2="12.91158125" layer="21"/>
<rectangle x1="25.273" y1="12.827" x2="26.20518125" y2="12.91158125" layer="21"/>
<rectangle x1="27.13481875" y1="12.827" x2="29.76118125" y2="12.91158125" layer="21"/>
<rectangle x1="30.607" y1="12.827" x2="31.115" y2="12.91158125" layer="21"/>
<rectangle x1="32.04718125" y1="12.827" x2="39.41318125" y2="12.91158125" layer="21"/>
<rectangle x1="0.29718125" y1="12.91158125" x2="1.397" y2="12.99641875" layer="21"/>
<rectangle x1="5.12318125" y1="12.91158125" x2="6.39318125" y2="12.99641875" layer="21"/>
<rectangle x1="9.779" y1="12.91158125" x2="11.049" y2="12.99641875" layer="21"/>
<rectangle x1="14.859" y1="12.91158125" x2="16.129" y2="12.99641875" layer="21"/>
<rectangle x1="17.907" y1="12.91158125" x2="19.431" y2="12.99641875" layer="21"/>
<rectangle x1="20.701" y1="12.91158125" x2="24.003" y2="12.99641875" layer="21"/>
<rectangle x1="25.273" y1="12.91158125" x2="26.20518125" y2="12.99641875" layer="21"/>
<rectangle x1="27.13481875" y1="12.91158125" x2="29.76118125" y2="12.99641875" layer="21"/>
<rectangle x1="30.607" y1="12.91158125" x2="31.115" y2="12.99641875" layer="21"/>
<rectangle x1="35.179" y1="12.91158125" x2="39.41318125" y2="12.99641875" layer="21"/>
<rectangle x1="0.29718125" y1="12.99641875" x2="1.397" y2="13.081" layer="21"/>
<rectangle x1="5.12318125" y1="12.99641875" x2="6.30681875" y2="13.081" layer="21"/>
<rectangle x1="9.779" y1="12.99641875" x2="11.049" y2="13.081" layer="21"/>
<rectangle x1="14.94281875" y1="12.99641875" x2="16.21281875" y2="13.081" layer="21"/>
<rectangle x1="17.907" y1="12.99641875" x2="19.34718125" y2="13.081" layer="21"/>
<rectangle x1="20.61718125" y1="12.99641875" x2="24.08681875" y2="13.081" layer="21"/>
<rectangle x1="25.35681875" y1="12.99641875" x2="26.20518125" y2="13.081" layer="21"/>
<rectangle x1="27.13481875" y1="12.99641875" x2="29.76118125" y2="13.081" layer="21"/>
<rectangle x1="30.607" y1="12.99641875" x2="31.115" y2="13.081" layer="21"/>
<rectangle x1="35.26281875" y1="12.99641875" x2="39.41318125" y2="13.081" layer="21"/>
<rectangle x1="0.29718125" y1="13.081" x2="1.397" y2="13.16558125" layer="21"/>
<rectangle x1="5.03681875" y1="13.081" x2="6.30681875" y2="13.16558125" layer="21"/>
<rectangle x1="9.86281875" y1="13.081" x2="11.049" y2="13.16558125" layer="21"/>
<rectangle x1="15.02918125" y1="13.081" x2="16.21281875" y2="13.16558125" layer="21"/>
<rectangle x1="17.907" y1="13.081" x2="19.34718125" y2="13.16558125" layer="21"/>
<rectangle x1="20.53081875" y1="13.081" x2="24.17318125" y2="13.16558125" layer="21"/>
<rectangle x1="25.35681875" y1="13.081" x2="26.20518125" y2="13.16558125" layer="21"/>
<rectangle x1="27.13481875" y1="13.081" x2="29.76118125" y2="13.16558125" layer="21"/>
<rectangle x1="30.607" y1="13.081" x2="31.115" y2="13.16558125" layer="21"/>
<rectangle x1="35.34918125" y1="13.081" x2="39.41318125" y2="13.16558125" layer="21"/>
<rectangle x1="0.29718125" y1="13.16558125" x2="1.397" y2="13.25041875" layer="21"/>
<rectangle x1="5.03681875" y1="13.16558125" x2="6.223" y2="13.25041875" layer="21"/>
<rectangle x1="9.94918125" y1="13.16558125" x2="11.13281875" y2="13.25041875" layer="21"/>
<rectangle x1="15.02918125" y1="13.16558125" x2="16.21281875" y2="13.25041875" layer="21"/>
<rectangle x1="17.907" y1="13.16558125" x2="19.26081875" y2="13.25041875" layer="21"/>
<rectangle x1="20.53081875" y1="13.16558125" x2="24.17318125" y2="13.25041875" layer="21"/>
<rectangle x1="25.44318125" y1="13.16558125" x2="26.20518125" y2="13.25041875" layer="21"/>
<rectangle x1="27.13481875" y1="13.16558125" x2="29.76118125" y2="13.25041875" layer="21"/>
<rectangle x1="30.607" y1="13.16558125" x2="31.03118125" y2="13.25041875" layer="21"/>
<rectangle x1="35.34918125" y1="13.16558125" x2="39.41318125" y2="13.25041875" layer="21"/>
<rectangle x1="0.29718125" y1="13.25041875" x2="1.397" y2="13.335" layer="21"/>
<rectangle x1="5.03681875" y1="13.25041875" x2="6.13918125" y2="13.335" layer="21"/>
<rectangle x1="9.94918125" y1="13.25041875" x2="11.13281875" y2="13.335" layer="21"/>
<rectangle x1="15.113" y1="13.25041875" x2="16.29918125" y2="13.335" layer="21"/>
<rectangle x1="17.907" y1="13.25041875" x2="19.26081875" y2="13.335" layer="21"/>
<rectangle x1="20.447" y1="13.25041875" x2="24.257" y2="13.335" layer="21"/>
<rectangle x1="25.44318125" y1="13.25041875" x2="26.20518125" y2="13.335" layer="21"/>
<rectangle x1="27.13481875" y1="13.25041875" x2="29.76118125" y2="13.335" layer="21"/>
<rectangle x1="30.607" y1="13.25041875" x2="31.03118125" y2="13.335" layer="21"/>
<rectangle x1="35.433" y1="13.25041875" x2="39.41318125" y2="13.335" layer="21"/>
<rectangle x1="0.29718125" y1="13.335" x2="1.31318125" y2="13.41958125" layer="21"/>
<rectangle x1="4.953" y1="13.335" x2="6.13918125" y2="13.41958125" layer="21"/>
<rectangle x1="10.033" y1="13.335" x2="11.21918125" y2="13.41958125" layer="21"/>
<rectangle x1="15.19681875" y1="13.335" x2="16.29918125" y2="13.41958125" layer="21"/>
<rectangle x1="17.907" y1="13.335" x2="19.26081875" y2="13.41958125" layer="21"/>
<rectangle x1="20.36318125" y1="13.335" x2="24.34081875" y2="13.41958125" layer="21"/>
<rectangle x1="25.44318125" y1="13.335" x2="26.20518125" y2="13.41958125" layer="21"/>
<rectangle x1="27.13481875" y1="13.335" x2="29.76118125" y2="13.41958125" layer="21"/>
<rectangle x1="30.607" y1="13.335" x2="31.03118125" y2="13.41958125" layer="21"/>
<rectangle x1="35.433" y1="13.335" x2="39.41318125" y2="13.41958125" layer="21"/>
<rectangle x1="0.29718125" y1="13.41958125" x2="1.31318125" y2="13.50441875" layer="21"/>
<rectangle x1="4.953" y1="13.41958125" x2="6.05281875" y2="13.50441875" layer="21"/>
<rectangle x1="10.033" y1="13.41958125" x2="11.21918125" y2="13.50441875" layer="21"/>
<rectangle x1="15.19681875" y1="13.41958125" x2="16.29918125" y2="13.50441875" layer="21"/>
<rectangle x1="17.907" y1="13.41958125" x2="19.177" y2="13.50441875" layer="21"/>
<rectangle x1="20.36318125" y1="13.41958125" x2="24.34081875" y2="13.50441875" layer="21"/>
<rectangle x1="25.44318125" y1="13.41958125" x2="26.20518125" y2="13.50441875" layer="21"/>
<rectangle x1="27.13481875" y1="13.41958125" x2="29.76118125" y2="13.50441875" layer="21"/>
<rectangle x1="30.607" y1="13.41958125" x2="31.03118125" y2="13.50441875" layer="21"/>
<rectangle x1="35.433" y1="13.41958125" x2="39.41318125" y2="13.50441875" layer="21"/>
<rectangle x1="0.21081875" y1="13.50441875" x2="1.31318125" y2="13.589" layer="21"/>
<rectangle x1="4.953" y1="13.50441875" x2="6.05281875" y2="13.589" layer="21"/>
<rectangle x1="10.11681875" y1="13.50441875" x2="11.21918125" y2="13.589" layer="21"/>
<rectangle x1="15.19681875" y1="13.50441875" x2="16.383" y2="13.589" layer="21"/>
<rectangle x1="17.907" y1="13.50441875" x2="19.177" y2="13.589" layer="21"/>
<rectangle x1="20.36318125" y1="13.50441875" x2="24.34081875" y2="13.589" layer="21"/>
<rectangle x1="25.527" y1="13.50441875" x2="26.20518125" y2="13.589" layer="21"/>
<rectangle x1="27.13481875" y1="13.50441875" x2="29.67481875" y2="13.589" layer="21"/>
<rectangle x1="30.607" y1="13.50441875" x2="31.03118125" y2="13.589" layer="21"/>
<rectangle x1="35.433" y1="13.50441875" x2="39.41318125" y2="13.589" layer="21"/>
<rectangle x1="0.21081875" y1="13.589" x2="1.31318125" y2="13.67358125" layer="21"/>
<rectangle x1="4.86918125" y1="13.589" x2="6.05281875" y2="13.67358125" layer="21"/>
<rectangle x1="10.11681875" y1="13.589" x2="11.21918125" y2="13.67358125" layer="21"/>
<rectangle x1="15.28318125" y1="13.589" x2="16.383" y2="13.67358125" layer="21"/>
<rectangle x1="17.907" y1="13.589" x2="19.177" y2="13.67358125" layer="21"/>
<rectangle x1="20.27681875" y1="13.589" x2="24.42718125" y2="13.67358125" layer="21"/>
<rectangle x1="25.527" y1="13.589" x2="26.20518125" y2="13.67358125" layer="21"/>
<rectangle x1="27.13481875" y1="13.589" x2="29.67481875" y2="13.67358125" layer="21"/>
<rectangle x1="30.607" y1="13.589" x2="31.115" y2="13.67358125" layer="21"/>
<rectangle x1="35.433" y1="13.589" x2="39.41318125" y2="13.67358125" layer="21"/>
<rectangle x1="0.21081875" y1="13.67358125" x2="1.31318125" y2="13.75841875" layer="21"/>
<rectangle x1="4.86918125" y1="13.67358125" x2="5.969" y2="13.75841875" layer="21"/>
<rectangle x1="10.20318125" y1="13.67358125" x2="11.303" y2="13.75841875" layer="21"/>
<rectangle x1="15.28318125" y1="13.67358125" x2="16.383" y2="13.75841875" layer="21"/>
<rectangle x1="17.82318125" y1="13.67358125" x2="19.177" y2="13.75841875" layer="21"/>
<rectangle x1="20.27681875" y1="13.67358125" x2="24.42718125" y2="13.75841875" layer="21"/>
<rectangle x1="25.527" y1="13.67358125" x2="26.20518125" y2="13.75841875" layer="21"/>
<rectangle x1="27.13481875" y1="13.67358125" x2="29.67481875" y2="13.75841875" layer="21"/>
<rectangle x1="30.52318125" y1="13.67358125" x2="31.115" y2="13.75841875" layer="21"/>
<rectangle x1="35.433" y1="13.67358125" x2="39.41318125" y2="13.75841875" layer="21"/>
<rectangle x1="0.21081875" y1="13.75841875" x2="1.31318125" y2="13.843" layer="21"/>
<rectangle x1="4.86918125" y1="13.75841875" x2="5.969" y2="13.843" layer="21"/>
<rectangle x1="10.20318125" y1="13.75841875" x2="11.303" y2="13.843" layer="21"/>
<rectangle x1="15.28318125" y1="13.75841875" x2="16.383" y2="13.843" layer="21"/>
<rectangle x1="17.82318125" y1="13.75841875" x2="19.177" y2="13.843" layer="21"/>
<rectangle x1="20.27681875" y1="13.75841875" x2="24.42718125" y2="13.843" layer="21"/>
<rectangle x1="25.527" y1="13.75841875" x2="26.20518125" y2="13.843" layer="21"/>
<rectangle x1="27.22118125" y1="13.75841875" x2="29.67481875" y2="13.843" layer="21"/>
<rectangle x1="30.52318125" y1="13.75841875" x2="31.115" y2="13.843" layer="21"/>
<rectangle x1="32.04718125" y1="13.75841875" x2="34.417" y2="13.843" layer="21"/>
<rectangle x1="35.34918125" y1="13.75841875" x2="39.41318125" y2="13.843" layer="21"/>
<rectangle x1="0.21081875" y1="13.843" x2="1.31318125" y2="13.92758125" layer="21"/>
<rectangle x1="4.86918125" y1="13.843" x2="5.969" y2="13.92758125" layer="21"/>
<rectangle x1="10.20318125" y1="13.843" x2="11.303" y2="13.92758125" layer="21"/>
<rectangle x1="15.367" y1="13.843" x2="16.46681875" y2="13.92758125" layer="21"/>
<rectangle x1="17.82318125" y1="13.843" x2="19.09318125" y2="13.92758125" layer="21"/>
<rectangle x1="20.193" y1="13.843" x2="24.42718125" y2="13.92758125" layer="21"/>
<rectangle x1="25.61081875" y1="13.843" x2="26.20518125" y2="13.92758125" layer="21"/>
<rectangle x1="27.22118125" y1="13.843" x2="29.591" y2="13.92758125" layer="21"/>
<rectangle x1="30.52318125" y1="13.843" x2="31.115" y2="13.92758125" layer="21"/>
<rectangle x1="32.04718125" y1="13.843" x2="34.417" y2="13.92758125" layer="21"/>
<rectangle x1="35.34918125" y1="13.843" x2="39.41318125" y2="13.92758125" layer="21"/>
<rectangle x1="0.21081875" y1="13.92758125" x2="1.31318125" y2="14.01241875" layer="21"/>
<rectangle x1="4.86918125" y1="13.92758125" x2="5.969" y2="14.01241875" layer="21"/>
<rectangle x1="10.20318125" y1="13.92758125" x2="11.303" y2="14.01241875" layer="21"/>
<rectangle x1="15.367" y1="13.92758125" x2="16.46681875" y2="14.01241875" layer="21"/>
<rectangle x1="17.82318125" y1="13.92758125" x2="19.09318125" y2="14.01241875" layer="21"/>
<rectangle x1="20.193" y1="13.92758125" x2="24.511" y2="14.01241875" layer="21"/>
<rectangle x1="25.61081875" y1="13.92758125" x2="26.20518125" y2="14.01241875" layer="21"/>
<rectangle x1="27.22118125" y1="13.92758125" x2="29.591" y2="14.01241875" layer="21"/>
<rectangle x1="30.52318125" y1="13.92758125" x2="31.115" y2="14.01241875" layer="21"/>
<rectangle x1="32.04718125" y1="13.92758125" x2="34.417" y2="14.01241875" layer="21"/>
<rectangle x1="35.34918125" y1="13.92758125" x2="39.41318125" y2="14.01241875" layer="21"/>
<rectangle x1="0.21081875" y1="14.01241875" x2="1.31318125" y2="14.097" layer="21"/>
<rectangle x1="4.86918125" y1="14.01241875" x2="5.88518125" y2="14.097" layer="21"/>
<rectangle x1="10.20318125" y1="14.01241875" x2="11.303" y2="14.097" layer="21"/>
<rectangle x1="15.367" y1="14.01241875" x2="16.46681875" y2="14.097" layer="21"/>
<rectangle x1="17.82318125" y1="14.01241875" x2="19.09318125" y2="14.097" layer="21"/>
<rectangle x1="20.193" y1="14.01241875" x2="24.511" y2="14.097" layer="21"/>
<rectangle x1="25.61081875" y1="14.01241875" x2="26.20518125" y2="14.097" layer="21"/>
<rectangle x1="27.305" y1="14.01241875" x2="29.50718125" y2="14.097" layer="21"/>
<rectangle x1="30.52318125" y1="14.01241875" x2="31.19881875" y2="14.097" layer="21"/>
<rectangle x1="32.131" y1="14.01241875" x2="34.33318125" y2="14.097" layer="21"/>
<rectangle x1="35.26281875" y1="14.01241875" x2="39.41318125" y2="14.097" layer="21"/>
<rectangle x1="0.21081875" y1="14.097" x2="1.31318125" y2="14.18158125" layer="21"/>
<rectangle x1="4.78281875" y1="14.097" x2="5.88518125" y2="14.18158125" layer="21"/>
<rectangle x1="10.287" y1="14.097" x2="11.303" y2="14.18158125" layer="21"/>
<rectangle x1="15.367" y1="14.097" x2="16.46681875" y2="14.18158125" layer="21"/>
<rectangle x1="17.82318125" y1="14.097" x2="19.09318125" y2="14.18158125" layer="21"/>
<rectangle x1="20.193" y1="14.097" x2="24.511" y2="14.18158125" layer="21"/>
<rectangle x1="25.61081875" y1="14.097" x2="26.20518125" y2="14.18158125" layer="21"/>
<rectangle x1="27.38881875" y1="14.097" x2="29.50718125" y2="14.18158125" layer="21"/>
<rectangle x1="30.43681875" y1="14.097" x2="31.19881875" y2="14.18158125" layer="21"/>
<rectangle x1="32.21481875" y1="14.097" x2="34.33318125" y2="14.18158125" layer="21"/>
<rectangle x1="35.26281875" y1="14.097" x2="39.41318125" y2="14.18158125" layer="21"/>
<rectangle x1="0.21081875" y1="14.18158125" x2="1.31318125" y2="14.26641875" layer="21"/>
<rectangle x1="4.78281875" y1="14.18158125" x2="5.88518125" y2="14.26641875" layer="21"/>
<rectangle x1="10.287" y1="14.18158125" x2="11.303" y2="14.26641875" layer="21"/>
<rectangle x1="15.367" y1="14.18158125" x2="16.46681875" y2="14.26641875" layer="21"/>
<rectangle x1="17.82318125" y1="14.18158125" x2="19.09318125" y2="14.26641875" layer="21"/>
<rectangle x1="20.193" y1="14.18158125" x2="24.511" y2="14.26641875" layer="21"/>
<rectangle x1="25.61081875" y1="14.18158125" x2="26.20518125" y2="14.26641875" layer="21"/>
<rectangle x1="27.38881875" y1="14.18158125" x2="29.42081875" y2="14.26641875" layer="21"/>
<rectangle x1="30.43681875" y1="14.18158125" x2="31.19881875" y2="14.26641875" layer="21"/>
<rectangle x1="32.21481875" y1="14.18158125" x2="34.24681875" y2="14.26641875" layer="21"/>
<rectangle x1="35.26281875" y1="14.18158125" x2="39.41318125" y2="14.26641875" layer="21"/>
<rectangle x1="0.21081875" y1="14.26641875" x2="1.31318125" y2="14.351" layer="21"/>
<rectangle x1="4.78281875" y1="14.26641875" x2="5.88518125" y2="14.351" layer="21"/>
<rectangle x1="10.287" y1="14.26641875" x2="11.303" y2="14.351" layer="21"/>
<rectangle x1="15.367" y1="14.26641875" x2="16.46681875" y2="14.351" layer="21"/>
<rectangle x1="17.82318125" y1="14.26641875" x2="19.09318125" y2="14.351" layer="21"/>
<rectangle x1="20.193" y1="14.26641875" x2="24.511" y2="14.351" layer="21"/>
<rectangle x1="25.61081875" y1="14.26641875" x2="26.20518125" y2="14.351" layer="21"/>
<rectangle x1="27.47518125" y1="14.26641875" x2="29.337" y2="14.351" layer="21"/>
<rectangle x1="30.353" y1="14.26641875" x2="31.28518125" y2="14.351" layer="21"/>
<rectangle x1="32.30118125" y1="14.26641875" x2="34.163" y2="14.351" layer="21"/>
<rectangle x1="35.179" y1="14.26641875" x2="39.41318125" y2="14.351" layer="21"/>
<rectangle x1="0.21081875" y1="14.351" x2="1.31318125" y2="14.43558125" layer="21"/>
<rectangle x1="4.78281875" y1="14.351" x2="5.88518125" y2="14.43558125" layer="21"/>
<rectangle x1="10.287" y1="14.351" x2="11.303" y2="14.43558125" layer="21"/>
<rectangle x1="15.367" y1="14.351" x2="16.46681875" y2="14.43558125" layer="21"/>
<rectangle x1="17.82318125" y1="14.351" x2="19.09318125" y2="14.43558125" layer="21"/>
<rectangle x1="20.193" y1="14.351" x2="24.511" y2="14.43558125" layer="21"/>
<rectangle x1="25.61081875" y1="14.351" x2="26.20518125" y2="14.43558125" layer="21"/>
<rectangle x1="27.559" y1="14.351" x2="29.25318125" y2="14.43558125" layer="21"/>
<rectangle x1="30.353" y1="14.351" x2="31.28518125" y2="14.43558125" layer="21"/>
<rectangle x1="32.385" y1="14.351" x2="34.07918125" y2="14.43558125" layer="21"/>
<rectangle x1="35.179" y1="14.351" x2="39.41318125" y2="14.43558125" layer="21"/>
<rectangle x1="0.21081875" y1="14.43558125" x2="1.31318125" y2="14.52041875" layer="21"/>
<rectangle x1="4.78281875" y1="14.43558125" x2="5.88518125" y2="14.52041875" layer="21"/>
<rectangle x1="10.287" y1="14.43558125" x2="11.303" y2="14.52041875" layer="21"/>
<rectangle x1="15.367" y1="14.43558125" x2="16.46681875" y2="14.52041875" layer="21"/>
<rectangle x1="17.82318125" y1="14.43558125" x2="19.09318125" y2="14.52041875" layer="21"/>
<rectangle x1="20.193" y1="14.43558125" x2="24.511" y2="14.52041875" layer="21"/>
<rectangle x1="25.61081875" y1="14.43558125" x2="26.20518125" y2="14.52041875" layer="21"/>
<rectangle x1="27.72918125" y1="14.43558125" x2="29.083" y2="14.52041875" layer="21"/>
<rectangle x1="30.26918125" y1="14.43558125" x2="31.369" y2="14.52041875" layer="21"/>
<rectangle x1="32.55518125" y1="14.43558125" x2="33.909" y2="14.52041875" layer="21"/>
<rectangle x1="35.09518125" y1="14.43558125" x2="39.41318125" y2="14.52041875" layer="21"/>
<rectangle x1="0.21081875" y1="14.52041875" x2="1.31318125" y2="14.605" layer="21"/>
<rectangle x1="4.78281875" y1="14.52041875" x2="5.88518125" y2="14.605" layer="21"/>
<rectangle x1="10.287" y1="14.52041875" x2="11.303" y2="14.605" layer="21"/>
<rectangle x1="15.367" y1="14.52041875" x2="16.46681875" y2="14.605" layer="21"/>
<rectangle x1="17.82318125" y1="14.52041875" x2="19.09318125" y2="14.605" layer="21"/>
<rectangle x1="20.193" y1="14.52041875" x2="24.511" y2="14.605" layer="21"/>
<rectangle x1="25.61081875" y1="14.52041875" x2="26.20518125" y2="14.605" layer="21"/>
<rectangle x1="27.89681875" y1="14.52041875" x2="28.91281875" y2="14.605" layer="21"/>
<rectangle x1="30.26918125" y1="14.52041875" x2="31.45281875" y2="14.605" layer="21"/>
<rectangle x1="32.72281875" y1="14.52041875" x2="33.82518125" y2="14.605" layer="21"/>
<rectangle x1="35.00881875" y1="14.52041875" x2="39.41318125" y2="14.605" layer="21"/>
<rectangle x1="0.21081875" y1="14.605" x2="1.31318125" y2="14.68958125" layer="21"/>
<rectangle x1="4.78281875" y1="14.605" x2="5.88518125" y2="14.68958125" layer="21"/>
<rectangle x1="10.287" y1="14.605" x2="11.303" y2="14.68958125" layer="21"/>
<rectangle x1="15.367" y1="14.605" x2="16.46681875" y2="14.68958125" layer="21"/>
<rectangle x1="17.82318125" y1="14.605" x2="19.09318125" y2="14.68958125" layer="21"/>
<rectangle x1="20.193" y1="14.605" x2="24.511" y2="14.68958125" layer="21"/>
<rectangle x1="25.61081875" y1="14.605" x2="26.20518125" y2="14.68958125" layer="21"/>
<rectangle x1="28.15081875" y1="14.605" x2="28.65881875" y2="14.68958125" layer="21"/>
<rectangle x1="30.18281875" y1="14.605" x2="31.45281875" y2="14.68958125" layer="21"/>
<rectangle x1="32.97681875" y1="14.605" x2="33.48481875" y2="14.68958125" layer="21"/>
<rectangle x1="35.00881875" y1="14.605" x2="39.41318125" y2="14.68958125" layer="21"/>
<rectangle x1="0.21081875" y1="14.68958125" x2="1.31318125" y2="14.77441875" layer="21"/>
<rectangle x1="4.78281875" y1="14.68958125" x2="5.88518125" y2="14.77441875" layer="21"/>
<rectangle x1="10.287" y1="14.68958125" x2="11.303" y2="14.77441875" layer="21"/>
<rectangle x1="15.367" y1="14.68958125" x2="16.46681875" y2="14.77441875" layer="21"/>
<rectangle x1="17.82318125" y1="14.68958125" x2="19.09318125" y2="14.77441875" layer="21"/>
<rectangle x1="20.193" y1="14.68958125" x2="24.511" y2="14.77441875" layer="21"/>
<rectangle x1="25.61081875" y1="14.68958125" x2="26.20518125" y2="14.77441875" layer="21"/>
<rectangle x1="30.099" y1="14.68958125" x2="31.53918125" y2="14.77441875" layer="21"/>
<rectangle x1="34.925" y1="14.68958125" x2="39.41318125" y2="14.77441875" layer="21"/>
<rectangle x1="0.21081875" y1="14.77441875" x2="1.31318125" y2="14.859" layer="21"/>
<rectangle x1="4.78281875" y1="14.77441875" x2="5.88518125" y2="14.859" layer="21"/>
<rectangle x1="10.287" y1="14.77441875" x2="11.303" y2="14.859" layer="21"/>
<rectangle x1="15.367" y1="14.77441875" x2="16.46681875" y2="14.859" layer="21"/>
<rectangle x1="17.82318125" y1="14.77441875" x2="19.09318125" y2="14.859" layer="21"/>
<rectangle x1="20.193" y1="14.77441875" x2="24.42718125" y2="14.859" layer="21"/>
<rectangle x1="25.61081875" y1="14.77441875" x2="26.20518125" y2="14.859" layer="21"/>
<rectangle x1="30.01518125" y1="14.77441875" x2="31.623" y2="14.859" layer="21"/>
<rectangle x1="34.84118125" y1="14.77441875" x2="39.41318125" y2="14.859" layer="21"/>
<rectangle x1="0.21081875" y1="14.859" x2="1.31318125" y2="14.94358125" layer="21"/>
<rectangle x1="4.78281875" y1="14.859" x2="5.88518125" y2="14.94358125" layer="21"/>
<rectangle x1="10.287" y1="14.859" x2="11.303" y2="14.94358125" layer="21"/>
<rectangle x1="15.28318125" y1="14.859" x2="16.383" y2="14.94358125" layer="21"/>
<rectangle x1="17.82318125" y1="14.859" x2="19.09318125" y2="14.94358125" layer="21"/>
<rectangle x1="20.27681875" y1="14.859" x2="24.42718125" y2="14.94358125" layer="21"/>
<rectangle x1="25.527" y1="14.859" x2="26.20518125" y2="14.94358125" layer="21"/>
<rectangle x1="29.92881875" y1="14.859" x2="31.70681875" y2="14.94358125" layer="21"/>
<rectangle x1="34.75481875" y1="14.859" x2="39.41318125" y2="14.94358125" layer="21"/>
<rectangle x1="0.21081875" y1="14.94358125" x2="1.31318125" y2="15.02841875" layer="21"/>
<rectangle x1="4.78281875" y1="14.94358125" x2="5.88518125" y2="15.02841875" layer="21"/>
<rectangle x1="10.287" y1="14.94358125" x2="11.303" y2="15.02841875" layer="21"/>
<rectangle x1="15.28318125" y1="14.94358125" x2="16.383" y2="15.02841875" layer="21"/>
<rectangle x1="17.82318125" y1="14.94358125" x2="19.177" y2="15.02841875" layer="21"/>
<rectangle x1="20.27681875" y1="14.94358125" x2="24.42718125" y2="15.02841875" layer="21"/>
<rectangle x1="25.527" y1="14.94358125" x2="26.20518125" y2="15.02841875" layer="21"/>
<rectangle x1="29.845" y1="14.94358125" x2="31.79318125" y2="15.02841875" layer="21"/>
<rectangle x1="34.671" y1="14.94358125" x2="39.41318125" y2="15.02841875" layer="21"/>
<rectangle x1="0.21081875" y1="15.02841875" x2="1.31318125" y2="15.113" layer="21"/>
<rectangle x1="4.78281875" y1="15.02841875" x2="5.88518125" y2="15.113" layer="21"/>
<rectangle x1="10.287" y1="15.02841875" x2="11.303" y2="15.113" layer="21"/>
<rectangle x1="15.28318125" y1="15.02841875" x2="16.383" y2="15.113" layer="21"/>
<rectangle x1="17.82318125" y1="15.02841875" x2="19.177" y2="15.113" layer="21"/>
<rectangle x1="20.27681875" y1="15.02841875" x2="24.42718125" y2="15.113" layer="21"/>
<rectangle x1="25.527" y1="15.02841875" x2="26.20518125" y2="15.113" layer="21"/>
<rectangle x1="29.76118125" y1="15.02841875" x2="31.877" y2="15.113" layer="21"/>
<rectangle x1="34.58718125" y1="15.02841875" x2="39.41318125" y2="15.113" layer="21"/>
<rectangle x1="0.29718125" y1="15.113" x2="1.31318125" y2="15.19758125" layer="21"/>
<rectangle x1="4.78281875" y1="15.113" x2="5.88518125" y2="15.19758125" layer="21"/>
<rectangle x1="10.287" y1="15.113" x2="11.303" y2="15.19758125" layer="21"/>
<rectangle x1="15.19681875" y1="15.113" x2="16.383" y2="15.19758125" layer="21"/>
<rectangle x1="17.907" y1="15.113" x2="19.177" y2="15.19758125" layer="21"/>
<rectangle x1="20.36318125" y1="15.113" x2="24.34081875" y2="15.19758125" layer="21"/>
<rectangle x1="25.527" y1="15.113" x2="26.20518125" y2="15.19758125" layer="21"/>
<rectangle x1="27.051" y1="15.113" x2="27.22118125" y2="15.19758125" layer="21"/>
<rectangle x1="29.67481875" y1="15.113" x2="32.04718125" y2="15.19758125" layer="21"/>
<rectangle x1="34.50081875" y1="15.113" x2="39.41318125" y2="15.19758125" layer="21"/>
<rectangle x1="0.29718125" y1="15.19758125" x2="1.31318125" y2="15.28241875" layer="21"/>
<rectangle x1="4.78281875" y1="15.19758125" x2="5.88518125" y2="15.28241875" layer="21"/>
<rectangle x1="10.287" y1="15.19758125" x2="11.303" y2="15.28241875" layer="21"/>
<rectangle x1="15.19681875" y1="15.19758125" x2="16.29918125" y2="15.28241875" layer="21"/>
<rectangle x1="17.907" y1="15.19758125" x2="19.177" y2="15.28241875" layer="21"/>
<rectangle x1="20.36318125" y1="15.19758125" x2="24.34081875" y2="15.28241875" layer="21"/>
<rectangle x1="25.44318125" y1="15.19758125" x2="26.289" y2="15.28241875" layer="21"/>
<rectangle x1="27.051" y1="15.19758125" x2="27.305" y2="15.28241875" layer="21"/>
<rectangle x1="29.50718125" y1="15.19758125" x2="32.131" y2="15.28241875" layer="21"/>
<rectangle x1="34.33318125" y1="15.19758125" x2="39.41318125" y2="15.28241875" layer="21"/>
<rectangle x1="0.29718125" y1="15.28241875" x2="1.397" y2="15.367" layer="21"/>
<rectangle x1="4.78281875" y1="15.28241875" x2="5.88518125" y2="15.367" layer="21"/>
<rectangle x1="10.287" y1="15.28241875" x2="11.303" y2="15.367" layer="21"/>
<rectangle x1="15.113" y1="15.28241875" x2="16.29918125" y2="15.367" layer="21"/>
<rectangle x1="17.907" y1="15.28241875" x2="19.26081875" y2="15.367" layer="21"/>
<rectangle x1="20.36318125" y1="15.28241875" x2="24.257" y2="15.367" layer="21"/>
<rectangle x1="25.44318125" y1="15.28241875" x2="26.289" y2="15.367" layer="21"/>
<rectangle x1="27.051" y1="15.28241875" x2="27.47518125" y2="15.367" layer="21"/>
<rectangle x1="29.337" y1="15.28241875" x2="32.30118125" y2="15.367" layer="21"/>
<rectangle x1="34.163" y1="15.28241875" x2="39.41318125" y2="15.367" layer="21"/>
<rectangle x1="0.29718125" y1="15.367" x2="1.397" y2="15.45158125" layer="21"/>
<rectangle x1="4.78281875" y1="15.367" x2="5.88518125" y2="15.45158125" layer="21"/>
<rectangle x1="10.287" y1="15.367" x2="11.303" y2="15.45158125" layer="21"/>
<rectangle x1="15.113" y1="15.367" x2="16.29918125" y2="15.45158125" layer="21"/>
<rectangle x1="17.907" y1="15.367" x2="19.26081875" y2="15.45158125" layer="21"/>
<rectangle x1="20.447" y1="15.367" x2="24.257" y2="15.45158125" layer="21"/>
<rectangle x1="25.44318125" y1="15.367" x2="26.37281875" y2="15.45158125" layer="21"/>
<rectangle x1="26.96718125" y1="15.367" x2="27.64281875" y2="15.45158125" layer="21"/>
<rectangle x1="29.16681875" y1="15.367" x2="32.46881875" y2="15.45158125" layer="21"/>
<rectangle x1="33.99281875" y1="15.367" x2="39.41318125" y2="15.45158125" layer="21"/>
<rectangle x1="0.29718125" y1="15.45158125" x2="1.397" y2="15.53641875" layer="21"/>
<rectangle x1="4.78281875" y1="15.45158125" x2="5.88518125" y2="15.53641875" layer="21"/>
<rectangle x1="10.287" y1="15.45158125" x2="11.303" y2="15.53641875" layer="21"/>
<rectangle x1="15.02918125" y1="15.45158125" x2="16.21281875" y2="15.53641875" layer="21"/>
<rectangle x1="17.907" y1="15.45158125" x2="19.26081875" y2="15.53641875" layer="21"/>
<rectangle x1="20.53081875" y1="15.45158125" x2="24.17318125" y2="15.53641875" layer="21"/>
<rectangle x1="25.35681875" y1="15.45158125" x2="26.45918125" y2="15.53641875" layer="21"/>
<rectangle x1="26.88081875" y1="15.45158125" x2="27.98318125" y2="15.53641875" layer="21"/>
<rectangle x1="28.91281875" y1="15.45158125" x2="32.80918125" y2="15.53641875" layer="21"/>
<rectangle x1="33.655" y1="15.45158125" x2="39.41318125" y2="15.53641875" layer="21"/>
<rectangle x1="0.29718125" y1="15.53641875" x2="1.397" y2="15.621" layer="21"/>
<rectangle x1="4.78281875" y1="15.53641875" x2="5.88518125" y2="15.621" layer="21"/>
<rectangle x1="10.287" y1="15.53641875" x2="11.303" y2="15.621" layer="21"/>
<rectangle x1="15.02918125" y1="15.53641875" x2="16.21281875" y2="15.621" layer="21"/>
<rectangle x1="17.907" y1="15.53641875" x2="19.34718125" y2="15.621" layer="21"/>
<rectangle x1="20.53081875" y1="15.53641875" x2="24.17318125" y2="15.621" layer="21"/>
<rectangle x1="25.35681875" y1="15.53641875" x2="39.41318125" y2="15.621" layer="21"/>
<rectangle x1="0.29718125" y1="15.621" x2="1.397" y2="15.70558125" layer="21"/>
<rectangle x1="4.78281875" y1="15.621" x2="5.88518125" y2="15.70558125" layer="21"/>
<rectangle x1="10.287" y1="15.621" x2="11.303" y2="15.70558125" layer="21"/>
<rectangle x1="14.94281875" y1="15.621" x2="16.21281875" y2="15.70558125" layer="21"/>
<rectangle x1="17.907" y1="15.621" x2="19.34718125" y2="15.70558125" layer="21"/>
<rectangle x1="20.61718125" y1="15.621" x2="24.08681875" y2="15.70558125" layer="21"/>
<rectangle x1="25.35681875" y1="15.621" x2="39.41318125" y2="15.70558125" layer="21"/>
<rectangle x1="0.29718125" y1="15.70558125" x2="1.397" y2="15.79041875" layer="21"/>
<rectangle x1="4.78281875" y1="15.70558125" x2="5.88518125" y2="15.79041875" layer="21"/>
<rectangle x1="10.287" y1="15.70558125" x2="11.303" y2="15.79041875" layer="21"/>
<rectangle x1="14.859" y1="15.70558125" x2="16.129" y2="15.79041875" layer="21"/>
<rectangle x1="17.907" y1="15.70558125" x2="19.431" y2="15.79041875" layer="21"/>
<rectangle x1="20.701" y1="15.70558125" x2="24.003" y2="15.79041875" layer="21"/>
<rectangle x1="25.273" y1="15.70558125" x2="39.41318125" y2="15.79041875" layer="21"/>
<rectangle x1="0.381" y1="15.79041875" x2="1.48081875" y2="15.875" layer="21"/>
<rectangle x1="4.78281875" y1="15.79041875" x2="5.88518125" y2="15.875" layer="21"/>
<rectangle x1="10.287" y1="15.79041875" x2="11.303" y2="15.875" layer="21"/>
<rectangle x1="14.77518125" y1="15.79041875" x2="16.129" y2="15.875" layer="21"/>
<rectangle x1="17.907" y1="15.79041875" x2="19.431" y2="15.875" layer="21"/>
<rectangle x1="20.78481875" y1="15.79041875" x2="23.91918125" y2="15.875" layer="21"/>
<rectangle x1="25.273" y1="15.79041875" x2="39.41318125" y2="15.875" layer="21"/>
<rectangle x1="0.381" y1="15.875" x2="1.48081875" y2="15.95958125" layer="21"/>
<rectangle x1="4.78281875" y1="15.875" x2="5.88518125" y2="15.95958125" layer="21"/>
<rectangle x1="10.287" y1="15.875" x2="11.303" y2="15.95958125" layer="21"/>
<rectangle x1="14.68881875" y1="15.875" x2="16.04518125" y2="15.95958125" layer="21"/>
<rectangle x1="17.907" y1="15.875" x2="19.51481875" y2="15.95958125" layer="21"/>
<rectangle x1="20.87118125" y1="15.875" x2="23.83281875" y2="15.95958125" layer="21"/>
<rectangle x1="25.18918125" y1="15.875" x2="39.41318125" y2="15.95958125" layer="21"/>
<rectangle x1="0.381" y1="15.95958125" x2="1.48081875" y2="16.04441875" layer="21"/>
<rectangle x1="4.78281875" y1="15.95958125" x2="5.88518125" y2="16.04441875" layer="21"/>
<rectangle x1="10.287" y1="15.95958125" x2="11.303" y2="16.04441875" layer="21"/>
<rectangle x1="14.605" y1="15.95958125" x2="16.04518125" y2="16.04441875" layer="21"/>
<rectangle x1="17.907" y1="15.95958125" x2="19.51481875" y2="16.04441875" layer="21"/>
<rectangle x1="20.955" y1="15.95958125" x2="23.749" y2="16.04441875" layer="21"/>
<rectangle x1="25.10281875" y1="15.95958125" x2="39.41318125" y2="16.04441875" layer="21"/>
<rectangle x1="0.381" y1="16.04441875" x2="1.48081875" y2="16.129" layer="21"/>
<rectangle x1="4.78281875" y1="16.04441875" x2="5.88518125" y2="16.129" layer="21"/>
<rectangle x1="10.287" y1="16.04441875" x2="11.303" y2="16.129" layer="21"/>
<rectangle x1="14.52118125" y1="16.04441875" x2="15.95881875" y2="16.129" layer="21"/>
<rectangle x1="17.907" y1="16.04441875" x2="19.60118125" y2="16.129" layer="21"/>
<rectangle x1="21.03881875" y1="16.04441875" x2="23.66518125" y2="16.129" layer="21"/>
<rectangle x1="25.10281875" y1="16.04441875" x2="39.32681875" y2="16.129" layer="21"/>
<rectangle x1="0.381" y1="16.129" x2="1.56718125" y2="16.21358125" layer="21"/>
<rectangle x1="4.78281875" y1="16.129" x2="5.88518125" y2="16.21358125" layer="21"/>
<rectangle x1="10.287" y1="16.129" x2="11.303" y2="16.21358125" layer="21"/>
<rectangle x1="14.351" y1="16.129" x2="15.875" y2="16.21358125" layer="21"/>
<rectangle x1="17.907" y1="16.129" x2="19.685" y2="16.21358125" layer="21"/>
<rectangle x1="21.209" y1="16.129" x2="23.495" y2="16.21358125" layer="21"/>
<rectangle x1="25.019" y1="16.129" x2="39.32681875" y2="16.21358125" layer="21"/>
<rectangle x1="0.46481875" y1="16.21358125" x2="1.56718125" y2="16.29841875" layer="21"/>
<rectangle x1="4.78281875" y1="16.21358125" x2="5.88518125" y2="16.29841875" layer="21"/>
<rectangle x1="10.287" y1="16.21358125" x2="11.303" y2="16.29841875" layer="21"/>
<rectangle x1="14.18081875" y1="16.21358125" x2="15.875" y2="16.29841875" layer="21"/>
<rectangle x1="17.99081875" y1="16.21358125" x2="19.685" y2="16.29841875" layer="21"/>
<rectangle x1="21.29281875" y1="16.21358125" x2="23.41118125" y2="16.29841875" layer="21"/>
<rectangle x1="24.93518125" y1="16.21358125" x2="39.32681875" y2="16.29841875" layer="21"/>
<rectangle x1="0.46481875" y1="16.29841875" x2="1.56718125" y2="16.383" layer="21"/>
<rectangle x1="4.78281875" y1="16.29841875" x2="5.88518125" y2="16.383" layer="21"/>
<rectangle x1="10.287" y1="16.29841875" x2="11.303" y2="16.383" layer="21"/>
<rectangle x1="14.097" y1="16.29841875" x2="15.79118125" y2="16.383" layer="21"/>
<rectangle x1="17.99081875" y1="16.29841875" x2="19.76881875" y2="16.383" layer="21"/>
<rectangle x1="21.463" y1="16.29841875" x2="23.241" y2="16.383" layer="21"/>
<rectangle x1="24.93518125" y1="16.29841875" x2="39.32681875" y2="16.383" layer="21"/>
<rectangle x1="0.46481875" y1="16.383" x2="1.651" y2="16.46758125" layer="21"/>
<rectangle x1="4.78281875" y1="16.383" x2="5.88518125" y2="16.46758125" layer="21"/>
<rectangle x1="10.287" y1="16.383" x2="11.303" y2="16.46758125" layer="21"/>
<rectangle x1="13.843" y1="16.383" x2="15.70481875" y2="16.46758125" layer="21"/>
<rectangle x1="17.99081875" y1="16.383" x2="19.85518125" y2="16.46758125" layer="21"/>
<rectangle x1="21.80081875" y1="16.383" x2="22.987" y2="16.46758125" layer="21"/>
<rectangle x1="24.84881875" y1="16.383" x2="39.32681875" y2="16.46758125" layer="21"/>
<rectangle x1="0.46481875" y1="16.46758125" x2="1.651" y2="16.55241875" layer="21"/>
<rectangle x1="4.78281875" y1="16.46758125" x2="5.88518125" y2="16.55241875" layer="21"/>
<rectangle x1="10.287" y1="16.46758125" x2="11.47318125" y2="16.55241875" layer="21"/>
<rectangle x1="13.25118125" y1="16.46758125" x2="15.621" y2="16.55241875" layer="21"/>
<rectangle x1="17.99081875" y1="16.46758125" x2="19.939" y2="16.55241875" layer="21"/>
<rectangle x1="24.765" y1="16.46758125" x2="39.243" y2="16.55241875" layer="21"/>
<rectangle x1="0.55118125" y1="16.55241875" x2="1.651" y2="16.637" layer="21"/>
<rectangle x1="4.78281875" y1="16.55241875" x2="5.88518125" y2="16.637" layer="21"/>
<rectangle x1="10.287" y1="16.55241875" x2="15.53718125" y2="16.637" layer="21"/>
<rectangle x1="17.99081875" y1="16.55241875" x2="20.02281875" y2="16.637" layer="21"/>
<rectangle x1="24.68118125" y1="16.55241875" x2="39.243" y2="16.637" layer="21"/>
<rectangle x1="0.55118125" y1="16.637" x2="1.73481875" y2="16.72158125" layer="21"/>
<rectangle x1="4.78281875" y1="16.637" x2="5.88518125" y2="16.72158125" layer="21"/>
<rectangle x1="10.287" y1="16.637" x2="15.45081875" y2="16.72158125" layer="21"/>
<rectangle x1="17.99081875" y1="16.637" x2="20.10918125" y2="16.72158125" layer="21"/>
<rectangle x1="24.59481875" y1="16.637" x2="39.243" y2="16.72158125" layer="21"/>
<rectangle x1="0.55118125" y1="16.72158125" x2="1.73481875" y2="16.80641875" layer="21"/>
<rectangle x1="4.78281875" y1="16.72158125" x2="5.88518125" y2="16.80641875" layer="21"/>
<rectangle x1="10.287" y1="16.72158125" x2="15.367" y2="16.80641875" layer="21"/>
<rectangle x1="17.99081875" y1="16.72158125" x2="20.193" y2="16.80641875" layer="21"/>
<rectangle x1="24.511" y1="16.72158125" x2="39.15918125" y2="16.80641875" layer="21"/>
<rectangle x1="0.635" y1="16.80641875" x2="1.73481875" y2="16.891" layer="21"/>
<rectangle x1="4.78281875" y1="16.80641875" x2="5.88518125" y2="16.891" layer="21"/>
<rectangle x1="10.287" y1="16.80641875" x2="15.28318125" y2="16.891" layer="21"/>
<rectangle x1="18.07718125" y1="16.80641875" x2="20.27681875" y2="16.891" layer="21"/>
<rectangle x1="24.42718125" y1="16.80641875" x2="39.15918125" y2="16.891" layer="21"/>
<rectangle x1="0.635" y1="16.891" x2="1.82118125" y2="16.97558125" layer="21"/>
<rectangle x1="4.78281875" y1="16.891" x2="5.88518125" y2="16.97558125" layer="21"/>
<rectangle x1="10.287" y1="16.891" x2="15.19681875" y2="16.97558125" layer="21"/>
<rectangle x1="18.07718125" y1="16.891" x2="20.36318125" y2="16.97558125" layer="21"/>
<rectangle x1="24.34081875" y1="16.891" x2="39.15918125" y2="16.97558125" layer="21"/>
<rectangle x1="0.635" y1="16.97558125" x2="1.82118125" y2="17.06041875" layer="21"/>
<rectangle x1="4.78281875" y1="16.97558125" x2="5.88518125" y2="17.06041875" layer="21"/>
<rectangle x1="10.20318125" y1="16.97558125" x2="15.02918125" y2="17.06041875" layer="21"/>
<rectangle x1="18.07718125" y1="16.97558125" x2="20.53081875" y2="17.06041875" layer="21"/>
<rectangle x1="24.17318125" y1="16.97558125" x2="39.07281875" y2="17.06041875" layer="21"/>
<rectangle x1="0.71881875" y1="17.06041875" x2="1.905" y2="17.145" layer="21"/>
<rectangle x1="4.78281875" y1="17.06041875" x2="5.88518125" y2="17.145" layer="21"/>
<rectangle x1="10.287" y1="17.06041875" x2="14.94281875" y2="17.145" layer="21"/>
<rectangle x1="18.07718125" y1="17.06041875" x2="20.61718125" y2="17.145" layer="21"/>
<rectangle x1="24.08681875" y1="17.06041875" x2="39.07281875" y2="17.145" layer="21"/>
<rectangle x1="0.71881875" y1="17.145" x2="1.905" y2="17.22958125" layer="21"/>
<rectangle x1="4.86918125" y1="17.145" x2="5.88518125" y2="17.22958125" layer="21"/>
<rectangle x1="10.287" y1="17.145" x2="14.77518125" y2="17.22958125" layer="21"/>
<rectangle x1="18.07718125" y1="17.145" x2="20.78481875" y2="17.22958125" layer="21"/>
<rectangle x1="23.91918125" y1="17.145" x2="39.07281875" y2="17.22958125" layer="21"/>
<rectangle x1="0.80518125" y1="17.22958125" x2="1.98881875" y2="17.31441875" layer="21"/>
<rectangle x1="4.86918125" y1="17.22958125" x2="5.88518125" y2="17.31441875" layer="21"/>
<rectangle x1="10.287" y1="17.22958125" x2="14.605" y2="17.31441875" layer="21"/>
<rectangle x1="18.07718125" y1="17.22958125" x2="20.955" y2="17.31441875" layer="21"/>
<rectangle x1="23.749" y1="17.22958125" x2="38.989" y2="17.31441875" layer="21"/>
<rectangle x1="0.80518125" y1="17.31441875" x2="1.98881875" y2="17.399" layer="21"/>
<rectangle x1="4.86918125" y1="17.31441875" x2="5.79881875" y2="17.399" layer="21"/>
<rectangle x1="10.37081875" y1="17.31441875" x2="14.43481875" y2="17.399" layer="21"/>
<rectangle x1="18.161" y1="17.31441875" x2="21.12518125" y2="17.399" layer="21"/>
<rectangle x1="23.57881875" y1="17.31441875" x2="38.989" y2="17.399" layer="21"/>
<rectangle x1="0.80518125" y1="17.399" x2="2.07518125" y2="17.48358125" layer="21"/>
<rectangle x1="4.953" y1="17.399" x2="5.715" y2="17.48358125" layer="21"/>
<rectangle x1="10.37081875" y1="17.399" x2="14.18081875" y2="17.48358125" layer="21"/>
<rectangle x1="18.161" y1="17.399" x2="21.37918125" y2="17.48358125" layer="21"/>
<rectangle x1="23.32481875" y1="17.399" x2="38.90518125" y2="17.48358125" layer="21"/>
<rectangle x1="0.889" y1="17.48358125" x2="2.07518125" y2="17.56841875" layer="21"/>
<rectangle x1="5.12318125" y1="17.48358125" x2="5.63118125" y2="17.56841875" layer="21"/>
<rectangle x1="10.541" y1="17.48358125" x2="13.843" y2="17.56841875" layer="21"/>
<rectangle x1="18.161" y1="17.48358125" x2="21.717" y2="17.56841875" layer="21"/>
<rectangle x1="22.987" y1="17.48358125" x2="38.90518125" y2="17.56841875" layer="21"/>
<rectangle x1="0.97281875" y1="17.56841875" x2="2.159" y2="17.653" layer="21"/>
<rectangle x1="18.161" y1="17.56841875" x2="38.81881875" y2="17.653" layer="21"/>
<rectangle x1="0.97281875" y1="17.653" x2="2.159" y2="17.73758125" layer="21"/>
<rectangle x1="18.161" y1="17.653" x2="38.81881875" y2="17.73758125" layer="21"/>
<rectangle x1="0.97281875" y1="17.73758125" x2="2.24281875" y2="17.82241875" layer="21"/>
<rectangle x1="18.24481875" y1="17.73758125" x2="38.81881875" y2="17.82241875" layer="21"/>
<rectangle x1="1.05918125" y1="17.82241875" x2="2.24281875" y2="17.907" layer="21"/>
<rectangle x1="18.24481875" y1="17.82241875" x2="38.735" y2="17.907" layer="21"/>
<rectangle x1="1.05918125" y1="17.907" x2="2.32918125" y2="17.99158125" layer="21"/>
<rectangle x1="18.24481875" y1="17.907" x2="38.735" y2="17.99158125" layer="21"/>
<rectangle x1="1.143" y1="17.99158125" x2="2.413" y2="18.07641875" layer="21"/>
<rectangle x1="18.24481875" y1="17.99158125" x2="38.65118125" y2="18.07641875" layer="21"/>
<rectangle x1="1.143" y1="18.07641875" x2="2.413" y2="18.161" layer="21"/>
<rectangle x1="18.24481875" y1="18.07641875" x2="38.56481875" y2="18.161" layer="21"/>
<rectangle x1="1.22681875" y1="18.161" x2="2.49681875" y2="18.24558125" layer="21"/>
<rectangle x1="18.33118125" y1="18.161" x2="38.56481875" y2="18.24558125" layer="21"/>
<rectangle x1="1.22681875" y1="18.24558125" x2="2.58318125" y2="18.33041875" layer="21"/>
<rectangle x1="18.33118125" y1="18.24558125" x2="38.481" y2="18.33041875" layer="21"/>
<rectangle x1="1.31318125" y1="18.33041875" x2="2.667" y2="18.415" layer="21"/>
<rectangle x1="18.33118125" y1="18.33041875" x2="38.481" y2="18.415" layer="21"/>
<rectangle x1="1.397" y1="18.415" x2="2.667" y2="18.49958125" layer="21"/>
<rectangle x1="18.33118125" y1="18.415" x2="38.39718125" y2="18.49958125" layer="21"/>
<rectangle x1="1.397" y1="18.49958125" x2="2.75081875" y2="18.58441875" layer="21"/>
<rectangle x1="18.415" y1="18.49958125" x2="38.31081875" y2="18.58441875" layer="21"/>
<rectangle x1="1.48081875" y1="18.58441875" x2="2.83718125" y2="18.669" layer="21"/>
<rectangle x1="18.415" y1="18.58441875" x2="38.31081875" y2="18.669" layer="21"/>
<rectangle x1="1.56718125" y1="18.669" x2="2.921" y2="18.75358125" layer="21"/>
<rectangle x1="18.415" y1="18.669" x2="38.227" y2="18.75358125" layer="21"/>
<rectangle x1="1.56718125" y1="18.75358125" x2="3.00481875" y2="18.83841875" layer="21"/>
<rectangle x1="18.49881875" y1="18.75358125" x2="38.227" y2="18.83841875" layer="21"/>
<rectangle x1="1.651" y1="18.83841875" x2="3.00481875" y2="18.923" layer="21"/>
<rectangle x1="18.49881875" y1="18.83841875" x2="38.14318125" y2="18.923" layer="21"/>
<rectangle x1="1.73481875" y1="18.923" x2="3.09118125" y2="19.00758125" layer="21"/>
<rectangle x1="18.49881875" y1="18.923" x2="38.05681875" y2="19.00758125" layer="21"/>
<rectangle x1="1.73481875" y1="19.00758125" x2="3.175" y2="19.09241875" layer="21"/>
<rectangle x1="18.49881875" y1="19.00758125" x2="37.973" y2="19.09241875" layer="21"/>
<rectangle x1="1.82118125" y1="19.09241875" x2="3.25881875" y2="19.177" layer="21"/>
<rectangle x1="18.58518125" y1="19.09241875" x2="37.973" y2="19.177" layer="21"/>
<rectangle x1="1.905" y1="19.177" x2="3.34518125" y2="19.26158125" layer="21"/>
<rectangle x1="18.58518125" y1="19.177" x2="37.88918125" y2="19.26158125" layer="21"/>
<rectangle x1="1.98881875" y1="19.26158125" x2="3.429" y2="19.34641875" layer="21"/>
<rectangle x1="18.669" y1="19.26158125" x2="37.80281875" y2="19.34641875" layer="21"/>
<rectangle x1="2.07518125" y1="19.34641875" x2="3.51281875" y2="19.431" layer="21"/>
<rectangle x1="18.669" y1="19.34641875" x2="37.719" y2="19.431" layer="21"/>
<rectangle x1="2.07518125" y1="19.431" x2="3.683" y2="19.51558125" layer="21"/>
<rectangle x1="18.669" y1="19.431" x2="37.63518125" y2="19.51558125" layer="21"/>
<rectangle x1="2.159" y1="19.51558125" x2="3.76681875" y2="19.60041875" layer="21"/>
<rectangle x1="18.669" y1="19.51558125" x2="37.54881875" y2="19.60041875" layer="21"/>
<rectangle x1="2.24281875" y1="19.60041875" x2="3.85318125" y2="19.685" layer="21"/>
<rectangle x1="18.75281875" y1="19.60041875" x2="37.465" y2="19.685" layer="21"/>
<rectangle x1="2.32918125" y1="19.685" x2="3.937" y2="19.76958125" layer="21"/>
<rectangle x1="18.75281875" y1="19.685" x2="37.465" y2="19.76958125" layer="21"/>
<rectangle x1="2.413" y1="19.76958125" x2="4.10718125" y2="19.85441875" layer="21"/>
<rectangle x1="18.83918125" y1="19.76958125" x2="37.38118125" y2="19.85441875" layer="21"/>
<rectangle x1="2.49681875" y1="19.85441875" x2="4.191" y2="19.939" layer="21"/>
<rectangle x1="18.83918125" y1="19.85441875" x2="37.29481875" y2="19.939" layer="21"/>
<rectangle x1="2.58318125" y1="19.939" x2="4.27481875" y2="20.02358125" layer="21"/>
<rectangle x1="18.923" y1="19.939" x2="37.211" y2="20.02358125" layer="21"/>
<rectangle x1="2.667" y1="20.02358125" x2="4.445" y2="20.10841875" layer="21"/>
<rectangle x1="18.923" y1="20.02358125" x2="37.12718125" y2="20.10841875" layer="21"/>
<rectangle x1="2.75081875" y1="20.10841875" x2="4.52881875" y2="20.193" layer="21"/>
<rectangle x1="19.00681875" y1="20.10841875" x2="36.957" y2="20.193" layer="21"/>
<rectangle x1="2.83718125" y1="20.193" x2="4.699" y2="20.27758125" layer="21"/>
<rectangle x1="19.00681875" y1="20.193" x2="36.87318125" y2="20.27758125" layer="21"/>
<rectangle x1="3.00481875" y1="20.27758125" x2="4.86918125" y2="20.36241875" layer="21"/>
<rectangle x1="19.09318125" y1="20.27758125" x2="36.78681875" y2="20.36241875" layer="21"/>
<rectangle x1="3.09118125" y1="20.36241875" x2="5.03681875" y2="20.447" layer="21"/>
<rectangle x1="19.09318125" y1="20.36241875" x2="36.703" y2="20.447" layer="21"/>
<rectangle x1="3.175" y1="20.447" x2="5.207" y2="20.53158125" layer="21"/>
<rectangle x1="19.177" y1="20.447" x2="36.61918125" y2="20.53158125" layer="21"/>
<rectangle x1="3.25881875" y1="20.53158125" x2="5.37718125" y2="20.61641875" layer="21"/>
<rectangle x1="19.177" y1="20.53158125" x2="36.53281875" y2="20.61641875" layer="21"/>
<rectangle x1="3.429" y1="20.61641875" x2="5.54481875" y2="20.701" layer="21"/>
<rectangle x1="19.26081875" y1="20.61641875" x2="36.36518125" y2="20.701" layer="21"/>
<rectangle x1="3.51281875" y1="20.701" x2="5.79881875" y2="20.78558125" layer="21"/>
<rectangle x1="19.26081875" y1="20.701" x2="36.27881875" y2="20.78558125" layer="21"/>
<rectangle x1="3.683" y1="20.78558125" x2="6.05281875" y2="20.87041875" layer="21"/>
<rectangle x1="19.34718125" y1="20.78558125" x2="36.195" y2="20.87041875" layer="21"/>
<rectangle x1="3.76681875" y1="20.87041875" x2="6.30681875" y2="20.955" layer="21"/>
<rectangle x1="19.34718125" y1="20.87041875" x2="36.02481875" y2="20.955" layer="21"/>
<rectangle x1="3.85318125" y1="20.955" x2="6.731" y2="21.03958125" layer="21"/>
<rectangle x1="19.431" y1="20.955" x2="35.85718125" y2="21.03958125" layer="21"/>
<rectangle x1="4.02081875" y1="21.03958125" x2="7.239" y2="21.12441875" layer="21"/>
<rectangle x1="19.51481875" y1="21.03958125" x2="35.77081875" y2="21.12441875" layer="21"/>
<rectangle x1="4.191" y1="21.12441875" x2="35.60318125" y2="21.209" layer="21"/>
<rectangle x1="4.27481875" y1="21.209" x2="35.433" y2="21.29358125" layer="21"/>
<rectangle x1="4.445" y1="21.29358125" x2="35.26281875" y2="21.37841875" layer="21"/>
<rectangle x1="4.61518125" y1="21.37841875" x2="35.179" y2="21.463" layer="21"/>
<rectangle x1="4.78281875" y1="21.463" x2="34.925" y2="21.54758125" layer="21"/>
<rectangle x1="4.953" y1="21.54758125" x2="34.75481875" y2="21.63241875" layer="21"/>
<rectangle x1="5.207" y1="21.63241875" x2="34.58718125" y2="21.717" layer="21"/>
<rectangle x1="5.461" y1="21.717" x2="34.33318125" y2="21.80158125" layer="21"/>
<rectangle x1="5.715" y1="21.80158125" x2="34.07918125" y2="21.88641875" layer="21"/>
<rectangle x1="5.969" y1="21.88641875" x2="33.82518125" y2="21.971" layer="21"/>
<rectangle x1="6.30681875" y1="21.971" x2="33.48481875" y2="22.05558125" layer="21"/>
<rectangle x1="6.731" y1="22.05558125" x2="32.97681875" y2="22.14041875" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="UDO-LOGO-10MM" urn="urn:adsk.eagle:package:6649258/2" type="box">
<packageinstances>
<packageinstance name="UDO-LOGO-10MM"/>
</packageinstances>
</package3d>
<package3d name="UDO-LOGO-12MM" urn="urn:adsk.eagle:package:6649257/3" type="box">
<packageinstances>
<packageinstance name="UDO-LOGO-12MM"/>
</packageinstances>
</package3d>
<package3d name="UDO-LOGO-15MM" urn="urn:adsk.eagle:package:6649256/2" type="box">
<packageinstances>
<packageinstance name="UDO-LOGO-15MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="UDO-LOGO-20MM">
<rectangle x1="3.00481875" y1="0.29641875" x2="3.429" y2="0.381" layer="94"/>
<rectangle x1="14.351" y1="0.29641875" x2="14.68881875" y2="0.381" layer="94"/>
<rectangle x1="2.75081875" y1="0.381" x2="3.683" y2="0.46558125" layer="94"/>
<rectangle x1="4.445" y1="0.381" x2="4.52881875" y2="0.46558125" layer="94"/>
<rectangle x1="5.54481875" y1="0.381" x2="5.715" y2="0.46558125" layer="94"/>
<rectangle x1="5.969" y1="0.381" x2="6.05281875" y2="0.46558125" layer="94"/>
<rectangle x1="6.985" y1="0.381" x2="7.15518125" y2="0.46558125" layer="94"/>
<rectangle x1="7.57681875" y1="0.381" x2="8.42518125" y2="0.46558125" layer="94"/>
<rectangle x1="9.94918125" y1="0.381" x2="10.20318125" y2="0.46558125" layer="94"/>
<rectangle x1="11.21918125" y1="0.381" x2="11.38681875" y2="0.46558125" layer="94"/>
<rectangle x1="12.40281875" y1="0.381" x2="12.74318125" y2="0.46558125" layer="94"/>
<rectangle x1="14.097" y1="0.381" x2="14.94281875" y2="0.46558125" layer="94"/>
<rectangle x1="15.70481875" y1="0.381" x2="15.79118125" y2="0.46558125" layer="94"/>
<rectangle x1="16.80718125" y1="0.381" x2="16.891" y2="0.46558125" layer="94"/>
<rectangle x1="17.653" y1="0.381" x2="17.907" y2="0.46558125" layer="94"/>
<rectangle x1="2.667" y1="0.46558125" x2="3.85318125" y2="0.55041875" layer="94"/>
<rectangle x1="4.36118125" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="94"/>
<rectangle x1="5.54481875" y1="0.46558125" x2="5.715" y2="0.55041875" layer="94"/>
<rectangle x1="5.88518125" y1="0.46558125" x2="6.13918125" y2="0.55041875" layer="94"/>
<rectangle x1="6.731" y1="0.46558125" x2="7.239" y2="0.55041875" layer="94"/>
<rectangle x1="7.493" y1="0.46558125" x2="8.67918125" y2="0.55041875" layer="94"/>
<rectangle x1="9.69518125" y1="0.46558125" x2="10.37081875" y2="0.55041875" layer="94"/>
<rectangle x1="11.049" y1="0.46558125" x2="11.64081875" y2="0.55041875" layer="94"/>
<rectangle x1="12.23518125" y1="0.46558125" x2="12.91081875" y2="0.55041875" layer="94"/>
<rectangle x1="13.92681875" y1="0.46558125" x2="15.113" y2="0.55041875" layer="94"/>
<rectangle x1="15.621" y1="0.46558125" x2="15.875" y2="0.55041875" layer="94"/>
<rectangle x1="16.72081875" y1="0.46558125" x2="16.97481875" y2="0.55041875" layer="94"/>
<rectangle x1="17.399" y1="0.46558125" x2="18.07718125" y2="0.55041875" layer="94"/>
<rectangle x1="2.58318125" y1="0.55041875" x2="3.00481875" y2="0.635" layer="94"/>
<rectangle x1="3.51281875" y1="0.55041875" x2="3.937" y2="0.635" layer="94"/>
<rectangle x1="4.36118125" y1="0.55041875" x2="4.61518125" y2="0.635" layer="94"/>
<rectangle x1="5.54481875" y1="0.55041875" x2="5.715" y2="0.635" layer="94"/>
<rectangle x1="5.88518125" y1="0.55041875" x2="6.13918125" y2="0.635" layer="94"/>
<rectangle x1="6.64718125" y1="0.55041875" x2="7.239" y2="0.635" layer="94"/>
<rectangle x1="7.493" y1="0.55041875" x2="8.84681875" y2="0.635" layer="94"/>
<rectangle x1="9.60881875" y1="0.55041875" x2="10.45718125" y2="0.635" layer="94"/>
<rectangle x1="10.96518125" y1="0.55041875" x2="11.72718125" y2="0.635" layer="94"/>
<rectangle x1="12.14881875" y1="0.55041875" x2="12.99718125" y2="0.635" layer="94"/>
<rectangle x1="13.843" y1="0.55041875" x2="14.26718125" y2="0.635" layer="94"/>
<rectangle x1="14.77518125" y1="0.55041875" x2="15.19681875" y2="0.635" layer="94"/>
<rectangle x1="15.621" y1="0.55041875" x2="15.875" y2="0.635" layer="94"/>
<rectangle x1="16.72081875" y1="0.55041875" x2="16.97481875" y2="0.635" layer="94"/>
<rectangle x1="17.31518125" y1="0.55041875" x2="18.24481875" y2="0.635" layer="94"/>
<rectangle x1="2.49681875" y1="0.635" x2="2.83718125" y2="0.71958125" layer="94"/>
<rectangle x1="3.683" y1="0.635" x2="4.02081875" y2="0.71958125" layer="94"/>
<rectangle x1="4.36118125" y1="0.635" x2="4.61518125" y2="0.71958125" layer="94"/>
<rectangle x1="5.54481875" y1="0.635" x2="5.715" y2="0.71958125" layer="94"/>
<rectangle x1="5.88518125" y1="0.635" x2="6.13918125" y2="0.71958125" layer="94"/>
<rectangle x1="6.56081875" y1="0.635" x2="6.90118125" y2="0.71958125" layer="94"/>
<rectangle x1="7.493" y1="0.635" x2="7.747" y2="0.71958125" layer="94"/>
<rectangle x1="8.509" y1="0.635" x2="8.93318125" y2="0.71958125" layer="94"/>
<rectangle x1="9.525" y1="0.635" x2="9.86281875" y2="0.71958125" layer="94"/>
<rectangle x1="10.20318125" y1="0.635" x2="10.541" y2="0.71958125" layer="94"/>
<rectangle x1="10.87881875" y1="0.635" x2="11.13281875" y2="0.71958125" layer="94"/>
<rectangle x1="11.47318125" y1="0.635" x2="11.811" y2="0.71958125" layer="94"/>
<rectangle x1="12.065" y1="0.635" x2="12.40281875" y2="0.71958125" layer="94"/>
<rectangle x1="12.74318125" y1="0.635" x2="13.081" y2="0.71958125" layer="94"/>
<rectangle x1="13.75918125" y1="0.635" x2="14.097" y2="0.71958125" layer="94"/>
<rectangle x1="14.94281875" y1="0.635" x2="15.28318125" y2="0.71958125" layer="94"/>
<rectangle x1="15.621" y1="0.635" x2="15.875" y2="0.71958125" layer="94"/>
<rectangle x1="16.72081875" y1="0.635" x2="16.97481875" y2="0.71958125" layer="94"/>
<rectangle x1="17.22881875" y1="0.635" x2="17.56918125" y2="0.71958125" layer="94"/>
<rectangle x1="17.907" y1="0.635" x2="18.33118125" y2="0.71958125" layer="94"/>
<rectangle x1="2.413" y1="0.71958125" x2="2.667" y2="0.80441875" layer="94"/>
<rectangle x1="3.76681875" y1="0.71958125" x2="4.02081875" y2="0.80441875" layer="94"/>
<rectangle x1="4.36118125" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="94"/>
<rectangle x1="5.54481875" y1="0.71958125" x2="5.715" y2="0.80441875" layer="94"/>
<rectangle x1="5.88518125" y1="0.71958125" x2="6.13918125" y2="0.80441875" layer="94"/>
<rectangle x1="6.56081875" y1="0.71958125" x2="6.81481875" y2="0.80441875" layer="94"/>
<rectangle x1="7.493" y1="0.71958125" x2="7.747" y2="0.80441875" layer="94"/>
<rectangle x1="8.67918125" y1="0.71958125" x2="9.017" y2="0.80441875" layer="94"/>
<rectangle x1="9.44118125" y1="0.71958125" x2="9.69518125" y2="0.80441875" layer="94"/>
<rectangle x1="10.37081875" y1="0.71958125" x2="10.62481875" y2="0.80441875" layer="94"/>
<rectangle x1="10.87881875" y1="0.71958125" x2="11.049" y2="0.80441875" layer="94"/>
<rectangle x1="11.557" y1="0.71958125" x2="11.811" y2="0.80441875" layer="94"/>
<rectangle x1="11.98118125" y1="0.71958125" x2="12.23518125" y2="0.80441875" layer="94"/>
<rectangle x1="12.91081875" y1="0.71958125" x2="13.16481875" y2="0.80441875" layer="94"/>
<rectangle x1="13.67281875" y1="0.71958125" x2="14.01318125" y2="0.80441875" layer="94"/>
<rectangle x1="15.02918125" y1="0.71958125" x2="15.367" y2="0.80441875" layer="94"/>
<rectangle x1="15.621" y1="0.71958125" x2="15.875" y2="0.80441875" layer="94"/>
<rectangle x1="16.72081875" y1="0.71958125" x2="16.97481875" y2="0.80441875" layer="94"/>
<rectangle x1="17.145" y1="0.71958125" x2="17.399" y2="0.80441875" layer="94"/>
<rectangle x1="18.07718125" y1="0.71958125" x2="18.33118125" y2="0.80441875" layer="94"/>
<rectangle x1="2.32918125" y1="0.80441875" x2="2.58318125" y2="0.889" layer="94"/>
<rectangle x1="3.85318125" y1="0.80441875" x2="4.10718125" y2="0.889" layer="94"/>
<rectangle x1="4.36118125" y1="0.80441875" x2="4.61518125" y2="0.889" layer="94"/>
<rectangle x1="5.54481875" y1="0.80441875" x2="5.715" y2="0.889" layer="94"/>
<rectangle x1="5.88518125" y1="0.80441875" x2="6.13918125" y2="0.889" layer="94"/>
<rectangle x1="6.477" y1="0.80441875" x2="6.731" y2="0.889" layer="94"/>
<rectangle x1="7.493" y1="0.80441875" x2="7.747" y2="0.889" layer="94"/>
<rectangle x1="8.763" y1="0.80441875" x2="9.10081875" y2="0.889" layer="94"/>
<rectangle x1="9.44118125" y1="0.80441875" x2="9.69518125" y2="0.889" layer="94"/>
<rectangle x1="10.45718125" y1="0.80441875" x2="10.71118125" y2="0.889" layer="94"/>
<rectangle x1="11.557" y1="0.80441875" x2="11.811" y2="0.889" layer="94"/>
<rectangle x1="11.98118125" y1="0.80441875" x2="12.14881875" y2="0.889" layer="94"/>
<rectangle x1="12.99718125" y1="0.80441875" x2="13.16481875" y2="0.889" layer="94"/>
<rectangle x1="13.67281875" y1="0.80441875" x2="13.92681875" y2="0.889" layer="94"/>
<rectangle x1="15.113" y1="0.80441875" x2="15.367" y2="0.889" layer="94"/>
<rectangle x1="15.621" y1="0.80441875" x2="15.875" y2="0.889" layer="94"/>
<rectangle x1="16.72081875" y1="0.80441875" x2="16.97481875" y2="0.889" layer="94"/>
<rectangle x1="17.145" y1="0.80441875" x2="17.399" y2="0.889" layer="94"/>
<rectangle x1="18.161" y1="0.80441875" x2="18.33118125" y2="0.889" layer="94"/>
<rectangle x1="2.32918125" y1="0.889" x2="2.58318125" y2="0.97358125" layer="94"/>
<rectangle x1="3.937" y1="0.889" x2="4.191" y2="0.97358125" layer="94"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.61518125" y2="0.97358125" layer="94"/>
<rectangle x1="5.54481875" y1="0.889" x2="5.715" y2="0.97358125" layer="94"/>
<rectangle x1="5.88518125" y1="0.889" x2="6.13918125" y2="0.97358125" layer="94"/>
<rectangle x1="6.477" y1="0.889" x2="6.731" y2="0.97358125" layer="94"/>
<rectangle x1="7.493" y1="0.889" x2="7.747" y2="0.97358125" layer="94"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.10081875" y2="0.97358125" layer="94"/>
<rectangle x1="9.35481875" y1="0.889" x2="9.60881875" y2="0.97358125" layer="94"/>
<rectangle x1="10.45718125" y1="0.889" x2="10.71118125" y2="0.97358125" layer="94"/>
<rectangle x1="11.47318125" y1="0.889" x2="11.72718125" y2="0.97358125" layer="94"/>
<rectangle x1="11.89481875" y1="0.889" x2="12.14881875" y2="0.97358125" layer="94"/>
<rectangle x1="13.589" y1="0.889" x2="13.843" y2="0.97358125" layer="94"/>
<rectangle x1="15.19681875" y1="0.889" x2="15.45081875" y2="0.97358125" layer="94"/>
<rectangle x1="15.621" y1="0.889" x2="15.875" y2="0.97358125" layer="94"/>
<rectangle x1="16.72081875" y1="0.889" x2="16.97481875" y2="0.97358125" layer="94"/>
<rectangle x1="17.06118125" y1="0.889" x2="17.31518125" y2="0.97358125" layer="94"/>
<rectangle x1="2.32918125" y1="0.97358125" x2="2.49681875" y2="1.05841875" layer="94"/>
<rectangle x1="3.937" y1="0.97358125" x2="4.191" y2="1.05841875" layer="94"/>
<rectangle x1="4.36118125" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="94"/>
<rectangle x1="5.54481875" y1="0.97358125" x2="5.715" y2="1.05841875" layer="94"/>
<rectangle x1="5.88518125" y1="0.97358125" x2="6.13918125" y2="1.05841875" layer="94"/>
<rectangle x1="6.477" y1="0.97358125" x2="6.64718125" y2="1.05841875" layer="94"/>
<rectangle x1="7.493" y1="0.97358125" x2="7.747" y2="1.05841875" layer="94"/>
<rectangle x1="8.93318125" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="94"/>
<rectangle x1="9.35481875" y1="0.97358125" x2="9.60881875" y2="1.05841875" layer="94"/>
<rectangle x1="10.45718125" y1="0.97358125" x2="10.71118125" y2="1.05841875" layer="94"/>
<rectangle x1="11.21918125" y1="0.97358125" x2="11.72718125" y2="1.05841875" layer="94"/>
<rectangle x1="11.89481875" y1="0.97358125" x2="13.25118125" y2="1.05841875" layer="94"/>
<rectangle x1="13.589" y1="0.97358125" x2="13.843" y2="1.05841875" layer="94"/>
<rectangle x1="15.19681875" y1="0.97358125" x2="15.45081875" y2="1.05841875" layer="94"/>
<rectangle x1="15.621" y1="0.97358125" x2="15.875" y2="1.05841875" layer="94"/>
<rectangle x1="16.72081875" y1="0.97358125" x2="16.97481875" y2="1.05841875" layer="94"/>
<rectangle x1="17.06118125" y1="0.97358125" x2="18.415" y2="1.05841875" layer="94"/>
<rectangle x1="2.24281875" y1="1.05841875" x2="2.49681875" y2="1.143" layer="94"/>
<rectangle x1="3.937" y1="1.05841875" x2="4.191" y2="1.143" layer="94"/>
<rectangle x1="4.36118125" y1="1.05841875" x2="4.61518125" y2="1.143" layer="94"/>
<rectangle x1="5.54481875" y1="1.05841875" x2="5.715" y2="1.143" layer="94"/>
<rectangle x1="5.88518125" y1="1.05841875" x2="6.13918125" y2="1.143" layer="94"/>
<rectangle x1="6.477" y1="1.05841875" x2="6.64718125" y2="1.143" layer="94"/>
<rectangle x1="7.493" y1="1.05841875" x2="7.747" y2="1.143" layer="94"/>
<rectangle x1="8.93318125" y1="1.05841875" x2="9.18718125" y2="1.143" layer="94"/>
<rectangle x1="9.35481875" y1="1.05841875" x2="9.60881875" y2="1.143" layer="94"/>
<rectangle x1="10.45718125" y1="1.05841875" x2="10.71118125" y2="1.143" layer="94"/>
<rectangle x1="11.049" y1="1.05841875" x2="11.557" y2="1.143" layer="94"/>
<rectangle x1="11.89481875" y1="1.05841875" x2="13.25118125" y2="1.143" layer="94"/>
<rectangle x1="13.589" y1="1.05841875" x2="13.75918125" y2="1.143" layer="94"/>
<rectangle x1="15.28318125" y1="1.05841875" x2="15.53718125" y2="1.143" layer="94"/>
<rectangle x1="15.621" y1="1.05841875" x2="15.875" y2="1.143" layer="94"/>
<rectangle x1="16.72081875" y1="1.05841875" x2="16.97481875" y2="1.143" layer="94"/>
<rectangle x1="17.06118125" y1="1.05841875" x2="18.415" y2="1.143" layer="94"/>
<rectangle x1="2.24281875" y1="1.143" x2="2.49681875" y2="1.22758125" layer="94"/>
<rectangle x1="4.02081875" y1="1.143" x2="4.191" y2="1.22758125" layer="94"/>
<rectangle x1="4.36118125" y1="1.143" x2="4.61518125" y2="1.22758125" layer="94"/>
<rectangle x1="5.54481875" y1="1.143" x2="5.715" y2="1.22758125" layer="94"/>
<rectangle x1="5.88518125" y1="1.143" x2="6.13918125" y2="1.22758125" layer="94"/>
<rectangle x1="6.477" y1="1.143" x2="6.64718125" y2="1.22758125" layer="94"/>
<rectangle x1="7.493" y1="1.143" x2="7.747" y2="1.22758125" layer="94"/>
<rectangle x1="9.017" y1="1.143" x2="9.271" y2="1.22758125" layer="94"/>
<rectangle x1="9.35481875" y1="1.143" x2="9.60881875" y2="1.22758125" layer="94"/>
<rectangle x1="10.45718125" y1="1.143" x2="10.71118125" y2="1.22758125" layer="94"/>
<rectangle x1="10.87881875" y1="1.143" x2="11.38681875" y2="1.22758125" layer="94"/>
<rectangle x1="11.89481875" y1="1.143" x2="13.25118125" y2="1.22758125" layer="94"/>
<rectangle x1="13.50518125" y1="1.143" x2="13.75918125" y2="1.22758125" layer="94"/>
<rectangle x1="15.28318125" y1="1.143" x2="15.53718125" y2="1.22758125" layer="94"/>
<rectangle x1="15.621" y1="1.143" x2="15.875" y2="1.22758125" layer="94"/>
<rectangle x1="16.72081875" y1="1.143" x2="16.97481875" y2="1.22758125" layer="94"/>
<rectangle x1="17.06118125" y1="1.143" x2="18.415" y2="1.22758125" layer="94"/>
<rectangle x1="2.24281875" y1="1.22758125" x2="2.49681875" y2="1.31241875" layer="94"/>
<rectangle x1="4.02081875" y1="1.22758125" x2="4.191" y2="1.31241875" layer="94"/>
<rectangle x1="4.36118125" y1="1.22758125" x2="4.61518125" y2="1.31241875" layer="94"/>
<rectangle x1="5.461" y1="1.22758125" x2="5.715" y2="1.31241875" layer="94"/>
<rectangle x1="5.88518125" y1="1.22758125" x2="6.13918125" y2="1.31241875" layer="94"/>
<rectangle x1="6.477" y1="1.22758125" x2="6.64718125" y2="1.31241875" layer="94"/>
<rectangle x1="7.493" y1="1.22758125" x2="7.747" y2="1.31241875" layer="94"/>
<rectangle x1="9.017" y1="1.22758125" x2="9.271" y2="1.31241875" layer="94"/>
<rectangle x1="9.35481875" y1="1.22758125" x2="9.60881875" y2="1.31241875" layer="94"/>
<rectangle x1="10.45718125" y1="1.22758125" x2="10.71118125" y2="1.31241875" layer="94"/>
<rectangle x1="10.795" y1="1.22758125" x2="11.21918125" y2="1.31241875" layer="94"/>
<rectangle x1="11.89481875" y1="1.22758125" x2="12.14881875" y2="1.31241875" layer="94"/>
<rectangle x1="12.99718125" y1="1.22758125" x2="13.25118125" y2="1.31241875" layer="94"/>
<rectangle x1="13.50518125" y1="1.22758125" x2="13.75918125" y2="1.31241875" layer="94"/>
<rectangle x1="15.28318125" y1="1.22758125" x2="15.53718125" y2="1.31241875" layer="94"/>
<rectangle x1="15.621" y1="1.22758125" x2="15.875" y2="1.31241875" layer="94"/>
<rectangle x1="16.72081875" y1="1.22758125" x2="16.97481875" y2="1.31241875" layer="94"/>
<rectangle x1="17.06118125" y1="1.22758125" x2="17.31518125" y2="1.31241875" layer="94"/>
<rectangle x1="18.161" y1="1.22758125" x2="18.415" y2="1.31241875" layer="94"/>
<rectangle x1="2.24281875" y1="1.31241875" x2="2.49681875" y2="1.397" layer="94"/>
<rectangle x1="4.02081875" y1="1.31241875" x2="4.191" y2="1.397" layer="94"/>
<rectangle x1="4.36118125" y1="1.31241875" x2="4.699" y2="1.397" layer="94"/>
<rectangle x1="5.461" y1="1.31241875" x2="5.715" y2="1.397" layer="94"/>
<rectangle x1="5.88518125" y1="1.31241875" x2="6.13918125" y2="1.397" layer="94"/>
<rectangle x1="6.477" y1="1.31241875" x2="6.64718125" y2="1.397" layer="94"/>
<rectangle x1="7.493" y1="1.31241875" x2="7.747" y2="1.397" layer="94"/>
<rectangle x1="9.017" y1="1.31241875" x2="9.271" y2="1.397" layer="94"/>
<rectangle x1="9.44118125" y1="1.31241875" x2="9.69518125" y2="1.397" layer="94"/>
<rectangle x1="10.45718125" y1="1.31241875" x2="10.62481875" y2="1.397" layer="94"/>
<rectangle x1="10.795" y1="1.31241875" x2="11.049" y2="1.397" layer="94"/>
<rectangle x1="11.64081875" y1="1.31241875" x2="11.72718125" y2="1.397" layer="94"/>
<rectangle x1="11.98118125" y1="1.31241875" x2="12.23518125" y2="1.397" layer="94"/>
<rectangle x1="12.99718125" y1="1.31241875" x2="13.16481875" y2="1.397" layer="94"/>
<rectangle x1="13.50518125" y1="1.31241875" x2="13.75918125" y2="1.397" layer="94"/>
<rectangle x1="15.28318125" y1="1.31241875" x2="15.53718125" y2="1.397" layer="94"/>
<rectangle x1="15.621" y1="1.31241875" x2="15.875" y2="1.397" layer="94"/>
<rectangle x1="16.637" y1="1.31241875" x2="16.891" y2="1.397" layer="94"/>
<rectangle x1="17.145" y1="1.31241875" x2="17.399" y2="1.397" layer="94"/>
<rectangle x1="18.161" y1="1.31241875" x2="18.415" y2="1.397" layer="94"/>
<rectangle x1="2.24281875" y1="1.397" x2="2.49681875" y2="1.48158125" layer="94"/>
<rectangle x1="4.02081875" y1="1.397" x2="4.191" y2="1.48158125" layer="94"/>
<rectangle x1="4.36118125" y1="1.397" x2="4.78281875" y2="1.48158125" layer="94"/>
<rectangle x1="5.37718125" y1="1.397" x2="5.63118125" y2="1.48158125" layer="94"/>
<rectangle x1="5.88518125" y1="1.397" x2="6.13918125" y2="1.48158125" layer="94"/>
<rectangle x1="6.477" y1="1.397" x2="6.64718125" y2="1.48158125" layer="94"/>
<rectangle x1="7.493" y1="1.397" x2="7.747" y2="1.48158125" layer="94"/>
<rectangle x1="9.017" y1="1.397" x2="9.271" y2="1.48158125" layer="94"/>
<rectangle x1="9.44118125" y1="1.397" x2="9.69518125" y2="1.48158125" layer="94"/>
<rectangle x1="10.37081875" y1="1.397" x2="10.62481875" y2="1.48158125" layer="94"/>
<rectangle x1="10.795" y1="1.397" x2="11.049" y2="1.48158125" layer="94"/>
<rectangle x1="11.557" y1="1.397" x2="11.811" y2="1.48158125" layer="94"/>
<rectangle x1="11.98118125" y1="1.397" x2="12.319" y2="1.48158125" layer="94"/>
<rectangle x1="12.91081875" y1="1.397" x2="13.16481875" y2="1.48158125" layer="94"/>
<rectangle x1="13.50518125" y1="1.397" x2="13.75918125" y2="1.48158125" layer="94"/>
<rectangle x1="15.28318125" y1="1.397" x2="15.53718125" y2="1.48158125" layer="94"/>
<rectangle x1="15.621" y1="1.397" x2="15.95881875" y2="1.48158125" layer="94"/>
<rectangle x1="16.637" y1="1.397" x2="16.891" y2="1.48158125" layer="94"/>
<rectangle x1="17.145" y1="1.397" x2="17.48281875" y2="1.48158125" layer="94"/>
<rectangle x1="18.07718125" y1="1.397" x2="18.33118125" y2="1.48158125" layer="94"/>
<rectangle x1="2.24281875" y1="1.48158125" x2="2.49681875" y2="1.56641875" layer="94"/>
<rectangle x1="4.02081875" y1="1.48158125" x2="4.191" y2="1.56641875" layer="94"/>
<rectangle x1="4.36118125" y1="1.48158125" x2="4.953" y2="1.56641875" layer="94"/>
<rectangle x1="5.207" y1="1.48158125" x2="5.63118125" y2="1.56641875" layer="94"/>
<rectangle x1="5.88518125" y1="1.48158125" x2="6.13918125" y2="1.56641875" layer="94"/>
<rectangle x1="6.477" y1="1.48158125" x2="6.64718125" y2="1.56641875" layer="94"/>
<rectangle x1="7.493" y1="1.48158125" x2="7.747" y2="1.56641875" layer="94"/>
<rectangle x1="9.017" y1="1.48158125" x2="9.271" y2="1.56641875" layer="94"/>
<rectangle x1="9.525" y1="1.48158125" x2="9.86281875" y2="1.56641875" layer="94"/>
<rectangle x1="10.20318125" y1="1.48158125" x2="10.541" y2="1.56641875" layer="94"/>
<rectangle x1="10.87881875" y1="1.48158125" x2="11.21918125" y2="1.56641875" layer="94"/>
<rectangle x1="11.47318125" y1="1.48158125" x2="11.72718125" y2="1.56641875" layer="94"/>
<rectangle x1="12.065" y1="1.48158125" x2="12.40281875" y2="1.56641875" layer="94"/>
<rectangle x1="12.74318125" y1="1.48158125" x2="13.081" y2="1.56641875" layer="94"/>
<rectangle x1="13.589" y1="1.48158125" x2="13.75918125" y2="1.56641875" layer="94"/>
<rectangle x1="15.28318125" y1="1.48158125" x2="15.53718125" y2="1.56641875" layer="94"/>
<rectangle x1="15.621" y1="1.48158125" x2="16.129" y2="1.56641875" layer="94"/>
<rectangle x1="16.46681875" y1="1.48158125" x2="16.80718125" y2="1.56641875" layer="94"/>
<rectangle x1="17.22881875" y1="1.48158125" x2="17.653" y2="1.56641875" layer="94"/>
<rectangle x1="17.907" y1="1.48158125" x2="18.24481875" y2="1.56641875" layer="94"/>
<rectangle x1="2.24281875" y1="1.56641875" x2="2.49681875" y2="1.651" layer="94"/>
<rectangle x1="4.02081875" y1="1.56641875" x2="4.191" y2="1.651" layer="94"/>
<rectangle x1="4.36118125" y1="1.56641875" x2="5.54481875" y2="1.651" layer="94"/>
<rectangle x1="5.88518125" y1="1.56641875" x2="6.13918125" y2="1.651" layer="94"/>
<rectangle x1="6.223" y1="1.56641875" x2="7.239" y2="1.651" layer="94"/>
<rectangle x1="7.493" y1="1.56641875" x2="7.747" y2="1.651" layer="94"/>
<rectangle x1="9.017" y1="1.56641875" x2="9.271" y2="1.651" layer="94"/>
<rectangle x1="9.60881875" y1="1.56641875" x2="10.45718125" y2="1.651" layer="94"/>
<rectangle x1="10.87881875" y1="1.56641875" x2="11.72718125" y2="1.651" layer="94"/>
<rectangle x1="12.14881875" y1="1.56641875" x2="12.99718125" y2="1.651" layer="94"/>
<rectangle x1="13.589" y1="1.56641875" x2="13.843" y2="1.651" layer="94"/>
<rectangle x1="15.19681875" y1="1.56641875" x2="15.45081875" y2="1.651" layer="94"/>
<rectangle x1="15.621" y1="1.56641875" x2="16.72081875" y2="1.651" layer="94"/>
<rectangle x1="17.31518125" y1="1.56641875" x2="18.161" y2="1.651" layer="94"/>
<rectangle x1="2.24281875" y1="1.651" x2="2.49681875" y2="1.73558125" layer="94"/>
<rectangle x1="4.02081875" y1="1.651" x2="4.191" y2="1.73558125" layer="94"/>
<rectangle x1="4.36118125" y1="1.651" x2="4.61518125" y2="1.73558125" layer="94"/>
<rectangle x1="4.78281875" y1="1.651" x2="5.37718125" y2="1.73558125" layer="94"/>
<rectangle x1="5.88518125" y1="1.651" x2="6.13918125" y2="1.73558125" layer="94"/>
<rectangle x1="6.223" y1="1.651" x2="7.239" y2="1.73558125" layer="94"/>
<rectangle x1="7.493" y1="1.651" x2="7.747" y2="1.73558125" layer="94"/>
<rectangle x1="9.017" y1="1.651" x2="9.18718125" y2="1.73558125" layer="94"/>
<rectangle x1="9.69518125" y1="1.651" x2="10.37081875" y2="1.73558125" layer="94"/>
<rectangle x1="11.049" y1="1.651" x2="11.557" y2="1.73558125" layer="94"/>
<rectangle x1="12.23518125" y1="1.651" x2="12.91081875" y2="1.73558125" layer="94"/>
<rectangle x1="13.589" y1="1.651" x2="13.843" y2="1.73558125" layer="94"/>
<rectangle x1="15.19681875" y1="1.651" x2="15.45081875" y2="1.73558125" layer="94"/>
<rectangle x1="15.621" y1="1.651" x2="15.79118125" y2="1.73558125" layer="94"/>
<rectangle x1="15.95881875" y1="1.651" x2="16.637" y2="1.73558125" layer="94"/>
<rectangle x1="17.399" y1="1.651" x2="18.07718125" y2="1.73558125" layer="94"/>
<rectangle x1="2.24281875" y1="1.73558125" x2="2.49681875" y2="1.82041875" layer="94"/>
<rectangle x1="4.02081875" y1="1.73558125" x2="4.191" y2="1.82041875" layer="94"/>
<rectangle x1="4.445" y1="1.73558125" x2="4.52881875" y2="1.82041875" layer="94"/>
<rectangle x1="4.953" y1="1.73558125" x2="5.12318125" y2="1.82041875" layer="94"/>
<rectangle x1="5.969" y1="1.73558125" x2="6.05281875" y2="1.82041875" layer="94"/>
<rectangle x1="6.30681875" y1="1.73558125" x2="7.15518125" y2="1.82041875" layer="94"/>
<rectangle x1="7.493" y1="1.73558125" x2="7.747" y2="1.82041875" layer="94"/>
<rectangle x1="8.93318125" y1="1.73558125" x2="9.18718125" y2="1.82041875" layer="94"/>
<rectangle x1="9.94918125" y1="1.73558125" x2="10.11681875" y2="1.82041875" layer="94"/>
<rectangle x1="11.21918125" y1="1.73558125" x2="11.38681875" y2="1.82041875" layer="94"/>
<rectangle x1="12.48918125" y1="1.73558125" x2="12.65681875" y2="1.82041875" layer="94"/>
<rectangle x1="13.67281875" y1="1.73558125" x2="13.92681875" y2="1.82041875" layer="94"/>
<rectangle x1="15.113" y1="1.73558125" x2="15.367" y2="1.82041875" layer="94"/>
<rectangle x1="16.21281875" y1="1.73558125" x2="16.383" y2="1.82041875" layer="94"/>
<rectangle x1="17.653" y1="1.73558125" x2="17.82318125" y2="1.82041875" layer="94"/>
<rectangle x1="2.24281875" y1="1.82041875" x2="2.49681875" y2="1.905" layer="94"/>
<rectangle x1="4.02081875" y1="1.82041875" x2="4.191" y2="1.905" layer="94"/>
<rectangle x1="5.88518125" y1="1.82041875" x2="6.13918125" y2="1.905" layer="94"/>
<rectangle x1="6.477" y1="1.82041875" x2="6.64718125" y2="1.905" layer="94"/>
<rectangle x1="7.493" y1="1.82041875" x2="7.747" y2="1.905" layer="94"/>
<rectangle x1="8.84681875" y1="1.82041875" x2="9.18718125" y2="1.905" layer="94"/>
<rectangle x1="13.67281875" y1="1.82041875" x2="14.01318125" y2="1.905" layer="94"/>
<rectangle x1="15.02918125" y1="1.82041875" x2="15.367" y2="1.905" layer="94"/>
<rectangle x1="2.24281875" y1="1.905" x2="2.49681875" y2="1.98958125" layer="94"/>
<rectangle x1="4.02081875" y1="1.905" x2="4.191" y2="1.98958125" layer="94"/>
<rectangle x1="5.88518125" y1="1.905" x2="6.13918125" y2="1.98958125" layer="94"/>
<rectangle x1="6.477" y1="1.905" x2="6.64718125" y2="1.98958125" layer="94"/>
<rectangle x1="7.493" y1="1.905" x2="7.747" y2="1.98958125" layer="94"/>
<rectangle x1="8.84681875" y1="1.905" x2="9.10081875" y2="1.98958125" layer="94"/>
<rectangle x1="13.75918125" y1="1.905" x2="14.097" y2="1.98958125" layer="94"/>
<rectangle x1="14.94281875" y1="1.905" x2="15.28318125" y2="1.98958125" layer="94"/>
<rectangle x1="2.24281875" y1="1.98958125" x2="2.49681875" y2="2.07441875" layer="94"/>
<rectangle x1="4.02081875" y1="1.98958125" x2="4.27481875" y2="2.07441875" layer="94"/>
<rectangle x1="5.88518125" y1="1.98958125" x2="6.05281875" y2="2.07441875" layer="94"/>
<rectangle x1="6.477" y1="1.98958125" x2="6.64718125" y2="2.07441875" layer="94"/>
<rectangle x1="7.493" y1="1.98958125" x2="7.747" y2="2.07441875" layer="94"/>
<rectangle x1="8.67918125" y1="1.98958125" x2="9.017" y2="2.07441875" layer="94"/>
<rectangle x1="13.843" y1="1.98958125" x2="14.26718125" y2="2.07441875" layer="94"/>
<rectangle x1="14.77518125" y1="1.98958125" x2="15.19681875" y2="2.07441875" layer="94"/>
<rectangle x1="2.24281875" y1="2.07441875" x2="2.49681875" y2="2.159" layer="94"/>
<rectangle x1="4.02081875" y1="2.07441875" x2="4.27481875" y2="2.159" layer="94"/>
<rectangle x1="6.477" y1="2.07441875" x2="6.64718125" y2="2.159" layer="94"/>
<rectangle x1="7.493" y1="2.07441875" x2="7.747" y2="2.159" layer="94"/>
<rectangle x1="8.59281875" y1="2.07441875" x2="8.93318125" y2="2.159" layer="94"/>
<rectangle x1="13.92681875" y1="2.07441875" x2="15.113" y2="2.159" layer="94"/>
<rectangle x1="2.24281875" y1="2.159" x2="2.49681875" y2="2.24358125" layer="94"/>
<rectangle x1="4.02081875" y1="2.159" x2="4.191" y2="2.24358125" layer="94"/>
<rectangle x1="6.477" y1="2.159" x2="6.64718125" y2="2.24358125" layer="94"/>
<rectangle x1="7.493" y1="2.159" x2="8.84681875" y2="2.24358125" layer="94"/>
<rectangle x1="14.097" y1="2.159" x2="14.94281875" y2="2.24358125" layer="94"/>
<rectangle x1="2.24281875" y1="2.24358125" x2="2.413" y2="2.32841875" layer="94"/>
<rectangle x1="4.02081875" y1="2.24358125" x2="4.191" y2="2.32841875" layer="94"/>
<rectangle x1="6.477" y1="2.24358125" x2="6.64718125" y2="2.32841875" layer="94"/>
<rectangle x1="7.493" y1="2.24358125" x2="8.763" y2="2.32841875" layer="94"/>
<rectangle x1="14.26718125" y1="2.24358125" x2="14.77518125" y2="2.32841875" layer="94"/>
<rectangle x1="7.493" y1="2.32841875" x2="8.509" y2="2.413" layer="94"/>
<rectangle x1="3.34518125" y1="3.25958125" x2="16.80718125" y2="3.34441875" layer="94"/>
<rectangle x1="3.00481875" y1="3.34441875" x2="17.145" y2="3.429" layer="94"/>
<rectangle x1="2.667" y1="3.429" x2="17.399" y2="3.51358125" layer="94"/>
<rectangle x1="2.49681875" y1="3.51358125" x2="17.56918125" y2="3.59841875" layer="94"/>
<rectangle x1="2.32918125" y1="3.59841875" x2="17.82318125" y2="3.683" layer="94"/>
<rectangle x1="2.159" y1="3.683" x2="17.99081875" y2="3.76758125" layer="94"/>
<rectangle x1="1.98881875" y1="3.76758125" x2="18.07718125" y2="3.85241875" layer="94"/>
<rectangle x1="1.905" y1="3.85241875" x2="3.51281875" y2="3.937" layer="94"/>
<rectangle x1="9.779" y1="3.85241875" x2="18.24481875" y2="3.937" layer="94"/>
<rectangle x1="1.73481875" y1="3.937" x2="3.175" y2="4.02158125" layer="94"/>
<rectangle x1="9.779" y1="3.937" x2="18.33118125" y2="4.02158125" layer="94"/>
<rectangle x1="1.651" y1="4.02158125" x2="2.83718125" y2="4.10641875" layer="94"/>
<rectangle x1="9.69518125" y1="4.02158125" x2="18.49881875" y2="4.10641875" layer="94"/>
<rectangle x1="1.56718125" y1="4.10641875" x2="2.667" y2="4.191" layer="94"/>
<rectangle x1="9.60881875" y1="4.10641875" x2="18.58518125" y2="4.191" layer="94"/>
<rectangle x1="1.397" y1="4.191" x2="2.49681875" y2="4.27558125" layer="94"/>
<rectangle x1="9.60881875" y1="4.191" x2="18.669" y2="4.27558125" layer="94"/>
<rectangle x1="1.31318125" y1="4.27558125" x2="2.32918125" y2="4.36041875" layer="94"/>
<rectangle x1="9.525" y1="4.27558125" x2="18.75281875" y2="4.36041875" layer="94"/>
<rectangle x1="1.22681875" y1="4.36041875" x2="2.159" y2="4.445" layer="94"/>
<rectangle x1="9.525" y1="4.36041875" x2="18.83918125" y2="4.445" layer="94"/>
<rectangle x1="1.143" y1="4.445" x2="2.07518125" y2="4.52958125" layer="94"/>
<rectangle x1="9.44118125" y1="4.445" x2="18.923" y2="4.52958125" layer="94"/>
<rectangle x1="1.05918125" y1="4.52958125" x2="1.98881875" y2="4.61441875" layer="94"/>
<rectangle x1="9.44118125" y1="4.52958125" x2="19.00681875" y2="4.61441875" layer="94"/>
<rectangle x1="0.97281875" y1="4.61441875" x2="1.905" y2="4.699" layer="94"/>
<rectangle x1="9.44118125" y1="4.61441875" x2="19.09318125" y2="4.699" layer="94"/>
<rectangle x1="0.97281875" y1="4.699" x2="1.73481875" y2="4.78358125" layer="94"/>
<rectangle x1="9.35481875" y1="4.699" x2="19.177" y2="4.78358125" layer="94"/>
<rectangle x1="0.889" y1="4.78358125" x2="1.651" y2="4.86841875" layer="94"/>
<rectangle x1="9.35481875" y1="4.78358125" x2="19.26081875" y2="4.86841875" layer="94"/>
<rectangle x1="0.80518125" y1="4.86841875" x2="1.56718125" y2="4.953" layer="94"/>
<rectangle x1="9.35481875" y1="4.86841875" x2="19.26081875" y2="4.953" layer="94"/>
<rectangle x1="0.71881875" y1="4.953" x2="1.48081875" y2="5.03758125" layer="94"/>
<rectangle x1="9.271" y1="4.953" x2="19.34718125" y2="5.03758125" layer="94"/>
<rectangle x1="0.71881875" y1="5.03758125" x2="1.48081875" y2="5.12241875" layer="94"/>
<rectangle x1="9.271" y1="5.03758125" x2="19.431" y2="5.12241875" layer="94"/>
<rectangle x1="0.635" y1="5.12241875" x2="1.397" y2="5.207" layer="94"/>
<rectangle x1="9.271" y1="5.12241875" x2="19.431" y2="5.207" layer="94"/>
<rectangle x1="0.55118125" y1="5.207" x2="1.31318125" y2="5.29158125" layer="94"/>
<rectangle x1="9.271" y1="5.207" x2="19.51481875" y2="5.29158125" layer="94"/>
<rectangle x1="0.55118125" y1="5.29158125" x2="1.22681875" y2="5.37641875" layer="94"/>
<rectangle x1="9.18718125" y1="5.29158125" x2="19.51481875" y2="5.37641875" layer="94"/>
<rectangle x1="0.46481875" y1="5.37641875" x2="1.22681875" y2="5.461" layer="94"/>
<rectangle x1="9.18718125" y1="5.37641875" x2="19.60118125" y2="5.461" layer="94"/>
<rectangle x1="0.46481875" y1="5.461" x2="1.143" y2="5.54558125" layer="94"/>
<rectangle x1="9.18718125" y1="5.461" x2="19.685" y2="5.54558125" layer="94"/>
<rectangle x1="0.381" y1="5.54558125" x2="1.05918125" y2="5.63041875" layer="94"/>
<rectangle x1="3.85318125" y1="5.54558125" x2="4.27481875" y2="5.63041875" layer="94"/>
<rectangle x1="5.88518125" y1="5.54558125" x2="6.81481875" y2="5.63041875" layer="94"/>
<rectangle x1="9.18718125" y1="5.54558125" x2="19.685" y2="5.63041875" layer="94"/>
<rectangle x1="0.381" y1="5.63041875" x2="1.05918125" y2="5.715" layer="94"/>
<rectangle x1="3.51281875" y1="5.63041875" x2="4.61518125" y2="5.715" layer="94"/>
<rectangle x1="5.79881875" y1="5.63041875" x2="7.239" y2="5.715" layer="94"/>
<rectangle x1="9.10081875" y1="5.63041875" x2="10.96518125" y2="5.715" layer="94"/>
<rectangle x1="11.64081875" y1="5.63041875" x2="19.685" y2="5.715" layer="94"/>
<rectangle x1="0.381" y1="5.715" x2="0.97281875" y2="5.79958125" layer="94"/>
<rectangle x1="3.34518125" y1="5.715" x2="4.78281875" y2="5.79958125" layer="94"/>
<rectangle x1="5.715" y1="5.715" x2="7.40918125" y2="5.79958125" layer="94"/>
<rectangle x1="9.10081875" y1="5.715" x2="10.71118125" y2="5.79958125" layer="94"/>
<rectangle x1="11.89481875" y1="5.715" x2="13.335" y2="5.79958125" layer="94"/>
<rectangle x1="13.67281875" y1="5.715" x2="15.113" y2="5.79958125" layer="94"/>
<rectangle x1="15.367" y1="5.715" x2="16.383" y2="5.79958125" layer="94"/>
<rectangle x1="17.22881875" y1="5.715" x2="19.76881875" y2="5.79958125" layer="94"/>
<rectangle x1="0.29718125" y1="5.79958125" x2="0.97281875" y2="5.88441875" layer="94"/>
<rectangle x1="3.175" y1="5.79958125" x2="4.953" y2="5.88441875" layer="94"/>
<rectangle x1="5.715" y1="5.79958125" x2="7.57681875" y2="5.88441875" layer="94"/>
<rectangle x1="9.10081875" y1="5.79958125" x2="10.541" y2="5.88441875" layer="94"/>
<rectangle x1="12.065" y1="5.79958125" x2="13.335" y2="5.88441875" layer="94"/>
<rectangle x1="13.67281875" y1="5.79958125" x2="15.113" y2="5.88441875" layer="94"/>
<rectangle x1="15.45081875" y1="5.79958125" x2="16.21281875" y2="5.88441875" layer="94"/>
<rectangle x1="17.48281875" y1="5.79958125" x2="19.76881875" y2="5.88441875" layer="94"/>
<rectangle x1="0.29718125" y1="5.88441875" x2="0.889" y2="5.969" layer="94"/>
<rectangle x1="3.09118125" y1="5.88441875" x2="5.12318125" y2="5.969" layer="94"/>
<rectangle x1="5.715" y1="5.88441875" x2="7.66318125" y2="5.969" layer="94"/>
<rectangle x1="9.10081875" y1="5.88441875" x2="10.37081875" y2="5.969" layer="94"/>
<rectangle x1="12.23518125" y1="5.88441875" x2="13.335" y2="5.969" layer="94"/>
<rectangle x1="13.67281875" y1="5.88441875" x2="15.113" y2="5.969" layer="94"/>
<rectangle x1="15.45081875" y1="5.88441875" x2="16.129" y2="5.969" layer="94"/>
<rectangle x1="17.56918125" y1="5.88441875" x2="19.85518125" y2="5.969" layer="94"/>
<rectangle x1="0.21081875" y1="5.969" x2="0.889" y2="6.05358125" layer="94"/>
<rectangle x1="2.921" y1="5.969" x2="5.207" y2="6.05358125" layer="94"/>
<rectangle x1="5.715" y1="5.969" x2="7.747" y2="6.05358125" layer="94"/>
<rectangle x1="9.10081875" y1="5.969" x2="10.287" y2="6.05358125" layer="94"/>
<rectangle x1="12.319" y1="5.969" x2="13.335" y2="6.05358125" layer="94"/>
<rectangle x1="13.67281875" y1="5.969" x2="15.113" y2="6.05358125" layer="94"/>
<rectangle x1="15.45081875" y1="5.969" x2="16.04518125" y2="6.05358125" layer="94"/>
<rectangle x1="17.653" y1="5.969" x2="19.85518125" y2="6.05358125" layer="94"/>
<rectangle x1="0.21081875" y1="6.05358125" x2="0.889" y2="6.13841875" layer="94"/>
<rectangle x1="2.83718125" y1="6.05358125" x2="5.29081875" y2="6.13841875" layer="94"/>
<rectangle x1="5.79881875" y1="6.05358125" x2="7.83081875" y2="6.13841875" layer="94"/>
<rectangle x1="9.10081875" y1="6.05358125" x2="10.11681875" y2="6.13841875" layer="94"/>
<rectangle x1="12.40281875" y1="6.05358125" x2="13.335" y2="6.13841875" layer="94"/>
<rectangle x1="13.67281875" y1="6.05358125" x2="15.113" y2="6.13841875" layer="94"/>
<rectangle x1="15.45081875" y1="6.05358125" x2="15.95881875" y2="6.13841875" layer="94"/>
<rectangle x1="16.55318125" y1="6.05358125" x2="17.06118125" y2="6.13841875" layer="94"/>
<rectangle x1="17.73681875" y1="6.05358125" x2="19.85518125" y2="6.13841875" layer="94"/>
<rectangle x1="0.21081875" y1="6.13841875" x2="0.80518125" y2="6.223" layer="94"/>
<rectangle x1="2.75081875" y1="6.13841875" x2="5.37718125" y2="6.223" layer="94"/>
<rectangle x1="5.88518125" y1="6.13841875" x2="7.91718125" y2="6.223" layer="94"/>
<rectangle x1="9.017" y1="6.13841875" x2="10.11681875" y2="6.223" layer="94"/>
<rectangle x1="10.96518125" y1="6.13841875" x2="11.64081875" y2="6.223" layer="94"/>
<rectangle x1="12.48918125" y1="6.13841875" x2="13.335" y2="6.223" layer="94"/>
<rectangle x1="13.67281875" y1="6.13841875" x2="15.02918125" y2="6.223" layer="94"/>
<rectangle x1="15.45081875" y1="6.13841875" x2="15.875" y2="6.223" layer="94"/>
<rectangle x1="16.46681875" y1="6.13841875" x2="17.22881875" y2="6.223" layer="94"/>
<rectangle x1="17.73681875" y1="6.13841875" x2="19.939" y2="6.223" layer="94"/>
<rectangle x1="0.21081875" y1="6.223" x2="0.80518125" y2="6.30758125" layer="94"/>
<rectangle x1="2.75081875" y1="6.223" x2="3.683" y2="6.30758125" layer="94"/>
<rectangle x1="4.445" y1="6.223" x2="5.37718125" y2="6.30758125" layer="94"/>
<rectangle x1="7.06881875" y1="6.223" x2="8.001" y2="6.30758125" layer="94"/>
<rectangle x1="9.017" y1="6.223" x2="10.033" y2="6.30758125" layer="94"/>
<rectangle x1="10.795" y1="6.223" x2="11.811" y2="6.30758125" layer="94"/>
<rectangle x1="12.573" y1="6.223" x2="13.335" y2="6.30758125" layer="94"/>
<rectangle x1="13.67281875" y1="6.223" x2="15.02918125" y2="6.30758125" layer="94"/>
<rectangle x1="15.45081875" y1="6.223" x2="15.875" y2="6.30758125" layer="94"/>
<rectangle x1="16.29918125" y1="6.223" x2="17.399" y2="6.30758125" layer="94"/>
<rectangle x1="17.73681875" y1="6.223" x2="19.939" y2="6.30758125" layer="94"/>
<rectangle x1="0.127" y1="6.30758125" x2="0.80518125" y2="6.39241875" layer="94"/>
<rectangle x1="2.667" y1="6.30758125" x2="3.51281875" y2="6.39241875" layer="94"/>
<rectangle x1="4.61518125" y1="6.30758125" x2="5.461" y2="6.39241875" layer="94"/>
<rectangle x1="7.239" y1="6.30758125" x2="8.08481875" y2="6.39241875" layer="94"/>
<rectangle x1="9.017" y1="6.30758125" x2="9.94918125" y2="6.39241875" layer="94"/>
<rectangle x1="10.62481875" y1="6.30758125" x2="11.98118125" y2="6.39241875" layer="94"/>
<rectangle x1="12.65681875" y1="6.30758125" x2="13.335" y2="6.39241875" layer="94"/>
<rectangle x1="13.67281875" y1="6.30758125" x2="15.113" y2="6.39241875" layer="94"/>
<rectangle x1="15.45081875" y1="6.30758125" x2="15.79118125" y2="6.39241875" layer="94"/>
<rectangle x1="16.21281875" y1="6.30758125" x2="17.48281875" y2="6.39241875" layer="94"/>
<rectangle x1="17.653" y1="6.30758125" x2="19.939" y2="6.39241875" layer="94"/>
<rectangle x1="0.127" y1="6.39241875" x2="0.71881875" y2="6.477" layer="94"/>
<rectangle x1="2.58318125" y1="6.39241875" x2="3.34518125" y2="6.477" layer="94"/>
<rectangle x1="4.78281875" y1="6.39241875" x2="5.54481875" y2="6.477" layer="94"/>
<rectangle x1="7.32281875" y1="6.39241875" x2="8.17118125" y2="6.477" layer="94"/>
<rectangle x1="9.017" y1="6.39241875" x2="9.86281875" y2="6.477" layer="94"/>
<rectangle x1="10.541" y1="6.39241875" x2="12.065" y2="6.477" layer="94"/>
<rectangle x1="12.65681875" y1="6.39241875" x2="13.335" y2="6.477" layer="94"/>
<rectangle x1="13.67281875" y1="6.39241875" x2="15.113" y2="6.477" layer="94"/>
<rectangle x1="15.45081875" y1="6.39241875" x2="15.79118125" y2="6.477" layer="94"/>
<rectangle x1="16.21281875" y1="6.39241875" x2="19.939" y2="6.477" layer="94"/>
<rectangle x1="0.127" y1="6.477" x2="0.71881875" y2="6.56158125" layer="94"/>
<rectangle x1="2.58318125" y1="6.477" x2="3.25881875" y2="6.56158125" layer="94"/>
<rectangle x1="4.86918125" y1="6.477" x2="5.54481875" y2="6.56158125" layer="94"/>
<rectangle x1="7.493" y1="6.477" x2="8.17118125" y2="6.56158125" layer="94"/>
<rectangle x1="9.017" y1="6.477" x2="9.86281875" y2="6.56158125" layer="94"/>
<rectangle x1="10.45718125" y1="6.477" x2="12.14881875" y2="6.56158125" layer="94"/>
<rectangle x1="12.74318125" y1="6.477" x2="13.335" y2="6.56158125" layer="94"/>
<rectangle x1="13.67281875" y1="6.477" x2="15.113" y2="6.56158125" layer="94"/>
<rectangle x1="15.45081875" y1="6.477" x2="15.79118125" y2="6.56158125" layer="94"/>
<rectangle x1="16.21281875" y1="6.477" x2="19.939" y2="6.56158125" layer="94"/>
<rectangle x1="0.127" y1="6.56158125" x2="0.71881875" y2="6.64641875" layer="94"/>
<rectangle x1="2.49681875" y1="6.56158125" x2="3.175" y2="6.64641875" layer="94"/>
<rectangle x1="4.953" y1="6.56158125" x2="5.63118125" y2="6.64641875" layer="94"/>
<rectangle x1="7.493" y1="6.56158125" x2="8.255" y2="6.64641875" layer="94"/>
<rectangle x1="9.017" y1="6.56158125" x2="9.779" y2="6.64641875" layer="94"/>
<rectangle x1="10.37081875" y1="6.56158125" x2="12.23518125" y2="6.64641875" layer="94"/>
<rectangle x1="12.74318125" y1="6.56158125" x2="13.335" y2="6.64641875" layer="94"/>
<rectangle x1="13.67281875" y1="6.56158125" x2="15.02918125" y2="6.64641875" layer="94"/>
<rectangle x1="15.45081875" y1="6.56158125" x2="15.79118125" y2="6.64641875" layer="94"/>
<rectangle x1="17.82318125" y1="6.56158125" x2="19.939" y2="6.64641875" layer="94"/>
<rectangle x1="0.127" y1="6.64641875" x2="0.71881875" y2="6.731" layer="94"/>
<rectangle x1="2.49681875" y1="6.64641875" x2="3.175" y2="6.731" layer="94"/>
<rectangle x1="4.953" y1="6.64641875" x2="5.63118125" y2="6.731" layer="94"/>
<rectangle x1="7.57681875" y1="6.64641875" x2="8.255" y2="6.731" layer="94"/>
<rectangle x1="9.017" y1="6.64641875" x2="9.779" y2="6.731" layer="94"/>
<rectangle x1="10.287" y1="6.64641875" x2="12.319" y2="6.731" layer="94"/>
<rectangle x1="12.827" y1="6.64641875" x2="13.335" y2="6.731" layer="94"/>
<rectangle x1="13.67281875" y1="6.64641875" x2="15.02918125" y2="6.731" layer="94"/>
<rectangle x1="15.45081875" y1="6.64641875" x2="15.70481875" y2="6.731" layer="94"/>
<rectangle x1="17.907" y1="6.64641875" x2="19.939" y2="6.731" layer="94"/>
<rectangle x1="0.04318125" y1="6.731" x2="0.71881875" y2="6.81558125" layer="94"/>
<rectangle x1="2.49681875" y1="6.731" x2="3.09118125" y2="6.81558125" layer="94"/>
<rectangle x1="5.03681875" y1="6.731" x2="5.715" y2="6.81558125" layer="94"/>
<rectangle x1="7.66318125" y1="6.731" x2="8.255" y2="6.81558125" layer="94"/>
<rectangle x1="9.017" y1="6.731" x2="9.779" y2="6.81558125" layer="94"/>
<rectangle x1="10.287" y1="6.731" x2="12.319" y2="6.81558125" layer="94"/>
<rectangle x1="12.827" y1="6.731" x2="13.335" y2="6.81558125" layer="94"/>
<rectangle x1="13.67281875" y1="6.731" x2="15.113" y2="6.81558125" layer="94"/>
<rectangle x1="15.45081875" y1="6.731" x2="15.70481875" y2="6.81558125" layer="94"/>
<rectangle x1="17.907" y1="6.731" x2="19.939" y2="6.81558125" layer="94"/>
<rectangle x1="0.04318125" y1="6.81558125" x2="0.71881875" y2="6.90041875" layer="94"/>
<rectangle x1="2.413" y1="6.81558125" x2="3.09118125" y2="6.90041875" layer="94"/>
<rectangle x1="5.03681875" y1="6.81558125" x2="5.715" y2="6.90041875" layer="94"/>
<rectangle x1="7.66318125" y1="6.81558125" x2="8.33881875" y2="6.90041875" layer="94"/>
<rectangle x1="9.017" y1="6.81558125" x2="9.69518125" y2="6.90041875" layer="94"/>
<rectangle x1="10.20318125" y1="6.81558125" x2="12.319" y2="6.90041875" layer="94"/>
<rectangle x1="12.827" y1="6.81558125" x2="13.335" y2="6.90041875" layer="94"/>
<rectangle x1="13.67281875" y1="6.81558125" x2="15.02918125" y2="6.90041875" layer="94"/>
<rectangle x1="15.45081875" y1="6.81558125" x2="15.70481875" y2="6.90041875" layer="94"/>
<rectangle x1="17.907" y1="6.81558125" x2="19.939" y2="6.90041875" layer="94"/>
<rectangle x1="0.04318125" y1="6.90041875" x2="0.635" y2="6.985" layer="94"/>
<rectangle x1="2.413" y1="6.90041875" x2="3.00481875" y2="6.985" layer="94"/>
<rectangle x1="5.12318125" y1="6.90041875" x2="5.715" y2="6.985" layer="94"/>
<rectangle x1="7.66318125" y1="6.90041875" x2="8.33881875" y2="6.985" layer="94"/>
<rectangle x1="9.017" y1="6.90041875" x2="9.69518125" y2="6.985" layer="94"/>
<rectangle x1="10.20318125" y1="6.90041875" x2="12.40281875" y2="6.985" layer="94"/>
<rectangle x1="12.91081875" y1="6.90041875" x2="13.335" y2="6.985" layer="94"/>
<rectangle x1="13.67281875" y1="6.90041875" x2="15.02918125" y2="6.985" layer="94"/>
<rectangle x1="15.45081875" y1="6.90041875" x2="15.79118125" y2="6.985" layer="94"/>
<rectangle x1="16.21281875" y1="6.90041875" x2="17.399" y2="6.985" layer="94"/>
<rectangle x1="17.907" y1="6.90041875" x2="19.939" y2="6.985" layer="94"/>
<rectangle x1="0.04318125" y1="6.985" x2="0.635" y2="7.06958125" layer="94"/>
<rectangle x1="2.413" y1="6.985" x2="3.00481875" y2="7.06958125" layer="94"/>
<rectangle x1="5.12318125" y1="6.985" x2="5.715" y2="7.06958125" layer="94"/>
<rectangle x1="7.747" y1="6.985" x2="8.33881875" y2="7.06958125" layer="94"/>
<rectangle x1="9.017" y1="6.985" x2="9.69518125" y2="7.06958125" layer="94"/>
<rectangle x1="10.20318125" y1="6.985" x2="12.40281875" y2="7.06958125" layer="94"/>
<rectangle x1="12.91081875" y1="6.985" x2="13.335" y2="7.06958125" layer="94"/>
<rectangle x1="13.75918125" y1="6.985" x2="15.02918125" y2="7.06958125" layer="94"/>
<rectangle x1="15.45081875" y1="6.985" x2="15.79118125" y2="7.06958125" layer="94"/>
<rectangle x1="16.21281875" y1="6.985" x2="17.48281875" y2="7.06958125" layer="94"/>
<rectangle x1="17.82318125" y1="6.985" x2="19.939" y2="7.06958125" layer="94"/>
<rectangle x1="0.04318125" y1="7.06958125" x2="0.635" y2="7.15441875" layer="94"/>
<rectangle x1="2.413" y1="7.06958125" x2="3.00481875" y2="7.15441875" layer="94"/>
<rectangle x1="5.12318125" y1="7.06958125" x2="5.715" y2="7.15441875" layer="94"/>
<rectangle x1="7.747" y1="7.06958125" x2="8.33881875" y2="7.15441875" layer="94"/>
<rectangle x1="9.017" y1="7.06958125" x2="9.69518125" y2="7.15441875" layer="94"/>
<rectangle x1="10.20318125" y1="7.06958125" x2="12.40281875" y2="7.15441875" layer="94"/>
<rectangle x1="12.91081875" y1="7.06958125" x2="13.335" y2="7.15441875" layer="94"/>
<rectangle x1="13.75918125" y1="7.06958125" x2="14.94281875" y2="7.15441875" layer="94"/>
<rectangle x1="15.367" y1="7.06958125" x2="15.79118125" y2="7.15441875" layer="94"/>
<rectangle x1="16.21281875" y1="7.06958125" x2="17.399" y2="7.15441875" layer="94"/>
<rectangle x1="17.82318125" y1="7.06958125" x2="19.939" y2="7.15441875" layer="94"/>
<rectangle x1="0.04318125" y1="7.15441875" x2="0.635" y2="7.239" layer="94"/>
<rectangle x1="2.413" y1="7.15441875" x2="3.00481875" y2="7.239" layer="94"/>
<rectangle x1="5.12318125" y1="7.15441875" x2="5.715" y2="7.239" layer="94"/>
<rectangle x1="7.747" y1="7.15441875" x2="8.33881875" y2="7.239" layer="94"/>
<rectangle x1="9.017" y1="7.15441875" x2="9.69518125" y2="7.239" layer="94"/>
<rectangle x1="10.20318125" y1="7.15441875" x2="12.40281875" y2="7.239" layer="94"/>
<rectangle x1="12.91081875" y1="7.15441875" x2="13.335" y2="7.239" layer="94"/>
<rectangle x1="13.843" y1="7.15441875" x2="14.94281875" y2="7.239" layer="94"/>
<rectangle x1="15.367" y1="7.15441875" x2="15.875" y2="7.239" layer="94"/>
<rectangle x1="16.29918125" y1="7.15441875" x2="17.31518125" y2="7.239" layer="94"/>
<rectangle x1="17.82318125" y1="7.15441875" x2="19.939" y2="7.239" layer="94"/>
<rectangle x1="0.04318125" y1="7.239" x2="0.635" y2="7.32358125" layer="94"/>
<rectangle x1="2.413" y1="7.239" x2="3.00481875" y2="7.32358125" layer="94"/>
<rectangle x1="5.12318125" y1="7.239" x2="5.715" y2="7.32358125" layer="94"/>
<rectangle x1="7.747" y1="7.239" x2="8.33881875" y2="7.32358125" layer="94"/>
<rectangle x1="9.017" y1="7.239" x2="9.69518125" y2="7.32358125" layer="94"/>
<rectangle x1="10.20318125" y1="7.239" x2="12.40281875" y2="7.32358125" layer="94"/>
<rectangle x1="12.91081875" y1="7.239" x2="13.335" y2="7.32358125" layer="94"/>
<rectangle x1="13.92681875" y1="7.239" x2="14.859" y2="7.32358125" layer="94"/>
<rectangle x1="15.28318125" y1="7.239" x2="15.875" y2="7.32358125" layer="94"/>
<rectangle x1="16.383" y1="7.239" x2="17.22881875" y2="7.32358125" layer="94"/>
<rectangle x1="17.73681875" y1="7.239" x2="19.939" y2="7.32358125" layer="94"/>
<rectangle x1="0.04318125" y1="7.32358125" x2="0.635" y2="7.40841875" layer="94"/>
<rectangle x1="2.413" y1="7.32358125" x2="3.00481875" y2="7.40841875" layer="94"/>
<rectangle x1="5.12318125" y1="7.32358125" x2="5.715" y2="7.40841875" layer="94"/>
<rectangle x1="7.747" y1="7.32358125" x2="8.33881875" y2="7.40841875" layer="94"/>
<rectangle x1="9.017" y1="7.32358125" x2="9.69518125" y2="7.40841875" layer="94"/>
<rectangle x1="10.20318125" y1="7.32358125" x2="12.40281875" y2="7.40841875" layer="94"/>
<rectangle x1="12.91081875" y1="7.32358125" x2="13.335" y2="7.40841875" layer="94"/>
<rectangle x1="14.01318125" y1="7.32358125" x2="14.68881875" y2="7.40841875" layer="94"/>
<rectangle x1="15.28318125" y1="7.32358125" x2="15.95881875" y2="7.40841875" layer="94"/>
<rectangle x1="16.46681875" y1="7.32358125" x2="17.145" y2="7.40841875" layer="94"/>
<rectangle x1="17.73681875" y1="7.32358125" x2="19.939" y2="7.40841875" layer="94"/>
<rectangle x1="0.04318125" y1="7.40841875" x2="0.635" y2="7.493" layer="94"/>
<rectangle x1="2.413" y1="7.40841875" x2="3.00481875" y2="7.493" layer="94"/>
<rectangle x1="5.12318125" y1="7.40841875" x2="5.715" y2="7.493" layer="94"/>
<rectangle x1="7.747" y1="7.40841875" x2="8.33881875" y2="7.493" layer="94"/>
<rectangle x1="9.017" y1="7.40841875" x2="9.69518125" y2="7.493" layer="94"/>
<rectangle x1="10.20318125" y1="7.40841875" x2="12.40281875" y2="7.493" layer="94"/>
<rectangle x1="12.91081875" y1="7.40841875" x2="13.335" y2="7.493" layer="94"/>
<rectangle x1="14.26718125" y1="7.40841875" x2="14.43481875" y2="7.493" layer="94"/>
<rectangle x1="15.19681875" y1="7.40841875" x2="15.95881875" y2="7.493" layer="94"/>
<rectangle x1="16.72081875" y1="7.40841875" x2="16.891" y2="7.493" layer="94"/>
<rectangle x1="17.653" y1="7.40841875" x2="19.939" y2="7.493" layer="94"/>
<rectangle x1="0.04318125" y1="7.493" x2="0.635" y2="7.57758125" layer="94"/>
<rectangle x1="2.413" y1="7.493" x2="3.00481875" y2="7.57758125" layer="94"/>
<rectangle x1="5.12318125" y1="7.493" x2="5.715" y2="7.57758125" layer="94"/>
<rectangle x1="7.66318125" y1="7.493" x2="8.33881875" y2="7.57758125" layer="94"/>
<rectangle x1="9.017" y1="7.493" x2="9.69518125" y2="7.57758125" layer="94"/>
<rectangle x1="10.20318125" y1="7.493" x2="12.40281875" y2="7.57758125" layer="94"/>
<rectangle x1="12.91081875" y1="7.493" x2="13.335" y2="7.57758125" layer="94"/>
<rectangle x1="15.113" y1="7.493" x2="16.04518125" y2="7.57758125" layer="94"/>
<rectangle x1="17.56918125" y1="7.493" x2="19.939" y2="7.57758125" layer="94"/>
<rectangle x1="0.04318125" y1="7.57758125" x2="0.71881875" y2="7.66241875" layer="94"/>
<rectangle x1="2.413" y1="7.57758125" x2="3.00481875" y2="7.66241875" layer="94"/>
<rectangle x1="5.12318125" y1="7.57758125" x2="5.715" y2="7.66241875" layer="94"/>
<rectangle x1="7.66318125" y1="7.57758125" x2="8.33881875" y2="7.66241875" layer="94"/>
<rectangle x1="9.017" y1="7.57758125" x2="9.69518125" y2="7.66241875" layer="94"/>
<rectangle x1="10.20318125" y1="7.57758125" x2="12.40281875" y2="7.66241875" layer="94"/>
<rectangle x1="12.91081875" y1="7.57758125" x2="13.335" y2="7.66241875" layer="94"/>
<rectangle x1="15.02918125" y1="7.57758125" x2="16.21281875" y2="7.66241875" layer="94"/>
<rectangle x1="17.48281875" y1="7.57758125" x2="19.939" y2="7.66241875" layer="94"/>
<rectangle x1="0.04318125" y1="7.66241875" x2="0.71881875" y2="7.747" layer="94"/>
<rectangle x1="2.413" y1="7.66241875" x2="3.00481875" y2="7.747" layer="94"/>
<rectangle x1="5.12318125" y1="7.66241875" x2="5.715" y2="7.747" layer="94"/>
<rectangle x1="7.66318125" y1="7.66241875" x2="8.255" y2="7.747" layer="94"/>
<rectangle x1="9.017" y1="7.66241875" x2="9.779" y2="7.747" layer="94"/>
<rectangle x1="10.287" y1="7.66241875" x2="12.319" y2="7.747" layer="94"/>
<rectangle x1="12.827" y1="7.66241875" x2="13.335" y2="7.747" layer="94"/>
<rectangle x1="13.67281875" y1="7.66241875" x2="13.843" y2="7.747" layer="94"/>
<rectangle x1="14.859" y1="7.66241875" x2="16.29918125" y2="7.747" layer="94"/>
<rectangle x1="17.31518125" y1="7.66241875" x2="19.939" y2="7.747" layer="94"/>
<rectangle x1="0.127" y1="7.747" x2="0.71881875" y2="7.83158125" layer="94"/>
<rectangle x1="2.413" y1="7.747" x2="3.00481875" y2="7.83158125" layer="94"/>
<rectangle x1="5.12318125" y1="7.747" x2="5.715" y2="7.83158125" layer="94"/>
<rectangle x1="7.57681875" y1="7.747" x2="8.255" y2="7.83158125" layer="94"/>
<rectangle x1="9.017" y1="7.747" x2="9.779" y2="7.83158125" layer="94"/>
<rectangle x1="10.287" y1="7.747" x2="12.319" y2="7.83158125" layer="94"/>
<rectangle x1="12.827" y1="7.747" x2="13.335" y2="7.83158125" layer="94"/>
<rectangle x1="13.589" y1="7.747" x2="14.097" y2="7.83158125" layer="94"/>
<rectangle x1="14.68881875" y1="7.747" x2="16.55318125" y2="7.83158125" layer="94"/>
<rectangle x1="17.06118125" y1="7.747" x2="19.939" y2="7.83158125" layer="94"/>
<rectangle x1="0.127" y1="7.83158125" x2="0.71881875" y2="7.91641875" layer="94"/>
<rectangle x1="2.413" y1="7.83158125" x2="3.00481875" y2="7.91641875" layer="94"/>
<rectangle x1="5.12318125" y1="7.83158125" x2="5.715" y2="7.91641875" layer="94"/>
<rectangle x1="7.57681875" y1="7.83158125" x2="8.255" y2="7.91641875" layer="94"/>
<rectangle x1="9.017" y1="7.83158125" x2="9.779" y2="7.91641875" layer="94"/>
<rectangle x1="10.37081875" y1="7.83158125" x2="12.23518125" y2="7.91641875" layer="94"/>
<rectangle x1="12.827" y1="7.83158125" x2="19.939" y2="7.91641875" layer="94"/>
<rectangle x1="0.127" y1="7.91641875" x2="0.71881875" y2="8.001" layer="94"/>
<rectangle x1="2.413" y1="7.91641875" x2="3.00481875" y2="8.001" layer="94"/>
<rectangle x1="5.12318125" y1="7.91641875" x2="5.715" y2="8.001" layer="94"/>
<rectangle x1="7.493" y1="7.91641875" x2="8.17118125" y2="8.001" layer="94"/>
<rectangle x1="9.017" y1="7.91641875" x2="9.86281875" y2="8.001" layer="94"/>
<rectangle x1="10.45718125" y1="7.91641875" x2="12.14881875" y2="8.001" layer="94"/>
<rectangle x1="12.74318125" y1="7.91641875" x2="19.939" y2="8.001" layer="94"/>
<rectangle x1="0.127" y1="8.001" x2="0.71881875" y2="8.08558125" layer="94"/>
<rectangle x1="2.413" y1="8.001" x2="3.00481875" y2="8.08558125" layer="94"/>
<rectangle x1="5.12318125" y1="8.001" x2="5.715" y2="8.08558125" layer="94"/>
<rectangle x1="7.40918125" y1="8.001" x2="8.17118125" y2="8.08558125" layer="94"/>
<rectangle x1="9.017" y1="8.001" x2="9.86281875" y2="8.08558125" layer="94"/>
<rectangle x1="10.45718125" y1="8.001" x2="12.065" y2="8.08558125" layer="94"/>
<rectangle x1="12.74318125" y1="8.001" x2="19.939" y2="8.08558125" layer="94"/>
<rectangle x1="0.127" y1="8.08558125" x2="0.80518125" y2="8.17041875" layer="94"/>
<rectangle x1="2.413" y1="8.08558125" x2="3.00481875" y2="8.17041875" layer="94"/>
<rectangle x1="5.12318125" y1="8.08558125" x2="5.715" y2="8.17041875" layer="94"/>
<rectangle x1="7.32281875" y1="8.08558125" x2="8.08481875" y2="8.17041875" layer="94"/>
<rectangle x1="9.017" y1="8.08558125" x2="9.94918125" y2="8.17041875" layer="94"/>
<rectangle x1="10.62481875" y1="8.08558125" x2="11.98118125" y2="8.17041875" layer="94"/>
<rectangle x1="12.65681875" y1="8.08558125" x2="19.939" y2="8.17041875" layer="94"/>
<rectangle x1="0.127" y1="8.17041875" x2="0.80518125" y2="8.255" layer="94"/>
<rectangle x1="2.413" y1="8.17041875" x2="3.00481875" y2="8.255" layer="94"/>
<rectangle x1="5.12318125" y1="8.17041875" x2="5.715" y2="8.255" layer="94"/>
<rectangle x1="7.15518125" y1="8.17041875" x2="8.001" y2="8.255" layer="94"/>
<rectangle x1="9.017" y1="8.17041875" x2="9.94918125" y2="8.255" layer="94"/>
<rectangle x1="10.71118125" y1="8.17041875" x2="11.89481875" y2="8.255" layer="94"/>
<rectangle x1="12.573" y1="8.17041875" x2="19.939" y2="8.255" layer="94"/>
<rectangle x1="0.21081875" y1="8.255" x2="0.80518125" y2="8.33958125" layer="94"/>
<rectangle x1="2.413" y1="8.255" x2="3.00481875" y2="8.33958125" layer="94"/>
<rectangle x1="5.12318125" y1="8.255" x2="5.715" y2="8.33958125" layer="94"/>
<rectangle x1="6.90118125" y1="8.255" x2="8.001" y2="8.33958125" layer="94"/>
<rectangle x1="9.017" y1="8.255" x2="10.033" y2="8.33958125" layer="94"/>
<rectangle x1="10.87881875" y1="8.255" x2="11.72718125" y2="8.33958125" layer="94"/>
<rectangle x1="12.573" y1="8.255" x2="19.939" y2="8.33958125" layer="94"/>
<rectangle x1="0.21081875" y1="8.33958125" x2="0.889" y2="8.42441875" layer="94"/>
<rectangle x1="2.413" y1="8.33958125" x2="3.00481875" y2="8.42441875" layer="94"/>
<rectangle x1="5.12318125" y1="8.33958125" x2="7.91718125" y2="8.42441875" layer="94"/>
<rectangle x1="9.10081875" y1="8.33958125" x2="10.11681875" y2="8.42441875" layer="94"/>
<rectangle x1="11.21918125" y1="8.33958125" x2="11.38681875" y2="8.42441875" layer="94"/>
<rectangle x1="12.48918125" y1="8.33958125" x2="19.85518125" y2="8.42441875" layer="94"/>
<rectangle x1="0.21081875" y1="8.42441875" x2="0.889" y2="8.509" layer="94"/>
<rectangle x1="2.413" y1="8.42441875" x2="3.00481875" y2="8.509" layer="94"/>
<rectangle x1="5.12318125" y1="8.42441875" x2="7.83081875" y2="8.509" layer="94"/>
<rectangle x1="9.10081875" y1="8.42441875" x2="10.20318125" y2="8.509" layer="94"/>
<rectangle x1="12.40281875" y1="8.42441875" x2="19.85518125" y2="8.509" layer="94"/>
<rectangle x1="0.29718125" y1="8.509" x2="0.889" y2="8.59358125" layer="94"/>
<rectangle x1="2.413" y1="8.509" x2="3.00481875" y2="8.59358125" layer="94"/>
<rectangle x1="5.12318125" y1="8.509" x2="7.747" y2="8.59358125" layer="94"/>
<rectangle x1="9.10081875" y1="8.509" x2="10.287" y2="8.59358125" layer="94"/>
<rectangle x1="12.23518125" y1="8.509" x2="19.85518125" y2="8.59358125" layer="94"/>
<rectangle x1="0.29718125" y1="8.59358125" x2="0.97281875" y2="8.67841875" layer="94"/>
<rectangle x1="2.413" y1="8.59358125" x2="3.00481875" y2="8.67841875" layer="94"/>
<rectangle x1="5.12318125" y1="8.59358125" x2="7.57681875" y2="8.67841875" layer="94"/>
<rectangle x1="9.10081875" y1="8.59358125" x2="10.45718125" y2="8.67841875" layer="94"/>
<rectangle x1="12.14881875" y1="8.59358125" x2="19.76881875" y2="8.67841875" layer="94"/>
<rectangle x1="0.29718125" y1="8.67841875" x2="0.97281875" y2="8.763" layer="94"/>
<rectangle x1="2.413" y1="8.67841875" x2="3.00481875" y2="8.763" layer="94"/>
<rectangle x1="5.12318125" y1="8.67841875" x2="7.493" y2="8.763" layer="94"/>
<rectangle x1="9.10081875" y1="8.67841875" x2="10.62481875" y2="8.763" layer="94"/>
<rectangle x1="11.98118125" y1="8.67841875" x2="19.76881875" y2="8.763" layer="94"/>
<rectangle x1="0.381" y1="8.763" x2="1.05918125" y2="8.84758125" layer="94"/>
<rectangle x1="2.413" y1="8.763" x2="2.921" y2="8.84758125" layer="94"/>
<rectangle x1="5.207" y1="8.763" x2="7.32281875" y2="8.84758125" layer="94"/>
<rectangle x1="9.10081875" y1="8.763" x2="10.795" y2="8.84758125" layer="94"/>
<rectangle x1="11.72718125" y1="8.763" x2="19.76881875" y2="8.84758125" layer="94"/>
<rectangle x1="0.381" y1="8.84758125" x2="1.05918125" y2="8.93241875" layer="94"/>
<rectangle x1="2.58318125" y1="8.84758125" x2="2.83718125" y2="8.93241875" layer="94"/>
<rectangle x1="5.29081875" y1="8.84758125" x2="7.06881875" y2="8.93241875" layer="94"/>
<rectangle x1="9.18718125" y1="8.84758125" x2="19.685" y2="8.93241875" layer="94"/>
<rectangle x1="0.46481875" y1="8.93241875" x2="1.143" y2="9.017" layer="94"/>
<rectangle x1="9.18718125" y1="8.93241875" x2="19.685" y2="9.017" layer="94"/>
<rectangle x1="0.46481875" y1="9.017" x2="1.143" y2="9.10158125" layer="94"/>
<rectangle x1="9.18718125" y1="9.017" x2="19.60118125" y2="9.10158125" layer="94"/>
<rectangle x1="0.55118125" y1="9.10158125" x2="1.22681875" y2="9.18641875" layer="94"/>
<rectangle x1="9.18718125" y1="9.10158125" x2="19.60118125" y2="9.18641875" layer="94"/>
<rectangle x1="0.55118125" y1="9.18641875" x2="1.31318125" y2="9.271" layer="94"/>
<rectangle x1="9.18718125" y1="9.18641875" x2="19.51481875" y2="9.271" layer="94"/>
<rectangle x1="0.635" y1="9.271" x2="1.397" y2="9.35558125" layer="94"/>
<rectangle x1="9.271" y1="9.271" x2="19.51481875" y2="9.35558125" layer="94"/>
<rectangle x1="0.71881875" y1="9.35558125" x2="1.397" y2="9.44041875" layer="94"/>
<rectangle x1="9.271" y1="9.35558125" x2="19.431" y2="9.44041875" layer="94"/>
<rectangle x1="0.71881875" y1="9.44041875" x2="1.48081875" y2="9.525" layer="94"/>
<rectangle x1="9.271" y1="9.44041875" x2="19.34718125" y2="9.525" layer="94"/>
<rectangle x1="0.80518125" y1="9.525" x2="1.56718125" y2="9.60958125" layer="94"/>
<rectangle x1="9.35481875" y1="9.525" x2="19.34718125" y2="9.60958125" layer="94"/>
<rectangle x1="0.889" y1="9.60958125" x2="1.651" y2="9.69441875" layer="94"/>
<rectangle x1="9.35481875" y1="9.60958125" x2="19.26081875" y2="9.69441875" layer="94"/>
<rectangle x1="0.889" y1="9.69441875" x2="1.73481875" y2="9.779" layer="94"/>
<rectangle x1="9.35481875" y1="9.69441875" x2="19.177" y2="9.779" layer="94"/>
<rectangle x1="0.97281875" y1="9.779" x2="1.82118125" y2="9.86358125" layer="94"/>
<rectangle x1="9.44118125" y1="9.779" x2="19.09318125" y2="9.86358125" layer="94"/>
<rectangle x1="1.05918125" y1="9.86358125" x2="1.905" y2="9.94841875" layer="94"/>
<rectangle x1="9.44118125" y1="9.86358125" x2="19.00681875" y2="9.94841875" layer="94"/>
<rectangle x1="1.143" y1="9.94841875" x2="2.07518125" y2="10.033" layer="94"/>
<rectangle x1="9.44118125" y1="9.94841875" x2="18.923" y2="10.033" layer="94"/>
<rectangle x1="1.22681875" y1="10.033" x2="2.159" y2="10.11758125" layer="94"/>
<rectangle x1="9.525" y1="10.033" x2="18.923" y2="10.11758125" layer="94"/>
<rectangle x1="1.31318125" y1="10.11758125" x2="2.24281875" y2="10.20241875" layer="94"/>
<rectangle x1="9.525" y1="10.11758125" x2="18.75281875" y2="10.20241875" layer="94"/>
<rectangle x1="1.397" y1="10.20241875" x2="2.413" y2="10.287" layer="94"/>
<rectangle x1="9.60881875" y1="10.20241875" x2="18.669" y2="10.287" layer="94"/>
<rectangle x1="1.48081875" y1="10.287" x2="2.58318125" y2="10.37158125" layer="94"/>
<rectangle x1="9.60881875" y1="10.287" x2="18.58518125" y2="10.37158125" layer="94"/>
<rectangle x1="1.56718125" y1="10.37158125" x2="2.75081875" y2="10.45641875" layer="94"/>
<rectangle x1="9.69518125" y1="10.37158125" x2="18.49881875" y2="10.45641875" layer="94"/>
<rectangle x1="1.73481875" y1="10.45641875" x2="3.00481875" y2="10.541" layer="94"/>
<rectangle x1="9.69518125" y1="10.45641875" x2="18.415" y2="10.541" layer="94"/>
<rectangle x1="1.82118125" y1="10.541" x2="3.34518125" y2="10.62558125" layer="94"/>
<rectangle x1="9.779" y1="10.541" x2="18.24481875" y2="10.62558125" layer="94"/>
<rectangle x1="1.98881875" y1="10.62558125" x2="9.69518125" y2="10.71041875" layer="94"/>
<rectangle x1="9.779" y1="10.62558125" x2="18.161" y2="10.71041875" layer="94"/>
<rectangle x1="2.07518125" y1="10.71041875" x2="17.99081875" y2="10.795" layer="94"/>
<rectangle x1="2.24281875" y1="10.795" x2="17.82318125" y2="10.87958125" layer="94"/>
<rectangle x1="2.413" y1="10.87958125" x2="17.653" y2="10.96441875" layer="94"/>
<rectangle x1="2.58318125" y1="10.96441875" x2="17.48281875" y2="11.049" layer="94"/>
<rectangle x1="2.83718125" y1="11.049" x2="17.22881875" y2="11.13358125" layer="94"/>
<rectangle x1="3.175" y1="11.13358125" x2="16.891" y2="11.21841875" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="UDO-LOGO-" prefix="LOGO" uservalue="yes">
<gates>
<gate name="LOGO" symbol="UDO-LOGO-20MM" x="0" y="0"/>
</gates>
<devices>
<device name="10MM" package="UDO-LOGO-10MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6649258/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="12MM" package="UDO-LOGO-12MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6649257/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="15MM" package="UDO-LOGO-15MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6649256/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="20MM" package="UDO-LOGO-20MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="30MM" package="UDO-LOGO-30MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="40MM" package="UDO-LOGO-40MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-dummy">
<packages>
<package name="EXTRA-BOM-LINE">
<text x="0" y="0" size="1.778" layer="48">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="48">&gt;VALUE</text>
<circle x="-2.54" y="0" radius="2.300065625" width="0.127" layer="48"/>
<wire x1="-4.064" y1="0" x2="-1.016" y2="0" width="0.127" layer="48"/>
<wire x1="-2.54" y1="1.524" x2="-2.54" y2="-1.27" width="0.127" layer="48"/>
</package>
</packages>
<symbols>
<symbol name="ARROW">
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.27" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="BOM">
<text x="2.54" y="2.54" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="2.54" y="0" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<circle x="0" y="0" radius="2.0478125" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="-2.54" size="1.778" layer="97" align="center-left">&gt;DESCRIPTION</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ARROW" prefix="DUMMY" uservalue="yes">
<gates>
<gate name="G$1" symbol="ARROW" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BOM" prefix="X" uservalue="yes">
<description>Dummy symbol for inserting extra BOM lines.</description>
<gates>
<gate name="X" symbol="BOM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EXTRA-BOM-LINE">
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-wago-mcs">
<packages>
<package name="P-733-372" urn="urn:adsk.eagle:footprint:8463454/1">
<wire x1="4.25" y1="16.25" x2="4.25" y2="-16.25" width="0.01" layer="51"/>
<wire x1="-2.35" y1="-15.1" x2="4.45" y2="-15.1" width="0.01" layer="51"/>
<wire x1="4.45" y1="13.7" x2="-0.55" y2="13.7" width="0.01" layer="51"/>
<wire x1="-0.55" y1="13.7" x2="-0.55" y2="15.1" width="0.01" layer="51"/>
<wire x1="4.45" y1="-13.8" x2="-0.55" y2="-13.8" width="0.01" layer="51"/>
<wire x1="-0.55" y1="-12.4" x2="4.45" y2="-12.4" width="0.01" layer="51"/>
<wire x1="-0.55" y1="-13.8" x2="-0.55" y2="-12.4" width="0.01" layer="51"/>
<wire x1="4.45" y1="15.1" x2="-2.35" y2="15.1" width="0.01" layer="51"/>
<wire x1="4.45" y1="16.25" x2="4.45" y2="-16.25" width="0.2" layer="21"/>
<wire x1="-2.35" y1="-15.4" x2="-2.35" y2="15.4" width="0.01" layer="51"/>
<wire x1="4.45" y1="-15.4" x2="2.45" y2="-15.4" width="0.01" layer="51"/>
<wire x1="-4.25" y1="16.25" x2="-4.25" y2="14.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="14.75" x2="3.65" y2="12.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="12.75" x2="-0.95" y2="12.75" width="0.01" layer="51"/>
<wire x1="-0.95" y1="12.75" x2="-1.25" y2="12.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="12.75" x2="-4.25" y2="12.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="12.75" x2="-4.45" y2="12.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="14.75" x2="-1.25" y2="14.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="14.75" x2="-0.95" y2="14.75" width="0.01" layer="51"/>
<wire x1="-0.95" y1="14.75" x2="3.65" y2="14.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-14.75" x2="-4.25" y2="-16.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-16.25" x2="4.45" y2="-16.25" width="0.2" layer="21"/>
<wire x1="-4.25" y1="-12.25" x2="-4.25" y2="-12.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-12.75" x2="3.65" y2="-14.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-12.75" x2="-1.25" y2="-12.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-12.75" x2="-0.95" y2="-12.75" width="0.01" layer="51"/>
<wire x1="-0.95" y1="-12.75" x2="3.65" y2="-12.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-14.75" x2="-0.95" y2="-14.75" width="0.01" layer="51"/>
<wire x1="-0.95" y1="-14.75" x2="-1.25" y2="-14.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-14.75" x2="-4.45" y2="-14.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-10.25" x2="3.65" y2="-10.8" width="0.01" layer="51"/>
<wire x1="3.65" y1="-10.8" x2="3.65" y2="-11.7" width="0.01" layer="51"/>
<wire x1="3.65" y1="-11.7" x2="3.65" y2="-12.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="-12.25" x2="-1.25" y2="-12.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-12.25" x2="-4.45" y2="-12.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-10.25" x2="-4.25" y2="-10.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-10.25" x2="-1.25" y2="-10.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-10.25" x2="3.65" y2="-10.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-9.75" x2="-4.25" y2="-10.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-7.25" x2="-4.25" y2="-7.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-7.75" x2="3.65" y2="-8.3" width="0.01" layer="51"/>
<wire x1="3.65" y1="-8.3" x2="3.65" y2="-9.2" width="0.01" layer="51"/>
<wire x1="3.65" y1="-9.2" x2="3.65" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-7.75" x2="-1.25" y2="-7.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-7.75" x2="3.65" y2="-7.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-9.75" x2="-1.25" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-9.75" x2="-4.45" y2="-9.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-5.25" x2="3.65" y2="-5.8" width="0.01" layer="51"/>
<wire x1="3.65" y1="-5.8" x2="3.65" y2="-6.7" width="0.01" layer="51"/>
<wire x1="3.65" y1="-6.7" x2="3.65" y2="-7.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="-7.25" x2="-1.25" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-7.25" x2="-4.45" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-5.25" x2="-4.25" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-5.25" x2="-1.25" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-5.25" x2="3.65" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-4.75" x2="-4.25" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-2.25" x2="-4.25" y2="-2.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-2.75" x2="3.65" y2="-3.3" width="0.01" layer="51"/>
<wire x1="3.65" y1="-3.3" x2="3.65" y2="-4.2" width="0.01" layer="51"/>
<wire x1="3.65" y1="-4.2" x2="3.65" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-2.75" x2="-1.25" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-2.75" x2="3.65" y2="-2.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-4.75" x2="-1.25" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-4.75" x2="-4.45" y2="-4.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-0.25" x2="3.65" y2="-0.8" width="0.01" layer="51"/>
<wire x1="3.65" y1="-0.8" x2="3.65" y2="-1.7" width="0.01" layer="51"/>
<wire x1="3.65" y1="-1.7" x2="3.65" y2="-2.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="-2.25" x2="-1.25" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-2.25" x2="-4.45" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-0.25" x2="-4.25" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-0.25" x2="-1.25" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-0.25" x2="3.65" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="0.25" x2="-4.25" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="2.75" x2="-4.25" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="2.25" x2="3.65" y2="1.7" width="0.01" layer="51"/>
<wire x1="3.65" y1="1.7" x2="3.65" y2="0.8" width="0.01" layer="51"/>
<wire x1="3.65" y1="0.8" x2="3.65" y2="0.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="2.25" x2="-1.25" y2="2.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="2.25" x2="3.65" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="0.25" x2="-1.25" y2="0.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="0.25" x2="-4.45" y2="0.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="4.75" x2="3.65" y2="4.2" width="0.01" layer="51"/>
<wire x1="3.65" y1="4.2" x2="3.65" y2="3.3" width="0.01" layer="51"/>
<wire x1="3.65" y1="3.3" x2="3.65" y2="2.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="2.75" x2="-1.25" y2="2.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="2.75" x2="-4.45" y2="2.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="4.75" x2="-4.25" y2="4.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="4.75" x2="-1.25" y2="4.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="4.75" x2="3.65" y2="4.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="5.25" x2="-4.25" y2="4.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="7.75" x2="-4.25" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="7.25" x2="3.65" y2="6.7" width="0.01" layer="51"/>
<wire x1="3.65" y1="6.7" x2="3.65" y2="5.8" width="0.01" layer="51"/>
<wire x1="3.65" y1="5.8" x2="3.65" y2="5.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="7.25" x2="-1.25" y2="7.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="7.25" x2="3.65" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="5.25" x2="-1.25" y2="5.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="5.25" x2="-4.45" y2="5.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="9.75" x2="3.65" y2="9.2" width="0.01" layer="51"/>
<wire x1="3.65" y1="9.2" x2="3.65" y2="8.3" width="0.01" layer="51"/>
<wire x1="3.65" y1="8.3" x2="3.65" y2="7.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="7.75" x2="-1.25" y2="7.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="7.75" x2="-4.45" y2="7.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="9.75" x2="-4.25" y2="9.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="9.75" x2="-1.25" y2="9.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="9.75" x2="3.65" y2="9.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="10.25" x2="-4.25" y2="9.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="12.75" x2="-4.25" y2="12.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="12.25" x2="3.65" y2="11.7" width="0.01" layer="51"/>
<wire x1="3.65" y1="11.7" x2="3.65" y2="10.8" width="0.01" layer="51"/>
<wire x1="3.65" y1="10.8" x2="3.65" y2="10.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="12.25" x2="-1.25" y2="12.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="12.25" x2="3.65" y2="12.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="10.25" x2="-1.25" y2="10.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="10.25" x2="-4.45" y2="10.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="14.2" x2="-0.95" y2="14.2" width="0.01" layer="51"/>
<wire x1="-4.45" y1="13.3" x2="-0.95" y2="13.3" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-13.3" x2="-0.95" y2="-13.3" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-14.2" x2="-0.95" y2="-14.2" width="0.01" layer="51"/>
<wire x1="-4.45" y1="11.7" x2="3.65" y2="11.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="10.8" x2="3.65" y2="10.8" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-10.8" x2="3.65" y2="-10.8" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-11.7" x2="3.65" y2="-11.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-8.3" x2="3.65" y2="-8.3" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-9.2" x2="3.65" y2="-9.2" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-5.8" x2="3.65" y2="-5.8" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-6.7" x2="3.65" y2="-6.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-3.3" x2="3.65" y2="-3.3" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-4.2" x2="3.65" y2="-4.2" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-0.8" x2="3.65" y2="-0.8" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-1.7" x2="3.65" y2="-1.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="1.7" x2="3.65" y2="1.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="0.8" x2="3.65" y2="0.8" width="0.01" layer="51"/>
<wire x1="-4.45" y1="4.2" x2="3.65" y2="4.2" width="0.01" layer="51"/>
<wire x1="-4.45" y1="3.3" x2="3.65" y2="3.3" width="0.01" layer="51"/>
<wire x1="-4.45" y1="6.7" x2="3.65" y2="6.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="5.8" x2="3.65" y2="5.8" width="0.01" layer="51"/>
<wire x1="-0.95" y1="-14.75" x2="-0.95" y2="-12.75" width="0.01" layer="51"/>
<wire x1="-0.95" y1="12.75" x2="-0.95" y2="14.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="9.2" x2="3.65" y2="9.2" width="0.01" layer="51"/>
<wire x1="-4.45" y1="8.3" x2="3.65" y2="8.3" width="0.01" layer="51"/>
<wire x1="4.45" y1="15.4" x2="2.45" y2="15.4" width="0.01" layer="51"/>
<wire x1="4.45" y1="16.25" x2="-4.45" y2="16.25" width="0.2" layer="21"/>
<wire x1="2.45" y1="16.25" x2="2.45" y2="15.75" width="0.01" layer="51"/>
<wire x1="2.45" y1="15.75" x2="2.45" y2="15.4" width="0.01" layer="51"/>
<wire x1="-4.45" y1="15.4" x2="-2.35" y2="15.4" width="0.01" layer="51"/>
<wire x1="-4.45" y1="15.75" x2="2.45" y2="15.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-12.75" x2="-4.45" y2="-14.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-10.25" x2="-4.45" y2="-12.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-7.75" x2="-4.45" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-5.25" x2="-4.45" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-2.75" x2="-4.45" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-0.25" x2="-4.45" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="2.25" x2="-4.45" y2="0.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="4.75" x2="-4.45" y2="2.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="7.25" x2="-4.45" y2="5.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="9.75" x2="-4.45" y2="7.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="12.25" x2="-4.45" y2="10.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="14.75" x2="-4.45" y2="12.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="16.25" x2="-4.45" y2="-15.4" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-15.4" x2="-4.45" y2="-15.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-15.75" x2="-4.45" y2="-16.25" width="0.01" layer="51"/>
<wire x1="2.45" y1="-15.4" x2="2.45" y2="-15.75" width="0.01" layer="51"/>
<wire x1="2.45" y1="-15.75" x2="2.45" y2="-16.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-15.75" x2="2.45" y2="-15.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-15.4" x2="-2.35" y2="-15.4" width="0.01" layer="51"/>
<wire x1="-1.25" y1="14.75" x2="-1.25" y2="14.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="14.15" x2="-1.25" y2="13.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="13.35" x2="-1.25" y2="12.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-12.75" x2="-1.25" y2="-13.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-13.35" x2="-1.25" y2="-14.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-14.15" x2="-1.25" y2="-14.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-10.25" x2="-1.25" y2="-10.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-10.85" x2="-1.25" y2="-11.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-11.65" x2="-1.25" y2="-12.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-7.75" x2="-1.25" y2="-8.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-8.35" x2="-1.25" y2="-9.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-9.15" x2="-1.25" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-5.25" x2="-1.25" y2="-5.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-5.85" x2="-1.25" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-6.65" x2="-1.25" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-2.75" x2="-1.25" y2="-3.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-3.35" x2="-1.25" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-4.15" x2="-1.25" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-0.25" x2="-1.25" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-0.85" x2="-1.25" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-1.65" x2="-1.25" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="2.25" x2="-1.25" y2="1.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="1.65" x2="-1.25" y2="0.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="0.85" x2="-1.25" y2="0.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="4.75" x2="-1.25" y2="4.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="4.15" x2="-1.25" y2="3.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="3.35" x2="-1.25" y2="2.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="7.25" x2="-1.25" y2="6.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="6.65" x2="-1.25" y2="5.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="5.85" x2="-1.25" y2="5.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="9.75" x2="-1.25" y2="9.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="9.15" x2="-1.25" y2="8.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="8.35" x2="-1.25" y2="7.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="12.25" x2="-1.25" y2="11.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="11.65" x2="-1.25" y2="10.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="10.85" x2="-1.25" y2="10.25" width="0.01" layer="51"/>
<wire x1="3.45" y1="14.15" x2="3.45" y2="13.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="14.15" x2="3.45" y2="14.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="13.35" x2="3.45" y2="13.35" width="0.01" layer="51"/>
<wire x1="3.45" y1="-13.35" x2="3.45" y2="-14.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-13.35" x2="3.45" y2="-13.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-14.15" x2="3.45" y2="-14.15" width="0.01" layer="51"/>
<wire x1="3.45" y1="-10.85" x2="3.45" y2="-11.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-10.85" x2="3.45" y2="-10.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-11.65" x2="3.45" y2="-11.65" width="0.01" layer="51"/>
<wire x1="3.45" y1="-8.35" x2="3.45" y2="-9.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-8.35" x2="3.45" y2="-8.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-9.15" x2="3.45" y2="-9.15" width="0.01" layer="51"/>
<wire x1="3.45" y1="-5.85" x2="3.45" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-5.85" x2="3.45" y2="-5.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-6.65" x2="3.45" y2="-6.65" width="0.01" layer="51"/>
<wire x1="3.45" y1="-3.35" x2="3.45" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-3.35" x2="3.45" y2="-3.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-4.15" x2="3.45" y2="-4.15" width="0.01" layer="51"/>
<wire x1="3.45" y1="-0.85" x2="3.45" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-0.85" x2="3.45" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-1.65" x2="3.45" y2="-1.65" width="0.01" layer="51"/>
<wire x1="3.45" y1="1.65" x2="3.45" y2="0.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="1.65" x2="3.45" y2="1.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="0.85" x2="3.45" y2="0.85" width="0.01" layer="51"/>
<wire x1="3.45" y1="4.15" x2="3.45" y2="3.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="4.15" x2="3.45" y2="4.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="3.35" x2="3.45" y2="3.35" width="0.01" layer="51"/>
<wire x1="3.45" y1="6.65" x2="3.45" y2="5.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="6.65" x2="3.45" y2="6.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="5.85" x2="3.45" y2="5.85" width="0.01" layer="51"/>
<wire x1="3.45" y1="9.15" x2="3.45" y2="8.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="9.15" x2="3.45" y2="9.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="8.35" x2="3.45" y2="8.35" width="0.01" layer="51"/>
<wire x1="3.45" y1="11.65" x2="3.45" y2="10.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="11.65" x2="3.45" y2="11.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="10.85" x2="3.45" y2="10.85" width="0.01" layer="51"/>
<wire x1="-4.05" y1="14.15" x2="-4.05" y2="13.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="14.15" x2="-4.05" y2="14.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="13.35" x2="-4.05" y2="13.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="13.35" x2="-3.25" y2="14.15" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-13.35" x2="-4.05" y2="-14.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-13.35" x2="-4.05" y2="-13.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-14.15" x2="-4.05" y2="-14.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-14.15" x2="-3.25" y2="-13.35" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-10.85" x2="-4.05" y2="-11.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-10.85" x2="-4.05" y2="-10.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-11.65" x2="-4.05" y2="-11.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-11.65" x2="-3.25" y2="-10.85" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-8.35" x2="-4.05" y2="-9.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-8.35" x2="-4.05" y2="-8.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-9.15" x2="-4.05" y2="-9.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-9.15" x2="-3.25" y2="-8.35" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-5.85" x2="-4.05" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-5.85" x2="-4.05" y2="-5.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-6.65" x2="-4.05" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-6.65" x2="-3.25" y2="-5.85" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-3.35" x2="-4.05" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-3.35" x2="-4.05" y2="-3.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-4.15" x2="-4.05" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-4.15" x2="-3.25" y2="-3.35" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-0.85" x2="-4.05" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-0.85" x2="-4.05" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-1.65" x2="-4.05" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-1.65" x2="-3.25" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-4.05" y1="1.65" x2="-4.05" y2="0.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="1.65" x2="-4.05" y2="1.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="0.85" x2="-4.05" y2="0.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="0.85" x2="-3.25" y2="1.65" width="0.01" layer="51"/>
<wire x1="-4.05" y1="4.15" x2="-4.05" y2="3.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="4.15" x2="-4.05" y2="4.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="3.35" x2="-4.05" y2="3.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="3.35" x2="-3.25" y2="4.15" width="0.01" layer="51"/>
<wire x1="-4.05" y1="6.65" x2="-4.05" y2="5.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="6.65" x2="-4.05" y2="6.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="5.85" x2="-4.05" y2="5.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="5.85" x2="-3.25" y2="6.65" width="0.01" layer="51"/>
<wire x1="-4.05" y1="9.15" x2="-4.05" y2="8.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="9.15" x2="-4.05" y2="9.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="8.35" x2="-4.05" y2="8.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="8.35" x2="-3.25" y2="9.15" width="0.01" layer="51"/>
<wire x1="-4.05" y1="11.65" x2="-4.05" y2="10.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="11.65" x2="-4.05" y2="11.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="10.85" x2="-4.05" y2="10.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="10.85" x2="-3.25" y2="11.65" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-16.24" x2="-4.45" y2="-14.77" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-12.73" x2="-4.45" y2="-12.27" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-10.23" x2="-4.45" y2="-9.77" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-7.73" x2="-4.45" y2="-7.27" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-5.23" x2="-4.45" y2="-4.77" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-2.74" x2="-4.45" y2="-2.27" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-0.23" x2="-4.45" y2="0.23" width="0.2" layer="21"/>
<wire x1="-4.45" y1="2.27" x2="-4.45" y2="2.73" width="0.2" layer="21"/>
<wire x1="-4.45" y1="4.76" x2="-4.45" y2="5.23" width="0.2" layer="21"/>
<wire x1="-4.45" y1="7.26" x2="-4.45" y2="7.73" width="0.2" layer="21"/>
<wire x1="-4.45" y1="9.77" x2="-4.45" y2="10.23" width="0.2" layer="21"/>
<wire x1="-4.45" y1="16.24" x2="-4.45" y2="14.77" width="0.2" layer="21"/>
<wire x1="-4.45" y1="12.27" x2="-4.45" y2="12.73" width="0.2" layer="21"/>
<pad name="L12" x="-3.65" y="13.75" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L11" x="-3.65" y="11.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L10" x="-3.65" y="8.75" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L9" x="-3.65" y="6.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L8" x="-3.65" y="3.75" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L7" x="-3.65" y="1.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L6" x="-3.65" y="-1.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L5" x="-3.65" y="-3.75" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L4" x="-3.65" y="-6.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L3" x="-3.65" y="-8.75" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L2" x="-3.65" y="-11.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L1" x="-3.65" y="-13.75" drill="1.1" diameter="1.6" shape="long"/>
<text x="-3.95" y="18.02" size="2" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-6.65" y="-15.46" size="2" layer="51" font="vector" ratio="10" rot="R90">1</text>
<text x="-3.95" y="-19.22" size="2" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="P-733-368" urn="urn:adsk.eagle:footprint:10357096/1">
<wire x1="4.25" y1="11.25" x2="4.25" y2="-11.25" width="0.01" layer="51"/>
<wire x1="-2.35" y1="-10.1" x2="4.45" y2="-10.1" width="0.01" layer="51"/>
<wire x1="4.45" y1="8.7" x2="-0.55" y2="8.7" width="0.01" layer="51"/>
<wire x1="-0.55" y1="8.7" x2="-0.55" y2="10.1" width="0.01" layer="51"/>
<wire x1="4.45" y1="-8.8" x2="-0.55" y2="-8.8" width="0.01" layer="51"/>
<wire x1="-0.55" y1="-7.4" x2="4.45" y2="-7.4" width="0.01" layer="51"/>
<wire x1="-0.55" y1="-8.8" x2="-0.55" y2="-7.4" width="0.01" layer="51"/>
<wire x1="4.45" y1="10.1" x2="-2.35" y2="10.1" width="0.01" layer="51"/>
<wire x1="4.45" y1="11.25" x2="4.45" y2="-11.25" width="0.2" layer="21"/>
<wire x1="-2.35" y1="-10.4" x2="-2.35" y2="10.4" width="0.01" layer="51"/>
<wire x1="4.45" y1="-10.4" x2="2.45" y2="-10.4" width="0.01" layer="51"/>
<wire x1="-4.25" y1="11.25" x2="-4.25" y2="9.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="9.75" x2="3.65" y2="7.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="7.75" x2="-0.95" y2="7.75" width="0.01" layer="51"/>
<wire x1="-0.95" y1="7.75" x2="-1.25" y2="7.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="7.75" x2="-4.25" y2="7.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="7.75" x2="-4.45" y2="7.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="9.75" x2="-1.25" y2="9.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="9.75" x2="-0.95" y2="9.75" width="0.01" layer="51"/>
<wire x1="-0.95" y1="9.75" x2="3.65" y2="9.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-9.75" x2="-4.25" y2="-11.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-11.25" x2="4.45" y2="-11.25" width="0.2" layer="21"/>
<wire x1="-4.25" y1="-7.25" x2="-4.25" y2="-7.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-7.75" x2="3.65" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-7.75" x2="-1.25" y2="-7.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-7.75" x2="-0.95" y2="-7.75" width="0.01" layer="51"/>
<wire x1="-0.95" y1="-7.75" x2="3.65" y2="-7.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-9.75" x2="-0.95" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-0.95" y1="-9.75" x2="-1.25" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-9.75" x2="-4.45" y2="-9.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-5.25" x2="3.65" y2="-5.8" width="0.01" layer="51"/>
<wire x1="3.65" y1="-5.8" x2="3.65" y2="-6.7" width="0.01" layer="51"/>
<wire x1="3.65" y1="-6.7" x2="3.65" y2="-7.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="-7.25" x2="-1.25" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-7.25" x2="-4.45" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-5.25" x2="-4.25" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-5.25" x2="-1.25" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-5.25" x2="3.65" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-4.75" x2="-4.25" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-2.25" x2="-4.25" y2="-2.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-2.75" x2="3.65" y2="-3.3" width="0.01" layer="51"/>
<wire x1="3.65" y1="-3.3" x2="3.65" y2="-4.2" width="0.01" layer="51"/>
<wire x1="3.65" y1="-4.2" x2="3.65" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-2.75" x2="-1.25" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-2.75" x2="3.65" y2="-2.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-4.75" x2="-1.25" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-4.75" x2="-4.45" y2="-4.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-0.25" x2="3.65" y2="-0.8" width="0.01" layer="51"/>
<wire x1="3.65" y1="-0.8" x2="3.65" y2="-1.7" width="0.01" layer="51"/>
<wire x1="3.65" y1="-1.7" x2="3.65" y2="-2.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="-2.25" x2="-1.25" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-2.25" x2="-4.45" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-0.25" x2="-4.25" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-0.25" x2="-1.25" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-0.25" x2="3.65" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="0.25" x2="-4.25" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="2.75" x2="-4.25" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="2.25" x2="3.65" y2="1.7" width="0.01" layer="51"/>
<wire x1="3.65" y1="1.7" x2="3.65" y2="0.8" width="0.01" layer="51"/>
<wire x1="3.65" y1="0.8" x2="3.65" y2="0.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="2.25" x2="-1.25" y2="2.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="2.25" x2="3.65" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="0.25" x2="-1.25" y2="0.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="0.25" x2="-4.45" y2="0.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="4.75" x2="3.65" y2="4.2" width="0.01" layer="51"/>
<wire x1="3.65" y1="4.2" x2="3.65" y2="3.3" width="0.01" layer="51"/>
<wire x1="3.65" y1="3.3" x2="3.65" y2="2.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="2.75" x2="-1.25" y2="2.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="2.75" x2="-4.45" y2="2.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="4.75" x2="-4.25" y2="4.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="4.75" x2="-1.25" y2="4.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="4.75" x2="3.65" y2="4.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="5.25" x2="-4.25" y2="4.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="7.75" x2="-4.25" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="7.25" x2="3.65" y2="6.7" width="0.01" layer="51"/>
<wire x1="3.65" y1="6.7" x2="3.65" y2="5.8" width="0.01" layer="51"/>
<wire x1="3.65" y1="5.8" x2="3.65" y2="5.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="7.25" x2="-1.25" y2="7.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="7.25" x2="3.65" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="5.25" x2="-1.25" y2="5.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="5.25" x2="-4.45" y2="5.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="9.2" x2="-0.95" y2="9.2" width="0.01" layer="51"/>
<wire x1="-4.45" y1="8.3" x2="-0.95" y2="8.3" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-8.3" x2="-0.95" y2="-8.3" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-9.2" x2="-0.95" y2="-9.2" width="0.01" layer="51"/>
<wire x1="-4.45" y1="6.7" x2="3.65" y2="6.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="5.8" x2="3.65" y2="5.8" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-5.8" x2="3.65" y2="-5.8" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-6.7" x2="3.65" y2="-6.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-3.3" x2="3.65" y2="-3.3" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-4.2" x2="3.65" y2="-4.2" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-0.8" x2="3.65" y2="-0.8" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-1.7" x2="3.65" y2="-1.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="1.7" x2="3.65" y2="1.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="0.8" x2="3.65" y2="0.8" width="0.01" layer="51"/>
<wire x1="-0.95" y1="-9.75" x2="-0.95" y2="-7.75" width="0.01" layer="51"/>
<wire x1="-0.95" y1="7.75" x2="-0.95" y2="9.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="4.2" x2="3.65" y2="4.2" width="0.01" layer="51"/>
<wire x1="-4.45" y1="3.3" x2="3.65" y2="3.3" width="0.01" layer="51"/>
<wire x1="4.45" y1="10.4" x2="2.45" y2="10.4" width="0.01" layer="51"/>
<wire x1="4.45" y1="11.25" x2="-4.45" y2="11.25" width="0.2" layer="21"/>
<wire x1="2.45" y1="11.25" x2="2.45" y2="10.75" width="0.01" layer="51"/>
<wire x1="2.45" y1="10.75" x2="2.45" y2="10.4" width="0.01" layer="51"/>
<wire x1="-4.45" y1="10.4" x2="-2.35" y2="10.4" width="0.01" layer="51"/>
<wire x1="-4.45" y1="10.75" x2="2.45" y2="10.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-7.75" x2="-4.45" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-5.25" x2="-4.45" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-2.75" x2="-4.45" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-0.25" x2="-4.45" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="2.25" x2="-4.45" y2="0.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="4.75" x2="-4.45" y2="2.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="7.25" x2="-4.45" y2="5.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="9.75" x2="-4.45" y2="7.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="11.25" x2="-4.45" y2="-10.4" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-10.4" x2="-4.45" y2="-10.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-10.75" x2="-4.45" y2="-11.25" width="0.01" layer="51"/>
<wire x1="2.45" y1="-10.4" x2="2.45" y2="-10.75" width="0.01" layer="51"/>
<wire x1="2.45" y1="-10.75" x2="2.45" y2="-11.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-10.75" x2="2.45" y2="-10.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-10.4" x2="-2.35" y2="-10.4" width="0.01" layer="51"/>
<wire x1="-1.25" y1="9.75" x2="-1.25" y2="9.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="9.15" x2="-1.25" y2="8.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="8.35" x2="-1.25" y2="7.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-7.75" x2="-1.25" y2="-8.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-8.35" x2="-1.25" y2="-9.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-9.15" x2="-1.25" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-5.25" x2="-1.25" y2="-5.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-5.85" x2="-1.25" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-6.65" x2="-1.25" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-2.75" x2="-1.25" y2="-3.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-3.35" x2="-1.25" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-4.15" x2="-1.25" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-0.25" x2="-1.25" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-0.85" x2="-1.25" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-1.65" x2="-1.25" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="2.25" x2="-1.25" y2="1.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="1.65" x2="-1.25" y2="0.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="0.85" x2="-1.25" y2="0.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="4.75" x2="-1.25" y2="4.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="4.15" x2="-1.25" y2="3.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="3.35" x2="-1.25" y2="2.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="7.25" x2="-1.25" y2="6.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="6.65" x2="-1.25" y2="5.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="5.85" x2="-1.25" y2="5.25" width="0.01" layer="51"/>
<wire x1="3.45" y1="9.15" x2="3.45" y2="8.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="9.15" x2="3.45" y2="9.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="8.35" x2="3.45" y2="8.35" width="0.01" layer="51"/>
<wire x1="3.45" y1="-8.35" x2="3.45" y2="-9.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-8.35" x2="3.45" y2="-8.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-9.15" x2="3.45" y2="-9.15" width="0.01" layer="51"/>
<wire x1="3.45" y1="-5.85" x2="3.45" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-5.85" x2="3.45" y2="-5.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-6.65" x2="3.45" y2="-6.65" width="0.01" layer="51"/>
<wire x1="3.45" y1="-3.35" x2="3.45" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-3.35" x2="3.45" y2="-3.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-4.15" x2="3.45" y2="-4.15" width="0.01" layer="51"/>
<wire x1="3.45" y1="-0.85" x2="3.45" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-0.85" x2="3.45" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-1.65" x2="3.45" y2="-1.65" width="0.01" layer="51"/>
<wire x1="3.45" y1="1.65" x2="3.45" y2="0.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="1.65" x2="3.45" y2="1.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="0.85" x2="3.45" y2="0.85" width="0.01" layer="51"/>
<wire x1="3.45" y1="4.15" x2="3.45" y2="3.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="4.15" x2="3.45" y2="4.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="3.35" x2="3.45" y2="3.35" width="0.01" layer="51"/>
<wire x1="3.45" y1="6.65" x2="3.45" y2="5.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="6.65" x2="3.45" y2="6.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="5.85" x2="3.45" y2="5.85" width="0.01" layer="51"/>
<wire x1="-4.05" y1="9.15" x2="-4.05" y2="8.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="9.15" x2="-4.05" y2="9.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="8.35" x2="-4.05" y2="8.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="8.35" x2="-3.25" y2="9.15" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-8.35" x2="-4.05" y2="-9.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-8.35" x2="-4.05" y2="-8.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-9.15" x2="-4.05" y2="-9.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-9.15" x2="-3.25" y2="-8.35" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-5.85" x2="-4.05" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-5.85" x2="-4.05" y2="-5.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-6.65" x2="-4.05" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-6.65" x2="-3.25" y2="-5.85" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-3.35" x2="-4.05" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-3.35" x2="-4.05" y2="-3.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-4.15" x2="-4.05" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-4.15" x2="-3.25" y2="-3.35" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-0.85" x2="-4.05" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-0.85" x2="-4.05" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-1.65" x2="-4.05" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-1.65" x2="-3.25" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-4.05" y1="1.65" x2="-4.05" y2="0.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="1.65" x2="-4.05" y2="1.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="0.85" x2="-4.05" y2="0.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="0.85" x2="-3.25" y2="1.65" width="0.01" layer="51"/>
<wire x1="-4.05" y1="4.15" x2="-4.05" y2="3.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="4.15" x2="-4.05" y2="4.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="3.35" x2="-4.05" y2="3.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="3.35" x2="-3.25" y2="4.15" width="0.01" layer="51"/>
<wire x1="-4.05" y1="6.65" x2="-4.05" y2="5.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="6.65" x2="-4.05" y2="6.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="5.85" x2="-4.05" y2="5.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="5.85" x2="-3.25" y2="6.65" width="0.01" layer="51"/>
<wire x1="-4.45" y1="11.23" x2="-4.45" y2="9.77" width="0.2" layer="21"/>
<wire x1="-4.45" y1="7.73" x2="-4.45" y2="7.26" width="0.2" layer="21"/>
<wire x1="-4.45" y1="5.23" x2="-4.45" y2="4.77" width="0.2" layer="21"/>
<wire x1="-4.45" y1="2.73" x2="-4.45" y2="2.26" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-11.25" x2="-4.45" y2="-9.77" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-7.73" x2="-4.45" y2="-7.27" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-5.23" x2="-4.45" y2="-4.77" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-2.73" x2="-4.45" y2="-2.27" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-0.23" x2="-4.45" y2="0.23" width="0.2" layer="21"/>
<pad name="L8" x="-3.65" y="8.75" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L7" x="-3.65" y="6.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L6" x="-3.65" y="3.75" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L5" x="-3.65" y="1.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L4" x="-3.65" y="-1.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L3" x="-3.65" y="-3.75" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L2" x="-3.65" y="-6.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L1" x="-3.65" y="-8.75" drill="1.1" diameter="1.6" shape="long"/>
<text x="-3.95" y="13.02" size="2" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-6.65" y="-10.3" size="2" layer="51" font="vector" ratio="10" rot="R90">1</text>
<text x="-3.95" y="-14.12" size="2" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="P-733-366" urn="urn:adsk.eagle:footprint:10355779/1">
<wire x1="4.25" y1="8.75" x2="4.25" y2="-8.75" width="0.01" layer="51"/>
<wire x1="-2.35" y1="-7.6" x2="4.45" y2="-7.6" width="0.01" layer="51"/>
<wire x1="4.45" y1="6.2" x2="-0.55" y2="6.2" width="0.01" layer="51"/>
<wire x1="-0.55" y1="6.2" x2="-0.55" y2="7.6" width="0.01" layer="51"/>
<wire x1="4.45" y1="-6.3" x2="-0.55" y2="-6.3" width="0.01" layer="51"/>
<wire x1="-0.55" y1="-4.9" x2="4.45" y2="-4.9" width="0.01" layer="51"/>
<wire x1="-0.55" y1="-6.3" x2="-0.55" y2="-4.9" width="0.01" layer="51"/>
<wire x1="4.45" y1="7.6" x2="-2.35" y2="7.6" width="0.01" layer="51"/>
<wire x1="4.45" y1="8.75" x2="4.45" y2="-8.75" width="0.2" layer="21"/>
<wire x1="-2.35" y1="-7.9" x2="-2.35" y2="7.9" width="0.01" layer="51"/>
<wire x1="4.45" y1="-7.9" x2="2.45" y2="-7.9" width="0.01" layer="51"/>
<wire x1="-4.25" y1="8.75" x2="-4.25" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="7.25" x2="3.65" y2="5.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="5.25" x2="-0.95" y2="5.25" width="0.01" layer="51"/>
<wire x1="-0.95" y1="5.25" x2="-1.25" y2="5.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="5.25" x2="-4.25" y2="5.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="5.25" x2="-4.45" y2="5.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="7.25" x2="-1.25" y2="7.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="7.25" x2="-0.95" y2="7.25" width="0.01" layer="51"/>
<wire x1="-0.95" y1="7.25" x2="3.65" y2="7.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-7.25" x2="-4.25" y2="-8.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-8.75" x2="4.45" y2="-8.75" width="0.2" layer="21"/>
<wire x1="-4.25" y1="-4.75" x2="-4.25" y2="-5.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="-5.25" x2="3.65" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-5.25" x2="-1.25" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-5.25" x2="-0.95" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-0.95" y1="-5.25" x2="3.65" y2="-5.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="-7.25" x2="-0.95" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-0.95" y1="-7.25" x2="-1.25" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-7.25" x2="-4.45" y2="-7.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="-2.75" x2="3.65" y2="-3.3" width="0.01" layer="51"/>
<wire x1="3.65" y1="-3.3" x2="3.65" y2="-4.2" width="0.01" layer="51"/>
<wire x1="3.65" y1="-4.2" x2="3.65" y2="-4.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="-4.75" x2="-1.25" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-4.75" x2="-4.45" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-2.75" x2="-4.25" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-2.75" x2="-1.25" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-2.75" x2="3.65" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="-2.25" x2="-4.25" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-4.25" y1="0.25" x2="-4.25" y2="-0.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="-0.25" x2="3.65" y2="-0.8" width="0.01" layer="51"/>
<wire x1="3.65" y1="-0.8" x2="3.65" y2="-1.7" width="0.01" layer="51"/>
<wire x1="3.65" y1="-1.7" x2="3.65" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-0.25" x2="-1.25" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-0.25" x2="3.65" y2="-0.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="-2.25" x2="-1.25" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-2.25" x2="-4.45" y2="-2.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="2.25" x2="3.65" y2="1.7" width="0.01" layer="51"/>
<wire x1="3.65" y1="1.7" x2="3.65" y2="0.8" width="0.01" layer="51"/>
<wire x1="3.65" y1="0.8" x2="3.65" y2="0.25" width="0.01" layer="51"/>
<wire x1="3.65" y1="0.25" x2="-1.25" y2="0.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="0.25" x2="-4.45" y2="0.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="2.25" x2="-4.25" y2="2.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="2.25" x2="-1.25" y2="2.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="2.25" x2="3.65" y2="2.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="2.75" x2="-4.25" y2="2.25" width="0.01" layer="51"/>
<wire x1="-4.25" y1="5.25" x2="-4.25" y2="4.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="4.75" x2="3.65" y2="4.2" width="0.01" layer="51"/>
<wire x1="3.65" y1="4.2" x2="3.65" y2="3.3" width="0.01" layer="51"/>
<wire x1="3.65" y1="3.3" x2="3.65" y2="2.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="4.75" x2="-1.25" y2="4.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="4.75" x2="3.65" y2="4.75" width="0.01" layer="51"/>
<wire x1="3.65" y1="2.75" x2="-1.25" y2="2.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="2.75" x2="-4.45" y2="2.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="6.7" x2="-0.95" y2="6.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="5.8" x2="-0.95" y2="5.8" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-5.8" x2="-0.95" y2="-5.8" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-6.7" x2="-0.95" y2="-6.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="4.2" x2="3.65" y2="4.2" width="0.01" layer="51"/>
<wire x1="-4.45" y1="3.3" x2="3.65" y2="3.3" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-3.3" x2="3.65" y2="-3.3" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-4.2" x2="3.65" y2="-4.2" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-0.8" x2="3.65" y2="-0.8" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-1.7" x2="3.65" y2="-1.7" width="0.01" layer="51"/>
<wire x1="-0.95" y1="-7.25" x2="-0.95" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-0.95" y1="5.25" x2="-0.95" y2="7.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="1.7" x2="3.65" y2="1.7" width="0.01" layer="51"/>
<wire x1="-4.45" y1="0.8" x2="3.65" y2="0.8" width="0.01" layer="51"/>
<wire x1="4.45" y1="7.9" x2="2.45" y2="7.9" width="0.01" layer="51"/>
<wire x1="4.45" y1="8.75" x2="-4.45" y2="8.75" width="0.2" layer="21"/>
<wire x1="2.45" y1="8.75" x2="2.45" y2="8.25" width="0.01" layer="51"/>
<wire x1="2.45" y1="8.25" x2="2.45" y2="7.9" width="0.01" layer="51"/>
<wire x1="-4.45" y1="7.9" x2="-2.35" y2="7.9" width="0.01" layer="51"/>
<wire x1="-4.45" y1="8.25" x2="2.45" y2="8.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-5.25" x2="-4.45" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-2.75" x2="-4.45" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-0.25" x2="-4.45" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="2.25" x2="-4.45" y2="0.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="4.75" x2="-4.45" y2="2.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="7.25" x2="-4.45" y2="5.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="8.75" x2="-4.45" y2="-7.9" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-7.9" x2="-4.45" y2="-8.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-8.25" x2="-4.45" y2="-8.75" width="0.01" layer="51"/>
<wire x1="2.45" y1="-7.9" x2="2.45" y2="-8.25" width="0.01" layer="51"/>
<wire x1="2.45" y1="-8.25" x2="2.45" y2="-8.75" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-8.25" x2="2.45" y2="-8.25" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-7.9" x2="-2.35" y2="-7.9" width="0.01" layer="51"/>
<wire x1="-1.25" y1="7.25" x2="-1.25" y2="6.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="6.65" x2="-1.25" y2="5.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="5.85" x2="-1.25" y2="5.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-5.25" x2="-1.25" y2="-5.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-5.85" x2="-1.25" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-6.65" x2="-1.25" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-2.75" x2="-1.25" y2="-3.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-3.35" x2="-1.25" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-4.15" x2="-1.25" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-0.25" x2="-1.25" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-0.85" x2="-1.25" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-1.65" x2="-1.25" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="2.25" x2="-1.25" y2="1.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="1.65" x2="-1.25" y2="0.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="0.85" x2="-1.25" y2="0.25" width="0.01" layer="51"/>
<wire x1="-1.25" y1="4.75" x2="-1.25" y2="4.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="4.15" x2="-1.25" y2="3.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="3.35" x2="-1.25" y2="2.75" width="0.01" layer="51"/>
<wire x1="3.45" y1="6.65" x2="3.45" y2="5.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="6.65" x2="3.45" y2="6.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="5.85" x2="3.45" y2="5.85" width="0.01" layer="51"/>
<wire x1="3.45" y1="-5.85" x2="3.45" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-5.85" x2="3.45" y2="-5.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-6.65" x2="3.45" y2="-6.65" width="0.01" layer="51"/>
<wire x1="3.45" y1="-3.35" x2="3.45" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-3.35" x2="3.45" y2="-3.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-4.15" x2="3.45" y2="-4.15" width="0.01" layer="51"/>
<wire x1="3.45" y1="-0.85" x2="3.45" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-0.85" x2="3.45" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="-1.65" x2="3.45" y2="-1.65" width="0.01" layer="51"/>
<wire x1="3.45" y1="1.65" x2="3.45" y2="0.85" width="0.01" layer="51"/>
<wire x1="-1.25" y1="1.65" x2="3.45" y2="1.65" width="0.01" layer="51"/>
<wire x1="-1.25" y1="0.85" x2="3.45" y2="0.85" width="0.01" layer="51"/>
<wire x1="3.45" y1="4.15" x2="3.45" y2="3.35" width="0.01" layer="51"/>
<wire x1="-1.25" y1="4.15" x2="3.45" y2="4.15" width="0.01" layer="51"/>
<wire x1="-1.25" y1="3.35" x2="3.45" y2="3.35" width="0.01" layer="51"/>
<wire x1="-4.05" y1="6.65" x2="-4.05" y2="5.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="6.65" x2="-4.05" y2="6.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="5.85" x2="-4.05" y2="5.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="5.85" x2="-3.25" y2="6.65" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-5.85" x2="-4.05" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-5.85" x2="-4.05" y2="-5.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-6.65" x2="-4.05" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-6.65" x2="-3.25" y2="-5.85" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-3.35" x2="-4.05" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-3.35" x2="-4.05" y2="-3.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-4.15" x2="-4.05" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-4.15" x2="-3.25" y2="-3.35" width="0.01" layer="51"/>
<wire x1="-4.05" y1="-0.85" x2="-4.05" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-0.85" x2="-4.05" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-1.65" x2="-4.05" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="-1.65" x2="-3.25" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-4.05" y1="1.65" x2="-4.05" y2="0.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="1.65" x2="-4.05" y2="1.65" width="0.01" layer="51"/>
<wire x1="-3.25" y1="0.85" x2="-4.05" y2="0.85" width="0.01" layer="51"/>
<wire x1="-3.25" y1="0.85" x2="-3.25" y2="1.65" width="0.01" layer="51"/>
<wire x1="-4.05" y1="4.15" x2="-4.05" y2="3.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="4.15" x2="-4.05" y2="4.15" width="0.01" layer="51"/>
<wire x1="-3.25" y1="3.35" x2="-4.05" y2="3.35" width="0.01" layer="51"/>
<wire x1="-3.25" y1="3.35" x2="-3.25" y2="4.15" width="0.01" layer="51"/>
<wire x1="-4.45" y1="-8.73" x2="-4.45" y2="-7.27" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-5.23" x2="-4.45" y2="-4.77" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-2.73" x2="-4.45" y2="-2.27" width="0.2" layer="21"/>
<wire x1="-4.45" y1="-0.23" x2="-4.45" y2="0.23" width="0.2" layer="21"/>
<wire x1="-4.45" y1="8.75" x2="-4.45" y2="7.27" width="0.2" layer="21"/>
<wire x1="-4.45" y1="5.23" x2="-4.45" y2="4.76" width="0.2" layer="21"/>
<wire x1="-4.45" y1="2.73" x2="-4.45" y2="2.27" width="0.2" layer="21"/>
<pad name="L6" x="-3.65" y="6.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L5" x="-3.65" y="3.75" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L4" x="-3.65" y="1.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L3" x="-3.65" y="-1.25" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L2" x="-3.65" y="-3.75" drill="1.1" diameter="1.6" shape="long"/>
<pad name="L1" x="-3.65" y="-6.25" drill="1.1" diameter="1.6" shape="long"/>
<text x="-3.95" y="10.52" size="2" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-6.65" y="-7.72" size="2" layer="51" font="vector" ratio="10" rot="R90">1</text>
<text x="-3.95" y="-11.72" size="2" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="P-733-372" urn="urn:adsk.eagle:package:8463460/2" type="model">
<packageinstances>
<packageinstance name="P-733-372"/>
</packageinstances>
</package3d>
<package3d name="P-733-368" urn="urn:adsk.eagle:package:10357097/2" type="model">
<packageinstances>
<packageinstance name="P-733-368"/>
</packageinstances>
</package3d>
<package3d name="P-733-366" urn="urn:adsk.eagle:package:10355780/2" type="model">
<packageinstances>
<packageinstance name="P-733-366"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="S-12-POL-S-1">
<wire x1="-6.35" y1="30.48" x2="3.81" y2="30.48" width="0.254" layer="97"/>
<wire x1="3.81" y1="30.48" x2="3.81" y2="25.4" width="0.254" layer="97"/>
<wire x1="3.81" y1="25.4" x2="3.81" y2="20.32" width="0.254" layer="97"/>
<wire x1="3.81" y1="20.32" x2="3.81" y2="15.24" width="0.254" layer="97"/>
<wire x1="3.81" y1="15.24" x2="3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="10.16" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="-10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="-15.24" width="0.254" layer="97"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="-20.32" width="0.254" layer="97"/>
<wire x1="3.81" y1="-20.32" x2="3.81" y2="-25.4" width="0.254" layer="97"/>
<wire x1="3.81" y1="-25.4" x2="3.81" y2="-30.48" width="0.254" layer="97"/>
<wire x1="3.81" y1="-30.48" x2="-6.35" y2="-30.48" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-30.48" x2="-6.35" y2="-25.4" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-25.4" x2="-6.35" y2="-20.32" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-20.32" x2="-6.35" y2="-15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-15.24" x2="-6.35" y2="-10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-10.16" x2="-6.35" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-5.08" x2="-6.35" y2="0" width="0.254" layer="97"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="15.24" x2="-6.35" y2="20.32" width="0.254" layer="97"/>
<wire x1="-6.35" y1="20.32" x2="-6.35" y2="25.4" width="0.254" layer="97"/>
<wire x1="-6.35" y1="25.4" x2="-6.35" y2="30.48" width="0.254" layer="97"/>
<wire x1="-6.35" y1="25.4" x2="3.81" y2="25.4" width="0.254" layer="97"/>
<wire x1="-6.35" y1="20.32" x2="3.81" y2="20.32" width="0.254" layer="97"/>
<wire x1="-6.35" y1="15.24" x2="3.81" y2="15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="10.16" x2="3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="5.08" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="0" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-5.08" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-10.16" x2="3.81" y2="-10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-15.24" x2="3.81" y2="-15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-20.32" x2="3.81" y2="-20.32" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-25.4" x2="3.81" y2="-25.4" width="0.254" layer="97"/>
<text x="-6.35" y="32.2" size="2" layer="95" font="vector" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-36.02" size="2" layer="96" font="vector" ratio="10">&gt;VALUE</text>
<pin name="P1" x="-2.54" y="-27.94" visible="pad" length="short" function="dot"/>
<pin name="P2" x="-2.54" y="-22.86" visible="pad" length="short" function="dot"/>
<pin name="P3" x="-2.54" y="-17.78" visible="pad" length="short" function="dot"/>
<pin name="P4" x="-2.54" y="-12.7" visible="pad" length="short" function="dot"/>
<pin name="P5" x="-2.54" y="-7.62" visible="pad" length="short" function="dot"/>
<pin name="P6" x="-2.54" y="-2.54" visible="pad" length="short" function="dot"/>
<pin name="P7" x="-2.54" y="2.54" visible="pad" length="short" function="dot"/>
<pin name="P8" x="-2.54" y="7.62" visible="pad" length="short" function="dot"/>
<pin name="P9" x="-2.54" y="12.7" visible="pad" length="short" function="dot"/>
<pin name="P10" x="-2.54" y="17.78" visible="pad" length="short" function="dot"/>
<pin name="P11" x="-2.54" y="22.86" visible="pad" length="short" function="dot"/>
<pin name="P12" x="-2.54" y="27.94" visible="pad" length="short" function="dot"/>
</symbol>
<symbol name="S-8-POL-S-1">
<wire x1="-6.35" y1="20.32" x2="3.81" y2="20.32" width="0.254" layer="97"/>
<wire x1="3.81" y1="20.32" x2="3.81" y2="15.24" width="0.254" layer="97"/>
<wire x1="3.81" y1="15.24" x2="3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="10.16" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="-10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="-15.24" width="0.254" layer="97"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="-20.32" width="0.254" layer="97"/>
<wire x1="3.81" y1="-20.32" x2="-6.35" y2="-20.32" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-20.32" x2="-6.35" y2="-15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-15.24" x2="-6.35" y2="-10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-10.16" x2="-6.35" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-5.08" x2="-6.35" y2="0" width="0.254" layer="97"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="15.24" x2="-6.35" y2="20.32" width="0.254" layer="97"/>
<wire x1="-6.35" y1="15.24" x2="3.81" y2="15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="10.16" x2="3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="5.08" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="0" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-5.08" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-10.16" x2="3.81" y2="-10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-15.24" x2="3.81" y2="-15.24" width="0.254" layer="97"/>
<text x="-6.35" y="22.68" size="2" layer="95" font="vector" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-25.46" size="2" layer="96" font="vector" ratio="10">&gt;VALUE</text>
<pin name="P2" x="-2.54" y="-12.7" visible="pad" length="short" function="dot" swaplevel="1"/>
<pin name="P3" x="-2.54" y="-7.62" visible="pad" length="short" function="dot" swaplevel="1"/>
<pin name="P4" x="-2.54" y="-2.54" visible="pad" length="short" function="dot" swaplevel="1"/>
<pin name="P5" x="-2.54" y="2.54" visible="pad" length="short" function="dot" swaplevel="1"/>
<pin name="P6" x="-2.54" y="7.62" visible="pad" length="short" function="dot" swaplevel="1"/>
<pin name="P7" x="-2.54" y="12.7" visible="pad" length="short" function="dot" swaplevel="1"/>
<pin name="P8" x="-2.54" y="17.78" visible="pad" length="short" function="dot" swaplevel="1"/>
<pin name="P1" x="-2.54" y="-17.78" visible="pad" length="short" function="dot" swaplevel="1"/>
</symbol>
<symbol name="S-6-POL-S">
<wire x1="-6.35" y1="15.24" x2="3.81" y2="15.24" width="0.254" layer="97"/>
<wire x1="3.81" y1="15.24" x2="3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="10.16" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="-10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="-15.24" width="0.254" layer="97"/>
<wire x1="3.81" y1="-15.24" x2="-6.35" y2="-15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-15.24" x2="-6.35" y2="-10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-10.16" x2="-6.35" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-5.08" x2="-6.35" y2="0" width="0.254" layer="97"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="10.16" x2="3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="5.08" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="0" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-5.08" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-10.16" x2="3.81" y2="-10.16" width="0.254" layer="97"/>
<text x="-6.45" y="17.44" size="2" layer="95" font="vector" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-20.32" size="2" layer="96" font="vector" ratio="10">&gt;VALUE</text>
<pin name="P1" x="-2.54" y="-12.7" visible="pad" length="short" function="dot"/>
<pin name="P2" x="-2.54" y="-7.62" visible="pad" length="short" function="dot"/>
<pin name="P3" x="-2.54" y="-2.54" visible="pad" length="short" function="dot"/>
<pin name="P4" x="-2.54" y="2.54" visible="pad" length="short" function="dot"/>
<pin name="P5" x="-2.54" y="7.62" visible="pad" length="short" function="dot"/>
<pin name="P6" x="-2.54" y="12.7" visible="pad" length="short" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="733-372" prefix="CN">
<description>&lt;b&gt;Serie 733,  Stiftleiste (für Leiterplatten) Einlötstift abgewinkelt 0,8 x 0,8 mm / Series 733,  Male header (for PCBs) Angled solder pin 0.8 x 0.8 mm&lt;/b&gt;&lt;br&gt;&lt;br&gt;Polzahl / Pole No.: 12 &lt;br&gt;Rastermaß / Pitch: 2.5 - 2.5  mm&lt;br&gt;Bemessungsspannung / Rated Voltage: 160 V&lt;br&gt;Nennstrom / Nominal Current: 6 A&lt;br&gt;Lötstiftlänge: / Solder Pin Length: 3.7 mm&lt;br&gt;Farbe / Color: lichtgrau / light gray&lt;br&gt;&lt;br&gt; Stand: 01.01.2014. Die jeweils aktuellen Daten zu diesem Artikel finden Sie in unserem Produktkatalog, den Sie unter &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt; einsehen können. &lt;br&gt; As of: 01/01/2014. Please find the newest data for this article in our product catalogue, which can be viewed under &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt;.&lt;br&gt;</description>
<gates>
<gate name="CN" symbol="S-12-POL-S-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P-733-372">
<connects>
<connect gate="CN" pin="P1" pad="L1"/>
<connect gate="CN" pin="P10" pad="L10"/>
<connect gate="CN" pin="P11" pad="L11"/>
<connect gate="CN" pin="P12" pad="L12"/>
<connect gate="CN" pin="P2" pad="L2"/>
<connect gate="CN" pin="P3" pad="L3"/>
<connect gate="CN" pin="P4" pad="L4"/>
<connect gate="CN" pin="P5" pad="L5"/>
<connect gate="CN" pin="P6" pad="L6"/>
<connect gate="CN" pin="P7" pad="L7"/>
<connect gate="CN" pin="P8" pad="L8"/>
<connect gate="CN" pin="P9" pad="L9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8463460/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="THT male header 0.8 x 0.8 mm solder pin, angled, 100% protected against mismating, Pin spacing 2.5 mm, 12-pole"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-372"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="733-368" prefix="CN">
<description>&lt;b&gt;Serie 733,  Stiftleiste (für Leiterplatten) Einlötstift abgewinkelt 0,8 x 0,8 mm / Series 733,  Male header (for PCBs) Angled solder pin 0.8 x 0.8 mm&lt;/b&gt;&lt;br&gt;&lt;br&gt;Polzahl / Pole No.: 8 &lt;br&gt;Rastermaß / Pitch: 2.5 - 2.5  mm&lt;br&gt;Bemessungsspannung / Rated Voltage: 160 V&lt;br&gt;Nennstrom / Nominal Current: 6 A&lt;br&gt;Lötstiftlänge: / Solder Pin Length: 3.7 mm&lt;br&gt;Farbe / Color: lichtgrau / light gray&lt;br&gt;&lt;br&gt; Stand: 01.01.2014. Die jeweils aktuellen Daten zu diesem Artikel finden Sie in unserem Produktkatalog, den Sie unter &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt; einsehen können. &lt;br&gt; As of: 01/01/2014. Please find the newest data for this article in our product catalogue, which can be viewed under &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt;.&lt;br&gt;</description>
<gates>
<gate name="CN" symbol="S-8-POL-S-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P-733-368">
<connects>
<connect gate="CN" pin="P1" pad="L1"/>
<connect gate="CN" pin="P2" pad="L2"/>
<connect gate="CN" pin="P3" pad="L3"/>
<connect gate="CN" pin="P4" pad="L4"/>
<connect gate="CN" pin="P5" pad="L5"/>
<connect gate="CN" pin="P6" pad="L6"/>
<connect gate="CN" pin="P7" pad="L7"/>
<connect gate="CN" pin="P8" pad="L8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10357097/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Series 733, Male header (for PCBs) Angled solder pin 0.8 x 0.8 mm"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-368"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="733-366" prefix="X">
<description>&lt;b&gt;Serie 733,  Stiftleiste (für Leiterplatten) Einlötstift abgewinkelt 0,8 x 0,8 mm / Series 733,  Male header (for PCBs) Angled solder pin 0.8 x 0.8 mm&lt;/b&gt;&lt;br&gt;&lt;br&gt;Polzahl / Pole No.: 6 &lt;br&gt;Rastermaß / Pitch: 2.5 - 2.5  mm&lt;br&gt;Bemessungsspannung / Rated Voltage: 160 V&lt;br&gt;Nennstrom / Nominal Current: 6 A&lt;br&gt;Lötstiftlänge: / Solder Pin Length: 3.7 mm&lt;br&gt;Farbe / Color: lichtgrau / light gray&lt;br&gt;&lt;br&gt; Stand: 01.01.2014. Die jeweils aktuellen Daten zu diesem Artikel finden Sie in unserem Produktkatalog, den Sie unter &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt; einsehen können. &lt;br&gt; As of: 01/01/2014. Please find the newest data for this article in our product catalogue, which can be viewed under &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt;.&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="S-6-POL-S" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P-733-366">
<connects>
<connect gate="G$1" pin="P1" pad="L1"/>
<connect gate="G$1" pin="P2" pad="L2"/>
<connect gate="G$1" pin="P3" pad="L3"/>
<connect gate="G$1" pin="P4" pad="L4"/>
<connect gate="G$1" pin="P5" pad="L5"/>
<connect gate="G$1" pin="P6" pad="L6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10355780/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Series 733, Male header (for PCBs) Angled solder pin 0.8 x 0.8 mm"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-366"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+24V" urn="urn:adsk.eagle:symbol:26935/1" library_version="1">
<wire x1="1.27" y1="-0.635" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V" urn="urn:adsk.eagle:component:26964/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+24V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl" urn="urn:adsk.eagle:library:334">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
http://www.secc.co.jp/pdf/os_e/2004/e_os_all.pdf &lt;b&gt;(SANYO)&lt;/b&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0&gt;
&lt;tr valign="top"&gt;

&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</description>
<packages>
<package name="R0402" urn="urn:adsk.eagle:footprint:23043/3" library_version="7">
<description>&lt;b&gt;Chip RESISTOR 0402 EIA (1005 Metric)&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1" y1="0.483" x2="1" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="0.483" x2="1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="-0.483" x2="-1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1" y1="-0.483" x2="-1" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.35" x2="0.1999" y2="0.35" layer="35"/>
</package>
<package name="R0603" urn="urn:adsk.eagle:footprint:23044/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805" urn="urn:adsk.eagle:footprint:23045/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W" urn="urn:adsk.eagle:footprint:23046/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206" urn="urn:adsk.eagle:footprint:23047/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W" urn="urn:adsk.eagle:footprint:23048/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210" urn="urn:adsk.eagle:footprint:23049/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W" urn="urn:adsk.eagle:footprint:23050/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010" urn="urn:adsk.eagle:footprint:23051/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W" urn="urn:adsk.eagle:footprint:23052/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012" urn="urn:adsk.eagle:footprint:23053/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W" urn="urn:adsk.eagle:footprint:23054/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512" urn="urn:adsk.eagle:footprint:23055/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W" urn="urn:adsk.eagle:footprint:23056/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216" urn="urn:adsk.eagle:footprint:23057/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W" urn="urn:adsk.eagle:footprint:23058/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225" urn="urn:adsk.eagle:footprint:23059/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W" urn="urn:adsk.eagle:footprint:23060/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025" urn="urn:adsk.eagle:footprint:23061/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W" urn="urn:adsk.eagle:footprint:23062/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332" urn="urn:adsk.eagle:footprint:23063/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W" urn="urn:adsk.eagle:footprint:25646/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805" urn="urn:adsk.eagle:footprint:23065/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206" urn="urn:adsk.eagle:footprint:23066/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406" urn="urn:adsk.eagle:footprint:23067/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012" urn="urn:adsk.eagle:footprint:23068/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309" urn="urn:adsk.eagle:footprint:23069/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216" urn="urn:adsk.eagle:footprint:23070/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516" urn="urn:adsk.eagle:footprint:23071/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923" urn="urn:adsk.eagle:footprint:23072/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5" urn="urn:adsk.eagle:footprint:22991/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7" urn="urn:adsk.eagle:footprint:22998/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V" urn="urn:adsk.eagle:footprint:22999/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/10" urn="urn:adsk.eagle:footprint:22992/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12" urn="urn:adsk.eagle:footprint:22993/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15" urn="urn:adsk.eagle:footprint:22997/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V" urn="urn:adsk.eagle:footprint:22994/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V" urn="urn:adsk.eagle:footprint:22995/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7" urn="urn:adsk.eagle:footprint:22996/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10" urn="urn:adsk.eagle:footprint:23073/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12" urn="urn:adsk.eagle:footprint:23074/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309V" urn="urn:adsk.eagle:footprint:23075/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="0411/12" urn="urn:adsk.eagle:footprint:23076/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15" urn="urn:adsk.eagle:footprint:23077/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V" urn="urn:adsk.eagle:footprint:23078/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15" urn="urn:adsk.eagle:footprint:23079/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V" urn="urn:adsk.eagle:footprint:23080/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17" urn="urn:adsk.eagle:footprint:23081/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22" urn="urn:adsk.eagle:footprint:23082/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V" urn="urn:adsk.eagle:footprint:23083/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22" urn="urn:adsk.eagle:footprint:23084/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V" urn="urn:adsk.eagle:footprint:23085/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15" urn="urn:adsk.eagle:footprint:23086/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22" urn="urn:adsk.eagle:footprint:23087/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V" urn="urn:adsk.eagle:footprint:23088/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12" urn="urn:adsk.eagle:footprint:23089/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17" urn="urn:adsk.eagle:footprint:23090/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0" urn="urn:adsk.eagle:footprint:23091/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102R" urn="urn:adsk.eagle:footprint:23092/1" library_version="7">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W" urn="urn:adsk.eagle:footprint:23093/1" library_version="7">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R" urn="urn:adsk.eagle:footprint:25676/1" library_version="7">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W" urn="urn:adsk.eagle:footprint:25677/1" library_version="7">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R" urn="urn:adsk.eagle:footprint:25678/1" library_version="7">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W" urn="urn:adsk.eagle:footprint:25679/1" library_version="7">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0922V" urn="urn:adsk.eagle:footprint:23098/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="RDH/15" urn="urn:adsk.eagle:footprint:23099/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="MINI_MELF-0102AX" urn="urn:adsk.eagle:footprint:23100/1" library_version="7">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="R0201" urn="urn:adsk.eagle:footprint:25683/1" library_version="7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VTA52" urn="urn:adsk.eagle:footprint:25684/1" library_version="7">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53" urn="urn:adsk.eagle:footprint:25685/1" library_version="7">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54" urn="urn:adsk.eagle:footprint:25686/1" library_version="7">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55" urn="urn:adsk.eagle:footprint:25687/1" library_version="7">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56" urn="urn:adsk.eagle:footprint:25688/1" library_version="7">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="VMTA55" urn="urn:adsk.eagle:footprint:25689/1" library_version="7">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60" urn="urn:adsk.eagle:footprint:25690/1" library_version="7">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="R4527" urn="urn:adsk.eagle:footprint:13246/1" library_version="7">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001" urn="urn:adsk.eagle:footprint:25692/1" library_version="7">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002" urn="urn:adsk.eagle:footprint:25693/1" library_version="7">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2" urn="urn:adsk.eagle:footprint:25694/1" library_version="7">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515" urn="urn:adsk.eagle:footprint:25695/1" library_version="7">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527" urn="urn:adsk.eagle:footprint:25696/1" library_version="7">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927" urn="urn:adsk.eagle:footprint:25697/1" library_version="7">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218" urn="urn:adsk.eagle:footprint:25698/1" library_version="7">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R" urn="urn:adsk.eagle:footprint:25699/1" library_version="7">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="PRL1632" urn="urn:adsk.eagle:footprint:25700/1" library_version="7">
<description>&lt;b&gt;PRL1632 are realized as 1W for 3.2 × 1.6mm(1206)&lt;/b&gt;&lt;p&gt;
Source: http://www.mouser.com/ds/2/392/products_18-2245.pdf</description>
<wire x1="0.7275" y1="-1.5228" x2="-0.7277" y2="-1.5228" width="0.1524" layer="51"/>
<wire x1="0.7275" y1="1.5228" x2="-0.7152" y2="1.5228" width="0.1524" layer="51"/>
<smd name="2" x="0.822" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="1" x="-0.822" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-1.4" y="1.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.4" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-1.6" x2="-0.4" y2="1.6" layer="51"/>
<rectangle x1="0.4" y1="-1.6" x2="0.8" y2="1.6" layer="51"/>
</package>
<package name="R01005" urn="urn:adsk.eagle:footprint:25701/1" library_version="7">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="R0402" urn="urn:adsk.eagle:package:23547/3" type="model" library_version="7">
<description>Chip RESISTOR 0402 EIA (1005 Metric)</description>
<packageinstances>
<packageinstance name="R0402"/>
</packageinstances>
</package3d>
<package3d name="R0603" urn="urn:adsk.eagle:package:23555/3" type="model" library_version="10">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0603"/>
</packageinstances>
</package3d>
<package3d name="R0805" urn="urn:adsk.eagle:package:23553/2" type="model" library_version="7">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0805"/>
</packageinstances>
</package3d>
<package3d name="R0805W" urn="urn:adsk.eagle:package:23537/2" type="model" library_version="7">
<description>RESISTOR wave soldering</description>
<packageinstances>
<packageinstance name="R0805W"/>
</packageinstances>
</package3d>
<package3d name="R1206" urn="urn:adsk.eagle:package:23540/2" type="model" library_version="7">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1206"/>
</packageinstances>
</package3d>
<package3d name="R1206W" urn="urn:adsk.eagle:package:23539/2" type="model" library_version="7">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R1206W"/>
</packageinstances>
</package3d>
<package3d name="R1210" urn="urn:adsk.eagle:package:23554/2" type="model" library_version="7">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1210"/>
</packageinstances>
</package3d>
<package3d name="R1210W" urn="urn:adsk.eagle:package:23541/2" type="model" library_version="7">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R1210W"/>
</packageinstances>
</package3d>
<package3d name="R2010" urn="urn:adsk.eagle:package:23551/2" type="model" library_version="7">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2010"/>
</packageinstances>
</package3d>
<package3d name="R2010W" urn="urn:adsk.eagle:package:23542/2" type="model" library_version="7">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2010W"/>
</packageinstances>
</package3d>
<package3d name="R2012" urn="urn:adsk.eagle:package:23543/2" type="model" library_version="7">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2012"/>
</packageinstances>
</package3d>
<package3d name="R2012W" urn="urn:adsk.eagle:package:23544/2" type="model" library_version="7">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2012W"/>
</packageinstances>
</package3d>
<package3d name="R2512" urn="urn:adsk.eagle:package:23545/2" type="model" library_version="7">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2512"/>
</packageinstances>
</package3d>
<package3d name="R2512W" urn="urn:adsk.eagle:package:23565/2" type="model" library_version="7">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2512W"/>
</packageinstances>
</package3d>
<package3d name="R3216" urn="urn:adsk.eagle:package:23557/2" type="model" library_version="7">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R3216"/>
</packageinstances>
</package3d>
<package3d name="R3216W" urn="urn:adsk.eagle:package:23548/2" type="model" library_version="7">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R3216W"/>
</packageinstances>
</package3d>
<package3d name="R3225" urn="urn:adsk.eagle:package:23549/2" type="model" library_version="7">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R3225"/>
</packageinstances>
</package3d>
<package3d name="R3225W" urn="urn:adsk.eagle:package:23550/2" type="model" library_version="7">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R3225W"/>
</packageinstances>
</package3d>
<package3d name="R5025" urn="urn:adsk.eagle:package:23552/2" type="model" library_version="7">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R5025"/>
</packageinstances>
</package3d>
<package3d name="R5025W" urn="urn:adsk.eagle:package:23558/2" type="model" library_version="7">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R5025W"/>
</packageinstances>
</package3d>
<package3d name="R6332" urn="urn:adsk.eagle:package:23559/2" type="model" library_version="7">
<description>RESISTOR
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<packageinstances>
<packageinstance name="R6332"/>
</packageinstances>
</package3d>
<package3d name="R6332W" urn="urn:adsk.eagle:package:26078/2" type="model" library_version="10">
<description>RESISTOR wave soldering
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<packageinstances>
<packageinstance name="R6332W"/>
</packageinstances>
</package3d>
<package3d name="M0805" urn="urn:adsk.eagle:package:23556/2" type="model" library_version="10">
<description>RESISTOR
MELF 0.10 W</description>
<packageinstances>
<packageinstance name="M0805"/>
</packageinstances>
</package3d>
<package3d name="M1206" urn="urn:adsk.eagle:package:23566/2" type="model" library_version="10">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M1206"/>
</packageinstances>
</package3d>
<package3d name="M1406" urn="urn:adsk.eagle:package:23569/2" type="model" library_version="10">
<description>RESISTOR
MELF 0.12 W</description>
<packageinstances>
<packageinstance name="M1406"/>
</packageinstances>
</package3d>
<package3d name="M2012" urn="urn:adsk.eagle:package:23561/2" type="model" library_version="7">
<description>RESISTOR
MELF 0.10 W</description>
<packageinstances>
<packageinstance name="M2012"/>
</packageinstances>
</package3d>
<package3d name="M2309" urn="urn:adsk.eagle:package:23562/2" type="model" library_version="7">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M2309"/>
</packageinstances>
</package3d>
<package3d name="M3216" urn="urn:adsk.eagle:package:23563/2" type="model" library_version="10">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M3216"/>
</packageinstances>
</package3d>
<package3d name="M3516" urn="urn:adsk.eagle:package:23573/2" type="model" library_version="10">
<description>RESISTOR
MELF 0.12 W</description>
<packageinstances>
<packageinstance name="M3516"/>
</packageinstances>
</package3d>
<package3d name="M5923" urn="urn:adsk.eagle:package:23564/3" type="model" library_version="10">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M5923"/>
</packageinstances>
</package3d>
<package3d name="0204/5" urn="urn:adsk.eagle:package:23488/1" type="box" library_version="7">
<description>RESISTOR
type 0204, grid 5 mm</description>
<packageinstances>
<packageinstance name="0204/5"/>
</packageinstances>
</package3d>
<package3d name="0204/7" urn="urn:adsk.eagle:package:23498/2" type="model" library_version="7">
<description>RESISTOR
type 0204, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0204/7"/>
</packageinstances>
</package3d>
<package3d name="0204V" urn="urn:adsk.eagle:package:23495/1" type="box" library_version="7">
<description>RESISTOR
type 0204, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0204V"/>
</packageinstances>
</package3d>
<package3d name="0207/10" urn="urn:adsk.eagle:package:23491/2" type="model" library_version="7">
<description>RESISTOR
type 0207, grid 10 mm</description>
<packageinstances>
<packageinstance name="0207/10"/>
</packageinstances>
</package3d>
<package3d name="0207/12" urn="urn:adsk.eagle:package:23489/1" type="box" library_version="7">
<description>RESISTOR
type 0207, grid 12 mm</description>
<packageinstances>
<packageinstance name="0207/12"/>
</packageinstances>
</package3d>
<package3d name="0207/15" urn="urn:adsk.eagle:package:23492/1" type="box" library_version="7">
<description>RESISTOR
type 0207, grid 15mm</description>
<packageinstances>
<packageinstance name="0207/15"/>
</packageinstances>
</package3d>
<package3d name="0207/2V" urn="urn:adsk.eagle:package:23490/1" type="box" library_version="7">
<description>RESISTOR
type 0207, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0207/2V"/>
</packageinstances>
</package3d>
<package3d name="0207/5V" urn="urn:adsk.eagle:package:23502/1" type="box" library_version="7">
<description>RESISTOR
type 0207, grid 5 mm</description>
<packageinstances>
<packageinstance name="0207/5V"/>
</packageinstances>
</package3d>
<package3d name="0207/7" urn="urn:adsk.eagle:package:23493/2" type="model" library_version="7">
<description>RESISTOR
type 0207, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0207/7"/>
</packageinstances>
</package3d>
<package3d name="0309/10" urn="urn:adsk.eagle:package:23567/2" type="model" library_version="10">
<description>RESISTOR
type 0309, grid 10mm</description>
<packageinstances>
<packageinstance name="0309/10"/>
</packageinstances>
</package3d>
<package3d name="0309/12" urn="urn:adsk.eagle:package:23571/1" type="box" library_version="7">
<description>RESISTOR
type 0309, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="0309/12"/>
</packageinstances>
</package3d>
<package3d name="0309V" urn="urn:adsk.eagle:package:23572/1" type="box" library_version="7">
<description>RESISTOR
type 0309, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0309V"/>
</packageinstances>
</package3d>
<package3d name="0411/12" urn="urn:adsk.eagle:package:23578/1" type="box" library_version="7">
<description>RESISTOR
type 0411, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="0411/12"/>
</packageinstances>
</package3d>
<package3d name="0411/15" urn="urn:adsk.eagle:package:23568/2" type="model" library_version="10">
<description>RESISTOR
type 0411, grid 15 mm</description>
<packageinstances>
<packageinstance name="0411/15"/>
</packageinstances>
</package3d>
<package3d name="0411V" urn="urn:adsk.eagle:package:23570/1" type="box" library_version="7">
<description>RESISTOR
type 0411, grid 3.81 mm</description>
<packageinstances>
<packageinstance name="0411V"/>
</packageinstances>
</package3d>
<package3d name="0414/15" urn="urn:adsk.eagle:package:23579/2" type="model" library_version="7">
<description>RESISTOR
type 0414, grid 15 mm</description>
<packageinstances>
<packageinstance name="0414/15"/>
</packageinstances>
</package3d>
<package3d name="0414V" urn="urn:adsk.eagle:package:23574/1" type="box" library_version="7">
<description>RESISTOR
type 0414, grid 5 mm</description>
<packageinstances>
<packageinstance name="0414V"/>
</packageinstances>
</package3d>
<package3d name="0617/17" urn="urn:adsk.eagle:package:23575/2" type="model" library_version="7">
<description>RESISTOR
type 0617, grid 17.5 mm</description>
<packageinstances>
<packageinstance name="0617/17"/>
</packageinstances>
</package3d>
<package3d name="0617/22" urn="urn:adsk.eagle:package:23577/1" type="box" library_version="7">
<description>RESISTOR
type 0617, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="0617/22"/>
</packageinstances>
</package3d>
<package3d name="0617V" urn="urn:adsk.eagle:package:23576/1" type="box" library_version="7">
<description>RESISTOR
type 0617, grid 5 mm</description>
<packageinstances>
<packageinstance name="0617V"/>
</packageinstances>
</package3d>
<package3d name="0922/22" urn="urn:adsk.eagle:package:23580/2" type="model" library_version="7">
<description>RESISTOR
type 0922, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="0922/22"/>
</packageinstances>
</package3d>
<package3d name="P0613V" urn="urn:adsk.eagle:package:23582/1" type="box" library_version="7">
<description>RESISTOR
type 0613, grid 5 mm</description>
<packageinstances>
<packageinstance name="P0613V"/>
</packageinstances>
</package3d>
<package3d name="P0613/15" urn="urn:adsk.eagle:package:23581/2" type="model" library_version="7">
<description>RESISTOR
type 0613, grid 15 mm</description>
<packageinstances>
<packageinstance name="P0613/15"/>
</packageinstances>
</package3d>
<package3d name="P0817/22" urn="urn:adsk.eagle:package:23583/1" type="box" library_version="7">
<description>RESISTOR
type 0817, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="P0817/22"/>
</packageinstances>
</package3d>
<package3d name="P0817V" urn="urn:adsk.eagle:package:23608/1" type="box" library_version="7">
<description>RESISTOR
type 0817, grid 6.35 mm</description>
<packageinstances>
<packageinstance name="P0817V"/>
</packageinstances>
</package3d>
<package3d name="V234/12" urn="urn:adsk.eagle:package:23592/1" type="box" library_version="7">
<description>RESISTOR
type V234, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="V234/12"/>
</packageinstances>
</package3d>
<package3d name="V235/17" urn="urn:adsk.eagle:package:23586/2" type="model" library_version="10">
<description>RESISTOR
type V235, grid 17.78 mm</description>
<packageinstances>
<packageinstance name="V235/17"/>
</packageinstances>
</package3d>
<package3d name="V526-0" urn="urn:adsk.eagle:package:23590/1" type="box" library_version="7">
<description>RESISTOR
type V526-0, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="V526-0"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102R" urn="urn:adsk.eagle:package:23591/2" type="model" library_version="7">
<description>CECC Size RC2211 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102W" urn="urn:adsk.eagle:package:23588/2" type="model" library_version="7">
<description>CECC Size RC2211 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102W"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0204R" urn="urn:adsk.eagle:package:26109/2" type="model" library_version="10">
<description>CECC Size RC3715 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0204R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0204W" urn="urn:adsk.eagle:package:26111/2" type="model" library_version="10">
<description>CECC Size RC3715 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0204W"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0207R" urn="urn:adsk.eagle:package:26113/2" type="model" library_version="10">
<description>CECC Size RC6123 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0207R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0207W" urn="urn:adsk.eagle:package:26112/2" type="model" library_version="10">
<description>CECC Size RC6123 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0207W"/>
</packageinstances>
</package3d>
<package3d name="0922V" urn="urn:adsk.eagle:package:23589/1" type="box" library_version="7">
<description>RESISTOR
type 0922, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0922V"/>
</packageinstances>
</package3d>
<package3d name="RDH/15" urn="urn:adsk.eagle:package:23595/1" type="box" library_version="7">
<description>RESISTOR
type RDH, grid 15 mm</description>
<packageinstances>
<packageinstance name="RDH/15"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102AX" urn="urn:adsk.eagle:package:23594/1" type="box" library_version="7">
<description>Mini MELF 0102 Axial</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102AX"/>
</packageinstances>
</package3d>
<package3d name="R0201" urn="urn:adsk.eagle:package:26117/2" type="model" library_version="10">
<description>RESISTOR chip
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<packageinstances>
<packageinstance name="R0201"/>
</packageinstances>
</package3d>
<package3d name="VTA52" urn="urn:adsk.eagle:package:26116/2" type="model" library_version="10">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR52
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA52"/>
</packageinstances>
</package3d>
<package3d name="VTA53" urn="urn:adsk.eagle:package:26118/2" type="model" library_version="10">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR53
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA53"/>
</packageinstances>
</package3d>
<package3d name="VTA54" urn="urn:adsk.eagle:package:26119/2" type="model" library_version="10">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR54
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA54"/>
</packageinstances>
</package3d>
<package3d name="VTA55" urn="urn:adsk.eagle:package:26120/2" type="model" library_version="10">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR55
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA55"/>
</packageinstances>
</package3d>
<package3d name="VTA56" urn="urn:adsk.eagle:package:26129/3" type="model" library_version="10">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR56
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA56"/>
</packageinstances>
</package3d>
<package3d name="VMTA55" urn="urn:adsk.eagle:package:26121/2" type="model" library_version="10">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RNC55
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VMTA55"/>
</packageinstances>
</package3d>
<package3d name="VMTB60" urn="urn:adsk.eagle:package:26122/2" type="model" library_version="10">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RNC60
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VMTB60"/>
</packageinstances>
</package3d>
<package3d name="R4527" urn="urn:adsk.eagle:package:13310/2" type="model" library_version="10">
<description>Package 4527
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<packageinstances>
<packageinstance name="R4527"/>
</packageinstances>
</package3d>
<package3d name="WSC0001" urn="urn:adsk.eagle:package:26123/2" type="model" library_version="10">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC0001"/>
</packageinstances>
</package3d>
<package3d name="WSC0002" urn="urn:adsk.eagle:package:26125/2" type="model" library_version="10">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC0002"/>
</packageinstances>
</package3d>
<package3d name="WSC01/2" urn="urn:adsk.eagle:package:26127/2" type="model" library_version="10">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC01/2"/>
</packageinstances>
</package3d>
<package3d name="WSC2515" urn="urn:adsk.eagle:package:26134/2" type="model" library_version="10">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC2515"/>
</packageinstances>
</package3d>
<package3d name="WSC4527" urn="urn:adsk.eagle:package:26126/2" type="model" library_version="10">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC4527"/>
</packageinstances>
</package3d>
<package3d name="WSC6927" urn="urn:adsk.eagle:package:26128/2" type="model" library_version="10">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC6927"/>
</packageinstances>
</package3d>
<package3d name="R1218" urn="urn:adsk.eagle:package:26131/2" type="model" library_version="10">
<description>CRCW1218 Thick Film, Rectangular Chip Resistors
Source: http://www.vishay.com .. dcrcw.pdf</description>
<packageinstances>
<packageinstance name="R1218"/>
</packageinstances>
</package3d>
<package3d name="1812X7R" urn="urn:adsk.eagle:package:26130/2" type="model" library_version="10">
<description>Chip Monolithic Ceramic Capacitors Medium Voltage High Capacitance for General Use
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<packageinstances>
<packageinstance name="1812X7R"/>
</packageinstances>
</package3d>
<package3d name="PRL1632" urn="urn:adsk.eagle:package:26132/2" type="model" library_version="10">
<description>PRL1632 are realized as 1W for 3.2 × 1.6mm(1206)
Source: http://www.mouser.com/ds/2/392/products_18-2245.pdf</description>
<packageinstances>
<packageinstance name="PRL1632"/>
</packageinstances>
</package3d>
<package3d name="R01005" urn="urn:adsk.eagle:package:26133/2" type="model" library_version="10">
<description>Chip, 0.40 X 0.20 X 0.16 mm body
&lt;p&gt;Chip package with body size 0.40 X 0.20 X 0.16 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="R01005"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="R-EU" urn="urn:adsk.eagle:symbol:23042/1" library_version="7">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-EU_" urn="urn:adsk.eagle:component:23791/20" prefix="R" uservalue="yes" library_version="10">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23547/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23553/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23537/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23540/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23539/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23554/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23541/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23551/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23542/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23543/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23544/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23545/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23565/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23557/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23548/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23549/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23550/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23552/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23558/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23559/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26078/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23556/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23566/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23569/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23561/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23562/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23563/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23573/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23564/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23488/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23498/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23495/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23491/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23489/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23492/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23490/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23502/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23493/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23567/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23571/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23572/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23578/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23568/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23570/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23579/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23574/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23575/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23577/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23576/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23580/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23582/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23581/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23583/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23608/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23592/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23586/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23590/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23591/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23588/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26109/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26111/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26113/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26112/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23589/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23595/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23594/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26117/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26116/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26118/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26119/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26120/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26129/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26121/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26122/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13310/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26123/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26125/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26127/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26134/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26126/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26128/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26131/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26130/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="PRL1632" package="PRL1632">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26132/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26133/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="blebox-dummy">
<packages>
<package name="EXTRA-BOM-LINE">
<text x="0" y="0" size="1.778" layer="48">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="48">&gt;VALUE</text>
<circle x="-2.54" y="0" radius="2.300065625" width="0.127" layer="48"/>
<wire x1="-4.064" y1="0" x2="-1.016" y2="0" width="0.127" layer="48"/>
<wire x1="-2.54" y1="1.524" x2="-2.54" y2="-1.27" width="0.127" layer="48"/>
</package>
</packages>
<symbols>
<symbol name="BOM">
<text x="2.54" y="2.54" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="2.54" y="0" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<circle x="0" y="0" radius="2.0478125" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="-2.54" size="1.778" layer="97" align="center-left">&gt;DESCRIPTION</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BOM" prefix="X" uservalue="yes">
<description>Dummy symbol for inserting extra BOM lines.</description>
<gates>
<gate name="X" symbol="BOM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EXTRA-BOM-LINE">
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-panasonic-relay">
<packages>
<package name="PA1A-PS-HORIZONTAL" urn="urn:adsk.eagle:footprint:6226245/2" locally_modified="yes">
<pad name="1" x="-8.89" y="0" drill="0.7"/>
<pad name="2" x="-6.35" y="0" drill="0.7"/>
<pad name="NO" x="8.89" y="0" drill="1"/>
<pad name="COM" x="3.81" y="0" drill="1"/>
<wire x1="-11.2" y1="19.1" x2="11.2" y2="19.1" width="0.2032" layer="21"/>
<wire x1="11.2" y1="19.1" x2="11.2" y2="1" width="0.2032" layer="21"/>
<wire x1="11.2" y1="1" x2="-11.2" y2="1" width="0.2032" layer="21"/>
<wire x1="-11.2" y1="1" x2="-11.2" y2="19.1" width="0.2032" layer="21"/>
<wire x1="11.3" y1="15.595240625" x2="11.3" y2="3.295240625" width="0.1524" layer="51"/>
<wire x1="11.3" y1="3.295240625" x2="11.3" y2="2.995240625" width="0.1524" layer="51"/>
<wire x1="11.3" y1="2.995240625" x2="11.3" y2="1.295240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="15.595240625" x2="-11.3" y2="3.295240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="3.295240625" x2="-11.3" y2="2.995240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="2.995240625" x2="-11.3" y2="1.295240625" width="0.1524" layer="51"/>
<wire x1="-9.6" y1="6.595240625" x2="-5.21" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="-5.21" y1="6.595240625" x2="-2.41" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="-2.41" y1="6.595240625" x2="4.95" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="4.95" y1="6.595240625" x2="7.75" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="7.75" y1="6.595240625" x2="9.6" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="1.295240625" x2="-10.6" y2="1.295240625" width="0.1524" layer="51"/>
<wire x1="-10.6" y1="1.595240625" x2="-10.6" y2="1.295240625" width="0.1524" layer="51"/>
<wire x1="10.6" y1="1.595240625" x2="10.6" y2="1.295240625" width="0.1524" layer="51"/>
<wire x1="10.6" y1="1.295240625" x2="11.3" y2="1.295240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="15.595240625" x2="-10.200009375" y2="15.595240625" width="0.1524" layer="51"/>
<wire x1="-10.2" y1="15.595240625" x2="-9.8" y2="14.995240625" width="0.1524" layer="51"/>
<wire x1="10.2" y1="15.595240625" x2="11.3" y2="15.595240625" width="0.1524" layer="51"/>
<wire x1="9.8" y1="14.995240625" x2="10.074571875" y2="6.60833125" width="0.1524" layer="51"/>
<wire x1="-10.074578125" y1="6.60833125" x2="-9.8" y2="14.995240625" width="0.1524" layer="51"/>
<wire x1="9.6" y1="6.595240625" x2="9.6" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="-5.11" y1="6.195240625" x2="-2.51" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="-5.21" y1="6.595240625" x2="-5.11" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="-2.51" y1="6.195240625" x2="-2.41" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="9.6" y1="6.195240625" x2="9.67478125" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="-10.6" y1="1.595240625" x2="-9.14" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="-9.14" y1="1.595240625" x2="-8.19" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="-8.19" y1="1.595240625" x2="-6.6" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="-6.6" y1="1.595240625" x2="-5.65" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="-5.65" y1="1.595240625" x2="3.41" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="3.41" y1="1.595240625" x2="4.51" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="4.51" y1="1.595240625" x2="8.49" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="8.49" y1="1.595240625" x2="9.59" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="9.59" y1="1.595240625" x2="10.6" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="-9.6" y1="6.595240625" x2="-9.6" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="-9.6747875" y1="6.195240625" x2="-9.6" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="4.95" y1="6.595240625" x2="5.05" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="7.65" y1="6.195240625" x2="7.75" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="5.05" y1="6.195240625" x2="7.65" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="3.295240625" x2="11.3" y2="3.295240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="2.995240625" x2="11.3" y2="2.995240625" width="0.1524" layer="51"/>
<wire x1="9.8" y1="14.995240625" x2="10.2" y2="15.595240625" width="0.1524" layer="51"/>
<wire x1="-8.69" y1="-2.704759375" x2="-9.09" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="-9.14" y1="1.595240625" x2="-9.14" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="-8.64" y1="1.345240625" x2="-8.64" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="-8.64" y1="-2.104759375" x2="-8.69" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="-9.09" y1="-2.704759375" x2="-9.14" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="-8.19" y1="1.595240625" x2="-8.64" y2="1.345240625" width="0.1524" layer="51"/>
<wire x1="-6.15" y1="-2.704759375" x2="-6.55" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="-6.6" y1="1.595240625" x2="-6.6" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="-6.1" y1="1.345240625" x2="-6.1" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="-6.1" y1="-2.104759375" x2="-6.15" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="-6.55" y1="-2.704759375" x2="-6.6" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="-5.65" y1="1.595240625" x2="-6.1" y2="1.345240625" width="0.1524" layer="51"/>
<wire x1="4.11" y1="-2.704759375" x2="3.51" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="3.41" y1="1.595240625" x2="3.41" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="4.21" y1="1.345240625" x2="4.21" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="4.21" y1="-2.104759375" x2="4.11" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="3.509990625" y1="-2.704759375" x2="3.41" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="4.51" y1="1.595240625" x2="4.21" y2="1.345240625" width="0.1524" layer="51"/>
<wire x1="9.19" y1="-2.704759375" x2="8.59" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="8.49" y1="1.595240625" x2="8.49" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="9.29" y1="1.345240625" x2="9.29" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="9.29" y1="-2.104759375" x2="9.19" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="8.59" y1="-2.704759375" x2="8.49" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="9.59" y1="1.595240625" x2="9.29" y2="1.345240625" width="0.1524" layer="51"/>
<wire x1="9.67478125" y1="6.195240625" x2="10.074565625" y2="6.60833125" width="0.1524" layer="51" curve="91.875435"/>
<wire x1="-10.07456875" y1="6.60833125" x2="-9.6747875" y2="6.195240625" width="0.1524" layer="51" curve="91.874988"/>
<text x="-12" y="2" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="13.0266" y="2.0984" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<circle x="11" y="5" radius="1" width="0" layer="35"/>
<circle x="-11" y="5" radius="1" width="0" layer="35"/>
</package>
<package name="PA1A-PS-VERTICAL" urn="urn:adsk.eagle:footprint:6395346/2" locally_modified="yes">
<pad name="1" x="-8.89" y="0" drill="0.7"/>
<pad name="2" x="-6.35" y="0" drill="0.7"/>
<pad name="NO" x="8.89" y="0" drill="1"/>
<pad name="COM" x="3.81" y="0" drill="1"/>
<wire x1="11.3" y1="-3.77" x2="-11.3" y2="-3.77" width="0.2032" layer="21"/>
<wire x1="11.3" y1="1.23" x2="-11.3" y2="1.23" width="0.2032" layer="21"/>
<wire x1="-11.3" y1="1.23" x2="-11.3" y2="-3.77" width="0.2032" layer="21"/>
<wire x1="11.3" y1="1.23" x2="11.3" y2="-3.77" width="0.2032" layer="21"/>
<text x="-11" y="1.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-11" y="-4" size="1.27" layer="27" align="top-left">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="PA1A-PS-HORIZONTAL" urn="urn:adsk.eagle:package:6226248/5" locally_modified="yes" type="model">
<packageinstances>
<packageinstance name="PA1A-PS-HORIZONTAL"/>
</packageinstances>
</package3d>
<package3d name="PA1A-PS-VERTICAL" urn="urn:adsk.eagle:package:6395349/4" locally_modified="yes" type="model">
<packageinstances>
<packageinstance name="PA1A-PS-VERTICAL"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="K">
<wire x1="-3.81" y1="-1.905" x2="-1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.905" x2="1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="1.905" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<text x="1.27" y="2.921" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.27" y="5.08" size="1.778" layer="95">&gt;PART</text>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<text x="-2.54" y="-2.54" size="1.27" layer="94" rot="R180">(+)</text>
</symbol>
<symbol name="S">
<wire x1="0" y1="3.175" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-3.175" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="95" rot="R90">&gt;PART</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="0.762" y="3.81" size="1.27" layer="94" rot="R270">(+)</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PA1A-PS" prefix="REL">
<gates>
<gate name="K" symbol="K" x="-7.62" y="0"/>
<gate name="S" symbol="S" x="7.62" y="0"/>
</gates>
<devices>
<device name="(HORIZONTAL)" package="PA1A-PS-HORIZONTAL">
<connects>
<connect gate="K" pin="1" pad="1"/>
<connect gate="K" pin="2" pad="2"/>
<connect gate="S" pin="P" pad="COM"/>
<connect gate="S" pin="S" pad="NO"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6226248/5"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Relay socket" constant="no"/>
<attribute name="MF" value="PANASONIC"/>
<attribute name="MPN" value="PA1A-PS" constant="no"/>
</technology>
</technologies>
</device>
<device name="(VERTICAL)" package="PA1A-PS-VERTICAL">
<connects>
<connect gate="K" pin="1" pad="1"/>
<connect gate="K" pin="2" pad="2"/>
<connect gate="S" pin="P" pad="COM"/>
<connect gate="S" pin="S" pad="NO"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6395349/4"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Relay socket" constant="no"/>
<attribute name="MF" value="PANASONIC" constant="no"/>
<attribute name="MPN" value="PA1A-PS" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-diode">
<packages>
<package name="MINIMELF" urn="urn:adsk.eagle:footprint:43207/1">
<description>&lt;b&gt;Mini Melf Diode&lt;/b&gt;</description>
<wire x1="1.3208" y1="0.7874" x2="-1.3208" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.7874" x2="-1.3208" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="0.5" y1="0.5" x2="-0.5" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-0.5" x2="0.5" y2="0.5" width="0.2032" layer="21"/>
<smd name="C" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="A" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8636" x2="-1.2954" y2="0.8636" layer="51"/>
<rectangle x1="1.2954" y1="-0.8636" x2="1.8542" y2="0.8636" layer="51"/>
<rectangle x1="-0.8636" y1="-0.7874" x2="-0.254" y2="0.7874" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="MINIMELF" urn="urn:adsk.eagle:package:7567381/2" type="model">
<description>Mini Melf Diode</description>
<packageinstances>
<packageinstance name="MINIMELF"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
<text x="2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LL4148" prefix="D">
<gates>
<gate name="D" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MINIMELF">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7567381/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Diode, generic, 100V/0.15A, MiniMELF"/>
<attribute name="MF" value="any" constant="no"/>
<attribute name="MPN" value="LL4148" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-finder-relay">
<packages>
<package name="93.11" urn="urn:adsk.eagle:footprint:9304451/2">
<description>FINDER 93.11 SOCKET</description>
<pad name="12" x="0.9" y="1.2" drill="1.3"/>
<pad name="11" x="5.9" y="1.2" drill="1.3"/>
<pad name="14" x="10.9" y="1.2" drill="1.3"/>
<pad name="A1" x="-10.2" y="1.2" drill="1.3"/>
<pad name="A2" x="-13.95" y="1.2" drill="1.3"/>
<wire x1="-16.5" y1="3.1" x2="16.5" y2="3.1" width="0.254" layer="21"/>
<wire x1="16.5" y1="3.1" x2="16.5" y2="-3.1" width="0.254" layer="21"/>
<wire x1="16.5" y1="-3.1" x2="-16.5" y2="-3.1" width="0.254" layer="21"/>
<wire x1="-16.5" y1="-3.1" x2="-16.5" y2="3.1" width="0.254" layer="21"/>
<text x="-16.5" y="3.75" size="1" layer="25" ratio="20">&gt;NAME</text>
<text x="16.5" y="3.75" size="1" layer="27" ratio="20" align="bottom-right">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="93.11" urn="urn:adsk.eagle:package:9304454/3" type="model">
<description>FINDER 93.11 SOCKET</description>
<packageinstances>
<packageinstance name="93.11"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="K+-">
<wire x1="-3.81" y1="-1.905" x2="-1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.905" x2="1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="1.905" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.016" y1="2.667" x2="-0.508" y2="2.667" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="2.921" x2="-0.762" y2="2.413" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.667" x2="-0.508" y2="-2.667" width="0.1524" layer="94"/>
<text x="1.27" y="2.921" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.27" y="5.08" size="1.778" layer="95">&gt;PART</text>
<pin name="-" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="+" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="U">
<wire x1="3.175" y1="5.08" x2="1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.175" y1="5.08" x2="-1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="2.54" y2="5.715" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<circle x="0" y="1.27" radius="0.127" width="0.4064" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;PART</text>
<pin name="NC" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="NO" x="-5.08" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="COM" x="0" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="93.11" prefix="REL">
<gates>
<gate name="K" symbol="K+-" x="0" y="0" addlevel="must"/>
<gate name="U" symbol="U" x="17.78" y="-2.54" addlevel="must"/>
</gates>
<devices>
<device name="" package="93.11">
<connects>
<connect gate="K" pin="+" pad="A1"/>
<connect gate="K" pin="-" pad="A2"/>
<connect gate="U" pin="COM" pad="11"/>
<connect gate="U" pin="NC" pad="12"/>
<connect gate="U" pin="NO" pad="14"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9304454/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Finder relay socket for 34 series relays"/>
<attribute name="MF" value="FINDER"/>
<attribute name="MPN" value="93.11"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="LOGO1" library="udo-logo" deviceset="UDO-LOGO-" device="10MM" package3d_urn="urn:adsk.eagle:package:6649258/2"/>
<part name="DUMMY8" library="udo-dummy" deviceset="ARROW" device="" value="12. FZ1-KP2 (4 wire)"/>
<part name="DUMMY9" library="udo-dummy" deviceset="ARROW" device="" value="13. FZ1-KP2 (4 wire)"/>
<part name="DUMMY11" library="udo-dummy" deviceset="ARROW" device="" value="15. Pressure 1 (4 wire)"/>
<part name="DUMMY12" library="udo-dummy" deviceset="ARROW" device="" value="16. Pressure 2 (4 wire)"/>
<part name="DUMMY7" library="udo-dummy" deviceset="ARROW" device="" value="11. Chest closed (3 wire)"/>
<part name="DUMMY6" library="udo-dummy" deviceset="ARROW" device="" value="10. Retracted (3 wire)"/>
<part name="DUMMY4" library="udo-dummy" deviceset="ARROW" device="" value="9. Extended (3 wire)"/>
<part name="DUMMY2" library="udo-dummy" deviceset="ARROW" device="" value="8. Shaker up (3 wire)"/>
<part name="DUMMY5" library="udo-dummy" deviceset="ARROW" device="" value="5. Valve 21 (Shaker retr)"/>
<part name="DUMMY3" library="udo-dummy" deviceset="ARROW" device="" value="4. Valve 12 (Port Ext)"/>
<part name="DUMMY1" library="udo-dummy" deviceset="ARROW" device="" value="3. Valve 11 (Port Retr)"/>
<part name="CN1" library="udo-wago-mcs" deviceset="733-372" device="" package3d_urn="urn:adsk.eagle:package:8463460/2"/>
<part name="CN3" library="udo-wago-mcs" deviceset="733-368" device="" package3d_urn="urn:adsk.eagle:package:10357097/2"/>
<part name="CN5" library="udo-wago-mcs" deviceset="733-368" device="" package3d_urn="urn:adsk.eagle:package:10357097/2"/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_P" device=""/>
<part name="FRAME2" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_P" device=""/>
<part name="REL3" library="udo-panasonic-relay" deviceset="PA1A-PS" device="(VERTICAL)" package3d_urn="urn:adsk.eagle:package:6395349/4" value="PA1A-PS"/>
<part name="X10" library="blebox-dummy" deviceset="BOM" device="" value="APAN3124">
<attribute name="DESCRIPTION" value="Form A relay, 24V, 5A"/>
<attribute name="MF" value="PANASONIC"/>
<attribute name="MPN" value="APAN3124"/>
</part>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="REL2" library="udo-panasonic-relay" deviceset="PA1A-PS" device="(VERTICAL)" package3d_urn="urn:adsk.eagle:package:6395349/4" value="PA1A-PS"/>
<part name="X9" library="blebox-dummy" deviceset="BOM" device="" value="APAN3124">
<attribute name="DESCRIPTION" value="Form A relay, 24V, 5A"/>
<attribute name="MF" value="PANASONIC"/>
<attribute name="MPN" value="APAN3124"/>
</part>
<part name="D2" library="udo-diode" deviceset="LL4148" device="" package3d_urn="urn:adsk.eagle:package:7567381/2"/>
<part name="D3" library="udo-diode" deviceset="LL4148" device="" package3d_urn="urn:adsk.eagle:package:7567381/2"/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+24V" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="REL1" library="udo-finder-relay" deviceset="93.11" device="" package3d_urn="urn:adsk.eagle:package:9304454/3"/>
<part name="X8" library="udo-dummy" deviceset="BOM" device="" value="34.51.7.024.0010">
<attribute name="DESCRIPTION" value="Finder relay, 24V, SPDT"/>
<attribute name="MF" value="FINDER"/>
<attribute name="MPN" value="34.51.7.024.0010"/>
</part>
<part name="D1" library="udo-diode" deviceset="LL4148" device="" package3d_urn="urn:adsk.eagle:package:7567381/2"/>
<part name="R1" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0805" package3d_urn="urn:adsk.eagle:package:23553/2" value="DNP"/>
<part name="REL5" library="udo-panasonic-relay" deviceset="PA1A-PS" device="(VERTICAL)" package3d_urn="urn:adsk.eagle:package:6395349/4" value="PA1A-PS"/>
<part name="X12" library="blebox-dummy" deviceset="BOM" device="" value="APAN3124">
<attribute name="DESCRIPTION" value="Form A relay, 24V, 5A"/>
<attribute name="MF" value="PANASONIC"/>
<attribute name="MPN" value="APAN3124"/>
</part>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="REL4" library="udo-panasonic-relay" deviceset="PA1A-PS" device="(VERTICAL)" package3d_urn="urn:adsk.eagle:package:6395349/4" value="PA1A-PS"/>
<part name="X11" library="blebox-dummy" deviceset="BOM" device="" value="APAN3124">
<attribute name="DESCRIPTION" value="Form A relay, 24V, 5A"/>
<attribute name="MF" value="PANASONIC"/>
<attribute name="MPN" value="APAN3124"/>
</part>
<part name="D4" library="udo-diode" deviceset="LL4148" device="" package3d_urn="urn:adsk.eagle:package:7567381/2"/>
<part name="D5" library="udo-diode" deviceset="LL4148" device="" package3d_urn="urn:adsk.eagle:package:7567381/2"/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="REL7" library="udo-panasonic-relay" deviceset="PA1A-PS" device="(VERTICAL)" package3d_urn="urn:adsk.eagle:package:6395349/4" value="PA1A-PS"/>
<part name="X14" library="blebox-dummy" deviceset="BOM" device="" value="APAN3124">
<attribute name="DESCRIPTION" value="Form A relay, 24V, 5A"/>
<attribute name="MF" value="PANASONIC"/>
<attribute name="MPN" value="APAN3124"/>
</part>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="REL6" library="udo-panasonic-relay" deviceset="PA1A-PS" device="(VERTICAL)" package3d_urn="urn:adsk.eagle:package:6395349/4" value="PA1A-PS"/>
<part name="X13" library="blebox-dummy" deviceset="BOM" device="" value="APAN3124">
<attribute name="DESCRIPTION" value="Form A relay, 24V, 5A"/>
<attribute name="MF" value="PANASONIC"/>
<attribute name="MPN" value="APAN3124"/>
</part>
<part name="D6" library="udo-diode" deviceset="LL4148" device="" package3d_urn="urn:adsk.eagle:package:7567381/2"/>
<part name="D7" library="udo-diode" deviceset="LL4148" device="" package3d_urn="urn:adsk.eagle:package:7567381/2"/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="X2" library="udo-dummy" deviceset="BOM" device="" value="733-112">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 12-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-112"/>
</part>
<part name="X5" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X6" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X1" library="udo-dummy" deviceset="BOM" device="" value="733-106">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 6-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-106"/>
</part>
<part name="CN2" library="udo-wago-mcs" deviceset="733-368" device="" package3d_urn="urn:adsk.eagle:package:10357097/2"/>
<part name="CN4" library="udo-wago-mcs" deviceset="733-368" device="" package3d_urn="urn:adsk.eagle:package:10357097/2"/>
<part name="DUMMY10" library="udo-dummy" deviceset="ARROW" device="" value="UD-C26-EZI"/>
<part name="X4" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X7" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X3" library="udo-wago-mcs" deviceset="733-366" device="" package3d_urn="urn:adsk.eagle:package:10355780/2"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="LOGO1" gate="LOGO" x="159.512" y="23.114" smashed="yes"/>
<instance part="DUMMY8" gate="G$1" x="20.32" y="167.64" smashed="yes">
<attribute name="VALUE" x="20.32" y="170.18" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="DUMMY9" gate="G$1" x="20.32" y="147.32" smashed="yes">
<attribute name="VALUE" x="20.32" y="149.86" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="DUMMY11" gate="G$1" x="20.32" y="114.3" smashed="yes">
<attribute name="VALUE" x="20.32" y="116.84" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="DUMMY12" gate="G$1" x="20.32" y="93.98" smashed="yes">
<attribute name="VALUE" x="20.32" y="96.52" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="DUMMY7" gate="G$1" x="20.32" y="195.58" smashed="yes">
<attribute name="VALUE" x="20.32" y="198.12" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="DUMMY6" gate="G$1" x="20.32" y="210.82" smashed="yes">
<attribute name="VALUE" x="20.32" y="213.36" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="DUMMY4" gate="G$1" x="20.32" y="226.06" smashed="yes">
<attribute name="VALUE" x="20.32" y="228.6" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="DUMMY2" gate="G$1" x="20.32" y="241.3" smashed="yes">
<attribute name="VALUE" x="20.32" y="243.84" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="DUMMY5" gate="G$1" x="162.56" y="226.06" smashed="yes">
<attribute name="VALUE" x="162.56" y="228.6" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="DUMMY3" gate="G$1" x="162.56" y="236.22" smashed="yes">
<attribute name="VALUE" x="162.56" y="238.76" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="DUMMY1" gate="G$1" x="162.56" y="246.38" smashed="yes">
<attribute name="VALUE" x="162.56" y="248.92" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="CN1" gate="CN" x="35.56" y="218.44" smashed="yes" rot="R180">
<attribute name="NAME" x="40.64" y="254" size="2" layer="95" font="vector" ratio="10" rot="R180"/>
<attribute name="VALUE" x="40.64" y="251.46" size="2" layer="96" font="vector" ratio="10" rot="R180"/>
</instance>
<instance part="CN3" gate="CN" x="35.56" y="157.48" smashed="yes" rot="R180">
<attribute name="NAME" x="40.64" y="182.88" size="2" layer="95" font="vector" ratio="10" rot="R180"/>
<attribute name="VALUE" x="40.64" y="180.34" size="2" layer="96" font="vector" ratio="10" rot="R180"/>
</instance>
<instance part="CN5" gate="CN" x="35.56" y="104.14" smashed="yes" rot="R180">
<attribute name="NAME" x="40.64" y="129.54" size="2" layer="95" font="vector" ratio="10" rot="R180"/>
<attribute name="VALUE" x="40.64" y="127" size="2" layer="96" font="vector" ratio="10" rot="R180"/>
</instance>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME1" gate="G$2" x="78.74" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="91.44" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="165.1" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="96.52" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="X2" gate="X" x="45.72" y="254" smashed="yes">
<attribute name="NAME" x="48.26" y="256.54" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="48.26" y="254" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="48.26" y="251.46" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="45.72" y="254" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="45.72" y="254" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X5" gate="X" x="45.72" y="182.88" smashed="yes">
<attribute name="NAME" x="48.26" y="185.42" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="48.26" y="182.88" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="48.26" y="180.34" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="45.72" y="182.88" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="45.72" y="182.88" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X6" gate="X" x="45.72" y="129.54" smashed="yes">
<attribute name="NAME" x="48.26" y="132.08" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="48.26" y="129.54" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="48.26" y="127" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="45.72" y="129.54" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="45.72" y="129.54" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X1" gate="X" x="137.16" y="256.54" smashed="yes" rot="MR0">
<attribute name="NAME" x="134.62" y="259.08" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="134.62" y="256.54" size="1.778" layer="96" rot="MR0" align="center-left"/>
<attribute name="DESCRIPTION" x="134.62" y="254" size="1.778" layer="97" rot="MR0" align="center-left"/>
<attribute name="MF" x="137.16" y="256.54" size="1.27" layer="96" rot="MR0" display="off"/>
<attribute name="MPN" x="137.16" y="256.54" size="1.27" layer="96" rot="MR0" display="off"/>
</instance>
<instance part="CN2" gate="CN" x="147.32" y="160.02" smashed="yes" rot="MR180">
<attribute name="NAME" x="142.24" y="185.42" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="142.24" y="182.88" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN4" gate="CN" x="147.32" y="111.76" smashed="yes" rot="MR180">
<attribute name="NAME" x="142.24" y="88.9" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="142.24" y="86.36" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="DUMMY10" gate="G$1" x="162.56" y="134.62" smashed="yes">
<attribute name="VALUE" x="162.56" y="137.16" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="X4" gate="X" x="137.16" y="185.42" smashed="yes" rot="MR0">
<attribute name="NAME" x="134.62" y="187.96" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="134.62" y="185.42" size="1.778" layer="96" rot="MR0" align="center-left"/>
<attribute name="DESCRIPTION" x="134.62" y="182.88" size="1.778" layer="97" rot="MR0" align="center-left"/>
<attribute name="MF" x="137.16" y="185.42" size="1.27" layer="96" rot="MR0" display="off"/>
<attribute name="MPN" x="137.16" y="185.42" size="1.27" layer="96" rot="MR0" display="off"/>
</instance>
<instance part="X7" gate="X" x="137.16" y="86.36" smashed="yes" rot="MR0">
<attribute name="NAME" x="134.62" y="88.9" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="134.62" y="86.36" size="1.778" layer="96" rot="MR0" align="center-left"/>
<attribute name="DESCRIPTION" x="134.62" y="83.82" size="1.778" layer="97" rot="MR0" align="center-left"/>
<attribute name="MF" x="137.16" y="86.36" size="1.27" layer="96" rot="MR0" display="off"/>
<attribute name="MPN" x="137.16" y="86.36" size="1.27" layer="96" rot="MR0" display="off"/>
</instance>
<instance part="X3" gate="G$1" x="147.32" y="236.22" smashed="yes" rot="MR180">
<attribute name="NAME" x="142.24" y="256.54" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="142.24" y="254" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="+24V" class="0">
<segment>
<pinref part="CN1" gate="CN" pin="P1"/>
<wire x1="38.1" y1="246.38" x2="45.72" y2="246.38" width="0.1524" layer="91"/>
<label x="45.72" y="246.38" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="P4"/>
<wire x1="38.1" y1="231.14" x2="45.72" y2="231.14" width="0.1524" layer="91"/>
<label x="45.72" y="231.14" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="P7"/>
<wire x1="38.1" y1="215.9" x2="45.72" y2="215.9" width="0.1524" layer="91"/>
<label x="45.72" y="215.9" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="P10"/>
<wire x1="38.1" y1="200.66" x2="45.72" y2="200.66" width="0.1524" layer="91"/>
<label x="45.72" y="200.66" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="38.1" y1="160.02" x2="45.72" y2="160.02" width="0.1524" layer="91"/>
<label x="45.72" y="160.02" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN3" gate="CN" pin="P4"/>
</segment>
<segment>
<wire x1="38.1" y1="121.92" x2="45.72" y2="121.92" width="0.1524" layer="91"/>
<label x="45.72" y="121.92" size="1.27" layer="95" xref="yes"/>
<pinref part="CN5" gate="CN" pin="P1"/>
</segment>
<segment>
<wire x1="38.1" y1="139.7" x2="45.72" y2="139.7" width="0.1524" layer="91"/>
<label x="45.72" y="139.7" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN3" gate="CN" pin="P8"/>
</segment>
<segment>
<wire x1="38.1" y1="101.6" x2="45.72" y2="101.6" width="0.1524" layer="91"/>
<label x="45.72" y="101.6" size="1.27" layer="95" xref="yes"/>
<pinref part="CN5" gate="CN" pin="P5"/>
</segment>
<segment>
<wire x1="144.78" y1="99.06" x2="137.16" y2="99.06" width="0.1524" layer="91"/>
<label x="137.16" y="99.06" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN4" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_SHAKER_UP" class="0">
<segment>
<pinref part="CN1" gate="CN" pin="P2"/>
<wire x1="38.1" y1="241.3" x2="45.72" y2="241.3" width="0.1524" layer="91"/>
<label x="45.72" y="241.3" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="144.78" y1="129.54" x2="137.16" y2="129.54" width="0.1524" layer="91"/>
<label x="137.16" y="129.54" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN4" gate="CN" pin="P1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="CN1" gate="CN" pin="P3"/>
<wire x1="38.1" y1="236.22" x2="45.72" y2="236.22" width="0.1524" layer="91"/>
<label x="45.72" y="236.22" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="P6"/>
<wire x1="38.1" y1="220.98" x2="45.72" y2="220.98" width="0.1524" layer="91"/>
<label x="45.72" y="220.98" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="P9"/>
<wire x1="38.1" y1="205.74" x2="45.72" y2="205.74" width="0.1524" layer="91"/>
<label x="45.72" y="205.74" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="P12"/>
<wire x1="38.1" y1="190.5" x2="45.72" y2="190.5" width="0.1524" layer="91"/>
<label x="45.72" y="190.5" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="38.1" y1="175.26" x2="45.72" y2="175.26" width="0.1524" layer="91"/>
<label x="45.72" y="175.26" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN3" gate="CN" pin="P1"/>
</segment>
<segment>
<wire x1="38.1" y1="154.94" x2="45.72" y2="154.94" width="0.1524" layer="91"/>
<label x="45.72" y="154.94" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN3" gate="CN" pin="P5"/>
</segment>
<segment>
<wire x1="38.1" y1="106.68" x2="45.72" y2="106.68" width="0.1524" layer="91"/>
<label x="45.72" y="106.68" size="1.27" layer="95" xref="yes"/>
<pinref part="CN5" gate="CN" pin="P4"/>
</segment>
<segment>
<wire x1="38.1" y1="86.36" x2="45.72" y2="86.36" width="0.1524" layer="91"/>
<label x="45.72" y="86.36" size="1.27" layer="95" xref="yes"/>
<pinref part="CN5" gate="CN" pin="P8"/>
</segment>
<segment>
<wire x1="144.78" y1="243.84" x2="137.16" y2="243.84" width="0.1524" layer="91"/>
<label x="137.16" y="243.84" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="X3" gate="G$1" pin="P2"/>
</segment>
<segment>
<wire x1="144.78" y1="233.68" x2="137.16" y2="233.68" width="0.1524" layer="91"/>
<label x="137.16" y="233.68" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="X3" gate="G$1" pin="P4"/>
</segment>
<segment>
<wire x1="144.78" y1="223.52" x2="137.16" y2="223.52" width="0.1524" layer="91"/>
<label x="137.16" y="223.52" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="X3" gate="G$1" pin="P6"/>
</segment>
<segment>
<wire x1="137.16" y1="93.98" x2="144.78" y2="93.98" width="0.1524" layer="91"/>
<label x="137.16" y="93.98" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN4" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_PORT_EXTENDED" class="0">
<segment>
<pinref part="CN1" gate="CN" pin="P5"/>
<wire x1="38.1" y1="226.06" x2="45.72" y2="226.06" width="0.1524" layer="91"/>
<label x="45.72" y="226.06" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="137.16" y1="142.24" x2="144.78" y2="142.24" width="0.1524" layer="91"/>
<label x="137.16" y="142.24" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN2" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_PORT_RETRACTED" class="0">
<segment>
<pinref part="CN1" gate="CN" pin="P8"/>
<wire x1="38.1" y1="210.82" x2="45.72" y2="210.82" width="0.1524" layer="91"/>
<label x="45.72" y="210.82" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="144.78" y1="147.32" x2="137.16" y2="147.32" width="0.1524" layer="91"/>
<label x="137.16" y="147.32" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN2" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_CHEST_CLOSED" class="0">
<segment>
<pinref part="CN1" gate="CN" pin="P11"/>
<wire x1="38.1" y1="195.58" x2="45.72" y2="195.58" width="0.1524" layer="91"/>
<label x="45.72" y="195.58" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="144.78" y1="152.4" x2="137.16" y2="152.4" width="0.1524" layer="91"/>
<label x="137.16" y="152.4" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN2" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_IN_PRESSURE2" class="0">
<segment>
<wire x1="38.1" y1="96.52" x2="45.72" y2="96.52" width="0.1524" layer="91"/>
<label x="45.72" y="96.52" size="1.27" layer="95" xref="yes"/>
<pinref part="CN5" gate="CN" pin="P6"/>
</segment>
<segment>
<wire x1="144.78" y1="177.8" x2="137.16" y2="177.8" width="0.1524" layer="91"/>
<label x="137.16" y="177.8" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN2" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_IN_BOX1" class="0">
<segment>
<wire x1="38.1" y1="170.18" x2="45.72" y2="170.18" width="0.1524" layer="91"/>
<label x="45.72" y="170.18" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN3" gate="CN" pin="P2"/>
</segment>
<segment>
<wire x1="144.78" y1="157.48" x2="137.16" y2="157.48" width="0.1524" layer="91"/>
<label x="137.16" y="157.48" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN2" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_BOX2" class="0">
<segment>
<wire x1="38.1" y1="149.86" x2="45.72" y2="149.86" width="0.1524" layer="91"/>
<label x="45.72" y="149.86" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN3" gate="CN" pin="P6"/>
</segment>
<segment>
<wire x1="144.78" y1="162.56" x2="137.16" y2="162.56" width="0.1524" layer="91"/>
<label x="137.16" y="162.56" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN2" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_PRESSURE1" class="0">
<segment>
<wire x1="38.1" y1="116.84" x2="45.72" y2="116.84" width="0.1524" layer="91"/>
<label x="45.72" y="116.84" size="1.27" layer="95" xref="yes"/>
<pinref part="CN5" gate="CN" pin="P2"/>
</segment>
<segment>
<wire x1="144.78" y1="172.72" x2="137.16" y2="172.72" width="0.1524" layer="91"/>
<label x="137.16" y="172.72" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN2" gate="CN" pin="P2"/>
</segment>
</net>
<net name="VALVE_21" class="0">
<segment>
<wire x1="144.78" y1="228.6" x2="137.16" y2="228.6" width="0.1524" layer="91"/>
<label x="137.16" y="228.6" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="X3" gate="G$1" pin="P5"/>
</segment>
</net>
<net name="VALVE_12" class="0">
<segment>
<wire x1="144.78" y1="238.76" x2="137.16" y2="238.76" width="0.1524" layer="91"/>
<label x="137.16" y="238.76" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="X3" gate="G$1" pin="P3"/>
</segment>
</net>
<net name="VALVE_11" class="0">
<segment>
<wire x1="137.16" y1="248.92" x2="144.78" y2="248.92" width="0.1524" layer="91"/>
<label x="137.16" y="248.92" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="X3" gate="G$1" pin="P1"/>
</segment>
</net>
<net name="SIG_OUT_VALVE11" class="0">
<segment>
<wire x1="144.78" y1="104.14" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<label x="137.16" y="104.14" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN4" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_VALVE12" class="0">
<segment>
<wire x1="144.78" y1="109.22" x2="137.16" y2="109.22" width="0.1524" layer="91"/>
<label x="137.16" y="109.22" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN4" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_OUT_VALVE21" class="0">
<segment>
<wire x1="144.78" y1="114.3" x2="137.16" y2="114.3" width="0.1524" layer="91"/>
<label x="137.16" y="114.3" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN4" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="144.78" y1="119.38" x2="137.16" y2="119.38" width="0.1524" layer="91"/>
<label x="137.16" y="119.38" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN4" gate="CN" pin="P3"/>
</segment>
</net>
<net name="+24V/2" class="0">
<segment>
<wire x1="144.78" y1="167.64" x2="137.16" y2="167.64" width="0.1524" layer="91"/>
<label x="137.16" y="167.64" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN2" gate="CN" pin="P3"/>
</segment>
</net>
<net name="+24V/1" class="0">
<segment>
<wire x1="144.78" y1="124.46" x2="137.16" y2="124.46" width="0.1524" layer="91"/>
<label x="137.16" y="124.46" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN4" gate="CN" pin="P2"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<wire x1="91.44" y1="170.18" x2="101.6" y2="170.18" width="0.1524" layer="94" style="shortdash"/>
<wire x1="91.44" y1="195.58" x2="101.6" y2="195.58" width="0.1524" layer="94" style="shortdash"/>
<text x="124.46" y="177.8" size="1.27" layer="97">Power: 23mA</text>
<wire x1="91.44" y1="114.3" x2="101.6" y2="114.3" width="0.1524" layer="94" style="shortdash"/>
<wire x1="91.44" y1="139.7" x2="101.6" y2="139.7" width="0.1524" layer="94" style="shortdash"/>
<wire x1="91.44" y1="58.42" x2="101.6" y2="58.42" width="0.1524" layer="94" style="shortdash"/>
<wire x1="91.44" y1="83.82" x2="101.6" y2="83.82" width="0.1524" layer="94" style="shortdash"/>
<text x="124.46" y="121.92" size="1.27" layer="97">Power: 23mA</text>
<text x="124.46" y="66.04" size="1.27" layer="97">Power: 23mA</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME2" gate="G$2" x="78.74" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="91.44" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="165.1" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="96.52" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="REL3" gate="K" x="86.36" y="170.18" smashed="yes" rot="MR180">
<attribute name="VALUE" x="87.63" y="162.179" size="1.778" layer="96" rot="MR180"/>
<attribute name="PART" x="87.63" y="165.1" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="REL3" gate="S" x="101.6" y="170.18" smashed="yes" rot="MR0">
<attribute name="PART" x="104.14" y="167.64" size="1.778" layer="95" rot="MR90"/>
</instance>
<instance part="X10" gate="X" x="68.58" y="170.18" smashed="yes" rot="MR0">
<attribute name="NAME" x="66.04" y="172.72" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="66.04" y="170.18" size="1.778" layer="96" rot="MR0" align="center-left"/>
<attribute name="DESCRIPTION" x="66.04" y="167.64" size="1.778" layer="97" rot="MR0" align="center-left"/>
<attribute name="MF" x="68.58" y="170.18" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
<attribute name="MPN" x="68.58" y="170.18" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
</instance>
<instance part="GND5" gate="1" x="116.84" y="157.48" smashed="yes" rot="MR0">
<attribute name="VALUE" x="119.38" y="154.94" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="REL2" gate="K" x="86.36" y="195.58" smashed="yes" rot="MR180">
<attribute name="VALUE" x="87.63" y="187.579" size="1.778" layer="96" rot="MR180"/>
<attribute name="PART" x="87.63" y="190.5" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="REL2" gate="S" x="101.6" y="195.58" smashed="yes" rot="MR0">
<attribute name="PART" x="104.14" y="193.04" size="1.778" layer="95" rot="MR90"/>
</instance>
<instance part="X9" gate="X" x="68.58" y="195.58" smashed="yes" rot="MR0">
<attribute name="NAME" x="66.04" y="198.12" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="66.04" y="195.58" size="1.778" layer="96" rot="MR0" align="center-left"/>
<attribute name="DESCRIPTION" x="66.04" y="193.04" size="1.778" layer="97" rot="MR0" align="center-left"/>
<attribute name="MF" x="68.58" y="195.58" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
<attribute name="MPN" x="68.58" y="195.58" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
</instance>
<instance part="D2" gate="D" x="68.58" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="60.96" y="185.42" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="63.5" y="185.42" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="D3" gate="D" x="116.84" y="167.64" smashed="yes" rot="R90">
<attribute name="NAME" x="110.998" y="170.18" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="113.284" y="170.18" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND2" gate="1" x="86.36" y="182.88" smashed="yes" rot="MR0">
<attribute name="VALUE" x="88.9" y="180.34" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND4" gate="1" x="86.36" y="157.48" smashed="yes" rot="MR0">
<attribute name="VALUE" x="88.9" y="154.94" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND3" gate="1" x="68.58" y="175.26" smashed="yes" rot="MR0">
<attribute name="VALUE" x="71.12" y="172.72" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="P+1" gate="1" x="101.6" y="251.46" smashed="yes" rot="MR0">
<attribute name="VALUE" x="104.14" y="246.38" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="GND1" gate="1" x="83.82" y="220.98" smashed="yes">
<attribute name="VALUE" x="81.28" y="218.44" size="1.778" layer="96"/>
</instance>
<instance part="REL1" gate="K" x="83.82" y="233.68" smashed="yes">
<attribute name="VALUE" x="82.55" y="246.761" size="1.778" layer="96"/>
<attribute name="PART" x="82.55" y="248.92" size="1.778" layer="95"/>
</instance>
<instance part="REL1" gate="U" x="101.6" y="238.76" smashed="yes" rot="R180"/>
<instance part="X8" gate="X" x="50.8" y="248.92" smashed="yes">
<attribute name="NAME" x="53.34" y="251.46" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="53.34" y="248.92" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="53.34" y="246.38" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="50.8" y="248.92" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="50.8" y="248.92" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="D1" gate="D" x="76.2" y="233.68" smashed="yes" rot="R90">
<attribute name="NAME" x="68.58" y="231.14" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="71.12" y="231.14" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R1" gate="G$1" x="91.44" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="89.9414" y="234.95" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="94.742" y="234.95" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="REL5" gate="K" x="86.36" y="114.3" smashed="yes" rot="MR180">
<attribute name="VALUE" x="87.63" y="106.299" size="1.778" layer="96" rot="MR180"/>
<attribute name="PART" x="87.63" y="109.22" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="REL5" gate="S" x="101.6" y="114.3" smashed="yes" rot="MR0">
<attribute name="PART" x="104.14" y="111.76" size="1.778" layer="95" rot="MR90"/>
</instance>
<instance part="X12" gate="X" x="68.58" y="114.3" smashed="yes" rot="MR0">
<attribute name="NAME" x="66.04" y="116.84" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="66.04" y="114.3" size="1.778" layer="96" rot="MR0" align="center-left"/>
<attribute name="DESCRIPTION" x="66.04" y="111.76" size="1.778" layer="97" rot="MR0" align="center-left"/>
<attribute name="MF" x="68.58" y="114.3" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
<attribute name="MPN" x="68.58" y="114.3" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
</instance>
<instance part="GND9" gate="1" x="116.84" y="101.6" smashed="yes" rot="MR0">
<attribute name="VALUE" x="119.38" y="99.06" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="REL4" gate="K" x="86.36" y="139.7" smashed="yes" rot="MR180">
<attribute name="VALUE" x="87.63" y="131.699" size="1.778" layer="96" rot="MR180"/>
<attribute name="PART" x="87.63" y="134.62" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="REL4" gate="S" x="101.6" y="139.7" smashed="yes" rot="MR0">
<attribute name="PART" x="104.14" y="137.16" size="1.778" layer="95" rot="MR90"/>
</instance>
<instance part="X11" gate="X" x="68.58" y="139.7" smashed="yes" rot="MR0">
<attribute name="NAME" x="66.04" y="142.24" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="66.04" y="139.7" size="1.778" layer="96" rot="MR0" align="center-left"/>
<attribute name="DESCRIPTION" x="66.04" y="137.16" size="1.778" layer="97" rot="MR0" align="center-left"/>
<attribute name="MF" x="68.58" y="139.7" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
<attribute name="MPN" x="68.58" y="139.7" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
</instance>
<instance part="D4" gate="D" x="68.58" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="60.96" y="129.54" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="63.5" y="129.54" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="D5" gate="D" x="116.84" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="110.998" y="114.3" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="113.284" y="114.3" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND6" gate="1" x="86.36" y="127" smashed="yes" rot="MR0">
<attribute name="VALUE" x="88.9" y="124.46" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND8" gate="1" x="86.36" y="101.6" smashed="yes" rot="MR0">
<attribute name="VALUE" x="88.9" y="99.06" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND7" gate="1" x="68.58" y="119.38" smashed="yes" rot="MR0">
<attribute name="VALUE" x="71.12" y="116.84" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="REL7" gate="K" x="86.36" y="58.42" smashed="yes" rot="MR180">
<attribute name="VALUE" x="87.63" y="50.419" size="1.778" layer="96" rot="MR180"/>
<attribute name="PART" x="87.63" y="53.34" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="REL7" gate="S" x="101.6" y="58.42" smashed="yes" rot="MR0">
<attribute name="PART" x="104.14" y="55.88" size="1.778" layer="95" rot="MR90"/>
</instance>
<instance part="X14" gate="X" x="68.58" y="58.42" smashed="yes" rot="MR0">
<attribute name="NAME" x="66.04" y="60.96" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="66.04" y="58.42" size="1.778" layer="96" rot="MR0" align="center-left"/>
<attribute name="DESCRIPTION" x="66.04" y="55.88" size="1.778" layer="97" rot="MR0" align="center-left"/>
<attribute name="MF" x="68.58" y="58.42" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
<attribute name="MPN" x="68.58" y="58.42" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
</instance>
<instance part="GND13" gate="1" x="116.84" y="45.72" smashed="yes" rot="MR0">
<attribute name="VALUE" x="119.38" y="43.18" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="REL6" gate="K" x="86.36" y="83.82" smashed="yes" rot="MR180">
<attribute name="VALUE" x="87.63" y="75.819" size="1.778" layer="96" rot="MR180"/>
<attribute name="PART" x="87.63" y="78.74" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="REL6" gate="S" x="101.6" y="83.82" smashed="yes" rot="MR0">
<attribute name="PART" x="104.14" y="81.28" size="1.778" layer="95" rot="MR90"/>
</instance>
<instance part="X13" gate="X" x="68.58" y="83.82" smashed="yes" rot="MR0">
<attribute name="NAME" x="66.04" y="86.36" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="66.04" y="83.82" size="1.778" layer="96" rot="MR0" align="center-left"/>
<attribute name="DESCRIPTION" x="66.04" y="81.28" size="1.778" layer="97" rot="MR0" align="center-left"/>
<attribute name="MF" x="68.58" y="83.82" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
<attribute name="MPN" x="68.58" y="83.82" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
</instance>
<instance part="D6" gate="D" x="68.58" y="71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="60.96" y="73.66" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="63.5" y="73.66" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="D7" gate="D" x="116.84" y="55.88" smashed="yes" rot="R90">
<attribute name="NAME" x="110.998" y="58.42" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="113.284" y="58.42" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND10" gate="1" x="86.36" y="71.12" smashed="yes" rot="MR0">
<attribute name="VALUE" x="88.9" y="68.58" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND12" gate="1" x="86.36" y="45.72" smashed="yes" rot="MR0">
<attribute name="VALUE" x="88.9" y="43.18" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND11" gate="1" x="68.58" y="63.5" smashed="yes" rot="MR0">
<attribute name="VALUE" x="71.12" y="60.96" size="1.778" layer="96" rot="MR0"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="+24V" class="0">
<segment>
<pinref part="P+1" gate="1" pin="+24V"/>
<wire x1="101.6" y1="241.3" x2="101.6" y2="243.84" width="0.1524" layer="91"/>
<pinref part="REL1" gate="U" pin="COM"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="101.6" y1="243.84" x2="101.6" y2="248.92" width="0.1524" layer="91"/>
<wire x1="91.44" y1="243.84" x2="101.6" y2="243.84" width="0.1524" layer="91"/>
<junction x="101.6" y="243.84"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="D3" gate="D" pin="A"/>
<wire x1="116.84" y1="165.1" x2="116.84" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="REL2" gate="K" pin="1"/>
<wire x1="86.36" y1="185.42" x2="86.36" y2="190.5" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="REL3" gate="K" pin="1"/>
<wire x1="86.36" y1="160.02" x2="86.36" y2="165.1" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="D2" gate="D" pin="A"/>
<wire x1="68.58" y1="177.8" x2="68.58" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="83.82" y1="223.52" x2="83.82" y2="226.06" width="0.1524" layer="91"/>
<pinref part="REL1" gate="K" pin="-"/>
<wire x1="83.82" y1="226.06" x2="83.82" y2="228.6" width="0.1524" layer="91"/>
<wire x1="83.82" y1="226.06" x2="76.2" y2="226.06" width="0.1524" layer="91"/>
<junction x="83.82" y="226.06"/>
<pinref part="D1" gate="D" pin="A"/>
<wire x1="76.2" y1="226.06" x2="76.2" y2="231.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="D5" gate="D" pin="A"/>
<wire x1="116.84" y1="109.22" x2="116.84" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="REL4" gate="K" pin="1"/>
<wire x1="86.36" y1="129.54" x2="86.36" y2="134.62" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="REL5" gate="K" pin="1"/>
<wire x1="86.36" y1="104.14" x2="86.36" y2="109.22" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="D4" gate="D" pin="A"/>
<wire x1="68.58" y1="121.92" x2="68.58" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="D7" gate="D" pin="A"/>
<wire x1="116.84" y1="53.34" x2="116.84" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="REL6" gate="K" pin="1"/>
<wire x1="86.36" y1="73.66" x2="86.36" y2="78.74" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="REL7" gate="K" pin="1"/>
<wire x1="86.36" y1="48.26" x2="86.36" y2="53.34" width="0.1524" layer="91"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="D6" gate="D" pin="A"/>
<wire x1="68.58" y1="66.04" x2="68.58" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VALVE_11" class="0">
<segment>
<pinref part="REL3" gate="S" pin="P"/>
<wire x1="101.6" y1="165.1" x2="101.6" y2="162.56" width="0.1524" layer="91"/>
<wire x1="101.6" y1="162.56" x2="109.22" y2="162.56" width="0.1524" layer="91"/>
<wire x1="109.22" y1="162.56" x2="109.22" y2="172.72" width="0.1524" layer="91"/>
<wire x1="109.22" y1="172.72" x2="116.84" y2="172.72" width="0.1524" layer="91"/>
<pinref part="D3" gate="D" pin="C"/>
<wire x1="116.84" y1="170.18" x2="116.84" y2="172.72" width="0.1524" layer="91"/>
<pinref part="REL2" gate="S" pin="P"/>
<wire x1="101.6" y1="190.5" x2="101.6" y2="187.96" width="0.1524" layer="91"/>
<wire x1="101.6" y1="187.96" x2="109.22" y2="187.96" width="0.1524" layer="91"/>
<wire x1="109.22" y1="187.96" x2="109.22" y2="172.72" width="0.1524" layer="91"/>
<junction x="109.22" y="172.72"/>
<junction x="116.84" y="172.72"/>
<wire x1="121.92" y1="172.72" x2="116.84" y2="172.72" width="0.1524" layer="91"/>
<label x="121.92" y="172.72" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_VALVE11" class="0">
<segment>
<pinref part="REL2" gate="K" pin="2"/>
<wire x1="86.36" y1="200.66" x2="86.36" y2="203.2" width="0.1524" layer="91"/>
<wire x1="86.36" y1="203.2" x2="73.66" y2="203.2" width="0.1524" layer="91"/>
<pinref part="REL3" gate="K" pin="2"/>
<wire x1="86.36" y1="175.26" x2="86.36" y2="177.8" width="0.1524" layer="91"/>
<wire x1="86.36" y1="177.8" x2="73.66" y2="177.8" width="0.1524" layer="91"/>
<wire x1="73.66" y1="203.2" x2="73.66" y2="187.96" width="0.1524" layer="91"/>
<wire x1="73.66" y1="187.96" x2="73.66" y2="177.8" width="0.1524" layer="91"/>
<wire x1="73.66" y1="187.96" x2="68.58" y2="187.96" width="0.1524" layer="91"/>
<junction x="73.66" y="187.96"/>
<label x="53.34" y="187.96" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="D2" gate="D" pin="C"/>
<wire x1="68.58" y1="187.96" x2="53.34" y2="187.96" width="0.1524" layer="91"/>
<wire x1="68.58" y1="185.42" x2="68.58" y2="187.96" width="0.1524" layer="91"/>
<junction x="68.58" y="187.96"/>
</segment>
</net>
<net name="+24V/2" class="0">
<segment>
<pinref part="REL3" gate="S" pin="S"/>
<wire x1="101.6" y1="175.26" x2="101.6" y2="177.8" width="0.1524" layer="91"/>
<label x="101.6" y="177.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="106.68" y1="233.68" x2="109.22" y2="233.68" width="0.1524" layer="91"/>
<wire x1="109.22" y1="233.68" x2="109.22" y2="228.6" width="0.1524" layer="91"/>
<label x="109.22" y="228.6" size="1.27" layer="95" rot="R270" xref="yes"/>
<pinref part="REL1" gate="U" pin="NO"/>
</segment>
<segment>
<pinref part="REL5" gate="S" pin="S"/>
<wire x1="101.6" y1="119.38" x2="101.6" y2="121.92" width="0.1524" layer="91"/>
<label x="101.6" y="121.92" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="REL7" gate="S" pin="S"/>
<wire x1="101.6" y1="63.5" x2="101.6" y2="66.04" width="0.1524" layer="91"/>
<label x="101.6" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="+24V/1" class="0">
<segment>
<pinref part="REL2" gate="S" pin="S"/>
<wire x1="101.6" y1="200.66" x2="101.6" y2="203.2" width="0.1524" layer="91"/>
<label x="101.6" y="203.2" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="96.52" y1="233.68" x2="93.98" y2="233.68" width="0.1524" layer="91"/>
<wire x1="93.98" y1="233.68" x2="93.98" y2="228.6" width="0.1524" layer="91"/>
<label x="93.98" y="228.6" size="1.27" layer="95" rot="R270" xref="yes"/>
<pinref part="REL1" gate="U" pin="NC"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="91.44" y1="233.68" x2="93.98" y2="233.68" width="0.1524" layer="91"/>
<junction x="93.98" y="233.68"/>
</segment>
<segment>
<pinref part="REL4" gate="S" pin="S"/>
<wire x1="101.6" y1="144.78" x2="101.6" y2="147.32" width="0.1524" layer="91"/>
<label x="101.6" y="147.32" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="REL6" gate="S" pin="S"/>
<wire x1="101.6" y1="88.9" x2="101.6" y2="91.44" width="0.1524" layer="91"/>
<label x="101.6" y="91.44" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="83.82" y1="238.76" x2="83.82" y2="241.3" width="0.1524" layer="91"/>
<wire x1="83.82" y1="241.3" x2="76.2" y2="241.3" width="0.1524" layer="91"/>
<label x="73.66" y="241.3" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="REL1" gate="K" pin="+"/>
<pinref part="D1" gate="D" pin="C"/>
<wire x1="76.2" y1="241.3" x2="73.66" y2="241.3" width="0.1524" layer="91"/>
<wire x1="76.2" y1="236.22" x2="76.2" y2="241.3" width="0.1524" layer="91"/>
<junction x="76.2" y="241.3"/>
</segment>
</net>
<net name="SIG_OUT_VALVE12" class="0">
<segment>
<pinref part="REL4" gate="K" pin="2"/>
<wire x1="86.36" y1="144.78" x2="86.36" y2="147.32" width="0.1524" layer="91"/>
<wire x1="86.36" y1="147.32" x2="73.66" y2="147.32" width="0.1524" layer="91"/>
<pinref part="REL5" gate="K" pin="2"/>
<wire x1="86.36" y1="119.38" x2="86.36" y2="121.92" width="0.1524" layer="91"/>
<wire x1="86.36" y1="121.92" x2="73.66" y2="121.92" width="0.1524" layer="91"/>
<wire x1="73.66" y1="147.32" x2="73.66" y2="132.08" width="0.1524" layer="91"/>
<wire x1="73.66" y1="132.08" x2="73.66" y2="121.92" width="0.1524" layer="91"/>
<wire x1="73.66" y1="132.08" x2="68.58" y2="132.08" width="0.1524" layer="91"/>
<junction x="73.66" y="132.08"/>
<label x="53.34" y="132.08" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="D4" gate="D" pin="C"/>
<wire x1="68.58" y1="132.08" x2="53.34" y2="132.08" width="0.1524" layer="91"/>
<wire x1="68.58" y1="129.54" x2="68.58" y2="132.08" width="0.1524" layer="91"/>
<junction x="68.58" y="132.08"/>
</segment>
</net>
<net name="SIG_OUT_VALVE21" class="0">
<segment>
<pinref part="REL6" gate="K" pin="2"/>
<wire x1="86.36" y1="88.9" x2="86.36" y2="91.44" width="0.1524" layer="91"/>
<wire x1="86.36" y1="91.44" x2="73.66" y2="91.44" width="0.1524" layer="91"/>
<pinref part="REL7" gate="K" pin="2"/>
<wire x1="86.36" y1="63.5" x2="86.36" y2="66.04" width="0.1524" layer="91"/>
<wire x1="86.36" y1="66.04" x2="73.66" y2="66.04" width="0.1524" layer="91"/>
<wire x1="73.66" y1="91.44" x2="73.66" y2="76.2" width="0.1524" layer="91"/>
<wire x1="73.66" y1="76.2" x2="73.66" y2="66.04" width="0.1524" layer="91"/>
<wire x1="73.66" y1="76.2" x2="68.58" y2="76.2" width="0.1524" layer="91"/>
<junction x="73.66" y="76.2"/>
<label x="53.34" y="76.2" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="D6" gate="D" pin="C"/>
<wire x1="68.58" y1="76.2" x2="53.34" y2="76.2" width="0.1524" layer="91"/>
<wire x1="68.58" y1="73.66" x2="68.58" y2="76.2" width="0.1524" layer="91"/>
<junction x="68.58" y="76.2"/>
</segment>
</net>
<net name="VALVE_21" class="0">
<segment>
<pinref part="REL7" gate="S" pin="P"/>
<wire x1="101.6" y1="53.34" x2="101.6" y2="50.8" width="0.1524" layer="91"/>
<wire x1="101.6" y1="50.8" x2="109.22" y2="50.8" width="0.1524" layer="91"/>
<wire x1="109.22" y1="50.8" x2="109.22" y2="60.96" width="0.1524" layer="91"/>
<wire x1="109.22" y1="60.96" x2="116.84" y2="60.96" width="0.1524" layer="91"/>
<pinref part="D7" gate="D" pin="C"/>
<wire x1="116.84" y1="58.42" x2="116.84" y2="60.96" width="0.1524" layer="91"/>
<pinref part="REL6" gate="S" pin="P"/>
<wire x1="101.6" y1="78.74" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<wire x1="101.6" y1="76.2" x2="109.22" y2="76.2" width="0.1524" layer="91"/>
<wire x1="109.22" y1="76.2" x2="109.22" y2="60.96" width="0.1524" layer="91"/>
<junction x="109.22" y="60.96"/>
<junction x="116.84" y="60.96"/>
<wire x1="121.92" y1="60.96" x2="116.84" y2="60.96" width="0.1524" layer="91"/>
<label x="121.92" y="60.96" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VALVE_12" class="0">
<segment>
<pinref part="REL5" gate="S" pin="P"/>
<wire x1="101.6" y1="109.22" x2="101.6" y2="106.68" width="0.1524" layer="91"/>
<wire x1="101.6" y1="106.68" x2="109.22" y2="106.68" width="0.1524" layer="91"/>
<wire x1="109.22" y1="106.68" x2="109.22" y2="116.84" width="0.1524" layer="91"/>
<wire x1="109.22" y1="116.84" x2="116.84" y2="116.84" width="0.1524" layer="91"/>
<pinref part="D5" gate="D" pin="C"/>
<wire x1="116.84" y1="114.3" x2="116.84" y2="116.84" width="0.1524" layer="91"/>
<pinref part="REL4" gate="S" pin="P"/>
<wire x1="101.6" y1="134.62" x2="101.6" y2="132.08" width="0.1524" layer="91"/>
<wire x1="101.6" y1="132.08" x2="109.22" y2="132.08" width="0.1524" layer="91"/>
<wire x1="109.22" y1="132.08" x2="109.22" y2="116.84" width="0.1524" layer="91"/>
<junction x="109.22" y="116.84"/>
<junction x="116.84" y="116.84"/>
<wire x1="121.92" y1="116.84" x2="116.84" y2="116.84" width="0.1524" layer="91"/>
<label x="121.92" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="113,1,169.228,28.4692,LOGO1,,,,,"/>
<approved hash="113,1,90.066,131.976,FRAME1,,,,,"/>
<approved hash="113,2,90.066,131.976,FRAME2,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
