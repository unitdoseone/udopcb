<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="42" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="47" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="43" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="39" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="42" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="47" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="42" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="47" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="42" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="47" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="42" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="47" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="10" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="42" fill="11" visible="no" active="no"/>
<layer number="52" name="bDocu" color="47" fill="11" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Warning" color="59" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DINA4_L" urn="urn:adsk.eagle:symbol:13867/1" library_version="1">
<frame x1="0" y1="0" x2="264.16" y2="180.34" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD" urn="urn:adsk.eagle:symbol:13864/1" library_version="1">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" urn="urn:adsk.eagle:component:13919/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="avx-idc">
<packages>
<package name="009177002" urn="urn:adsk.eagle:footprint:6122/1">
<description>&lt;b&gt;Wire to Board IDC Connector&lt;/b&gt; 2 WAY&lt;p&gt;
Source: http://www.avx.com/docs/Catalogs/9177.pdf</description>
<wire x1="8.8284" y1="-4.397" x2="6.2405" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="6.2405" y1="-4.397" x2="5.2557" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="3.7672" y1="-4.397" x2="2.7824" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="-4.397" x2="-2.7595" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="-2.7595" y1="-4.397" x2="-3.7443" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="-5.2328" y1="-4.397" x2="-8.7826" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="-8.7826" y1="-4.397" x2="-8.7826" y2="4.397" width="0.2032" layer="21"/>
<wire x1="-8.7826" y1="4.397" x2="-5.2099" y2="4.397" width="0.2032" layer="51"/>
<wire x1="-3.7443" y1="4.397" x2="3.7901" y2="4.397" width="0.2032" layer="51"/>
<wire x1="5.2557" y1="4.397" x2="8.8284" y2="4.397" width="0.2032" layer="51"/>
<wire x1="8.8284" y1="4.397" x2="8.8284" y2="-4.397" width="0.2032" layer="21"/>
<wire x1="-6.2176" y1="-4.397" x2="-6.2176" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-6.2176" y1="-3.5497" x2="-6.2176" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="-6.2176" y1="-1.7405" x2="-6.2176" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-6.2176" y1="-1.2138" x2="-6.2176" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-6.2176" y1="1.2367" x2="-6.2176" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-6.2176" y1="1.7405" x2="-6.2176" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-6.2176" y1="3.5726" x2="-6.2176" y2="4.3512" width="0.2032" layer="51"/>
<wire x1="-2.7595" y1="-4.397" x2="-2.7595" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-2.7595" y1="-3.5497" x2="-2.7595" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="-2.7595" y1="-1.7405" x2="-2.7595" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-2.7595" y1="-1.2138" x2="-2.7595" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-2.7595" y1="1.2367" x2="-2.7595" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-2.7595" y1="1.7405" x2="-2.7595" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-2.7595" y1="3.5726" x2="-2.7595" y2="4.3512" width="0.2032" layer="51"/>
<wire x1="-5.2328" y1="-4.397" x2="-5.2328" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-5.2328" y1="-3.5497" x2="-6.2176" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-5.2328" y1="-3.5497" x2="-3.7443" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-3.7443" y1="-3.5497" x2="-2.7595" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-3.7443" y1="-4.397" x2="-3.7443" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-6.2176" y1="-1.7405" x2="-4.8664" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="-4.8664" y1="-1.7405" x2="-2.7595" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="-6.2176" y1="-1.2138" x2="-4.8664" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-4.8664" y1="-1.2138" x2="-4.1107" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-4.1107" y1="-1.2138" x2="-2.7595" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-6.2176" y1="1.2367" x2="-4.8664" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-4.8664" y1="1.2367" x2="-4.0878" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-4.0878" y1="1.2367" x2="-2.7595" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-6.2176" y1="1.7405" x2="-4.8664" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-4.8664" y1="1.7405" x2="-4.0878" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-4.0878" y1="1.7405" x2="-2.7595" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-4.8664" y1="-1.7405" x2="-4.8664" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-4.1107" y1="-1.7634" x2="-4.1107" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-4.8664" y1="1.2367" x2="-4.8664" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-4.0878" y1="1.2367" x2="-4.0878" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-6.2176" y1="3.5726" x2="-5.2099" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-5.2099" y1="3.5726" x2="-3.7443" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-3.7443" y1="3.5726" x2="-2.7595" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-5.2099" y1="3.5726" x2="-5.2099" y2="4.397" width="0.2032" layer="51"/>
<wire x1="-3.7443" y1="3.5726" x2="-3.7443" y2="4.397" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="-4.397" x2="2.7824" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="-3.5497" x2="2.7824" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="-1.7405" x2="2.7824" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="-1.2138" x2="2.7824" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="1.2367" x2="2.7824" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="1.7405" x2="2.7824" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="3.5726" x2="2.7824" y2="4.3512" width="0.2032" layer="51"/>
<wire x1="6.2405" y1="-4.397" x2="6.2405" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="6.2405" y1="-3.5497" x2="6.2405" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="6.2405" y1="-1.7405" x2="6.2405" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="6.2405" y1="-1.2138" x2="6.2405" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="6.2405" y1="1.2367" x2="6.2405" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="6.2405" y1="1.7405" x2="6.2405" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="6.2405" y1="3.5726" x2="6.2405" y2="4.3512" width="0.2032" layer="51"/>
<wire x1="3.7672" y1="-4.397" x2="3.7672" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="3.7672" y1="-3.5497" x2="2.7824" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="3.7672" y1="-3.5497" x2="5.2557" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="5.2557" y1="-3.5497" x2="6.2405" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="5.2557" y1="-4.397" x2="5.2557" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="-1.7405" x2="4.1336" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="4.1336" y1="-1.7405" x2="6.2405" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="-1.2138" x2="4.1336" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="4.1336" y1="-1.2138" x2="4.8893" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="4.8893" y1="-1.2138" x2="6.2405" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="1.2367" x2="4.1336" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="4.1336" y1="1.2367" x2="4.9122" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="4.9122" y1="1.2367" x2="6.2405" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="1.7405" x2="4.1336" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="4.1336" y1="1.7405" x2="4.9122" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="4.9122" y1="1.7405" x2="6.2405" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="4.1336" y1="-1.7405" x2="4.1336" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="4.8893" y1="-1.7634" x2="4.8893" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="4.1336" y1="1.2367" x2="4.1336" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="4.9122" y1="1.2367" x2="4.9122" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="2.7824" y1="3.5726" x2="3.7901" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="3.7901" y1="3.5726" x2="5.2557" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="5.2557" y1="3.5726" x2="6.2405" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="3.7901" y1="3.5726" x2="3.7901" y2="4.397" width="0.2032" layer="51"/>
<wire x1="5.2557" y1="3.5726" x2="5.2557" y2="4.397" width="0.2032" layer="51"/>
<wire x1="1.2824" y1="-4.397" x2="-1.2595" y2="-4.397" width="0.2032" layer="21"/>
<wire x1="-1.2443" y1="4.397" x2="1.2901" y2="4.397" width="0.2032" layer="21"/>
<smd name="1" x="-4.5" y="0" dx="5.9" dy="9.4" layer="1"/>
<smd name="2" x="4.5" y="0" dx="5.9" dy="9.4" layer="1"/>
<text x="-8" y="5.04" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.5" y="-6.29" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0091760020" urn="urn:adsk.eagle:footprint:6228039/1" locally_modified="yes">
<description>&lt;b&gt;Wire to Board Strip IDC Connector&lt;/b&gt; 2 WAY&lt;p&gt;
Source: http://www.avx.com/docs/Catalogs/9176.pdf</description>
<wire x1="3.9169" y1="-2.4454" x2="3.0327" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="3.0327" y1="-2.4454" x2="0.9694" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="0.9694" y1="-2.4454" x2="-0.9127" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="-0.9127" y1="-2.4454" x2="-2.976" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="-2.976" y1="-2.4454" x2="-3.9171" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="-3.9171" y1="-2.4454" x2="-3.9171" y2="2.4563" width="0.1016" layer="21"/>
<wire x1="-3.9171" y1="2.4563" x2="-2.976" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-2.976" y1="2.4563" x2="-1.4476" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-1.4476" y1="2.4563" x2="-0.9127" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-0.9127" y1="2.4563" x2="0.9694" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="0.9694" y1="2.4563" x2="3.0327" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="3.0327" y1="2.4563" x2="3.9169" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="3.9169" y1="2.4563" x2="3.9169" y2="-2.4454" width="0.1016" layer="21"/>
<wire x1="3.0327" y1="-2.4454" x2="3.0327" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="3.0327" y1="-2.0415" x2="3.0327" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="3.0327" y1="-1.2664" x2="3.0327" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="3.0327" y1="-0.7205" x2="3.0327" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="3.0327" y1="0.7314" x2="3.0327" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="3.0327" y1="1.2773" x2="3.0327" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="3.0327" y1="2.0305" x2="3.0327" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="0.9694" y1="-2.4454" x2="0.9694" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="0.9694" y1="-2.0415" x2="0.9694" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="0.9694" y1="-1.2664" x2="0.9694" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="0.9694" y1="-0.7205" x2="0.9694" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="0.9694" y1="0.7314" x2="0.9694" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="0.9694" y1="1.2773" x2="0.9694" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="0.9694" y1="2.0305" x2="0.9694" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="0.9694" y1="-2.0415" x2="1.4716" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="1.4716" y1="-2.0415" x2="1.4825" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="1.4825" y1="-2.0415" x2="1.4825" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="1.4716" y1="-2.0415" x2="2.4759" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="2.4759" y1="-2.0415" x2="2.4868" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="2.4868" y1="-2.0415" x2="2.4868" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="2.4759" y1="-2.0415" x2="3.0327" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="3.0327" y1="-0.7205" x2="2.3558" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="2.3558" y1="-0.7205" x2="1.6353" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="1.6353" y1="-0.7205" x2="0.9694" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="0.9694" y1="-1.2664" x2="1.6353" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="1.6353" y1="-1.2664" x2="2.3558" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="2.3558" y1="-1.2664" x2="3.0327" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="2.3558" y1="-1.2664" x2="2.3558" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="1.6353" y1="-1.2664" x2="1.6353" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="0.9694" y1="0.7314" x2="1.6244" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="1.6244" y1="0.7314" x2="2.3558" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="2.3558" y1="0.7314" x2="3.0327" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="3.0327" y1="1.2773" x2="2.3558" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="2.3558" y1="1.2773" x2="1.6244" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="1.6244" y1="1.2773" x2="0.9694" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="1.6244" y1="0.7314" x2="1.6244" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="2.3558" y1="0.7314" x2="2.3558" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="3.0327" y1="2.0305" x2="2.4978" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="2.4978" y1="2.0305" x2="1.4934" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="1.4934" y1="2.0305" x2="0.9694" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="1.4934" y1="2.0305" x2="1.4934" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="2.4978" y1="2.0305" x2="2.4978" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-0.9127" y1="-2.4454" x2="-0.9127" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-0.9127" y1="-2.0415" x2="-0.9127" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="-0.9127" y1="-1.2664" x2="-0.9127" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-0.9127" y1="-0.7205" x2="-0.9127" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="-0.9127" y1="0.7314" x2="-0.9127" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-0.9127" y1="1.2773" x2="-0.9127" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-0.9127" y1="2.0305" x2="-0.9127" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-2.976" y1="-2.4454" x2="-2.976" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-2.976" y1="-2.0415" x2="-2.976" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="-2.976" y1="-1.2664" x2="-2.976" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-2.976" y1="-0.7205" x2="-2.976" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="-2.976" y1="0.7314" x2="-2.976" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-2.976" y1="1.2773" x2="-2.976" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-2.976" y1="2.0305" x2="-2.976" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-2.976" y1="-2.0415" x2="-2.4738" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-2.4738" y1="-2.0415" x2="-2.4629" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-2.4629" y1="-2.0415" x2="-2.4629" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="-2.4738" y1="-2.0415" x2="-1.4695" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-1.4695" y1="-2.0415" x2="-1.4586" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-1.4586" y1="-2.0415" x2="-1.4586" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="-1.4695" y1="-2.0415" x2="-0.9127" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-0.9127" y1="-0.7205" x2="-1.5896" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-1.5896" y1="-0.7205" x2="-2.3101" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-2.3101" y1="-0.7205" x2="-2.976" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-2.976" y1="-1.2664" x2="-2.3101" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="-2.3101" y1="-1.2664" x2="-1.5896" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="-1.5896" y1="-1.2664" x2="-0.9127" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="-1.5896" y1="-1.2664" x2="-1.5896" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-2.3101" y1="-1.2664" x2="-2.3101" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-2.976" y1="0.7314" x2="-2.321" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="-2.321" y1="0.7314" x2="-1.5896" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="-1.5896" y1="0.7314" x2="-0.9127" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="-0.9127" y1="1.2773" x2="-1.5896" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-1.5896" y1="1.2773" x2="-2.321" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-2.321" y1="1.2773" x2="-2.976" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-2.321" y1="0.7314" x2="-2.321" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-1.5896" y1="0.7314" x2="-1.5896" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-0.9127" y1="2.0305" x2="-1.4476" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-1.4476" y1="2.0305" x2="-2.452" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-2.452" y1="2.0305" x2="-2.976" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-2.452" y1="2.0305" x2="-2.452" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-1.4476" y1="2.0305" x2="-1.4476" y2="2.4563" width="0.1016" layer="51"/>
<smd name="1" x="-2" y="0" dx="3" dy="5" layer="1"/>
<smd name="2" x="2" y="0" dx="3" dy="5" layer="1"/>
<text x="-3.75" y="2.75" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.75" y="-4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0091750020" urn="urn:adsk.eagle:footprint:6228081/1" locally_modified="yes">
<description>&lt;b&gt;Wire to Board IDC Connector&lt;/b&gt; 2 WAY&lt;p&gt;
Source: http://www.avx.com/docs/Catalogs/9175.pdf</description>
<wire x1="-2.45" y1="1.2" x2="-1.45" y2="1.2" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="1.2" x2="-1.45" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="0.7" x2="-1.05" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-1.05" y1="0.7" x2="-1.05" y2="1.2" width="0.1016" layer="21"/>
<wire x1="-1.05" y1="1.2" x2="1.05" y2="1.2" width="0.1016" layer="51"/>
<wire x1="1.05" y1="1.2" x2="1.05" y2="0.7" width="0.1016" layer="51"/>
<wire x1="1.05" y1="0.7" x2="1.45" y2="0.7" width="0.1016" layer="51"/>
<wire x1="1.45" y1="0.7" x2="1.45" y2="1.2" width="0.1016" layer="21"/>
<wire x1="1.45" y1="1.2" x2="2.45" y2="1.2" width="0.1016" layer="21"/>
<wire x1="2.45" y1="1.2" x2="2.45" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-1.2" x2="1.45" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="1.45" y1="-1.2" x2="1.45" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="1.45" y1="-0.7" x2="1.05" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-0.7" x2="1.05" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="1.05" y1="-1.2" x2="-1.05" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="-1.05" y1="-1.2" x2="-1.05" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-1.05" y1="-0.7" x2="-1.45" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="-0.7" x2="-1.45" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="-1.45" y1="-1.2" x2="-2.45" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="-1.2" x2="-2.45" y2="1.2" width="0.1016" layer="21"/>
<wire x1="-1.05" y1="0.7" x2="-0.8" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.8" y1="0.7" x2="-0.8" y2="0.15" width="0.1016" layer="21"/>
<wire x1="-0.8" y1="0.15" x2="-1.125" y2="0.15" width="0.1016" layer="21"/>
<wire x1="-1.125" y1="0.15" x2="-1.125" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-1.125" y1="-0.15" x2="-0.8" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-0.8" y1="-0.15" x2="-0.8" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.8" y1="-0.7" x2="-1.05" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="-0.7" x2="-1.7" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-1.7" y1="-0.7" x2="-1.7" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="-1.7" y1="-0.15" x2="-1.375" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-1.375" y1="-0.15" x2="-1.375" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-1.375" y1="0.15" x2="-1.7" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.15" x2="-1.7" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.7" x2="-1.45" y2="0.7" width="0.1016" layer="51"/>
<wire x1="1.45" y1="0.7" x2="1.7" y2="0.7" width="0.1016" layer="21"/>
<wire x1="1.7" y1="0.7" x2="1.7" y2="0.15" width="0.1016" layer="21"/>
<wire x1="1.7" y1="0.15" x2="1.375" y2="0.15" width="0.1016" layer="21"/>
<wire x1="1.375" y1="0.15" x2="1.375" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="1.375" y1="-0.15" x2="1.7" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="1.7" y1="-0.15" x2="1.7" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="1.7" y1="-0.7" x2="1.45" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-0.7" x2="0.8" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.8" y1="-0.15" x2="1.125" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="1.125" y1="-0.15" x2="1.125" y2="0.15" width="0.1016" layer="51"/>
<wire x1="1.125" y1="0.15" x2="0.8" y2="0.15" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.15" x2="0.8" y2="0.7" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.7" x2="1.05" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.15" x2="-1.7" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-0.8" y1="0.15" x2="-0.8" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.15" x2="0.8" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="0.8" y1="-0.7" x2="0.8" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="1.7" y1="0.15" x2="1.7" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-1.05" y1="1.2" x2="0.05" y2="1.2" width="0.1016" layer="21"/>
<wire x1="1.05" y1="-1.2" x2="-0.05" y2="-1.2" width="0.1016" layer="21"/>
<smd name="1" x="-1.8" y="0.725" dx="1" dy="1.45" layer="1"/>
<smd name="1@1" x="-0.7" y="-0.725" dx="1" dy="1.45" layer="1"/>
<smd name="2" x="0.7" y="0.725" dx="1" dy="1.45" layer="1"/>
<smd name="2@1" x="1.8" y="-0.725" dx="1" dy="1.45" layer="1"/>
<text x="-2.5" y="1.75" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.45" y1="1.2" x2="-1.45" y2="1.2" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="1.2" x2="-1.45" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="0.7" x2="-1.05" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-1.05" y1="0.7" x2="-1.05" y2="1.2" width="0.1016" layer="21"/>
<wire x1="-1.05" y1="1.2" x2="1.05" y2="1.2" width="0.1016" layer="51"/>
<wire x1="1.05" y1="1.2" x2="1.05" y2="0.7" width="0.1016" layer="51"/>
<wire x1="1.05" y1="0.7" x2="1.45" y2="0.7" width="0.1016" layer="51"/>
<wire x1="1.45" y1="0.7" x2="1.45" y2="1.2" width="0.1016" layer="21"/>
<wire x1="1.45" y1="1.2" x2="2.45" y2="1.2" width="0.1016" layer="21"/>
<wire x1="2.45" y1="1.2" x2="2.45" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-1.2" x2="1.45" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="1.45" y1="-1.2" x2="1.45" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="1.45" y1="-0.7" x2="1.05" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-0.7" x2="1.05" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="1.05" y1="-1.2" x2="-1.05" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="-1.05" y1="-1.2" x2="-1.05" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-1.05" y1="-0.7" x2="-1.45" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="-0.7" x2="-1.45" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="-1.45" y1="-1.2" x2="-2.45" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="-1.2" x2="-2.45" y2="1.2" width="0.1016" layer="21"/>
<wire x1="-1.05" y1="0.7" x2="-0.8" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.8" y1="0.7" x2="-0.8" y2="0.15" width="0.1016" layer="21"/>
<wire x1="-0.8" y1="0.15" x2="-1.125" y2="0.15" width="0.1016" layer="21"/>
<wire x1="-1.125" y1="0.15" x2="-1.125" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-1.125" y1="-0.15" x2="-0.8" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-0.8" y1="-0.15" x2="-0.8" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.8" y1="-0.7" x2="-1.05" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="-0.7" x2="-1.7" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-1.7" y1="-0.7" x2="-1.7" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="-1.7" y1="-0.15" x2="-1.375" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-1.375" y1="-0.15" x2="-1.375" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-1.375" y1="0.15" x2="-1.7" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.15" x2="-1.7" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.7" x2="-1.45" y2="0.7" width="0.1016" layer="51"/>
<wire x1="1.45" y1="0.7" x2="1.7" y2="0.7" width="0.1016" layer="21"/>
<wire x1="1.7" y1="0.7" x2="1.7" y2="0.15" width="0.1016" layer="21"/>
<wire x1="1.7" y1="0.15" x2="1.375" y2="0.15" width="0.1016" layer="21"/>
<wire x1="1.375" y1="0.15" x2="1.375" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="1.375" y1="-0.15" x2="1.7" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="1.7" y1="-0.15" x2="1.7" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="1.7" y1="-0.7" x2="1.45" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-0.7" x2="0.8" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.8" y1="-0.15" x2="1.125" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="1.125" y1="-0.15" x2="1.125" y2="0.15" width="0.1016" layer="51"/>
<wire x1="1.125" y1="0.15" x2="0.8" y2="0.15" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.15" x2="0.8" y2="0.7" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.7" x2="1.05" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.15" x2="-1.7" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-0.8" y1="0.15" x2="-0.8" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.15" x2="0.8" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="0.8" y1="-0.7" x2="0.8" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="1.7" y1="0.15" x2="1.7" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-1.05" y1="1.2" x2="0.05" y2="1.2" width="0.1016" layer="21"/>
<wire x1="1.05" y1="-1.2" x2="-0.05" y2="-1.2" width="0.1016" layer="21"/>
<smd name="3" x="-1.8" y="0.725" dx="1" dy="1.45" layer="1"/>
<smd name="1@2" x="-0.7" y="-0.725" dx="1" dy="1.45" layer="1"/>
<smd name="4" x="0.7" y="0.725" dx="1" dy="1.45" layer="1"/>
<smd name="2@2" x="1.8" y="-0.725" dx="1" dy="1.45" layer="1"/>
<text x="-2.5" y="1.75" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0091750027" urn="urn:adsk.eagle:footprint:6339544/1">
<wire x1="-2.45" y1="1.2" x2="-1.45" y2="1.2" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="1.2" x2="-1.45" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="0.7" x2="-1.05" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-1.05" y1="0.7" x2="-1.05" y2="1.2" width="0.1016" layer="21"/>
<wire x1="-1.05" y1="1.2" x2="1.05" y2="1.2" width="0.1016" layer="51"/>
<wire x1="1.05" y1="1.2" x2="1.05" y2="0.7" width="0.1016" layer="51"/>
<wire x1="1.05" y1="0.7" x2="1.45" y2="0.7" width="0.1016" layer="51"/>
<wire x1="1.45" y1="0.7" x2="1.45" y2="1.2" width="0.1016" layer="21"/>
<wire x1="1.45" y1="1.2" x2="2.45" y2="1.2" width="0.1016" layer="21"/>
<wire x1="2.45" y1="1.2" x2="2.45" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-1.2" x2="1.45" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="1.45" y1="-1.2" x2="1.45" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="1.45" y1="-0.7" x2="1.05" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-0.7" x2="1.05" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="1.05" y1="-1.2" x2="-1.05" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="-1.05" y1="-1.2" x2="-1.05" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-1.05" y1="-0.7" x2="-1.45" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="-0.7" x2="-1.45" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="-1.45" y1="-1.2" x2="-2.45" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="-1.2" x2="-2.45" y2="1.2" width="0.1016" layer="21"/>
<wire x1="-1.05" y1="0.7" x2="-0.8" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.8" y1="0.7" x2="-0.8" y2="0.15" width="0.1016" layer="21"/>
<wire x1="-0.8" y1="0.15" x2="-1.125" y2="0.15" width="0.1016" layer="21"/>
<wire x1="-1.125" y1="0.15" x2="-1.125" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-1.125" y1="-0.15" x2="-0.8" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-0.8" y1="-0.15" x2="-0.8" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.8" y1="-0.7" x2="-1.05" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="-0.7" x2="-1.7" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-1.7" y1="-0.7" x2="-1.7" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="-1.7" y1="-0.15" x2="-1.375" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-1.375" y1="-0.15" x2="-1.375" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-1.375" y1="0.15" x2="-1.7" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.15" x2="-1.7" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.7" x2="-1.45" y2="0.7" width="0.1016" layer="51"/>
<wire x1="1.45" y1="0.7" x2="1.7" y2="0.7" width="0.1016" layer="21"/>
<wire x1="1.7" y1="0.7" x2="1.7" y2="0.15" width="0.1016" layer="21"/>
<wire x1="1.7" y1="0.15" x2="1.375" y2="0.15" width="0.1016" layer="21"/>
<wire x1="1.375" y1="0.15" x2="1.375" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="1.375" y1="-0.15" x2="1.7" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="1.7" y1="-0.15" x2="1.7" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="1.7" y1="-0.7" x2="1.45" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-0.7" x2="0.8" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.8" y1="-0.15" x2="1.125" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="1.125" y1="-0.15" x2="1.125" y2="0.15" width="0.1016" layer="51"/>
<wire x1="1.125" y1="0.15" x2="0.8" y2="0.15" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.15" x2="0.8" y2="0.7" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.7" x2="1.05" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.15" x2="-1.7" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-0.8" y1="0.15" x2="-0.8" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.15" x2="0.8" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="0.8" y1="-0.7" x2="0.8" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="1.7" y1="0.15" x2="1.7" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-1.05" y1="1.2" x2="0.05" y2="1.2" width="0.1016" layer="21"/>
<wire x1="1.05" y1="-1.2" x2="-0.05" y2="-1.2" width="0.1016" layer="21"/>
<smd name="1" x="-1.8" y="0.725" dx="1" dy="1.45" layer="1"/>
<smd name="1@1" x="-0.7" y="-0.725" dx="1" dy="1.45" layer="1"/>
<smd name="2" x="0.7" y="0.725" dx="1" dy="1.45" layer="1"/>
<smd name="2@1" x="1.8" y="-0.725" dx="1" dy="1.45" layer="1"/>
<text x="-2.5" y="1.75" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="009177003" urn="urn:adsk.eagle:footprint:6125/1">
<description>&lt;b&gt;Wire to Board IDC Connector&lt;/b&gt; 3 WAY&lt;p&gt;
Source: http://www.avx.com/docs/Catalogs/9177.pdf</description>
<wire x1="13.3284" y1="-4.397" x2="10.7405" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="10.7405" y1="-4.397" x2="9.7557" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="8.2672" y1="-4.397" x2="7.2824" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="-4.397" x2="1.7405" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="1.7405" y1="-4.397" x2="0.7557" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="-0.7328" y1="-4.397" x2="-1.7176" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="-1.7176" y1="-4.397" x2="-8.2443" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="-9.7328" y1="-4.397" x2="-13.2826" y2="-4.397" width="0.2032" layer="51"/>
<wire x1="-13.2826" y1="-4.397" x2="-13.2826" y2="4.397" width="0.2032" layer="21"/>
<wire x1="-13.2826" y1="4.397" x2="-9.7099" y2="4.397" width="0.2032" layer="51"/>
<wire x1="-8.2443" y1="4.397" x2="-0.7099" y2="4.397" width="0.2032" layer="51"/>
<wire x1="0.7557" y1="4.397" x2="8.2901" y2="4.397" width="0.2032" layer="51"/>
<wire x1="9.7557" y1="4.397" x2="13.3284" y2="4.397" width="0.2032" layer="51"/>
<wire x1="13.3284" y1="4.397" x2="13.3284" y2="-4.397" width="0.2032" layer="21"/>
<wire x1="-1.7176" y1="-4.397" x2="-1.7176" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-1.7176" y1="-3.5497" x2="-1.7176" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="-1.7176" y1="-1.7405" x2="-1.7176" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-1.7176" y1="-1.2138" x2="-1.7176" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-1.7176" y1="1.2367" x2="-1.7176" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-1.7176" y1="1.7405" x2="-1.7176" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-1.7176" y1="3.5726" x2="-1.7176" y2="4.3512" width="0.2032" layer="51"/>
<wire x1="1.7405" y1="-4.397" x2="1.7405" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="1.7405" y1="-3.5497" x2="1.7405" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="1.7405" y1="-1.7405" x2="1.7405" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="1.7405" y1="-1.2138" x2="1.7405" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="1.7405" y1="1.2367" x2="1.7405" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="1.7405" y1="1.7405" x2="1.7405" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="1.7405" y1="3.5726" x2="1.7405" y2="4.3512" width="0.2032" layer="51"/>
<wire x1="-0.7328" y1="-4.397" x2="-0.7328" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-0.7328" y1="-3.5497" x2="-1.7176" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-0.7328" y1="-3.5497" x2="0.7557" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="0.7557" y1="-3.5497" x2="1.7405" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="0.7557" y1="-4.397" x2="0.7557" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-1.7176" y1="-1.7405" x2="-0.3664" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="-0.3664" y1="-1.7405" x2="0.3893" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="0.3893" y1="-1.7405" x2="1.7405" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="-1.7176" y1="-1.2138" x2="-0.3664" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-0.3664" y1="-1.2138" x2="0.3893" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="0.3893" y1="-1.2138" x2="1.7405" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-1.7176" y1="1.2367" x2="-0.3664" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-0.3664" y1="1.2367" x2="0.4122" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="0.4122" y1="1.2367" x2="1.7405" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-1.7176" y1="1.7405" x2="-0.3664" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-0.3664" y1="1.7405" x2="0.4122" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="0.4122" y1="1.7405" x2="1.7405" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-0.3664" y1="-1.7405" x2="-0.3664" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="0.3893" y1="-1.7405" x2="0.3893" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-0.3664" y1="1.2367" x2="-0.3664" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="0.4122" y1="1.2367" x2="0.4122" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-1.7176" y1="3.5726" x2="-0.7099" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-0.7099" y1="3.5726" x2="0.7557" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="0.7557" y1="3.5726" x2="1.7405" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-0.7099" y1="3.5726" x2="-0.7099" y2="4.397" width="0.2032" layer="51"/>
<wire x1="0.7557" y1="3.5726" x2="0.7557" y2="4.397" width="0.2032" layer="51"/>
<wire x1="-10.7176" y1="-4.397" x2="-10.7176" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-10.7176" y1="-3.5497" x2="-10.7176" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="-10.7176" y1="-1.7405" x2="-10.7176" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-10.7176" y1="-1.2138" x2="-10.7176" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-10.7176" y1="1.2367" x2="-10.7176" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-10.7176" y1="1.7405" x2="-10.7176" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-10.7176" y1="3.5726" x2="-10.7176" y2="4.3512" width="0.2032" layer="51"/>
<wire x1="-7.2595" y1="-4.397" x2="-7.2595" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-7.2595" y1="-3.5497" x2="-7.2595" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="-7.2595" y1="-1.7405" x2="-7.2595" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-7.2595" y1="-1.2138" x2="-7.2595" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-7.2595" y1="1.2367" x2="-7.2595" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-7.2595" y1="1.7405" x2="-7.2595" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-7.2595" y1="3.5726" x2="-7.2595" y2="4.3512" width="0.2032" layer="51"/>
<wire x1="-9.7328" y1="-4.397" x2="-9.7328" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-9.7328" y1="-3.5497" x2="-10.7176" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-9.7328" y1="-3.5497" x2="-8.2443" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-8.2443" y1="-3.5497" x2="-7.2595" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-8.2443" y1="-4.397" x2="-8.2443" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="-10.7176" y1="-1.7405" x2="-9.3664" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="-9.3664" y1="-1.7405" x2="-7.2595" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="-10.7176" y1="-1.2138" x2="-9.3664" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-9.3664" y1="-1.2138" x2="-8.6107" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-8.6107" y1="-1.2138" x2="-7.2595" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-10.7176" y1="1.2367" x2="-9.3664" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-9.3664" y1="1.2367" x2="-8.5878" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-8.5878" y1="1.2367" x2="-7.2595" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="-10.7176" y1="1.7405" x2="-9.3664" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-9.3664" y1="1.7405" x2="-8.5878" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-8.5878" y1="1.7405" x2="-7.2595" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-9.3664" y1="-1.7405" x2="-9.3664" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-8.6107" y1="-1.7634" x2="-8.6107" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="-9.3664" y1="1.2367" x2="-9.3664" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-8.5878" y1="1.2367" x2="-8.5878" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="-10.7176" y1="3.5726" x2="-9.7099" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-9.7099" y1="3.5726" x2="-8.2443" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-8.2443" y1="3.5726" x2="-7.2595" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="-9.7099" y1="3.5726" x2="-9.7099" y2="4.397" width="0.2032" layer="51"/>
<wire x1="-8.2443" y1="3.5726" x2="-8.2443" y2="4.397" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="-4.397" x2="7.2824" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="-3.5497" x2="7.2824" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="-1.7405" x2="7.2824" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="-1.2138" x2="7.2824" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="1.2367" x2="7.2824" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="1.7405" x2="7.2824" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="3.5726" x2="7.2824" y2="4.3512" width="0.2032" layer="51"/>
<wire x1="10.7405" y1="-4.397" x2="10.7405" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="10.7405" y1="-3.5497" x2="10.7405" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="10.7405" y1="-1.7405" x2="10.7405" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="10.7405" y1="-1.2138" x2="10.7405" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="10.7405" y1="1.2367" x2="10.7405" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="10.7405" y1="1.7405" x2="10.7405" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="10.7405" y1="3.5726" x2="10.7405" y2="4.3512" width="0.2032" layer="51"/>
<wire x1="8.2672" y1="-4.397" x2="8.2672" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="8.2672" y1="-3.5497" x2="7.2824" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="8.2672" y1="-3.5497" x2="9.7557" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="9.7557" y1="-3.5497" x2="10.7405" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="9.7557" y1="-4.397" x2="9.7557" y2="-3.5497" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="-1.7405" x2="8.6336" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="8.6336" y1="-1.7405" x2="10.7405" y2="-1.7405" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="-1.2138" x2="8.6336" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="8.6336" y1="-1.2138" x2="9.3893" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="9.3893" y1="-1.2138" x2="10.7405" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="1.2367" x2="8.6336" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="8.6336" y1="1.2367" x2="9.4122" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="9.4122" y1="1.2367" x2="10.7405" y2="1.2367" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="1.7405" x2="8.6336" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="8.6336" y1="1.7405" x2="9.4122" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="9.4122" y1="1.7405" x2="10.7405" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="8.6336" y1="-1.7405" x2="8.6336" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="9.3893" y1="-1.7634" x2="9.3893" y2="-1.2138" width="0.2032" layer="51"/>
<wire x1="8.6336" y1="1.2367" x2="8.6336" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="9.4122" y1="1.2367" x2="9.4122" y2="1.7405" width="0.2032" layer="51"/>
<wire x1="7.2824" y1="3.5726" x2="8.2901" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="8.2901" y1="3.5726" x2="9.7557" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="9.7557" y1="3.5726" x2="10.7405" y2="3.5726" width="0.2032" layer="51"/>
<wire x1="8.2901" y1="3.5726" x2="8.2901" y2="4.397" width="0.2032" layer="51"/>
<wire x1="9.7557" y1="3.5726" x2="9.7557" y2="4.397" width="0.2032" layer="51"/>
<wire x1="-5.7443" y1="4.397" x2="-3.2099" y2="4.397" width="0.2032" layer="21"/>
<wire x1="-3.2176" y1="-4.397" x2="-5.7443" y2="-4.397" width="0.2032" layer="21"/>
<wire x1="5.7824" y1="-4.397" x2="3.2405" y2="-4.397" width="0.2032" layer="21"/>
<wire x1="3.2557" y1="4.397" x2="5.7901" y2="4.397" width="0.2032" layer="21"/>
<smd name="1" x="-9" y="0" dx="5.9" dy="9.4" layer="1"/>
<smd name="2" x="0" y="0" dx="5.9" dy="9.4" layer="1"/>
<smd name="3" x="9" y="0" dx="5.9" dy="9.4" layer="1"/>
<text x="-12.5" y="5.04" size="1.27" layer="25">&gt;NAME</text>
<text x="-13" y="-6.29" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0091760030" urn="urn:adsk.eagle:footprint:6228019/1" locally_modified="yes">
<description>&lt;b&gt;Wire to Board Strip IDC Connector&lt;/b&gt; 3 WAY&lt;p&gt;
Source: http://www.avx.com/docs/Catalogs/9176.pdf</description>
<wire x1="5.9169" y1="-2.4454" x2="4.989" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="4.989" y1="-2.4454" x2="4.4431" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="4.4431" y1="-2.4454" x2="3.4388" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="3.4388" y1="-2.4454" x2="2.9257" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="2.9257" y1="-2.4454" x2="1.0327" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="1.0327" y1="-2.4454" x2="-1.0306" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="-1.0306" y1="-2.4454" x2="-2.9127" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="-2.9127" y1="-2.4454" x2="-4.976" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="-4.976" y1="-2.4454" x2="-5.9171" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="-5.9171" y1="-2.4454" x2="-5.9171" y2="2.4563" width="0.1016" layer="21"/>
<wire x1="-5.9171" y1="2.4563" x2="-4.976" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-4.976" y1="2.4563" x2="-3.4476" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-3.4476" y1="2.4563" x2="-2.9127" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-2.9127" y1="2.4563" x2="-1.0306" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-1.0306" y1="2.4563" x2="1.0327" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="1.0327" y1="2.4563" x2="2.9257" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="2.9257" y1="2.4563" x2="3.4497" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="3.4497" y1="2.4563" x2="4.4541" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="4.4541" y1="2.4563" x2="4.989" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="4.989" y1="2.4563" x2="5.9169" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="5.9169" y1="2.4563" x2="5.9169" y2="-2.4454" width="0.1016" layer="21"/>
<wire x1="4.989" y1="-2.4454" x2="4.989" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="4.989" y1="-2.0415" x2="4.989" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="4.989" y1="-1.2664" x2="4.989" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="4.989" y1="-0.7205" x2="4.989" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="4.989" y1="0.7314" x2="4.989" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="4.989" y1="1.2773" x2="4.989" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="4.989" y1="2.0305" x2="4.989" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="2.9257" y1="-2.4454" x2="2.9257" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="2.9257" y1="-2.0415" x2="2.9257" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="2.9257" y1="-1.2664" x2="2.9257" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="2.9257" y1="-0.7205" x2="2.9257" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="2.9257" y1="0.7314" x2="2.9257" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="2.9257" y1="1.2773" x2="2.9257" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="2.9257" y1="2.0305" x2="2.9257" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="2.9257" y1="-2.0415" x2="3.4279" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="3.4279" y1="-2.0415" x2="3.4388" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="3.4388" y1="-2.0415" x2="3.4388" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="3.4279" y1="-2.0415" x2="4.4322" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="4.4322" y1="-2.0415" x2="4.4431" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="4.4431" y1="-2.0415" x2="4.4431" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="4.4322" y1="-2.0415" x2="4.989" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="4.989" y1="-0.7205" x2="4.3121" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="4.3121" y1="-0.7205" x2="3.5916" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="3.5916" y1="-0.7205" x2="2.9257" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="2.9257" y1="-1.2664" x2="3.5916" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="3.5916" y1="-1.2664" x2="4.3121" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="4.3121" y1="-1.2664" x2="4.989" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="4.3121" y1="-1.2664" x2="4.3121" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="3.5916" y1="-1.2664" x2="3.5916" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="2.9257" y1="0.7314" x2="3.5807" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="3.5807" y1="0.7314" x2="4.3121" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="4.3121" y1="0.7314" x2="4.989" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="4.989" y1="1.2773" x2="4.3121" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="4.3121" y1="1.2773" x2="3.5807" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="3.5807" y1="1.2773" x2="2.9257" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="3.5807" y1="0.7314" x2="3.5807" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="4.3121" y1="0.7314" x2="4.3121" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="4.989" y1="2.0305" x2="4.4541" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="4.4541" y1="2.0305" x2="3.4497" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="3.4497" y1="2.0305" x2="2.9257" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="3.4497" y1="2.0305" x2="3.4497" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="4.4541" y1="2.0305" x2="4.4541" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="1.0327" y1="-2.4454" x2="1.0327" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="1.0327" y1="-2.0415" x2="1.0327" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="1.0327" y1="-1.2664" x2="1.0327" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="1.0327" y1="-0.7205" x2="1.0327" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="1.0327" y1="0.7314" x2="1.0327" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="1.0327" y1="1.2773" x2="1.0327" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="1.0327" y1="2.0305" x2="1.0327" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-1.0306" y1="-2.4454" x2="-1.0306" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-1.0306" y1="-2.0415" x2="-1.0306" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="-1.0306" y1="-1.2664" x2="-1.0306" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-1.0306" y1="-0.7205" x2="-1.0306" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="-1.0306" y1="0.7314" x2="-1.0306" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-1.0306" y1="1.2773" x2="-1.0306" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-1.0306" y1="2.0305" x2="-1.0306" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-1.0306" y1="-2.0415" x2="-0.5284" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-0.5284" y1="-2.0415" x2="-0.5175" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-0.5175" y1="-2.0415" x2="-0.5175" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="-0.5284" y1="-2.0415" x2="0.4759" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="0.4759" y1="-2.0415" x2="0.4868" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="0.4868" y1="-2.0415" x2="0.4868" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="0.4759" y1="-2.0415" x2="1.0327" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="1.0327" y1="-0.7205" x2="0.3558" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="0.3558" y1="-0.7205" x2="-0.3647" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-0.3647" y1="-0.7205" x2="-1.0306" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-1.0306" y1="-1.2664" x2="-0.3647" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="-0.3647" y1="-1.2664" x2="0.3558" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="0.3558" y1="-1.2664" x2="1.0327" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="0.3558" y1="-1.2664" x2="0.3558" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-0.3647" y1="-1.2664" x2="-0.3647" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-1.0306" y1="0.7314" x2="-0.3756" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="-0.3756" y1="0.7314" x2="0.3558" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="0.3558" y1="0.7314" x2="1.0327" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="1.0327" y1="1.2773" x2="0.3558" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="0.3558" y1="1.2773" x2="-0.3756" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-0.3756" y1="1.2773" x2="-1.0306" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-0.3756" y1="0.7314" x2="-0.3756" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="0.3558" y1="0.7314" x2="0.3558" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="1.0327" y1="2.0305" x2="0.4978" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="0.4978" y1="2.0305" x2="-0.5066" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-0.5066" y1="2.0305" x2="-1.0306" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-0.5066" y1="2.0305" x2="-0.5066" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="0.4978" y1="2.0305" x2="0.4978" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-2.9127" y1="-2.4454" x2="-2.9127" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-2.9127" y1="-2.0415" x2="-2.9127" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="-2.9127" y1="-1.2664" x2="-2.9127" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-2.9127" y1="-0.7205" x2="-2.9127" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="-2.9127" y1="0.7314" x2="-2.9127" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-2.9127" y1="1.2773" x2="-2.9127" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-2.9127" y1="2.0305" x2="-2.9127" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-4.976" y1="-2.4454" x2="-4.976" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-4.976" y1="-2.0415" x2="-4.976" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="-4.976" y1="-1.2664" x2="-4.976" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-4.976" y1="-0.7205" x2="-4.976" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="-4.976" y1="0.7314" x2="-4.976" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-4.976" y1="1.2773" x2="-4.976" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-4.976" y1="2.0305" x2="-4.976" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-4.976" y1="-2.0415" x2="-4.4738" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-4.4738" y1="-2.0415" x2="-4.4629" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-4.4629" y1="-2.0415" x2="-4.4629" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="-4.4738" y1="-2.0415" x2="-3.4695" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-3.4695" y1="-2.0415" x2="-3.4586" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-3.4586" y1="-2.0415" x2="-3.4586" y2="-2.4454" width="0.1016" layer="51"/>
<wire x1="-3.4695" y1="-2.0415" x2="-2.9127" y2="-2.0415" width="0.1016" layer="51"/>
<wire x1="-2.9127" y1="-0.7205" x2="-3.5896" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-3.5896" y1="-0.7205" x2="-4.3101" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-4.3101" y1="-0.7205" x2="-4.976" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-4.976" y1="-1.2664" x2="-4.3101" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="-4.3101" y1="-1.2664" x2="-3.5896" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="-3.5896" y1="-1.2664" x2="-2.9127" y2="-1.2664" width="0.1016" layer="51"/>
<wire x1="-3.5896" y1="-1.2664" x2="-3.5896" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-4.3101" y1="-1.2664" x2="-4.3101" y2="-0.7205" width="0.1016" layer="51"/>
<wire x1="-4.976" y1="0.7314" x2="-4.321" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="-4.321" y1="0.7314" x2="-3.5896" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="-3.5896" y1="0.7314" x2="-2.9127" y2="0.7314" width="0.1016" layer="51"/>
<wire x1="-2.9127" y1="1.2773" x2="-3.5896" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-3.5896" y1="1.2773" x2="-4.321" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-4.321" y1="1.2773" x2="-4.976" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-4.321" y1="0.7314" x2="-4.321" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-3.5896" y1="0.7314" x2="-3.5896" y2="1.2773" width="0.1016" layer="51"/>
<wire x1="-2.9127" y1="2.0305" x2="-3.4476" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-3.4476" y1="2.0305" x2="-4.452" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-4.452" y1="2.0305" x2="-4.976" y2="2.0305" width="0.1016" layer="51"/>
<wire x1="-4.452" y1="2.0305" x2="-4.452" y2="2.4563" width="0.1016" layer="51"/>
<wire x1="-3.4476" y1="2.0305" x2="-3.4476" y2="2.4563" width="0.1016" layer="51"/>
<smd name="1" x="-4" y="0" dx="3" dy="5" layer="1"/>
<smd name="2" x="0" y="0" dx="3" dy="5" layer="1"/>
<smd name="3" x="4" y="0" dx="3" dy="5" layer="1"/>
<text x="-5.75" y="2.75" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.75" y="-4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0091750030" urn="urn:adsk.eagle:footprint:6228105/1" locally_modified="yes">
<description>&lt;b&gt;Wire to Board IDC Connector&lt;/b&gt; 3 WAY&lt;p&gt;
Source: http://www.avx.com/docs/Catalogs/9175.pdf</description>
<wire x1="-3.7" y1="1.2" x2="-2.7" y2="1.2" width="0.1016" layer="51"/>
<wire x1="-2.7" y1="1.2" x2="-2.7" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-2.7" y1="0.7" x2="-2.3" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-2.3" y1="0.7" x2="-2.3" y2="1.2" width="0.1016" layer="21"/>
<wire x1="-2.3" y1="1.2" x2="-0.2" y2="1.2" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="1.2" x2="-0.2" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="0.7" x2="0.2" y2="0.7" width="0.1016" layer="51"/>
<wire x1="0.2" y1="0.7" x2="0.2" y2="1.2" width="0.1016" layer="21"/>
<wire x1="0.2" y1="1.2" x2="1.2" y2="1.2" width="0.1016" layer="21"/>
<wire x1="1.2" y1="-1.2" x2="0.2" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="0.2" y1="-1.2" x2="0.2" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="0.2" y1="-0.7" x2="-0.2" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="-0.7" x2="-0.2" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-1.2" x2="-2.3" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="-2.3" y1="-1.2" x2="-2.3" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-2.3" y1="-0.7" x2="-2.7" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-2.7" y1="-0.7" x2="-2.7" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="-2.7" y1="-1.2" x2="-3.7" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="-3.7" y1="-1.2" x2="-3.7" y2="1.2" width="0.1016" layer="21"/>
<wire x1="-2.3" y1="0.7" x2="-2.05" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-2.05" y1="0.7" x2="-2.05" y2="0.15" width="0.1016" layer="21"/>
<wire x1="-2.05" y1="0.15" x2="-2.375" y2="0.15" width="0.1016" layer="21"/>
<wire x1="-2.375" y1="0.15" x2="-2.375" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-2.375" y1="-0.15" x2="-2.05" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-2.05" y1="-0.15" x2="-2.05" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-2.05" y1="-0.7" x2="-2.3" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-2.7" y1="-0.7" x2="-2.95" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="-0.7" x2="-2.95" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="-0.15" x2="-2.625" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-2.625" y1="-0.15" x2="-2.625" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-2.625" y1="0.15" x2="-2.95" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-2.95" y1="0.15" x2="-2.95" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-2.95" y1="0.7" x2="-2.7" y2="0.7" width="0.1016" layer="51"/>
<wire x1="0.2" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="0.15" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.15" x2="0.125" y2="0.15" width="0.1016" layer="21"/>
<wire x1="0.125" y1="0.15" x2="0.125" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="0.125" y1="-0.15" x2="0.45" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-0.15" x2="0.45" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-0.7" x2="0.2" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.15" x2="-0.125" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="-0.125" y1="-0.15" x2="-0.125" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-0.125" y1="0.15" x2="-0.45" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.15" x2="-0.45" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.7" x2="-0.2" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-2.95" y1="0.15" x2="-2.95" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-2.05" y1="0.15" x2="-2.05" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.15" x2="-0.45" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.15" x2="0.45" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-2.3" y1="1.2" x2="-1.2" y2="1.2" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-1.2" x2="-1.3" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="0.2" y1="1.2" x2="2.3" y2="1.2" width="0.1016" layer="51"/>
<wire x1="2.3" y1="1.2" x2="2.3" y2="0.7" width="0.1016" layer="51"/>
<wire x1="2.3" y1="0.7" x2="2.7" y2="0.7" width="0.1016" layer="51"/>
<wire x1="2.7" y1="0.7" x2="2.7" y2="1.2" width="0.1016" layer="21"/>
<wire x1="2.7" y1="1.2" x2="3.7" y2="1.2" width="0.1016" layer="21"/>
<wire x1="3.7" y1="1.2" x2="3.7" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="3.7" y1="-1.2" x2="2.7" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="2.7" y1="-1.2" x2="2.7" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="2.7" y1="-0.7" x2="2.3" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="2.3" y1="-0.7" x2="2.3" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="2.3" y1="-1.2" x2="0.2" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="2.7" y1="0.7" x2="2.95" y2="0.7" width="0.1016" layer="21"/>
<wire x1="2.95" y1="0.7" x2="2.95" y2="0.15" width="0.1016" layer="21"/>
<wire x1="2.95" y1="0.15" x2="2.625" y2="0.15" width="0.1016" layer="21"/>
<wire x1="2.625" y1="0.15" x2="2.625" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="2.625" y1="-0.15" x2="2.95" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="2.95" y1="-0.15" x2="2.95" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="2.95" y1="-0.7" x2="2.7" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="2.3" y1="-0.7" x2="2.05" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="2.05" y1="-0.15" x2="2.375" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="2.375" y1="-0.15" x2="2.375" y2="0.15" width="0.1016" layer="51"/>
<wire x1="2.375" y1="0.15" x2="2.05" y2="0.15" width="0.1016" layer="51"/>
<wire x1="2.05" y1="0.15" x2="2.05" y2="0.7" width="0.1016" layer="51"/>
<wire x1="2.05" y1="0.7" x2="2.3" y2="0.7" width="0.1016" layer="51"/>
<wire x1="2.05" y1="0.15" x2="2.05" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="2.05" y1="-0.7" x2="2.05" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="2.95" y1="0.15" x2="2.95" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="0.2" y1="1.2" x2="1.3" y2="1.2" width="0.1016" layer="21"/>
<wire x1="2.3" y1="-1.2" x2="1.2" y2="-1.2" width="0.1016" layer="21"/>
<smd name="1" x="-3.05" y="0.725" dx="1" dy="1.45" layer="1"/>
<smd name="1@1" x="-1.95" y="-0.725" dx="1" dy="1.45" layer="1"/>
<smd name="2" x="-0.55" y="0.725" dx="1" dy="1.45" layer="1"/>
<smd name="2@1" x="0.55" y="-0.725" dx="1" dy="1.45" layer="1"/>
<smd name="3" x="1.95" y="0.725" dx="1" dy="1.45" layer="1"/>
<smd name="3@2" x="3.05" y="-0.725" dx="1" dy="1.45" layer="1"/>
<text x="-3.75" y="1.75" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.75" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.7" y1="1.2" x2="-2.7" y2="1.2" width="0.1016" layer="51"/>
<wire x1="-2.7" y1="1.2" x2="-2.7" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-2.7" y1="0.7" x2="-2.3" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-2.3" y1="0.7" x2="-2.3" y2="1.2" width="0.1016" layer="21"/>
<wire x1="-2.3" y1="1.2" x2="-0.2" y2="1.2" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="1.2" x2="-0.2" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="0.7" x2="0.2" y2="0.7" width="0.1016" layer="51"/>
<wire x1="0.2" y1="0.7" x2="0.2" y2="1.2" width="0.1016" layer="21"/>
<wire x1="0.2" y1="1.2" x2="1.2" y2="1.2" width="0.1016" layer="21"/>
<wire x1="1.2" y1="-1.2" x2="0.2" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="0.2" y1="-1.2" x2="0.2" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="0.2" y1="-0.7" x2="-0.2" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="-0.7" x2="-0.2" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-1.2" x2="-2.3" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="-2.3" y1="-1.2" x2="-2.3" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-2.3" y1="-0.7" x2="-2.7" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-2.7" y1="-0.7" x2="-2.7" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="-2.7" y1="-1.2" x2="-3.7" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="-3.7" y1="-1.2" x2="-3.7" y2="1.2" width="0.1016" layer="21"/>
<wire x1="-2.3" y1="0.7" x2="-2.05" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-2.05" y1="0.7" x2="-2.05" y2="0.15" width="0.1016" layer="21"/>
<wire x1="-2.05" y1="0.15" x2="-2.375" y2="0.15" width="0.1016" layer="21"/>
<wire x1="-2.375" y1="0.15" x2="-2.375" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-2.375" y1="-0.15" x2="-2.05" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-2.05" y1="-0.15" x2="-2.05" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-2.05" y1="-0.7" x2="-2.3" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-2.7" y1="-0.7" x2="-2.95" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="-0.7" x2="-2.95" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="-0.15" x2="-2.625" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-2.625" y1="-0.15" x2="-2.625" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-2.625" y1="0.15" x2="-2.95" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-2.95" y1="0.15" x2="-2.95" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-2.95" y1="0.7" x2="-2.7" y2="0.7" width="0.1016" layer="51"/>
<wire x1="0.2" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="0.15" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.15" x2="0.125" y2="0.15" width="0.1016" layer="21"/>
<wire x1="0.125" y1="0.15" x2="0.125" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="0.125" y1="-0.15" x2="0.45" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-0.15" x2="0.45" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-0.7" x2="0.2" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.15" x2="-0.125" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="-0.125" y1="-0.15" x2="-0.125" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-0.125" y1="0.15" x2="-0.45" y2="0.15" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.15" x2="-0.45" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.7" x2="-0.2" y2="0.7" width="0.1016" layer="51"/>
<wire x1="-2.95" y1="0.15" x2="-2.95" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-2.05" y1="0.15" x2="-2.05" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.15" x2="-0.45" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.15" x2="0.45" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-2.3" y1="1.2" x2="-1.2" y2="1.2" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-1.2" x2="-1.3" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="0.2" y1="1.2" x2="2.3" y2="1.2" width="0.1016" layer="51"/>
<wire x1="2.3" y1="1.2" x2="2.3" y2="0.7" width="0.1016" layer="51"/>
<wire x1="2.3" y1="0.7" x2="2.7" y2="0.7" width="0.1016" layer="51"/>
<wire x1="2.7" y1="0.7" x2="2.7" y2="1.2" width="0.1016" layer="21"/>
<wire x1="2.7" y1="1.2" x2="3.7" y2="1.2" width="0.1016" layer="21"/>
<wire x1="3.7" y1="1.2" x2="3.7" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="3.7" y1="-1.2" x2="2.7" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="2.7" y1="-1.2" x2="2.7" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="2.7" y1="-0.7" x2="2.3" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="2.3" y1="-0.7" x2="2.3" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="2.3" y1="-1.2" x2="0.2" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="2.7" y1="0.7" x2="2.95" y2="0.7" width="0.1016" layer="21"/>
<wire x1="2.95" y1="0.7" x2="2.95" y2="0.15" width="0.1016" layer="21"/>
<wire x1="2.95" y1="0.15" x2="2.625" y2="0.15" width="0.1016" layer="21"/>
<wire x1="2.625" y1="0.15" x2="2.625" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="2.625" y1="-0.15" x2="2.95" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="2.95" y1="-0.15" x2="2.95" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="2.95" y1="-0.7" x2="2.7" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="2.3" y1="-0.7" x2="2.05" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="2.05" y1="-0.15" x2="2.375" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="2.375" y1="-0.15" x2="2.375" y2="0.15" width="0.1016" layer="51"/>
<wire x1="2.375" y1="0.15" x2="2.05" y2="0.15" width="0.1016" layer="51"/>
<wire x1="2.05" y1="0.15" x2="2.05" y2="0.7" width="0.1016" layer="51"/>
<wire x1="2.05" y1="0.7" x2="2.3" y2="0.7" width="0.1016" layer="51"/>
<wire x1="2.05" y1="0.15" x2="2.05" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="2.05" y1="-0.7" x2="2.05" y2="-0.15" width="0.1016" layer="21"/>
<wire x1="2.95" y1="0.15" x2="2.95" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="0.2" y1="1.2" x2="1.3" y2="1.2" width="0.1016" layer="21"/>
<wire x1="2.3" y1="-1.2" x2="1.2" y2="-1.2" width="0.1016" layer="21"/>
<smd name="4" x="-3.05" y="0.725" dx="1" dy="1.45" layer="1"/>
<smd name="1@2" x="-1.95" y="-0.725" dx="1" dy="1.45" layer="1"/>
<smd name="5" x="-0.55" y="0.725" dx="1" dy="1.45" layer="1"/>
<smd name="2@2" x="0.55" y="-0.725" dx="1" dy="1.45" layer="1"/>
<smd name="6" x="1.95" y="0.725" dx="1" dy="1.45" layer="1"/>
<smd name="3@1" x="3.05" y="-0.725" dx="1" dy="1.45" layer="1"/>
<text x="-3.75" y="1.75" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.75" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="009177002" urn="urn:adsk.eagle:package:6132/1" type="box">
<description>Wire to Board IDC Connector 2 WAY
Source: http://www.avx.com/docs/Catalogs/9177.pdf</description>
<packageinstances>
<packageinstance name="009177002"/>
</packageinstances>
</package3d>
<package3d name="009176002A" urn="urn:adsk.eagle:package:6228045/2" locally_modified="yes" type="model">
<description>&lt;b&gt;Wire to Board Strip IDC Connector&lt;/b&gt; 2 WAY&lt;p&gt;
Source: http://www.avx.com/docs/Catalogs/9176.pdf</description>
<packageinstances>
<packageinstance name="0091760020"/>
</packageinstances>
</package3d>
<package3d name="009175002A" urn="urn:adsk.eagle:package:6228087/2" locally_modified="yes" type="model">
<description>&lt;b&gt;Wire to Board IDC Connector&lt;/b&gt; 2 WAY&lt;p&gt;
Source: http://www.avx.com/docs/Catalogs/9175.pdf</description>
<packageinstances>
<packageinstance name="0091750020"/>
</packageinstances>
</package3d>
<package3d name="0091750027" urn="urn:adsk.eagle:package:6339550/3" type="model">
<packageinstances>
<packageinstance name="0091750027"/>
</packageinstances>
</package3d>
<package3d name="009177003" urn="urn:adsk.eagle:package:6131/1" type="box">
<description>Wire to Board IDC Connector 3 WAY
Source: http://www.avx.com/docs/Catalogs/9177.pdf</description>
<packageinstances>
<packageinstance name="009177003"/>
</packageinstances>
</package3d>
<package3d name="009176003A" urn="urn:adsk.eagle:package:6228026/2" locally_modified="yes" type="model">
<description>&lt;b&gt;Wire to Board Strip IDC Connector&lt;/b&gt; 3 WAY&lt;p&gt;
Source: http://www.avx.com/docs/Catalogs/9176.pdf</description>
<packageinstances>
<packageinstance name="0091760030"/>
</packageinstances>
</package3d>
<package3d name="009175003A" urn="urn:adsk.eagle:package:6228114/2" locally_modified="yes" type="model">
<description>&lt;b&gt;Wire to Board IDC Connector&lt;/b&gt; 3 WAY&lt;p&gt;
Source: http://www.avx.com/docs/Catalogs/9175.pdf</description>
<packageinstances>
<packageinstance name="0091750030"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="2-PIN">
<wire x1="0.635" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0" y1="-0.508" x2="0.635" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.508" x2="1.524" y2="-0.508" width="0.254" layer="94" curve="-120.510237"/>
<wire x1="1.524" y1="0.508" x2="0.635" y2="0.508" width="0.254" layer="94" curve="-120.510237"/>
<wire x1="0.635" y1="-2.032" x2="0" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="0.635" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0.635" y1="-3.048" x2="1.524" y2="-3.048" width="0.254" layer="94" curve="-120.510237"/>
<wire x1="1.524" y1="-2.032" x2="0.635" y2="-2.032" width="0.254" layer="94" curve="-120.510237"/>
<text x="0" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pin" length="short" direction="pas"/>
<pin name="2" x="-2.54" y="-2.54" visible="pin" length="short" direction="pas"/>
</symbol>
<symbol name="3-PIN">
<wire x1="0.635" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0" y1="-0.508" x2="0.635" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.508" x2="1.524" y2="-0.508" width="0.254" layer="94" curve="-120.510237"/>
<wire x1="1.524" y1="0.508" x2="0.635" y2="0.508" width="0.254" layer="94" curve="-120.510237"/>
<wire x1="0.635" y1="-2.032" x2="0" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="0.635" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0.635" y1="-3.048" x2="1.524" y2="-3.048" width="0.254" layer="94" curve="-120.510237"/>
<wire x1="1.524" y1="-2.032" x2="0.635" y2="-2.032" width="0.254" layer="94" curve="-120.510237"/>
<wire x1="0.635" y1="-4.572" x2="0" y2="-4.572" width="0.254" layer="94"/>
<wire x1="0" y1="-4.572" x2="0" y2="-5.588" width="0.254" layer="94"/>
<wire x1="0" y1="-5.588" x2="0.635" y2="-5.588" width="0.254" layer="94"/>
<wire x1="0.635" y1="-5.588" x2="1.524" y2="-5.588" width="0.254" layer="94" curve="-120.510237"/>
<wire x1="1.524" y1="-4.572" x2="0.635" y2="-4.572" width="0.254" layer="94" curve="-120.510237"/>
<text x="0" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-8.89" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pin" length="short" direction="pas"/>
<pin name="2" x="-2.54" y="-2.54" visible="pin" length="short" direction="pas"/>
<pin name="3" x="-2.54" y="-5.08" visible="pin" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="00917?002*" prefix="X">
<description>&lt;b&gt;Wire to Board Strip IDC Connector&lt;/b&gt; 2 WAY&lt;p&gt;
Source: http://www.avx.com/docs/Catalogs/9176.pdf&lt;br&gt;
http://www.avx.com/docs/Catalogs/9177.pdf</description>
<gates>
<gate name="G$1" symbol="2-PIN" x="0" y="0"/>
</gates>
<devices>
<device name="7" package="009177002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6132/1"/>
</package3dinstances>
<technologies>
<technology name="001006">
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009177002001006" constant="no"/>
</technology>
</technologies>
</device>
<device name="6" package="0091760020">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6228045/2"/>
</package3dinstances>
<technologies>
<technology name="001006">
<attribute name="DESCRIPTION" value="IDC connector, 2-way, AWG 18" constant="no"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009176002001006" constant="no"/>
</technology>
<technology name="011006">
<attribute name="DESCRIPTION" value="IDC connector, 2-way, AWG 20" constant="no"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009176002011006" constant="no"/>
</technology>
<technology name="022006">
<attribute name="DESCRIPTION" value="IDC connector, 2-way, AWG 22" constant="no"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009176002022006" constant="no"/>
</technology>
<technology name="032006">
<attribute name="DESCRIPTION" value="IDC connector, 2-way, AWG 24" constant="no"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009176002032006" constant="no"/>
</technology>
</technologies>
</device>
<device name="5" package="0091750020">
<connects>
<connect gate="G$1" pin="1" pad="1 1@1"/>
<connect gate="G$1" pin="2" pad="2 2@1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6228087/2"/>
</package3dinstances>
<technologies>
<technology name="001006">
<attribute name="DESCRIPTION" value="IDC connector, 2-way, AWG 28" constant="no"/>
<attribute name="MF" value="AVX" constant="no"/>
<attribute name="MPN" value="009175002001006" constant="no"/>
</technology>
<technology name="002006">
<attribute name="DESCRIPTION" value="IDC connector, 2-way, AWG 26" constant="no"/>
<attribute name="MF" value="AVX" constant="no"/>
<attribute name="MPN" value="009175002002006" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="00917500270*" prefix="X">
<gates>
<gate name="X" symbol="2-PIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0091750027">
<connects>
<connect gate="X" pin="1" pad="1 1@1"/>
<connect gate="X" pin="2" pad="2 2@1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6339550/3"/>
</package3dinstances>
<technologies>
<technology name="1196">
<attribute name="DESCRIPTION" value="IDC connector, 2-way, AWG 28, wire stop" constant="no"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009175002701196" constant="no"/>
</technology>
<technology name="2996">
<attribute name="DESCRIPTION" value="IDC connector, 2-way, AWG 26, wire stop" constant="no"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009175002702996" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="00917?003*" prefix="X">
<description>&lt;b&gt;Wire to Board Strip IDC Connector&lt;/b&gt; 3 WAY&lt;p&gt;
Source: http://www.avx.com/docs/Catalogs/9176.pdf&lt;br&gt;
http://www.avx.com/docs/Catalogs/9177.pdf</description>
<gates>
<gate name="G$1" symbol="3-PIN" x="0" y="0"/>
</gates>
<devices>
<device name="7" package="009177003">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6131/1"/>
</package3dinstances>
<technologies>
<technology name="001006">
<attribute name="MF" value="AVX" constant="no"/>
<attribute name="MPN" value="009177003001006" constant="no"/>
</technology>
</technologies>
</device>
<device name="6" package="0091760030">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6228026/2"/>
</package3dinstances>
<technologies>
<technology name="001006">
<attribute name="DESCRIPTION" value="IDC connector, 3-way, AWG 18" constant="no"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009176003001006" constant="no"/>
</technology>
<technology name="011006">
<attribute name="DESCRIPTION" value="IDC connector, 3-way, AWG 20" constant="no"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009176003011006" constant="no"/>
</technology>
<technology name="022006">
<attribute name="DESCRIPTION" value="IDC connector, 3-way, AWG 22" constant="no"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009176003022006" constant="no"/>
</technology>
<technology name="032006">
<attribute name="DESCRIPTION" value="IDC connector, 3-way, AWG 24" constant="no"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009176003032006" constant="no"/>
</technology>
</technologies>
</device>
<device name="5" package="0091750030">
<connects>
<connect gate="G$1" pin="1" pad="1 1@1"/>
<connect gate="G$1" pin="2" pad="2 2@1"/>
<connect gate="G$1" pin="3" pad="3 3@2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6228114/2"/>
</package3dinstances>
<technologies>
<technology name="001006">
<attribute name="DESCRIPTION" value="IDC connector, 3-way, AWG 28" constant="no"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009175003001006" constant="no"/>
</technology>
<technology name="002006">
<attribute name="DESCRIPTION" value="IDC connector, 3-way, AWG 26" constant="no"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="009175003002006" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="AGND" urn="urn:adsk.eagle:symbol:26949/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.0922" y1="-0.508" x2="1.0922" y2="-0.508" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="AGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AGND" urn="urn:adsk.eagle:component:26977/1" prefix="AGND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VR1" symbol="AGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-logo">
<packages>
<package name="UDO-LOGO-15MM" urn="urn:adsk.eagle:footprint:6649249/1">
<rectangle x1="2.24281875" y1="0.21158125" x2="2.58318125" y2="0.29641875" layer="21"/>
<rectangle x1="10.71118125" y1="0.21158125" x2="11.049" y2="0.29641875" layer="21"/>
<rectangle x1="1.98881875" y1="0.29641875" x2="2.83718125" y2="0.381" layer="21"/>
<rectangle x1="3.25881875" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="4.10718125" y1="0.29641875" x2="4.27481875" y2="0.381" layer="21"/>
<rectangle x1="4.445" y1="0.29641875" x2="4.52881875" y2="0.381" layer="21"/>
<rectangle x1="5.12318125" y1="0.29641875" x2="5.37718125" y2="0.381" layer="21"/>
<rectangle x1="5.63118125" y1="0.29641875" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="7.32281875" y1="0.29641875" x2="7.747" y2="0.381" layer="21"/>
<rectangle x1="8.33881875" y1="0.29641875" x2="8.67918125" y2="0.381" layer="21"/>
<rectangle x1="9.18718125" y1="0.29641875" x2="9.60881875" y2="0.381" layer="21"/>
<rectangle x1="10.45718125" y1="0.29641875" x2="11.303" y2="0.381" layer="21"/>
<rectangle x1="11.72718125" y1="0.29641875" x2="11.89481875" y2="0.381" layer="21"/>
<rectangle x1="12.573" y1="0.29641875" x2="12.74318125" y2="0.381" layer="21"/>
<rectangle x1="13.081" y1="0.29641875" x2="13.50518125" y2="0.381" layer="21"/>
<rectangle x1="1.905" y1="0.381" x2="2.24281875" y2="0.46558125" layer="21"/>
<rectangle x1="2.58318125" y1="0.381" x2="2.921" y2="0.46558125" layer="21"/>
<rectangle x1="3.25881875" y1="0.381" x2="3.429" y2="0.46558125" layer="21"/>
<rectangle x1="4.10718125" y1="0.381" x2="4.27481875" y2="0.46558125" layer="21"/>
<rectangle x1="4.445" y1="0.381" x2="4.61518125" y2="0.46558125" layer="21"/>
<rectangle x1="5.03681875" y1="0.381" x2="5.37718125" y2="0.46558125" layer="21"/>
<rectangle x1="5.63118125" y1="0.381" x2="6.56081875" y2="0.46558125" layer="21"/>
<rectangle x1="7.15518125" y1="0.381" x2="7.83081875" y2="0.46558125" layer="21"/>
<rectangle x1="8.17118125" y1="0.381" x2="8.763" y2="0.46558125" layer="21"/>
<rectangle x1="9.10081875" y1="0.381" x2="9.779" y2="0.46558125" layer="21"/>
<rectangle x1="10.37081875" y1="0.381" x2="10.71118125" y2="0.46558125" layer="21"/>
<rectangle x1="11.049" y1="0.381" x2="11.38681875" y2="0.46558125" layer="21"/>
<rectangle x1="11.72718125" y1="0.381" x2="11.89481875" y2="0.46558125" layer="21"/>
<rectangle x1="12.573" y1="0.381" x2="12.74318125" y2="0.46558125" layer="21"/>
<rectangle x1="12.99718125" y1="0.381" x2="13.67281875" y2="0.46558125" layer="21"/>
<rectangle x1="1.82118125" y1="0.46558125" x2="2.07518125" y2="0.55041875" layer="21"/>
<rectangle x1="2.75081875" y1="0.46558125" x2="3.00481875" y2="0.55041875" layer="21"/>
<rectangle x1="3.25881875" y1="0.46558125" x2="3.429" y2="0.55041875" layer="21"/>
<rectangle x1="4.10718125" y1="0.46558125" x2="4.27481875" y2="0.55041875" layer="21"/>
<rectangle x1="4.445" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="21"/>
<rectangle x1="4.953" y1="0.46558125" x2="5.12318125" y2="0.55041875" layer="21"/>
<rectangle x1="5.63118125" y1="0.46558125" x2="5.79881875" y2="0.55041875" layer="21"/>
<rectangle x1="6.39318125" y1="0.46558125" x2="6.731" y2="0.55041875" layer="21"/>
<rectangle x1="7.06881875" y1="0.46558125" x2="7.32281875" y2="0.55041875" layer="21"/>
<rectangle x1="7.66318125" y1="0.46558125" x2="7.91718125" y2="0.55041875" layer="21"/>
<rectangle x1="8.17118125" y1="0.46558125" x2="8.33881875" y2="0.55041875" layer="21"/>
<rectangle x1="8.59281875" y1="0.46558125" x2="8.84681875" y2="0.55041875" layer="21"/>
<rectangle x1="9.017" y1="0.46558125" x2="9.271" y2="0.55041875" layer="21"/>
<rectangle x1="9.60881875" y1="0.46558125" x2="9.86281875" y2="0.55041875" layer="21"/>
<rectangle x1="10.287" y1="0.46558125" x2="10.541" y2="0.55041875" layer="21"/>
<rectangle x1="11.21918125" y1="0.46558125" x2="11.47318125" y2="0.55041875" layer="21"/>
<rectangle x1="11.72718125" y1="0.46558125" x2="11.89481875" y2="0.55041875" layer="21"/>
<rectangle x1="12.573" y1="0.46558125" x2="12.74318125" y2="0.55041875" layer="21"/>
<rectangle x1="12.91081875" y1="0.46558125" x2="13.16481875" y2="0.55041875" layer="21"/>
<rectangle x1="13.50518125" y1="0.46558125" x2="13.75918125" y2="0.55041875" layer="21"/>
<rectangle x1="1.73481875" y1="0.55041875" x2="1.98881875" y2="0.635" layer="21"/>
<rectangle x1="2.83718125" y1="0.55041875" x2="3.09118125" y2="0.635" layer="21"/>
<rectangle x1="3.25881875" y1="0.55041875" x2="3.429" y2="0.635" layer="21"/>
<rectangle x1="4.10718125" y1="0.55041875" x2="4.27481875" y2="0.635" layer="21"/>
<rectangle x1="4.445" y1="0.55041875" x2="4.61518125" y2="0.635" layer="21"/>
<rectangle x1="4.86918125" y1="0.55041875" x2="5.03681875" y2="0.635" layer="21"/>
<rectangle x1="5.63118125" y1="0.55041875" x2="5.79881875" y2="0.635" layer="21"/>
<rectangle x1="6.56081875" y1="0.55041875" x2="6.81481875" y2="0.635" layer="21"/>
<rectangle x1="7.06881875" y1="0.55041875" x2="7.239" y2="0.635" layer="21"/>
<rectangle x1="7.747" y1="0.55041875" x2="8.001" y2="0.635" layer="21"/>
<rectangle x1="8.17118125" y1="0.55041875" x2="8.255" y2="0.635" layer="21"/>
<rectangle x1="8.67918125" y1="0.55041875" x2="8.84681875" y2="0.635" layer="21"/>
<rectangle x1="8.93318125" y1="0.55041875" x2="9.18718125" y2="0.635" layer="21"/>
<rectangle x1="9.69518125" y1="0.55041875" x2="9.86281875" y2="0.635" layer="21"/>
<rectangle x1="10.20318125" y1="0.55041875" x2="10.45718125" y2="0.635" layer="21"/>
<rectangle x1="11.303" y1="0.55041875" x2="11.557" y2="0.635" layer="21"/>
<rectangle x1="11.72718125" y1="0.55041875" x2="11.89481875" y2="0.635" layer="21"/>
<rectangle x1="12.573" y1="0.55041875" x2="12.74318125" y2="0.635" layer="21"/>
<rectangle x1="12.827" y1="0.55041875" x2="13.081" y2="0.635" layer="21"/>
<rectangle x1="13.589" y1="0.55041875" x2="13.75918125" y2="0.635" layer="21"/>
<rectangle x1="1.73481875" y1="0.635" x2="1.905" y2="0.71958125" layer="21"/>
<rectangle x1="2.921" y1="0.635" x2="3.09118125" y2="0.71958125" layer="21"/>
<rectangle x1="3.25881875" y1="0.635" x2="3.429" y2="0.71958125" layer="21"/>
<rectangle x1="4.10718125" y1="0.635" x2="4.27481875" y2="0.71958125" layer="21"/>
<rectangle x1="4.445" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="4.86918125" y1="0.635" x2="5.03681875" y2="0.71958125" layer="21"/>
<rectangle x1="5.63118125" y1="0.635" x2="5.79881875" y2="0.71958125" layer="21"/>
<rectangle x1="6.64718125" y1="0.635" x2="6.81481875" y2="0.71958125" layer="21"/>
<rectangle x1="6.985" y1="0.635" x2="7.239" y2="0.71958125" layer="21"/>
<rectangle x1="7.83081875" y1="0.635" x2="8.001" y2="0.71958125" layer="21"/>
<rectangle x1="8.59281875" y1="0.635" x2="8.84681875" y2="0.71958125" layer="21"/>
<rectangle x1="8.93318125" y1="0.635" x2="9.10081875" y2="0.71958125" layer="21"/>
<rectangle x1="10.20318125" y1="0.635" x2="10.37081875" y2="0.71958125" layer="21"/>
<rectangle x1="11.38681875" y1="0.635" x2="11.557" y2="0.71958125" layer="21"/>
<rectangle x1="11.72718125" y1="0.635" x2="11.89481875" y2="0.71958125" layer="21"/>
<rectangle x1="12.573" y1="0.635" x2="12.74318125" y2="0.71958125" layer="21"/>
<rectangle x1="12.827" y1="0.635" x2="12.99718125" y2="0.71958125" layer="21"/>
<rectangle x1="1.73481875" y1="0.71958125" x2="1.905" y2="0.80441875" layer="21"/>
<rectangle x1="2.921" y1="0.71958125" x2="3.175" y2="0.80441875" layer="21"/>
<rectangle x1="3.25881875" y1="0.71958125" x2="3.429" y2="0.80441875" layer="21"/>
<rectangle x1="4.10718125" y1="0.71958125" x2="4.27481875" y2="0.80441875" layer="21"/>
<rectangle x1="4.445" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="4.78281875" y1="0.71958125" x2="4.953" y2="0.80441875" layer="21"/>
<rectangle x1="5.63118125" y1="0.71958125" x2="5.79881875" y2="0.80441875" layer="21"/>
<rectangle x1="6.731" y1="0.71958125" x2="6.90118125" y2="0.80441875" layer="21"/>
<rectangle x1="6.985" y1="0.71958125" x2="7.15518125" y2="0.80441875" layer="21"/>
<rectangle x1="7.83081875" y1="0.71958125" x2="8.001" y2="0.80441875" layer="21"/>
<rectangle x1="8.33881875" y1="0.71958125" x2="8.763" y2="0.80441875" layer="21"/>
<rectangle x1="8.93318125" y1="0.71958125" x2="9.94918125" y2="0.80441875" layer="21"/>
<rectangle x1="10.20318125" y1="0.71958125" x2="10.37081875" y2="0.80441875" layer="21"/>
<rectangle x1="11.38681875" y1="0.71958125" x2="11.557" y2="0.80441875" layer="21"/>
<rectangle x1="11.72718125" y1="0.71958125" x2="11.89481875" y2="0.80441875" layer="21"/>
<rectangle x1="12.573" y1="0.71958125" x2="12.74318125" y2="0.80441875" layer="21"/>
<rectangle x1="12.827" y1="0.71958125" x2="13.843" y2="0.80441875" layer="21"/>
<rectangle x1="1.651" y1="0.80441875" x2="1.82118125" y2="0.889" layer="21"/>
<rectangle x1="3.00481875" y1="0.80441875" x2="3.175" y2="0.889" layer="21"/>
<rectangle x1="3.25881875" y1="0.80441875" x2="3.429" y2="0.889" layer="21"/>
<rectangle x1="4.10718125" y1="0.80441875" x2="4.27481875" y2="0.889" layer="21"/>
<rectangle x1="4.445" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="4.78281875" y1="0.80441875" x2="4.953" y2="0.889" layer="21"/>
<rectangle x1="5.63118125" y1="0.80441875" x2="5.79881875" y2="0.889" layer="21"/>
<rectangle x1="6.731" y1="0.80441875" x2="6.90118125" y2="0.889" layer="21"/>
<rectangle x1="6.985" y1="0.80441875" x2="7.15518125" y2="0.889" layer="21"/>
<rectangle x1="7.83081875" y1="0.80441875" x2="8.001" y2="0.889" layer="21"/>
<rectangle x1="8.17118125" y1="0.80441875" x2="8.59281875" y2="0.889" layer="21"/>
<rectangle x1="8.93318125" y1="0.80441875" x2="9.94918125" y2="0.889" layer="21"/>
<rectangle x1="10.11681875" y1="0.80441875" x2="10.287" y2="0.889" layer="21"/>
<rectangle x1="11.47318125" y1="0.80441875" x2="11.64081875" y2="0.889" layer="21"/>
<rectangle x1="11.72718125" y1="0.80441875" x2="11.89481875" y2="0.889" layer="21"/>
<rectangle x1="12.573" y1="0.80441875" x2="12.74318125" y2="0.889" layer="21"/>
<rectangle x1="12.827" y1="0.80441875" x2="13.843" y2="0.889" layer="21"/>
<rectangle x1="1.651" y1="0.889" x2="1.82118125" y2="0.97358125" layer="21"/>
<rectangle x1="3.00481875" y1="0.889" x2="3.175" y2="0.97358125" layer="21"/>
<rectangle x1="3.25881875" y1="0.889" x2="3.51281875" y2="0.97358125" layer="21"/>
<rectangle x1="4.10718125" y1="0.889" x2="4.27481875" y2="0.97358125" layer="21"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.61518125" y2="0.97358125" layer="21"/>
<rectangle x1="4.78281875" y1="0.889" x2="4.953" y2="0.97358125" layer="21"/>
<rectangle x1="5.63118125" y1="0.889" x2="5.79881875" y2="0.97358125" layer="21"/>
<rectangle x1="6.731" y1="0.889" x2="6.90118125" y2="0.97358125" layer="21"/>
<rectangle x1="6.985" y1="0.889" x2="7.239" y2="0.97358125" layer="21"/>
<rectangle x1="7.83081875" y1="0.889" x2="8.001" y2="0.97358125" layer="21"/>
<rectangle x1="8.08481875" y1="0.889" x2="8.42518125" y2="0.97358125" layer="21"/>
<rectangle x1="8.93318125" y1="0.889" x2="9.10081875" y2="0.97358125" layer="21"/>
<rectangle x1="9.779" y1="0.889" x2="9.94918125" y2="0.97358125" layer="21"/>
<rectangle x1="10.11681875" y1="0.889" x2="10.287" y2="0.97358125" layer="21"/>
<rectangle x1="11.47318125" y1="0.889" x2="11.64081875" y2="0.97358125" layer="21"/>
<rectangle x1="11.72718125" y1="0.889" x2="11.89481875" y2="0.97358125" layer="21"/>
<rectangle x1="12.48918125" y1="0.889" x2="12.74318125" y2="0.97358125" layer="21"/>
<rectangle x1="12.827" y1="0.889" x2="12.99718125" y2="0.97358125" layer="21"/>
<rectangle x1="13.589" y1="0.889" x2="13.843" y2="0.97358125" layer="21"/>
<rectangle x1="1.651" y1="0.97358125" x2="1.82118125" y2="1.05841875" layer="21"/>
<rectangle x1="3.00481875" y1="0.97358125" x2="3.175" y2="1.05841875" layer="21"/>
<rectangle x1="3.25881875" y1="0.97358125" x2="3.51281875" y2="1.05841875" layer="21"/>
<rectangle x1="4.02081875" y1="0.97358125" x2="4.27481875" y2="1.05841875" layer="21"/>
<rectangle x1="4.36118125" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="21"/>
<rectangle x1="4.78281875" y1="0.97358125" x2="4.953" y2="1.05841875" layer="21"/>
<rectangle x1="5.63118125" y1="0.97358125" x2="5.79881875" y2="1.05841875" layer="21"/>
<rectangle x1="6.731" y1="0.97358125" x2="6.90118125" y2="1.05841875" layer="21"/>
<rectangle x1="7.06881875" y1="0.97358125" x2="7.239" y2="1.05841875" layer="21"/>
<rectangle x1="7.747" y1="0.97358125" x2="8.001" y2="1.05841875" layer="21"/>
<rectangle x1="8.08481875" y1="0.97358125" x2="8.255" y2="1.05841875" layer="21"/>
<rectangle x1="8.67918125" y1="0.97358125" x2="8.84681875" y2="1.05841875" layer="21"/>
<rectangle x1="8.93318125" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="21"/>
<rectangle x1="9.69518125" y1="0.97358125" x2="9.86281875" y2="1.05841875" layer="21"/>
<rectangle x1="10.11681875" y1="0.97358125" x2="10.287" y2="1.05841875" layer="21"/>
<rectangle x1="11.47318125" y1="0.97358125" x2="11.64081875" y2="1.05841875" layer="21"/>
<rectangle x1="11.72718125" y1="0.97358125" x2="11.98118125" y2="1.05841875" layer="21"/>
<rectangle x1="12.48918125" y1="0.97358125" x2="12.65681875" y2="1.05841875" layer="21"/>
<rectangle x1="12.827" y1="0.97358125" x2="12.99718125" y2="1.05841875" layer="21"/>
<rectangle x1="13.589" y1="0.97358125" x2="13.75918125" y2="1.05841875" layer="21"/>
<rectangle x1="1.651" y1="1.05841875" x2="1.82118125" y2="1.143" layer="21"/>
<rectangle x1="3.00481875" y1="1.05841875" x2="3.175" y2="1.143" layer="21"/>
<rectangle x1="3.25881875" y1="1.05841875" x2="3.59918125" y2="1.143" layer="21"/>
<rectangle x1="3.937" y1="1.05841875" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="4.36118125" y1="1.05841875" x2="4.61518125" y2="1.143" layer="21"/>
<rectangle x1="4.78281875" y1="1.05841875" x2="4.953" y2="1.143" layer="21"/>
<rectangle x1="5.63118125" y1="1.05841875" x2="5.79881875" y2="1.143" layer="21"/>
<rectangle x1="6.731" y1="1.05841875" x2="6.90118125" y2="1.143" layer="21"/>
<rectangle x1="7.06881875" y1="1.05841875" x2="7.32281875" y2="1.143" layer="21"/>
<rectangle x1="7.66318125" y1="1.05841875" x2="7.91718125" y2="1.143" layer="21"/>
<rectangle x1="8.08481875" y1="1.05841875" x2="8.33881875" y2="1.143" layer="21"/>
<rectangle x1="8.59281875" y1="1.05841875" x2="8.84681875" y2="1.143" layer="21"/>
<rectangle x1="9.017" y1="1.05841875" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="9.60881875" y1="1.05841875" x2="9.86281875" y2="1.143" layer="21"/>
<rectangle x1="10.11681875" y1="1.05841875" x2="10.287" y2="1.143" layer="21"/>
<rectangle x1="11.47318125" y1="1.05841875" x2="11.64081875" y2="1.143" layer="21"/>
<rectangle x1="11.72718125" y1="1.05841875" x2="12.065" y2="1.143" layer="21"/>
<rectangle x1="12.40281875" y1="1.05841875" x2="12.65681875" y2="1.143" layer="21"/>
<rectangle x1="12.91081875" y1="1.05841875" x2="13.16481875" y2="1.143" layer="21"/>
<rectangle x1="13.50518125" y1="1.05841875" x2="13.75918125" y2="1.143" layer="21"/>
<rectangle x1="1.651" y1="1.143" x2="1.82118125" y2="1.22758125" layer="21"/>
<rectangle x1="3.00481875" y1="1.143" x2="3.175" y2="1.22758125" layer="21"/>
<rectangle x1="3.25881875" y1="1.143" x2="4.10718125" y2="1.22758125" layer="21"/>
<rectangle x1="4.36118125" y1="1.143" x2="4.61518125" y2="1.22758125" layer="21"/>
<rectangle x1="4.699" y1="1.143" x2="5.37718125" y2="1.22758125" layer="21"/>
<rectangle x1="5.63118125" y1="1.143" x2="5.79881875" y2="1.22758125" layer="21"/>
<rectangle x1="6.731" y1="1.143" x2="6.90118125" y2="1.22758125" layer="21"/>
<rectangle x1="7.15518125" y1="1.143" x2="7.83081875" y2="1.22758125" layer="21"/>
<rectangle x1="8.17118125" y1="1.143" x2="8.763" y2="1.22758125" layer="21"/>
<rectangle x1="9.10081875" y1="1.143" x2="9.779" y2="1.22758125" layer="21"/>
<rectangle x1="10.20318125" y1="1.143" x2="10.37081875" y2="1.22758125" layer="21"/>
<rectangle x1="11.38681875" y1="1.143" x2="11.557" y2="1.22758125" layer="21"/>
<rectangle x1="11.72718125" y1="1.143" x2="12.573" y2="1.22758125" layer="21"/>
<rectangle x1="12.99718125" y1="1.143" x2="13.67281875" y2="1.22758125" layer="21"/>
<rectangle x1="1.651" y1="1.22758125" x2="1.82118125" y2="1.31241875" layer="21"/>
<rectangle x1="3.00481875" y1="1.22758125" x2="3.175" y2="1.31241875" layer="21"/>
<rectangle x1="3.25881875" y1="1.22758125" x2="3.429" y2="1.31241875" layer="21"/>
<rectangle x1="3.59918125" y1="1.22758125" x2="4.02081875" y2="1.31241875" layer="21"/>
<rectangle x1="4.445" y1="1.22758125" x2="4.52881875" y2="1.31241875" layer="21"/>
<rectangle x1="4.699" y1="1.22758125" x2="5.37718125" y2="1.31241875" layer="21"/>
<rectangle x1="5.63118125" y1="1.22758125" x2="5.79881875" y2="1.31241875" layer="21"/>
<rectangle x1="6.731" y1="1.22758125" x2="6.90118125" y2="1.31241875" layer="21"/>
<rectangle x1="7.32281875" y1="1.22758125" x2="7.747" y2="1.31241875" layer="21"/>
<rectangle x1="8.255" y1="1.22758125" x2="8.67918125" y2="1.31241875" layer="21"/>
<rectangle x1="9.18718125" y1="1.22758125" x2="9.60881875" y2="1.31241875" layer="21"/>
<rectangle x1="10.20318125" y1="1.22758125" x2="10.37081875" y2="1.31241875" layer="21"/>
<rectangle x1="11.38681875" y1="1.22758125" x2="11.557" y2="1.31241875" layer="21"/>
<rectangle x1="11.72718125" y1="1.22758125" x2="11.89481875" y2="1.31241875" layer="21"/>
<rectangle x1="11.98118125" y1="1.22758125" x2="12.40281875" y2="1.31241875" layer="21"/>
<rectangle x1="13.081" y1="1.22758125" x2="13.50518125" y2="1.31241875" layer="21"/>
<rectangle x1="1.651" y1="1.31241875" x2="1.82118125" y2="1.397" layer="21"/>
<rectangle x1="3.00481875" y1="1.31241875" x2="3.175" y2="1.397" layer="21"/>
<rectangle x1="4.445" y1="1.31241875" x2="4.52881875" y2="1.397" layer="21"/>
<rectangle x1="4.78281875" y1="1.31241875" x2="4.953" y2="1.397" layer="21"/>
<rectangle x1="5.63118125" y1="1.31241875" x2="5.79881875" y2="1.397" layer="21"/>
<rectangle x1="6.64718125" y1="1.31241875" x2="6.90118125" y2="1.397" layer="21"/>
<rectangle x1="10.20318125" y1="1.31241875" x2="10.45718125" y2="1.397" layer="21"/>
<rectangle x1="11.303" y1="1.31241875" x2="11.557" y2="1.397" layer="21"/>
<rectangle x1="1.651" y1="1.397" x2="1.82118125" y2="1.48158125" layer="21"/>
<rectangle x1="3.00481875" y1="1.397" x2="3.175" y2="1.48158125" layer="21"/>
<rectangle x1="4.36118125" y1="1.397" x2="4.61518125" y2="1.48158125" layer="21"/>
<rectangle x1="4.78281875" y1="1.397" x2="4.953" y2="1.48158125" layer="21"/>
<rectangle x1="5.63118125" y1="1.397" x2="5.79881875" y2="1.48158125" layer="21"/>
<rectangle x1="6.56081875" y1="1.397" x2="6.81481875" y2="1.48158125" layer="21"/>
<rectangle x1="10.287" y1="1.397" x2="10.541" y2="1.48158125" layer="21"/>
<rectangle x1="11.21918125" y1="1.397" x2="11.47318125" y2="1.48158125" layer="21"/>
<rectangle x1="1.651" y1="1.48158125" x2="1.82118125" y2="1.56641875" layer="21"/>
<rectangle x1="3.00481875" y1="1.48158125" x2="3.175" y2="1.56641875" layer="21"/>
<rectangle x1="4.445" y1="1.48158125" x2="4.52881875" y2="1.56641875" layer="21"/>
<rectangle x1="4.78281875" y1="1.48158125" x2="4.953" y2="1.56641875" layer="21"/>
<rectangle x1="5.63118125" y1="1.48158125" x2="5.79881875" y2="1.56641875" layer="21"/>
<rectangle x1="6.477" y1="1.48158125" x2="6.731" y2="1.56641875" layer="21"/>
<rectangle x1="10.37081875" y1="1.48158125" x2="10.71118125" y2="1.56641875" layer="21"/>
<rectangle x1="11.049" y1="1.48158125" x2="11.38681875" y2="1.56641875" layer="21"/>
<rectangle x1="1.651" y1="1.56641875" x2="1.82118125" y2="1.651" layer="21"/>
<rectangle x1="3.00481875" y1="1.56641875" x2="3.175" y2="1.651" layer="21"/>
<rectangle x1="4.78281875" y1="1.56641875" x2="4.953" y2="1.651" layer="21"/>
<rectangle x1="5.63118125" y1="1.56641875" x2="5.79881875" y2="1.651" layer="21"/>
<rectangle x1="5.88518125" y1="1.56641875" x2="6.64718125" y2="1.651" layer="21"/>
<rectangle x1="10.541" y1="1.56641875" x2="11.21918125" y2="1.651" layer="21"/>
<rectangle x1="1.651" y1="1.651" x2="1.82118125" y2="1.73558125" layer="21"/>
<rectangle x1="3.00481875" y1="1.651" x2="3.175" y2="1.73558125" layer="21"/>
<rectangle x1="4.86918125" y1="1.651" x2="4.953" y2="1.73558125" layer="21"/>
<rectangle x1="5.63118125" y1="1.651" x2="6.56081875" y2="1.73558125" layer="21"/>
<rectangle x1="10.71118125" y1="1.651" x2="11.049" y2="1.73558125" layer="21"/>
<rectangle x1="5.715" y1="1.73558125" x2="6.30681875" y2="1.82041875" layer="21"/>
<rectangle x1="2.49681875" y1="2.413" x2="12.573" y2="2.49758125" layer="21"/>
<rectangle x1="2.159" y1="2.49758125" x2="12.91081875" y2="2.58241875" layer="21"/>
<rectangle x1="1.905" y1="2.58241875" x2="13.16481875" y2="2.667" layer="21"/>
<rectangle x1="1.73481875" y1="2.667" x2="13.335" y2="2.75158125" layer="21"/>
<rectangle x1="1.56718125" y1="2.75158125" x2="13.50518125" y2="2.83641875" layer="21"/>
<rectangle x1="1.397" y1="2.83641875" x2="2.667" y2="2.921" layer="21"/>
<rectangle x1="7.32281875" y1="2.83641875" x2="13.589" y2="2.921" layer="21"/>
<rectangle x1="1.31318125" y1="2.921" x2="2.32918125" y2="3.00558125" layer="21"/>
<rectangle x1="7.32281875" y1="2.921" x2="13.75918125" y2="3.00558125" layer="21"/>
<rectangle x1="1.22681875" y1="3.00558125" x2="2.07518125" y2="3.09041875" layer="21"/>
<rectangle x1="7.239" y1="3.00558125" x2="13.843" y2="3.09041875" layer="21"/>
<rectangle x1="1.05918125" y1="3.09041875" x2="1.905" y2="3.175" layer="21"/>
<rectangle x1="7.15518125" y1="3.09041875" x2="13.92681875" y2="3.175" layer="21"/>
<rectangle x1="0.97281875" y1="3.175" x2="1.73481875" y2="3.25958125" layer="21"/>
<rectangle x1="7.15518125" y1="3.175" x2="14.097" y2="3.25958125" layer="21"/>
<rectangle x1="0.889" y1="3.25958125" x2="1.56718125" y2="3.34441875" layer="21"/>
<rectangle x1="7.15518125" y1="3.25958125" x2="14.18081875" y2="3.34441875" layer="21"/>
<rectangle x1="0.80518125" y1="3.34441875" x2="1.48081875" y2="3.429" layer="21"/>
<rectangle x1="7.06881875" y1="3.34441875" x2="14.26718125" y2="3.429" layer="21"/>
<rectangle x1="0.71881875" y1="3.429" x2="1.397" y2="3.51358125" layer="21"/>
<rectangle x1="7.06881875" y1="3.429" x2="14.26718125" y2="3.51358125" layer="21"/>
<rectangle x1="0.71881875" y1="3.51358125" x2="1.31318125" y2="3.59841875" layer="21"/>
<rectangle x1="6.985" y1="3.51358125" x2="14.351" y2="3.59841875" layer="21"/>
<rectangle x1="0.635" y1="3.59841875" x2="1.22681875" y2="3.683" layer="21"/>
<rectangle x1="6.985" y1="3.59841875" x2="14.43481875" y2="3.683" layer="21"/>
<rectangle x1="0.55118125" y1="3.683" x2="1.143" y2="3.76758125" layer="21"/>
<rectangle x1="6.985" y1="3.683" x2="14.52118125" y2="3.76758125" layer="21"/>
<rectangle x1="0.46481875" y1="3.76758125" x2="1.05918125" y2="3.85241875" layer="21"/>
<rectangle x1="6.90118125" y1="3.76758125" x2="14.52118125" y2="3.85241875" layer="21"/>
<rectangle x1="0.46481875" y1="3.85241875" x2="0.97281875" y2="3.937" layer="21"/>
<rectangle x1="6.90118125" y1="3.85241875" x2="14.605" y2="3.937" layer="21"/>
<rectangle x1="0.381" y1="3.937" x2="0.889" y2="4.02158125" layer="21"/>
<rectangle x1="6.90118125" y1="3.937" x2="14.68881875" y2="4.02158125" layer="21"/>
<rectangle x1="0.381" y1="4.02158125" x2="0.889" y2="4.10641875" layer="21"/>
<rectangle x1="6.90118125" y1="4.02158125" x2="14.68881875" y2="4.10641875" layer="21"/>
<rectangle x1="0.29718125" y1="4.10641875" x2="0.80518125" y2="4.191" layer="21"/>
<rectangle x1="6.81481875" y1="4.10641875" x2="14.77518125" y2="4.191" layer="21"/>
<rectangle x1="0.29718125" y1="4.191" x2="0.80518125" y2="4.27558125" layer="21"/>
<rectangle x1="2.58318125" y1="4.191" x2="3.51281875" y2="4.27558125" layer="21"/>
<rectangle x1="4.27481875" y1="4.191" x2="5.461" y2="4.27558125" layer="21"/>
<rectangle x1="6.81481875" y1="4.191" x2="8.17118125" y2="4.27558125" layer="21"/>
<rectangle x1="8.763" y1="4.191" x2="10.033" y2="4.27558125" layer="21"/>
<rectangle x1="10.11681875" y1="4.191" x2="11.38681875" y2="4.27558125" layer="21"/>
<rectangle x1="11.47318125" y1="4.191" x2="12.48918125" y2="4.27558125" layer="21"/>
<rectangle x1="12.65681875" y1="4.191" x2="14.77518125" y2="4.27558125" layer="21"/>
<rectangle x1="0.21081875" y1="4.27558125" x2="0.71881875" y2="4.36041875" layer="21"/>
<rectangle x1="2.413" y1="4.27558125" x2="3.683" y2="4.36041875" layer="21"/>
<rectangle x1="4.27481875" y1="4.27558125" x2="5.63118125" y2="4.36041875" layer="21"/>
<rectangle x1="6.81481875" y1="4.27558125" x2="7.91718125" y2="4.36041875" layer="21"/>
<rectangle x1="9.017" y1="4.27558125" x2="9.94918125" y2="4.36041875" layer="21"/>
<rectangle x1="10.20318125" y1="4.27558125" x2="11.303" y2="4.36041875" layer="21"/>
<rectangle x1="11.557" y1="4.27558125" x2="12.23518125" y2="4.36041875" layer="21"/>
<rectangle x1="12.99718125" y1="4.27558125" x2="14.859" y2="4.36041875" layer="21"/>
<rectangle x1="0.21081875" y1="4.36041875" x2="0.71881875" y2="4.445" layer="21"/>
<rectangle x1="2.32918125" y1="4.36041875" x2="3.76681875" y2="4.445" layer="21"/>
<rectangle x1="4.27481875" y1="4.36041875" x2="5.715" y2="4.445" layer="21"/>
<rectangle x1="6.81481875" y1="4.36041875" x2="7.747" y2="4.445" layer="21"/>
<rectangle x1="9.18718125" y1="4.36041875" x2="9.94918125" y2="4.445" layer="21"/>
<rectangle x1="10.287" y1="4.36041875" x2="11.303" y2="4.445" layer="21"/>
<rectangle x1="11.557" y1="4.36041875" x2="12.065" y2="4.445" layer="21"/>
<rectangle x1="13.16481875" y1="4.36041875" x2="14.859" y2="4.445" layer="21"/>
<rectangle x1="0.21081875" y1="4.445" x2="0.635" y2="4.52958125" layer="21"/>
<rectangle x1="2.159" y1="4.445" x2="3.85318125" y2="4.52958125" layer="21"/>
<rectangle x1="4.27481875" y1="4.445" x2="5.79881875" y2="4.52958125" layer="21"/>
<rectangle x1="6.81481875" y1="4.445" x2="7.66318125" y2="4.52958125" layer="21"/>
<rectangle x1="9.271" y1="4.445" x2="9.94918125" y2="4.52958125" layer="21"/>
<rectangle x1="10.287" y1="4.445" x2="11.303" y2="4.52958125" layer="21"/>
<rectangle x1="11.557" y1="4.445" x2="11.98118125" y2="4.52958125" layer="21"/>
<rectangle x1="13.25118125" y1="4.445" x2="14.859" y2="4.52958125" layer="21"/>
<rectangle x1="0.127" y1="4.52958125" x2="0.635" y2="4.61441875" layer="21"/>
<rectangle x1="2.07518125" y1="4.52958125" x2="3.937" y2="4.61441875" layer="21"/>
<rectangle x1="4.36118125" y1="4.52958125" x2="5.88518125" y2="4.61441875" layer="21"/>
<rectangle x1="6.81481875" y1="4.52958125" x2="7.57681875" y2="4.61441875" layer="21"/>
<rectangle x1="8.33881875" y1="4.52958125" x2="8.59281875" y2="4.61441875" layer="21"/>
<rectangle x1="9.35481875" y1="4.52958125" x2="9.94918125" y2="4.61441875" layer="21"/>
<rectangle x1="10.287" y1="4.52958125" x2="11.303" y2="4.61441875" layer="21"/>
<rectangle x1="11.557" y1="4.52958125" x2="11.89481875" y2="4.61441875" layer="21"/>
<rectangle x1="12.40281875" y1="4.52958125" x2="12.91081875" y2="4.61441875" layer="21"/>
<rectangle x1="13.335" y1="4.52958125" x2="14.94281875" y2="4.61441875" layer="21"/>
<rectangle x1="0.127" y1="4.61441875" x2="0.635" y2="4.699" layer="21"/>
<rectangle x1="2.07518125" y1="4.61441875" x2="2.75081875" y2="4.699" layer="21"/>
<rectangle x1="3.34518125" y1="4.61441875" x2="4.02081875" y2="4.699" layer="21"/>
<rectangle x1="5.29081875" y1="4.61441875" x2="5.969" y2="4.699" layer="21"/>
<rectangle x1="6.81481875" y1="4.61441875" x2="7.493" y2="4.699" layer="21"/>
<rectangle x1="8.08481875" y1="4.61441875" x2="8.84681875" y2="4.699" layer="21"/>
<rectangle x1="9.44118125" y1="4.61441875" x2="9.94918125" y2="4.699" layer="21"/>
<rectangle x1="10.287" y1="4.61441875" x2="11.303" y2="4.699" layer="21"/>
<rectangle x1="11.557" y1="4.61441875" x2="11.89481875" y2="4.699" layer="21"/>
<rectangle x1="12.23518125" y1="4.61441875" x2="12.99718125" y2="4.699" layer="21"/>
<rectangle x1="13.335" y1="4.61441875" x2="14.94281875" y2="4.699" layer="21"/>
<rectangle x1="0.127" y1="4.699" x2="0.55118125" y2="4.78358125" layer="21"/>
<rectangle x1="1.98881875" y1="4.699" x2="2.58318125" y2="4.78358125" layer="21"/>
<rectangle x1="3.51281875" y1="4.699" x2="4.10718125" y2="4.78358125" layer="21"/>
<rectangle x1="5.461" y1="4.699" x2="6.05281875" y2="4.78358125" layer="21"/>
<rectangle x1="6.731" y1="4.699" x2="7.40918125" y2="4.78358125" layer="21"/>
<rectangle x1="7.91718125" y1="4.699" x2="9.017" y2="4.78358125" layer="21"/>
<rectangle x1="9.44118125" y1="4.699" x2="9.94918125" y2="4.78358125" layer="21"/>
<rectangle x1="10.287" y1="4.699" x2="11.303" y2="4.78358125" layer="21"/>
<rectangle x1="11.557" y1="4.699" x2="11.811" y2="4.78358125" layer="21"/>
<rectangle x1="12.14881875" y1="4.699" x2="13.081" y2="4.78358125" layer="21"/>
<rectangle x1="13.25118125" y1="4.699" x2="14.94281875" y2="4.78358125" layer="21"/>
<rectangle x1="0.04318125" y1="4.78358125" x2="0.55118125" y2="4.86841875" layer="21"/>
<rectangle x1="1.905" y1="4.78358125" x2="2.49681875" y2="4.86841875" layer="21"/>
<rectangle x1="3.59918125" y1="4.78358125" x2="4.191" y2="4.86841875" layer="21"/>
<rectangle x1="5.54481875" y1="4.78358125" x2="6.13918125" y2="4.86841875" layer="21"/>
<rectangle x1="6.731" y1="4.78358125" x2="7.40918125" y2="4.86841875" layer="21"/>
<rectangle x1="7.83081875" y1="4.78358125" x2="9.10081875" y2="4.86841875" layer="21"/>
<rectangle x1="9.525" y1="4.78358125" x2="9.94918125" y2="4.86841875" layer="21"/>
<rectangle x1="10.287" y1="4.78358125" x2="11.303" y2="4.86841875" layer="21"/>
<rectangle x1="11.557" y1="4.78358125" x2="11.811" y2="4.86841875" layer="21"/>
<rectangle x1="12.14881875" y1="4.78358125" x2="14.94281875" y2="4.86841875" layer="21"/>
<rectangle x1="0.04318125" y1="4.86841875" x2="0.55118125" y2="4.953" layer="21"/>
<rectangle x1="1.905" y1="4.86841875" x2="2.413" y2="4.953" layer="21"/>
<rectangle x1="3.683" y1="4.86841875" x2="4.191" y2="4.953" layer="21"/>
<rectangle x1="5.63118125" y1="4.86841875" x2="6.13918125" y2="4.953" layer="21"/>
<rectangle x1="6.731" y1="4.86841875" x2="7.32281875" y2="4.953" layer="21"/>
<rectangle x1="7.747" y1="4.86841875" x2="9.10081875" y2="4.953" layer="21"/>
<rectangle x1="9.60881875" y1="4.86841875" x2="9.94918125" y2="4.953" layer="21"/>
<rectangle x1="10.287" y1="4.86841875" x2="11.303" y2="4.953" layer="21"/>
<rectangle x1="11.557" y1="4.86841875" x2="11.811" y2="4.953" layer="21"/>
<rectangle x1="13.335" y1="4.86841875" x2="14.94281875" y2="4.953" layer="21"/>
<rectangle x1="0.04318125" y1="4.953" x2="0.55118125" y2="5.03758125" layer="21"/>
<rectangle x1="1.82118125" y1="4.953" x2="2.32918125" y2="5.03758125" layer="21"/>
<rectangle x1="3.76681875" y1="4.953" x2="4.191" y2="5.03758125" layer="21"/>
<rectangle x1="5.715" y1="4.953" x2="6.13918125" y2="5.03758125" layer="21"/>
<rectangle x1="6.731" y1="4.953" x2="7.32281875" y2="5.03758125" layer="21"/>
<rectangle x1="7.747" y1="4.953" x2="9.18718125" y2="5.03758125" layer="21"/>
<rectangle x1="9.60881875" y1="4.953" x2="9.94918125" y2="5.03758125" layer="21"/>
<rectangle x1="10.287" y1="4.953" x2="11.303" y2="5.03758125" layer="21"/>
<rectangle x1="11.557" y1="4.953" x2="11.811" y2="5.03758125" layer="21"/>
<rectangle x1="13.41881875" y1="4.953" x2="14.94281875" y2="5.03758125" layer="21"/>
<rectangle x1="0.04318125" y1="5.03758125" x2="0.46481875" y2="5.12241875" layer="21"/>
<rectangle x1="1.82118125" y1="5.03758125" x2="2.32918125" y2="5.12241875" layer="21"/>
<rectangle x1="3.76681875" y1="5.03758125" x2="4.27481875" y2="5.12241875" layer="21"/>
<rectangle x1="5.715" y1="5.03758125" x2="6.223" y2="5.12241875" layer="21"/>
<rectangle x1="6.731" y1="5.03758125" x2="7.32281875" y2="5.12241875" layer="21"/>
<rectangle x1="7.66318125" y1="5.03758125" x2="9.271" y2="5.12241875" layer="21"/>
<rectangle x1="9.60881875" y1="5.03758125" x2="9.94918125" y2="5.12241875" layer="21"/>
<rectangle x1="10.287" y1="5.03758125" x2="11.303" y2="5.12241875" layer="21"/>
<rectangle x1="11.557" y1="5.03758125" x2="11.811" y2="5.12241875" layer="21"/>
<rectangle x1="13.41881875" y1="5.03758125" x2="14.94281875" y2="5.12241875" layer="21"/>
<rectangle x1="0.04318125" y1="5.12241875" x2="0.46481875" y2="5.207" layer="21"/>
<rectangle x1="1.82118125" y1="5.12241875" x2="2.24281875" y2="5.207" layer="21"/>
<rectangle x1="3.76681875" y1="5.12241875" x2="4.27481875" y2="5.207" layer="21"/>
<rectangle x1="5.79881875" y1="5.12241875" x2="6.223" y2="5.207" layer="21"/>
<rectangle x1="6.731" y1="5.12241875" x2="7.239" y2="5.207" layer="21"/>
<rectangle x1="7.66318125" y1="5.12241875" x2="9.271" y2="5.207" layer="21"/>
<rectangle x1="9.69518125" y1="5.12241875" x2="9.94918125" y2="5.207" layer="21"/>
<rectangle x1="10.287" y1="5.12241875" x2="11.303" y2="5.207" layer="21"/>
<rectangle x1="11.557" y1="5.12241875" x2="11.811" y2="5.207" layer="21"/>
<rectangle x1="13.41881875" y1="5.12241875" x2="14.94281875" y2="5.207" layer="21"/>
<rectangle x1="0.04318125" y1="5.207" x2="0.46481875" y2="5.29158125" layer="21"/>
<rectangle x1="1.82118125" y1="5.207" x2="2.24281875" y2="5.29158125" layer="21"/>
<rectangle x1="3.85318125" y1="5.207" x2="4.27481875" y2="5.29158125" layer="21"/>
<rectangle x1="5.79881875" y1="5.207" x2="6.223" y2="5.29158125" layer="21"/>
<rectangle x1="6.731" y1="5.207" x2="7.239" y2="5.29158125" layer="21"/>
<rectangle x1="7.66318125" y1="5.207" x2="9.271" y2="5.29158125" layer="21"/>
<rectangle x1="9.69518125" y1="5.207" x2="9.94918125" y2="5.29158125" layer="21"/>
<rectangle x1="10.287" y1="5.207" x2="11.21918125" y2="5.29158125" layer="21"/>
<rectangle x1="11.557" y1="5.207" x2="11.811" y2="5.29158125" layer="21"/>
<rectangle x1="12.14881875" y1="5.207" x2="13.081" y2="5.29158125" layer="21"/>
<rectangle x1="13.335" y1="5.207" x2="14.94281875" y2="5.29158125" layer="21"/>
<rectangle x1="0.04318125" y1="5.29158125" x2="0.46481875" y2="5.37641875" layer="21"/>
<rectangle x1="1.82118125" y1="5.29158125" x2="2.24281875" y2="5.37641875" layer="21"/>
<rectangle x1="3.85318125" y1="5.29158125" x2="4.27481875" y2="5.37641875" layer="21"/>
<rectangle x1="5.79881875" y1="5.29158125" x2="6.223" y2="5.37641875" layer="21"/>
<rectangle x1="6.731" y1="5.29158125" x2="7.239" y2="5.37641875" layer="21"/>
<rectangle x1="7.57681875" y1="5.29158125" x2="9.271" y2="5.37641875" layer="21"/>
<rectangle x1="9.69518125" y1="5.29158125" x2="9.94918125" y2="5.37641875" layer="21"/>
<rectangle x1="10.37081875" y1="5.29158125" x2="11.21918125" y2="5.37641875" layer="21"/>
<rectangle x1="11.557" y1="5.29158125" x2="11.811" y2="5.37641875" layer="21"/>
<rectangle x1="12.14881875" y1="5.29158125" x2="12.99718125" y2="5.37641875" layer="21"/>
<rectangle x1="13.335" y1="5.29158125" x2="14.94281875" y2="5.37641875" layer="21"/>
<rectangle x1="0.04318125" y1="5.37641875" x2="0.46481875" y2="5.461" layer="21"/>
<rectangle x1="1.82118125" y1="5.37641875" x2="2.24281875" y2="5.461" layer="21"/>
<rectangle x1="3.85318125" y1="5.37641875" x2="4.27481875" y2="5.461" layer="21"/>
<rectangle x1="5.79881875" y1="5.37641875" x2="6.223" y2="5.461" layer="21"/>
<rectangle x1="6.731" y1="5.37641875" x2="7.239" y2="5.461" layer="21"/>
<rectangle x1="7.57681875" y1="5.37641875" x2="9.271" y2="5.461" layer="21"/>
<rectangle x1="9.69518125" y1="5.37641875" x2="9.94918125" y2="5.461" layer="21"/>
<rectangle x1="10.45718125" y1="5.37641875" x2="11.13281875" y2="5.461" layer="21"/>
<rectangle x1="11.47318125" y1="5.37641875" x2="11.89481875" y2="5.461" layer="21"/>
<rectangle x1="12.23518125" y1="5.37641875" x2="12.91081875" y2="5.461" layer="21"/>
<rectangle x1="13.335" y1="5.37641875" x2="14.94281875" y2="5.461" layer="21"/>
<rectangle x1="0.04318125" y1="5.461" x2="0.46481875" y2="5.54558125" layer="21"/>
<rectangle x1="1.82118125" y1="5.461" x2="2.24281875" y2="5.54558125" layer="21"/>
<rectangle x1="3.85318125" y1="5.461" x2="4.27481875" y2="5.54558125" layer="21"/>
<rectangle x1="5.79881875" y1="5.461" x2="6.223" y2="5.54558125" layer="21"/>
<rectangle x1="6.731" y1="5.461" x2="7.239" y2="5.54558125" layer="21"/>
<rectangle x1="7.57681875" y1="5.461" x2="9.271" y2="5.54558125" layer="21"/>
<rectangle x1="9.69518125" y1="5.461" x2="9.94918125" y2="5.54558125" layer="21"/>
<rectangle x1="10.541" y1="5.461" x2="10.96518125" y2="5.54558125" layer="21"/>
<rectangle x1="11.38681875" y1="5.461" x2="11.98118125" y2="5.54558125" layer="21"/>
<rectangle x1="12.40281875" y1="5.461" x2="12.827" y2="5.54558125" layer="21"/>
<rectangle x1="13.25118125" y1="5.461" x2="14.94281875" y2="5.54558125" layer="21"/>
<rectangle x1="0.04318125" y1="5.54558125" x2="0.46481875" y2="5.63041875" layer="21"/>
<rectangle x1="1.82118125" y1="5.54558125" x2="2.24281875" y2="5.63041875" layer="21"/>
<rectangle x1="3.85318125" y1="5.54558125" x2="4.27481875" y2="5.63041875" layer="21"/>
<rectangle x1="5.79881875" y1="5.54558125" x2="6.223" y2="5.63041875" layer="21"/>
<rectangle x1="6.731" y1="5.54558125" x2="7.239" y2="5.63041875" layer="21"/>
<rectangle x1="7.66318125" y1="5.54558125" x2="9.271" y2="5.63041875" layer="21"/>
<rectangle x1="9.69518125" y1="5.54558125" x2="9.94918125" y2="5.63041875" layer="21"/>
<rectangle x1="11.38681875" y1="5.54558125" x2="11.98118125" y2="5.63041875" layer="21"/>
<rectangle x1="13.16481875" y1="5.54558125" x2="14.94281875" y2="5.63041875" layer="21"/>
<rectangle x1="0.04318125" y1="5.63041875" x2="0.46481875" y2="5.715" layer="21"/>
<rectangle x1="1.82118125" y1="5.63041875" x2="2.24281875" y2="5.715" layer="21"/>
<rectangle x1="3.85318125" y1="5.63041875" x2="4.27481875" y2="5.715" layer="21"/>
<rectangle x1="5.715" y1="5.63041875" x2="6.223" y2="5.715" layer="21"/>
<rectangle x1="6.731" y1="5.63041875" x2="7.239" y2="5.715" layer="21"/>
<rectangle x1="7.66318125" y1="5.63041875" x2="9.271" y2="5.715" layer="21"/>
<rectangle x1="9.60881875" y1="5.63041875" x2="9.94918125" y2="5.715" layer="21"/>
<rectangle x1="11.21918125" y1="5.63041875" x2="12.14881875" y2="5.715" layer="21"/>
<rectangle x1="13.081" y1="5.63041875" x2="14.94281875" y2="5.715" layer="21"/>
<rectangle x1="0.04318125" y1="5.715" x2="0.55118125" y2="5.79958125" layer="21"/>
<rectangle x1="1.82118125" y1="5.715" x2="2.24281875" y2="5.79958125" layer="21"/>
<rectangle x1="3.85318125" y1="5.715" x2="4.27481875" y2="5.79958125" layer="21"/>
<rectangle x1="5.715" y1="5.715" x2="6.223" y2="5.79958125" layer="21"/>
<rectangle x1="6.731" y1="5.715" x2="7.32281875" y2="5.79958125" layer="21"/>
<rectangle x1="7.66318125" y1="5.715" x2="9.271" y2="5.79958125" layer="21"/>
<rectangle x1="9.60881875" y1="5.715" x2="9.94918125" y2="5.79958125" layer="21"/>
<rectangle x1="10.20318125" y1="5.715" x2="10.45718125" y2="5.79958125" layer="21"/>
<rectangle x1="11.13281875" y1="5.715" x2="12.23518125" y2="5.79958125" layer="21"/>
<rectangle x1="12.91081875" y1="5.715" x2="14.94281875" y2="5.79958125" layer="21"/>
<rectangle x1="0.04318125" y1="5.79958125" x2="0.55118125" y2="5.88441875" layer="21"/>
<rectangle x1="1.82118125" y1="5.79958125" x2="2.24281875" y2="5.88441875" layer="21"/>
<rectangle x1="3.85318125" y1="5.79958125" x2="4.27481875" y2="5.88441875" layer="21"/>
<rectangle x1="5.63118125" y1="5.79958125" x2="6.13918125" y2="5.88441875" layer="21"/>
<rectangle x1="6.731" y1="5.79958125" x2="7.32281875" y2="5.88441875" layer="21"/>
<rectangle x1="7.747" y1="5.79958125" x2="9.18718125" y2="5.88441875" layer="21"/>
<rectangle x1="9.60881875" y1="5.79958125" x2="14.94281875" y2="5.88441875" layer="21"/>
<rectangle x1="0.04318125" y1="5.88441875" x2="0.55118125" y2="5.969" layer="21"/>
<rectangle x1="1.82118125" y1="5.88441875" x2="2.24281875" y2="5.969" layer="21"/>
<rectangle x1="3.85318125" y1="5.88441875" x2="4.27481875" y2="5.969" layer="21"/>
<rectangle x1="5.63118125" y1="5.88441875" x2="6.13918125" y2="5.969" layer="21"/>
<rectangle x1="6.731" y1="5.88441875" x2="7.40918125" y2="5.969" layer="21"/>
<rectangle x1="7.83081875" y1="5.88441875" x2="9.10081875" y2="5.969" layer="21"/>
<rectangle x1="9.525" y1="5.88441875" x2="14.94281875" y2="5.969" layer="21"/>
<rectangle x1="0.127" y1="5.969" x2="0.55118125" y2="6.05358125" layer="21"/>
<rectangle x1="1.82118125" y1="5.969" x2="2.24281875" y2="6.05358125" layer="21"/>
<rectangle x1="3.85318125" y1="5.969" x2="4.27481875" y2="6.05358125" layer="21"/>
<rectangle x1="5.54481875" y1="5.969" x2="6.05281875" y2="6.05358125" layer="21"/>
<rectangle x1="6.731" y1="5.969" x2="7.40918125" y2="6.05358125" layer="21"/>
<rectangle x1="7.91718125" y1="5.969" x2="9.017" y2="6.05358125" layer="21"/>
<rectangle x1="9.525" y1="5.969" x2="14.94281875" y2="6.05358125" layer="21"/>
<rectangle x1="0.127" y1="6.05358125" x2="0.55118125" y2="6.13841875" layer="21"/>
<rectangle x1="1.82118125" y1="6.05358125" x2="2.24281875" y2="6.13841875" layer="21"/>
<rectangle x1="3.85318125" y1="6.05358125" x2="4.27481875" y2="6.13841875" layer="21"/>
<rectangle x1="5.37718125" y1="6.05358125" x2="6.05281875" y2="6.13841875" layer="21"/>
<rectangle x1="6.731" y1="6.05358125" x2="7.493" y2="6.13841875" layer="21"/>
<rectangle x1="8.001" y1="6.05358125" x2="8.93318125" y2="6.13841875" layer="21"/>
<rectangle x1="9.44118125" y1="6.05358125" x2="14.94281875" y2="6.13841875" layer="21"/>
<rectangle x1="0.127" y1="6.13841875" x2="0.635" y2="6.223" layer="21"/>
<rectangle x1="1.82118125" y1="6.13841875" x2="2.24281875" y2="6.223" layer="21"/>
<rectangle x1="3.85318125" y1="6.13841875" x2="4.27481875" y2="6.223" layer="21"/>
<rectangle x1="5.207" y1="6.13841875" x2="5.969" y2="6.223" layer="21"/>
<rectangle x1="6.81481875" y1="6.13841875" x2="7.493" y2="6.223" layer="21"/>
<rectangle x1="8.17118125" y1="6.13841875" x2="8.763" y2="6.223" layer="21"/>
<rectangle x1="9.35481875" y1="6.13841875" x2="14.94281875" y2="6.223" layer="21"/>
<rectangle x1="0.127" y1="6.223" x2="0.635" y2="6.30758125" layer="21"/>
<rectangle x1="1.82118125" y1="6.223" x2="2.24281875" y2="6.30758125" layer="21"/>
<rectangle x1="3.85318125" y1="6.223" x2="5.88518125" y2="6.30758125" layer="21"/>
<rectangle x1="6.81481875" y1="6.223" x2="7.57681875" y2="6.30758125" layer="21"/>
<rectangle x1="9.35481875" y1="6.223" x2="14.859" y2="6.30758125" layer="21"/>
<rectangle x1="0.21081875" y1="6.30758125" x2="0.635" y2="6.39241875" layer="21"/>
<rectangle x1="1.82118125" y1="6.30758125" x2="2.24281875" y2="6.39241875" layer="21"/>
<rectangle x1="3.85318125" y1="6.30758125" x2="5.79881875" y2="6.39241875" layer="21"/>
<rectangle x1="6.81481875" y1="6.30758125" x2="7.66318125" y2="6.39241875" layer="21"/>
<rectangle x1="9.18718125" y1="6.30758125" x2="14.859" y2="6.39241875" layer="21"/>
<rectangle x1="0.21081875" y1="6.39241875" x2="0.71881875" y2="6.477" layer="21"/>
<rectangle x1="1.82118125" y1="6.39241875" x2="2.24281875" y2="6.477" layer="21"/>
<rectangle x1="3.85318125" y1="6.39241875" x2="5.715" y2="6.477" layer="21"/>
<rectangle x1="6.81481875" y1="6.39241875" x2="7.83081875" y2="6.477" layer="21"/>
<rectangle x1="9.10081875" y1="6.39241875" x2="14.859" y2="6.477" layer="21"/>
<rectangle x1="0.21081875" y1="6.477" x2="0.71881875" y2="6.56158125" layer="21"/>
<rectangle x1="1.82118125" y1="6.477" x2="2.24281875" y2="6.56158125" layer="21"/>
<rectangle x1="3.85318125" y1="6.477" x2="5.54481875" y2="6.56158125" layer="21"/>
<rectangle x1="6.81481875" y1="6.477" x2="8.001" y2="6.56158125" layer="21"/>
<rectangle x1="8.93318125" y1="6.477" x2="14.77518125" y2="6.56158125" layer="21"/>
<rectangle x1="0.29718125" y1="6.56158125" x2="0.80518125" y2="6.64641875" layer="21"/>
<rectangle x1="1.905" y1="6.56158125" x2="2.159" y2="6.64641875" layer="21"/>
<rectangle x1="3.937" y1="6.56158125" x2="5.37718125" y2="6.64641875" layer="21"/>
<rectangle x1="6.81481875" y1="6.56158125" x2="8.33881875" y2="6.64641875" layer="21"/>
<rectangle x1="8.59281875" y1="6.56158125" x2="14.77518125" y2="6.64641875" layer="21"/>
<rectangle x1="0.29718125" y1="6.64641875" x2="0.80518125" y2="6.731" layer="21"/>
<rectangle x1="6.90118125" y1="6.64641875" x2="14.77518125" y2="6.731" layer="21"/>
<rectangle x1="0.381" y1="6.731" x2="0.889" y2="6.81558125" layer="21"/>
<rectangle x1="6.90118125" y1="6.731" x2="14.68881875" y2="6.81558125" layer="21"/>
<rectangle x1="0.381" y1="6.81558125" x2="0.97281875" y2="6.90041875" layer="21"/>
<rectangle x1="6.90118125" y1="6.81558125" x2="14.605" y2="6.90041875" layer="21"/>
<rectangle x1="0.46481875" y1="6.90041875" x2="0.97281875" y2="6.985" layer="21"/>
<rectangle x1="6.90118125" y1="6.90041875" x2="14.605" y2="6.985" layer="21"/>
<rectangle x1="0.55118125" y1="6.985" x2="1.05918125" y2="7.06958125" layer="21"/>
<rectangle x1="6.985" y1="6.985" x2="14.52118125" y2="7.06958125" layer="21"/>
<rectangle x1="0.55118125" y1="7.06958125" x2="1.143" y2="7.15441875" layer="21"/>
<rectangle x1="6.985" y1="7.06958125" x2="14.52118125" y2="7.15441875" layer="21"/>
<rectangle x1="0.635" y1="7.15441875" x2="1.22681875" y2="7.239" layer="21"/>
<rectangle x1="6.985" y1="7.15441875" x2="14.43481875" y2="7.239" layer="21"/>
<rectangle x1="0.71881875" y1="7.239" x2="1.31318125" y2="7.32358125" layer="21"/>
<rectangle x1="6.985" y1="7.239" x2="14.351" y2="7.32358125" layer="21"/>
<rectangle x1="0.80518125" y1="7.32358125" x2="1.397" y2="7.40841875" layer="21"/>
<rectangle x1="7.06881875" y1="7.32358125" x2="14.26718125" y2="7.40841875" layer="21"/>
<rectangle x1="0.889" y1="7.40841875" x2="1.48081875" y2="7.493" layer="21"/>
<rectangle x1="7.06881875" y1="7.40841875" x2="14.18081875" y2="7.493" layer="21"/>
<rectangle x1="0.889" y1="7.493" x2="1.651" y2="7.57758125" layer="21"/>
<rectangle x1="7.15518125" y1="7.493" x2="14.097" y2="7.57758125" layer="21"/>
<rectangle x1="1.05918125" y1="7.57758125" x2="1.73481875" y2="7.66241875" layer="21"/>
<rectangle x1="7.15518125" y1="7.57758125" x2="14.01318125" y2="7.66241875" layer="21"/>
<rectangle x1="1.143" y1="7.66241875" x2="1.905" y2="7.747" layer="21"/>
<rectangle x1="7.239" y1="7.66241875" x2="13.92681875" y2="7.747" layer="21"/>
<rectangle x1="1.22681875" y1="7.747" x2="2.159" y2="7.83158125" layer="21"/>
<rectangle x1="7.239" y1="7.747" x2="13.843" y2="7.83158125" layer="21"/>
<rectangle x1="1.31318125" y1="7.83158125" x2="2.413" y2="7.91641875" layer="21"/>
<rectangle x1="7.32281875" y1="7.83158125" x2="13.67281875" y2="7.91641875" layer="21"/>
<rectangle x1="1.48081875" y1="7.91641875" x2="7.239" y2="8.001" layer="21"/>
<rectangle x1="7.32281875" y1="7.91641875" x2="13.589" y2="8.001" layer="21"/>
<rectangle x1="1.651" y1="8.001" x2="13.41881875" y2="8.08558125" layer="21"/>
<rectangle x1="1.82118125" y1="8.08558125" x2="13.25118125" y2="8.17041875" layer="21"/>
<rectangle x1="1.98881875" y1="8.17041875" x2="13.081" y2="8.255" layer="21"/>
<rectangle x1="2.24281875" y1="8.255" x2="12.827" y2="8.33958125" layer="21"/>
</package>
<package name="UDO-LOGO-20MM">
<rectangle x1="3.00481875" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="14.351" y1="0.29641875" x2="14.68881875" y2="0.381" layer="21"/>
<rectangle x1="2.75081875" y1="0.381" x2="3.683" y2="0.46558125" layer="21"/>
<rectangle x1="4.445" y1="0.381" x2="4.52881875" y2="0.46558125" layer="21"/>
<rectangle x1="5.54481875" y1="0.381" x2="5.715" y2="0.46558125" layer="21"/>
<rectangle x1="5.969" y1="0.381" x2="6.05281875" y2="0.46558125" layer="21"/>
<rectangle x1="6.985" y1="0.381" x2="7.15518125" y2="0.46558125" layer="21"/>
<rectangle x1="7.57681875" y1="0.381" x2="8.42518125" y2="0.46558125" layer="21"/>
<rectangle x1="9.94918125" y1="0.381" x2="10.20318125" y2="0.46558125" layer="21"/>
<rectangle x1="11.21918125" y1="0.381" x2="11.38681875" y2="0.46558125" layer="21"/>
<rectangle x1="12.40281875" y1="0.381" x2="12.74318125" y2="0.46558125" layer="21"/>
<rectangle x1="14.097" y1="0.381" x2="14.94281875" y2="0.46558125" layer="21"/>
<rectangle x1="15.70481875" y1="0.381" x2="15.79118125" y2="0.46558125" layer="21"/>
<rectangle x1="16.80718125" y1="0.381" x2="16.891" y2="0.46558125" layer="21"/>
<rectangle x1="17.653" y1="0.381" x2="17.907" y2="0.46558125" layer="21"/>
<rectangle x1="2.667" y1="0.46558125" x2="3.85318125" y2="0.55041875" layer="21"/>
<rectangle x1="4.36118125" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="21"/>
<rectangle x1="5.54481875" y1="0.46558125" x2="5.715" y2="0.55041875" layer="21"/>
<rectangle x1="5.88518125" y1="0.46558125" x2="6.13918125" y2="0.55041875" layer="21"/>
<rectangle x1="6.731" y1="0.46558125" x2="7.239" y2="0.55041875" layer="21"/>
<rectangle x1="7.493" y1="0.46558125" x2="8.67918125" y2="0.55041875" layer="21"/>
<rectangle x1="9.69518125" y1="0.46558125" x2="10.37081875" y2="0.55041875" layer="21"/>
<rectangle x1="11.049" y1="0.46558125" x2="11.64081875" y2="0.55041875" layer="21"/>
<rectangle x1="12.23518125" y1="0.46558125" x2="12.91081875" y2="0.55041875" layer="21"/>
<rectangle x1="13.92681875" y1="0.46558125" x2="15.113" y2="0.55041875" layer="21"/>
<rectangle x1="15.621" y1="0.46558125" x2="15.875" y2="0.55041875" layer="21"/>
<rectangle x1="16.72081875" y1="0.46558125" x2="16.97481875" y2="0.55041875" layer="21"/>
<rectangle x1="17.399" y1="0.46558125" x2="18.07718125" y2="0.55041875" layer="21"/>
<rectangle x1="2.58318125" y1="0.55041875" x2="3.00481875" y2="0.635" layer="21"/>
<rectangle x1="3.51281875" y1="0.55041875" x2="3.937" y2="0.635" layer="21"/>
<rectangle x1="4.36118125" y1="0.55041875" x2="4.61518125" y2="0.635" layer="21"/>
<rectangle x1="5.54481875" y1="0.55041875" x2="5.715" y2="0.635" layer="21"/>
<rectangle x1="5.88518125" y1="0.55041875" x2="6.13918125" y2="0.635" layer="21"/>
<rectangle x1="6.64718125" y1="0.55041875" x2="7.239" y2="0.635" layer="21"/>
<rectangle x1="7.493" y1="0.55041875" x2="8.84681875" y2="0.635" layer="21"/>
<rectangle x1="9.60881875" y1="0.55041875" x2="10.45718125" y2="0.635" layer="21"/>
<rectangle x1="10.96518125" y1="0.55041875" x2="11.72718125" y2="0.635" layer="21"/>
<rectangle x1="12.14881875" y1="0.55041875" x2="12.99718125" y2="0.635" layer="21"/>
<rectangle x1="13.843" y1="0.55041875" x2="14.26718125" y2="0.635" layer="21"/>
<rectangle x1="14.77518125" y1="0.55041875" x2="15.19681875" y2="0.635" layer="21"/>
<rectangle x1="15.621" y1="0.55041875" x2="15.875" y2="0.635" layer="21"/>
<rectangle x1="16.72081875" y1="0.55041875" x2="16.97481875" y2="0.635" layer="21"/>
<rectangle x1="17.31518125" y1="0.55041875" x2="18.24481875" y2="0.635" layer="21"/>
<rectangle x1="2.49681875" y1="0.635" x2="2.83718125" y2="0.71958125" layer="21"/>
<rectangle x1="3.683" y1="0.635" x2="4.02081875" y2="0.71958125" layer="21"/>
<rectangle x1="4.36118125" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="5.54481875" y1="0.635" x2="5.715" y2="0.71958125" layer="21"/>
<rectangle x1="5.88518125" y1="0.635" x2="6.13918125" y2="0.71958125" layer="21"/>
<rectangle x1="6.56081875" y1="0.635" x2="6.90118125" y2="0.71958125" layer="21"/>
<rectangle x1="7.493" y1="0.635" x2="7.747" y2="0.71958125" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="8.93318125" y2="0.71958125" layer="21"/>
<rectangle x1="9.525" y1="0.635" x2="9.86281875" y2="0.71958125" layer="21"/>
<rectangle x1="10.20318125" y1="0.635" x2="10.541" y2="0.71958125" layer="21"/>
<rectangle x1="10.87881875" y1="0.635" x2="11.13281875" y2="0.71958125" layer="21"/>
<rectangle x1="11.47318125" y1="0.635" x2="11.811" y2="0.71958125" layer="21"/>
<rectangle x1="12.065" y1="0.635" x2="12.40281875" y2="0.71958125" layer="21"/>
<rectangle x1="12.74318125" y1="0.635" x2="13.081" y2="0.71958125" layer="21"/>
<rectangle x1="13.75918125" y1="0.635" x2="14.097" y2="0.71958125" layer="21"/>
<rectangle x1="14.94281875" y1="0.635" x2="15.28318125" y2="0.71958125" layer="21"/>
<rectangle x1="15.621" y1="0.635" x2="15.875" y2="0.71958125" layer="21"/>
<rectangle x1="16.72081875" y1="0.635" x2="16.97481875" y2="0.71958125" layer="21"/>
<rectangle x1="17.22881875" y1="0.635" x2="17.56918125" y2="0.71958125" layer="21"/>
<rectangle x1="17.907" y1="0.635" x2="18.33118125" y2="0.71958125" layer="21"/>
<rectangle x1="2.413" y1="0.71958125" x2="2.667" y2="0.80441875" layer="21"/>
<rectangle x1="3.76681875" y1="0.71958125" x2="4.02081875" y2="0.80441875" layer="21"/>
<rectangle x1="4.36118125" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="5.54481875" y1="0.71958125" x2="5.715" y2="0.80441875" layer="21"/>
<rectangle x1="5.88518125" y1="0.71958125" x2="6.13918125" y2="0.80441875" layer="21"/>
<rectangle x1="6.56081875" y1="0.71958125" x2="6.81481875" y2="0.80441875" layer="21"/>
<rectangle x1="7.493" y1="0.71958125" x2="7.747" y2="0.80441875" layer="21"/>
<rectangle x1="8.67918125" y1="0.71958125" x2="9.017" y2="0.80441875" layer="21"/>
<rectangle x1="9.44118125" y1="0.71958125" x2="9.69518125" y2="0.80441875" layer="21"/>
<rectangle x1="10.37081875" y1="0.71958125" x2="10.62481875" y2="0.80441875" layer="21"/>
<rectangle x1="10.87881875" y1="0.71958125" x2="11.049" y2="0.80441875" layer="21"/>
<rectangle x1="11.557" y1="0.71958125" x2="11.811" y2="0.80441875" layer="21"/>
<rectangle x1="11.98118125" y1="0.71958125" x2="12.23518125" y2="0.80441875" layer="21"/>
<rectangle x1="12.91081875" y1="0.71958125" x2="13.16481875" y2="0.80441875" layer="21"/>
<rectangle x1="13.67281875" y1="0.71958125" x2="14.01318125" y2="0.80441875" layer="21"/>
<rectangle x1="15.02918125" y1="0.71958125" x2="15.367" y2="0.80441875" layer="21"/>
<rectangle x1="15.621" y1="0.71958125" x2="15.875" y2="0.80441875" layer="21"/>
<rectangle x1="16.72081875" y1="0.71958125" x2="16.97481875" y2="0.80441875" layer="21"/>
<rectangle x1="17.145" y1="0.71958125" x2="17.399" y2="0.80441875" layer="21"/>
<rectangle x1="18.07718125" y1="0.71958125" x2="18.33118125" y2="0.80441875" layer="21"/>
<rectangle x1="2.32918125" y1="0.80441875" x2="2.58318125" y2="0.889" layer="21"/>
<rectangle x1="3.85318125" y1="0.80441875" x2="4.10718125" y2="0.889" layer="21"/>
<rectangle x1="4.36118125" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="5.54481875" y1="0.80441875" x2="5.715" y2="0.889" layer="21"/>
<rectangle x1="5.88518125" y1="0.80441875" x2="6.13918125" y2="0.889" layer="21"/>
<rectangle x1="6.477" y1="0.80441875" x2="6.731" y2="0.889" layer="21"/>
<rectangle x1="7.493" y1="0.80441875" x2="7.747" y2="0.889" layer="21"/>
<rectangle x1="8.763" y1="0.80441875" x2="9.10081875" y2="0.889" layer="21"/>
<rectangle x1="9.44118125" y1="0.80441875" x2="9.69518125" y2="0.889" layer="21"/>
<rectangle x1="10.45718125" y1="0.80441875" x2="10.71118125" y2="0.889" layer="21"/>
<rectangle x1="11.557" y1="0.80441875" x2="11.811" y2="0.889" layer="21"/>
<rectangle x1="11.98118125" y1="0.80441875" x2="12.14881875" y2="0.889" layer="21"/>
<rectangle x1="12.99718125" y1="0.80441875" x2="13.16481875" y2="0.889" layer="21"/>
<rectangle x1="13.67281875" y1="0.80441875" x2="13.92681875" y2="0.889" layer="21"/>
<rectangle x1="15.113" y1="0.80441875" x2="15.367" y2="0.889" layer="21"/>
<rectangle x1="15.621" y1="0.80441875" x2="15.875" y2="0.889" layer="21"/>
<rectangle x1="16.72081875" y1="0.80441875" x2="16.97481875" y2="0.889" layer="21"/>
<rectangle x1="17.145" y1="0.80441875" x2="17.399" y2="0.889" layer="21"/>
<rectangle x1="18.161" y1="0.80441875" x2="18.33118125" y2="0.889" layer="21"/>
<rectangle x1="2.32918125" y1="0.889" x2="2.58318125" y2="0.97358125" layer="21"/>
<rectangle x1="3.937" y1="0.889" x2="4.191" y2="0.97358125" layer="21"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.61518125" y2="0.97358125" layer="21"/>
<rectangle x1="5.54481875" y1="0.889" x2="5.715" y2="0.97358125" layer="21"/>
<rectangle x1="5.88518125" y1="0.889" x2="6.13918125" y2="0.97358125" layer="21"/>
<rectangle x1="6.477" y1="0.889" x2="6.731" y2="0.97358125" layer="21"/>
<rectangle x1="7.493" y1="0.889" x2="7.747" y2="0.97358125" layer="21"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.10081875" y2="0.97358125" layer="21"/>
<rectangle x1="9.35481875" y1="0.889" x2="9.60881875" y2="0.97358125" layer="21"/>
<rectangle x1="10.45718125" y1="0.889" x2="10.71118125" y2="0.97358125" layer="21"/>
<rectangle x1="11.47318125" y1="0.889" x2="11.72718125" y2="0.97358125" layer="21"/>
<rectangle x1="11.89481875" y1="0.889" x2="12.14881875" y2="0.97358125" layer="21"/>
<rectangle x1="13.589" y1="0.889" x2="13.843" y2="0.97358125" layer="21"/>
<rectangle x1="15.19681875" y1="0.889" x2="15.45081875" y2="0.97358125" layer="21"/>
<rectangle x1="15.621" y1="0.889" x2="15.875" y2="0.97358125" layer="21"/>
<rectangle x1="16.72081875" y1="0.889" x2="16.97481875" y2="0.97358125" layer="21"/>
<rectangle x1="17.06118125" y1="0.889" x2="17.31518125" y2="0.97358125" layer="21"/>
<rectangle x1="2.32918125" y1="0.97358125" x2="2.49681875" y2="1.05841875" layer="21"/>
<rectangle x1="3.937" y1="0.97358125" x2="4.191" y2="1.05841875" layer="21"/>
<rectangle x1="4.36118125" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="21"/>
<rectangle x1="5.54481875" y1="0.97358125" x2="5.715" y2="1.05841875" layer="21"/>
<rectangle x1="5.88518125" y1="0.97358125" x2="6.13918125" y2="1.05841875" layer="21"/>
<rectangle x1="6.477" y1="0.97358125" x2="6.64718125" y2="1.05841875" layer="21"/>
<rectangle x1="7.493" y1="0.97358125" x2="7.747" y2="1.05841875" layer="21"/>
<rectangle x1="8.93318125" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="21"/>
<rectangle x1="9.35481875" y1="0.97358125" x2="9.60881875" y2="1.05841875" layer="21"/>
<rectangle x1="10.45718125" y1="0.97358125" x2="10.71118125" y2="1.05841875" layer="21"/>
<rectangle x1="11.21918125" y1="0.97358125" x2="11.72718125" y2="1.05841875" layer="21"/>
<rectangle x1="11.89481875" y1="0.97358125" x2="13.25118125" y2="1.05841875" layer="21"/>
<rectangle x1="13.589" y1="0.97358125" x2="13.843" y2="1.05841875" layer="21"/>
<rectangle x1="15.19681875" y1="0.97358125" x2="15.45081875" y2="1.05841875" layer="21"/>
<rectangle x1="15.621" y1="0.97358125" x2="15.875" y2="1.05841875" layer="21"/>
<rectangle x1="16.72081875" y1="0.97358125" x2="16.97481875" y2="1.05841875" layer="21"/>
<rectangle x1="17.06118125" y1="0.97358125" x2="18.415" y2="1.05841875" layer="21"/>
<rectangle x1="2.24281875" y1="1.05841875" x2="2.49681875" y2="1.143" layer="21"/>
<rectangle x1="3.937" y1="1.05841875" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="4.36118125" y1="1.05841875" x2="4.61518125" y2="1.143" layer="21"/>
<rectangle x1="5.54481875" y1="1.05841875" x2="5.715" y2="1.143" layer="21"/>
<rectangle x1="5.88518125" y1="1.05841875" x2="6.13918125" y2="1.143" layer="21"/>
<rectangle x1="6.477" y1="1.05841875" x2="6.64718125" y2="1.143" layer="21"/>
<rectangle x1="7.493" y1="1.05841875" x2="7.747" y2="1.143" layer="21"/>
<rectangle x1="8.93318125" y1="1.05841875" x2="9.18718125" y2="1.143" layer="21"/>
<rectangle x1="9.35481875" y1="1.05841875" x2="9.60881875" y2="1.143" layer="21"/>
<rectangle x1="10.45718125" y1="1.05841875" x2="10.71118125" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="1.05841875" x2="11.557" y2="1.143" layer="21"/>
<rectangle x1="11.89481875" y1="1.05841875" x2="13.25118125" y2="1.143" layer="21"/>
<rectangle x1="13.589" y1="1.05841875" x2="13.75918125" y2="1.143" layer="21"/>
<rectangle x1="15.28318125" y1="1.05841875" x2="15.53718125" y2="1.143" layer="21"/>
<rectangle x1="15.621" y1="1.05841875" x2="15.875" y2="1.143" layer="21"/>
<rectangle x1="16.72081875" y1="1.05841875" x2="16.97481875" y2="1.143" layer="21"/>
<rectangle x1="17.06118125" y1="1.05841875" x2="18.415" y2="1.143" layer="21"/>
<rectangle x1="2.24281875" y1="1.143" x2="2.49681875" y2="1.22758125" layer="21"/>
<rectangle x1="4.02081875" y1="1.143" x2="4.191" y2="1.22758125" layer="21"/>
<rectangle x1="4.36118125" y1="1.143" x2="4.61518125" y2="1.22758125" layer="21"/>
<rectangle x1="5.54481875" y1="1.143" x2="5.715" y2="1.22758125" layer="21"/>
<rectangle x1="5.88518125" y1="1.143" x2="6.13918125" y2="1.22758125" layer="21"/>
<rectangle x1="6.477" y1="1.143" x2="6.64718125" y2="1.22758125" layer="21"/>
<rectangle x1="7.493" y1="1.143" x2="7.747" y2="1.22758125" layer="21"/>
<rectangle x1="9.017" y1="1.143" x2="9.271" y2="1.22758125" layer="21"/>
<rectangle x1="9.35481875" y1="1.143" x2="9.60881875" y2="1.22758125" layer="21"/>
<rectangle x1="10.45718125" y1="1.143" x2="10.71118125" y2="1.22758125" layer="21"/>
<rectangle x1="10.87881875" y1="1.143" x2="11.38681875" y2="1.22758125" layer="21"/>
<rectangle x1="11.89481875" y1="1.143" x2="13.25118125" y2="1.22758125" layer="21"/>
<rectangle x1="13.50518125" y1="1.143" x2="13.75918125" y2="1.22758125" layer="21"/>
<rectangle x1="15.28318125" y1="1.143" x2="15.53718125" y2="1.22758125" layer="21"/>
<rectangle x1="15.621" y1="1.143" x2="15.875" y2="1.22758125" layer="21"/>
<rectangle x1="16.72081875" y1="1.143" x2="16.97481875" y2="1.22758125" layer="21"/>
<rectangle x1="17.06118125" y1="1.143" x2="18.415" y2="1.22758125" layer="21"/>
<rectangle x1="2.24281875" y1="1.22758125" x2="2.49681875" y2="1.31241875" layer="21"/>
<rectangle x1="4.02081875" y1="1.22758125" x2="4.191" y2="1.31241875" layer="21"/>
<rectangle x1="4.36118125" y1="1.22758125" x2="4.61518125" y2="1.31241875" layer="21"/>
<rectangle x1="5.461" y1="1.22758125" x2="5.715" y2="1.31241875" layer="21"/>
<rectangle x1="5.88518125" y1="1.22758125" x2="6.13918125" y2="1.31241875" layer="21"/>
<rectangle x1="6.477" y1="1.22758125" x2="6.64718125" y2="1.31241875" layer="21"/>
<rectangle x1="7.493" y1="1.22758125" x2="7.747" y2="1.31241875" layer="21"/>
<rectangle x1="9.017" y1="1.22758125" x2="9.271" y2="1.31241875" layer="21"/>
<rectangle x1="9.35481875" y1="1.22758125" x2="9.60881875" y2="1.31241875" layer="21"/>
<rectangle x1="10.45718125" y1="1.22758125" x2="10.71118125" y2="1.31241875" layer="21"/>
<rectangle x1="10.795" y1="1.22758125" x2="11.21918125" y2="1.31241875" layer="21"/>
<rectangle x1="11.89481875" y1="1.22758125" x2="12.14881875" y2="1.31241875" layer="21"/>
<rectangle x1="12.99718125" y1="1.22758125" x2="13.25118125" y2="1.31241875" layer="21"/>
<rectangle x1="13.50518125" y1="1.22758125" x2="13.75918125" y2="1.31241875" layer="21"/>
<rectangle x1="15.28318125" y1="1.22758125" x2="15.53718125" y2="1.31241875" layer="21"/>
<rectangle x1="15.621" y1="1.22758125" x2="15.875" y2="1.31241875" layer="21"/>
<rectangle x1="16.72081875" y1="1.22758125" x2="16.97481875" y2="1.31241875" layer="21"/>
<rectangle x1="17.06118125" y1="1.22758125" x2="17.31518125" y2="1.31241875" layer="21"/>
<rectangle x1="18.161" y1="1.22758125" x2="18.415" y2="1.31241875" layer="21"/>
<rectangle x1="2.24281875" y1="1.31241875" x2="2.49681875" y2="1.397" layer="21"/>
<rectangle x1="4.02081875" y1="1.31241875" x2="4.191" y2="1.397" layer="21"/>
<rectangle x1="4.36118125" y1="1.31241875" x2="4.699" y2="1.397" layer="21"/>
<rectangle x1="5.461" y1="1.31241875" x2="5.715" y2="1.397" layer="21"/>
<rectangle x1="5.88518125" y1="1.31241875" x2="6.13918125" y2="1.397" layer="21"/>
<rectangle x1="6.477" y1="1.31241875" x2="6.64718125" y2="1.397" layer="21"/>
<rectangle x1="7.493" y1="1.31241875" x2="7.747" y2="1.397" layer="21"/>
<rectangle x1="9.017" y1="1.31241875" x2="9.271" y2="1.397" layer="21"/>
<rectangle x1="9.44118125" y1="1.31241875" x2="9.69518125" y2="1.397" layer="21"/>
<rectangle x1="10.45718125" y1="1.31241875" x2="10.62481875" y2="1.397" layer="21"/>
<rectangle x1="10.795" y1="1.31241875" x2="11.049" y2="1.397" layer="21"/>
<rectangle x1="11.64081875" y1="1.31241875" x2="11.72718125" y2="1.397" layer="21"/>
<rectangle x1="11.98118125" y1="1.31241875" x2="12.23518125" y2="1.397" layer="21"/>
<rectangle x1="12.99718125" y1="1.31241875" x2="13.16481875" y2="1.397" layer="21"/>
<rectangle x1="13.50518125" y1="1.31241875" x2="13.75918125" y2="1.397" layer="21"/>
<rectangle x1="15.28318125" y1="1.31241875" x2="15.53718125" y2="1.397" layer="21"/>
<rectangle x1="15.621" y1="1.31241875" x2="15.875" y2="1.397" layer="21"/>
<rectangle x1="16.637" y1="1.31241875" x2="16.891" y2="1.397" layer="21"/>
<rectangle x1="17.145" y1="1.31241875" x2="17.399" y2="1.397" layer="21"/>
<rectangle x1="18.161" y1="1.31241875" x2="18.415" y2="1.397" layer="21"/>
<rectangle x1="2.24281875" y1="1.397" x2="2.49681875" y2="1.48158125" layer="21"/>
<rectangle x1="4.02081875" y1="1.397" x2="4.191" y2="1.48158125" layer="21"/>
<rectangle x1="4.36118125" y1="1.397" x2="4.78281875" y2="1.48158125" layer="21"/>
<rectangle x1="5.37718125" y1="1.397" x2="5.63118125" y2="1.48158125" layer="21"/>
<rectangle x1="5.88518125" y1="1.397" x2="6.13918125" y2="1.48158125" layer="21"/>
<rectangle x1="6.477" y1="1.397" x2="6.64718125" y2="1.48158125" layer="21"/>
<rectangle x1="7.493" y1="1.397" x2="7.747" y2="1.48158125" layer="21"/>
<rectangle x1="9.017" y1="1.397" x2="9.271" y2="1.48158125" layer="21"/>
<rectangle x1="9.44118125" y1="1.397" x2="9.69518125" y2="1.48158125" layer="21"/>
<rectangle x1="10.37081875" y1="1.397" x2="10.62481875" y2="1.48158125" layer="21"/>
<rectangle x1="10.795" y1="1.397" x2="11.049" y2="1.48158125" layer="21"/>
<rectangle x1="11.557" y1="1.397" x2="11.811" y2="1.48158125" layer="21"/>
<rectangle x1="11.98118125" y1="1.397" x2="12.319" y2="1.48158125" layer="21"/>
<rectangle x1="12.91081875" y1="1.397" x2="13.16481875" y2="1.48158125" layer="21"/>
<rectangle x1="13.50518125" y1="1.397" x2="13.75918125" y2="1.48158125" layer="21"/>
<rectangle x1="15.28318125" y1="1.397" x2="15.53718125" y2="1.48158125" layer="21"/>
<rectangle x1="15.621" y1="1.397" x2="15.95881875" y2="1.48158125" layer="21"/>
<rectangle x1="16.637" y1="1.397" x2="16.891" y2="1.48158125" layer="21"/>
<rectangle x1="17.145" y1="1.397" x2="17.48281875" y2="1.48158125" layer="21"/>
<rectangle x1="18.07718125" y1="1.397" x2="18.33118125" y2="1.48158125" layer="21"/>
<rectangle x1="2.24281875" y1="1.48158125" x2="2.49681875" y2="1.56641875" layer="21"/>
<rectangle x1="4.02081875" y1="1.48158125" x2="4.191" y2="1.56641875" layer="21"/>
<rectangle x1="4.36118125" y1="1.48158125" x2="4.953" y2="1.56641875" layer="21"/>
<rectangle x1="5.207" y1="1.48158125" x2="5.63118125" y2="1.56641875" layer="21"/>
<rectangle x1="5.88518125" y1="1.48158125" x2="6.13918125" y2="1.56641875" layer="21"/>
<rectangle x1="6.477" y1="1.48158125" x2="6.64718125" y2="1.56641875" layer="21"/>
<rectangle x1="7.493" y1="1.48158125" x2="7.747" y2="1.56641875" layer="21"/>
<rectangle x1="9.017" y1="1.48158125" x2="9.271" y2="1.56641875" layer="21"/>
<rectangle x1="9.525" y1="1.48158125" x2="9.86281875" y2="1.56641875" layer="21"/>
<rectangle x1="10.20318125" y1="1.48158125" x2="10.541" y2="1.56641875" layer="21"/>
<rectangle x1="10.87881875" y1="1.48158125" x2="11.21918125" y2="1.56641875" layer="21"/>
<rectangle x1="11.47318125" y1="1.48158125" x2="11.72718125" y2="1.56641875" layer="21"/>
<rectangle x1="12.065" y1="1.48158125" x2="12.40281875" y2="1.56641875" layer="21"/>
<rectangle x1="12.74318125" y1="1.48158125" x2="13.081" y2="1.56641875" layer="21"/>
<rectangle x1="13.589" y1="1.48158125" x2="13.75918125" y2="1.56641875" layer="21"/>
<rectangle x1="15.28318125" y1="1.48158125" x2="15.53718125" y2="1.56641875" layer="21"/>
<rectangle x1="15.621" y1="1.48158125" x2="16.129" y2="1.56641875" layer="21"/>
<rectangle x1="16.46681875" y1="1.48158125" x2="16.80718125" y2="1.56641875" layer="21"/>
<rectangle x1="17.22881875" y1="1.48158125" x2="17.653" y2="1.56641875" layer="21"/>
<rectangle x1="17.907" y1="1.48158125" x2="18.24481875" y2="1.56641875" layer="21"/>
<rectangle x1="2.24281875" y1="1.56641875" x2="2.49681875" y2="1.651" layer="21"/>
<rectangle x1="4.02081875" y1="1.56641875" x2="4.191" y2="1.651" layer="21"/>
<rectangle x1="4.36118125" y1="1.56641875" x2="5.54481875" y2="1.651" layer="21"/>
<rectangle x1="5.88518125" y1="1.56641875" x2="6.13918125" y2="1.651" layer="21"/>
<rectangle x1="6.223" y1="1.56641875" x2="7.239" y2="1.651" layer="21"/>
<rectangle x1="7.493" y1="1.56641875" x2="7.747" y2="1.651" layer="21"/>
<rectangle x1="9.017" y1="1.56641875" x2="9.271" y2="1.651" layer="21"/>
<rectangle x1="9.60881875" y1="1.56641875" x2="10.45718125" y2="1.651" layer="21"/>
<rectangle x1="10.87881875" y1="1.56641875" x2="11.72718125" y2="1.651" layer="21"/>
<rectangle x1="12.14881875" y1="1.56641875" x2="12.99718125" y2="1.651" layer="21"/>
<rectangle x1="13.589" y1="1.56641875" x2="13.843" y2="1.651" layer="21"/>
<rectangle x1="15.19681875" y1="1.56641875" x2="15.45081875" y2="1.651" layer="21"/>
<rectangle x1="15.621" y1="1.56641875" x2="16.72081875" y2="1.651" layer="21"/>
<rectangle x1="17.31518125" y1="1.56641875" x2="18.161" y2="1.651" layer="21"/>
<rectangle x1="2.24281875" y1="1.651" x2="2.49681875" y2="1.73558125" layer="21"/>
<rectangle x1="4.02081875" y1="1.651" x2="4.191" y2="1.73558125" layer="21"/>
<rectangle x1="4.36118125" y1="1.651" x2="4.61518125" y2="1.73558125" layer="21"/>
<rectangle x1="4.78281875" y1="1.651" x2="5.37718125" y2="1.73558125" layer="21"/>
<rectangle x1="5.88518125" y1="1.651" x2="6.13918125" y2="1.73558125" layer="21"/>
<rectangle x1="6.223" y1="1.651" x2="7.239" y2="1.73558125" layer="21"/>
<rectangle x1="7.493" y1="1.651" x2="7.747" y2="1.73558125" layer="21"/>
<rectangle x1="9.017" y1="1.651" x2="9.18718125" y2="1.73558125" layer="21"/>
<rectangle x1="9.69518125" y1="1.651" x2="10.37081875" y2="1.73558125" layer="21"/>
<rectangle x1="11.049" y1="1.651" x2="11.557" y2="1.73558125" layer="21"/>
<rectangle x1="12.23518125" y1="1.651" x2="12.91081875" y2="1.73558125" layer="21"/>
<rectangle x1="13.589" y1="1.651" x2="13.843" y2="1.73558125" layer="21"/>
<rectangle x1="15.19681875" y1="1.651" x2="15.45081875" y2="1.73558125" layer="21"/>
<rectangle x1="15.621" y1="1.651" x2="15.79118125" y2="1.73558125" layer="21"/>
<rectangle x1="15.95881875" y1="1.651" x2="16.637" y2="1.73558125" layer="21"/>
<rectangle x1="17.399" y1="1.651" x2="18.07718125" y2="1.73558125" layer="21"/>
<rectangle x1="2.24281875" y1="1.73558125" x2="2.49681875" y2="1.82041875" layer="21"/>
<rectangle x1="4.02081875" y1="1.73558125" x2="4.191" y2="1.82041875" layer="21"/>
<rectangle x1="4.445" y1="1.73558125" x2="4.52881875" y2="1.82041875" layer="21"/>
<rectangle x1="4.953" y1="1.73558125" x2="5.12318125" y2="1.82041875" layer="21"/>
<rectangle x1="5.969" y1="1.73558125" x2="6.05281875" y2="1.82041875" layer="21"/>
<rectangle x1="6.30681875" y1="1.73558125" x2="7.15518125" y2="1.82041875" layer="21"/>
<rectangle x1="7.493" y1="1.73558125" x2="7.747" y2="1.82041875" layer="21"/>
<rectangle x1="8.93318125" y1="1.73558125" x2="9.18718125" y2="1.82041875" layer="21"/>
<rectangle x1="9.94918125" y1="1.73558125" x2="10.11681875" y2="1.82041875" layer="21"/>
<rectangle x1="11.21918125" y1="1.73558125" x2="11.38681875" y2="1.82041875" layer="21"/>
<rectangle x1="12.48918125" y1="1.73558125" x2="12.65681875" y2="1.82041875" layer="21"/>
<rectangle x1="13.67281875" y1="1.73558125" x2="13.92681875" y2="1.82041875" layer="21"/>
<rectangle x1="15.113" y1="1.73558125" x2="15.367" y2="1.82041875" layer="21"/>
<rectangle x1="16.21281875" y1="1.73558125" x2="16.383" y2="1.82041875" layer="21"/>
<rectangle x1="17.653" y1="1.73558125" x2="17.82318125" y2="1.82041875" layer="21"/>
<rectangle x1="2.24281875" y1="1.82041875" x2="2.49681875" y2="1.905" layer="21"/>
<rectangle x1="4.02081875" y1="1.82041875" x2="4.191" y2="1.905" layer="21"/>
<rectangle x1="5.88518125" y1="1.82041875" x2="6.13918125" y2="1.905" layer="21"/>
<rectangle x1="6.477" y1="1.82041875" x2="6.64718125" y2="1.905" layer="21"/>
<rectangle x1="7.493" y1="1.82041875" x2="7.747" y2="1.905" layer="21"/>
<rectangle x1="8.84681875" y1="1.82041875" x2="9.18718125" y2="1.905" layer="21"/>
<rectangle x1="13.67281875" y1="1.82041875" x2="14.01318125" y2="1.905" layer="21"/>
<rectangle x1="15.02918125" y1="1.82041875" x2="15.367" y2="1.905" layer="21"/>
<rectangle x1="2.24281875" y1="1.905" x2="2.49681875" y2="1.98958125" layer="21"/>
<rectangle x1="4.02081875" y1="1.905" x2="4.191" y2="1.98958125" layer="21"/>
<rectangle x1="5.88518125" y1="1.905" x2="6.13918125" y2="1.98958125" layer="21"/>
<rectangle x1="6.477" y1="1.905" x2="6.64718125" y2="1.98958125" layer="21"/>
<rectangle x1="7.493" y1="1.905" x2="7.747" y2="1.98958125" layer="21"/>
<rectangle x1="8.84681875" y1="1.905" x2="9.10081875" y2="1.98958125" layer="21"/>
<rectangle x1="13.75918125" y1="1.905" x2="14.097" y2="1.98958125" layer="21"/>
<rectangle x1="14.94281875" y1="1.905" x2="15.28318125" y2="1.98958125" layer="21"/>
<rectangle x1="2.24281875" y1="1.98958125" x2="2.49681875" y2="2.07441875" layer="21"/>
<rectangle x1="4.02081875" y1="1.98958125" x2="4.27481875" y2="2.07441875" layer="21"/>
<rectangle x1="5.88518125" y1="1.98958125" x2="6.05281875" y2="2.07441875" layer="21"/>
<rectangle x1="6.477" y1="1.98958125" x2="6.64718125" y2="2.07441875" layer="21"/>
<rectangle x1="7.493" y1="1.98958125" x2="7.747" y2="2.07441875" layer="21"/>
<rectangle x1="8.67918125" y1="1.98958125" x2="9.017" y2="2.07441875" layer="21"/>
<rectangle x1="13.843" y1="1.98958125" x2="14.26718125" y2="2.07441875" layer="21"/>
<rectangle x1="14.77518125" y1="1.98958125" x2="15.19681875" y2="2.07441875" layer="21"/>
<rectangle x1="2.24281875" y1="2.07441875" x2="2.49681875" y2="2.159" layer="21"/>
<rectangle x1="4.02081875" y1="2.07441875" x2="4.27481875" y2="2.159" layer="21"/>
<rectangle x1="6.477" y1="2.07441875" x2="6.64718125" y2="2.159" layer="21"/>
<rectangle x1="7.493" y1="2.07441875" x2="7.747" y2="2.159" layer="21"/>
<rectangle x1="8.59281875" y1="2.07441875" x2="8.93318125" y2="2.159" layer="21"/>
<rectangle x1="13.92681875" y1="2.07441875" x2="15.113" y2="2.159" layer="21"/>
<rectangle x1="2.24281875" y1="2.159" x2="2.49681875" y2="2.24358125" layer="21"/>
<rectangle x1="4.02081875" y1="2.159" x2="4.191" y2="2.24358125" layer="21"/>
<rectangle x1="6.477" y1="2.159" x2="6.64718125" y2="2.24358125" layer="21"/>
<rectangle x1="7.493" y1="2.159" x2="8.84681875" y2="2.24358125" layer="21"/>
<rectangle x1="14.097" y1="2.159" x2="14.94281875" y2="2.24358125" layer="21"/>
<rectangle x1="2.24281875" y1="2.24358125" x2="2.413" y2="2.32841875" layer="21"/>
<rectangle x1="4.02081875" y1="2.24358125" x2="4.191" y2="2.32841875" layer="21"/>
<rectangle x1="6.477" y1="2.24358125" x2="6.64718125" y2="2.32841875" layer="21"/>
<rectangle x1="7.493" y1="2.24358125" x2="8.763" y2="2.32841875" layer="21"/>
<rectangle x1="14.26718125" y1="2.24358125" x2="14.77518125" y2="2.32841875" layer="21"/>
<rectangle x1="7.493" y1="2.32841875" x2="8.509" y2="2.413" layer="21"/>
<rectangle x1="3.34518125" y1="3.25958125" x2="16.80718125" y2="3.34441875" layer="21"/>
<rectangle x1="3.00481875" y1="3.34441875" x2="17.145" y2="3.429" layer="21"/>
<rectangle x1="2.667" y1="3.429" x2="17.399" y2="3.51358125" layer="21"/>
<rectangle x1="2.49681875" y1="3.51358125" x2="17.56918125" y2="3.59841875" layer="21"/>
<rectangle x1="2.32918125" y1="3.59841875" x2="17.82318125" y2="3.683" layer="21"/>
<rectangle x1="2.159" y1="3.683" x2="17.99081875" y2="3.76758125" layer="21"/>
<rectangle x1="1.98881875" y1="3.76758125" x2="18.07718125" y2="3.85241875" layer="21"/>
<rectangle x1="1.905" y1="3.85241875" x2="3.51281875" y2="3.937" layer="21"/>
<rectangle x1="9.779" y1="3.85241875" x2="18.24481875" y2="3.937" layer="21"/>
<rectangle x1="1.73481875" y1="3.937" x2="3.175" y2="4.02158125" layer="21"/>
<rectangle x1="9.779" y1="3.937" x2="18.33118125" y2="4.02158125" layer="21"/>
<rectangle x1="1.651" y1="4.02158125" x2="2.83718125" y2="4.10641875" layer="21"/>
<rectangle x1="9.69518125" y1="4.02158125" x2="18.49881875" y2="4.10641875" layer="21"/>
<rectangle x1="1.56718125" y1="4.10641875" x2="2.667" y2="4.191" layer="21"/>
<rectangle x1="9.60881875" y1="4.10641875" x2="18.58518125" y2="4.191" layer="21"/>
<rectangle x1="1.397" y1="4.191" x2="2.49681875" y2="4.27558125" layer="21"/>
<rectangle x1="9.60881875" y1="4.191" x2="18.669" y2="4.27558125" layer="21"/>
<rectangle x1="1.31318125" y1="4.27558125" x2="2.32918125" y2="4.36041875" layer="21"/>
<rectangle x1="9.525" y1="4.27558125" x2="18.75281875" y2="4.36041875" layer="21"/>
<rectangle x1="1.22681875" y1="4.36041875" x2="2.159" y2="4.445" layer="21"/>
<rectangle x1="9.525" y1="4.36041875" x2="18.83918125" y2="4.445" layer="21"/>
<rectangle x1="1.143" y1="4.445" x2="2.07518125" y2="4.52958125" layer="21"/>
<rectangle x1="9.44118125" y1="4.445" x2="18.923" y2="4.52958125" layer="21"/>
<rectangle x1="1.05918125" y1="4.52958125" x2="1.98881875" y2="4.61441875" layer="21"/>
<rectangle x1="9.44118125" y1="4.52958125" x2="19.00681875" y2="4.61441875" layer="21"/>
<rectangle x1="0.97281875" y1="4.61441875" x2="1.905" y2="4.699" layer="21"/>
<rectangle x1="9.44118125" y1="4.61441875" x2="19.09318125" y2="4.699" layer="21"/>
<rectangle x1="0.97281875" y1="4.699" x2="1.73481875" y2="4.78358125" layer="21"/>
<rectangle x1="9.35481875" y1="4.699" x2="19.177" y2="4.78358125" layer="21"/>
<rectangle x1="0.889" y1="4.78358125" x2="1.651" y2="4.86841875" layer="21"/>
<rectangle x1="9.35481875" y1="4.78358125" x2="19.26081875" y2="4.86841875" layer="21"/>
<rectangle x1="0.80518125" y1="4.86841875" x2="1.56718125" y2="4.953" layer="21"/>
<rectangle x1="9.35481875" y1="4.86841875" x2="19.26081875" y2="4.953" layer="21"/>
<rectangle x1="0.71881875" y1="4.953" x2="1.48081875" y2="5.03758125" layer="21"/>
<rectangle x1="9.271" y1="4.953" x2="19.34718125" y2="5.03758125" layer="21"/>
<rectangle x1="0.71881875" y1="5.03758125" x2="1.48081875" y2="5.12241875" layer="21"/>
<rectangle x1="9.271" y1="5.03758125" x2="19.431" y2="5.12241875" layer="21"/>
<rectangle x1="0.635" y1="5.12241875" x2="1.397" y2="5.207" layer="21"/>
<rectangle x1="9.271" y1="5.12241875" x2="19.431" y2="5.207" layer="21"/>
<rectangle x1="0.55118125" y1="5.207" x2="1.31318125" y2="5.29158125" layer="21"/>
<rectangle x1="9.271" y1="5.207" x2="19.51481875" y2="5.29158125" layer="21"/>
<rectangle x1="0.55118125" y1="5.29158125" x2="1.22681875" y2="5.37641875" layer="21"/>
<rectangle x1="9.18718125" y1="5.29158125" x2="19.51481875" y2="5.37641875" layer="21"/>
<rectangle x1="0.46481875" y1="5.37641875" x2="1.22681875" y2="5.461" layer="21"/>
<rectangle x1="9.18718125" y1="5.37641875" x2="19.60118125" y2="5.461" layer="21"/>
<rectangle x1="0.46481875" y1="5.461" x2="1.143" y2="5.54558125" layer="21"/>
<rectangle x1="9.18718125" y1="5.461" x2="19.685" y2="5.54558125" layer="21"/>
<rectangle x1="0.381" y1="5.54558125" x2="1.05918125" y2="5.63041875" layer="21"/>
<rectangle x1="3.85318125" y1="5.54558125" x2="4.27481875" y2="5.63041875" layer="21"/>
<rectangle x1="5.88518125" y1="5.54558125" x2="6.81481875" y2="5.63041875" layer="21"/>
<rectangle x1="9.18718125" y1="5.54558125" x2="19.685" y2="5.63041875" layer="21"/>
<rectangle x1="0.381" y1="5.63041875" x2="1.05918125" y2="5.715" layer="21"/>
<rectangle x1="3.51281875" y1="5.63041875" x2="4.61518125" y2="5.715" layer="21"/>
<rectangle x1="5.79881875" y1="5.63041875" x2="7.239" y2="5.715" layer="21"/>
<rectangle x1="9.10081875" y1="5.63041875" x2="10.96518125" y2="5.715" layer="21"/>
<rectangle x1="11.64081875" y1="5.63041875" x2="19.685" y2="5.715" layer="21"/>
<rectangle x1="0.381" y1="5.715" x2="0.97281875" y2="5.79958125" layer="21"/>
<rectangle x1="3.34518125" y1="5.715" x2="4.78281875" y2="5.79958125" layer="21"/>
<rectangle x1="5.715" y1="5.715" x2="7.40918125" y2="5.79958125" layer="21"/>
<rectangle x1="9.10081875" y1="5.715" x2="10.71118125" y2="5.79958125" layer="21"/>
<rectangle x1="11.89481875" y1="5.715" x2="13.335" y2="5.79958125" layer="21"/>
<rectangle x1="13.67281875" y1="5.715" x2="15.113" y2="5.79958125" layer="21"/>
<rectangle x1="15.367" y1="5.715" x2="16.383" y2="5.79958125" layer="21"/>
<rectangle x1="17.22881875" y1="5.715" x2="19.76881875" y2="5.79958125" layer="21"/>
<rectangle x1="0.29718125" y1="5.79958125" x2="0.97281875" y2="5.88441875" layer="21"/>
<rectangle x1="3.175" y1="5.79958125" x2="4.953" y2="5.88441875" layer="21"/>
<rectangle x1="5.715" y1="5.79958125" x2="7.57681875" y2="5.88441875" layer="21"/>
<rectangle x1="9.10081875" y1="5.79958125" x2="10.541" y2="5.88441875" layer="21"/>
<rectangle x1="12.065" y1="5.79958125" x2="13.335" y2="5.88441875" layer="21"/>
<rectangle x1="13.67281875" y1="5.79958125" x2="15.113" y2="5.88441875" layer="21"/>
<rectangle x1="15.45081875" y1="5.79958125" x2="16.21281875" y2="5.88441875" layer="21"/>
<rectangle x1="17.48281875" y1="5.79958125" x2="19.76881875" y2="5.88441875" layer="21"/>
<rectangle x1="0.29718125" y1="5.88441875" x2="0.889" y2="5.969" layer="21"/>
<rectangle x1="3.09118125" y1="5.88441875" x2="5.12318125" y2="5.969" layer="21"/>
<rectangle x1="5.715" y1="5.88441875" x2="7.66318125" y2="5.969" layer="21"/>
<rectangle x1="9.10081875" y1="5.88441875" x2="10.37081875" y2="5.969" layer="21"/>
<rectangle x1="12.23518125" y1="5.88441875" x2="13.335" y2="5.969" layer="21"/>
<rectangle x1="13.67281875" y1="5.88441875" x2="15.113" y2="5.969" layer="21"/>
<rectangle x1="15.45081875" y1="5.88441875" x2="16.129" y2="5.969" layer="21"/>
<rectangle x1="17.56918125" y1="5.88441875" x2="19.85518125" y2="5.969" layer="21"/>
<rectangle x1="0.21081875" y1="5.969" x2="0.889" y2="6.05358125" layer="21"/>
<rectangle x1="2.921" y1="5.969" x2="5.207" y2="6.05358125" layer="21"/>
<rectangle x1="5.715" y1="5.969" x2="7.747" y2="6.05358125" layer="21"/>
<rectangle x1="9.10081875" y1="5.969" x2="10.287" y2="6.05358125" layer="21"/>
<rectangle x1="12.319" y1="5.969" x2="13.335" y2="6.05358125" layer="21"/>
<rectangle x1="13.67281875" y1="5.969" x2="15.113" y2="6.05358125" layer="21"/>
<rectangle x1="15.45081875" y1="5.969" x2="16.04518125" y2="6.05358125" layer="21"/>
<rectangle x1="17.653" y1="5.969" x2="19.85518125" y2="6.05358125" layer="21"/>
<rectangle x1="0.21081875" y1="6.05358125" x2="0.889" y2="6.13841875" layer="21"/>
<rectangle x1="2.83718125" y1="6.05358125" x2="5.29081875" y2="6.13841875" layer="21"/>
<rectangle x1="5.79881875" y1="6.05358125" x2="7.83081875" y2="6.13841875" layer="21"/>
<rectangle x1="9.10081875" y1="6.05358125" x2="10.11681875" y2="6.13841875" layer="21"/>
<rectangle x1="12.40281875" y1="6.05358125" x2="13.335" y2="6.13841875" layer="21"/>
<rectangle x1="13.67281875" y1="6.05358125" x2="15.113" y2="6.13841875" layer="21"/>
<rectangle x1="15.45081875" y1="6.05358125" x2="15.95881875" y2="6.13841875" layer="21"/>
<rectangle x1="16.55318125" y1="6.05358125" x2="17.06118125" y2="6.13841875" layer="21"/>
<rectangle x1="17.73681875" y1="6.05358125" x2="19.85518125" y2="6.13841875" layer="21"/>
<rectangle x1="0.21081875" y1="6.13841875" x2="0.80518125" y2="6.223" layer="21"/>
<rectangle x1="2.75081875" y1="6.13841875" x2="5.37718125" y2="6.223" layer="21"/>
<rectangle x1="5.88518125" y1="6.13841875" x2="7.91718125" y2="6.223" layer="21"/>
<rectangle x1="9.017" y1="6.13841875" x2="10.11681875" y2="6.223" layer="21"/>
<rectangle x1="10.96518125" y1="6.13841875" x2="11.64081875" y2="6.223" layer="21"/>
<rectangle x1="12.48918125" y1="6.13841875" x2="13.335" y2="6.223" layer="21"/>
<rectangle x1="13.67281875" y1="6.13841875" x2="15.02918125" y2="6.223" layer="21"/>
<rectangle x1="15.45081875" y1="6.13841875" x2="15.875" y2="6.223" layer="21"/>
<rectangle x1="16.46681875" y1="6.13841875" x2="17.22881875" y2="6.223" layer="21"/>
<rectangle x1="17.73681875" y1="6.13841875" x2="19.939" y2="6.223" layer="21"/>
<rectangle x1="0.21081875" y1="6.223" x2="0.80518125" y2="6.30758125" layer="21"/>
<rectangle x1="2.75081875" y1="6.223" x2="3.683" y2="6.30758125" layer="21"/>
<rectangle x1="4.445" y1="6.223" x2="5.37718125" y2="6.30758125" layer="21"/>
<rectangle x1="7.06881875" y1="6.223" x2="8.001" y2="6.30758125" layer="21"/>
<rectangle x1="9.017" y1="6.223" x2="10.033" y2="6.30758125" layer="21"/>
<rectangle x1="10.795" y1="6.223" x2="11.811" y2="6.30758125" layer="21"/>
<rectangle x1="12.573" y1="6.223" x2="13.335" y2="6.30758125" layer="21"/>
<rectangle x1="13.67281875" y1="6.223" x2="15.02918125" y2="6.30758125" layer="21"/>
<rectangle x1="15.45081875" y1="6.223" x2="15.875" y2="6.30758125" layer="21"/>
<rectangle x1="16.29918125" y1="6.223" x2="17.399" y2="6.30758125" layer="21"/>
<rectangle x1="17.73681875" y1="6.223" x2="19.939" y2="6.30758125" layer="21"/>
<rectangle x1="0.127" y1="6.30758125" x2="0.80518125" y2="6.39241875" layer="21"/>
<rectangle x1="2.667" y1="6.30758125" x2="3.51281875" y2="6.39241875" layer="21"/>
<rectangle x1="4.61518125" y1="6.30758125" x2="5.461" y2="6.39241875" layer="21"/>
<rectangle x1="7.239" y1="6.30758125" x2="8.08481875" y2="6.39241875" layer="21"/>
<rectangle x1="9.017" y1="6.30758125" x2="9.94918125" y2="6.39241875" layer="21"/>
<rectangle x1="10.62481875" y1="6.30758125" x2="11.98118125" y2="6.39241875" layer="21"/>
<rectangle x1="12.65681875" y1="6.30758125" x2="13.335" y2="6.39241875" layer="21"/>
<rectangle x1="13.67281875" y1="6.30758125" x2="15.113" y2="6.39241875" layer="21"/>
<rectangle x1="15.45081875" y1="6.30758125" x2="15.79118125" y2="6.39241875" layer="21"/>
<rectangle x1="16.21281875" y1="6.30758125" x2="17.48281875" y2="6.39241875" layer="21"/>
<rectangle x1="17.653" y1="6.30758125" x2="19.939" y2="6.39241875" layer="21"/>
<rectangle x1="0.127" y1="6.39241875" x2="0.71881875" y2="6.477" layer="21"/>
<rectangle x1="2.58318125" y1="6.39241875" x2="3.34518125" y2="6.477" layer="21"/>
<rectangle x1="4.78281875" y1="6.39241875" x2="5.54481875" y2="6.477" layer="21"/>
<rectangle x1="7.32281875" y1="6.39241875" x2="8.17118125" y2="6.477" layer="21"/>
<rectangle x1="9.017" y1="6.39241875" x2="9.86281875" y2="6.477" layer="21"/>
<rectangle x1="10.541" y1="6.39241875" x2="12.065" y2="6.477" layer="21"/>
<rectangle x1="12.65681875" y1="6.39241875" x2="13.335" y2="6.477" layer="21"/>
<rectangle x1="13.67281875" y1="6.39241875" x2="15.113" y2="6.477" layer="21"/>
<rectangle x1="15.45081875" y1="6.39241875" x2="15.79118125" y2="6.477" layer="21"/>
<rectangle x1="16.21281875" y1="6.39241875" x2="19.939" y2="6.477" layer="21"/>
<rectangle x1="0.127" y1="6.477" x2="0.71881875" y2="6.56158125" layer="21"/>
<rectangle x1="2.58318125" y1="6.477" x2="3.25881875" y2="6.56158125" layer="21"/>
<rectangle x1="4.86918125" y1="6.477" x2="5.54481875" y2="6.56158125" layer="21"/>
<rectangle x1="7.493" y1="6.477" x2="8.17118125" y2="6.56158125" layer="21"/>
<rectangle x1="9.017" y1="6.477" x2="9.86281875" y2="6.56158125" layer="21"/>
<rectangle x1="10.45718125" y1="6.477" x2="12.14881875" y2="6.56158125" layer="21"/>
<rectangle x1="12.74318125" y1="6.477" x2="13.335" y2="6.56158125" layer="21"/>
<rectangle x1="13.67281875" y1="6.477" x2="15.113" y2="6.56158125" layer="21"/>
<rectangle x1="15.45081875" y1="6.477" x2="15.79118125" y2="6.56158125" layer="21"/>
<rectangle x1="16.21281875" y1="6.477" x2="19.939" y2="6.56158125" layer="21"/>
<rectangle x1="0.127" y1="6.56158125" x2="0.71881875" y2="6.64641875" layer="21"/>
<rectangle x1="2.49681875" y1="6.56158125" x2="3.175" y2="6.64641875" layer="21"/>
<rectangle x1="4.953" y1="6.56158125" x2="5.63118125" y2="6.64641875" layer="21"/>
<rectangle x1="7.493" y1="6.56158125" x2="8.255" y2="6.64641875" layer="21"/>
<rectangle x1="9.017" y1="6.56158125" x2="9.779" y2="6.64641875" layer="21"/>
<rectangle x1="10.37081875" y1="6.56158125" x2="12.23518125" y2="6.64641875" layer="21"/>
<rectangle x1="12.74318125" y1="6.56158125" x2="13.335" y2="6.64641875" layer="21"/>
<rectangle x1="13.67281875" y1="6.56158125" x2="15.02918125" y2="6.64641875" layer="21"/>
<rectangle x1="15.45081875" y1="6.56158125" x2="15.79118125" y2="6.64641875" layer="21"/>
<rectangle x1="17.82318125" y1="6.56158125" x2="19.939" y2="6.64641875" layer="21"/>
<rectangle x1="0.127" y1="6.64641875" x2="0.71881875" y2="6.731" layer="21"/>
<rectangle x1="2.49681875" y1="6.64641875" x2="3.175" y2="6.731" layer="21"/>
<rectangle x1="4.953" y1="6.64641875" x2="5.63118125" y2="6.731" layer="21"/>
<rectangle x1="7.57681875" y1="6.64641875" x2="8.255" y2="6.731" layer="21"/>
<rectangle x1="9.017" y1="6.64641875" x2="9.779" y2="6.731" layer="21"/>
<rectangle x1="10.287" y1="6.64641875" x2="12.319" y2="6.731" layer="21"/>
<rectangle x1="12.827" y1="6.64641875" x2="13.335" y2="6.731" layer="21"/>
<rectangle x1="13.67281875" y1="6.64641875" x2="15.02918125" y2="6.731" layer="21"/>
<rectangle x1="15.45081875" y1="6.64641875" x2="15.70481875" y2="6.731" layer="21"/>
<rectangle x1="17.907" y1="6.64641875" x2="19.939" y2="6.731" layer="21"/>
<rectangle x1="0.04318125" y1="6.731" x2="0.71881875" y2="6.81558125" layer="21"/>
<rectangle x1="2.49681875" y1="6.731" x2="3.09118125" y2="6.81558125" layer="21"/>
<rectangle x1="5.03681875" y1="6.731" x2="5.715" y2="6.81558125" layer="21"/>
<rectangle x1="7.66318125" y1="6.731" x2="8.255" y2="6.81558125" layer="21"/>
<rectangle x1="9.017" y1="6.731" x2="9.779" y2="6.81558125" layer="21"/>
<rectangle x1="10.287" y1="6.731" x2="12.319" y2="6.81558125" layer="21"/>
<rectangle x1="12.827" y1="6.731" x2="13.335" y2="6.81558125" layer="21"/>
<rectangle x1="13.67281875" y1="6.731" x2="15.113" y2="6.81558125" layer="21"/>
<rectangle x1="15.45081875" y1="6.731" x2="15.70481875" y2="6.81558125" layer="21"/>
<rectangle x1="17.907" y1="6.731" x2="19.939" y2="6.81558125" layer="21"/>
<rectangle x1="0.04318125" y1="6.81558125" x2="0.71881875" y2="6.90041875" layer="21"/>
<rectangle x1="2.413" y1="6.81558125" x2="3.09118125" y2="6.90041875" layer="21"/>
<rectangle x1="5.03681875" y1="6.81558125" x2="5.715" y2="6.90041875" layer="21"/>
<rectangle x1="7.66318125" y1="6.81558125" x2="8.33881875" y2="6.90041875" layer="21"/>
<rectangle x1="9.017" y1="6.81558125" x2="9.69518125" y2="6.90041875" layer="21"/>
<rectangle x1="10.20318125" y1="6.81558125" x2="12.319" y2="6.90041875" layer="21"/>
<rectangle x1="12.827" y1="6.81558125" x2="13.335" y2="6.90041875" layer="21"/>
<rectangle x1="13.67281875" y1="6.81558125" x2="15.02918125" y2="6.90041875" layer="21"/>
<rectangle x1="15.45081875" y1="6.81558125" x2="15.70481875" y2="6.90041875" layer="21"/>
<rectangle x1="17.907" y1="6.81558125" x2="19.939" y2="6.90041875" layer="21"/>
<rectangle x1="0.04318125" y1="6.90041875" x2="0.635" y2="6.985" layer="21"/>
<rectangle x1="2.413" y1="6.90041875" x2="3.00481875" y2="6.985" layer="21"/>
<rectangle x1="5.12318125" y1="6.90041875" x2="5.715" y2="6.985" layer="21"/>
<rectangle x1="7.66318125" y1="6.90041875" x2="8.33881875" y2="6.985" layer="21"/>
<rectangle x1="9.017" y1="6.90041875" x2="9.69518125" y2="6.985" layer="21"/>
<rectangle x1="10.20318125" y1="6.90041875" x2="12.40281875" y2="6.985" layer="21"/>
<rectangle x1="12.91081875" y1="6.90041875" x2="13.335" y2="6.985" layer="21"/>
<rectangle x1="13.67281875" y1="6.90041875" x2="15.02918125" y2="6.985" layer="21"/>
<rectangle x1="15.45081875" y1="6.90041875" x2="15.79118125" y2="6.985" layer="21"/>
<rectangle x1="16.21281875" y1="6.90041875" x2="17.399" y2="6.985" layer="21"/>
<rectangle x1="17.907" y1="6.90041875" x2="19.939" y2="6.985" layer="21"/>
<rectangle x1="0.04318125" y1="6.985" x2="0.635" y2="7.06958125" layer="21"/>
<rectangle x1="2.413" y1="6.985" x2="3.00481875" y2="7.06958125" layer="21"/>
<rectangle x1="5.12318125" y1="6.985" x2="5.715" y2="7.06958125" layer="21"/>
<rectangle x1="7.747" y1="6.985" x2="8.33881875" y2="7.06958125" layer="21"/>
<rectangle x1="9.017" y1="6.985" x2="9.69518125" y2="7.06958125" layer="21"/>
<rectangle x1="10.20318125" y1="6.985" x2="12.40281875" y2="7.06958125" layer="21"/>
<rectangle x1="12.91081875" y1="6.985" x2="13.335" y2="7.06958125" layer="21"/>
<rectangle x1="13.75918125" y1="6.985" x2="15.02918125" y2="7.06958125" layer="21"/>
<rectangle x1="15.45081875" y1="6.985" x2="15.79118125" y2="7.06958125" layer="21"/>
<rectangle x1="16.21281875" y1="6.985" x2="17.48281875" y2="7.06958125" layer="21"/>
<rectangle x1="17.82318125" y1="6.985" x2="19.939" y2="7.06958125" layer="21"/>
<rectangle x1="0.04318125" y1="7.06958125" x2="0.635" y2="7.15441875" layer="21"/>
<rectangle x1="2.413" y1="7.06958125" x2="3.00481875" y2="7.15441875" layer="21"/>
<rectangle x1="5.12318125" y1="7.06958125" x2="5.715" y2="7.15441875" layer="21"/>
<rectangle x1="7.747" y1="7.06958125" x2="8.33881875" y2="7.15441875" layer="21"/>
<rectangle x1="9.017" y1="7.06958125" x2="9.69518125" y2="7.15441875" layer="21"/>
<rectangle x1="10.20318125" y1="7.06958125" x2="12.40281875" y2="7.15441875" layer="21"/>
<rectangle x1="12.91081875" y1="7.06958125" x2="13.335" y2="7.15441875" layer="21"/>
<rectangle x1="13.75918125" y1="7.06958125" x2="14.94281875" y2="7.15441875" layer="21"/>
<rectangle x1="15.367" y1="7.06958125" x2="15.79118125" y2="7.15441875" layer="21"/>
<rectangle x1="16.21281875" y1="7.06958125" x2="17.399" y2="7.15441875" layer="21"/>
<rectangle x1="17.82318125" y1="7.06958125" x2="19.939" y2="7.15441875" layer="21"/>
<rectangle x1="0.04318125" y1="7.15441875" x2="0.635" y2="7.239" layer="21"/>
<rectangle x1="2.413" y1="7.15441875" x2="3.00481875" y2="7.239" layer="21"/>
<rectangle x1="5.12318125" y1="7.15441875" x2="5.715" y2="7.239" layer="21"/>
<rectangle x1="7.747" y1="7.15441875" x2="8.33881875" y2="7.239" layer="21"/>
<rectangle x1="9.017" y1="7.15441875" x2="9.69518125" y2="7.239" layer="21"/>
<rectangle x1="10.20318125" y1="7.15441875" x2="12.40281875" y2="7.239" layer="21"/>
<rectangle x1="12.91081875" y1="7.15441875" x2="13.335" y2="7.239" layer="21"/>
<rectangle x1="13.843" y1="7.15441875" x2="14.94281875" y2="7.239" layer="21"/>
<rectangle x1="15.367" y1="7.15441875" x2="15.875" y2="7.239" layer="21"/>
<rectangle x1="16.29918125" y1="7.15441875" x2="17.31518125" y2="7.239" layer="21"/>
<rectangle x1="17.82318125" y1="7.15441875" x2="19.939" y2="7.239" layer="21"/>
<rectangle x1="0.04318125" y1="7.239" x2="0.635" y2="7.32358125" layer="21"/>
<rectangle x1="2.413" y1="7.239" x2="3.00481875" y2="7.32358125" layer="21"/>
<rectangle x1="5.12318125" y1="7.239" x2="5.715" y2="7.32358125" layer="21"/>
<rectangle x1="7.747" y1="7.239" x2="8.33881875" y2="7.32358125" layer="21"/>
<rectangle x1="9.017" y1="7.239" x2="9.69518125" y2="7.32358125" layer="21"/>
<rectangle x1="10.20318125" y1="7.239" x2="12.40281875" y2="7.32358125" layer="21"/>
<rectangle x1="12.91081875" y1="7.239" x2="13.335" y2="7.32358125" layer="21"/>
<rectangle x1="13.92681875" y1="7.239" x2="14.859" y2="7.32358125" layer="21"/>
<rectangle x1="15.28318125" y1="7.239" x2="15.875" y2="7.32358125" layer="21"/>
<rectangle x1="16.383" y1="7.239" x2="17.22881875" y2="7.32358125" layer="21"/>
<rectangle x1="17.73681875" y1="7.239" x2="19.939" y2="7.32358125" layer="21"/>
<rectangle x1="0.04318125" y1="7.32358125" x2="0.635" y2="7.40841875" layer="21"/>
<rectangle x1="2.413" y1="7.32358125" x2="3.00481875" y2="7.40841875" layer="21"/>
<rectangle x1="5.12318125" y1="7.32358125" x2="5.715" y2="7.40841875" layer="21"/>
<rectangle x1="7.747" y1="7.32358125" x2="8.33881875" y2="7.40841875" layer="21"/>
<rectangle x1="9.017" y1="7.32358125" x2="9.69518125" y2="7.40841875" layer="21"/>
<rectangle x1="10.20318125" y1="7.32358125" x2="12.40281875" y2="7.40841875" layer="21"/>
<rectangle x1="12.91081875" y1="7.32358125" x2="13.335" y2="7.40841875" layer="21"/>
<rectangle x1="14.01318125" y1="7.32358125" x2="14.68881875" y2="7.40841875" layer="21"/>
<rectangle x1="15.28318125" y1="7.32358125" x2="15.95881875" y2="7.40841875" layer="21"/>
<rectangle x1="16.46681875" y1="7.32358125" x2="17.145" y2="7.40841875" layer="21"/>
<rectangle x1="17.73681875" y1="7.32358125" x2="19.939" y2="7.40841875" layer="21"/>
<rectangle x1="0.04318125" y1="7.40841875" x2="0.635" y2="7.493" layer="21"/>
<rectangle x1="2.413" y1="7.40841875" x2="3.00481875" y2="7.493" layer="21"/>
<rectangle x1="5.12318125" y1="7.40841875" x2="5.715" y2="7.493" layer="21"/>
<rectangle x1="7.747" y1="7.40841875" x2="8.33881875" y2="7.493" layer="21"/>
<rectangle x1="9.017" y1="7.40841875" x2="9.69518125" y2="7.493" layer="21"/>
<rectangle x1="10.20318125" y1="7.40841875" x2="12.40281875" y2="7.493" layer="21"/>
<rectangle x1="12.91081875" y1="7.40841875" x2="13.335" y2="7.493" layer="21"/>
<rectangle x1="14.26718125" y1="7.40841875" x2="14.43481875" y2="7.493" layer="21"/>
<rectangle x1="15.19681875" y1="7.40841875" x2="15.95881875" y2="7.493" layer="21"/>
<rectangle x1="16.72081875" y1="7.40841875" x2="16.891" y2="7.493" layer="21"/>
<rectangle x1="17.653" y1="7.40841875" x2="19.939" y2="7.493" layer="21"/>
<rectangle x1="0.04318125" y1="7.493" x2="0.635" y2="7.57758125" layer="21"/>
<rectangle x1="2.413" y1="7.493" x2="3.00481875" y2="7.57758125" layer="21"/>
<rectangle x1="5.12318125" y1="7.493" x2="5.715" y2="7.57758125" layer="21"/>
<rectangle x1="7.66318125" y1="7.493" x2="8.33881875" y2="7.57758125" layer="21"/>
<rectangle x1="9.017" y1="7.493" x2="9.69518125" y2="7.57758125" layer="21"/>
<rectangle x1="10.20318125" y1="7.493" x2="12.40281875" y2="7.57758125" layer="21"/>
<rectangle x1="12.91081875" y1="7.493" x2="13.335" y2="7.57758125" layer="21"/>
<rectangle x1="15.113" y1="7.493" x2="16.04518125" y2="7.57758125" layer="21"/>
<rectangle x1="17.56918125" y1="7.493" x2="19.939" y2="7.57758125" layer="21"/>
<rectangle x1="0.04318125" y1="7.57758125" x2="0.71881875" y2="7.66241875" layer="21"/>
<rectangle x1="2.413" y1="7.57758125" x2="3.00481875" y2="7.66241875" layer="21"/>
<rectangle x1="5.12318125" y1="7.57758125" x2="5.715" y2="7.66241875" layer="21"/>
<rectangle x1="7.66318125" y1="7.57758125" x2="8.33881875" y2="7.66241875" layer="21"/>
<rectangle x1="9.017" y1="7.57758125" x2="9.69518125" y2="7.66241875" layer="21"/>
<rectangle x1="10.20318125" y1="7.57758125" x2="12.40281875" y2="7.66241875" layer="21"/>
<rectangle x1="12.91081875" y1="7.57758125" x2="13.335" y2="7.66241875" layer="21"/>
<rectangle x1="15.02918125" y1="7.57758125" x2="16.21281875" y2="7.66241875" layer="21"/>
<rectangle x1="17.48281875" y1="7.57758125" x2="19.939" y2="7.66241875" layer="21"/>
<rectangle x1="0.04318125" y1="7.66241875" x2="0.71881875" y2="7.747" layer="21"/>
<rectangle x1="2.413" y1="7.66241875" x2="3.00481875" y2="7.747" layer="21"/>
<rectangle x1="5.12318125" y1="7.66241875" x2="5.715" y2="7.747" layer="21"/>
<rectangle x1="7.66318125" y1="7.66241875" x2="8.255" y2="7.747" layer="21"/>
<rectangle x1="9.017" y1="7.66241875" x2="9.779" y2="7.747" layer="21"/>
<rectangle x1="10.287" y1="7.66241875" x2="12.319" y2="7.747" layer="21"/>
<rectangle x1="12.827" y1="7.66241875" x2="13.335" y2="7.747" layer="21"/>
<rectangle x1="13.67281875" y1="7.66241875" x2="13.843" y2="7.747" layer="21"/>
<rectangle x1="14.859" y1="7.66241875" x2="16.29918125" y2="7.747" layer="21"/>
<rectangle x1="17.31518125" y1="7.66241875" x2="19.939" y2="7.747" layer="21"/>
<rectangle x1="0.127" y1="7.747" x2="0.71881875" y2="7.83158125" layer="21"/>
<rectangle x1="2.413" y1="7.747" x2="3.00481875" y2="7.83158125" layer="21"/>
<rectangle x1="5.12318125" y1="7.747" x2="5.715" y2="7.83158125" layer="21"/>
<rectangle x1="7.57681875" y1="7.747" x2="8.255" y2="7.83158125" layer="21"/>
<rectangle x1="9.017" y1="7.747" x2="9.779" y2="7.83158125" layer="21"/>
<rectangle x1="10.287" y1="7.747" x2="12.319" y2="7.83158125" layer="21"/>
<rectangle x1="12.827" y1="7.747" x2="13.335" y2="7.83158125" layer="21"/>
<rectangle x1="13.589" y1="7.747" x2="14.097" y2="7.83158125" layer="21"/>
<rectangle x1="14.68881875" y1="7.747" x2="16.55318125" y2="7.83158125" layer="21"/>
<rectangle x1="17.06118125" y1="7.747" x2="19.939" y2="7.83158125" layer="21"/>
<rectangle x1="0.127" y1="7.83158125" x2="0.71881875" y2="7.91641875" layer="21"/>
<rectangle x1="2.413" y1="7.83158125" x2="3.00481875" y2="7.91641875" layer="21"/>
<rectangle x1="5.12318125" y1="7.83158125" x2="5.715" y2="7.91641875" layer="21"/>
<rectangle x1="7.57681875" y1="7.83158125" x2="8.255" y2="7.91641875" layer="21"/>
<rectangle x1="9.017" y1="7.83158125" x2="9.779" y2="7.91641875" layer="21"/>
<rectangle x1="10.37081875" y1="7.83158125" x2="12.23518125" y2="7.91641875" layer="21"/>
<rectangle x1="12.827" y1="7.83158125" x2="19.939" y2="7.91641875" layer="21"/>
<rectangle x1="0.127" y1="7.91641875" x2="0.71881875" y2="8.001" layer="21"/>
<rectangle x1="2.413" y1="7.91641875" x2="3.00481875" y2="8.001" layer="21"/>
<rectangle x1="5.12318125" y1="7.91641875" x2="5.715" y2="8.001" layer="21"/>
<rectangle x1="7.493" y1="7.91641875" x2="8.17118125" y2="8.001" layer="21"/>
<rectangle x1="9.017" y1="7.91641875" x2="9.86281875" y2="8.001" layer="21"/>
<rectangle x1="10.45718125" y1="7.91641875" x2="12.14881875" y2="8.001" layer="21"/>
<rectangle x1="12.74318125" y1="7.91641875" x2="19.939" y2="8.001" layer="21"/>
<rectangle x1="0.127" y1="8.001" x2="0.71881875" y2="8.08558125" layer="21"/>
<rectangle x1="2.413" y1="8.001" x2="3.00481875" y2="8.08558125" layer="21"/>
<rectangle x1="5.12318125" y1="8.001" x2="5.715" y2="8.08558125" layer="21"/>
<rectangle x1="7.40918125" y1="8.001" x2="8.17118125" y2="8.08558125" layer="21"/>
<rectangle x1="9.017" y1="8.001" x2="9.86281875" y2="8.08558125" layer="21"/>
<rectangle x1="10.45718125" y1="8.001" x2="12.065" y2="8.08558125" layer="21"/>
<rectangle x1="12.74318125" y1="8.001" x2="19.939" y2="8.08558125" layer="21"/>
<rectangle x1="0.127" y1="8.08558125" x2="0.80518125" y2="8.17041875" layer="21"/>
<rectangle x1="2.413" y1="8.08558125" x2="3.00481875" y2="8.17041875" layer="21"/>
<rectangle x1="5.12318125" y1="8.08558125" x2="5.715" y2="8.17041875" layer="21"/>
<rectangle x1="7.32281875" y1="8.08558125" x2="8.08481875" y2="8.17041875" layer="21"/>
<rectangle x1="9.017" y1="8.08558125" x2="9.94918125" y2="8.17041875" layer="21"/>
<rectangle x1="10.62481875" y1="8.08558125" x2="11.98118125" y2="8.17041875" layer="21"/>
<rectangle x1="12.65681875" y1="8.08558125" x2="19.939" y2="8.17041875" layer="21"/>
<rectangle x1="0.127" y1="8.17041875" x2="0.80518125" y2="8.255" layer="21"/>
<rectangle x1="2.413" y1="8.17041875" x2="3.00481875" y2="8.255" layer="21"/>
<rectangle x1="5.12318125" y1="8.17041875" x2="5.715" y2="8.255" layer="21"/>
<rectangle x1="7.15518125" y1="8.17041875" x2="8.001" y2="8.255" layer="21"/>
<rectangle x1="9.017" y1="8.17041875" x2="9.94918125" y2="8.255" layer="21"/>
<rectangle x1="10.71118125" y1="8.17041875" x2="11.89481875" y2="8.255" layer="21"/>
<rectangle x1="12.573" y1="8.17041875" x2="19.939" y2="8.255" layer="21"/>
<rectangle x1="0.21081875" y1="8.255" x2="0.80518125" y2="8.33958125" layer="21"/>
<rectangle x1="2.413" y1="8.255" x2="3.00481875" y2="8.33958125" layer="21"/>
<rectangle x1="5.12318125" y1="8.255" x2="5.715" y2="8.33958125" layer="21"/>
<rectangle x1="6.90118125" y1="8.255" x2="8.001" y2="8.33958125" layer="21"/>
<rectangle x1="9.017" y1="8.255" x2="10.033" y2="8.33958125" layer="21"/>
<rectangle x1="10.87881875" y1="8.255" x2="11.72718125" y2="8.33958125" layer="21"/>
<rectangle x1="12.573" y1="8.255" x2="19.939" y2="8.33958125" layer="21"/>
<rectangle x1="0.21081875" y1="8.33958125" x2="0.889" y2="8.42441875" layer="21"/>
<rectangle x1="2.413" y1="8.33958125" x2="3.00481875" y2="8.42441875" layer="21"/>
<rectangle x1="5.12318125" y1="8.33958125" x2="7.91718125" y2="8.42441875" layer="21"/>
<rectangle x1="9.10081875" y1="8.33958125" x2="10.11681875" y2="8.42441875" layer="21"/>
<rectangle x1="11.21918125" y1="8.33958125" x2="11.38681875" y2="8.42441875" layer="21"/>
<rectangle x1="12.48918125" y1="8.33958125" x2="19.85518125" y2="8.42441875" layer="21"/>
<rectangle x1="0.21081875" y1="8.42441875" x2="0.889" y2="8.509" layer="21"/>
<rectangle x1="2.413" y1="8.42441875" x2="3.00481875" y2="8.509" layer="21"/>
<rectangle x1="5.12318125" y1="8.42441875" x2="7.83081875" y2="8.509" layer="21"/>
<rectangle x1="9.10081875" y1="8.42441875" x2="10.20318125" y2="8.509" layer="21"/>
<rectangle x1="12.40281875" y1="8.42441875" x2="19.85518125" y2="8.509" layer="21"/>
<rectangle x1="0.29718125" y1="8.509" x2="0.889" y2="8.59358125" layer="21"/>
<rectangle x1="2.413" y1="8.509" x2="3.00481875" y2="8.59358125" layer="21"/>
<rectangle x1="5.12318125" y1="8.509" x2="7.747" y2="8.59358125" layer="21"/>
<rectangle x1="9.10081875" y1="8.509" x2="10.287" y2="8.59358125" layer="21"/>
<rectangle x1="12.23518125" y1="8.509" x2="19.85518125" y2="8.59358125" layer="21"/>
<rectangle x1="0.29718125" y1="8.59358125" x2="0.97281875" y2="8.67841875" layer="21"/>
<rectangle x1="2.413" y1="8.59358125" x2="3.00481875" y2="8.67841875" layer="21"/>
<rectangle x1="5.12318125" y1="8.59358125" x2="7.57681875" y2="8.67841875" layer="21"/>
<rectangle x1="9.10081875" y1="8.59358125" x2="10.45718125" y2="8.67841875" layer="21"/>
<rectangle x1="12.14881875" y1="8.59358125" x2="19.76881875" y2="8.67841875" layer="21"/>
<rectangle x1="0.29718125" y1="8.67841875" x2="0.97281875" y2="8.763" layer="21"/>
<rectangle x1="2.413" y1="8.67841875" x2="3.00481875" y2="8.763" layer="21"/>
<rectangle x1="5.12318125" y1="8.67841875" x2="7.493" y2="8.763" layer="21"/>
<rectangle x1="9.10081875" y1="8.67841875" x2="10.62481875" y2="8.763" layer="21"/>
<rectangle x1="11.98118125" y1="8.67841875" x2="19.76881875" y2="8.763" layer="21"/>
<rectangle x1="0.381" y1="8.763" x2="1.05918125" y2="8.84758125" layer="21"/>
<rectangle x1="2.413" y1="8.763" x2="2.921" y2="8.84758125" layer="21"/>
<rectangle x1="5.207" y1="8.763" x2="7.32281875" y2="8.84758125" layer="21"/>
<rectangle x1="9.10081875" y1="8.763" x2="10.795" y2="8.84758125" layer="21"/>
<rectangle x1="11.72718125" y1="8.763" x2="19.76881875" y2="8.84758125" layer="21"/>
<rectangle x1="0.381" y1="8.84758125" x2="1.05918125" y2="8.93241875" layer="21"/>
<rectangle x1="2.58318125" y1="8.84758125" x2="2.83718125" y2="8.93241875" layer="21"/>
<rectangle x1="5.29081875" y1="8.84758125" x2="7.06881875" y2="8.93241875" layer="21"/>
<rectangle x1="9.18718125" y1="8.84758125" x2="19.685" y2="8.93241875" layer="21"/>
<rectangle x1="0.46481875" y1="8.93241875" x2="1.143" y2="9.017" layer="21"/>
<rectangle x1="9.18718125" y1="8.93241875" x2="19.685" y2="9.017" layer="21"/>
<rectangle x1="0.46481875" y1="9.017" x2="1.143" y2="9.10158125" layer="21"/>
<rectangle x1="9.18718125" y1="9.017" x2="19.60118125" y2="9.10158125" layer="21"/>
<rectangle x1="0.55118125" y1="9.10158125" x2="1.22681875" y2="9.18641875" layer="21"/>
<rectangle x1="9.18718125" y1="9.10158125" x2="19.60118125" y2="9.18641875" layer="21"/>
<rectangle x1="0.55118125" y1="9.18641875" x2="1.31318125" y2="9.271" layer="21"/>
<rectangle x1="9.18718125" y1="9.18641875" x2="19.51481875" y2="9.271" layer="21"/>
<rectangle x1="0.635" y1="9.271" x2="1.397" y2="9.35558125" layer="21"/>
<rectangle x1="9.271" y1="9.271" x2="19.51481875" y2="9.35558125" layer="21"/>
<rectangle x1="0.71881875" y1="9.35558125" x2="1.397" y2="9.44041875" layer="21"/>
<rectangle x1="9.271" y1="9.35558125" x2="19.431" y2="9.44041875" layer="21"/>
<rectangle x1="0.71881875" y1="9.44041875" x2="1.48081875" y2="9.525" layer="21"/>
<rectangle x1="9.271" y1="9.44041875" x2="19.34718125" y2="9.525" layer="21"/>
<rectangle x1="0.80518125" y1="9.525" x2="1.56718125" y2="9.60958125" layer="21"/>
<rectangle x1="9.35481875" y1="9.525" x2="19.34718125" y2="9.60958125" layer="21"/>
<rectangle x1="0.889" y1="9.60958125" x2="1.651" y2="9.69441875" layer="21"/>
<rectangle x1="9.35481875" y1="9.60958125" x2="19.26081875" y2="9.69441875" layer="21"/>
<rectangle x1="0.889" y1="9.69441875" x2="1.73481875" y2="9.779" layer="21"/>
<rectangle x1="9.35481875" y1="9.69441875" x2="19.177" y2="9.779" layer="21"/>
<rectangle x1="0.97281875" y1="9.779" x2="1.82118125" y2="9.86358125" layer="21"/>
<rectangle x1="9.44118125" y1="9.779" x2="19.09318125" y2="9.86358125" layer="21"/>
<rectangle x1="1.05918125" y1="9.86358125" x2="1.905" y2="9.94841875" layer="21"/>
<rectangle x1="9.44118125" y1="9.86358125" x2="19.00681875" y2="9.94841875" layer="21"/>
<rectangle x1="1.143" y1="9.94841875" x2="2.07518125" y2="10.033" layer="21"/>
<rectangle x1="9.44118125" y1="9.94841875" x2="18.923" y2="10.033" layer="21"/>
<rectangle x1="1.22681875" y1="10.033" x2="2.159" y2="10.11758125" layer="21"/>
<rectangle x1="9.525" y1="10.033" x2="18.923" y2="10.11758125" layer="21"/>
<rectangle x1="1.31318125" y1="10.11758125" x2="2.24281875" y2="10.20241875" layer="21"/>
<rectangle x1="9.525" y1="10.11758125" x2="18.75281875" y2="10.20241875" layer="21"/>
<rectangle x1="1.397" y1="10.20241875" x2="2.413" y2="10.287" layer="21"/>
<rectangle x1="9.60881875" y1="10.20241875" x2="18.669" y2="10.287" layer="21"/>
<rectangle x1="1.48081875" y1="10.287" x2="2.58318125" y2="10.37158125" layer="21"/>
<rectangle x1="9.60881875" y1="10.287" x2="18.58518125" y2="10.37158125" layer="21"/>
<rectangle x1="1.56718125" y1="10.37158125" x2="2.75081875" y2="10.45641875" layer="21"/>
<rectangle x1="9.69518125" y1="10.37158125" x2="18.49881875" y2="10.45641875" layer="21"/>
<rectangle x1="1.73481875" y1="10.45641875" x2="3.00481875" y2="10.541" layer="21"/>
<rectangle x1="9.69518125" y1="10.45641875" x2="18.415" y2="10.541" layer="21"/>
<rectangle x1="1.82118125" y1="10.541" x2="3.34518125" y2="10.62558125" layer="21"/>
<rectangle x1="9.779" y1="10.541" x2="18.24481875" y2="10.62558125" layer="21"/>
<rectangle x1="1.98881875" y1="10.62558125" x2="9.69518125" y2="10.71041875" layer="21"/>
<rectangle x1="9.779" y1="10.62558125" x2="18.161" y2="10.71041875" layer="21"/>
<rectangle x1="2.07518125" y1="10.71041875" x2="17.99081875" y2="10.795" layer="21"/>
<rectangle x1="2.24281875" y1="10.795" x2="17.82318125" y2="10.87958125" layer="21"/>
<rectangle x1="2.413" y1="10.87958125" x2="17.653" y2="10.96441875" layer="21"/>
<rectangle x1="2.58318125" y1="10.96441875" x2="17.48281875" y2="11.049" layer="21"/>
<rectangle x1="2.83718125" y1="11.049" x2="17.22881875" y2="11.13358125" layer="21"/>
<rectangle x1="3.175" y1="11.13358125" x2="16.891" y2="11.21841875" layer="21"/>
</package>
<package name="UDO-LOGO-30MM">
<rectangle x1="4.36118125" y1="0.55041875" x2="5.29081875" y2="0.635" layer="21"/>
<rectangle x1="21.29281875" y1="0.55041875" x2="22.30881875" y2="0.635" layer="21"/>
<rectangle x1="4.191" y1="0.635" x2="5.54481875" y2="0.71958125" layer="21"/>
<rectangle x1="6.731" y1="0.635" x2="6.81481875" y2="0.71958125" layer="21"/>
<rectangle x1="8.42518125" y1="0.635" x2="8.509" y2="0.71958125" layer="21"/>
<rectangle x1="8.93318125" y1="0.635" x2="9.10081875" y2="0.71958125" layer="21"/>
<rectangle x1="10.541" y1="0.635" x2="10.795" y2="0.71958125" layer="21"/>
<rectangle x1="11.38681875" y1="0.635" x2="12.65681875" y2="0.71958125" layer="21"/>
<rectangle x1="14.859" y1="0.635" x2="15.28318125" y2="0.71958125" layer="21"/>
<rectangle x1="16.80718125" y1="0.635" x2="17.145" y2="0.71958125" layer="21"/>
<rectangle x1="18.669" y1="0.635" x2="19.09318125" y2="0.71958125" layer="21"/>
<rectangle x1="21.12518125" y1="0.635" x2="22.479" y2="0.71958125" layer="21"/>
<rectangle x1="23.495" y1="0.635" x2="23.66518125" y2="0.71958125" layer="21"/>
<rectangle x1="25.273" y1="0.635" x2="25.35681875" y2="0.71958125" layer="21"/>
<rectangle x1="26.45918125" y1="0.635" x2="26.797" y2="0.71958125" layer="21"/>
<rectangle x1="4.10718125" y1="0.71958125" x2="5.715" y2="0.80441875" layer="21"/>
<rectangle x1="6.64718125" y1="0.71958125" x2="6.90118125" y2="0.80441875" layer="21"/>
<rectangle x1="8.33881875" y1="0.71958125" x2="8.59281875" y2="0.80441875" layer="21"/>
<rectangle x1="8.84681875" y1="0.71958125" x2="9.18718125" y2="0.80441875" layer="21"/>
<rectangle x1="10.287" y1="0.71958125" x2="10.87881875" y2="0.80441875" layer="21"/>
<rectangle x1="11.303" y1="0.71958125" x2="12.91081875" y2="0.80441875" layer="21"/>
<rectangle x1="14.605" y1="0.71958125" x2="15.53718125" y2="0.80441875" layer="21"/>
<rectangle x1="16.637" y1="0.71958125" x2="17.399" y2="0.80441875" layer="21"/>
<rectangle x1="18.415" y1="0.71958125" x2="19.34718125" y2="0.80441875" layer="21"/>
<rectangle x1="20.955" y1="0.71958125" x2="22.64918125" y2="0.80441875" layer="21"/>
<rectangle x1="23.495" y1="0.71958125" x2="23.749" y2="0.80441875" layer="21"/>
<rectangle x1="25.18918125" y1="0.71958125" x2="25.44318125" y2="0.80441875" layer="21"/>
<rectangle x1="26.20518125" y1="0.71958125" x2="27.13481875" y2="0.80441875" layer="21"/>
<rectangle x1="3.937" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="5.12318125" y1="0.80441875" x2="5.79881875" y2="0.889" layer="21"/>
<rectangle x1="6.64718125" y1="0.80441875" x2="6.90118125" y2="0.889" layer="21"/>
<rectangle x1="8.33881875" y1="0.80441875" x2="8.59281875" y2="0.889" layer="21"/>
<rectangle x1="8.84681875" y1="0.80441875" x2="9.18718125" y2="0.889" layer="21"/>
<rectangle x1="10.11681875" y1="0.80441875" x2="10.87881875" y2="0.889" layer="21"/>
<rectangle x1="11.303" y1="0.80441875" x2="13.081" y2="0.889" layer="21"/>
<rectangle x1="14.52118125" y1="0.80441875" x2="15.621" y2="0.889" layer="21"/>
<rectangle x1="16.46681875" y1="0.80441875" x2="17.48281875" y2="0.889" layer="21"/>
<rectangle x1="18.33118125" y1="0.80441875" x2="19.431" y2="0.889" layer="21"/>
<rectangle x1="20.87118125" y1="0.80441875" x2="21.54681875" y2="0.889" layer="21"/>
<rectangle x1="22.05481875" y1="0.80441875" x2="22.733" y2="0.889" layer="21"/>
<rectangle x1="23.495" y1="0.80441875" x2="23.749" y2="0.889" layer="21"/>
<rectangle x1="25.18918125" y1="0.80441875" x2="25.44318125" y2="0.889" layer="21"/>
<rectangle x1="26.035" y1="0.80441875" x2="27.22118125" y2="0.889" layer="21"/>
<rectangle x1="3.85318125" y1="0.889" x2="4.36118125" y2="0.97358125" layer="21"/>
<rectangle x1="5.37718125" y1="0.889" x2="5.88518125" y2="0.97358125" layer="21"/>
<rectangle x1="6.64718125" y1="0.889" x2="6.90118125" y2="0.97358125" layer="21"/>
<rectangle x1="8.33881875" y1="0.889" x2="8.59281875" y2="0.97358125" layer="21"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.18718125" y2="0.97358125" layer="21"/>
<rectangle x1="10.033" y1="0.889" x2="10.795" y2="0.97358125" layer="21"/>
<rectangle x1="11.303" y1="0.889" x2="13.25118125" y2="0.97358125" layer="21"/>
<rectangle x1="14.351" y1="0.889" x2="15.79118125" y2="0.97358125" layer="21"/>
<rectangle x1="16.383" y1="0.889" x2="17.56918125" y2="0.97358125" layer="21"/>
<rectangle x1="18.24481875" y1="0.889" x2="19.60118125" y2="0.97358125" layer="21"/>
<rectangle x1="20.78481875" y1="0.889" x2="21.29281875" y2="0.97358125" layer="21"/>
<rectangle x1="22.30881875" y1="0.889" x2="22.81681875" y2="0.97358125" layer="21"/>
<rectangle x1="23.495" y1="0.889" x2="23.749" y2="0.97358125" layer="21"/>
<rectangle x1="25.18918125" y1="0.889" x2="25.44318125" y2="0.97358125" layer="21"/>
<rectangle x1="25.95118125" y1="0.889" x2="27.305" y2="0.97358125" layer="21"/>
<rectangle x1="3.76681875" y1="0.97358125" x2="4.191" y2="1.05841875" layer="21"/>
<rectangle x1="5.54481875" y1="0.97358125" x2="5.969" y2="1.05841875" layer="21"/>
<rectangle x1="6.64718125" y1="0.97358125" x2="6.90118125" y2="1.05841875" layer="21"/>
<rectangle x1="8.33881875" y1="0.97358125" x2="8.59281875" y2="1.05841875" layer="21"/>
<rectangle x1="8.84681875" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="21"/>
<rectangle x1="9.94918125" y1="0.97358125" x2="10.45718125" y2="1.05841875" layer="21"/>
<rectangle x1="11.303" y1="0.97358125" x2="11.557" y2="1.05841875" layer="21"/>
<rectangle x1="12.827" y1="0.97358125" x2="13.335" y2="1.05841875" layer="21"/>
<rectangle x1="14.351" y1="0.97358125" x2="14.859" y2="1.05841875" layer="21"/>
<rectangle x1="15.367" y1="0.97358125" x2="15.875" y2="1.05841875" layer="21"/>
<rectangle x1="16.383" y1="0.97358125" x2="16.80718125" y2="1.05841875" layer="21"/>
<rectangle x1="17.22881875" y1="0.97358125" x2="17.653" y2="1.05841875" layer="21"/>
<rectangle x1="18.161" y1="0.97358125" x2="18.58518125" y2="1.05841875" layer="21"/>
<rectangle x1="19.177" y1="0.97358125" x2="19.685" y2="1.05841875" layer="21"/>
<rectangle x1="20.701" y1="0.97358125" x2="21.12518125" y2="1.05841875" layer="21"/>
<rectangle x1="22.479" y1="0.97358125" x2="22.90318125" y2="1.05841875" layer="21"/>
<rectangle x1="23.495" y1="0.97358125" x2="23.749" y2="1.05841875" layer="21"/>
<rectangle x1="25.18918125" y1="0.97358125" x2="25.44318125" y2="1.05841875" layer="21"/>
<rectangle x1="25.86481875" y1="0.97358125" x2="26.37281875" y2="1.05841875" layer="21"/>
<rectangle x1="26.96718125" y1="0.97358125" x2="27.38881875" y2="1.05841875" layer="21"/>
<rectangle x1="3.683" y1="1.05841875" x2="4.10718125" y2="1.143" layer="21"/>
<rectangle x1="5.63118125" y1="1.05841875" x2="6.05281875" y2="1.143" layer="21"/>
<rectangle x1="6.64718125" y1="1.05841875" x2="6.90118125" y2="1.143" layer="21"/>
<rectangle x1="8.33881875" y1="1.05841875" x2="8.59281875" y2="1.143" layer="21"/>
<rectangle x1="8.84681875" y1="1.05841875" x2="9.18718125" y2="1.143" layer="21"/>
<rectangle x1="9.86281875" y1="1.05841875" x2="10.287" y2="1.143" layer="21"/>
<rectangle x1="11.303" y1="1.05841875" x2="11.557" y2="1.143" layer="21"/>
<rectangle x1="12.99718125" y1="1.05841875" x2="13.41881875" y2="1.143" layer="21"/>
<rectangle x1="14.26718125" y1="1.05841875" x2="14.68881875" y2="1.143" layer="21"/>
<rectangle x1="15.53718125" y1="1.05841875" x2="15.875" y2="1.143" layer="21"/>
<rectangle x1="16.29918125" y1="1.05841875" x2="16.637" y2="1.143" layer="21"/>
<rectangle x1="17.399" y1="1.05841875" x2="17.653" y2="1.143" layer="21"/>
<rectangle x1="18.07718125" y1="1.05841875" x2="18.49881875" y2="1.143" layer="21"/>
<rectangle x1="19.34718125" y1="1.05841875" x2="19.685" y2="1.143" layer="21"/>
<rectangle x1="20.61718125" y1="1.05841875" x2="21.03881875" y2="1.143" layer="21"/>
<rectangle x1="22.56281875" y1="1.05841875" x2="22.987" y2="1.143" layer="21"/>
<rectangle x1="23.495" y1="1.05841875" x2="23.749" y2="1.143" layer="21"/>
<rectangle x1="25.18918125" y1="1.05841875" x2="25.44318125" y2="1.143" layer="21"/>
<rectangle x1="25.86481875" y1="1.05841875" x2="26.20518125" y2="1.143" layer="21"/>
<rectangle x1="27.051" y1="1.05841875" x2="27.47518125" y2="1.143" layer="21"/>
<rectangle x1="3.683" y1="1.143" x2="4.02081875" y2="1.22758125" layer="21"/>
<rectangle x1="5.715" y1="1.143" x2="6.13918125" y2="1.22758125" layer="21"/>
<rectangle x1="6.64718125" y1="1.143" x2="6.90118125" y2="1.22758125" layer="21"/>
<rectangle x1="8.33881875" y1="1.143" x2="8.59281875" y2="1.22758125" layer="21"/>
<rectangle x1="8.84681875" y1="1.143" x2="9.18718125" y2="1.22758125" layer="21"/>
<rectangle x1="9.779" y1="1.143" x2="10.20318125" y2="1.22758125" layer="21"/>
<rectangle x1="11.303" y1="1.143" x2="11.557" y2="1.22758125" layer="21"/>
<rectangle x1="13.081" y1="1.143" x2="13.50518125" y2="1.22758125" layer="21"/>
<rectangle x1="14.18081875" y1="1.143" x2="14.52118125" y2="1.22758125" layer="21"/>
<rectangle x1="15.621" y1="1.143" x2="15.95881875" y2="1.22758125" layer="21"/>
<rectangle x1="16.383" y1="1.143" x2="16.637" y2="1.22758125" layer="21"/>
<rectangle x1="17.399" y1="1.143" x2="17.73681875" y2="1.22758125" layer="21"/>
<rectangle x1="17.99081875" y1="1.143" x2="18.33118125" y2="1.22758125" layer="21"/>
<rectangle x1="19.431" y1="1.143" x2="19.76881875" y2="1.22758125" layer="21"/>
<rectangle x1="20.61718125" y1="1.143" x2="20.955" y2="1.22758125" layer="21"/>
<rectangle x1="22.64918125" y1="1.143" x2="23.07081875" y2="1.22758125" layer="21"/>
<rectangle x1="23.495" y1="1.143" x2="23.749" y2="1.22758125" layer="21"/>
<rectangle x1="25.18918125" y1="1.143" x2="25.44318125" y2="1.22758125" layer="21"/>
<rectangle x1="25.781" y1="1.143" x2="26.11881875" y2="1.22758125" layer="21"/>
<rectangle x1="27.22118125" y1="1.143" x2="27.559" y2="1.22758125" layer="21"/>
<rectangle x1="3.59918125" y1="1.22758125" x2="3.937" y2="1.31241875" layer="21"/>
<rectangle x1="5.79881875" y1="1.22758125" x2="6.13918125" y2="1.31241875" layer="21"/>
<rectangle x1="6.64718125" y1="1.22758125" x2="6.90118125" y2="1.31241875" layer="21"/>
<rectangle x1="8.33881875" y1="1.22758125" x2="8.59281875" y2="1.31241875" layer="21"/>
<rectangle x1="8.84681875" y1="1.22758125" x2="9.18718125" y2="1.31241875" layer="21"/>
<rectangle x1="9.779" y1="1.22758125" x2="10.11681875" y2="1.31241875" layer="21"/>
<rectangle x1="11.303" y1="1.22758125" x2="11.557" y2="1.31241875" layer="21"/>
<rectangle x1="13.25118125" y1="1.22758125" x2="13.589" y2="1.31241875" layer="21"/>
<rectangle x1="14.18081875" y1="1.22758125" x2="14.52118125" y2="1.31241875" layer="21"/>
<rectangle x1="15.70481875" y1="1.22758125" x2="15.95881875" y2="1.31241875" layer="21"/>
<rectangle x1="17.399" y1="1.22758125" x2="17.653" y2="1.31241875" layer="21"/>
<rectangle x1="17.99081875" y1="1.22758125" x2="18.33118125" y2="1.31241875" layer="21"/>
<rectangle x1="19.51481875" y1="1.22758125" x2="19.76881875" y2="1.31241875" layer="21"/>
<rectangle x1="20.53081875" y1="1.22758125" x2="20.87118125" y2="1.31241875" layer="21"/>
<rectangle x1="22.733" y1="1.22758125" x2="23.07081875" y2="1.31241875" layer="21"/>
<rectangle x1="23.495" y1="1.22758125" x2="23.749" y2="1.31241875" layer="21"/>
<rectangle x1="25.18918125" y1="1.22758125" x2="25.44318125" y2="1.31241875" layer="21"/>
<rectangle x1="25.69718125" y1="1.22758125" x2="26.035" y2="1.31241875" layer="21"/>
<rectangle x1="27.22118125" y1="1.22758125" x2="27.559" y2="1.31241875" layer="21"/>
<rectangle x1="3.51281875" y1="1.31241875" x2="3.85318125" y2="1.397" layer="21"/>
<rectangle x1="5.88518125" y1="1.31241875" x2="6.223" y2="1.397" layer="21"/>
<rectangle x1="6.64718125" y1="1.31241875" x2="6.90118125" y2="1.397" layer="21"/>
<rectangle x1="8.33881875" y1="1.31241875" x2="8.59281875" y2="1.397" layer="21"/>
<rectangle x1="8.84681875" y1="1.31241875" x2="9.18718125" y2="1.397" layer="21"/>
<rectangle x1="9.779" y1="1.31241875" x2="10.033" y2="1.397" layer="21"/>
<rectangle x1="11.303" y1="1.31241875" x2="11.557" y2="1.397" layer="21"/>
<rectangle x1="13.335" y1="1.31241875" x2="13.67281875" y2="1.397" layer="21"/>
<rectangle x1="14.097" y1="1.31241875" x2="14.43481875" y2="1.397" layer="21"/>
<rectangle x1="15.70481875" y1="1.31241875" x2="16.04518125" y2="1.397" layer="21"/>
<rectangle x1="17.31518125" y1="1.31241875" x2="17.653" y2="1.397" layer="21"/>
<rectangle x1="17.907" y1="1.31241875" x2="18.24481875" y2="1.397" layer="21"/>
<rectangle x1="19.51481875" y1="1.31241875" x2="19.76881875" y2="1.397" layer="21"/>
<rectangle x1="20.447" y1="1.31241875" x2="20.78481875" y2="1.397" layer="21"/>
<rectangle x1="22.81681875" y1="1.31241875" x2="23.15718125" y2="1.397" layer="21"/>
<rectangle x1="23.495" y1="1.31241875" x2="23.749" y2="1.397" layer="21"/>
<rectangle x1="25.18918125" y1="1.31241875" x2="25.44318125" y2="1.397" layer="21"/>
<rectangle x1="25.69718125" y1="1.31241875" x2="26.035" y2="1.397" layer="21"/>
<rectangle x1="27.305" y1="1.31241875" x2="27.47518125" y2="1.397" layer="21"/>
<rectangle x1="3.51281875" y1="1.397" x2="3.85318125" y2="1.48158125" layer="21"/>
<rectangle x1="5.88518125" y1="1.397" x2="6.223" y2="1.48158125" layer="21"/>
<rectangle x1="6.64718125" y1="1.397" x2="6.90118125" y2="1.48158125" layer="21"/>
<rectangle x1="8.33881875" y1="1.397" x2="8.59281875" y2="1.48158125" layer="21"/>
<rectangle x1="8.84681875" y1="1.397" x2="9.18718125" y2="1.48158125" layer="21"/>
<rectangle x1="9.69518125" y1="1.397" x2="10.033" y2="1.48158125" layer="21"/>
<rectangle x1="11.303" y1="1.397" x2="11.557" y2="1.48158125" layer="21"/>
<rectangle x1="13.335" y1="1.397" x2="13.67281875" y2="1.48158125" layer="21"/>
<rectangle x1="14.097" y1="1.397" x2="14.43481875" y2="1.48158125" layer="21"/>
<rectangle x1="15.79118125" y1="1.397" x2="16.04518125" y2="1.48158125" layer="21"/>
<rectangle x1="17.22881875" y1="1.397" x2="17.653" y2="1.48158125" layer="21"/>
<rectangle x1="17.907" y1="1.397" x2="18.24481875" y2="1.48158125" layer="21"/>
<rectangle x1="20.447" y1="1.397" x2="20.78481875" y2="1.48158125" layer="21"/>
<rectangle x1="22.81681875" y1="1.397" x2="23.15718125" y2="1.48158125" layer="21"/>
<rectangle x1="23.495" y1="1.397" x2="23.749" y2="1.48158125" layer="21"/>
<rectangle x1="25.18918125" y1="1.397" x2="25.44318125" y2="1.48158125" layer="21"/>
<rectangle x1="25.69718125" y1="1.397" x2="25.95118125" y2="1.48158125" layer="21"/>
<rectangle x1="3.51281875" y1="1.48158125" x2="3.76681875" y2="1.56641875" layer="21"/>
<rectangle x1="5.969" y1="1.48158125" x2="6.30681875" y2="1.56641875" layer="21"/>
<rectangle x1="6.64718125" y1="1.48158125" x2="6.90118125" y2="1.56641875" layer="21"/>
<rectangle x1="8.33881875" y1="1.48158125" x2="8.59281875" y2="1.56641875" layer="21"/>
<rectangle x1="8.84681875" y1="1.48158125" x2="9.18718125" y2="1.56641875" layer="21"/>
<rectangle x1="9.69518125" y1="1.48158125" x2="10.033" y2="1.56641875" layer="21"/>
<rectangle x1="11.303" y1="1.48158125" x2="11.557" y2="1.56641875" layer="21"/>
<rectangle x1="13.41881875" y1="1.48158125" x2="13.75918125" y2="1.56641875" layer="21"/>
<rectangle x1="14.097" y1="1.48158125" x2="14.351" y2="1.56641875" layer="21"/>
<rectangle x1="15.79118125" y1="1.48158125" x2="16.04518125" y2="1.56641875" layer="21"/>
<rectangle x1="16.97481875" y1="1.48158125" x2="17.56918125" y2="1.56641875" layer="21"/>
<rectangle x1="17.907" y1="1.48158125" x2="19.76881875" y2="1.56641875" layer="21"/>
<rectangle x1="20.447" y1="1.48158125" x2="20.701" y2="1.56641875" layer="21"/>
<rectangle x1="22.90318125" y1="1.48158125" x2="23.241" y2="1.56641875" layer="21"/>
<rectangle x1="23.495" y1="1.48158125" x2="23.749" y2="1.56641875" layer="21"/>
<rectangle x1="25.18918125" y1="1.48158125" x2="25.44318125" y2="1.56641875" layer="21"/>
<rectangle x1="25.69718125" y1="1.48158125" x2="27.559" y2="1.56641875" layer="21"/>
<rectangle x1="3.429" y1="1.56641875" x2="3.76681875" y2="1.651" layer="21"/>
<rectangle x1="5.969" y1="1.56641875" x2="6.30681875" y2="1.651" layer="21"/>
<rectangle x1="6.64718125" y1="1.56641875" x2="6.90118125" y2="1.651" layer="21"/>
<rectangle x1="8.33881875" y1="1.56641875" x2="8.59281875" y2="1.651" layer="21"/>
<rectangle x1="8.84681875" y1="1.56641875" x2="9.18718125" y2="1.651" layer="21"/>
<rectangle x1="9.69518125" y1="1.56641875" x2="10.033" y2="1.651" layer="21"/>
<rectangle x1="11.303" y1="1.56641875" x2="11.557" y2="1.651" layer="21"/>
<rectangle x1="13.50518125" y1="1.56641875" x2="13.75918125" y2="1.651" layer="21"/>
<rectangle x1="14.097" y1="1.56641875" x2="14.351" y2="1.651" layer="21"/>
<rectangle x1="15.79118125" y1="1.56641875" x2="16.04518125" y2="1.651" layer="21"/>
<rectangle x1="16.72081875" y1="1.56641875" x2="17.48281875" y2="1.651" layer="21"/>
<rectangle x1="17.907" y1="1.56641875" x2="19.85518125" y2="1.651" layer="21"/>
<rectangle x1="20.36318125" y1="1.56641875" x2="20.701" y2="1.651" layer="21"/>
<rectangle x1="22.90318125" y1="1.56641875" x2="23.241" y2="1.651" layer="21"/>
<rectangle x1="23.495" y1="1.56641875" x2="23.749" y2="1.651" layer="21"/>
<rectangle x1="25.18918125" y1="1.56641875" x2="25.44318125" y2="1.651" layer="21"/>
<rectangle x1="25.61081875" y1="1.56641875" x2="27.64281875" y2="1.651" layer="21"/>
<rectangle x1="3.429" y1="1.651" x2="3.76681875" y2="1.73558125" layer="21"/>
<rectangle x1="5.969" y1="1.651" x2="6.30681875" y2="1.73558125" layer="21"/>
<rectangle x1="6.64718125" y1="1.651" x2="6.90118125" y2="1.73558125" layer="21"/>
<rectangle x1="8.33881875" y1="1.651" x2="8.59281875" y2="1.73558125" layer="21"/>
<rectangle x1="8.84681875" y1="1.651" x2="9.18718125" y2="1.73558125" layer="21"/>
<rectangle x1="9.69518125" y1="1.651" x2="10.033" y2="1.73558125" layer="21"/>
<rectangle x1="11.303" y1="1.651" x2="11.557" y2="1.73558125" layer="21"/>
<rectangle x1="13.50518125" y1="1.651" x2="13.843" y2="1.73558125" layer="21"/>
<rectangle x1="14.097" y1="1.651" x2="14.351" y2="1.73558125" layer="21"/>
<rectangle x1="15.79118125" y1="1.651" x2="16.04518125" y2="1.73558125" layer="21"/>
<rectangle x1="16.55318125" y1="1.651" x2="17.31518125" y2="1.73558125" layer="21"/>
<rectangle x1="17.907" y1="1.651" x2="19.939" y2="1.73558125" layer="21"/>
<rectangle x1="20.36318125" y1="1.651" x2="20.701" y2="1.73558125" layer="21"/>
<rectangle x1="22.90318125" y1="1.651" x2="23.241" y2="1.73558125" layer="21"/>
<rectangle x1="23.495" y1="1.651" x2="23.749" y2="1.73558125" layer="21"/>
<rectangle x1="25.18918125" y1="1.651" x2="25.44318125" y2="1.73558125" layer="21"/>
<rectangle x1="25.61081875" y1="1.651" x2="27.64281875" y2="1.73558125" layer="21"/>
<rectangle x1="3.429" y1="1.73558125" x2="3.683" y2="1.82041875" layer="21"/>
<rectangle x1="6.05281875" y1="1.73558125" x2="6.30681875" y2="1.82041875" layer="21"/>
<rectangle x1="6.64718125" y1="1.73558125" x2="6.90118125" y2="1.82041875" layer="21"/>
<rectangle x1="8.33881875" y1="1.73558125" x2="8.59281875" y2="1.82041875" layer="21"/>
<rectangle x1="8.84681875" y1="1.73558125" x2="9.18718125" y2="1.82041875" layer="21"/>
<rectangle x1="9.69518125" y1="1.73558125" x2="10.033" y2="1.82041875" layer="21"/>
<rectangle x1="11.303" y1="1.73558125" x2="11.557" y2="1.82041875" layer="21"/>
<rectangle x1="13.50518125" y1="1.73558125" x2="13.843" y2="1.82041875" layer="21"/>
<rectangle x1="14.097" y1="1.73558125" x2="14.351" y2="1.82041875" layer="21"/>
<rectangle x1="15.79118125" y1="1.73558125" x2="16.04518125" y2="1.82041875" layer="21"/>
<rectangle x1="16.383" y1="1.73558125" x2="17.145" y2="1.82041875" layer="21"/>
<rectangle x1="17.907" y1="1.73558125" x2="19.85518125" y2="1.82041875" layer="21"/>
<rectangle x1="20.36318125" y1="1.73558125" x2="20.61718125" y2="1.82041875" layer="21"/>
<rectangle x1="22.987" y1="1.73558125" x2="23.241" y2="1.82041875" layer="21"/>
<rectangle x1="23.495" y1="1.73558125" x2="23.749" y2="1.82041875" layer="21"/>
<rectangle x1="25.18918125" y1="1.73558125" x2="25.44318125" y2="1.82041875" layer="21"/>
<rectangle x1="25.69718125" y1="1.73558125" x2="27.64281875" y2="1.82041875" layer="21"/>
<rectangle x1="3.429" y1="1.82041875" x2="3.683" y2="1.905" layer="21"/>
<rectangle x1="6.05281875" y1="1.82041875" x2="6.30681875" y2="1.905" layer="21"/>
<rectangle x1="6.64718125" y1="1.82041875" x2="6.90118125" y2="1.905" layer="21"/>
<rectangle x1="8.33881875" y1="1.82041875" x2="8.59281875" y2="1.905" layer="21"/>
<rectangle x1="8.84681875" y1="1.82041875" x2="9.18718125" y2="1.905" layer="21"/>
<rectangle x1="9.69518125" y1="1.82041875" x2="10.033" y2="1.905" layer="21"/>
<rectangle x1="11.303" y1="1.82041875" x2="11.557" y2="1.905" layer="21"/>
<rectangle x1="13.589" y1="1.82041875" x2="13.843" y2="1.905" layer="21"/>
<rectangle x1="14.097" y1="1.82041875" x2="14.43481875" y2="1.905" layer="21"/>
<rectangle x1="15.79118125" y1="1.82041875" x2="16.04518125" y2="1.905" layer="21"/>
<rectangle x1="16.383" y1="1.82041875" x2="16.891" y2="1.905" layer="21"/>
<rectangle x1="17.907" y1="1.82041875" x2="18.24481875" y2="1.905" layer="21"/>
<rectangle x1="19.60118125" y1="1.82041875" x2="19.85518125" y2="1.905" layer="21"/>
<rectangle x1="20.36318125" y1="1.82041875" x2="20.61718125" y2="1.905" layer="21"/>
<rectangle x1="22.987" y1="1.82041875" x2="23.241" y2="1.905" layer="21"/>
<rectangle x1="23.495" y1="1.82041875" x2="23.749" y2="1.905" layer="21"/>
<rectangle x1="25.10281875" y1="1.82041875" x2="25.44318125" y2="1.905" layer="21"/>
<rectangle x1="25.69718125" y1="1.82041875" x2="25.95118125" y2="1.905" layer="21"/>
<rectangle x1="27.305" y1="1.82041875" x2="27.64281875" y2="1.905" layer="21"/>
<rectangle x1="3.429" y1="1.905" x2="3.683" y2="1.98958125" layer="21"/>
<rectangle x1="6.05281875" y1="1.905" x2="6.30681875" y2="1.98958125" layer="21"/>
<rectangle x1="6.64718125" y1="1.905" x2="6.985" y2="1.98958125" layer="21"/>
<rectangle x1="8.255" y1="1.905" x2="8.59281875" y2="1.98958125" layer="21"/>
<rectangle x1="8.84681875" y1="1.905" x2="9.18718125" y2="1.98958125" layer="21"/>
<rectangle x1="9.69518125" y1="1.905" x2="10.033" y2="1.98958125" layer="21"/>
<rectangle x1="11.303" y1="1.905" x2="11.557" y2="1.98958125" layer="21"/>
<rectangle x1="13.589" y1="1.905" x2="13.843" y2="1.98958125" layer="21"/>
<rectangle x1="14.097" y1="1.905" x2="14.43481875" y2="1.98958125" layer="21"/>
<rectangle x1="15.70481875" y1="1.905" x2="16.04518125" y2="1.98958125" layer="21"/>
<rectangle x1="16.29918125" y1="1.905" x2="16.72081875" y2="1.98958125" layer="21"/>
<rectangle x1="17.907" y1="1.905" x2="18.24481875" y2="1.98958125" layer="21"/>
<rectangle x1="19.51481875" y1="1.905" x2="19.85518125" y2="1.98958125" layer="21"/>
<rectangle x1="20.36318125" y1="1.905" x2="20.61718125" y2="1.98958125" layer="21"/>
<rectangle x1="22.987" y1="1.905" x2="23.241" y2="1.98958125" layer="21"/>
<rectangle x1="23.495" y1="1.905" x2="23.83281875" y2="1.98958125" layer="21"/>
<rectangle x1="25.10281875" y1="1.905" x2="25.44318125" y2="1.98958125" layer="21"/>
<rectangle x1="25.69718125" y1="1.905" x2="26.035" y2="1.98958125" layer="21"/>
<rectangle x1="27.305" y1="1.905" x2="27.64281875" y2="1.98958125" layer="21"/>
<rectangle x1="3.429" y1="1.98958125" x2="3.683" y2="2.07441875" layer="21"/>
<rectangle x1="6.05281875" y1="1.98958125" x2="6.30681875" y2="2.07441875" layer="21"/>
<rectangle x1="6.64718125" y1="1.98958125" x2="6.985" y2="2.07441875" layer="21"/>
<rectangle x1="8.255" y1="1.98958125" x2="8.509" y2="2.07441875" layer="21"/>
<rectangle x1="8.84681875" y1="1.98958125" x2="9.18718125" y2="2.07441875" layer="21"/>
<rectangle x1="9.69518125" y1="1.98958125" x2="10.033" y2="2.07441875" layer="21"/>
<rectangle x1="11.303" y1="1.98958125" x2="11.557" y2="2.07441875" layer="21"/>
<rectangle x1="13.589" y1="1.98958125" x2="13.843" y2="2.07441875" layer="21"/>
<rectangle x1="14.18081875" y1="1.98958125" x2="14.43481875" y2="2.07441875" layer="21"/>
<rectangle x1="15.70481875" y1="1.98958125" x2="16.04518125" y2="2.07441875" layer="21"/>
<rectangle x1="16.29918125" y1="1.98958125" x2="16.55318125" y2="2.07441875" layer="21"/>
<rectangle x1="17.99081875" y1="1.98958125" x2="18.24481875" y2="2.07441875" layer="21"/>
<rectangle x1="19.51481875" y1="1.98958125" x2="19.85518125" y2="2.07441875" layer="21"/>
<rectangle x1="20.36318125" y1="1.98958125" x2="20.61718125" y2="2.07441875" layer="21"/>
<rectangle x1="22.987" y1="1.98958125" x2="23.241" y2="2.07441875" layer="21"/>
<rectangle x1="23.495" y1="1.98958125" x2="23.83281875" y2="2.07441875" layer="21"/>
<rectangle x1="25.019" y1="1.98958125" x2="25.35681875" y2="2.07441875" layer="21"/>
<rectangle x1="25.69718125" y1="1.98958125" x2="26.035" y2="2.07441875" layer="21"/>
<rectangle x1="27.22118125" y1="1.98958125" x2="27.559" y2="2.07441875" layer="21"/>
<rectangle x1="3.429" y1="2.07441875" x2="3.683" y2="2.159" layer="21"/>
<rectangle x1="6.05281875" y1="2.07441875" x2="6.30681875" y2="2.159" layer="21"/>
<rectangle x1="6.64718125" y1="2.07441875" x2="7.06881875" y2="2.159" layer="21"/>
<rectangle x1="8.17118125" y1="2.07441875" x2="8.509" y2="2.159" layer="21"/>
<rectangle x1="8.84681875" y1="2.07441875" x2="9.18718125" y2="2.159" layer="21"/>
<rectangle x1="9.69518125" y1="2.07441875" x2="10.033" y2="2.159" layer="21"/>
<rectangle x1="11.303" y1="2.07441875" x2="11.557" y2="2.159" layer="21"/>
<rectangle x1="13.589" y1="2.07441875" x2="13.92681875" y2="2.159" layer="21"/>
<rectangle x1="14.18081875" y1="2.07441875" x2="14.52118125" y2="2.159" layer="21"/>
<rectangle x1="15.621" y1="2.07441875" x2="15.95881875" y2="2.159" layer="21"/>
<rectangle x1="16.29918125" y1="2.07441875" x2="16.55318125" y2="2.159" layer="21"/>
<rectangle x1="17.399" y1="2.07441875" x2="17.653" y2="2.159" layer="21"/>
<rectangle x1="17.99081875" y1="2.07441875" x2="18.33118125" y2="2.159" layer="21"/>
<rectangle x1="19.431" y1="2.07441875" x2="19.76881875" y2="2.159" layer="21"/>
<rectangle x1="20.36318125" y1="2.07441875" x2="20.61718125" y2="2.159" layer="21"/>
<rectangle x1="22.987" y1="2.07441875" x2="23.241" y2="2.159" layer="21"/>
<rectangle x1="23.495" y1="2.07441875" x2="23.91918125" y2="2.159" layer="21"/>
<rectangle x1="25.019" y1="2.07441875" x2="25.35681875" y2="2.159" layer="21"/>
<rectangle x1="25.781" y1="2.07441875" x2="26.11881875" y2="2.159" layer="21"/>
<rectangle x1="27.22118125" y1="2.07441875" x2="27.559" y2="2.159" layer="21"/>
<rectangle x1="3.429" y1="2.159" x2="3.683" y2="2.24358125" layer="21"/>
<rectangle x1="6.05281875" y1="2.159" x2="6.30681875" y2="2.24358125" layer="21"/>
<rectangle x1="6.64718125" y1="2.159" x2="7.15518125" y2="2.24358125" layer="21"/>
<rectangle x1="8.08481875" y1="2.159" x2="8.42518125" y2="2.24358125" layer="21"/>
<rectangle x1="8.84681875" y1="2.159" x2="9.18718125" y2="2.24358125" layer="21"/>
<rectangle x1="9.69518125" y1="2.159" x2="10.033" y2="2.24358125" layer="21"/>
<rectangle x1="11.303" y1="2.159" x2="11.557" y2="2.24358125" layer="21"/>
<rectangle x1="13.589" y1="2.159" x2="13.92681875" y2="2.24358125" layer="21"/>
<rectangle x1="14.26718125" y1="2.159" x2="14.605" y2="2.24358125" layer="21"/>
<rectangle x1="15.53718125" y1="2.159" x2="15.95881875" y2="2.24358125" layer="21"/>
<rectangle x1="16.29918125" y1="2.159" x2="16.637" y2="2.24358125" layer="21"/>
<rectangle x1="17.31518125" y1="2.159" x2="17.653" y2="2.24358125" layer="21"/>
<rectangle x1="18.07718125" y1="2.159" x2="18.415" y2="2.24358125" layer="21"/>
<rectangle x1="19.34718125" y1="2.159" x2="19.76881875" y2="2.24358125" layer="21"/>
<rectangle x1="20.36318125" y1="2.159" x2="20.61718125" y2="2.24358125" layer="21"/>
<rectangle x1="22.987" y1="2.159" x2="23.241" y2="2.24358125" layer="21"/>
<rectangle x1="23.495" y1="2.159" x2="24.003" y2="2.24358125" layer="21"/>
<rectangle x1="24.93518125" y1="2.159" x2="25.273" y2="2.24358125" layer="21"/>
<rectangle x1="25.781" y1="2.159" x2="26.20518125" y2="2.24358125" layer="21"/>
<rectangle x1="27.13481875" y1="2.159" x2="27.47518125" y2="2.24358125" layer="21"/>
<rectangle x1="3.429" y1="2.24358125" x2="3.683" y2="2.32841875" layer="21"/>
<rectangle x1="6.05281875" y1="2.24358125" x2="6.30681875" y2="2.32841875" layer="21"/>
<rectangle x1="6.64718125" y1="2.24358125" x2="7.32281875" y2="2.32841875" layer="21"/>
<rectangle x1="7.91718125" y1="2.24358125" x2="8.42518125" y2="2.32841875" layer="21"/>
<rectangle x1="8.84681875" y1="2.24358125" x2="9.18718125" y2="2.32841875" layer="21"/>
<rectangle x1="9.69518125" y1="2.24358125" x2="10.033" y2="2.32841875" layer="21"/>
<rectangle x1="11.303" y1="2.24358125" x2="11.557" y2="2.32841875" layer="21"/>
<rectangle x1="13.589" y1="2.24358125" x2="13.843" y2="2.32841875" layer="21"/>
<rectangle x1="14.26718125" y1="2.24358125" x2="14.77518125" y2="2.32841875" layer="21"/>
<rectangle x1="15.45081875" y1="2.24358125" x2="15.875" y2="2.32841875" layer="21"/>
<rectangle x1="16.29918125" y1="2.24358125" x2="16.72081875" y2="2.32841875" layer="21"/>
<rectangle x1="17.22881875" y1="2.24358125" x2="17.653" y2="2.32841875" layer="21"/>
<rectangle x1="18.07718125" y1="2.24358125" x2="18.58518125" y2="2.32841875" layer="21"/>
<rectangle x1="19.177" y1="2.24358125" x2="19.685" y2="2.32841875" layer="21"/>
<rectangle x1="20.36318125" y1="2.24358125" x2="20.701" y2="2.32841875" layer="21"/>
<rectangle x1="22.90318125" y1="2.24358125" x2="23.241" y2="2.32841875" layer="21"/>
<rectangle x1="23.495" y1="2.24358125" x2="24.17318125" y2="2.32841875" layer="21"/>
<rectangle x1="24.765" y1="2.24358125" x2="25.273" y2="2.32841875" layer="21"/>
<rectangle x1="25.86481875" y1="2.24358125" x2="26.289" y2="2.32841875" layer="21"/>
<rectangle x1="26.96718125" y1="2.24358125" x2="27.47518125" y2="2.32841875" layer="21"/>
<rectangle x1="3.429" y1="2.32841875" x2="3.683" y2="2.413" layer="21"/>
<rectangle x1="6.05281875" y1="2.32841875" x2="6.30681875" y2="2.413" layer="21"/>
<rectangle x1="6.64718125" y1="2.32841875" x2="7.493" y2="2.413" layer="21"/>
<rectangle x1="7.66318125" y1="2.32841875" x2="8.33881875" y2="2.413" layer="21"/>
<rectangle x1="8.84681875" y1="2.32841875" x2="9.18718125" y2="2.413" layer="21"/>
<rectangle x1="9.69518125" y1="2.32841875" x2="10.033" y2="2.413" layer="21"/>
<rectangle x1="11.303" y1="2.32841875" x2="11.557" y2="2.413" layer="21"/>
<rectangle x1="13.589" y1="2.32841875" x2="13.843" y2="2.413" layer="21"/>
<rectangle x1="14.351" y1="2.32841875" x2="15.02918125" y2="2.413" layer="21"/>
<rectangle x1="15.19681875" y1="2.32841875" x2="15.79118125" y2="2.413" layer="21"/>
<rectangle x1="16.383" y1="2.32841875" x2="16.891" y2="2.413" layer="21"/>
<rectangle x1="17.06118125" y1="2.32841875" x2="17.56918125" y2="2.413" layer="21"/>
<rectangle x1="18.161" y1="2.32841875" x2="18.83918125" y2="2.413" layer="21"/>
<rectangle x1="19.00681875" y1="2.32841875" x2="19.60118125" y2="2.413" layer="21"/>
<rectangle x1="20.36318125" y1="2.32841875" x2="20.701" y2="2.413" layer="21"/>
<rectangle x1="22.90318125" y1="2.32841875" x2="23.241" y2="2.413" layer="21"/>
<rectangle x1="23.495" y1="2.32841875" x2="24.34081875" y2="2.413" layer="21"/>
<rectangle x1="24.511" y1="2.32841875" x2="25.18918125" y2="2.413" layer="21"/>
<rectangle x1="25.95118125" y1="2.32841875" x2="26.543" y2="2.413" layer="21"/>
<rectangle x1="26.71318125" y1="2.32841875" x2="27.38881875" y2="2.413" layer="21"/>
<rectangle x1="3.429" y1="2.413" x2="3.683" y2="2.49758125" layer="21"/>
<rectangle x1="6.05281875" y1="2.413" x2="6.30681875" y2="2.49758125" layer="21"/>
<rectangle x1="6.64718125" y1="2.413" x2="6.90118125" y2="2.49758125" layer="21"/>
<rectangle x1="6.985" y1="2.413" x2="8.255" y2="2.49758125" layer="21"/>
<rectangle x1="8.84681875" y1="2.413" x2="9.18718125" y2="2.49758125" layer="21"/>
<rectangle x1="9.35481875" y1="2.413" x2="10.795" y2="2.49758125" layer="21"/>
<rectangle x1="11.303" y1="2.413" x2="11.557" y2="2.49758125" layer="21"/>
<rectangle x1="13.50518125" y1="2.413" x2="13.843" y2="2.49758125" layer="21"/>
<rectangle x1="14.43481875" y1="2.413" x2="15.70481875" y2="2.49758125" layer="21"/>
<rectangle x1="16.46681875" y1="2.413" x2="17.48281875" y2="2.49758125" layer="21"/>
<rectangle x1="18.24481875" y1="2.413" x2="19.51481875" y2="2.49758125" layer="21"/>
<rectangle x1="20.447" y1="2.413" x2="20.701" y2="2.49758125" layer="21"/>
<rectangle x1="22.90318125" y1="2.413" x2="23.241" y2="2.49758125" layer="21"/>
<rectangle x1="23.495" y1="2.413" x2="23.749" y2="2.49758125" layer="21"/>
<rectangle x1="23.83281875" y1="2.413" x2="25.10281875" y2="2.49758125" layer="21"/>
<rectangle x1="26.035" y1="2.413" x2="27.305" y2="2.49758125" layer="21"/>
<rectangle x1="3.429" y1="2.49758125" x2="3.683" y2="2.58241875" layer="21"/>
<rectangle x1="6.05281875" y1="2.49758125" x2="6.30681875" y2="2.58241875" layer="21"/>
<rectangle x1="6.64718125" y1="2.49758125" x2="6.90118125" y2="2.58241875" layer="21"/>
<rectangle x1="7.15518125" y1="2.49758125" x2="8.08481875" y2="2.58241875" layer="21"/>
<rectangle x1="8.84681875" y1="2.49758125" x2="9.18718125" y2="2.58241875" layer="21"/>
<rectangle x1="9.35481875" y1="2.49758125" x2="10.87881875" y2="2.58241875" layer="21"/>
<rectangle x1="11.303" y1="2.49758125" x2="11.557" y2="2.58241875" layer="21"/>
<rectangle x1="13.50518125" y1="2.49758125" x2="13.843" y2="2.58241875" layer="21"/>
<rectangle x1="14.605" y1="2.49758125" x2="15.53718125" y2="2.58241875" layer="21"/>
<rectangle x1="16.55318125" y1="2.49758125" x2="17.399" y2="2.58241875" layer="21"/>
<rectangle x1="18.415" y1="2.49758125" x2="19.34718125" y2="2.58241875" layer="21"/>
<rectangle x1="20.447" y1="2.49758125" x2="20.78481875" y2="2.58241875" layer="21"/>
<rectangle x1="22.81681875" y1="2.49758125" x2="23.15718125" y2="2.58241875" layer="21"/>
<rectangle x1="23.495" y1="2.49758125" x2="23.749" y2="2.58241875" layer="21"/>
<rectangle x1="24.003" y1="2.49758125" x2="24.93518125" y2="2.58241875" layer="21"/>
<rectangle x1="26.20518125" y1="2.49758125" x2="27.13481875" y2="2.58241875" layer="21"/>
<rectangle x1="3.429" y1="2.58241875" x2="3.683" y2="2.667" layer="21"/>
<rectangle x1="6.05281875" y1="2.58241875" x2="6.30681875" y2="2.667" layer="21"/>
<rectangle x1="6.64718125" y1="2.58241875" x2="6.81481875" y2="2.667" layer="21"/>
<rectangle x1="7.32281875" y1="2.58241875" x2="7.91718125" y2="2.667" layer="21"/>
<rectangle x1="8.93318125" y1="2.58241875" x2="9.10081875" y2="2.667" layer="21"/>
<rectangle x1="9.44118125" y1="2.58241875" x2="10.795" y2="2.667" layer="21"/>
<rectangle x1="11.303" y1="2.58241875" x2="11.557" y2="2.667" layer="21"/>
<rectangle x1="13.50518125" y1="2.58241875" x2="13.843" y2="2.667" layer="21"/>
<rectangle x1="14.77518125" y1="2.58241875" x2="15.367" y2="2.667" layer="21"/>
<rectangle x1="16.72081875" y1="2.58241875" x2="17.22881875" y2="2.667" layer="21"/>
<rectangle x1="18.58518125" y1="2.58241875" x2="19.177" y2="2.667" layer="21"/>
<rectangle x1="20.447" y1="2.58241875" x2="20.78481875" y2="2.667" layer="21"/>
<rectangle x1="22.81681875" y1="2.58241875" x2="23.15718125" y2="2.667" layer="21"/>
<rectangle x1="23.495" y1="2.58241875" x2="23.66518125" y2="2.667" layer="21"/>
<rectangle x1="24.17318125" y1="2.58241875" x2="24.765" y2="2.667" layer="21"/>
<rectangle x1="26.37281875" y1="2.58241875" x2="26.96718125" y2="2.667" layer="21"/>
<rectangle x1="3.429" y1="2.667" x2="3.683" y2="2.75158125" layer="21"/>
<rectangle x1="6.05281875" y1="2.667" x2="6.30681875" y2="2.75158125" layer="21"/>
<rectangle x1="9.69518125" y1="2.667" x2="10.033" y2="2.75158125" layer="21"/>
<rectangle x1="11.303" y1="2.667" x2="11.557" y2="2.75158125" layer="21"/>
<rectangle x1="13.41881875" y1="2.667" x2="13.75918125" y2="2.75158125" layer="21"/>
<rectangle x1="20.53081875" y1="2.667" x2="20.87118125" y2="2.75158125" layer="21"/>
<rectangle x1="22.733" y1="2.667" x2="23.07081875" y2="2.75158125" layer="21"/>
<rectangle x1="3.429" y1="2.75158125" x2="3.683" y2="2.83641875" layer="21"/>
<rectangle x1="6.05281875" y1="2.75158125" x2="6.30681875" y2="2.83641875" layer="21"/>
<rectangle x1="8.93318125" y1="2.75158125" x2="9.10081875" y2="2.83641875" layer="21"/>
<rectangle x1="9.69518125" y1="2.75158125" x2="10.033" y2="2.83641875" layer="21"/>
<rectangle x1="11.303" y1="2.75158125" x2="11.557" y2="2.83641875" layer="21"/>
<rectangle x1="13.41881875" y1="2.75158125" x2="13.75918125" y2="2.83641875" layer="21"/>
<rectangle x1="20.61718125" y1="2.75158125" x2="20.955" y2="2.83641875" layer="21"/>
<rectangle x1="22.64918125" y1="2.75158125" x2="23.07081875" y2="2.83641875" layer="21"/>
<rectangle x1="3.429" y1="2.83641875" x2="3.683" y2="2.921" layer="21"/>
<rectangle x1="6.05281875" y1="2.83641875" x2="6.30681875" y2="2.921" layer="21"/>
<rectangle x1="8.84681875" y1="2.83641875" x2="9.18718125" y2="2.921" layer="21"/>
<rectangle x1="9.69518125" y1="2.83641875" x2="10.033" y2="2.921" layer="21"/>
<rectangle x1="11.303" y1="2.83641875" x2="11.557" y2="2.921" layer="21"/>
<rectangle x1="13.335" y1="2.83641875" x2="13.67281875" y2="2.921" layer="21"/>
<rectangle x1="20.61718125" y1="2.83641875" x2="21.03881875" y2="2.921" layer="21"/>
<rectangle x1="22.56281875" y1="2.83641875" x2="22.987" y2="2.921" layer="21"/>
<rectangle x1="3.429" y1="2.921" x2="3.683" y2="3.00558125" layer="21"/>
<rectangle x1="6.05281875" y1="2.921" x2="6.30681875" y2="3.00558125" layer="21"/>
<rectangle x1="8.84681875" y1="2.921" x2="9.18718125" y2="3.00558125" layer="21"/>
<rectangle x1="9.69518125" y1="2.921" x2="10.033" y2="3.00558125" layer="21"/>
<rectangle x1="11.303" y1="2.921" x2="11.557" y2="3.00558125" layer="21"/>
<rectangle x1="13.25118125" y1="2.921" x2="13.589" y2="3.00558125" layer="21"/>
<rectangle x1="20.701" y1="2.921" x2="21.12518125" y2="3.00558125" layer="21"/>
<rectangle x1="22.479" y1="2.921" x2="22.90318125" y2="3.00558125" layer="21"/>
<rectangle x1="3.429" y1="3.00558125" x2="3.683" y2="3.09041875" layer="21"/>
<rectangle x1="6.05281875" y1="3.00558125" x2="6.30681875" y2="3.09041875" layer="21"/>
<rectangle x1="8.93318125" y1="3.00558125" x2="9.18718125" y2="3.09041875" layer="21"/>
<rectangle x1="9.69518125" y1="3.00558125" x2="10.033" y2="3.09041875" layer="21"/>
<rectangle x1="11.303" y1="3.00558125" x2="11.557" y2="3.09041875" layer="21"/>
<rectangle x1="13.16481875" y1="3.00558125" x2="13.589" y2="3.09041875" layer="21"/>
<rectangle x1="20.78481875" y1="3.00558125" x2="21.29281875" y2="3.09041875" layer="21"/>
<rectangle x1="22.30881875" y1="3.00558125" x2="22.81681875" y2="3.09041875" layer="21"/>
<rectangle x1="3.429" y1="3.09041875" x2="3.683" y2="3.175" layer="21"/>
<rectangle x1="6.05281875" y1="3.09041875" x2="6.39318125" y2="3.175" layer="21"/>
<rectangle x1="9.69518125" y1="3.09041875" x2="10.033" y2="3.175" layer="21"/>
<rectangle x1="11.303" y1="3.09041875" x2="11.557" y2="3.175" layer="21"/>
<rectangle x1="12.99718125" y1="3.09041875" x2="13.50518125" y2="3.175" layer="21"/>
<rectangle x1="20.87118125" y1="3.09041875" x2="21.54681875" y2="3.175" layer="21"/>
<rectangle x1="22.05481875" y1="3.09041875" x2="22.733" y2="3.175" layer="21"/>
<rectangle x1="3.429" y1="3.175" x2="3.683" y2="3.25958125" layer="21"/>
<rectangle x1="6.05281875" y1="3.175" x2="6.30681875" y2="3.25958125" layer="21"/>
<rectangle x1="9.69518125" y1="3.175" x2="10.033" y2="3.25958125" layer="21"/>
<rectangle x1="11.303" y1="3.175" x2="11.557" y2="3.25958125" layer="21"/>
<rectangle x1="12.827" y1="3.175" x2="13.41881875" y2="3.25958125" layer="21"/>
<rectangle x1="20.955" y1="3.175" x2="22.64918125" y2="3.25958125" layer="21"/>
<rectangle x1="3.429" y1="3.25958125" x2="3.683" y2="3.34441875" layer="21"/>
<rectangle x1="6.05281875" y1="3.25958125" x2="6.30681875" y2="3.34441875" layer="21"/>
<rectangle x1="9.69518125" y1="3.25958125" x2="10.033" y2="3.34441875" layer="21"/>
<rectangle x1="11.303" y1="3.25958125" x2="13.335" y2="3.34441875" layer="21"/>
<rectangle x1="21.12518125" y1="3.25958125" x2="22.479" y2="3.34441875" layer="21"/>
<rectangle x1="3.429" y1="3.34441875" x2="3.683" y2="3.429" layer="21"/>
<rectangle x1="6.05281875" y1="3.34441875" x2="6.30681875" y2="3.429" layer="21"/>
<rectangle x1="9.69518125" y1="3.34441875" x2="9.94918125" y2="3.429" layer="21"/>
<rectangle x1="11.303" y1="3.34441875" x2="13.16481875" y2="3.429" layer="21"/>
<rectangle x1="21.29281875" y1="3.34441875" x2="22.225" y2="3.429" layer="21"/>
<rectangle x1="11.303" y1="3.429" x2="12.99718125" y2="3.51358125" layer="21"/>
<rectangle x1="11.303" y1="3.51358125" x2="12.827" y2="3.59841875" layer="21"/>
<rectangle x1="5.63118125" y1="4.86841875" x2="24.59481875" y2="4.953" layer="21"/>
<rectangle x1="5.03681875" y1="4.953" x2="25.18918125" y2="5.03758125" layer="21"/>
<rectangle x1="4.699" y1="5.03758125" x2="25.527" y2="5.12241875" layer="21"/>
<rectangle x1="4.36118125" y1="5.12241875" x2="25.86481875" y2="5.207" layer="21"/>
<rectangle x1="4.10718125" y1="5.207" x2="26.11881875" y2="5.29158125" layer="21"/>
<rectangle x1="3.85318125" y1="5.29158125" x2="26.289" y2="5.37641875" layer="21"/>
<rectangle x1="3.683" y1="5.37641875" x2="26.543" y2="5.461" layer="21"/>
<rectangle x1="3.51281875" y1="5.461" x2="26.71318125" y2="5.54558125" layer="21"/>
<rectangle x1="3.34518125" y1="5.54558125" x2="26.88081875" y2="5.63041875" layer="21"/>
<rectangle x1="3.175" y1="5.63041875" x2="27.051" y2="5.715" layer="21"/>
<rectangle x1="3.00481875" y1="5.715" x2="27.13481875" y2="5.79958125" layer="21"/>
<rectangle x1="2.921" y1="5.79958125" x2="5.29081875" y2="5.88441875" layer="21"/>
<rectangle x1="14.77518125" y1="5.79958125" x2="27.305" y2="5.88441875" layer="21"/>
<rectangle x1="2.75081875" y1="5.88441875" x2="4.86918125" y2="5.969" layer="21"/>
<rectangle x1="14.68881875" y1="5.88441875" x2="27.38881875" y2="5.969" layer="21"/>
<rectangle x1="2.667" y1="5.969" x2="4.52881875" y2="6.05358125" layer="21"/>
<rectangle x1="14.605" y1="5.969" x2="27.559" y2="6.05358125" layer="21"/>
<rectangle x1="2.49681875" y1="6.05358125" x2="4.36118125" y2="6.13841875" layer="21"/>
<rectangle x1="14.605" y1="6.05358125" x2="27.64281875" y2="6.13841875" layer="21"/>
<rectangle x1="2.413" y1="6.13841875" x2="4.10718125" y2="6.223" layer="21"/>
<rectangle x1="14.52118125" y1="6.13841875" x2="27.72918125" y2="6.223" layer="21"/>
<rectangle x1="2.32918125" y1="6.223" x2="3.937" y2="6.30758125" layer="21"/>
<rectangle x1="14.52118125" y1="6.223" x2="27.89681875" y2="6.30758125" layer="21"/>
<rectangle x1="2.24281875" y1="6.30758125" x2="3.76681875" y2="6.39241875" layer="21"/>
<rectangle x1="14.43481875" y1="6.30758125" x2="27.98318125" y2="6.39241875" layer="21"/>
<rectangle x1="2.159" y1="6.39241875" x2="3.59918125" y2="6.477" layer="21"/>
<rectangle x1="14.43481875" y1="6.39241875" x2="28.067" y2="6.477" layer="21"/>
<rectangle x1="2.07518125" y1="6.477" x2="3.429" y2="6.56158125" layer="21"/>
<rectangle x1="14.351" y1="6.477" x2="28.15081875" y2="6.56158125" layer="21"/>
<rectangle x1="1.905" y1="6.56158125" x2="3.34518125" y2="6.64641875" layer="21"/>
<rectangle x1="14.351" y1="6.56158125" x2="28.23718125" y2="6.64641875" layer="21"/>
<rectangle x1="1.905" y1="6.64641875" x2="3.175" y2="6.731" layer="21"/>
<rectangle x1="14.26718125" y1="6.64641875" x2="28.321" y2="6.731" layer="21"/>
<rectangle x1="1.73481875" y1="6.731" x2="3.09118125" y2="6.81558125" layer="21"/>
<rectangle x1="14.26718125" y1="6.731" x2="28.40481875" y2="6.81558125" layer="21"/>
<rectangle x1="1.73481875" y1="6.81558125" x2="3.00481875" y2="6.90041875" layer="21"/>
<rectangle x1="14.18081875" y1="6.81558125" x2="28.49118125" y2="6.90041875" layer="21"/>
<rectangle x1="1.651" y1="6.90041875" x2="2.83718125" y2="6.985" layer="21"/>
<rectangle x1="14.18081875" y1="6.90041875" x2="28.575" y2="6.985" layer="21"/>
<rectangle x1="1.56718125" y1="6.985" x2="2.75081875" y2="7.06958125" layer="21"/>
<rectangle x1="14.18081875" y1="6.985" x2="28.65881875" y2="7.06958125" layer="21"/>
<rectangle x1="1.48081875" y1="7.06958125" x2="2.667" y2="7.15441875" layer="21"/>
<rectangle x1="14.097" y1="7.06958125" x2="28.74518125" y2="7.15441875" layer="21"/>
<rectangle x1="1.397" y1="7.15441875" x2="2.58318125" y2="7.239" layer="21"/>
<rectangle x1="14.097" y1="7.15441875" x2="28.74518125" y2="7.239" layer="21"/>
<rectangle x1="1.31318125" y1="7.239" x2="2.49681875" y2="7.32358125" layer="21"/>
<rectangle x1="14.097" y1="7.239" x2="28.829" y2="7.32358125" layer="21"/>
<rectangle x1="1.31318125" y1="7.32358125" x2="2.413" y2="7.40841875" layer="21"/>
<rectangle x1="14.01318125" y1="7.32358125" x2="28.91281875" y2="7.40841875" layer="21"/>
<rectangle x1="1.22681875" y1="7.40841875" x2="2.32918125" y2="7.493" layer="21"/>
<rectangle x1="14.01318125" y1="7.40841875" x2="28.99918125" y2="7.493" layer="21"/>
<rectangle x1="1.143" y1="7.493" x2="2.24281875" y2="7.57758125" layer="21"/>
<rectangle x1="14.01318125" y1="7.493" x2="28.99918125" y2="7.57758125" layer="21"/>
<rectangle x1="1.143" y1="7.57758125" x2="2.159" y2="7.66241875" layer="21"/>
<rectangle x1="13.92681875" y1="7.57758125" x2="29.083" y2="7.66241875" layer="21"/>
<rectangle x1="1.05918125" y1="7.66241875" x2="2.07518125" y2="7.747" layer="21"/>
<rectangle x1="13.92681875" y1="7.66241875" x2="29.16681875" y2="7.747" layer="21"/>
<rectangle x1="0.97281875" y1="7.747" x2="2.07518125" y2="7.83158125" layer="21"/>
<rectangle x1="13.92681875" y1="7.747" x2="29.16681875" y2="7.83158125" layer="21"/>
<rectangle x1="0.97281875" y1="7.83158125" x2="1.98881875" y2="7.91641875" layer="21"/>
<rectangle x1="13.92681875" y1="7.83158125" x2="29.25318125" y2="7.91641875" layer="21"/>
<rectangle x1="0.889" y1="7.91641875" x2="1.905" y2="8.001" layer="21"/>
<rectangle x1="13.843" y1="7.91641875" x2="29.337" y2="8.001" layer="21"/>
<rectangle x1="0.889" y1="8.001" x2="1.82118125" y2="8.08558125" layer="21"/>
<rectangle x1="13.843" y1="8.001" x2="29.337" y2="8.08558125" layer="21"/>
<rectangle x1="0.80518125" y1="8.08558125" x2="1.82118125" y2="8.17041875" layer="21"/>
<rectangle x1="13.843" y1="8.08558125" x2="29.42081875" y2="8.17041875" layer="21"/>
<rectangle x1="0.71881875" y1="8.17041875" x2="1.73481875" y2="8.255" layer="21"/>
<rectangle x1="13.843" y1="8.17041875" x2="29.42081875" y2="8.255" layer="21"/>
<rectangle x1="0.71881875" y1="8.255" x2="1.73481875" y2="8.33958125" layer="21"/>
<rectangle x1="13.75918125" y1="8.255" x2="29.50718125" y2="8.33958125" layer="21"/>
<rectangle x1="0.71881875" y1="8.33958125" x2="1.651" y2="8.42441875" layer="21"/>
<rectangle x1="13.75918125" y1="8.33958125" x2="29.50718125" y2="8.42441875" layer="21"/>
<rectangle x1="0.635" y1="8.42441875" x2="1.651" y2="8.509" layer="21"/>
<rectangle x1="5.63118125" y1="8.42441875" x2="6.64718125" y2="8.509" layer="21"/>
<rectangle x1="8.763" y1="8.42441875" x2="10.541" y2="8.509" layer="21"/>
<rectangle x1="13.75918125" y1="8.42441875" x2="16.80718125" y2="8.509" layer="21"/>
<rectangle x1="17.145" y1="8.42441875" x2="29.591" y2="8.509" layer="21"/>
<rectangle x1="0.635" y1="8.509" x2="1.56718125" y2="8.59358125" layer="21"/>
<rectangle x1="5.29081875" y1="8.509" x2="6.90118125" y2="8.59358125" layer="21"/>
<rectangle x1="8.67918125" y1="8.509" x2="10.87881875" y2="8.59358125" layer="21"/>
<rectangle x1="13.75918125" y1="8.509" x2="16.29918125" y2="8.59358125" layer="21"/>
<rectangle x1="17.653" y1="8.509" x2="20.10918125" y2="8.59358125" layer="21"/>
<rectangle x1="20.447" y1="8.509" x2="22.733" y2="8.59358125" layer="21"/>
<rectangle x1="23.07081875" y1="8.509" x2="24.84881875" y2="8.59358125" layer="21"/>
<rectangle x1="25.61081875" y1="8.509" x2="29.591" y2="8.59358125" layer="21"/>
<rectangle x1="0.55118125" y1="8.59358125" x2="1.48081875" y2="8.67841875" layer="21"/>
<rectangle x1="5.12318125" y1="8.59358125" x2="7.15518125" y2="8.67841875" layer="21"/>
<rectangle x1="8.59281875" y1="8.59358125" x2="11.049" y2="8.67841875" layer="21"/>
<rectangle x1="13.75918125" y1="8.59358125" x2="16.04518125" y2="8.67841875" layer="21"/>
<rectangle x1="17.907" y1="8.59358125" x2="20.02281875" y2="8.67841875" layer="21"/>
<rectangle x1="20.53081875" y1="8.59358125" x2="22.64918125" y2="8.67841875" layer="21"/>
<rectangle x1="23.15718125" y1="8.59358125" x2="24.59481875" y2="8.67841875" layer="21"/>
<rectangle x1="25.86481875" y1="8.59358125" x2="29.591" y2="8.67841875" layer="21"/>
<rectangle x1="0.55118125" y1="8.67841875" x2="1.48081875" y2="8.763" layer="21"/>
<rectangle x1="4.953" y1="8.67841875" x2="7.32281875" y2="8.763" layer="21"/>
<rectangle x1="8.59281875" y1="8.67841875" x2="11.21918125" y2="8.763" layer="21"/>
<rectangle x1="13.67281875" y1="8.67841875" x2="15.875" y2="8.763" layer="21"/>
<rectangle x1="18.07718125" y1="8.67841875" x2="19.939" y2="8.763" layer="21"/>
<rectangle x1="20.53081875" y1="8.67841875" x2="22.64918125" y2="8.763" layer="21"/>
<rectangle x1="23.15718125" y1="8.67841875" x2="24.42718125" y2="8.763" layer="21"/>
<rectangle x1="26.11881875" y1="8.67841875" x2="29.67481875" y2="8.763" layer="21"/>
<rectangle x1="0.46481875" y1="8.763" x2="1.397" y2="8.84758125" layer="21"/>
<rectangle x1="4.78281875" y1="8.763" x2="7.493" y2="8.84758125" layer="21"/>
<rectangle x1="8.59281875" y1="8.763" x2="11.38681875" y2="8.84758125" layer="21"/>
<rectangle x1="13.67281875" y1="8.763" x2="15.70481875" y2="8.84758125" layer="21"/>
<rectangle x1="18.24481875" y1="8.763" x2="19.939" y2="8.84758125" layer="21"/>
<rectangle x1="20.53081875" y1="8.763" x2="22.64918125" y2="8.84758125" layer="21"/>
<rectangle x1="23.241" y1="8.763" x2="24.34081875" y2="8.84758125" layer="21"/>
<rectangle x1="26.289" y1="8.763" x2="29.67481875" y2="8.84758125" layer="21"/>
<rectangle x1="0.46481875" y1="8.84758125" x2="1.397" y2="8.93241875" layer="21"/>
<rectangle x1="4.699" y1="8.84758125" x2="7.57681875" y2="8.93241875" layer="21"/>
<rectangle x1="8.59281875" y1="8.84758125" x2="11.47318125" y2="8.93241875" layer="21"/>
<rectangle x1="13.67281875" y1="8.84758125" x2="15.621" y2="8.93241875" layer="21"/>
<rectangle x1="18.33118125" y1="8.84758125" x2="19.939" y2="8.93241875" layer="21"/>
<rectangle x1="20.53081875" y1="8.84758125" x2="22.56281875" y2="8.93241875" layer="21"/>
<rectangle x1="23.241" y1="8.84758125" x2="24.17318125" y2="8.93241875" layer="21"/>
<rectangle x1="26.37281875" y1="8.84758125" x2="29.76118125" y2="8.93241875" layer="21"/>
<rectangle x1="0.46481875" y1="8.93241875" x2="1.397" y2="9.017" layer="21"/>
<rectangle x1="4.52881875" y1="8.93241875" x2="7.66318125" y2="9.017" layer="21"/>
<rectangle x1="8.59281875" y1="8.93241875" x2="11.557" y2="9.017" layer="21"/>
<rectangle x1="13.67281875" y1="8.93241875" x2="15.45081875" y2="9.017" layer="21"/>
<rectangle x1="18.49881875" y1="8.93241875" x2="19.939" y2="9.017" layer="21"/>
<rectangle x1="20.53081875" y1="8.93241875" x2="22.56281875" y2="9.017" layer="21"/>
<rectangle x1="23.241" y1="8.93241875" x2="24.08681875" y2="9.017" layer="21"/>
<rectangle x1="26.45918125" y1="8.93241875" x2="29.76118125" y2="9.017" layer="21"/>
<rectangle x1="0.381" y1="9.017" x2="1.31318125" y2="9.10158125" layer="21"/>
<rectangle x1="4.445" y1="9.017" x2="7.83081875" y2="9.10158125" layer="21"/>
<rectangle x1="8.59281875" y1="9.017" x2="11.72718125" y2="9.10158125" layer="21"/>
<rectangle x1="13.67281875" y1="9.017" x2="15.367" y2="9.10158125" layer="21"/>
<rectangle x1="18.58518125" y1="9.017" x2="19.939" y2="9.10158125" layer="21"/>
<rectangle x1="20.53081875" y1="9.017" x2="22.56281875" y2="9.10158125" layer="21"/>
<rectangle x1="23.241" y1="9.017" x2="24.003" y2="9.10158125" layer="21"/>
<rectangle x1="26.543" y1="9.017" x2="29.76118125" y2="9.10158125" layer="21"/>
<rectangle x1="0.381" y1="9.10158125" x2="1.31318125" y2="9.18641875" layer="21"/>
<rectangle x1="4.36118125" y1="9.10158125" x2="7.91718125" y2="9.18641875" layer="21"/>
<rectangle x1="8.67918125" y1="9.10158125" x2="11.811" y2="9.18641875" layer="21"/>
<rectangle x1="13.67281875" y1="9.10158125" x2="15.28318125" y2="9.18641875" layer="21"/>
<rectangle x1="18.669" y1="9.10158125" x2="19.939" y2="9.18641875" layer="21"/>
<rectangle x1="20.53081875" y1="9.10158125" x2="22.56281875" y2="9.18641875" layer="21"/>
<rectangle x1="23.241" y1="9.10158125" x2="23.91918125" y2="9.18641875" layer="21"/>
<rectangle x1="25.019" y1="9.10158125" x2="25.44318125" y2="9.18641875" layer="21"/>
<rectangle x1="26.62681875" y1="9.10158125" x2="29.845" y2="9.18641875" layer="21"/>
<rectangle x1="0.381" y1="9.18641875" x2="1.31318125" y2="9.271" layer="21"/>
<rectangle x1="4.27481875" y1="9.18641875" x2="8.001" y2="9.271" layer="21"/>
<rectangle x1="8.763" y1="9.18641875" x2="11.89481875" y2="9.271" layer="21"/>
<rectangle x1="13.67281875" y1="9.18641875" x2="15.19681875" y2="9.271" layer="21"/>
<rectangle x1="16.891" y1="9.18641875" x2="17.06118125" y2="9.271" layer="21"/>
<rectangle x1="18.75281875" y1="9.18641875" x2="19.939" y2="9.271" layer="21"/>
<rectangle x1="20.53081875" y1="9.18641875" x2="22.56281875" y2="9.271" layer="21"/>
<rectangle x1="23.241" y1="9.18641875" x2="23.91918125" y2="9.271" layer="21"/>
<rectangle x1="24.765" y1="9.18641875" x2="25.781" y2="9.271" layer="21"/>
<rectangle x1="26.71318125" y1="9.18641875" x2="29.845" y2="9.271" layer="21"/>
<rectangle x1="0.29718125" y1="9.271" x2="1.22681875" y2="9.35558125" layer="21"/>
<rectangle x1="4.191" y1="9.271" x2="5.79881875" y2="9.35558125" layer="21"/>
<rectangle x1="6.477" y1="9.271" x2="8.08481875" y2="9.35558125" layer="21"/>
<rectangle x1="10.287" y1="9.271" x2="11.89481875" y2="9.35558125" layer="21"/>
<rectangle x1="13.589" y1="9.271" x2="15.113" y2="9.35558125" layer="21"/>
<rectangle x1="16.46681875" y1="9.271" x2="17.48281875" y2="9.35558125" layer="21"/>
<rectangle x1="18.83918125" y1="9.271" x2="19.939" y2="9.35558125" layer="21"/>
<rectangle x1="20.53081875" y1="9.271" x2="22.56281875" y2="9.35558125" layer="21"/>
<rectangle x1="23.241" y1="9.271" x2="23.83281875" y2="9.35558125" layer="21"/>
<rectangle x1="24.68118125" y1="9.271" x2="25.95118125" y2="9.35558125" layer="21"/>
<rectangle x1="26.71318125" y1="9.271" x2="29.845" y2="9.35558125" layer="21"/>
<rectangle x1="0.29718125" y1="9.35558125" x2="1.22681875" y2="9.44041875" layer="21"/>
<rectangle x1="4.10718125" y1="9.35558125" x2="5.54481875" y2="9.44041875" layer="21"/>
<rectangle x1="6.731" y1="9.35558125" x2="8.08481875" y2="9.44041875" layer="21"/>
<rectangle x1="10.62481875" y1="9.35558125" x2="11.98118125" y2="9.44041875" layer="21"/>
<rectangle x1="13.589" y1="9.35558125" x2="15.02918125" y2="9.44041875" layer="21"/>
<rectangle x1="16.21281875" y1="9.35558125" x2="17.653" y2="9.44041875" layer="21"/>
<rectangle x1="18.923" y1="9.35558125" x2="19.939" y2="9.44041875" layer="21"/>
<rectangle x1="20.53081875" y1="9.35558125" x2="22.56281875" y2="9.44041875" layer="21"/>
<rectangle x1="23.241" y1="9.35558125" x2="23.749" y2="9.44041875" layer="21"/>
<rectangle x1="24.59481875" y1="9.35558125" x2="26.035" y2="9.44041875" layer="21"/>
<rectangle x1="26.71318125" y1="9.35558125" x2="29.845" y2="9.44041875" layer="21"/>
<rectangle x1="0.29718125" y1="9.44041875" x2="1.22681875" y2="9.525" layer="21"/>
<rectangle x1="4.10718125" y1="9.44041875" x2="5.37718125" y2="9.525" layer="21"/>
<rectangle x1="6.90118125" y1="9.44041875" x2="8.17118125" y2="9.525" layer="21"/>
<rectangle x1="10.795" y1="9.44041875" x2="12.065" y2="9.525" layer="21"/>
<rectangle x1="13.589" y1="9.44041875" x2="14.94281875" y2="9.525" layer="21"/>
<rectangle x1="16.129" y1="9.44041875" x2="17.82318125" y2="9.525" layer="21"/>
<rectangle x1="18.923" y1="9.44041875" x2="19.939" y2="9.525" layer="21"/>
<rectangle x1="20.53081875" y1="9.44041875" x2="22.56281875" y2="9.525" layer="21"/>
<rectangle x1="23.241" y1="9.44041875" x2="23.749" y2="9.525" layer="21"/>
<rectangle x1="24.511" y1="9.44041875" x2="26.11881875" y2="9.525" layer="21"/>
<rectangle x1="26.62681875" y1="9.44041875" x2="29.92881875" y2="9.525" layer="21"/>
<rectangle x1="0.29718125" y1="9.525" x2="1.143" y2="9.60958125" layer="21"/>
<rectangle x1="4.02081875" y1="9.525" x2="5.207" y2="9.60958125" layer="21"/>
<rectangle x1="7.06881875" y1="9.525" x2="8.255" y2="9.60958125" layer="21"/>
<rectangle x1="10.96518125" y1="9.525" x2="12.14881875" y2="9.60958125" layer="21"/>
<rectangle x1="13.589" y1="9.525" x2="14.94281875" y2="9.60958125" layer="21"/>
<rectangle x1="15.95881875" y1="9.525" x2="17.99081875" y2="9.60958125" layer="21"/>
<rectangle x1="19.00681875" y1="9.525" x2="19.939" y2="9.60958125" layer="21"/>
<rectangle x1="20.53081875" y1="9.525" x2="22.56281875" y2="9.60958125" layer="21"/>
<rectangle x1="23.241" y1="9.525" x2="23.749" y2="9.60958125" layer="21"/>
<rectangle x1="24.42718125" y1="9.525" x2="26.20518125" y2="9.60958125" layer="21"/>
<rectangle x1="26.543" y1="9.525" x2="29.92881875" y2="9.60958125" layer="21"/>
<rectangle x1="0.21081875" y1="9.60958125" x2="1.143" y2="9.69441875" layer="21"/>
<rectangle x1="3.937" y1="9.60958125" x2="5.12318125" y2="9.69441875" layer="21"/>
<rectangle x1="7.15518125" y1="9.60958125" x2="8.255" y2="9.69441875" layer="21"/>
<rectangle x1="11.049" y1="9.60958125" x2="12.14881875" y2="9.69441875" layer="21"/>
<rectangle x1="13.589" y1="9.60958125" x2="14.859" y2="9.69441875" layer="21"/>
<rectangle x1="15.875" y1="9.60958125" x2="18.07718125" y2="9.69441875" layer="21"/>
<rectangle x1="19.09318125" y1="9.60958125" x2="19.939" y2="9.69441875" layer="21"/>
<rectangle x1="20.53081875" y1="9.60958125" x2="22.56281875" y2="9.69441875" layer="21"/>
<rectangle x1="23.241" y1="9.60958125" x2="23.66518125" y2="9.69441875" layer="21"/>
<rectangle x1="24.34081875" y1="9.60958125" x2="29.92881875" y2="9.69441875" layer="21"/>
<rectangle x1="0.21081875" y1="9.69441875" x2="1.143" y2="9.779" layer="21"/>
<rectangle x1="3.937" y1="9.69441875" x2="4.953" y2="9.779" layer="21"/>
<rectangle x1="7.239" y1="9.69441875" x2="8.33881875" y2="9.779" layer="21"/>
<rectangle x1="11.13281875" y1="9.69441875" x2="12.23518125" y2="9.779" layer="21"/>
<rectangle x1="13.589" y1="9.69441875" x2="14.77518125" y2="9.779" layer="21"/>
<rectangle x1="15.79118125" y1="9.69441875" x2="18.161" y2="9.779" layer="21"/>
<rectangle x1="19.09318125" y1="9.69441875" x2="19.939" y2="9.779" layer="21"/>
<rectangle x1="20.53081875" y1="9.69441875" x2="22.56281875" y2="9.779" layer="21"/>
<rectangle x1="23.241" y1="9.69441875" x2="23.66518125" y2="9.779" layer="21"/>
<rectangle x1="24.257" y1="9.69441875" x2="29.92881875" y2="9.779" layer="21"/>
<rectangle x1="0.21081875" y1="9.779" x2="1.143" y2="9.86358125" layer="21"/>
<rectangle x1="3.85318125" y1="9.779" x2="4.86918125" y2="9.86358125" layer="21"/>
<rectangle x1="7.32281875" y1="9.779" x2="8.33881875" y2="9.86358125" layer="21"/>
<rectangle x1="11.21918125" y1="9.779" x2="12.23518125" y2="9.86358125" layer="21"/>
<rectangle x1="13.589" y1="9.779" x2="14.77518125" y2="9.86358125" layer="21"/>
<rectangle x1="15.70481875" y1="9.779" x2="18.24481875" y2="9.86358125" layer="21"/>
<rectangle x1="19.177" y1="9.779" x2="19.939" y2="9.86358125" layer="21"/>
<rectangle x1="20.53081875" y1="9.779" x2="22.56281875" y2="9.86358125" layer="21"/>
<rectangle x1="23.241" y1="9.779" x2="23.66518125" y2="9.86358125" layer="21"/>
<rectangle x1="24.34081875" y1="9.779" x2="29.92881875" y2="9.86358125" layer="21"/>
<rectangle x1="0.21081875" y1="9.86358125" x2="1.05918125" y2="9.94841875" layer="21"/>
<rectangle x1="3.85318125" y1="9.86358125" x2="4.86918125" y2="9.94841875" layer="21"/>
<rectangle x1="7.40918125" y1="9.86358125" x2="8.42518125" y2="9.94841875" layer="21"/>
<rectangle x1="11.303" y1="9.86358125" x2="12.319" y2="9.94841875" layer="21"/>
<rectangle x1="13.589" y1="9.86358125" x2="14.68881875" y2="9.94841875" layer="21"/>
<rectangle x1="15.621" y1="9.86358125" x2="18.33118125" y2="9.94841875" layer="21"/>
<rectangle x1="19.177" y1="9.86358125" x2="19.939" y2="9.94841875" layer="21"/>
<rectangle x1="20.53081875" y1="9.86358125" x2="22.56281875" y2="9.94841875" layer="21"/>
<rectangle x1="23.241" y1="9.86358125" x2="23.66518125" y2="9.94841875" layer="21"/>
<rectangle x1="26.71318125" y1="9.86358125" x2="29.92881875" y2="9.94841875" layer="21"/>
<rectangle x1="0.21081875" y1="9.94841875" x2="1.05918125" y2="10.033" layer="21"/>
<rectangle x1="3.76681875" y1="9.94841875" x2="4.78281875" y2="10.033" layer="21"/>
<rectangle x1="7.493" y1="9.94841875" x2="8.42518125" y2="10.033" layer="21"/>
<rectangle x1="11.38681875" y1="9.94841875" x2="12.319" y2="10.033" layer="21"/>
<rectangle x1="13.589" y1="9.94841875" x2="14.68881875" y2="10.033" layer="21"/>
<rectangle x1="15.53718125" y1="9.94841875" x2="18.33118125" y2="10.033" layer="21"/>
<rectangle x1="19.26081875" y1="9.94841875" x2="19.939" y2="10.033" layer="21"/>
<rectangle x1="20.53081875" y1="9.94841875" x2="22.56281875" y2="10.033" layer="21"/>
<rectangle x1="23.241" y1="9.94841875" x2="23.57881875" y2="10.033" layer="21"/>
<rectangle x1="26.797" y1="9.94841875" x2="29.92881875" y2="10.033" layer="21"/>
<rectangle x1="0.21081875" y1="10.033" x2="1.05918125" y2="10.11758125" layer="21"/>
<rectangle x1="3.76681875" y1="10.033" x2="4.699" y2="10.11758125" layer="21"/>
<rectangle x1="7.493" y1="10.033" x2="8.509" y2="10.11758125" layer="21"/>
<rectangle x1="11.38681875" y1="10.033" x2="12.40281875" y2="10.11758125" layer="21"/>
<rectangle x1="13.589" y1="10.033" x2="14.68881875" y2="10.11758125" layer="21"/>
<rectangle x1="15.53718125" y1="10.033" x2="18.415" y2="10.11758125" layer="21"/>
<rectangle x1="19.26081875" y1="10.033" x2="19.939" y2="10.11758125" layer="21"/>
<rectangle x1="20.53081875" y1="10.033" x2="22.56281875" y2="10.11758125" layer="21"/>
<rectangle x1="23.241" y1="10.033" x2="23.57881875" y2="10.11758125" layer="21"/>
<rectangle x1="26.88081875" y1="10.033" x2="29.92881875" y2="10.11758125" layer="21"/>
<rectangle x1="0.127" y1="10.11758125" x2="1.05918125" y2="10.20241875" layer="21"/>
<rectangle x1="3.76681875" y1="10.11758125" x2="4.699" y2="10.20241875" layer="21"/>
<rectangle x1="7.57681875" y1="10.11758125" x2="8.509" y2="10.20241875" layer="21"/>
<rectangle x1="11.47318125" y1="10.11758125" x2="12.40281875" y2="10.20241875" layer="21"/>
<rectangle x1="13.589" y1="10.11758125" x2="14.605" y2="10.20241875" layer="21"/>
<rectangle x1="15.45081875" y1="10.11758125" x2="18.49881875" y2="10.20241875" layer="21"/>
<rectangle x1="19.34718125" y1="10.11758125" x2="19.939" y2="10.20241875" layer="21"/>
<rectangle x1="20.53081875" y1="10.11758125" x2="22.56281875" y2="10.20241875" layer="21"/>
<rectangle x1="23.241" y1="10.11758125" x2="23.57881875" y2="10.20241875" layer="21"/>
<rectangle x1="26.88081875" y1="10.11758125" x2="29.92881875" y2="10.20241875" layer="21"/>
<rectangle x1="0.127" y1="10.20241875" x2="1.05918125" y2="10.287" layer="21"/>
<rectangle x1="3.683" y1="10.20241875" x2="4.61518125" y2="10.287" layer="21"/>
<rectangle x1="7.57681875" y1="10.20241875" x2="8.509" y2="10.287" layer="21"/>
<rectangle x1="11.557" y1="10.20241875" x2="12.40281875" y2="10.287" layer="21"/>
<rectangle x1="13.50518125" y1="10.20241875" x2="14.605" y2="10.287" layer="21"/>
<rectangle x1="15.45081875" y1="10.20241875" x2="18.49881875" y2="10.287" layer="21"/>
<rectangle x1="19.34718125" y1="10.20241875" x2="19.939" y2="10.287" layer="21"/>
<rectangle x1="20.53081875" y1="10.20241875" x2="22.56281875" y2="10.287" layer="21"/>
<rectangle x1="23.241" y1="10.20241875" x2="23.57881875" y2="10.287" layer="21"/>
<rectangle x1="26.88081875" y1="10.20241875" x2="29.92881875" y2="10.287" layer="21"/>
<rectangle x1="0.127" y1="10.287" x2="0.97281875" y2="10.37158125" layer="21"/>
<rectangle x1="3.683" y1="10.287" x2="4.61518125" y2="10.37158125" layer="21"/>
<rectangle x1="7.66318125" y1="10.287" x2="8.509" y2="10.37158125" layer="21"/>
<rectangle x1="11.557" y1="10.287" x2="12.48918125" y2="10.37158125" layer="21"/>
<rectangle x1="13.50518125" y1="10.287" x2="14.605" y2="10.37158125" layer="21"/>
<rectangle x1="15.367" y1="10.287" x2="18.49881875" y2="10.37158125" layer="21"/>
<rectangle x1="19.34718125" y1="10.287" x2="19.939" y2="10.37158125" layer="21"/>
<rectangle x1="20.53081875" y1="10.287" x2="22.56281875" y2="10.37158125" layer="21"/>
<rectangle x1="23.241" y1="10.287" x2="23.57881875" y2="10.37158125" layer="21"/>
<rectangle x1="26.88081875" y1="10.287" x2="29.92881875" y2="10.37158125" layer="21"/>
<rectangle x1="0.127" y1="10.37158125" x2="0.97281875" y2="10.45641875" layer="21"/>
<rectangle x1="3.683" y1="10.37158125" x2="4.61518125" y2="10.45641875" layer="21"/>
<rectangle x1="7.66318125" y1="10.37158125" x2="8.59281875" y2="10.45641875" layer="21"/>
<rectangle x1="11.557" y1="10.37158125" x2="12.48918125" y2="10.45641875" layer="21"/>
<rectangle x1="13.50518125" y1="10.37158125" x2="14.605" y2="10.45641875" layer="21"/>
<rectangle x1="15.367" y1="10.37158125" x2="18.58518125" y2="10.45641875" layer="21"/>
<rectangle x1="19.34718125" y1="10.37158125" x2="19.939" y2="10.45641875" layer="21"/>
<rectangle x1="20.61718125" y1="10.37158125" x2="22.56281875" y2="10.45641875" layer="21"/>
<rectangle x1="23.15718125" y1="10.37158125" x2="23.66518125" y2="10.45641875" layer="21"/>
<rectangle x1="26.88081875" y1="10.37158125" x2="29.92881875" y2="10.45641875" layer="21"/>
<rectangle x1="0.127" y1="10.45641875" x2="0.97281875" y2="10.541" layer="21"/>
<rectangle x1="3.683" y1="10.45641875" x2="4.52881875" y2="10.541" layer="21"/>
<rectangle x1="7.747" y1="10.45641875" x2="8.59281875" y2="10.541" layer="21"/>
<rectangle x1="11.64081875" y1="10.45641875" x2="12.48918125" y2="10.541" layer="21"/>
<rectangle x1="13.50518125" y1="10.45641875" x2="14.52118125" y2="10.541" layer="21"/>
<rectangle x1="15.367" y1="10.45641875" x2="18.58518125" y2="10.541" layer="21"/>
<rectangle x1="19.34718125" y1="10.45641875" x2="19.939" y2="10.541" layer="21"/>
<rectangle x1="20.61718125" y1="10.45641875" x2="22.56281875" y2="10.541" layer="21"/>
<rectangle x1="23.15718125" y1="10.45641875" x2="23.66518125" y2="10.541" layer="21"/>
<rectangle x1="24.34081875" y1="10.45641875" x2="26.20518125" y2="10.541" layer="21"/>
<rectangle x1="26.797" y1="10.45641875" x2="29.92881875" y2="10.541" layer="21"/>
<rectangle x1="0.127" y1="10.541" x2="0.97281875" y2="10.62558125" layer="21"/>
<rectangle x1="3.683" y1="10.541" x2="4.52881875" y2="10.62558125" layer="21"/>
<rectangle x1="7.747" y1="10.541" x2="8.59281875" y2="10.62558125" layer="21"/>
<rectangle x1="11.64081875" y1="10.541" x2="12.48918125" y2="10.62558125" layer="21"/>
<rectangle x1="13.50518125" y1="10.541" x2="14.52118125" y2="10.62558125" layer="21"/>
<rectangle x1="15.28318125" y1="10.541" x2="18.58518125" y2="10.62558125" layer="21"/>
<rectangle x1="19.431" y1="10.541" x2="19.939" y2="10.62558125" layer="21"/>
<rectangle x1="20.61718125" y1="10.541" x2="22.479" y2="10.62558125" layer="21"/>
<rectangle x1="23.15718125" y1="10.541" x2="23.66518125" y2="10.62558125" layer="21"/>
<rectangle x1="24.34081875" y1="10.541" x2="26.20518125" y2="10.62558125" layer="21"/>
<rectangle x1="26.797" y1="10.541" x2="29.92881875" y2="10.62558125" layer="21"/>
<rectangle x1="0.127" y1="10.62558125" x2="0.97281875" y2="10.71041875" layer="21"/>
<rectangle x1="3.59918125" y1="10.62558125" x2="4.52881875" y2="10.71041875" layer="21"/>
<rectangle x1="7.747" y1="10.62558125" x2="8.59281875" y2="10.71041875" layer="21"/>
<rectangle x1="11.64081875" y1="10.62558125" x2="12.48918125" y2="10.71041875" layer="21"/>
<rectangle x1="13.50518125" y1="10.62558125" x2="14.52118125" y2="10.71041875" layer="21"/>
<rectangle x1="15.28318125" y1="10.62558125" x2="18.669" y2="10.71041875" layer="21"/>
<rectangle x1="19.431" y1="10.62558125" x2="19.939" y2="10.71041875" layer="21"/>
<rectangle x1="20.701" y1="10.62558125" x2="22.479" y2="10.71041875" layer="21"/>
<rectangle x1="23.15718125" y1="10.62558125" x2="23.66518125" y2="10.71041875" layer="21"/>
<rectangle x1="24.34081875" y1="10.62558125" x2="26.11881875" y2="10.71041875" layer="21"/>
<rectangle x1="26.797" y1="10.62558125" x2="29.92881875" y2="10.71041875" layer="21"/>
<rectangle x1="0.127" y1="10.71041875" x2="0.97281875" y2="10.795" layer="21"/>
<rectangle x1="3.59918125" y1="10.71041875" x2="4.52881875" y2="10.795" layer="21"/>
<rectangle x1="7.747" y1="10.71041875" x2="8.59281875" y2="10.795" layer="21"/>
<rectangle x1="11.64081875" y1="10.71041875" x2="12.48918125" y2="10.795" layer="21"/>
<rectangle x1="13.50518125" y1="10.71041875" x2="14.52118125" y2="10.795" layer="21"/>
<rectangle x1="15.28318125" y1="10.71041875" x2="18.669" y2="10.795" layer="21"/>
<rectangle x1="19.431" y1="10.71041875" x2="19.939" y2="10.795" layer="21"/>
<rectangle x1="20.78481875" y1="10.71041875" x2="22.39518125" y2="10.795" layer="21"/>
<rectangle x1="23.07081875" y1="10.71041875" x2="23.749" y2="10.795" layer="21"/>
<rectangle x1="24.42718125" y1="10.71041875" x2="26.035" y2="10.795" layer="21"/>
<rectangle x1="26.797" y1="10.71041875" x2="29.92881875" y2="10.795" layer="21"/>
<rectangle x1="0.127" y1="10.795" x2="0.97281875" y2="10.87958125" layer="21"/>
<rectangle x1="3.59918125" y1="10.795" x2="4.52881875" y2="10.87958125" layer="21"/>
<rectangle x1="7.747" y1="10.795" x2="8.59281875" y2="10.87958125" layer="21"/>
<rectangle x1="11.64081875" y1="10.795" x2="12.48918125" y2="10.87958125" layer="21"/>
<rectangle x1="13.50518125" y1="10.795" x2="14.52118125" y2="10.87958125" layer="21"/>
<rectangle x1="15.28318125" y1="10.795" x2="18.669" y2="10.87958125" layer="21"/>
<rectangle x1="19.431" y1="10.795" x2="19.939" y2="10.87958125" layer="21"/>
<rectangle x1="20.78481875" y1="10.795" x2="22.30881875" y2="10.87958125" layer="21"/>
<rectangle x1="23.07081875" y1="10.795" x2="23.749" y2="10.87958125" layer="21"/>
<rectangle x1="24.511" y1="10.795" x2="26.035" y2="10.87958125" layer="21"/>
<rectangle x1="26.71318125" y1="10.795" x2="29.92881875" y2="10.87958125" layer="21"/>
<rectangle x1="0.127" y1="10.87958125" x2="0.97281875" y2="10.96441875" layer="21"/>
<rectangle x1="3.59918125" y1="10.87958125" x2="4.52881875" y2="10.96441875" layer="21"/>
<rectangle x1="7.747" y1="10.87958125" x2="8.59281875" y2="10.96441875" layer="21"/>
<rectangle x1="11.64081875" y1="10.87958125" x2="12.48918125" y2="10.96441875" layer="21"/>
<rectangle x1="13.50518125" y1="10.87958125" x2="14.52118125" y2="10.96441875" layer="21"/>
<rectangle x1="15.28318125" y1="10.87958125" x2="18.669" y2="10.96441875" layer="21"/>
<rectangle x1="19.431" y1="10.87958125" x2="19.939" y2="10.96441875" layer="21"/>
<rectangle x1="20.87118125" y1="10.87958125" x2="22.225" y2="10.96441875" layer="21"/>
<rectangle x1="22.987" y1="10.87958125" x2="23.83281875" y2="10.96441875" layer="21"/>
<rectangle x1="24.59481875" y1="10.87958125" x2="25.95118125" y2="10.96441875" layer="21"/>
<rectangle x1="26.71318125" y1="10.87958125" x2="29.92881875" y2="10.96441875" layer="21"/>
<rectangle x1="0.127" y1="10.96441875" x2="0.97281875" y2="11.049" layer="21"/>
<rectangle x1="3.59918125" y1="10.96441875" x2="4.52881875" y2="11.049" layer="21"/>
<rectangle x1="7.747" y1="10.96441875" x2="8.59281875" y2="11.049" layer="21"/>
<rectangle x1="11.64081875" y1="10.96441875" x2="12.48918125" y2="11.049" layer="21"/>
<rectangle x1="13.50518125" y1="10.96441875" x2="14.52118125" y2="11.049" layer="21"/>
<rectangle x1="15.28318125" y1="10.96441875" x2="18.669" y2="11.049" layer="21"/>
<rectangle x1="19.431" y1="10.96441875" x2="19.939" y2="11.049" layer="21"/>
<rectangle x1="21.03881875" y1="10.96441875" x2="22.14118125" y2="11.049" layer="21"/>
<rectangle x1="22.987" y1="10.96441875" x2="23.83281875" y2="11.049" layer="21"/>
<rectangle x1="24.68118125" y1="10.96441875" x2="25.781" y2="11.049" layer="21"/>
<rectangle x1="26.62681875" y1="10.96441875" x2="29.92881875" y2="11.049" layer="21"/>
<rectangle x1="0.127" y1="11.049" x2="0.97281875" y2="11.13358125" layer="21"/>
<rectangle x1="3.59918125" y1="11.049" x2="4.52881875" y2="11.13358125" layer="21"/>
<rectangle x1="7.747" y1="11.049" x2="8.59281875" y2="11.13358125" layer="21"/>
<rectangle x1="11.64081875" y1="11.049" x2="12.48918125" y2="11.13358125" layer="21"/>
<rectangle x1="13.50518125" y1="11.049" x2="14.52118125" y2="11.13358125" layer="21"/>
<rectangle x1="15.28318125" y1="11.049" x2="18.669" y2="11.13358125" layer="21"/>
<rectangle x1="19.431" y1="11.049" x2="19.939" y2="11.13358125" layer="21"/>
<rectangle x1="21.209" y1="11.049" x2="21.971" y2="11.13358125" layer="21"/>
<rectangle x1="22.90318125" y1="11.049" x2="23.91918125" y2="11.13358125" layer="21"/>
<rectangle x1="24.84881875" y1="11.049" x2="25.61081875" y2="11.13358125" layer="21"/>
<rectangle x1="26.543" y1="11.049" x2="29.92881875" y2="11.13358125" layer="21"/>
<rectangle x1="0.127" y1="11.13358125" x2="0.97281875" y2="11.21841875" layer="21"/>
<rectangle x1="3.59918125" y1="11.13358125" x2="4.52881875" y2="11.21841875" layer="21"/>
<rectangle x1="7.747" y1="11.13358125" x2="8.59281875" y2="11.21841875" layer="21"/>
<rectangle x1="11.64081875" y1="11.13358125" x2="12.48918125" y2="11.21841875" layer="21"/>
<rectangle x1="13.50518125" y1="11.13358125" x2="14.52118125" y2="11.21841875" layer="21"/>
<rectangle x1="15.28318125" y1="11.13358125" x2="18.58518125" y2="11.21841875" layer="21"/>
<rectangle x1="19.431" y1="11.13358125" x2="19.939" y2="11.21841875" layer="21"/>
<rectangle x1="21.463" y1="11.13358125" x2="21.63318125" y2="11.21841875" layer="21"/>
<rectangle x1="22.81681875" y1="11.13358125" x2="24.003" y2="11.21841875" layer="21"/>
<rectangle x1="25.18918125" y1="11.13358125" x2="25.273" y2="11.21841875" layer="21"/>
<rectangle x1="26.543" y1="11.13358125" x2="29.92881875" y2="11.21841875" layer="21"/>
<rectangle x1="0.127" y1="11.21841875" x2="0.97281875" y2="11.303" layer="21"/>
<rectangle x1="3.59918125" y1="11.21841875" x2="4.52881875" y2="11.303" layer="21"/>
<rectangle x1="7.747" y1="11.21841875" x2="8.59281875" y2="11.303" layer="21"/>
<rectangle x1="11.64081875" y1="11.21841875" x2="12.48918125" y2="11.303" layer="21"/>
<rectangle x1="13.50518125" y1="11.21841875" x2="14.52118125" y2="11.303" layer="21"/>
<rectangle x1="15.28318125" y1="11.21841875" x2="18.58518125" y2="11.303" layer="21"/>
<rectangle x1="19.34718125" y1="11.21841875" x2="19.939" y2="11.303" layer="21"/>
<rectangle x1="22.81681875" y1="11.21841875" x2="24.003" y2="11.303" layer="21"/>
<rectangle x1="26.45918125" y1="11.21841875" x2="29.92881875" y2="11.303" layer="21"/>
<rectangle x1="0.127" y1="11.303" x2="0.97281875" y2="11.38758125" layer="21"/>
<rectangle x1="3.59918125" y1="11.303" x2="4.52881875" y2="11.38758125" layer="21"/>
<rectangle x1="7.747" y1="11.303" x2="8.59281875" y2="11.38758125" layer="21"/>
<rectangle x1="11.557" y1="11.303" x2="12.48918125" y2="11.38758125" layer="21"/>
<rectangle x1="13.50518125" y1="11.303" x2="14.52118125" y2="11.38758125" layer="21"/>
<rectangle x1="15.367" y1="11.303" x2="18.58518125" y2="11.38758125" layer="21"/>
<rectangle x1="19.34718125" y1="11.303" x2="19.939" y2="11.38758125" layer="21"/>
<rectangle x1="22.64918125" y1="11.303" x2="24.08681875" y2="11.38758125" layer="21"/>
<rectangle x1="26.37281875" y1="11.303" x2="29.92881875" y2="11.38758125" layer="21"/>
<rectangle x1="0.127" y1="11.38758125" x2="0.97281875" y2="11.47241875" layer="21"/>
<rectangle x1="3.59918125" y1="11.38758125" x2="4.52881875" y2="11.47241875" layer="21"/>
<rectangle x1="7.747" y1="11.38758125" x2="8.59281875" y2="11.47241875" layer="21"/>
<rectangle x1="11.557" y1="11.38758125" x2="12.48918125" y2="11.47241875" layer="21"/>
<rectangle x1="13.50518125" y1="11.38758125" x2="14.605" y2="11.47241875" layer="21"/>
<rectangle x1="15.367" y1="11.38758125" x2="18.58518125" y2="11.47241875" layer="21"/>
<rectangle x1="19.34718125" y1="11.38758125" x2="19.939" y2="11.47241875" layer="21"/>
<rectangle x1="22.56281875" y1="11.38758125" x2="24.257" y2="11.47241875" layer="21"/>
<rectangle x1="26.289" y1="11.38758125" x2="29.92881875" y2="11.47241875" layer="21"/>
<rectangle x1="0.127" y1="11.47241875" x2="1.05918125" y2="11.557" layer="21"/>
<rectangle x1="3.59918125" y1="11.47241875" x2="4.52881875" y2="11.557" layer="21"/>
<rectangle x1="7.747" y1="11.47241875" x2="8.59281875" y2="11.557" layer="21"/>
<rectangle x1="11.557" y1="11.47241875" x2="12.40281875" y2="11.557" layer="21"/>
<rectangle x1="13.50518125" y1="11.47241875" x2="14.605" y2="11.557" layer="21"/>
<rectangle x1="15.45081875" y1="11.47241875" x2="18.49881875" y2="11.557" layer="21"/>
<rectangle x1="19.34718125" y1="11.47241875" x2="19.939" y2="11.557" layer="21"/>
<rectangle x1="20.53081875" y1="11.47241875" x2="20.701" y2="11.557" layer="21"/>
<rectangle x1="22.479" y1="11.47241875" x2="24.34081875" y2="11.557" layer="21"/>
<rectangle x1="26.11881875" y1="11.47241875" x2="29.92881875" y2="11.557" layer="21"/>
<rectangle x1="0.127" y1="11.557" x2="1.05918125" y2="11.64158125" layer="21"/>
<rectangle x1="3.59918125" y1="11.557" x2="4.52881875" y2="11.64158125" layer="21"/>
<rectangle x1="7.747" y1="11.557" x2="8.59281875" y2="11.64158125" layer="21"/>
<rectangle x1="11.47318125" y1="11.557" x2="12.40281875" y2="11.64158125" layer="21"/>
<rectangle x1="13.50518125" y1="11.557" x2="14.605" y2="11.64158125" layer="21"/>
<rectangle x1="15.45081875" y1="11.557" x2="18.49881875" y2="11.64158125" layer="21"/>
<rectangle x1="19.34718125" y1="11.557" x2="19.939" y2="11.64158125" layer="21"/>
<rectangle x1="20.53081875" y1="11.557" x2="20.78481875" y2="11.64158125" layer="21"/>
<rectangle x1="22.30881875" y1="11.557" x2="24.511" y2="11.64158125" layer="21"/>
<rectangle x1="26.035" y1="11.557" x2="29.92881875" y2="11.64158125" layer="21"/>
<rectangle x1="0.21081875" y1="11.64158125" x2="1.05918125" y2="11.72641875" layer="21"/>
<rectangle x1="3.59918125" y1="11.64158125" x2="4.52881875" y2="11.72641875" layer="21"/>
<rectangle x1="7.747" y1="11.64158125" x2="8.59281875" y2="11.72641875" layer="21"/>
<rectangle x1="11.47318125" y1="11.64158125" x2="12.40281875" y2="11.72641875" layer="21"/>
<rectangle x1="13.589" y1="11.64158125" x2="14.605" y2="11.72641875" layer="21"/>
<rectangle x1="15.45081875" y1="11.64158125" x2="18.415" y2="11.72641875" layer="21"/>
<rectangle x1="19.26081875" y1="11.64158125" x2="20.02281875" y2="11.72641875" layer="21"/>
<rectangle x1="20.447" y1="11.64158125" x2="21.03881875" y2="11.72641875" layer="21"/>
<rectangle x1="22.14118125" y1="11.64158125" x2="24.68118125" y2="11.72641875" layer="21"/>
<rectangle x1="25.781" y1="11.64158125" x2="29.92881875" y2="11.72641875" layer="21"/>
<rectangle x1="0.21081875" y1="11.72641875" x2="1.05918125" y2="11.811" layer="21"/>
<rectangle x1="3.59918125" y1="11.72641875" x2="4.52881875" y2="11.811" layer="21"/>
<rectangle x1="7.747" y1="11.72641875" x2="8.59281875" y2="11.811" layer="21"/>
<rectangle x1="11.38681875" y1="11.72641875" x2="12.40281875" y2="11.811" layer="21"/>
<rectangle x1="13.589" y1="11.72641875" x2="14.68881875" y2="11.811" layer="21"/>
<rectangle x1="15.53718125" y1="11.72641875" x2="18.415" y2="11.811" layer="21"/>
<rectangle x1="19.26081875" y1="11.72641875" x2="20.10918125" y2="11.811" layer="21"/>
<rectangle x1="20.36318125" y1="11.72641875" x2="21.29281875" y2="11.811" layer="21"/>
<rectangle x1="21.88718125" y1="11.72641875" x2="24.93518125" y2="11.811" layer="21"/>
<rectangle x1="25.527" y1="11.72641875" x2="29.92881875" y2="11.811" layer="21"/>
<rectangle x1="0.21081875" y1="11.811" x2="1.05918125" y2="11.89558125" layer="21"/>
<rectangle x1="3.59918125" y1="11.811" x2="4.52881875" y2="11.89558125" layer="21"/>
<rectangle x1="7.747" y1="11.811" x2="8.59281875" y2="11.89558125" layer="21"/>
<rectangle x1="11.38681875" y1="11.811" x2="12.319" y2="11.89558125" layer="21"/>
<rectangle x1="13.589" y1="11.811" x2="14.68881875" y2="11.89558125" layer="21"/>
<rectangle x1="15.621" y1="11.811" x2="18.33118125" y2="11.89558125" layer="21"/>
<rectangle x1="19.26081875" y1="11.811" x2="29.92881875" y2="11.89558125" layer="21"/>
<rectangle x1="0.21081875" y1="11.89558125" x2="1.05918125" y2="11.98041875" layer="21"/>
<rectangle x1="3.59918125" y1="11.89558125" x2="4.52881875" y2="11.98041875" layer="21"/>
<rectangle x1="7.747" y1="11.89558125" x2="8.59281875" y2="11.98041875" layer="21"/>
<rectangle x1="11.303" y1="11.89558125" x2="12.319" y2="11.98041875" layer="21"/>
<rectangle x1="13.589" y1="11.89558125" x2="14.77518125" y2="11.98041875" layer="21"/>
<rectangle x1="15.621" y1="11.89558125" x2="18.24481875" y2="11.98041875" layer="21"/>
<rectangle x1="19.177" y1="11.89558125" x2="29.92881875" y2="11.98041875" layer="21"/>
<rectangle x1="0.21081875" y1="11.98041875" x2="1.143" y2="12.065" layer="21"/>
<rectangle x1="3.59918125" y1="11.98041875" x2="4.52881875" y2="12.065" layer="21"/>
<rectangle x1="7.747" y1="11.98041875" x2="8.59281875" y2="12.065" layer="21"/>
<rectangle x1="11.21918125" y1="11.98041875" x2="12.23518125" y2="12.065" layer="21"/>
<rectangle x1="13.589" y1="11.98041875" x2="14.77518125" y2="12.065" layer="21"/>
<rectangle x1="15.70481875" y1="11.98041875" x2="18.161" y2="12.065" layer="21"/>
<rectangle x1="19.177" y1="11.98041875" x2="29.92881875" y2="12.065" layer="21"/>
<rectangle x1="0.21081875" y1="12.065" x2="1.143" y2="12.14958125" layer="21"/>
<rectangle x1="3.59918125" y1="12.065" x2="4.52881875" y2="12.14958125" layer="21"/>
<rectangle x1="7.747" y1="12.065" x2="8.59281875" y2="12.14958125" layer="21"/>
<rectangle x1="11.13281875" y1="12.065" x2="12.23518125" y2="12.14958125" layer="21"/>
<rectangle x1="13.589" y1="12.065" x2="14.859" y2="12.14958125" layer="21"/>
<rectangle x1="15.79118125" y1="12.065" x2="18.07718125" y2="12.14958125" layer="21"/>
<rectangle x1="19.09318125" y1="12.065" x2="29.92881875" y2="12.14958125" layer="21"/>
<rectangle x1="0.29718125" y1="12.14958125" x2="1.143" y2="12.23441875" layer="21"/>
<rectangle x1="3.59918125" y1="12.14958125" x2="4.52881875" y2="12.23441875" layer="21"/>
<rectangle x1="7.747" y1="12.14958125" x2="8.59281875" y2="12.23441875" layer="21"/>
<rectangle x1="11.049" y1="12.14958125" x2="12.14881875" y2="12.23441875" layer="21"/>
<rectangle x1="13.589" y1="12.14958125" x2="14.859" y2="12.23441875" layer="21"/>
<rectangle x1="15.875" y1="12.14958125" x2="17.99081875" y2="12.23441875" layer="21"/>
<rectangle x1="19.09318125" y1="12.14958125" x2="29.92881875" y2="12.23441875" layer="21"/>
<rectangle x1="0.29718125" y1="12.23441875" x2="1.143" y2="12.319" layer="21"/>
<rectangle x1="3.59918125" y1="12.23441875" x2="4.52881875" y2="12.319" layer="21"/>
<rectangle x1="7.747" y1="12.23441875" x2="8.59281875" y2="12.319" layer="21"/>
<rectangle x1="10.87881875" y1="12.23441875" x2="12.065" y2="12.319" layer="21"/>
<rectangle x1="13.589" y1="12.23441875" x2="14.94281875" y2="12.319" layer="21"/>
<rectangle x1="16.04518125" y1="12.23441875" x2="17.907" y2="12.319" layer="21"/>
<rectangle x1="19.00681875" y1="12.23441875" x2="29.92881875" y2="12.319" layer="21"/>
<rectangle x1="0.29718125" y1="12.319" x2="1.22681875" y2="12.40358125" layer="21"/>
<rectangle x1="3.59918125" y1="12.319" x2="4.52881875" y2="12.40358125" layer="21"/>
<rectangle x1="7.747" y1="12.319" x2="8.59281875" y2="12.40358125" layer="21"/>
<rectangle x1="10.71118125" y1="12.319" x2="12.065" y2="12.40358125" layer="21"/>
<rectangle x1="13.589" y1="12.319" x2="15.02918125" y2="12.40358125" layer="21"/>
<rectangle x1="16.129" y1="12.319" x2="17.82318125" y2="12.40358125" layer="21"/>
<rectangle x1="18.923" y1="12.319" x2="29.92881875" y2="12.40358125" layer="21"/>
<rectangle x1="0.29718125" y1="12.40358125" x2="1.22681875" y2="12.48841875" layer="21"/>
<rectangle x1="3.59918125" y1="12.40358125" x2="4.52881875" y2="12.48841875" layer="21"/>
<rectangle x1="7.747" y1="12.40358125" x2="8.59281875" y2="12.48841875" layer="21"/>
<rectangle x1="10.541" y1="12.40358125" x2="11.98118125" y2="12.48841875" layer="21"/>
<rectangle x1="13.589" y1="12.40358125" x2="15.02918125" y2="12.48841875" layer="21"/>
<rectangle x1="16.29918125" y1="12.40358125" x2="17.653" y2="12.48841875" layer="21"/>
<rectangle x1="18.83918125" y1="12.40358125" x2="29.845" y2="12.48841875" layer="21"/>
<rectangle x1="0.381" y1="12.48841875" x2="1.22681875" y2="12.573" layer="21"/>
<rectangle x1="3.59918125" y1="12.48841875" x2="4.52881875" y2="12.573" layer="21"/>
<rectangle x1="7.747" y1="12.48841875" x2="11.89481875" y2="12.573" layer="21"/>
<rectangle x1="13.67281875" y1="12.48841875" x2="15.113" y2="12.573" layer="21"/>
<rectangle x1="16.55318125" y1="12.48841875" x2="17.31518125" y2="12.573" layer="21"/>
<rectangle x1="18.83918125" y1="12.48841875" x2="29.845" y2="12.573" layer="21"/>
<rectangle x1="0.381" y1="12.573" x2="1.31318125" y2="12.65758125" layer="21"/>
<rectangle x1="3.59918125" y1="12.573" x2="4.52881875" y2="12.65758125" layer="21"/>
<rectangle x1="7.747" y1="12.573" x2="11.811" y2="12.65758125" layer="21"/>
<rectangle x1="13.67281875" y1="12.573" x2="15.19681875" y2="12.65758125" layer="21"/>
<rectangle x1="18.75281875" y1="12.573" x2="29.845" y2="12.65758125" layer="21"/>
<rectangle x1="0.381" y1="12.65758125" x2="1.31318125" y2="12.74241875" layer="21"/>
<rectangle x1="3.59918125" y1="12.65758125" x2="4.52881875" y2="12.74241875" layer="21"/>
<rectangle x1="7.747" y1="12.65758125" x2="11.72718125" y2="12.74241875" layer="21"/>
<rectangle x1="13.67281875" y1="12.65758125" x2="15.28318125" y2="12.74241875" layer="21"/>
<rectangle x1="18.669" y1="12.65758125" x2="29.76118125" y2="12.74241875" layer="21"/>
<rectangle x1="0.381" y1="12.74241875" x2="1.31318125" y2="12.827" layer="21"/>
<rectangle x1="3.59918125" y1="12.74241875" x2="4.52881875" y2="12.827" layer="21"/>
<rectangle x1="7.747" y1="12.74241875" x2="11.64081875" y2="12.827" layer="21"/>
<rectangle x1="13.67281875" y1="12.74241875" x2="15.367" y2="12.827" layer="21"/>
<rectangle x1="18.49881875" y1="12.74241875" x2="29.76118125" y2="12.827" layer="21"/>
<rectangle x1="0.46481875" y1="12.827" x2="1.397" y2="12.91158125" layer="21"/>
<rectangle x1="3.59918125" y1="12.827" x2="4.52881875" y2="12.91158125" layer="21"/>
<rectangle x1="7.747" y1="12.827" x2="11.557" y2="12.91158125" layer="21"/>
<rectangle x1="13.67281875" y1="12.827" x2="15.53718125" y2="12.91158125" layer="21"/>
<rectangle x1="18.415" y1="12.827" x2="29.76118125" y2="12.91158125" layer="21"/>
<rectangle x1="0.46481875" y1="12.91158125" x2="1.397" y2="12.99641875" layer="21"/>
<rectangle x1="3.59918125" y1="12.91158125" x2="4.52881875" y2="12.99641875" layer="21"/>
<rectangle x1="7.747" y1="12.91158125" x2="11.38681875" y2="12.99641875" layer="21"/>
<rectangle x1="13.67281875" y1="12.91158125" x2="15.621" y2="12.99641875" layer="21"/>
<rectangle x1="18.33118125" y1="12.91158125" x2="29.67481875" y2="12.99641875" layer="21"/>
<rectangle x1="0.55118125" y1="12.99641875" x2="1.48081875" y2="13.081" layer="21"/>
<rectangle x1="3.59918125" y1="12.99641875" x2="4.445" y2="13.081" layer="21"/>
<rectangle x1="7.747" y1="12.99641875" x2="11.303" y2="13.081" layer="21"/>
<rectangle x1="13.67281875" y1="12.99641875" x2="15.79118125" y2="13.081" layer="21"/>
<rectangle x1="18.161" y1="12.99641875" x2="29.67481875" y2="13.081" layer="21"/>
<rectangle x1="0.55118125" y1="13.081" x2="1.48081875" y2="13.16558125" layer="21"/>
<rectangle x1="3.683" y1="13.081" x2="4.445" y2="13.16558125" layer="21"/>
<rectangle x1="7.83081875" y1="13.081" x2="11.13281875" y2="13.16558125" layer="21"/>
<rectangle x1="13.75918125" y1="13.081" x2="15.95881875" y2="13.16558125" layer="21"/>
<rectangle x1="17.99081875" y1="13.081" x2="29.67481875" y2="13.16558125" layer="21"/>
<rectangle x1="0.55118125" y1="13.16558125" x2="1.56718125" y2="13.25041875" layer="21"/>
<rectangle x1="3.683" y1="13.16558125" x2="4.445" y2="13.25041875" layer="21"/>
<rectangle x1="7.83081875" y1="13.16558125" x2="10.96518125" y2="13.25041875" layer="21"/>
<rectangle x1="13.75918125" y1="13.16558125" x2="16.129" y2="13.25041875" layer="21"/>
<rectangle x1="17.82318125" y1="13.16558125" x2="29.591" y2="13.25041875" layer="21"/>
<rectangle x1="0.635" y1="13.25041875" x2="1.56718125" y2="13.335" layer="21"/>
<rectangle x1="3.76681875" y1="13.25041875" x2="4.36118125" y2="13.335" layer="21"/>
<rectangle x1="7.91718125" y1="13.25041875" x2="10.71118125" y2="13.335" layer="21"/>
<rectangle x1="13.75918125" y1="13.25041875" x2="16.46681875" y2="13.335" layer="21"/>
<rectangle x1="17.48281875" y1="13.25041875" x2="29.591" y2="13.335" layer="21"/>
<rectangle x1="0.635" y1="13.335" x2="1.651" y2="13.41958125" layer="21"/>
<rectangle x1="3.937" y1="13.335" x2="4.191" y2="13.41958125" layer="21"/>
<rectangle x1="8.08481875" y1="13.335" x2="10.37081875" y2="13.41958125" layer="21"/>
<rectangle x1="13.75918125" y1="13.335" x2="29.50718125" y2="13.41958125" layer="21"/>
<rectangle x1="0.71881875" y1="13.41958125" x2="1.651" y2="13.50441875" layer="21"/>
<rectangle x1="13.75918125" y1="13.41958125" x2="29.50718125" y2="13.50441875" layer="21"/>
<rectangle x1="0.71881875" y1="13.50441875" x2="1.73481875" y2="13.589" layer="21"/>
<rectangle x1="13.843" y1="13.50441875" x2="29.42081875" y2="13.589" layer="21"/>
<rectangle x1="0.80518125" y1="13.589" x2="1.82118125" y2="13.67358125" layer="21"/>
<rectangle x1="13.843" y1="13.589" x2="29.42081875" y2="13.67358125" layer="21"/>
<rectangle x1="0.80518125" y1="13.67358125" x2="1.82118125" y2="13.75841875" layer="21"/>
<rectangle x1="13.843" y1="13.67358125" x2="29.337" y2="13.75841875" layer="21"/>
<rectangle x1="0.889" y1="13.75841875" x2="1.905" y2="13.843" layer="21"/>
<rectangle x1="13.843" y1="13.75841875" x2="29.337" y2="13.843" layer="21"/>
<rectangle x1="0.889" y1="13.843" x2="1.905" y2="13.92758125" layer="21"/>
<rectangle x1="13.92681875" y1="13.843" x2="29.25318125" y2="13.92758125" layer="21"/>
<rectangle x1="0.97281875" y1="13.92758125" x2="1.98881875" y2="14.01241875" layer="21"/>
<rectangle x1="13.92681875" y1="13.92758125" x2="29.25318125" y2="14.01241875" layer="21"/>
<rectangle x1="1.05918125" y1="14.01241875" x2="2.07518125" y2="14.097" layer="21"/>
<rectangle x1="13.92681875" y1="14.01241875" x2="29.16681875" y2="14.097" layer="21"/>
<rectangle x1="1.05918125" y1="14.097" x2="2.159" y2="14.18158125" layer="21"/>
<rectangle x1="13.92681875" y1="14.097" x2="29.083" y2="14.18158125" layer="21"/>
<rectangle x1="1.143" y1="14.18158125" x2="2.24281875" y2="14.26641875" layer="21"/>
<rectangle x1="13.92681875" y1="14.18158125" x2="29.083" y2="14.26641875" layer="21"/>
<rectangle x1="1.143" y1="14.26641875" x2="2.24281875" y2="14.351" layer="21"/>
<rectangle x1="14.01318125" y1="14.26641875" x2="28.99918125" y2="14.351" layer="21"/>
<rectangle x1="1.22681875" y1="14.351" x2="2.32918125" y2="14.43558125" layer="21"/>
<rectangle x1="14.01318125" y1="14.351" x2="28.91281875" y2="14.43558125" layer="21"/>
<rectangle x1="1.31318125" y1="14.43558125" x2="2.413" y2="14.52041875" layer="21"/>
<rectangle x1="14.01318125" y1="14.43558125" x2="28.91281875" y2="14.52041875" layer="21"/>
<rectangle x1="1.397" y1="14.52041875" x2="2.49681875" y2="14.605" layer="21"/>
<rectangle x1="14.097" y1="14.52041875" x2="28.829" y2="14.605" layer="21"/>
<rectangle x1="1.48081875" y1="14.605" x2="2.58318125" y2="14.68958125" layer="21"/>
<rectangle x1="14.097" y1="14.605" x2="28.74518125" y2="14.68958125" layer="21"/>
<rectangle x1="1.48081875" y1="14.68958125" x2="2.667" y2="14.77441875" layer="21"/>
<rectangle x1="14.18081875" y1="14.68958125" x2="28.65881875" y2="14.77441875" layer="21"/>
<rectangle x1="1.56718125" y1="14.77441875" x2="2.83718125" y2="14.859" layer="21"/>
<rectangle x1="14.18081875" y1="14.77441875" x2="28.575" y2="14.859" layer="21"/>
<rectangle x1="1.651" y1="14.859" x2="2.921" y2="14.94358125" layer="21"/>
<rectangle x1="14.18081875" y1="14.859" x2="28.49118125" y2="14.94358125" layer="21"/>
<rectangle x1="1.73481875" y1="14.94358125" x2="3.00481875" y2="15.02841875" layer="21"/>
<rectangle x1="14.26718125" y1="14.94358125" x2="28.49118125" y2="15.02841875" layer="21"/>
<rectangle x1="1.82118125" y1="15.02841875" x2="3.09118125" y2="15.113" layer="21"/>
<rectangle x1="14.26718125" y1="15.02841875" x2="28.40481875" y2="15.113" layer="21"/>
<rectangle x1="1.905" y1="15.113" x2="3.25881875" y2="15.19758125" layer="21"/>
<rectangle x1="14.26718125" y1="15.113" x2="28.321" y2="15.19758125" layer="21"/>
<rectangle x1="1.98881875" y1="15.19758125" x2="3.34518125" y2="15.28241875" layer="21"/>
<rectangle x1="14.351" y1="15.19758125" x2="28.23718125" y2="15.28241875" layer="21"/>
<rectangle x1="2.07518125" y1="15.28241875" x2="3.51281875" y2="15.367" layer="21"/>
<rectangle x1="14.351" y1="15.28241875" x2="28.15081875" y2="15.367" layer="21"/>
<rectangle x1="2.159" y1="15.367" x2="3.683" y2="15.45158125" layer="21"/>
<rectangle x1="14.43481875" y1="15.367" x2="28.067" y2="15.45158125" layer="21"/>
<rectangle x1="2.24281875" y1="15.45158125" x2="3.76681875" y2="15.53641875" layer="21"/>
<rectangle x1="14.43481875" y1="15.45158125" x2="27.89681875" y2="15.53641875" layer="21"/>
<rectangle x1="2.32918125" y1="15.53641875" x2="4.02081875" y2="15.621" layer="21"/>
<rectangle x1="14.52118125" y1="15.53641875" x2="27.813" y2="15.621" layer="21"/>
<rectangle x1="2.49681875" y1="15.621" x2="4.191" y2="15.70558125" layer="21"/>
<rectangle x1="14.52118125" y1="15.621" x2="27.72918125" y2="15.70558125" layer="21"/>
<rectangle x1="2.58318125" y1="15.70558125" x2="4.445" y2="15.79041875" layer="21"/>
<rectangle x1="14.605" y1="15.70558125" x2="27.64281875" y2="15.79041875" layer="21"/>
<rectangle x1="2.75081875" y1="15.79041875" x2="4.61518125" y2="15.875" layer="21"/>
<rectangle x1="14.68881875" y1="15.79041875" x2="27.47518125" y2="15.875" layer="21"/>
<rectangle x1="2.83718125" y1="15.875" x2="5.03681875" y2="15.95958125" layer="21"/>
<rectangle x1="14.68881875" y1="15.875" x2="27.38881875" y2="15.95958125" layer="21"/>
<rectangle x1="3.00481875" y1="15.95958125" x2="5.461" y2="16.04441875" layer="21"/>
<rectangle x1="14.77518125" y1="15.95958125" x2="27.22118125" y2="16.04441875" layer="21"/>
<rectangle x1="3.09118125" y1="16.04441875" x2="27.13481875" y2="16.129" layer="21"/>
<rectangle x1="3.25881875" y1="16.129" x2="26.96718125" y2="16.21358125" layer="21"/>
<rectangle x1="3.429" y1="16.21358125" x2="26.797" y2="16.29841875" layer="21"/>
<rectangle x1="3.59918125" y1="16.29841875" x2="26.62681875" y2="16.383" layer="21"/>
<rectangle x1="3.76681875" y1="16.383" x2="26.45918125" y2="16.46758125" layer="21"/>
<rectangle x1="3.937" y1="16.46758125" x2="26.20518125" y2="16.55241875" layer="21"/>
<rectangle x1="4.191" y1="16.55241875" x2="26.035" y2="16.637" layer="21"/>
<rectangle x1="4.445" y1="16.637" x2="25.69718125" y2="16.72158125" layer="21"/>
<rectangle x1="4.78281875" y1="16.72158125" x2="25.35681875" y2="16.80641875" layer="21"/>
<rectangle x1="5.207" y1="16.80641875" x2="25.019" y2="16.891" layer="21"/>
</package>
<package name="UDO-LOGO-40MM">
<rectangle x1="5.969" y1="0.71958125" x2="6.81481875" y2="0.80441875" layer="21"/>
<rectangle x1="28.321" y1="0.71958125" x2="29.083" y2="0.80441875" layer="21"/>
<rectangle x1="5.715" y1="0.80441875" x2="7.06881875" y2="0.889" layer="21"/>
<rectangle x1="28.067" y1="0.80441875" x2="29.42081875" y2="0.889" layer="21"/>
<rectangle x1="5.54481875" y1="0.889" x2="7.32281875" y2="0.97358125" layer="21"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.017" y2="0.97358125" layer="21"/>
<rectangle x1="11.049" y1="0.889" x2="11.303" y2="0.97358125" layer="21"/>
<rectangle x1="11.811" y1="0.889" x2="11.98118125" y2="0.97358125" layer="21"/>
<rectangle x1="13.843" y1="0.889" x2="14.18081875" y2="0.97358125" layer="21"/>
<rectangle x1="14.94281875" y1="0.889" x2="16.72081875" y2="0.97358125" layer="21"/>
<rectangle x1="19.60118125" y1="0.889" x2="20.10918125" y2="0.97358125" layer="21"/>
<rectangle x1="22.14118125" y1="0.889" x2="22.56281875" y2="0.97358125" layer="21"/>
<rectangle x1="24.59481875" y1="0.889" x2="25.10281875" y2="0.97358125" layer="21"/>
<rectangle x1="27.813" y1="0.889" x2="29.591" y2="0.97358125" layer="21"/>
<rectangle x1="30.94481875" y1="0.889" x2="31.19881875" y2="0.97358125" layer="21"/>
<rectangle x1="33.23081875" y1="0.889" x2="33.401" y2="0.97358125" layer="21"/>
<rectangle x1="34.84118125" y1="0.889" x2="35.34918125" y2="0.97358125" layer="21"/>
<rectangle x1="5.37718125" y1="0.97358125" x2="7.493" y2="1.05841875" layer="21"/>
<rectangle x1="8.763" y1="0.97358125" x2="9.10081875" y2="1.05841875" layer="21"/>
<rectangle x1="10.96518125" y1="0.97358125" x2="11.303" y2="1.05841875" layer="21"/>
<rectangle x1="11.72718125" y1="0.97358125" x2="12.065" y2="1.05841875" layer="21"/>
<rectangle x1="13.589" y1="0.97358125" x2="14.26718125" y2="1.05841875" layer="21"/>
<rectangle x1="14.94281875" y1="0.97358125" x2="16.97481875" y2="1.05841875" layer="21"/>
<rectangle x1="19.34718125" y1="0.97358125" x2="20.36318125" y2="1.05841875" layer="21"/>
<rectangle x1="21.971" y1="0.97358125" x2="22.90318125" y2="1.05841875" layer="21"/>
<rectangle x1="24.34081875" y1="0.97358125" x2="25.44318125" y2="1.05841875" layer="21"/>
<rectangle x1="27.64281875" y1="0.97358125" x2="29.76118125" y2="1.05841875" layer="21"/>
<rectangle x1="30.94481875" y1="0.97358125" x2="31.28518125" y2="1.05841875" layer="21"/>
<rectangle x1="33.147" y1="0.97358125" x2="33.48481875" y2="1.05841875" layer="21"/>
<rectangle x1="34.58718125" y1="0.97358125" x2="35.60318125" y2="1.05841875" layer="21"/>
<rectangle x1="5.29081875" y1="1.05841875" x2="6.05281875" y2="1.143" layer="21"/>
<rectangle x1="6.81481875" y1="1.05841875" x2="7.57681875" y2="1.143" layer="21"/>
<rectangle x1="8.763" y1="1.05841875" x2="9.10081875" y2="1.143" layer="21"/>
<rectangle x1="10.96518125" y1="1.05841875" x2="11.303" y2="1.143" layer="21"/>
<rectangle x1="11.72718125" y1="1.05841875" x2="12.065" y2="1.143" layer="21"/>
<rectangle x1="13.41881875" y1="1.05841875" x2="14.26718125" y2="1.143" layer="21"/>
<rectangle x1="14.859" y1="1.05841875" x2="17.22881875" y2="1.143" layer="21"/>
<rectangle x1="19.177" y1="1.05841875" x2="20.53081875" y2="1.143" layer="21"/>
<rectangle x1="21.80081875" y1="1.05841875" x2="23.07081875" y2="1.143" layer="21"/>
<rectangle x1="24.17318125" y1="1.05841875" x2="25.61081875" y2="1.143" layer="21"/>
<rectangle x1="27.559" y1="1.05841875" x2="28.40481875" y2="1.143" layer="21"/>
<rectangle x1="29.083" y1="1.05841875" x2="29.845" y2="1.143" layer="21"/>
<rectangle x1="30.94481875" y1="1.05841875" x2="31.28518125" y2="1.143" layer="21"/>
<rectangle x1="33.147" y1="1.05841875" x2="33.48481875" y2="1.143" layer="21"/>
<rectangle x1="34.417" y1="1.05841875" x2="35.77081875" y2="1.143" layer="21"/>
<rectangle x1="5.207" y1="1.143" x2="5.79881875" y2="1.22758125" layer="21"/>
<rectangle x1="6.985" y1="1.143" x2="7.66318125" y2="1.22758125" layer="21"/>
<rectangle x1="8.763" y1="1.143" x2="9.10081875" y2="1.22758125" layer="21"/>
<rectangle x1="10.96518125" y1="1.143" x2="11.303" y2="1.22758125" layer="21"/>
<rectangle x1="11.72718125" y1="1.143" x2="12.065" y2="1.22758125" layer="21"/>
<rectangle x1="13.25118125" y1="1.143" x2="14.26718125" y2="1.22758125" layer="21"/>
<rectangle x1="14.859" y1="1.143" x2="17.31518125" y2="1.22758125" layer="21"/>
<rectangle x1="19.09318125" y1="1.143" x2="20.701" y2="1.22758125" layer="21"/>
<rectangle x1="21.717" y1="1.143" x2="23.07081875" y2="1.22758125" layer="21"/>
<rectangle x1="24.08681875" y1="1.143" x2="25.69718125" y2="1.22758125" layer="21"/>
<rectangle x1="27.47518125" y1="1.143" x2="28.067" y2="1.22758125" layer="21"/>
<rectangle x1="29.337" y1="1.143" x2="29.92881875" y2="1.22758125" layer="21"/>
<rectangle x1="30.94481875" y1="1.143" x2="31.28518125" y2="1.22758125" layer="21"/>
<rectangle x1="33.147" y1="1.143" x2="33.48481875" y2="1.22758125" layer="21"/>
<rectangle x1="34.33318125" y1="1.143" x2="35.941" y2="1.22758125" layer="21"/>
<rectangle x1="5.03681875" y1="1.22758125" x2="5.63118125" y2="1.31241875" layer="21"/>
<rectangle x1="7.239" y1="1.22758125" x2="7.747" y2="1.31241875" layer="21"/>
<rectangle x1="8.763" y1="1.22758125" x2="9.10081875" y2="1.31241875" layer="21"/>
<rectangle x1="10.96518125" y1="1.22758125" x2="11.303" y2="1.31241875" layer="21"/>
<rectangle x1="11.72718125" y1="1.22758125" x2="12.065" y2="1.31241875" layer="21"/>
<rectangle x1="13.16481875" y1="1.22758125" x2="13.843" y2="1.31241875" layer="21"/>
<rectangle x1="14.859" y1="1.22758125" x2="15.28318125" y2="1.31241875" layer="21"/>
<rectangle x1="16.637" y1="1.22758125" x2="17.48281875" y2="1.31241875" layer="21"/>
<rectangle x1="18.923" y1="1.22758125" x2="19.685" y2="1.31241875" layer="21"/>
<rectangle x1="20.02281875" y1="1.22758125" x2="20.78481875" y2="1.31241875" layer="21"/>
<rectangle x1="21.63318125" y1="1.22758125" x2="22.225" y2="1.31241875" layer="21"/>
<rectangle x1="22.56281875" y1="1.22758125" x2="23.15718125" y2="1.31241875" layer="21"/>
<rectangle x1="24.003" y1="1.22758125" x2="24.68118125" y2="1.31241875" layer="21"/>
<rectangle x1="25.10281875" y1="1.22758125" x2="25.781" y2="1.31241875" layer="21"/>
<rectangle x1="27.38881875" y1="1.22758125" x2="27.98318125" y2="1.31241875" layer="21"/>
<rectangle x1="29.50718125" y1="1.22758125" x2="30.01518125" y2="1.31241875" layer="21"/>
<rectangle x1="30.94481875" y1="1.22758125" x2="31.28518125" y2="1.31241875" layer="21"/>
<rectangle x1="33.147" y1="1.22758125" x2="33.48481875" y2="1.31241875" layer="21"/>
<rectangle x1="34.163" y1="1.22758125" x2="34.925" y2="1.31241875" layer="21"/>
<rectangle x1="35.26281875" y1="1.22758125" x2="36.02481875" y2="1.31241875" layer="21"/>
<rectangle x1="5.03681875" y1="1.31241875" x2="5.54481875" y2="1.397" layer="21"/>
<rectangle x1="7.32281875" y1="1.31241875" x2="7.83081875" y2="1.397" layer="21"/>
<rectangle x1="8.763" y1="1.31241875" x2="9.10081875" y2="1.397" layer="21"/>
<rectangle x1="10.96518125" y1="1.31241875" x2="11.303" y2="1.397" layer="21"/>
<rectangle x1="11.72718125" y1="1.31241875" x2="12.065" y2="1.397" layer="21"/>
<rectangle x1="13.081" y1="1.31241875" x2="13.67281875" y2="1.397" layer="21"/>
<rectangle x1="14.94281875" y1="1.31241875" x2="15.19681875" y2="1.397" layer="21"/>
<rectangle x1="16.891" y1="1.31241875" x2="17.56918125" y2="1.397" layer="21"/>
<rectangle x1="18.923" y1="1.31241875" x2="19.431" y2="1.397" layer="21"/>
<rectangle x1="20.27681875" y1="1.31241875" x2="20.87118125" y2="1.397" layer="21"/>
<rectangle x1="21.54681875" y1="1.31241875" x2="22.05481875" y2="1.397" layer="21"/>
<rectangle x1="22.733" y1="1.31241875" x2="23.241" y2="1.397" layer="21"/>
<rectangle x1="23.91918125" y1="1.31241875" x2="24.42718125" y2="1.397" layer="21"/>
<rectangle x1="25.273" y1="1.31241875" x2="25.86481875" y2="1.397" layer="21"/>
<rectangle x1="27.305" y1="1.31241875" x2="27.813" y2="1.397" layer="21"/>
<rectangle x1="29.591" y1="1.31241875" x2="30.099" y2="1.397" layer="21"/>
<rectangle x1="30.94481875" y1="1.31241875" x2="31.28518125" y2="1.397" layer="21"/>
<rectangle x1="33.147" y1="1.31241875" x2="33.48481875" y2="1.397" layer="21"/>
<rectangle x1="34.163" y1="1.31241875" x2="34.671" y2="1.397" layer="21"/>
<rectangle x1="35.51681875" y1="1.31241875" x2="36.11118125" y2="1.397" layer="21"/>
<rectangle x1="4.953" y1="1.397" x2="5.37718125" y2="1.48158125" layer="21"/>
<rectangle x1="7.40918125" y1="1.397" x2="7.91718125" y2="1.48158125" layer="21"/>
<rectangle x1="8.763" y1="1.397" x2="9.10081875" y2="1.48158125" layer="21"/>
<rectangle x1="10.96518125" y1="1.397" x2="11.303" y2="1.48158125" layer="21"/>
<rectangle x1="11.72718125" y1="1.397" x2="12.065" y2="1.48158125" layer="21"/>
<rectangle x1="13.081" y1="1.397" x2="13.50518125" y2="1.48158125" layer="21"/>
<rectangle x1="14.859" y1="1.397" x2="15.19681875" y2="1.48158125" layer="21"/>
<rectangle x1="17.06118125" y1="1.397" x2="17.653" y2="1.48158125" layer="21"/>
<rectangle x1="18.83918125" y1="1.397" x2="19.26081875" y2="1.48158125" layer="21"/>
<rectangle x1="20.447" y1="1.397" x2="20.87118125" y2="1.48158125" layer="21"/>
<rectangle x1="21.54681875" y1="1.397" x2="21.971" y2="1.48158125" layer="21"/>
<rectangle x1="22.90318125" y1="1.397" x2="23.241" y2="1.48158125" layer="21"/>
<rectangle x1="23.83281875" y1="1.397" x2="24.34081875" y2="1.48158125" layer="21"/>
<rectangle x1="25.44318125" y1="1.397" x2="25.95118125" y2="1.48158125" layer="21"/>
<rectangle x1="27.22118125" y1="1.397" x2="27.72918125" y2="1.48158125" layer="21"/>
<rectangle x1="29.76118125" y1="1.397" x2="30.18281875" y2="1.48158125" layer="21"/>
<rectangle x1="30.94481875" y1="1.397" x2="31.28518125" y2="1.48158125" layer="21"/>
<rectangle x1="33.147" y1="1.397" x2="33.48481875" y2="1.48158125" layer="21"/>
<rectangle x1="34.07918125" y1="1.397" x2="34.50081875" y2="1.48158125" layer="21"/>
<rectangle x1="35.687" y1="1.397" x2="36.11118125" y2="1.48158125" layer="21"/>
<rectangle x1="4.86918125" y1="1.48158125" x2="5.29081875" y2="1.56641875" layer="21"/>
<rectangle x1="7.57681875" y1="1.48158125" x2="8.001" y2="1.56641875" layer="21"/>
<rectangle x1="8.763" y1="1.48158125" x2="9.10081875" y2="1.56641875" layer="21"/>
<rectangle x1="10.96518125" y1="1.48158125" x2="11.303" y2="1.56641875" layer="21"/>
<rectangle x1="11.72718125" y1="1.48158125" x2="12.065" y2="1.56641875" layer="21"/>
<rectangle x1="12.99718125" y1="1.48158125" x2="13.41881875" y2="1.56641875" layer="21"/>
<rectangle x1="14.859" y1="1.48158125" x2="15.28318125" y2="1.56641875" layer="21"/>
<rectangle x1="17.22881875" y1="1.48158125" x2="17.73681875" y2="1.56641875" layer="21"/>
<rectangle x1="18.75281875" y1="1.48158125" x2="19.177" y2="1.56641875" layer="21"/>
<rectangle x1="20.53081875" y1="1.48158125" x2="20.955" y2="1.56641875" layer="21"/>
<rectangle x1="21.54681875" y1="1.48158125" x2="21.88718125" y2="1.56641875" layer="21"/>
<rectangle x1="22.90318125" y1="1.48158125" x2="23.241" y2="1.56641875" layer="21"/>
<rectangle x1="23.749" y1="1.48158125" x2="24.257" y2="1.56641875" layer="21"/>
<rectangle x1="25.527" y1="1.48158125" x2="25.95118125" y2="1.56641875" layer="21"/>
<rectangle x1="27.13481875" y1="1.48158125" x2="27.64281875" y2="1.56641875" layer="21"/>
<rectangle x1="29.845" y1="1.48158125" x2="30.26918125" y2="1.56641875" layer="21"/>
<rectangle x1="30.94481875" y1="1.48158125" x2="31.28518125" y2="1.56641875" layer="21"/>
<rectangle x1="33.147" y1="1.48158125" x2="33.48481875" y2="1.56641875" layer="21"/>
<rectangle x1="33.99281875" y1="1.48158125" x2="34.417" y2="1.56641875" layer="21"/>
<rectangle x1="35.77081875" y1="1.48158125" x2="36.195" y2="1.56641875" layer="21"/>
<rectangle x1="4.78281875" y1="1.56641875" x2="5.207" y2="1.651" layer="21"/>
<rectangle x1="7.57681875" y1="1.56641875" x2="8.001" y2="1.651" layer="21"/>
<rectangle x1="8.763" y1="1.56641875" x2="9.10081875" y2="1.651" layer="21"/>
<rectangle x1="10.96518125" y1="1.56641875" x2="11.303" y2="1.651" layer="21"/>
<rectangle x1="11.72718125" y1="1.56641875" x2="12.065" y2="1.651" layer="21"/>
<rectangle x1="12.91081875" y1="1.56641875" x2="13.335" y2="1.651" layer="21"/>
<rectangle x1="14.859" y1="1.56641875" x2="15.28318125" y2="1.651" layer="21"/>
<rectangle x1="17.31518125" y1="1.56641875" x2="17.82318125" y2="1.651" layer="21"/>
<rectangle x1="18.669" y1="1.56641875" x2="19.09318125" y2="1.651" layer="21"/>
<rectangle x1="20.61718125" y1="1.56641875" x2="21.03881875" y2="1.651" layer="21"/>
<rectangle x1="21.54681875" y1="1.56641875" x2="21.80081875" y2="1.651" layer="21"/>
<rectangle x1="22.987" y1="1.56641875" x2="23.241" y2="1.651" layer="21"/>
<rectangle x1="23.749" y1="1.56641875" x2="24.17318125" y2="1.651" layer="21"/>
<rectangle x1="25.61081875" y1="1.56641875" x2="26.035" y2="1.651" layer="21"/>
<rectangle x1="27.13481875" y1="1.56641875" x2="27.47518125" y2="1.651" layer="21"/>
<rectangle x1="29.92881875" y1="1.56641875" x2="30.353" y2="1.651" layer="21"/>
<rectangle x1="30.94481875" y1="1.56641875" x2="31.28518125" y2="1.651" layer="21"/>
<rectangle x1="33.147" y1="1.56641875" x2="33.48481875" y2="1.651" layer="21"/>
<rectangle x1="33.99281875" y1="1.56641875" x2="34.33318125" y2="1.651" layer="21"/>
<rectangle x1="35.85718125" y1="1.56641875" x2="36.27881875" y2="1.651" layer="21"/>
<rectangle x1="4.78281875" y1="1.651" x2="5.207" y2="1.73558125" layer="21"/>
<rectangle x1="7.66318125" y1="1.651" x2="8.08481875" y2="1.73558125" layer="21"/>
<rectangle x1="8.763" y1="1.651" x2="9.10081875" y2="1.73558125" layer="21"/>
<rectangle x1="10.96518125" y1="1.651" x2="11.303" y2="1.73558125" layer="21"/>
<rectangle x1="11.72718125" y1="1.651" x2="12.065" y2="1.73558125" layer="21"/>
<rectangle x1="12.91081875" y1="1.651" x2="13.25118125" y2="1.73558125" layer="21"/>
<rectangle x1="14.859" y1="1.651" x2="15.19681875" y2="1.73558125" layer="21"/>
<rectangle x1="17.48281875" y1="1.651" x2="17.907" y2="1.73558125" layer="21"/>
<rectangle x1="18.669" y1="1.651" x2="19.09318125" y2="1.73558125" layer="21"/>
<rectangle x1="20.701" y1="1.651" x2="21.03881875" y2="1.73558125" layer="21"/>
<rectangle x1="22.90318125" y1="1.651" x2="23.241" y2="1.73558125" layer="21"/>
<rectangle x1="23.66518125" y1="1.651" x2="24.08681875" y2="1.73558125" layer="21"/>
<rectangle x1="25.69718125" y1="1.651" x2="26.035" y2="1.73558125" layer="21"/>
<rectangle x1="27.051" y1="1.651" x2="27.47518125" y2="1.73558125" layer="21"/>
<rectangle x1="30.01518125" y1="1.651" x2="30.353" y2="1.73558125" layer="21"/>
<rectangle x1="30.94481875" y1="1.651" x2="31.28518125" y2="1.73558125" layer="21"/>
<rectangle x1="33.147" y1="1.651" x2="33.48481875" y2="1.73558125" layer="21"/>
<rectangle x1="33.909" y1="1.651" x2="34.33318125" y2="1.73558125" layer="21"/>
<rectangle x1="35.941" y1="1.651" x2="36.27881875" y2="1.73558125" layer="21"/>
<rectangle x1="4.699" y1="1.73558125" x2="5.12318125" y2="1.82041875" layer="21"/>
<rectangle x1="7.747" y1="1.73558125" x2="8.17118125" y2="1.82041875" layer="21"/>
<rectangle x1="8.763" y1="1.73558125" x2="9.10081875" y2="1.82041875" layer="21"/>
<rectangle x1="10.96518125" y1="1.73558125" x2="11.303" y2="1.82041875" layer="21"/>
<rectangle x1="11.72718125" y1="1.73558125" x2="12.065" y2="1.82041875" layer="21"/>
<rectangle x1="12.91081875" y1="1.73558125" x2="13.25118125" y2="1.82041875" layer="21"/>
<rectangle x1="14.859" y1="1.73558125" x2="15.28318125" y2="1.82041875" layer="21"/>
<rectangle x1="17.56918125" y1="1.73558125" x2="17.99081875" y2="1.82041875" layer="21"/>
<rectangle x1="18.669" y1="1.73558125" x2="19.00681875" y2="1.82041875" layer="21"/>
<rectangle x1="20.701" y1="1.73558125" x2="21.12518125" y2="1.82041875" layer="21"/>
<rectangle x1="22.90318125" y1="1.73558125" x2="23.241" y2="1.82041875" layer="21"/>
<rectangle x1="23.66518125" y1="1.73558125" x2="24.003" y2="1.82041875" layer="21"/>
<rectangle x1="25.781" y1="1.73558125" x2="25.95118125" y2="1.82041875" layer="21"/>
<rectangle x1="26.96718125" y1="1.73558125" x2="27.38881875" y2="1.82041875" layer="21"/>
<rectangle x1="30.01518125" y1="1.73558125" x2="30.43681875" y2="1.82041875" layer="21"/>
<rectangle x1="30.94481875" y1="1.73558125" x2="31.28518125" y2="1.82041875" layer="21"/>
<rectangle x1="33.147" y1="1.73558125" x2="33.48481875" y2="1.82041875" layer="21"/>
<rectangle x1="33.909" y1="1.73558125" x2="34.24681875" y2="1.82041875" layer="21"/>
<rectangle x1="35.941" y1="1.73558125" x2="36.195" y2="1.82041875" layer="21"/>
<rectangle x1="4.699" y1="1.82041875" x2="5.03681875" y2="1.905" layer="21"/>
<rectangle x1="7.83081875" y1="1.82041875" x2="8.17118125" y2="1.905" layer="21"/>
<rectangle x1="8.763" y1="1.82041875" x2="9.10081875" y2="1.905" layer="21"/>
<rectangle x1="10.96518125" y1="1.82041875" x2="11.303" y2="1.905" layer="21"/>
<rectangle x1="11.72718125" y1="1.82041875" x2="12.065" y2="1.905" layer="21"/>
<rectangle x1="12.827" y1="1.82041875" x2="13.16481875" y2="1.905" layer="21"/>
<rectangle x1="14.859" y1="1.82041875" x2="15.28318125" y2="1.905" layer="21"/>
<rectangle x1="17.56918125" y1="1.82041875" x2="17.99081875" y2="1.905" layer="21"/>
<rectangle x1="18.58518125" y1="1.82041875" x2="19.00681875" y2="1.905" layer="21"/>
<rectangle x1="20.78481875" y1="1.82041875" x2="21.12518125" y2="1.905" layer="21"/>
<rectangle x1="22.733" y1="1.82041875" x2="23.241" y2="1.905" layer="21"/>
<rectangle x1="23.66518125" y1="1.82041875" x2="24.003" y2="1.905" layer="21"/>
<rectangle x1="26.96718125" y1="1.82041875" x2="27.305" y2="1.905" layer="21"/>
<rectangle x1="30.099" y1="1.82041875" x2="30.43681875" y2="1.905" layer="21"/>
<rectangle x1="30.94481875" y1="1.82041875" x2="31.28518125" y2="1.905" layer="21"/>
<rectangle x1="33.147" y1="1.82041875" x2="33.48481875" y2="1.905" layer="21"/>
<rectangle x1="33.82518125" y1="1.82041875" x2="34.163" y2="1.905" layer="21"/>
<rectangle x1="4.61518125" y1="1.905" x2="5.03681875" y2="1.98958125" layer="21"/>
<rectangle x1="7.83081875" y1="1.905" x2="8.255" y2="1.98958125" layer="21"/>
<rectangle x1="8.763" y1="1.905" x2="9.10081875" y2="1.98958125" layer="21"/>
<rectangle x1="10.96518125" y1="1.905" x2="11.303" y2="1.98958125" layer="21"/>
<rectangle x1="11.72718125" y1="1.905" x2="12.065" y2="1.98958125" layer="21"/>
<rectangle x1="12.827" y1="1.905" x2="13.16481875" y2="1.98958125" layer="21"/>
<rectangle x1="14.859" y1="1.905" x2="15.28318125" y2="1.98958125" layer="21"/>
<rectangle x1="17.653" y1="1.905" x2="18.07718125" y2="1.98958125" layer="21"/>
<rectangle x1="18.58518125" y1="1.905" x2="18.923" y2="1.98958125" layer="21"/>
<rectangle x1="20.78481875" y1="1.905" x2="21.12518125" y2="1.98958125" layer="21"/>
<rectangle x1="22.64918125" y1="1.905" x2="23.15718125" y2="1.98958125" layer="21"/>
<rectangle x1="23.57881875" y1="1.905" x2="23.91918125" y2="1.98958125" layer="21"/>
<rectangle x1="26.96718125" y1="1.905" x2="27.305" y2="1.98958125" layer="21"/>
<rectangle x1="30.099" y1="1.905" x2="30.52318125" y2="1.98958125" layer="21"/>
<rectangle x1="30.94481875" y1="1.905" x2="31.28518125" y2="1.98958125" layer="21"/>
<rectangle x1="33.147" y1="1.905" x2="33.48481875" y2="1.98958125" layer="21"/>
<rectangle x1="33.82518125" y1="1.905" x2="34.163" y2="1.98958125" layer="21"/>
<rectangle x1="4.61518125" y1="1.98958125" x2="4.953" y2="2.07441875" layer="21"/>
<rectangle x1="7.91718125" y1="1.98958125" x2="8.255" y2="2.07441875" layer="21"/>
<rectangle x1="8.763" y1="1.98958125" x2="9.10081875" y2="2.07441875" layer="21"/>
<rectangle x1="10.96518125" y1="1.98958125" x2="11.303" y2="2.07441875" layer="21"/>
<rectangle x1="11.72718125" y1="1.98958125" x2="12.065" y2="2.07441875" layer="21"/>
<rectangle x1="12.827" y1="1.98958125" x2="13.16481875" y2="2.07441875" layer="21"/>
<rectangle x1="14.859" y1="1.98958125" x2="15.19681875" y2="2.07441875" layer="21"/>
<rectangle x1="17.73681875" y1="1.98958125" x2="18.07718125" y2="2.07441875" layer="21"/>
<rectangle x1="18.58518125" y1="1.98958125" x2="18.923" y2="2.07441875" layer="21"/>
<rectangle x1="20.78481875" y1="1.98958125" x2="21.12518125" y2="2.07441875" layer="21"/>
<rectangle x1="22.39518125" y1="1.98958125" x2="23.07081875" y2="2.07441875" layer="21"/>
<rectangle x1="23.57881875" y1="1.98958125" x2="25.86481875" y2="2.07441875" layer="21"/>
<rectangle x1="26.88081875" y1="1.98958125" x2="27.22118125" y2="2.07441875" layer="21"/>
<rectangle x1="30.18281875" y1="1.98958125" x2="30.52318125" y2="2.07441875" layer="21"/>
<rectangle x1="30.94481875" y1="1.98958125" x2="31.28518125" y2="2.07441875" layer="21"/>
<rectangle x1="33.147" y1="1.98958125" x2="33.48481875" y2="2.07441875" layer="21"/>
<rectangle x1="33.82518125" y1="1.98958125" x2="34.163" y2="2.07441875" layer="21"/>
<rectangle x1="34.33318125" y1="1.98958125" x2="35.941" y2="2.07441875" layer="21"/>
<rectangle x1="36.02481875" y1="1.98958125" x2="36.27881875" y2="2.07441875" layer="21"/>
<rectangle x1="4.61518125" y1="2.07441875" x2="4.953" y2="2.159" layer="21"/>
<rectangle x1="7.91718125" y1="2.07441875" x2="8.255" y2="2.159" layer="21"/>
<rectangle x1="8.763" y1="2.07441875" x2="9.10081875" y2="2.159" layer="21"/>
<rectangle x1="10.96518125" y1="2.07441875" x2="11.303" y2="2.159" layer="21"/>
<rectangle x1="11.72718125" y1="2.07441875" x2="12.065" y2="2.159" layer="21"/>
<rectangle x1="12.827" y1="2.07441875" x2="13.16481875" y2="2.159" layer="21"/>
<rectangle x1="14.859" y1="2.07441875" x2="15.19681875" y2="2.159" layer="21"/>
<rectangle x1="17.73681875" y1="2.07441875" x2="18.161" y2="2.159" layer="21"/>
<rectangle x1="18.58518125" y1="2.07441875" x2="18.923" y2="2.159" layer="21"/>
<rectangle x1="20.78481875" y1="2.07441875" x2="21.12518125" y2="2.159" layer="21"/>
<rectangle x1="22.14118125" y1="2.07441875" x2="22.987" y2="2.159" layer="21"/>
<rectangle x1="23.57881875" y1="2.07441875" x2="26.11881875" y2="2.159" layer="21"/>
<rectangle x1="26.88081875" y1="2.07441875" x2="27.22118125" y2="2.159" layer="21"/>
<rectangle x1="30.18281875" y1="2.07441875" x2="30.52318125" y2="2.159" layer="21"/>
<rectangle x1="30.94481875" y1="2.07441875" x2="31.28518125" y2="2.159" layer="21"/>
<rectangle x1="33.147" y1="2.07441875" x2="33.48481875" y2="2.159" layer="21"/>
<rectangle x1="33.82518125" y1="2.07441875" x2="36.36518125" y2="2.159" layer="21"/>
<rectangle x1="4.52881875" y1="2.159" x2="4.953" y2="2.24358125" layer="21"/>
<rectangle x1="7.91718125" y1="2.159" x2="8.255" y2="2.24358125" layer="21"/>
<rectangle x1="8.763" y1="2.159" x2="9.10081875" y2="2.24358125" layer="21"/>
<rectangle x1="10.96518125" y1="2.159" x2="11.303" y2="2.24358125" layer="21"/>
<rectangle x1="11.72718125" y1="2.159" x2="12.065" y2="2.24358125" layer="21"/>
<rectangle x1="12.827" y1="2.159" x2="13.16481875" y2="2.24358125" layer="21"/>
<rectangle x1="14.859" y1="2.159" x2="15.19681875" y2="2.24358125" layer="21"/>
<rectangle x1="17.82318125" y1="2.159" x2="18.161" y2="2.24358125" layer="21"/>
<rectangle x1="18.58518125" y1="2.159" x2="18.923" y2="2.24358125" layer="21"/>
<rectangle x1="20.78481875" y1="2.159" x2="21.12518125" y2="2.24358125" layer="21"/>
<rectangle x1="21.971" y1="2.159" x2="22.90318125" y2="2.24358125" layer="21"/>
<rectangle x1="23.57881875" y1="2.159" x2="26.20518125" y2="2.24358125" layer="21"/>
<rectangle x1="26.88081875" y1="2.159" x2="27.22118125" y2="2.24358125" layer="21"/>
<rectangle x1="30.18281875" y1="2.159" x2="30.607" y2="2.24358125" layer="21"/>
<rectangle x1="30.94481875" y1="2.159" x2="31.28518125" y2="2.24358125" layer="21"/>
<rectangle x1="33.147" y1="2.159" x2="33.48481875" y2="2.24358125" layer="21"/>
<rectangle x1="33.82518125" y1="2.159" x2="36.36518125" y2="2.24358125" layer="21"/>
<rectangle x1="4.52881875" y1="2.24358125" x2="4.86918125" y2="2.32841875" layer="21"/>
<rectangle x1="7.91718125" y1="2.24358125" x2="8.33881875" y2="2.32841875" layer="21"/>
<rectangle x1="8.763" y1="2.24358125" x2="9.10081875" y2="2.32841875" layer="21"/>
<rectangle x1="10.96518125" y1="2.24358125" x2="11.303" y2="2.32841875" layer="21"/>
<rectangle x1="11.72718125" y1="2.24358125" x2="12.065" y2="2.32841875" layer="21"/>
<rectangle x1="12.827" y1="2.24358125" x2="13.16481875" y2="2.32841875" layer="21"/>
<rectangle x1="14.859" y1="2.24358125" x2="15.19681875" y2="2.32841875" layer="21"/>
<rectangle x1="17.82318125" y1="2.24358125" x2="18.161" y2="2.32841875" layer="21"/>
<rectangle x1="18.58518125" y1="2.24358125" x2="18.923" y2="2.32841875" layer="21"/>
<rectangle x1="20.78481875" y1="2.24358125" x2="21.12518125" y2="2.32841875" layer="21"/>
<rectangle x1="21.717" y1="2.24358125" x2="22.64918125" y2="2.32841875" layer="21"/>
<rectangle x1="23.57881875" y1="2.24358125" x2="26.20518125" y2="2.32841875" layer="21"/>
<rectangle x1="26.797" y1="2.24358125" x2="27.22118125" y2="2.32841875" layer="21"/>
<rectangle x1="30.26918125" y1="2.24358125" x2="30.607" y2="2.32841875" layer="21"/>
<rectangle x1="30.94481875" y1="2.24358125" x2="31.28518125" y2="2.32841875" layer="21"/>
<rectangle x1="33.147" y1="2.24358125" x2="33.48481875" y2="2.32841875" layer="21"/>
<rectangle x1="33.82518125" y1="2.24358125" x2="36.36518125" y2="2.32841875" layer="21"/>
<rectangle x1="4.52881875" y1="2.32841875" x2="4.86918125" y2="2.413" layer="21"/>
<rectangle x1="8.001" y1="2.32841875" x2="8.33881875" y2="2.413" layer="21"/>
<rectangle x1="8.763" y1="2.32841875" x2="9.10081875" y2="2.413" layer="21"/>
<rectangle x1="10.96518125" y1="2.32841875" x2="11.303" y2="2.413" layer="21"/>
<rectangle x1="11.72718125" y1="2.32841875" x2="12.065" y2="2.413" layer="21"/>
<rectangle x1="12.827" y1="2.32841875" x2="13.16481875" y2="2.413" layer="21"/>
<rectangle x1="14.859" y1="2.32841875" x2="15.19681875" y2="2.413" layer="21"/>
<rectangle x1="17.82318125" y1="2.32841875" x2="18.24481875" y2="2.413" layer="21"/>
<rectangle x1="18.58518125" y1="2.32841875" x2="18.923" y2="2.413" layer="21"/>
<rectangle x1="20.78481875" y1="2.32841875" x2="21.12518125" y2="2.413" layer="21"/>
<rectangle x1="21.63318125" y1="2.32841875" x2="22.479" y2="2.413" layer="21"/>
<rectangle x1="23.57881875" y1="2.32841875" x2="24.257" y2="2.413" layer="21"/>
<rectangle x1="24.34081875" y1="2.32841875" x2="26.20518125" y2="2.413" layer="21"/>
<rectangle x1="26.797" y1="2.32841875" x2="27.13481875" y2="2.413" layer="21"/>
<rectangle x1="30.26918125" y1="2.32841875" x2="30.607" y2="2.413" layer="21"/>
<rectangle x1="30.94481875" y1="2.32841875" x2="31.28518125" y2="2.413" layer="21"/>
<rectangle x1="33.147" y1="2.32841875" x2="33.48481875" y2="2.413" layer="21"/>
<rectangle x1="33.82518125" y1="2.32841875" x2="35.179" y2="2.413" layer="21"/>
<rectangle x1="35.34918125" y1="2.32841875" x2="35.60318125" y2="2.413" layer="21"/>
<rectangle x1="35.77081875" y1="2.32841875" x2="35.941" y2="2.413" layer="21"/>
<rectangle x1="36.02481875" y1="2.32841875" x2="36.36518125" y2="2.413" layer="21"/>
<rectangle x1="4.52881875" y1="2.413" x2="4.86918125" y2="2.49758125" layer="21"/>
<rectangle x1="8.001" y1="2.413" x2="8.33881875" y2="2.49758125" layer="21"/>
<rectangle x1="8.763" y1="2.413" x2="9.10081875" y2="2.49758125" layer="21"/>
<rectangle x1="10.96518125" y1="2.413" x2="11.303" y2="2.49758125" layer="21"/>
<rectangle x1="11.72718125" y1="2.413" x2="12.065" y2="2.49758125" layer="21"/>
<rectangle x1="12.827" y1="2.413" x2="13.16481875" y2="2.49758125" layer="21"/>
<rectangle x1="14.859" y1="2.413" x2="15.19681875" y2="2.49758125" layer="21"/>
<rectangle x1="17.907" y1="2.413" x2="18.24481875" y2="2.49758125" layer="21"/>
<rectangle x1="18.58518125" y1="2.413" x2="18.923" y2="2.49758125" layer="21"/>
<rectangle x1="20.78481875" y1="2.413" x2="21.12518125" y2="2.49758125" layer="21"/>
<rectangle x1="21.54681875" y1="2.413" x2="22.225" y2="2.49758125" layer="21"/>
<rectangle x1="23.57881875" y1="2.413" x2="23.91918125" y2="2.49758125" layer="21"/>
<rectangle x1="25.781" y1="2.413" x2="26.11881875" y2="2.49758125" layer="21"/>
<rectangle x1="26.797" y1="2.413" x2="27.13481875" y2="2.49758125" layer="21"/>
<rectangle x1="30.26918125" y1="2.413" x2="30.607" y2="2.49758125" layer="21"/>
<rectangle x1="30.94481875" y1="2.413" x2="31.28518125" y2="2.49758125" layer="21"/>
<rectangle x1="33.147" y1="2.413" x2="33.48481875" y2="2.49758125" layer="21"/>
<rectangle x1="33.82518125" y1="2.413" x2="34.163" y2="2.49758125" layer="21"/>
<rectangle x1="36.02481875" y1="2.413" x2="36.36518125" y2="2.49758125" layer="21"/>
<rectangle x1="4.52881875" y1="2.49758125" x2="4.86918125" y2="2.58241875" layer="21"/>
<rectangle x1="8.001" y1="2.49758125" x2="8.33881875" y2="2.58241875" layer="21"/>
<rectangle x1="8.763" y1="2.49758125" x2="9.18718125" y2="2.58241875" layer="21"/>
<rectangle x1="10.96518125" y1="2.49758125" x2="11.303" y2="2.58241875" layer="21"/>
<rectangle x1="11.72718125" y1="2.49758125" x2="12.065" y2="2.58241875" layer="21"/>
<rectangle x1="12.827" y1="2.49758125" x2="13.16481875" y2="2.58241875" layer="21"/>
<rectangle x1="14.859" y1="2.49758125" x2="15.19681875" y2="2.58241875" layer="21"/>
<rectangle x1="17.907" y1="2.49758125" x2="18.24481875" y2="2.58241875" layer="21"/>
<rectangle x1="18.58518125" y1="2.49758125" x2="19.00681875" y2="2.58241875" layer="21"/>
<rectangle x1="20.78481875" y1="2.49758125" x2="21.12518125" y2="2.58241875" layer="21"/>
<rectangle x1="21.463" y1="2.49758125" x2="21.971" y2="2.58241875" layer="21"/>
<rectangle x1="23.66518125" y1="2.49758125" x2="24.003" y2="2.58241875" layer="21"/>
<rectangle x1="25.781" y1="2.49758125" x2="26.11881875" y2="2.58241875" layer="21"/>
<rectangle x1="26.797" y1="2.49758125" x2="27.13481875" y2="2.58241875" layer="21"/>
<rectangle x1="30.26918125" y1="2.49758125" x2="30.607" y2="2.58241875" layer="21"/>
<rectangle x1="30.94481875" y1="2.49758125" x2="31.369" y2="2.58241875" layer="21"/>
<rectangle x1="33.06318125" y1="2.49758125" x2="33.48481875" y2="2.58241875" layer="21"/>
<rectangle x1="33.82518125" y1="2.49758125" x2="34.24681875" y2="2.58241875" layer="21"/>
<rectangle x1="36.02481875" y1="2.49758125" x2="36.36518125" y2="2.58241875" layer="21"/>
<rectangle x1="4.52881875" y1="2.58241875" x2="4.86918125" y2="2.667" layer="21"/>
<rectangle x1="8.001" y1="2.58241875" x2="8.33881875" y2="2.667" layer="21"/>
<rectangle x1="8.763" y1="2.58241875" x2="9.18718125" y2="2.667" layer="21"/>
<rectangle x1="10.87881875" y1="2.58241875" x2="11.303" y2="2.667" layer="21"/>
<rectangle x1="11.72718125" y1="2.58241875" x2="12.065" y2="2.667" layer="21"/>
<rectangle x1="12.827" y1="2.58241875" x2="13.16481875" y2="2.667" layer="21"/>
<rectangle x1="14.859" y1="2.58241875" x2="15.19681875" y2="2.667" layer="21"/>
<rectangle x1="17.907" y1="2.58241875" x2="18.24481875" y2="2.667" layer="21"/>
<rectangle x1="18.669" y1="2.58241875" x2="19.00681875" y2="2.667" layer="21"/>
<rectangle x1="20.701" y1="2.58241875" x2="21.12518125" y2="2.667" layer="21"/>
<rectangle x1="21.463" y1="2.58241875" x2="21.88718125" y2="2.667" layer="21"/>
<rectangle x1="23.66518125" y1="2.58241875" x2="24.003" y2="2.667" layer="21"/>
<rectangle x1="25.69718125" y1="2.58241875" x2="26.11881875" y2="2.667" layer="21"/>
<rectangle x1="26.797" y1="2.58241875" x2="27.13481875" y2="2.667" layer="21"/>
<rectangle x1="30.26918125" y1="2.58241875" x2="30.607" y2="2.667" layer="21"/>
<rectangle x1="30.94481875" y1="2.58241875" x2="31.369" y2="2.667" layer="21"/>
<rectangle x1="33.06318125" y1="2.58241875" x2="33.401" y2="2.667" layer="21"/>
<rectangle x1="33.909" y1="2.58241875" x2="34.24681875" y2="2.667" layer="21"/>
<rectangle x1="35.941" y1="2.58241875" x2="36.36518125" y2="2.667" layer="21"/>
<rectangle x1="4.52881875" y1="2.667" x2="4.86918125" y2="2.75158125" layer="21"/>
<rectangle x1="8.001" y1="2.667" x2="8.33881875" y2="2.75158125" layer="21"/>
<rectangle x1="8.763" y1="2.667" x2="9.271" y2="2.75158125" layer="21"/>
<rectangle x1="10.87881875" y1="2.667" x2="11.21918125" y2="2.75158125" layer="21"/>
<rectangle x1="11.72718125" y1="2.667" x2="12.065" y2="2.75158125" layer="21"/>
<rectangle x1="12.827" y1="2.667" x2="13.16481875" y2="2.75158125" layer="21"/>
<rectangle x1="14.859" y1="2.667" x2="15.19681875" y2="2.75158125" layer="21"/>
<rectangle x1="17.907" y1="2.667" x2="18.24481875" y2="2.75158125" layer="21"/>
<rectangle x1="18.669" y1="2.667" x2="19.09318125" y2="2.75158125" layer="21"/>
<rectangle x1="20.701" y1="2.667" x2="21.03881875" y2="2.75158125" layer="21"/>
<rectangle x1="21.463" y1="2.667" x2="21.80081875" y2="2.75158125" layer="21"/>
<rectangle x1="23.66518125" y1="2.667" x2="24.08681875" y2="2.75158125" layer="21"/>
<rectangle x1="25.69718125" y1="2.667" x2="26.035" y2="2.75158125" layer="21"/>
<rectangle x1="26.797" y1="2.667" x2="27.13481875" y2="2.75158125" layer="21"/>
<rectangle x1="30.26918125" y1="2.667" x2="30.607" y2="2.75158125" layer="21"/>
<rectangle x1="30.94481875" y1="2.667" x2="31.45281875" y2="2.75158125" layer="21"/>
<rectangle x1="32.97681875" y1="2.667" x2="33.401" y2="2.75158125" layer="21"/>
<rectangle x1="33.909" y1="2.667" x2="34.33318125" y2="2.75158125" layer="21"/>
<rectangle x1="35.941" y1="2.667" x2="36.27881875" y2="2.75158125" layer="21"/>
<rectangle x1="4.52881875" y1="2.75158125" x2="4.86918125" y2="2.83641875" layer="21"/>
<rectangle x1="8.001" y1="2.75158125" x2="8.33881875" y2="2.83641875" layer="21"/>
<rectangle x1="8.763" y1="2.75158125" x2="9.271" y2="2.83641875" layer="21"/>
<rectangle x1="10.795" y1="2.75158125" x2="11.21918125" y2="2.83641875" layer="21"/>
<rectangle x1="11.72718125" y1="2.75158125" x2="12.065" y2="2.83641875" layer="21"/>
<rectangle x1="12.827" y1="2.75158125" x2="13.16481875" y2="2.83641875" layer="21"/>
<rectangle x1="14.859" y1="2.75158125" x2="15.19681875" y2="2.83641875" layer="21"/>
<rectangle x1="17.907" y1="2.75158125" x2="18.24481875" y2="2.83641875" layer="21"/>
<rectangle x1="18.75281875" y1="2.75158125" x2="19.09318125" y2="2.83641875" layer="21"/>
<rectangle x1="20.61718125" y1="2.75158125" x2="21.03881875" y2="2.83641875" layer="21"/>
<rectangle x1="21.463" y1="2.75158125" x2="21.80081875" y2="2.83641875" layer="21"/>
<rectangle x1="22.987" y1="2.75158125" x2="23.15718125" y2="2.83641875" layer="21"/>
<rectangle x1="23.749" y1="2.75158125" x2="24.17318125" y2="2.83641875" layer="21"/>
<rectangle x1="25.61081875" y1="2.75158125" x2="26.035" y2="2.83641875" layer="21"/>
<rectangle x1="26.797" y1="2.75158125" x2="27.13481875" y2="2.83641875" layer="21"/>
<rectangle x1="30.26918125" y1="2.75158125" x2="30.607" y2="2.83641875" layer="21"/>
<rectangle x1="30.94481875" y1="2.75158125" x2="31.45281875" y2="2.83641875" layer="21"/>
<rectangle x1="32.97681875" y1="2.75158125" x2="33.401" y2="2.83641875" layer="21"/>
<rectangle x1="33.909" y1="2.75158125" x2="34.33318125" y2="2.83641875" layer="21"/>
<rectangle x1="35.85718125" y1="2.75158125" x2="36.27881875" y2="2.83641875" layer="21"/>
<rectangle x1="4.52881875" y1="2.83641875" x2="4.86918125" y2="2.921" layer="21"/>
<rectangle x1="8.001" y1="2.83641875" x2="8.33881875" y2="2.921" layer="21"/>
<rectangle x1="8.763" y1="2.83641875" x2="9.35481875" y2="2.921" layer="21"/>
<rectangle x1="10.71118125" y1="2.83641875" x2="11.13281875" y2="2.921" layer="21"/>
<rectangle x1="11.72718125" y1="2.83641875" x2="12.065" y2="2.921" layer="21"/>
<rectangle x1="12.827" y1="2.83641875" x2="13.16481875" y2="2.921" layer="21"/>
<rectangle x1="14.859" y1="2.83641875" x2="15.19681875" y2="2.921" layer="21"/>
<rectangle x1="17.907" y1="2.83641875" x2="18.24481875" y2="2.921" layer="21"/>
<rectangle x1="18.75281875" y1="2.83641875" x2="19.177" y2="2.921" layer="21"/>
<rectangle x1="20.53081875" y1="2.83641875" x2="20.955" y2="2.921" layer="21"/>
<rectangle x1="21.463" y1="2.83641875" x2="21.80081875" y2="2.921" layer="21"/>
<rectangle x1="22.90318125" y1="2.83641875" x2="23.241" y2="2.921" layer="21"/>
<rectangle x1="23.749" y1="2.83641875" x2="24.257" y2="2.921" layer="21"/>
<rectangle x1="25.527" y1="2.83641875" x2="25.95118125" y2="2.921" layer="21"/>
<rectangle x1="26.797" y1="2.83641875" x2="27.13481875" y2="2.921" layer="21"/>
<rectangle x1="30.26918125" y1="2.83641875" x2="30.607" y2="2.921" layer="21"/>
<rectangle x1="30.94481875" y1="2.83641875" x2="31.53918125" y2="2.921" layer="21"/>
<rectangle x1="32.893" y1="2.83641875" x2="33.31718125" y2="2.921" layer="21"/>
<rectangle x1="33.99281875" y1="2.83641875" x2="34.417" y2="2.921" layer="21"/>
<rectangle x1="35.77081875" y1="2.83641875" x2="36.195" y2="2.921" layer="21"/>
<rectangle x1="4.52881875" y1="2.921" x2="4.86918125" y2="3.00558125" layer="21"/>
<rectangle x1="8.001" y1="2.921" x2="8.33881875" y2="3.00558125" layer="21"/>
<rectangle x1="8.763" y1="2.921" x2="9.44118125" y2="3.00558125" layer="21"/>
<rectangle x1="10.62481875" y1="2.921" x2="11.049" y2="3.00558125" layer="21"/>
<rectangle x1="11.72718125" y1="2.921" x2="12.065" y2="3.00558125" layer="21"/>
<rectangle x1="12.827" y1="2.921" x2="13.16481875" y2="3.00558125" layer="21"/>
<rectangle x1="14.859" y1="2.921" x2="15.19681875" y2="3.00558125" layer="21"/>
<rectangle x1="17.907" y1="2.921" x2="18.24481875" y2="3.00558125" layer="21"/>
<rectangle x1="18.83918125" y1="2.921" x2="19.26081875" y2="3.00558125" layer="21"/>
<rectangle x1="20.447" y1="2.921" x2="20.87118125" y2="3.00558125" layer="21"/>
<rectangle x1="21.463" y1="2.921" x2="21.88718125" y2="3.00558125" layer="21"/>
<rectangle x1="22.81681875" y1="2.921" x2="23.241" y2="3.00558125" layer="21"/>
<rectangle x1="23.83281875" y1="2.921" x2="24.34081875" y2="3.00558125" layer="21"/>
<rectangle x1="25.44318125" y1="2.921" x2="25.95118125" y2="3.00558125" layer="21"/>
<rectangle x1="26.797" y1="2.921" x2="27.22118125" y2="3.00558125" layer="21"/>
<rectangle x1="30.26918125" y1="2.921" x2="30.607" y2="3.00558125" layer="21"/>
<rectangle x1="30.94481875" y1="2.921" x2="31.623" y2="3.00558125" layer="21"/>
<rectangle x1="32.80918125" y1="2.921" x2="33.23081875" y2="3.00558125" layer="21"/>
<rectangle x1="34.07918125" y1="2.921" x2="34.50081875" y2="3.00558125" layer="21"/>
<rectangle x1="35.687" y1="2.921" x2="36.195" y2="3.00558125" layer="21"/>
<rectangle x1="4.52881875" y1="3.00558125" x2="4.86918125" y2="3.09041875" layer="21"/>
<rectangle x1="8.001" y1="3.00558125" x2="8.33881875" y2="3.09041875" layer="21"/>
<rectangle x1="8.763" y1="3.00558125" x2="9.60881875" y2="3.09041875" layer="21"/>
<rectangle x1="10.541" y1="3.00558125" x2="11.049" y2="3.09041875" layer="21"/>
<rectangle x1="11.72718125" y1="3.00558125" x2="12.065" y2="3.09041875" layer="21"/>
<rectangle x1="12.827" y1="3.00558125" x2="13.16481875" y2="3.09041875" layer="21"/>
<rectangle x1="14.859" y1="3.00558125" x2="15.19681875" y2="3.09041875" layer="21"/>
<rectangle x1="17.907" y1="3.00558125" x2="18.24481875" y2="3.09041875" layer="21"/>
<rectangle x1="18.83918125" y1="3.00558125" x2="19.431" y2="3.09041875" layer="21"/>
<rectangle x1="20.27681875" y1="3.00558125" x2="20.87118125" y2="3.09041875" layer="21"/>
<rectangle x1="21.54681875" y1="3.00558125" x2="21.971" y2="3.09041875" layer="21"/>
<rectangle x1="22.733" y1="3.00558125" x2="23.15718125" y2="3.09041875" layer="21"/>
<rectangle x1="23.91918125" y1="3.00558125" x2="24.42718125" y2="3.09041875" layer="21"/>
<rectangle x1="25.273" y1="3.00558125" x2="25.86481875" y2="3.09041875" layer="21"/>
<rectangle x1="26.88081875" y1="3.00558125" x2="27.22118125" y2="3.09041875" layer="21"/>
<rectangle x1="30.18281875" y1="3.00558125" x2="30.607" y2="3.09041875" layer="21"/>
<rectangle x1="30.94481875" y1="3.00558125" x2="31.79318125" y2="3.09041875" layer="21"/>
<rectangle x1="32.639" y1="3.00558125" x2="33.23081875" y2="3.09041875" layer="21"/>
<rectangle x1="34.163" y1="3.00558125" x2="34.671" y2="3.09041875" layer="21"/>
<rectangle x1="35.60318125" y1="3.00558125" x2="36.11118125" y2="3.09041875" layer="21"/>
<rectangle x1="4.52881875" y1="3.09041875" x2="4.86918125" y2="3.175" layer="21"/>
<rectangle x1="8.001" y1="3.09041875" x2="8.33881875" y2="3.175" layer="21"/>
<rectangle x1="8.763" y1="3.09041875" x2="9.10081875" y2="3.175" layer="21"/>
<rectangle x1="9.18718125" y1="3.09041875" x2="9.86281875" y2="3.175" layer="21"/>
<rectangle x1="10.20318125" y1="3.09041875" x2="10.96518125" y2="3.175" layer="21"/>
<rectangle x1="11.72718125" y1="3.09041875" x2="12.065" y2="3.175" layer="21"/>
<rectangle x1="12.827" y1="3.09041875" x2="13.16481875" y2="3.175" layer="21"/>
<rectangle x1="14.859" y1="3.09041875" x2="15.19681875" y2="3.175" layer="21"/>
<rectangle x1="17.907" y1="3.09041875" x2="18.24481875" y2="3.175" layer="21"/>
<rectangle x1="19.00681875" y1="3.09041875" x2="19.685" y2="3.175" layer="21"/>
<rectangle x1="20.02281875" y1="3.09041875" x2="20.78481875" y2="3.175" layer="21"/>
<rectangle x1="21.54681875" y1="3.09041875" x2="22.225" y2="3.175" layer="21"/>
<rectangle x1="22.56281875" y1="3.09041875" x2="23.15718125" y2="3.175" layer="21"/>
<rectangle x1="24.003" y1="3.09041875" x2="24.68118125" y2="3.175" layer="21"/>
<rectangle x1="25.10281875" y1="3.09041875" x2="25.781" y2="3.175" layer="21"/>
<rectangle x1="26.88081875" y1="3.09041875" x2="27.22118125" y2="3.175" layer="21"/>
<rectangle x1="30.18281875" y1="3.09041875" x2="30.52318125" y2="3.175" layer="21"/>
<rectangle x1="30.94481875" y1="3.09041875" x2="32.04718125" y2="3.175" layer="21"/>
<rectangle x1="32.385" y1="3.09041875" x2="33.06318125" y2="3.175" layer="21"/>
<rectangle x1="34.24681875" y1="3.09041875" x2="34.925" y2="3.175" layer="21"/>
<rectangle x1="35.26281875" y1="3.09041875" x2="36.02481875" y2="3.175" layer="21"/>
<rectangle x1="4.52881875" y1="3.175" x2="4.86918125" y2="3.25958125" layer="21"/>
<rectangle x1="8.001" y1="3.175" x2="8.33881875" y2="3.25958125" layer="21"/>
<rectangle x1="8.763" y1="3.175" x2="9.10081875" y2="3.25958125" layer="21"/>
<rectangle x1="9.271" y1="3.175" x2="10.87881875" y2="3.25958125" layer="21"/>
<rectangle x1="11.72718125" y1="3.175" x2="12.065" y2="3.25958125" layer="21"/>
<rectangle x1="12.40281875" y1="3.175" x2="14.26718125" y2="3.25958125" layer="21"/>
<rectangle x1="14.859" y1="3.175" x2="15.19681875" y2="3.25958125" layer="21"/>
<rectangle x1="17.907" y1="3.175" x2="18.24481875" y2="3.25958125" layer="21"/>
<rectangle x1="19.09318125" y1="3.175" x2="20.701" y2="3.25958125" layer="21"/>
<rectangle x1="21.63318125" y1="3.175" x2="23.07081875" y2="3.25958125" layer="21"/>
<rectangle x1="24.08681875" y1="3.175" x2="25.69718125" y2="3.25958125" layer="21"/>
<rectangle x1="26.88081875" y1="3.175" x2="27.22118125" y2="3.25958125" layer="21"/>
<rectangle x1="30.18281875" y1="3.175" x2="30.52318125" y2="3.25958125" layer="21"/>
<rectangle x1="30.94481875" y1="3.175" x2="31.28518125" y2="3.25958125" layer="21"/>
<rectangle x1="31.45281875" y1="3.175" x2="33.06318125" y2="3.25958125" layer="21"/>
<rectangle x1="34.33318125" y1="3.175" x2="35.941" y2="3.25958125" layer="21"/>
<rectangle x1="4.52881875" y1="3.25958125" x2="4.86918125" y2="3.34441875" layer="21"/>
<rectangle x1="8.001" y1="3.25958125" x2="8.33881875" y2="3.34441875" layer="21"/>
<rectangle x1="8.763" y1="3.25958125" x2="9.10081875" y2="3.34441875" layer="21"/>
<rectangle x1="9.35481875" y1="3.25958125" x2="10.71118125" y2="3.34441875" layer="21"/>
<rectangle x1="11.72718125" y1="3.25958125" x2="12.065" y2="3.34441875" layer="21"/>
<rectangle x1="12.319" y1="3.25958125" x2="14.26718125" y2="3.34441875" layer="21"/>
<rectangle x1="14.859" y1="3.25958125" x2="15.19681875" y2="3.34441875" layer="21"/>
<rectangle x1="17.82318125" y1="3.25958125" x2="18.161" y2="3.34441875" layer="21"/>
<rectangle x1="19.177" y1="3.25958125" x2="20.53081875" y2="3.34441875" layer="21"/>
<rectangle x1="21.717" y1="3.25958125" x2="22.987" y2="3.34441875" layer="21"/>
<rectangle x1="24.17318125" y1="3.25958125" x2="25.61081875" y2="3.34441875" layer="21"/>
<rectangle x1="26.88081875" y1="3.25958125" x2="27.305" y2="3.34441875" layer="21"/>
<rectangle x1="30.099" y1="3.25958125" x2="30.52318125" y2="3.34441875" layer="21"/>
<rectangle x1="30.94481875" y1="3.25958125" x2="31.28518125" y2="3.34441875" layer="21"/>
<rectangle x1="31.53918125" y1="3.25958125" x2="32.893" y2="3.34441875" layer="21"/>
<rectangle x1="34.417" y1="3.25958125" x2="35.77081875" y2="3.34441875" layer="21"/>
<rectangle x1="4.52881875" y1="3.34441875" x2="4.86918125" y2="3.429" layer="21"/>
<rectangle x1="8.001" y1="3.34441875" x2="8.33881875" y2="3.429" layer="21"/>
<rectangle x1="8.763" y1="3.34441875" x2="9.10081875" y2="3.429" layer="21"/>
<rectangle x1="9.525" y1="3.34441875" x2="10.541" y2="3.429" layer="21"/>
<rectangle x1="11.72718125" y1="3.34441875" x2="12.065" y2="3.429" layer="21"/>
<rectangle x1="12.40281875" y1="3.34441875" x2="14.26718125" y2="3.429" layer="21"/>
<rectangle x1="14.859" y1="3.34441875" x2="15.19681875" y2="3.429" layer="21"/>
<rectangle x1="17.82318125" y1="3.34441875" x2="18.161" y2="3.429" layer="21"/>
<rectangle x1="19.34718125" y1="3.34441875" x2="20.36318125" y2="3.429" layer="21"/>
<rectangle x1="21.88718125" y1="3.34441875" x2="22.81681875" y2="3.429" layer="21"/>
<rectangle x1="24.34081875" y1="3.34441875" x2="25.35681875" y2="3.429" layer="21"/>
<rectangle x1="26.96718125" y1="3.34441875" x2="27.305" y2="3.429" layer="21"/>
<rectangle x1="30.099" y1="3.34441875" x2="30.43681875" y2="3.429" layer="21"/>
<rectangle x1="30.94481875" y1="3.34441875" x2="31.28518125" y2="3.429" layer="21"/>
<rectangle x1="31.70681875" y1="3.34441875" x2="32.72281875" y2="3.429" layer="21"/>
<rectangle x1="34.58718125" y1="3.34441875" x2="35.60318125" y2="3.429" layer="21"/>
<rectangle x1="4.52881875" y1="3.429" x2="4.86918125" y2="3.51358125" layer="21"/>
<rectangle x1="8.001" y1="3.429" x2="8.33881875" y2="3.51358125" layer="21"/>
<rectangle x1="8.84681875" y1="3.429" x2="9.017" y2="3.51358125" layer="21"/>
<rectangle x1="9.779" y1="3.429" x2="10.287" y2="3.51358125" layer="21"/>
<rectangle x1="11.811" y1="3.429" x2="11.98118125" y2="3.51358125" layer="21"/>
<rectangle x1="12.40281875" y1="3.429" x2="14.18081875" y2="3.51358125" layer="21"/>
<rectangle x1="14.859" y1="3.429" x2="15.19681875" y2="3.51358125" layer="21"/>
<rectangle x1="17.82318125" y1="3.429" x2="18.161" y2="3.51358125" layer="21"/>
<rectangle x1="19.60118125" y1="3.429" x2="20.10918125" y2="3.51358125" layer="21"/>
<rectangle x1="22.14118125" y1="3.429" x2="22.56281875" y2="3.51358125" layer="21"/>
<rectangle x1="24.59481875" y1="3.429" x2="25.10281875" y2="3.51358125" layer="21"/>
<rectangle x1="26.96718125" y1="3.429" x2="27.38881875" y2="3.51358125" layer="21"/>
<rectangle x1="30.01518125" y1="3.429" x2="30.43681875" y2="3.51358125" layer="21"/>
<rectangle x1="31.03118125" y1="3.429" x2="31.19881875" y2="3.51358125" layer="21"/>
<rectangle x1="31.96081875" y1="3.429" x2="32.46881875" y2="3.51358125" layer="21"/>
<rectangle x1="34.84118125" y1="3.429" x2="35.34918125" y2="3.51358125" layer="21"/>
<rectangle x1="4.52881875" y1="3.51358125" x2="4.86918125" y2="3.59841875" layer="21"/>
<rectangle x1="8.001" y1="3.51358125" x2="8.33881875" y2="3.59841875" layer="21"/>
<rectangle x1="12.827" y1="3.51358125" x2="13.16481875" y2="3.59841875" layer="21"/>
<rectangle x1="14.859" y1="3.51358125" x2="15.19681875" y2="3.59841875" layer="21"/>
<rectangle x1="17.73681875" y1="3.51358125" x2="18.07718125" y2="3.59841875" layer="21"/>
<rectangle x1="27.051" y1="3.51358125" x2="27.47518125" y2="3.59841875" layer="21"/>
<rectangle x1="30.01518125" y1="3.51358125" x2="30.353" y2="3.59841875" layer="21"/>
<rectangle x1="4.52881875" y1="3.59841875" x2="4.86918125" y2="3.683" layer="21"/>
<rectangle x1="8.001" y1="3.59841875" x2="8.33881875" y2="3.683" layer="21"/>
<rectangle x1="11.811" y1="3.59841875" x2="11.98118125" y2="3.683" layer="21"/>
<rectangle x1="12.827" y1="3.59841875" x2="13.16481875" y2="3.683" layer="21"/>
<rectangle x1="14.859" y1="3.59841875" x2="15.19681875" y2="3.683" layer="21"/>
<rectangle x1="17.653" y1="3.59841875" x2="18.07718125" y2="3.683" layer="21"/>
<rectangle x1="27.13481875" y1="3.59841875" x2="27.47518125" y2="3.683" layer="21"/>
<rectangle x1="29.92881875" y1="3.59841875" x2="30.353" y2="3.683" layer="21"/>
<rectangle x1="4.52881875" y1="3.683" x2="4.86918125" y2="3.76758125" layer="21"/>
<rectangle x1="8.001" y1="3.683" x2="8.33881875" y2="3.76758125" layer="21"/>
<rectangle x1="11.72718125" y1="3.683" x2="12.065" y2="3.76758125" layer="21"/>
<rectangle x1="12.827" y1="3.683" x2="13.16481875" y2="3.76758125" layer="21"/>
<rectangle x1="14.859" y1="3.683" x2="15.19681875" y2="3.76758125" layer="21"/>
<rectangle x1="17.653" y1="3.683" x2="18.07718125" y2="3.76758125" layer="21"/>
<rectangle x1="27.13481875" y1="3.683" x2="27.559" y2="3.76758125" layer="21"/>
<rectangle x1="29.845" y1="3.683" x2="30.26918125" y2="3.76758125" layer="21"/>
<rectangle x1="4.52881875" y1="3.76758125" x2="4.86918125" y2="3.85241875" layer="21"/>
<rectangle x1="8.001" y1="3.76758125" x2="8.33881875" y2="3.85241875" layer="21"/>
<rectangle x1="11.72718125" y1="3.76758125" x2="12.065" y2="3.85241875" layer="21"/>
<rectangle x1="12.827" y1="3.76758125" x2="13.16481875" y2="3.85241875" layer="21"/>
<rectangle x1="14.859" y1="3.76758125" x2="15.19681875" y2="3.85241875" layer="21"/>
<rectangle x1="17.56918125" y1="3.76758125" x2="17.99081875" y2="3.85241875" layer="21"/>
<rectangle x1="27.22118125" y1="3.76758125" x2="27.72918125" y2="3.85241875" layer="21"/>
<rectangle x1="29.76118125" y1="3.76758125" x2="30.18281875" y2="3.85241875" layer="21"/>
<rectangle x1="4.52881875" y1="3.85241875" x2="4.86918125" y2="3.937" layer="21"/>
<rectangle x1="8.001" y1="3.85241875" x2="8.33881875" y2="3.937" layer="21"/>
<rectangle x1="11.72718125" y1="3.85241875" x2="12.065" y2="3.937" layer="21"/>
<rectangle x1="12.827" y1="3.85241875" x2="13.16481875" y2="3.937" layer="21"/>
<rectangle x1="14.859" y1="3.85241875" x2="15.19681875" y2="3.937" layer="21"/>
<rectangle x1="17.48281875" y1="3.85241875" x2="17.907" y2="3.937" layer="21"/>
<rectangle x1="27.305" y1="3.85241875" x2="27.813" y2="3.937" layer="21"/>
<rectangle x1="29.591" y1="3.85241875" x2="30.099" y2="3.937" layer="21"/>
<rectangle x1="4.52881875" y1="3.937" x2="4.86918125" y2="4.02158125" layer="21"/>
<rectangle x1="8.001" y1="3.937" x2="8.33881875" y2="4.02158125" layer="21"/>
<rectangle x1="11.72718125" y1="3.937" x2="12.065" y2="4.02158125" layer="21"/>
<rectangle x1="12.827" y1="3.937" x2="13.16481875" y2="4.02158125" layer="21"/>
<rectangle x1="14.859" y1="3.937" x2="15.19681875" y2="4.02158125" layer="21"/>
<rectangle x1="17.399" y1="3.937" x2="17.907" y2="4.02158125" layer="21"/>
<rectangle x1="27.38881875" y1="3.937" x2="27.89681875" y2="4.02158125" layer="21"/>
<rectangle x1="29.50718125" y1="3.937" x2="30.099" y2="4.02158125" layer="21"/>
<rectangle x1="4.52881875" y1="4.02158125" x2="4.86918125" y2="4.10641875" layer="21"/>
<rectangle x1="8.001" y1="4.02158125" x2="8.33881875" y2="4.10641875" layer="21"/>
<rectangle x1="11.811" y1="4.02158125" x2="11.98118125" y2="4.10641875" layer="21"/>
<rectangle x1="12.827" y1="4.02158125" x2="13.16481875" y2="4.10641875" layer="21"/>
<rectangle x1="14.859" y1="4.02158125" x2="15.19681875" y2="4.10641875" layer="21"/>
<rectangle x1="17.31518125" y1="4.02158125" x2="17.82318125" y2="4.10641875" layer="21"/>
<rectangle x1="27.47518125" y1="4.02158125" x2="28.067" y2="4.10641875" layer="21"/>
<rectangle x1="29.337" y1="4.02158125" x2="30.01518125" y2="4.10641875" layer="21"/>
<rectangle x1="4.52881875" y1="4.10641875" x2="4.86918125" y2="4.191" layer="21"/>
<rectangle x1="8.001" y1="4.10641875" x2="8.33881875" y2="4.191" layer="21"/>
<rectangle x1="12.827" y1="4.10641875" x2="13.16481875" y2="4.191" layer="21"/>
<rectangle x1="14.859" y1="4.10641875" x2="15.19681875" y2="4.191" layer="21"/>
<rectangle x1="17.22881875" y1="4.10641875" x2="17.73681875" y2="4.191" layer="21"/>
<rectangle x1="27.559" y1="4.10641875" x2="28.321" y2="4.191" layer="21"/>
<rectangle x1="29.083" y1="4.10641875" x2="29.845" y2="4.191" layer="21"/>
<rectangle x1="4.52881875" y1="4.191" x2="4.86918125" y2="4.27558125" layer="21"/>
<rectangle x1="8.001" y1="4.191" x2="8.33881875" y2="4.27558125" layer="21"/>
<rectangle x1="12.827" y1="4.191" x2="13.16481875" y2="4.27558125" layer="21"/>
<rectangle x1="14.859" y1="4.191" x2="15.19681875" y2="4.27558125" layer="21"/>
<rectangle x1="17.06118125" y1="4.191" x2="17.653" y2="4.27558125" layer="21"/>
<rectangle x1="27.64281875" y1="4.191" x2="29.76118125" y2="4.27558125" layer="21"/>
<rectangle x1="4.52881875" y1="4.27558125" x2="4.86918125" y2="4.36041875" layer="21"/>
<rectangle x1="8.001" y1="4.27558125" x2="8.33881875" y2="4.36041875" layer="21"/>
<rectangle x1="12.827" y1="4.27558125" x2="13.16481875" y2="4.36041875" layer="21"/>
<rectangle x1="14.94281875" y1="4.27558125" x2="15.28318125" y2="4.36041875" layer="21"/>
<rectangle x1="16.80718125" y1="4.27558125" x2="17.56918125" y2="4.36041875" layer="21"/>
<rectangle x1="27.813" y1="4.27558125" x2="29.591" y2="4.36041875" layer="21"/>
<rectangle x1="4.52881875" y1="4.36041875" x2="4.86918125" y2="4.445" layer="21"/>
<rectangle x1="8.001" y1="4.36041875" x2="8.33881875" y2="4.445" layer="21"/>
<rectangle x1="12.827" y1="4.36041875" x2="13.16481875" y2="4.445" layer="21"/>
<rectangle x1="14.859" y1="4.36041875" x2="17.399" y2="4.445" layer="21"/>
<rectangle x1="27.98318125" y1="4.36041875" x2="29.42081875" y2="4.445" layer="21"/>
<rectangle x1="4.52881875" y1="4.445" x2="4.86918125" y2="4.52958125" layer="21"/>
<rectangle x1="8.001" y1="4.445" x2="8.255" y2="4.52958125" layer="21"/>
<rectangle x1="12.827" y1="4.445" x2="13.081" y2="4.52958125" layer="21"/>
<rectangle x1="14.859" y1="4.445" x2="17.31518125" y2="4.52958125" layer="21"/>
<rectangle x1="28.321" y1="4.445" x2="29.16681875" y2="4.52958125" layer="21"/>
<rectangle x1="14.859" y1="4.52958125" x2="17.145" y2="4.61441875" layer="21"/>
<rectangle x1="14.94281875" y1="4.61441875" x2="16.891" y2="4.699" layer="21"/>
<rectangle x1="7.239" y1="6.477" x2="32.46881875" y2="6.56158125" layer="21"/>
<rectangle x1="6.64718125" y1="6.56158125" x2="33.147" y2="6.64641875" layer="21"/>
<rectangle x1="6.30681875" y1="6.64641875" x2="33.48481875" y2="6.731" layer="21"/>
<rectangle x1="5.969" y1="6.731" x2="33.82518125" y2="6.81558125" layer="21"/>
<rectangle x1="5.63118125" y1="6.81558125" x2="34.07918125" y2="6.90041875" layer="21"/>
<rectangle x1="5.37718125" y1="6.90041875" x2="34.33318125" y2="6.985" layer="21"/>
<rectangle x1="5.207" y1="6.985" x2="34.58718125" y2="7.06958125" layer="21"/>
<rectangle x1="4.953" y1="7.06958125" x2="34.75481875" y2="7.15441875" layer="21"/>
<rectangle x1="4.78281875" y1="7.15441875" x2="34.925" y2="7.239" layer="21"/>
<rectangle x1="4.61518125" y1="7.239" x2="35.179" y2="7.32358125" layer="21"/>
<rectangle x1="4.445" y1="7.32358125" x2="35.34918125" y2="7.40841875" layer="21"/>
<rectangle x1="4.27481875" y1="7.40841875" x2="35.51681875" y2="7.493" layer="21"/>
<rectangle x1="4.10718125" y1="7.493" x2="35.60318125" y2="7.57758125" layer="21"/>
<rectangle x1="4.02081875" y1="7.57758125" x2="7.15518125" y2="7.66241875" layer="21"/>
<rectangle x1="19.51481875" y1="7.57758125" x2="35.77081875" y2="7.66241875" layer="21"/>
<rectangle x1="3.85318125" y1="7.66241875" x2="6.64718125" y2="7.747" layer="21"/>
<rectangle x1="19.431" y1="7.66241875" x2="35.941" y2="7.747" layer="21"/>
<rectangle x1="3.76681875" y1="7.747" x2="6.30681875" y2="7.83158125" layer="21"/>
<rectangle x1="19.431" y1="7.747" x2="36.02481875" y2="7.83158125" layer="21"/>
<rectangle x1="3.59918125" y1="7.83158125" x2="6.05281875" y2="7.91641875" layer="21"/>
<rectangle x1="19.34718125" y1="7.83158125" x2="36.195" y2="7.91641875" layer="21"/>
<rectangle x1="3.51281875" y1="7.91641875" x2="5.79881875" y2="8.001" layer="21"/>
<rectangle x1="19.26081875" y1="7.91641875" x2="36.27881875" y2="8.001" layer="21"/>
<rectangle x1="3.34518125" y1="8.001" x2="5.54481875" y2="8.08558125" layer="21"/>
<rectangle x1="19.26081875" y1="8.001" x2="36.36518125" y2="8.08558125" layer="21"/>
<rectangle x1="3.25881875" y1="8.08558125" x2="5.37718125" y2="8.17041875" layer="21"/>
<rectangle x1="19.177" y1="8.08558125" x2="36.53281875" y2="8.17041875" layer="21"/>
<rectangle x1="3.175" y1="8.17041875" x2="5.207" y2="8.255" layer="21"/>
<rectangle x1="19.177" y1="8.17041875" x2="36.61918125" y2="8.255" layer="21"/>
<rectangle x1="3.00481875" y1="8.255" x2="5.03681875" y2="8.33958125" layer="21"/>
<rectangle x1="19.09318125" y1="8.255" x2="36.703" y2="8.33958125" layer="21"/>
<rectangle x1="3.00481875" y1="8.33958125" x2="4.86918125" y2="8.42441875" layer="21"/>
<rectangle x1="19.09318125" y1="8.33958125" x2="36.78681875" y2="8.42441875" layer="21"/>
<rectangle x1="2.83718125" y1="8.42441875" x2="4.699" y2="8.509" layer="21"/>
<rectangle x1="19.00681875" y1="8.42441875" x2="36.957" y2="8.509" layer="21"/>
<rectangle x1="2.75081875" y1="8.509" x2="4.61518125" y2="8.59358125" layer="21"/>
<rectangle x1="19.00681875" y1="8.509" x2="37.04081875" y2="8.59358125" layer="21"/>
<rectangle x1="2.667" y1="8.59358125" x2="4.445" y2="8.67841875" layer="21"/>
<rectangle x1="18.923" y1="8.59358125" x2="37.12718125" y2="8.67841875" layer="21"/>
<rectangle x1="2.58318125" y1="8.67841875" x2="4.27481875" y2="8.763" layer="21"/>
<rectangle x1="18.923" y1="8.67841875" x2="37.211" y2="8.763" layer="21"/>
<rectangle x1="2.49681875" y1="8.763" x2="4.191" y2="8.84758125" layer="21"/>
<rectangle x1="18.83918125" y1="8.763" x2="37.29481875" y2="8.84758125" layer="21"/>
<rectangle x1="2.413" y1="8.84758125" x2="4.02081875" y2="8.93241875" layer="21"/>
<rectangle x1="18.83918125" y1="8.84758125" x2="37.38118125" y2="8.93241875" layer="21"/>
<rectangle x1="2.32918125" y1="8.93241875" x2="3.937" y2="9.017" layer="21"/>
<rectangle x1="18.75281875" y1="8.93241875" x2="37.465" y2="9.017" layer="21"/>
<rectangle x1="2.24281875" y1="9.017" x2="3.85318125" y2="9.10158125" layer="21"/>
<rectangle x1="18.75281875" y1="9.017" x2="37.54881875" y2="9.10158125" layer="21"/>
<rectangle x1="2.159" y1="9.10158125" x2="3.76681875" y2="9.18641875" layer="21"/>
<rectangle x1="18.75281875" y1="9.10158125" x2="37.63518125" y2="9.18641875" layer="21"/>
<rectangle x1="2.07518125" y1="9.18641875" x2="3.683" y2="9.271" layer="21"/>
<rectangle x1="18.669" y1="9.18641875" x2="37.63518125" y2="9.271" layer="21"/>
<rectangle x1="2.07518125" y1="9.271" x2="3.51281875" y2="9.35558125" layer="21"/>
<rectangle x1="18.669" y1="9.271" x2="37.719" y2="9.35558125" layer="21"/>
<rectangle x1="1.98881875" y1="9.35558125" x2="3.429" y2="9.44041875" layer="21"/>
<rectangle x1="18.669" y1="9.35558125" x2="37.80281875" y2="9.44041875" layer="21"/>
<rectangle x1="1.905" y1="9.44041875" x2="3.34518125" y2="9.525" layer="21"/>
<rectangle x1="18.58518125" y1="9.44041875" x2="37.88918125" y2="9.525" layer="21"/>
<rectangle x1="1.82118125" y1="9.525" x2="3.25881875" y2="9.60958125" layer="21"/>
<rectangle x1="18.58518125" y1="9.525" x2="37.973" y2="9.60958125" layer="21"/>
<rectangle x1="1.73481875" y1="9.60958125" x2="3.175" y2="9.69441875" layer="21"/>
<rectangle x1="18.49881875" y1="9.60958125" x2="37.973" y2="9.69441875" layer="21"/>
<rectangle x1="1.73481875" y1="9.69441875" x2="3.09118125" y2="9.779" layer="21"/>
<rectangle x1="18.49881875" y1="9.69441875" x2="38.05681875" y2="9.779" layer="21"/>
<rectangle x1="1.651" y1="9.779" x2="3.00481875" y2="9.86358125" layer="21"/>
<rectangle x1="18.49881875" y1="9.779" x2="38.14318125" y2="9.86358125" layer="21"/>
<rectangle x1="1.56718125" y1="9.86358125" x2="2.921" y2="9.94841875" layer="21"/>
<rectangle x1="18.415" y1="9.86358125" x2="38.227" y2="9.94841875" layer="21"/>
<rectangle x1="1.56718125" y1="9.94841875" x2="2.921" y2="10.033" layer="21"/>
<rectangle x1="18.415" y1="9.94841875" x2="38.227" y2="10.033" layer="21"/>
<rectangle x1="1.48081875" y1="10.033" x2="2.83718125" y2="10.11758125" layer="21"/>
<rectangle x1="18.415" y1="10.033" x2="38.31081875" y2="10.11758125" layer="21"/>
<rectangle x1="1.397" y1="10.11758125" x2="2.75081875" y2="10.20241875" layer="21"/>
<rectangle x1="18.415" y1="10.11758125" x2="38.39718125" y2="10.20241875" layer="21"/>
<rectangle x1="1.397" y1="10.20241875" x2="2.667" y2="10.287" layer="21"/>
<rectangle x1="18.33118125" y1="10.20241875" x2="38.39718125" y2="10.287" layer="21"/>
<rectangle x1="1.31318125" y1="10.287" x2="2.58318125" y2="10.37158125" layer="21"/>
<rectangle x1="18.33118125" y1="10.287" x2="38.481" y2="10.37158125" layer="21"/>
<rectangle x1="1.22681875" y1="10.37158125" x2="2.58318125" y2="10.45641875" layer="21"/>
<rectangle x1="18.33118125" y1="10.37158125" x2="38.481" y2="10.45641875" layer="21"/>
<rectangle x1="1.22681875" y1="10.45641875" x2="2.49681875" y2="10.541" layer="21"/>
<rectangle x1="18.33118125" y1="10.45641875" x2="38.56481875" y2="10.541" layer="21"/>
<rectangle x1="1.143" y1="10.541" x2="2.413" y2="10.62558125" layer="21"/>
<rectangle x1="18.24481875" y1="10.541" x2="38.56481875" y2="10.62558125" layer="21"/>
<rectangle x1="1.143" y1="10.62558125" x2="2.413" y2="10.71041875" layer="21"/>
<rectangle x1="18.24481875" y1="10.62558125" x2="38.65118125" y2="10.71041875" layer="21"/>
<rectangle x1="1.05918125" y1="10.71041875" x2="2.32918125" y2="10.795" layer="21"/>
<rectangle x1="18.24481875" y1="10.71041875" x2="38.65118125" y2="10.795" layer="21"/>
<rectangle x1="1.05918125" y1="10.795" x2="2.24281875" y2="10.87958125" layer="21"/>
<rectangle x1="18.24481875" y1="10.795" x2="38.735" y2="10.87958125" layer="21"/>
<rectangle x1="0.97281875" y1="10.87958125" x2="2.24281875" y2="10.96441875" layer="21"/>
<rectangle x1="18.24481875" y1="10.87958125" x2="38.81881875" y2="10.96441875" layer="21"/>
<rectangle x1="0.97281875" y1="10.96441875" x2="2.159" y2="11.049" layer="21"/>
<rectangle x1="18.161" y1="10.96441875" x2="38.81881875" y2="11.049" layer="21"/>
<rectangle x1="0.889" y1="11.049" x2="2.159" y2="11.13358125" layer="21"/>
<rectangle x1="18.161" y1="11.049" x2="38.90518125" y2="11.13358125" layer="21"/>
<rectangle x1="0.889" y1="11.13358125" x2="2.07518125" y2="11.21841875" layer="21"/>
<rectangle x1="7.40918125" y1="11.13358125" x2="8.763" y2="11.21841875" layer="21"/>
<rectangle x1="11.557" y1="11.13358125" x2="13.92681875" y2="11.21841875" layer="21"/>
<rectangle x1="18.161" y1="11.13358125" x2="21.63318125" y2="11.21841875" layer="21"/>
<rectangle x1="23.07081875" y1="11.13358125" x2="33.06318125" y2="11.21841875" layer="21"/>
<rectangle x1="33.147" y1="11.13358125" x2="38.90518125" y2="11.21841875" layer="21"/>
<rectangle x1="0.80518125" y1="11.21841875" x2="2.07518125" y2="11.303" layer="21"/>
<rectangle x1="7.06881875" y1="11.21841875" x2="9.10081875" y2="11.303" layer="21"/>
<rectangle x1="11.47318125" y1="11.21841875" x2="14.18081875" y2="11.303" layer="21"/>
<rectangle x1="18.161" y1="11.21841875" x2="21.29281875" y2="11.303" layer="21"/>
<rectangle x1="23.32481875" y1="11.21841875" x2="26.45918125" y2="11.303" layer="21"/>
<rectangle x1="26.88081875" y1="11.21841875" x2="29.92881875" y2="11.303" layer="21"/>
<rectangle x1="30.43681875" y1="11.21841875" x2="32.639" y2="11.303" layer="21"/>
<rectangle x1="33.82518125" y1="11.21841875" x2="38.90518125" y2="11.303" layer="21"/>
<rectangle x1="0.80518125" y1="11.303" x2="1.98881875" y2="11.38758125" layer="21"/>
<rectangle x1="6.81481875" y1="11.303" x2="9.35481875" y2="11.38758125" layer="21"/>
<rectangle x1="11.38681875" y1="11.303" x2="14.43481875" y2="11.38758125" layer="21"/>
<rectangle x1="18.161" y1="11.303" x2="21.12518125" y2="11.38758125" layer="21"/>
<rectangle x1="23.57881875" y1="11.303" x2="26.37281875" y2="11.38758125" layer="21"/>
<rectangle x1="27.051" y1="11.303" x2="29.845" y2="11.38758125" layer="21"/>
<rectangle x1="30.52318125" y1="11.303" x2="32.385" y2="11.38758125" layer="21"/>
<rectangle x1="34.163" y1="11.303" x2="38.989" y2="11.38758125" layer="21"/>
<rectangle x1="0.80518125" y1="11.38758125" x2="1.98881875" y2="11.47241875" layer="21"/>
<rectangle x1="6.64718125" y1="11.38758125" x2="9.525" y2="11.47241875" layer="21"/>
<rectangle x1="11.38681875" y1="11.38758125" x2="14.605" y2="11.47241875" layer="21"/>
<rectangle x1="18.07718125" y1="11.38758125" x2="20.955" y2="11.47241875" layer="21"/>
<rectangle x1="23.749" y1="11.38758125" x2="26.289" y2="11.47241875" layer="21"/>
<rectangle x1="27.051" y1="11.38758125" x2="29.76118125" y2="11.47241875" layer="21"/>
<rectangle x1="30.52318125" y1="11.38758125" x2="32.21481875" y2="11.47241875" layer="21"/>
<rectangle x1="34.33318125" y1="11.38758125" x2="38.989" y2="11.47241875" layer="21"/>
<rectangle x1="0.71881875" y1="11.47241875" x2="1.905" y2="11.557" layer="21"/>
<rectangle x1="6.477" y1="11.47241875" x2="9.69518125" y2="11.557" layer="21"/>
<rectangle x1="11.303" y1="11.47241875" x2="14.859" y2="11.557" layer="21"/>
<rectangle x1="18.07718125" y1="11.47241875" x2="20.78481875" y2="11.557" layer="21"/>
<rectangle x1="23.91918125" y1="11.47241875" x2="26.20518125" y2="11.557" layer="21"/>
<rectangle x1="27.13481875" y1="11.47241875" x2="29.76118125" y2="11.557" layer="21"/>
<rectangle x1="30.607" y1="11.47241875" x2="32.04718125" y2="11.557" layer="21"/>
<rectangle x1="34.50081875" y1="11.47241875" x2="39.07281875" y2="11.557" layer="21"/>
<rectangle x1="0.71881875" y1="11.557" x2="1.905" y2="11.64158125" layer="21"/>
<rectangle x1="6.39318125" y1="11.557" x2="9.779" y2="11.64158125" layer="21"/>
<rectangle x1="11.303" y1="11.557" x2="14.94281875" y2="11.64158125" layer="21"/>
<rectangle x1="18.07718125" y1="11.557" x2="20.61718125" y2="11.64158125" layer="21"/>
<rectangle x1="24.08681875" y1="11.557" x2="26.20518125" y2="11.64158125" layer="21"/>
<rectangle x1="27.13481875" y1="11.557" x2="29.76118125" y2="11.64158125" layer="21"/>
<rectangle x1="30.607" y1="11.557" x2="31.96081875" y2="11.64158125" layer="21"/>
<rectangle x1="34.671" y1="11.557" x2="39.07281875" y2="11.64158125" layer="21"/>
<rectangle x1="0.635" y1="11.64158125" x2="1.82118125" y2="11.72641875" layer="21"/>
<rectangle x1="6.223" y1="11.64158125" x2="9.94918125" y2="11.72641875" layer="21"/>
<rectangle x1="11.303" y1="11.64158125" x2="15.02918125" y2="11.72641875" layer="21"/>
<rectangle x1="18.07718125" y1="11.64158125" x2="20.53081875" y2="11.72641875" layer="21"/>
<rectangle x1="24.17318125" y1="11.64158125" x2="26.20518125" y2="11.72641875" layer="21"/>
<rectangle x1="27.13481875" y1="11.64158125" x2="29.76118125" y2="11.72641875" layer="21"/>
<rectangle x1="30.607" y1="11.64158125" x2="31.877" y2="11.72641875" layer="21"/>
<rectangle x1="34.75481875" y1="11.64158125" x2="39.07281875" y2="11.72641875" layer="21"/>
<rectangle x1="0.635" y1="11.72641875" x2="1.82118125" y2="11.811" layer="21"/>
<rectangle x1="6.13918125" y1="11.72641875" x2="10.033" y2="11.811" layer="21"/>
<rectangle x1="11.303" y1="11.72641875" x2="15.19681875" y2="11.811" layer="21"/>
<rectangle x1="18.07718125" y1="11.72641875" x2="20.36318125" y2="11.811" layer="21"/>
<rectangle x1="24.34081875" y1="11.72641875" x2="26.20518125" y2="11.811" layer="21"/>
<rectangle x1="27.13481875" y1="11.72641875" x2="29.76118125" y2="11.811" layer="21"/>
<rectangle x1="30.607" y1="11.72641875" x2="31.79318125" y2="11.811" layer="21"/>
<rectangle x1="34.84118125" y1="11.72641875" x2="39.15918125" y2="11.811" layer="21"/>
<rectangle x1="0.635" y1="11.811" x2="1.73481875" y2="11.89558125" layer="21"/>
<rectangle x1="5.969" y1="11.811" x2="10.11681875" y2="11.89558125" layer="21"/>
<rectangle x1="11.38681875" y1="11.811" x2="15.28318125" y2="11.89558125" layer="21"/>
<rectangle x1="18.07718125" y1="11.811" x2="20.27681875" y2="11.89558125" layer="21"/>
<rectangle x1="24.42718125" y1="11.811" x2="26.20518125" y2="11.89558125" layer="21"/>
<rectangle x1="27.13481875" y1="11.811" x2="29.76118125" y2="11.89558125" layer="21"/>
<rectangle x1="30.607" y1="11.811" x2="31.623" y2="11.89558125" layer="21"/>
<rectangle x1="35.00881875" y1="11.811" x2="39.15918125" y2="11.89558125" layer="21"/>
<rectangle x1="0.55118125" y1="11.89558125" x2="1.73481875" y2="11.98041875" layer="21"/>
<rectangle x1="5.88518125" y1="11.89558125" x2="10.287" y2="11.98041875" layer="21"/>
<rectangle x1="11.38681875" y1="11.89558125" x2="15.367" y2="11.98041875" layer="21"/>
<rectangle x1="17.99081875" y1="11.89558125" x2="20.193" y2="11.98041875" layer="21"/>
<rectangle x1="24.511" y1="11.89558125" x2="26.20518125" y2="11.98041875" layer="21"/>
<rectangle x1="27.13481875" y1="11.89558125" x2="29.76118125" y2="11.98041875" layer="21"/>
<rectangle x1="30.607" y1="11.89558125" x2="31.623" y2="11.98041875" layer="21"/>
<rectangle x1="35.00881875" y1="11.89558125" x2="39.15918125" y2="11.98041875" layer="21"/>
<rectangle x1="0.55118125" y1="11.98041875" x2="1.73481875" y2="12.065" layer="21"/>
<rectangle x1="5.79881875" y1="11.98041875" x2="10.37081875" y2="12.065" layer="21"/>
<rectangle x1="11.47318125" y1="11.98041875" x2="15.45081875" y2="12.065" layer="21"/>
<rectangle x1="17.99081875" y1="11.98041875" x2="20.10918125" y2="12.065" layer="21"/>
<rectangle x1="24.59481875" y1="11.98041875" x2="26.20518125" y2="12.065" layer="21"/>
<rectangle x1="27.13481875" y1="11.98041875" x2="29.76118125" y2="12.065" layer="21"/>
<rectangle x1="30.607" y1="11.98041875" x2="31.53918125" y2="12.065" layer="21"/>
<rectangle x1="35.09518125" y1="11.98041875" x2="39.243" y2="12.065" layer="21"/>
<rectangle x1="0.55118125" y1="12.065" x2="1.651" y2="12.14958125" layer="21"/>
<rectangle x1="5.715" y1="12.065" x2="10.45718125" y2="12.14958125" layer="21"/>
<rectangle x1="11.557" y1="12.065" x2="15.53718125" y2="12.14958125" layer="21"/>
<rectangle x1="17.99081875" y1="12.065" x2="20.02281875" y2="12.14958125" layer="21"/>
<rectangle x1="24.68118125" y1="12.065" x2="26.20518125" y2="12.14958125" layer="21"/>
<rectangle x1="27.13481875" y1="12.065" x2="29.76118125" y2="12.14958125" layer="21"/>
<rectangle x1="30.607" y1="12.065" x2="31.45281875" y2="12.14958125" layer="21"/>
<rectangle x1="32.80918125" y1="12.065" x2="33.655" y2="12.14958125" layer="21"/>
<rectangle x1="35.179" y1="12.065" x2="39.243" y2="12.14958125" layer="21"/>
<rectangle x1="0.46481875" y1="12.14958125" x2="1.651" y2="12.23441875" layer="21"/>
<rectangle x1="5.63118125" y1="12.14958125" x2="7.91718125" y2="12.23441875" layer="21"/>
<rectangle x1="8.255" y1="12.14958125" x2="10.541" y2="12.23441875" layer="21"/>
<rectangle x1="13.335" y1="12.14958125" x2="15.621" y2="12.23441875" layer="21"/>
<rectangle x1="17.99081875" y1="12.14958125" x2="19.939" y2="12.23441875" layer="21"/>
<rectangle x1="22.225" y1="12.14958125" x2="22.56281875" y2="12.23441875" layer="21"/>
<rectangle x1="24.765" y1="12.14958125" x2="26.20518125" y2="12.23441875" layer="21"/>
<rectangle x1="27.13481875" y1="12.14958125" x2="29.76118125" y2="12.23441875" layer="21"/>
<rectangle x1="30.607" y1="12.14958125" x2="31.369" y2="12.23441875" layer="21"/>
<rectangle x1="32.639" y1="12.14958125" x2="33.909" y2="12.23441875" layer="21"/>
<rectangle x1="35.179" y1="12.14958125" x2="39.243" y2="12.23441875" layer="21"/>
<rectangle x1="0.46481875" y1="12.23441875" x2="1.651" y2="12.319" layer="21"/>
<rectangle x1="5.54481875" y1="12.23441875" x2="7.40918125" y2="12.319" layer="21"/>
<rectangle x1="8.763" y1="12.23441875" x2="10.62481875" y2="12.319" layer="21"/>
<rectangle x1="13.843" y1="12.23441875" x2="15.70481875" y2="12.319" layer="21"/>
<rectangle x1="17.99081875" y1="12.23441875" x2="19.85518125" y2="12.319" layer="21"/>
<rectangle x1="21.717" y1="12.23441875" x2="22.987" y2="12.319" layer="21"/>
<rectangle x1="24.84881875" y1="12.23441875" x2="26.20518125" y2="12.319" layer="21"/>
<rectangle x1="27.13481875" y1="12.23441875" x2="29.76118125" y2="12.319" layer="21"/>
<rectangle x1="30.607" y1="12.23441875" x2="31.369" y2="12.319" layer="21"/>
<rectangle x1="32.46881875" y1="12.23441875" x2="34.07918125" y2="12.319" layer="21"/>
<rectangle x1="35.179" y1="12.23441875" x2="39.32681875" y2="12.319" layer="21"/>
<rectangle x1="0.46481875" y1="12.319" x2="1.56718125" y2="12.40358125" layer="21"/>
<rectangle x1="5.54481875" y1="12.319" x2="7.239" y2="12.40358125" layer="21"/>
<rectangle x1="8.93318125" y1="12.319" x2="10.62481875" y2="12.40358125" layer="21"/>
<rectangle x1="14.097" y1="12.319" x2="15.79118125" y2="12.40358125" layer="21"/>
<rectangle x1="17.99081875" y1="12.319" x2="19.76881875" y2="12.40358125" layer="21"/>
<rectangle x1="21.463" y1="12.319" x2="23.241" y2="12.40358125" layer="21"/>
<rectangle x1="24.93518125" y1="12.319" x2="26.20518125" y2="12.40358125" layer="21"/>
<rectangle x1="27.13481875" y1="12.319" x2="29.76118125" y2="12.40358125" layer="21"/>
<rectangle x1="30.607" y1="12.319" x2="31.28518125" y2="12.40358125" layer="21"/>
<rectangle x1="32.385" y1="12.319" x2="34.24681875" y2="12.40358125" layer="21"/>
<rectangle x1="35.179" y1="12.319" x2="39.32681875" y2="12.40358125" layer="21"/>
<rectangle x1="0.46481875" y1="12.40358125" x2="1.56718125" y2="12.48841875" layer="21"/>
<rectangle x1="5.461" y1="12.40358125" x2="7.06881875" y2="12.48841875" layer="21"/>
<rectangle x1="9.10081875" y1="12.40358125" x2="10.71118125" y2="12.48841875" layer="21"/>
<rectangle x1="14.26718125" y1="12.40358125" x2="15.875" y2="12.48841875" layer="21"/>
<rectangle x1="17.99081875" y1="12.40358125" x2="19.685" y2="12.48841875" layer="21"/>
<rectangle x1="21.29281875" y1="12.40358125" x2="23.41118125" y2="12.48841875" layer="21"/>
<rectangle x1="25.019" y1="12.40358125" x2="26.20518125" y2="12.48841875" layer="21"/>
<rectangle x1="27.13481875" y1="12.40358125" x2="29.76118125" y2="12.48841875" layer="21"/>
<rectangle x1="30.607" y1="12.40358125" x2="31.28518125" y2="12.48841875" layer="21"/>
<rectangle x1="32.30118125" y1="12.40358125" x2="34.33318125" y2="12.48841875" layer="21"/>
<rectangle x1="35.09518125" y1="12.40358125" x2="39.32681875" y2="12.48841875" layer="21"/>
<rectangle x1="0.46481875" y1="12.48841875" x2="1.56718125" y2="12.573" layer="21"/>
<rectangle x1="5.37718125" y1="12.48841875" x2="6.90118125" y2="12.573" layer="21"/>
<rectangle x1="9.271" y1="12.48841875" x2="10.795" y2="12.573" layer="21"/>
<rectangle x1="14.351" y1="12.48841875" x2="15.875" y2="12.573" layer="21"/>
<rectangle x1="17.907" y1="12.48841875" x2="19.685" y2="12.573" layer="21"/>
<rectangle x1="21.209" y1="12.48841875" x2="23.495" y2="12.573" layer="21"/>
<rectangle x1="25.019" y1="12.48841875" x2="26.20518125" y2="12.573" layer="21"/>
<rectangle x1="27.13481875" y1="12.48841875" x2="29.76118125" y2="12.573" layer="21"/>
<rectangle x1="30.607" y1="12.48841875" x2="31.19881875" y2="12.573" layer="21"/>
<rectangle x1="32.21481875" y1="12.48841875" x2="34.417" y2="12.573" layer="21"/>
<rectangle x1="35.09518125" y1="12.48841875" x2="39.32681875" y2="12.573" layer="21"/>
<rectangle x1="0.381" y1="12.573" x2="1.48081875" y2="12.65758125" layer="21"/>
<rectangle x1="5.29081875" y1="12.573" x2="6.731" y2="12.65758125" layer="21"/>
<rectangle x1="9.35481875" y1="12.573" x2="10.87881875" y2="12.65758125" layer="21"/>
<rectangle x1="14.52118125" y1="12.573" x2="15.95881875" y2="12.65758125" layer="21"/>
<rectangle x1="17.907" y1="12.573" x2="19.60118125" y2="12.65758125" layer="21"/>
<rectangle x1="21.03881875" y1="12.573" x2="23.66518125" y2="12.65758125" layer="21"/>
<rectangle x1="25.10281875" y1="12.573" x2="26.20518125" y2="12.65758125" layer="21"/>
<rectangle x1="27.13481875" y1="12.573" x2="29.76118125" y2="12.65758125" layer="21"/>
<rectangle x1="30.607" y1="12.573" x2="31.19881875" y2="12.65758125" layer="21"/>
<rectangle x1="32.131" y1="12.573" x2="34.50081875" y2="12.65758125" layer="21"/>
<rectangle x1="35.00881875" y1="12.573" x2="39.32681875" y2="12.65758125" layer="21"/>
<rectangle x1="0.381" y1="12.65758125" x2="1.48081875" y2="12.74241875" layer="21"/>
<rectangle x1="5.29081875" y1="12.65758125" x2="6.64718125" y2="12.74241875" layer="21"/>
<rectangle x1="9.525" y1="12.65758125" x2="10.87881875" y2="12.74241875" layer="21"/>
<rectangle x1="14.605" y1="12.65758125" x2="16.04518125" y2="12.74241875" layer="21"/>
<rectangle x1="17.907" y1="12.65758125" x2="19.51481875" y2="12.74241875" layer="21"/>
<rectangle x1="20.955" y1="12.65758125" x2="23.749" y2="12.74241875" layer="21"/>
<rectangle x1="25.18918125" y1="12.65758125" x2="26.20518125" y2="12.74241875" layer="21"/>
<rectangle x1="27.13481875" y1="12.65758125" x2="29.76118125" y2="12.74241875" layer="21"/>
<rectangle x1="30.607" y1="12.65758125" x2="31.19881875" y2="12.74241875" layer="21"/>
<rectangle x1="32.131" y1="12.65758125" x2="39.41318125" y2="12.74241875" layer="21"/>
<rectangle x1="0.381" y1="12.74241875" x2="1.48081875" y2="12.827" layer="21"/>
<rectangle x1="5.207" y1="12.74241875" x2="6.56081875" y2="12.827" layer="21"/>
<rectangle x1="9.60881875" y1="12.74241875" x2="10.87881875" y2="12.827" layer="21"/>
<rectangle x1="14.68881875" y1="12.74241875" x2="16.04518125" y2="12.827" layer="21"/>
<rectangle x1="17.907" y1="12.74241875" x2="19.51481875" y2="12.827" layer="21"/>
<rectangle x1="20.87118125" y1="12.74241875" x2="23.83281875" y2="12.827" layer="21"/>
<rectangle x1="25.18918125" y1="12.74241875" x2="26.20518125" y2="12.827" layer="21"/>
<rectangle x1="27.13481875" y1="12.74241875" x2="29.76118125" y2="12.827" layer="21"/>
<rectangle x1="30.607" y1="12.74241875" x2="31.115" y2="12.827" layer="21"/>
<rectangle x1="32.04718125" y1="12.74241875" x2="39.41318125" y2="12.827" layer="21"/>
<rectangle x1="0.381" y1="12.827" x2="1.48081875" y2="12.91158125" layer="21"/>
<rectangle x1="5.207" y1="12.827" x2="6.477" y2="12.91158125" layer="21"/>
<rectangle x1="9.69518125" y1="12.827" x2="10.96518125" y2="12.91158125" layer="21"/>
<rectangle x1="14.77518125" y1="12.827" x2="16.129" y2="12.91158125" layer="21"/>
<rectangle x1="17.907" y1="12.827" x2="19.431" y2="12.91158125" layer="21"/>
<rectangle x1="20.701" y1="12.827" x2="23.91918125" y2="12.91158125" layer="21"/>
<rectangle x1="25.273" y1="12.827" x2="26.20518125" y2="12.91158125" layer="21"/>
<rectangle x1="27.13481875" y1="12.827" x2="29.76118125" y2="12.91158125" layer="21"/>
<rectangle x1="30.607" y1="12.827" x2="31.115" y2="12.91158125" layer="21"/>
<rectangle x1="32.04718125" y1="12.827" x2="39.41318125" y2="12.91158125" layer="21"/>
<rectangle x1="0.29718125" y1="12.91158125" x2="1.397" y2="12.99641875" layer="21"/>
<rectangle x1="5.12318125" y1="12.91158125" x2="6.39318125" y2="12.99641875" layer="21"/>
<rectangle x1="9.779" y1="12.91158125" x2="11.049" y2="12.99641875" layer="21"/>
<rectangle x1="14.859" y1="12.91158125" x2="16.129" y2="12.99641875" layer="21"/>
<rectangle x1="17.907" y1="12.91158125" x2="19.431" y2="12.99641875" layer="21"/>
<rectangle x1="20.701" y1="12.91158125" x2="24.003" y2="12.99641875" layer="21"/>
<rectangle x1="25.273" y1="12.91158125" x2="26.20518125" y2="12.99641875" layer="21"/>
<rectangle x1="27.13481875" y1="12.91158125" x2="29.76118125" y2="12.99641875" layer="21"/>
<rectangle x1="30.607" y1="12.91158125" x2="31.115" y2="12.99641875" layer="21"/>
<rectangle x1="35.179" y1="12.91158125" x2="39.41318125" y2="12.99641875" layer="21"/>
<rectangle x1="0.29718125" y1="12.99641875" x2="1.397" y2="13.081" layer="21"/>
<rectangle x1="5.12318125" y1="12.99641875" x2="6.30681875" y2="13.081" layer="21"/>
<rectangle x1="9.779" y1="12.99641875" x2="11.049" y2="13.081" layer="21"/>
<rectangle x1="14.94281875" y1="12.99641875" x2="16.21281875" y2="13.081" layer="21"/>
<rectangle x1="17.907" y1="12.99641875" x2="19.34718125" y2="13.081" layer="21"/>
<rectangle x1="20.61718125" y1="12.99641875" x2="24.08681875" y2="13.081" layer="21"/>
<rectangle x1="25.35681875" y1="12.99641875" x2="26.20518125" y2="13.081" layer="21"/>
<rectangle x1="27.13481875" y1="12.99641875" x2="29.76118125" y2="13.081" layer="21"/>
<rectangle x1="30.607" y1="12.99641875" x2="31.115" y2="13.081" layer="21"/>
<rectangle x1="35.26281875" y1="12.99641875" x2="39.41318125" y2="13.081" layer="21"/>
<rectangle x1="0.29718125" y1="13.081" x2="1.397" y2="13.16558125" layer="21"/>
<rectangle x1="5.03681875" y1="13.081" x2="6.30681875" y2="13.16558125" layer="21"/>
<rectangle x1="9.86281875" y1="13.081" x2="11.049" y2="13.16558125" layer="21"/>
<rectangle x1="15.02918125" y1="13.081" x2="16.21281875" y2="13.16558125" layer="21"/>
<rectangle x1="17.907" y1="13.081" x2="19.34718125" y2="13.16558125" layer="21"/>
<rectangle x1="20.53081875" y1="13.081" x2="24.17318125" y2="13.16558125" layer="21"/>
<rectangle x1="25.35681875" y1="13.081" x2="26.20518125" y2="13.16558125" layer="21"/>
<rectangle x1="27.13481875" y1="13.081" x2="29.76118125" y2="13.16558125" layer="21"/>
<rectangle x1="30.607" y1="13.081" x2="31.115" y2="13.16558125" layer="21"/>
<rectangle x1="35.34918125" y1="13.081" x2="39.41318125" y2="13.16558125" layer="21"/>
<rectangle x1="0.29718125" y1="13.16558125" x2="1.397" y2="13.25041875" layer="21"/>
<rectangle x1="5.03681875" y1="13.16558125" x2="6.223" y2="13.25041875" layer="21"/>
<rectangle x1="9.94918125" y1="13.16558125" x2="11.13281875" y2="13.25041875" layer="21"/>
<rectangle x1="15.02918125" y1="13.16558125" x2="16.21281875" y2="13.25041875" layer="21"/>
<rectangle x1="17.907" y1="13.16558125" x2="19.26081875" y2="13.25041875" layer="21"/>
<rectangle x1="20.53081875" y1="13.16558125" x2="24.17318125" y2="13.25041875" layer="21"/>
<rectangle x1="25.44318125" y1="13.16558125" x2="26.20518125" y2="13.25041875" layer="21"/>
<rectangle x1="27.13481875" y1="13.16558125" x2="29.76118125" y2="13.25041875" layer="21"/>
<rectangle x1="30.607" y1="13.16558125" x2="31.03118125" y2="13.25041875" layer="21"/>
<rectangle x1="35.34918125" y1="13.16558125" x2="39.41318125" y2="13.25041875" layer="21"/>
<rectangle x1="0.29718125" y1="13.25041875" x2="1.397" y2="13.335" layer="21"/>
<rectangle x1="5.03681875" y1="13.25041875" x2="6.13918125" y2="13.335" layer="21"/>
<rectangle x1="9.94918125" y1="13.25041875" x2="11.13281875" y2="13.335" layer="21"/>
<rectangle x1="15.113" y1="13.25041875" x2="16.29918125" y2="13.335" layer="21"/>
<rectangle x1="17.907" y1="13.25041875" x2="19.26081875" y2="13.335" layer="21"/>
<rectangle x1="20.447" y1="13.25041875" x2="24.257" y2="13.335" layer="21"/>
<rectangle x1="25.44318125" y1="13.25041875" x2="26.20518125" y2="13.335" layer="21"/>
<rectangle x1="27.13481875" y1="13.25041875" x2="29.76118125" y2="13.335" layer="21"/>
<rectangle x1="30.607" y1="13.25041875" x2="31.03118125" y2="13.335" layer="21"/>
<rectangle x1="35.433" y1="13.25041875" x2="39.41318125" y2="13.335" layer="21"/>
<rectangle x1="0.29718125" y1="13.335" x2="1.31318125" y2="13.41958125" layer="21"/>
<rectangle x1="4.953" y1="13.335" x2="6.13918125" y2="13.41958125" layer="21"/>
<rectangle x1="10.033" y1="13.335" x2="11.21918125" y2="13.41958125" layer="21"/>
<rectangle x1="15.19681875" y1="13.335" x2="16.29918125" y2="13.41958125" layer="21"/>
<rectangle x1="17.907" y1="13.335" x2="19.26081875" y2="13.41958125" layer="21"/>
<rectangle x1="20.36318125" y1="13.335" x2="24.34081875" y2="13.41958125" layer="21"/>
<rectangle x1="25.44318125" y1="13.335" x2="26.20518125" y2="13.41958125" layer="21"/>
<rectangle x1="27.13481875" y1="13.335" x2="29.76118125" y2="13.41958125" layer="21"/>
<rectangle x1="30.607" y1="13.335" x2="31.03118125" y2="13.41958125" layer="21"/>
<rectangle x1="35.433" y1="13.335" x2="39.41318125" y2="13.41958125" layer="21"/>
<rectangle x1="0.29718125" y1="13.41958125" x2="1.31318125" y2="13.50441875" layer="21"/>
<rectangle x1="4.953" y1="13.41958125" x2="6.05281875" y2="13.50441875" layer="21"/>
<rectangle x1="10.033" y1="13.41958125" x2="11.21918125" y2="13.50441875" layer="21"/>
<rectangle x1="15.19681875" y1="13.41958125" x2="16.29918125" y2="13.50441875" layer="21"/>
<rectangle x1="17.907" y1="13.41958125" x2="19.177" y2="13.50441875" layer="21"/>
<rectangle x1="20.36318125" y1="13.41958125" x2="24.34081875" y2="13.50441875" layer="21"/>
<rectangle x1="25.44318125" y1="13.41958125" x2="26.20518125" y2="13.50441875" layer="21"/>
<rectangle x1="27.13481875" y1="13.41958125" x2="29.76118125" y2="13.50441875" layer="21"/>
<rectangle x1="30.607" y1="13.41958125" x2="31.03118125" y2="13.50441875" layer="21"/>
<rectangle x1="35.433" y1="13.41958125" x2="39.41318125" y2="13.50441875" layer="21"/>
<rectangle x1="0.21081875" y1="13.50441875" x2="1.31318125" y2="13.589" layer="21"/>
<rectangle x1="4.953" y1="13.50441875" x2="6.05281875" y2="13.589" layer="21"/>
<rectangle x1="10.11681875" y1="13.50441875" x2="11.21918125" y2="13.589" layer="21"/>
<rectangle x1="15.19681875" y1="13.50441875" x2="16.383" y2="13.589" layer="21"/>
<rectangle x1="17.907" y1="13.50441875" x2="19.177" y2="13.589" layer="21"/>
<rectangle x1="20.36318125" y1="13.50441875" x2="24.34081875" y2="13.589" layer="21"/>
<rectangle x1="25.527" y1="13.50441875" x2="26.20518125" y2="13.589" layer="21"/>
<rectangle x1="27.13481875" y1="13.50441875" x2="29.67481875" y2="13.589" layer="21"/>
<rectangle x1="30.607" y1="13.50441875" x2="31.03118125" y2="13.589" layer="21"/>
<rectangle x1="35.433" y1="13.50441875" x2="39.41318125" y2="13.589" layer="21"/>
<rectangle x1="0.21081875" y1="13.589" x2="1.31318125" y2="13.67358125" layer="21"/>
<rectangle x1="4.86918125" y1="13.589" x2="6.05281875" y2="13.67358125" layer="21"/>
<rectangle x1="10.11681875" y1="13.589" x2="11.21918125" y2="13.67358125" layer="21"/>
<rectangle x1="15.28318125" y1="13.589" x2="16.383" y2="13.67358125" layer="21"/>
<rectangle x1="17.907" y1="13.589" x2="19.177" y2="13.67358125" layer="21"/>
<rectangle x1="20.27681875" y1="13.589" x2="24.42718125" y2="13.67358125" layer="21"/>
<rectangle x1="25.527" y1="13.589" x2="26.20518125" y2="13.67358125" layer="21"/>
<rectangle x1="27.13481875" y1="13.589" x2="29.67481875" y2="13.67358125" layer="21"/>
<rectangle x1="30.607" y1="13.589" x2="31.115" y2="13.67358125" layer="21"/>
<rectangle x1="35.433" y1="13.589" x2="39.41318125" y2="13.67358125" layer="21"/>
<rectangle x1="0.21081875" y1="13.67358125" x2="1.31318125" y2="13.75841875" layer="21"/>
<rectangle x1="4.86918125" y1="13.67358125" x2="5.969" y2="13.75841875" layer="21"/>
<rectangle x1="10.20318125" y1="13.67358125" x2="11.303" y2="13.75841875" layer="21"/>
<rectangle x1="15.28318125" y1="13.67358125" x2="16.383" y2="13.75841875" layer="21"/>
<rectangle x1="17.82318125" y1="13.67358125" x2="19.177" y2="13.75841875" layer="21"/>
<rectangle x1="20.27681875" y1="13.67358125" x2="24.42718125" y2="13.75841875" layer="21"/>
<rectangle x1="25.527" y1="13.67358125" x2="26.20518125" y2="13.75841875" layer="21"/>
<rectangle x1="27.13481875" y1="13.67358125" x2="29.67481875" y2="13.75841875" layer="21"/>
<rectangle x1="30.52318125" y1="13.67358125" x2="31.115" y2="13.75841875" layer="21"/>
<rectangle x1="35.433" y1="13.67358125" x2="39.41318125" y2="13.75841875" layer="21"/>
<rectangle x1="0.21081875" y1="13.75841875" x2="1.31318125" y2="13.843" layer="21"/>
<rectangle x1="4.86918125" y1="13.75841875" x2="5.969" y2="13.843" layer="21"/>
<rectangle x1="10.20318125" y1="13.75841875" x2="11.303" y2="13.843" layer="21"/>
<rectangle x1="15.28318125" y1="13.75841875" x2="16.383" y2="13.843" layer="21"/>
<rectangle x1="17.82318125" y1="13.75841875" x2="19.177" y2="13.843" layer="21"/>
<rectangle x1="20.27681875" y1="13.75841875" x2="24.42718125" y2="13.843" layer="21"/>
<rectangle x1="25.527" y1="13.75841875" x2="26.20518125" y2="13.843" layer="21"/>
<rectangle x1="27.22118125" y1="13.75841875" x2="29.67481875" y2="13.843" layer="21"/>
<rectangle x1="30.52318125" y1="13.75841875" x2="31.115" y2="13.843" layer="21"/>
<rectangle x1="32.04718125" y1="13.75841875" x2="34.417" y2="13.843" layer="21"/>
<rectangle x1="35.34918125" y1="13.75841875" x2="39.41318125" y2="13.843" layer="21"/>
<rectangle x1="0.21081875" y1="13.843" x2="1.31318125" y2="13.92758125" layer="21"/>
<rectangle x1="4.86918125" y1="13.843" x2="5.969" y2="13.92758125" layer="21"/>
<rectangle x1="10.20318125" y1="13.843" x2="11.303" y2="13.92758125" layer="21"/>
<rectangle x1="15.367" y1="13.843" x2="16.46681875" y2="13.92758125" layer="21"/>
<rectangle x1="17.82318125" y1="13.843" x2="19.09318125" y2="13.92758125" layer="21"/>
<rectangle x1="20.193" y1="13.843" x2="24.42718125" y2="13.92758125" layer="21"/>
<rectangle x1="25.61081875" y1="13.843" x2="26.20518125" y2="13.92758125" layer="21"/>
<rectangle x1="27.22118125" y1="13.843" x2="29.591" y2="13.92758125" layer="21"/>
<rectangle x1="30.52318125" y1="13.843" x2="31.115" y2="13.92758125" layer="21"/>
<rectangle x1="32.04718125" y1="13.843" x2="34.417" y2="13.92758125" layer="21"/>
<rectangle x1="35.34918125" y1="13.843" x2="39.41318125" y2="13.92758125" layer="21"/>
<rectangle x1="0.21081875" y1="13.92758125" x2="1.31318125" y2="14.01241875" layer="21"/>
<rectangle x1="4.86918125" y1="13.92758125" x2="5.969" y2="14.01241875" layer="21"/>
<rectangle x1="10.20318125" y1="13.92758125" x2="11.303" y2="14.01241875" layer="21"/>
<rectangle x1="15.367" y1="13.92758125" x2="16.46681875" y2="14.01241875" layer="21"/>
<rectangle x1="17.82318125" y1="13.92758125" x2="19.09318125" y2="14.01241875" layer="21"/>
<rectangle x1="20.193" y1="13.92758125" x2="24.511" y2="14.01241875" layer="21"/>
<rectangle x1="25.61081875" y1="13.92758125" x2="26.20518125" y2="14.01241875" layer="21"/>
<rectangle x1="27.22118125" y1="13.92758125" x2="29.591" y2="14.01241875" layer="21"/>
<rectangle x1="30.52318125" y1="13.92758125" x2="31.115" y2="14.01241875" layer="21"/>
<rectangle x1="32.04718125" y1="13.92758125" x2="34.417" y2="14.01241875" layer="21"/>
<rectangle x1="35.34918125" y1="13.92758125" x2="39.41318125" y2="14.01241875" layer="21"/>
<rectangle x1="0.21081875" y1="14.01241875" x2="1.31318125" y2="14.097" layer="21"/>
<rectangle x1="4.86918125" y1="14.01241875" x2="5.88518125" y2="14.097" layer="21"/>
<rectangle x1="10.20318125" y1="14.01241875" x2="11.303" y2="14.097" layer="21"/>
<rectangle x1="15.367" y1="14.01241875" x2="16.46681875" y2="14.097" layer="21"/>
<rectangle x1="17.82318125" y1="14.01241875" x2="19.09318125" y2="14.097" layer="21"/>
<rectangle x1="20.193" y1="14.01241875" x2="24.511" y2="14.097" layer="21"/>
<rectangle x1="25.61081875" y1="14.01241875" x2="26.20518125" y2="14.097" layer="21"/>
<rectangle x1="27.305" y1="14.01241875" x2="29.50718125" y2="14.097" layer="21"/>
<rectangle x1="30.52318125" y1="14.01241875" x2="31.19881875" y2="14.097" layer="21"/>
<rectangle x1="32.131" y1="14.01241875" x2="34.33318125" y2="14.097" layer="21"/>
<rectangle x1="35.26281875" y1="14.01241875" x2="39.41318125" y2="14.097" layer="21"/>
<rectangle x1="0.21081875" y1="14.097" x2="1.31318125" y2="14.18158125" layer="21"/>
<rectangle x1="4.78281875" y1="14.097" x2="5.88518125" y2="14.18158125" layer="21"/>
<rectangle x1="10.287" y1="14.097" x2="11.303" y2="14.18158125" layer="21"/>
<rectangle x1="15.367" y1="14.097" x2="16.46681875" y2="14.18158125" layer="21"/>
<rectangle x1="17.82318125" y1="14.097" x2="19.09318125" y2="14.18158125" layer="21"/>
<rectangle x1="20.193" y1="14.097" x2="24.511" y2="14.18158125" layer="21"/>
<rectangle x1="25.61081875" y1="14.097" x2="26.20518125" y2="14.18158125" layer="21"/>
<rectangle x1="27.38881875" y1="14.097" x2="29.50718125" y2="14.18158125" layer="21"/>
<rectangle x1="30.43681875" y1="14.097" x2="31.19881875" y2="14.18158125" layer="21"/>
<rectangle x1="32.21481875" y1="14.097" x2="34.33318125" y2="14.18158125" layer="21"/>
<rectangle x1="35.26281875" y1="14.097" x2="39.41318125" y2="14.18158125" layer="21"/>
<rectangle x1="0.21081875" y1="14.18158125" x2="1.31318125" y2="14.26641875" layer="21"/>
<rectangle x1="4.78281875" y1="14.18158125" x2="5.88518125" y2="14.26641875" layer="21"/>
<rectangle x1="10.287" y1="14.18158125" x2="11.303" y2="14.26641875" layer="21"/>
<rectangle x1="15.367" y1="14.18158125" x2="16.46681875" y2="14.26641875" layer="21"/>
<rectangle x1="17.82318125" y1="14.18158125" x2="19.09318125" y2="14.26641875" layer="21"/>
<rectangle x1="20.193" y1="14.18158125" x2="24.511" y2="14.26641875" layer="21"/>
<rectangle x1="25.61081875" y1="14.18158125" x2="26.20518125" y2="14.26641875" layer="21"/>
<rectangle x1="27.38881875" y1="14.18158125" x2="29.42081875" y2="14.26641875" layer="21"/>
<rectangle x1="30.43681875" y1="14.18158125" x2="31.19881875" y2="14.26641875" layer="21"/>
<rectangle x1="32.21481875" y1="14.18158125" x2="34.24681875" y2="14.26641875" layer="21"/>
<rectangle x1="35.26281875" y1="14.18158125" x2="39.41318125" y2="14.26641875" layer="21"/>
<rectangle x1="0.21081875" y1="14.26641875" x2="1.31318125" y2="14.351" layer="21"/>
<rectangle x1="4.78281875" y1="14.26641875" x2="5.88518125" y2="14.351" layer="21"/>
<rectangle x1="10.287" y1="14.26641875" x2="11.303" y2="14.351" layer="21"/>
<rectangle x1="15.367" y1="14.26641875" x2="16.46681875" y2="14.351" layer="21"/>
<rectangle x1="17.82318125" y1="14.26641875" x2="19.09318125" y2="14.351" layer="21"/>
<rectangle x1="20.193" y1="14.26641875" x2="24.511" y2="14.351" layer="21"/>
<rectangle x1="25.61081875" y1="14.26641875" x2="26.20518125" y2="14.351" layer="21"/>
<rectangle x1="27.47518125" y1="14.26641875" x2="29.337" y2="14.351" layer="21"/>
<rectangle x1="30.353" y1="14.26641875" x2="31.28518125" y2="14.351" layer="21"/>
<rectangle x1="32.30118125" y1="14.26641875" x2="34.163" y2="14.351" layer="21"/>
<rectangle x1="35.179" y1="14.26641875" x2="39.41318125" y2="14.351" layer="21"/>
<rectangle x1="0.21081875" y1="14.351" x2="1.31318125" y2="14.43558125" layer="21"/>
<rectangle x1="4.78281875" y1="14.351" x2="5.88518125" y2="14.43558125" layer="21"/>
<rectangle x1="10.287" y1="14.351" x2="11.303" y2="14.43558125" layer="21"/>
<rectangle x1="15.367" y1="14.351" x2="16.46681875" y2="14.43558125" layer="21"/>
<rectangle x1="17.82318125" y1="14.351" x2="19.09318125" y2="14.43558125" layer="21"/>
<rectangle x1="20.193" y1="14.351" x2="24.511" y2="14.43558125" layer="21"/>
<rectangle x1="25.61081875" y1="14.351" x2="26.20518125" y2="14.43558125" layer="21"/>
<rectangle x1="27.559" y1="14.351" x2="29.25318125" y2="14.43558125" layer="21"/>
<rectangle x1="30.353" y1="14.351" x2="31.28518125" y2="14.43558125" layer="21"/>
<rectangle x1="32.385" y1="14.351" x2="34.07918125" y2="14.43558125" layer="21"/>
<rectangle x1="35.179" y1="14.351" x2="39.41318125" y2="14.43558125" layer="21"/>
<rectangle x1="0.21081875" y1="14.43558125" x2="1.31318125" y2="14.52041875" layer="21"/>
<rectangle x1="4.78281875" y1="14.43558125" x2="5.88518125" y2="14.52041875" layer="21"/>
<rectangle x1="10.287" y1="14.43558125" x2="11.303" y2="14.52041875" layer="21"/>
<rectangle x1="15.367" y1="14.43558125" x2="16.46681875" y2="14.52041875" layer="21"/>
<rectangle x1="17.82318125" y1="14.43558125" x2="19.09318125" y2="14.52041875" layer="21"/>
<rectangle x1="20.193" y1="14.43558125" x2="24.511" y2="14.52041875" layer="21"/>
<rectangle x1="25.61081875" y1="14.43558125" x2="26.20518125" y2="14.52041875" layer="21"/>
<rectangle x1="27.72918125" y1="14.43558125" x2="29.083" y2="14.52041875" layer="21"/>
<rectangle x1="30.26918125" y1="14.43558125" x2="31.369" y2="14.52041875" layer="21"/>
<rectangle x1="32.55518125" y1="14.43558125" x2="33.909" y2="14.52041875" layer="21"/>
<rectangle x1="35.09518125" y1="14.43558125" x2="39.41318125" y2="14.52041875" layer="21"/>
<rectangle x1="0.21081875" y1="14.52041875" x2="1.31318125" y2="14.605" layer="21"/>
<rectangle x1="4.78281875" y1="14.52041875" x2="5.88518125" y2="14.605" layer="21"/>
<rectangle x1="10.287" y1="14.52041875" x2="11.303" y2="14.605" layer="21"/>
<rectangle x1="15.367" y1="14.52041875" x2="16.46681875" y2="14.605" layer="21"/>
<rectangle x1="17.82318125" y1="14.52041875" x2="19.09318125" y2="14.605" layer="21"/>
<rectangle x1="20.193" y1="14.52041875" x2="24.511" y2="14.605" layer="21"/>
<rectangle x1="25.61081875" y1="14.52041875" x2="26.20518125" y2="14.605" layer="21"/>
<rectangle x1="27.89681875" y1="14.52041875" x2="28.91281875" y2="14.605" layer="21"/>
<rectangle x1="30.26918125" y1="14.52041875" x2="31.45281875" y2="14.605" layer="21"/>
<rectangle x1="32.72281875" y1="14.52041875" x2="33.82518125" y2="14.605" layer="21"/>
<rectangle x1="35.00881875" y1="14.52041875" x2="39.41318125" y2="14.605" layer="21"/>
<rectangle x1="0.21081875" y1="14.605" x2="1.31318125" y2="14.68958125" layer="21"/>
<rectangle x1="4.78281875" y1="14.605" x2="5.88518125" y2="14.68958125" layer="21"/>
<rectangle x1="10.287" y1="14.605" x2="11.303" y2="14.68958125" layer="21"/>
<rectangle x1="15.367" y1="14.605" x2="16.46681875" y2="14.68958125" layer="21"/>
<rectangle x1="17.82318125" y1="14.605" x2="19.09318125" y2="14.68958125" layer="21"/>
<rectangle x1="20.193" y1="14.605" x2="24.511" y2="14.68958125" layer="21"/>
<rectangle x1="25.61081875" y1="14.605" x2="26.20518125" y2="14.68958125" layer="21"/>
<rectangle x1="28.15081875" y1="14.605" x2="28.65881875" y2="14.68958125" layer="21"/>
<rectangle x1="30.18281875" y1="14.605" x2="31.45281875" y2="14.68958125" layer="21"/>
<rectangle x1="32.97681875" y1="14.605" x2="33.48481875" y2="14.68958125" layer="21"/>
<rectangle x1="35.00881875" y1="14.605" x2="39.41318125" y2="14.68958125" layer="21"/>
<rectangle x1="0.21081875" y1="14.68958125" x2="1.31318125" y2="14.77441875" layer="21"/>
<rectangle x1="4.78281875" y1="14.68958125" x2="5.88518125" y2="14.77441875" layer="21"/>
<rectangle x1="10.287" y1="14.68958125" x2="11.303" y2="14.77441875" layer="21"/>
<rectangle x1="15.367" y1="14.68958125" x2="16.46681875" y2="14.77441875" layer="21"/>
<rectangle x1="17.82318125" y1="14.68958125" x2="19.09318125" y2="14.77441875" layer="21"/>
<rectangle x1="20.193" y1="14.68958125" x2="24.511" y2="14.77441875" layer="21"/>
<rectangle x1="25.61081875" y1="14.68958125" x2="26.20518125" y2="14.77441875" layer="21"/>
<rectangle x1="30.099" y1="14.68958125" x2="31.53918125" y2="14.77441875" layer="21"/>
<rectangle x1="34.925" y1="14.68958125" x2="39.41318125" y2="14.77441875" layer="21"/>
<rectangle x1="0.21081875" y1="14.77441875" x2="1.31318125" y2="14.859" layer="21"/>
<rectangle x1="4.78281875" y1="14.77441875" x2="5.88518125" y2="14.859" layer="21"/>
<rectangle x1="10.287" y1="14.77441875" x2="11.303" y2="14.859" layer="21"/>
<rectangle x1="15.367" y1="14.77441875" x2="16.46681875" y2="14.859" layer="21"/>
<rectangle x1="17.82318125" y1="14.77441875" x2="19.09318125" y2="14.859" layer="21"/>
<rectangle x1="20.193" y1="14.77441875" x2="24.42718125" y2="14.859" layer="21"/>
<rectangle x1="25.61081875" y1="14.77441875" x2="26.20518125" y2="14.859" layer="21"/>
<rectangle x1="30.01518125" y1="14.77441875" x2="31.623" y2="14.859" layer="21"/>
<rectangle x1="34.84118125" y1="14.77441875" x2="39.41318125" y2="14.859" layer="21"/>
<rectangle x1="0.21081875" y1="14.859" x2="1.31318125" y2="14.94358125" layer="21"/>
<rectangle x1="4.78281875" y1="14.859" x2="5.88518125" y2="14.94358125" layer="21"/>
<rectangle x1="10.287" y1="14.859" x2="11.303" y2="14.94358125" layer="21"/>
<rectangle x1="15.28318125" y1="14.859" x2="16.383" y2="14.94358125" layer="21"/>
<rectangle x1="17.82318125" y1="14.859" x2="19.09318125" y2="14.94358125" layer="21"/>
<rectangle x1="20.27681875" y1="14.859" x2="24.42718125" y2="14.94358125" layer="21"/>
<rectangle x1="25.527" y1="14.859" x2="26.20518125" y2="14.94358125" layer="21"/>
<rectangle x1="29.92881875" y1="14.859" x2="31.70681875" y2="14.94358125" layer="21"/>
<rectangle x1="34.75481875" y1="14.859" x2="39.41318125" y2="14.94358125" layer="21"/>
<rectangle x1="0.21081875" y1="14.94358125" x2="1.31318125" y2="15.02841875" layer="21"/>
<rectangle x1="4.78281875" y1="14.94358125" x2="5.88518125" y2="15.02841875" layer="21"/>
<rectangle x1="10.287" y1="14.94358125" x2="11.303" y2="15.02841875" layer="21"/>
<rectangle x1="15.28318125" y1="14.94358125" x2="16.383" y2="15.02841875" layer="21"/>
<rectangle x1="17.82318125" y1="14.94358125" x2="19.177" y2="15.02841875" layer="21"/>
<rectangle x1="20.27681875" y1="14.94358125" x2="24.42718125" y2="15.02841875" layer="21"/>
<rectangle x1="25.527" y1="14.94358125" x2="26.20518125" y2="15.02841875" layer="21"/>
<rectangle x1="29.845" y1="14.94358125" x2="31.79318125" y2="15.02841875" layer="21"/>
<rectangle x1="34.671" y1="14.94358125" x2="39.41318125" y2="15.02841875" layer="21"/>
<rectangle x1="0.21081875" y1="15.02841875" x2="1.31318125" y2="15.113" layer="21"/>
<rectangle x1="4.78281875" y1="15.02841875" x2="5.88518125" y2="15.113" layer="21"/>
<rectangle x1="10.287" y1="15.02841875" x2="11.303" y2="15.113" layer="21"/>
<rectangle x1="15.28318125" y1="15.02841875" x2="16.383" y2="15.113" layer="21"/>
<rectangle x1="17.82318125" y1="15.02841875" x2="19.177" y2="15.113" layer="21"/>
<rectangle x1="20.27681875" y1="15.02841875" x2="24.42718125" y2="15.113" layer="21"/>
<rectangle x1="25.527" y1="15.02841875" x2="26.20518125" y2="15.113" layer="21"/>
<rectangle x1="29.76118125" y1="15.02841875" x2="31.877" y2="15.113" layer="21"/>
<rectangle x1="34.58718125" y1="15.02841875" x2="39.41318125" y2="15.113" layer="21"/>
<rectangle x1="0.29718125" y1="15.113" x2="1.31318125" y2="15.19758125" layer="21"/>
<rectangle x1="4.78281875" y1="15.113" x2="5.88518125" y2="15.19758125" layer="21"/>
<rectangle x1="10.287" y1="15.113" x2="11.303" y2="15.19758125" layer="21"/>
<rectangle x1="15.19681875" y1="15.113" x2="16.383" y2="15.19758125" layer="21"/>
<rectangle x1="17.907" y1="15.113" x2="19.177" y2="15.19758125" layer="21"/>
<rectangle x1="20.36318125" y1="15.113" x2="24.34081875" y2="15.19758125" layer="21"/>
<rectangle x1="25.527" y1="15.113" x2="26.20518125" y2="15.19758125" layer="21"/>
<rectangle x1="27.051" y1="15.113" x2="27.22118125" y2="15.19758125" layer="21"/>
<rectangle x1="29.67481875" y1="15.113" x2="32.04718125" y2="15.19758125" layer="21"/>
<rectangle x1="34.50081875" y1="15.113" x2="39.41318125" y2="15.19758125" layer="21"/>
<rectangle x1="0.29718125" y1="15.19758125" x2="1.31318125" y2="15.28241875" layer="21"/>
<rectangle x1="4.78281875" y1="15.19758125" x2="5.88518125" y2="15.28241875" layer="21"/>
<rectangle x1="10.287" y1="15.19758125" x2="11.303" y2="15.28241875" layer="21"/>
<rectangle x1="15.19681875" y1="15.19758125" x2="16.29918125" y2="15.28241875" layer="21"/>
<rectangle x1="17.907" y1="15.19758125" x2="19.177" y2="15.28241875" layer="21"/>
<rectangle x1="20.36318125" y1="15.19758125" x2="24.34081875" y2="15.28241875" layer="21"/>
<rectangle x1="25.44318125" y1="15.19758125" x2="26.289" y2="15.28241875" layer="21"/>
<rectangle x1="27.051" y1="15.19758125" x2="27.305" y2="15.28241875" layer="21"/>
<rectangle x1="29.50718125" y1="15.19758125" x2="32.131" y2="15.28241875" layer="21"/>
<rectangle x1="34.33318125" y1="15.19758125" x2="39.41318125" y2="15.28241875" layer="21"/>
<rectangle x1="0.29718125" y1="15.28241875" x2="1.397" y2="15.367" layer="21"/>
<rectangle x1="4.78281875" y1="15.28241875" x2="5.88518125" y2="15.367" layer="21"/>
<rectangle x1="10.287" y1="15.28241875" x2="11.303" y2="15.367" layer="21"/>
<rectangle x1="15.113" y1="15.28241875" x2="16.29918125" y2="15.367" layer="21"/>
<rectangle x1="17.907" y1="15.28241875" x2="19.26081875" y2="15.367" layer="21"/>
<rectangle x1="20.36318125" y1="15.28241875" x2="24.257" y2="15.367" layer="21"/>
<rectangle x1="25.44318125" y1="15.28241875" x2="26.289" y2="15.367" layer="21"/>
<rectangle x1="27.051" y1="15.28241875" x2="27.47518125" y2="15.367" layer="21"/>
<rectangle x1="29.337" y1="15.28241875" x2="32.30118125" y2="15.367" layer="21"/>
<rectangle x1="34.163" y1="15.28241875" x2="39.41318125" y2="15.367" layer="21"/>
<rectangle x1="0.29718125" y1="15.367" x2="1.397" y2="15.45158125" layer="21"/>
<rectangle x1="4.78281875" y1="15.367" x2="5.88518125" y2="15.45158125" layer="21"/>
<rectangle x1="10.287" y1="15.367" x2="11.303" y2="15.45158125" layer="21"/>
<rectangle x1="15.113" y1="15.367" x2="16.29918125" y2="15.45158125" layer="21"/>
<rectangle x1="17.907" y1="15.367" x2="19.26081875" y2="15.45158125" layer="21"/>
<rectangle x1="20.447" y1="15.367" x2="24.257" y2="15.45158125" layer="21"/>
<rectangle x1="25.44318125" y1="15.367" x2="26.37281875" y2="15.45158125" layer="21"/>
<rectangle x1="26.96718125" y1="15.367" x2="27.64281875" y2="15.45158125" layer="21"/>
<rectangle x1="29.16681875" y1="15.367" x2="32.46881875" y2="15.45158125" layer="21"/>
<rectangle x1="33.99281875" y1="15.367" x2="39.41318125" y2="15.45158125" layer="21"/>
<rectangle x1="0.29718125" y1="15.45158125" x2="1.397" y2="15.53641875" layer="21"/>
<rectangle x1="4.78281875" y1="15.45158125" x2="5.88518125" y2="15.53641875" layer="21"/>
<rectangle x1="10.287" y1="15.45158125" x2="11.303" y2="15.53641875" layer="21"/>
<rectangle x1="15.02918125" y1="15.45158125" x2="16.21281875" y2="15.53641875" layer="21"/>
<rectangle x1="17.907" y1="15.45158125" x2="19.26081875" y2="15.53641875" layer="21"/>
<rectangle x1="20.53081875" y1="15.45158125" x2="24.17318125" y2="15.53641875" layer="21"/>
<rectangle x1="25.35681875" y1="15.45158125" x2="26.45918125" y2="15.53641875" layer="21"/>
<rectangle x1="26.88081875" y1="15.45158125" x2="27.98318125" y2="15.53641875" layer="21"/>
<rectangle x1="28.91281875" y1="15.45158125" x2="32.80918125" y2="15.53641875" layer="21"/>
<rectangle x1="33.655" y1="15.45158125" x2="39.41318125" y2="15.53641875" layer="21"/>
<rectangle x1="0.29718125" y1="15.53641875" x2="1.397" y2="15.621" layer="21"/>
<rectangle x1="4.78281875" y1="15.53641875" x2="5.88518125" y2="15.621" layer="21"/>
<rectangle x1="10.287" y1="15.53641875" x2="11.303" y2="15.621" layer="21"/>
<rectangle x1="15.02918125" y1="15.53641875" x2="16.21281875" y2="15.621" layer="21"/>
<rectangle x1="17.907" y1="15.53641875" x2="19.34718125" y2="15.621" layer="21"/>
<rectangle x1="20.53081875" y1="15.53641875" x2="24.17318125" y2="15.621" layer="21"/>
<rectangle x1="25.35681875" y1="15.53641875" x2="39.41318125" y2="15.621" layer="21"/>
<rectangle x1="0.29718125" y1="15.621" x2="1.397" y2="15.70558125" layer="21"/>
<rectangle x1="4.78281875" y1="15.621" x2="5.88518125" y2="15.70558125" layer="21"/>
<rectangle x1="10.287" y1="15.621" x2="11.303" y2="15.70558125" layer="21"/>
<rectangle x1="14.94281875" y1="15.621" x2="16.21281875" y2="15.70558125" layer="21"/>
<rectangle x1="17.907" y1="15.621" x2="19.34718125" y2="15.70558125" layer="21"/>
<rectangle x1="20.61718125" y1="15.621" x2="24.08681875" y2="15.70558125" layer="21"/>
<rectangle x1="25.35681875" y1="15.621" x2="39.41318125" y2="15.70558125" layer="21"/>
<rectangle x1="0.29718125" y1="15.70558125" x2="1.397" y2="15.79041875" layer="21"/>
<rectangle x1="4.78281875" y1="15.70558125" x2="5.88518125" y2="15.79041875" layer="21"/>
<rectangle x1="10.287" y1="15.70558125" x2="11.303" y2="15.79041875" layer="21"/>
<rectangle x1="14.859" y1="15.70558125" x2="16.129" y2="15.79041875" layer="21"/>
<rectangle x1="17.907" y1="15.70558125" x2="19.431" y2="15.79041875" layer="21"/>
<rectangle x1="20.701" y1="15.70558125" x2="24.003" y2="15.79041875" layer="21"/>
<rectangle x1="25.273" y1="15.70558125" x2="39.41318125" y2="15.79041875" layer="21"/>
<rectangle x1="0.381" y1="15.79041875" x2="1.48081875" y2="15.875" layer="21"/>
<rectangle x1="4.78281875" y1="15.79041875" x2="5.88518125" y2="15.875" layer="21"/>
<rectangle x1="10.287" y1="15.79041875" x2="11.303" y2="15.875" layer="21"/>
<rectangle x1="14.77518125" y1="15.79041875" x2="16.129" y2="15.875" layer="21"/>
<rectangle x1="17.907" y1="15.79041875" x2="19.431" y2="15.875" layer="21"/>
<rectangle x1="20.78481875" y1="15.79041875" x2="23.91918125" y2="15.875" layer="21"/>
<rectangle x1="25.273" y1="15.79041875" x2="39.41318125" y2="15.875" layer="21"/>
<rectangle x1="0.381" y1="15.875" x2="1.48081875" y2="15.95958125" layer="21"/>
<rectangle x1="4.78281875" y1="15.875" x2="5.88518125" y2="15.95958125" layer="21"/>
<rectangle x1="10.287" y1="15.875" x2="11.303" y2="15.95958125" layer="21"/>
<rectangle x1="14.68881875" y1="15.875" x2="16.04518125" y2="15.95958125" layer="21"/>
<rectangle x1="17.907" y1="15.875" x2="19.51481875" y2="15.95958125" layer="21"/>
<rectangle x1="20.87118125" y1="15.875" x2="23.83281875" y2="15.95958125" layer="21"/>
<rectangle x1="25.18918125" y1="15.875" x2="39.41318125" y2="15.95958125" layer="21"/>
<rectangle x1="0.381" y1="15.95958125" x2="1.48081875" y2="16.04441875" layer="21"/>
<rectangle x1="4.78281875" y1="15.95958125" x2="5.88518125" y2="16.04441875" layer="21"/>
<rectangle x1="10.287" y1="15.95958125" x2="11.303" y2="16.04441875" layer="21"/>
<rectangle x1="14.605" y1="15.95958125" x2="16.04518125" y2="16.04441875" layer="21"/>
<rectangle x1="17.907" y1="15.95958125" x2="19.51481875" y2="16.04441875" layer="21"/>
<rectangle x1="20.955" y1="15.95958125" x2="23.749" y2="16.04441875" layer="21"/>
<rectangle x1="25.10281875" y1="15.95958125" x2="39.41318125" y2="16.04441875" layer="21"/>
<rectangle x1="0.381" y1="16.04441875" x2="1.48081875" y2="16.129" layer="21"/>
<rectangle x1="4.78281875" y1="16.04441875" x2="5.88518125" y2="16.129" layer="21"/>
<rectangle x1="10.287" y1="16.04441875" x2="11.303" y2="16.129" layer="21"/>
<rectangle x1="14.52118125" y1="16.04441875" x2="15.95881875" y2="16.129" layer="21"/>
<rectangle x1="17.907" y1="16.04441875" x2="19.60118125" y2="16.129" layer="21"/>
<rectangle x1="21.03881875" y1="16.04441875" x2="23.66518125" y2="16.129" layer="21"/>
<rectangle x1="25.10281875" y1="16.04441875" x2="39.32681875" y2="16.129" layer="21"/>
<rectangle x1="0.381" y1="16.129" x2="1.56718125" y2="16.21358125" layer="21"/>
<rectangle x1="4.78281875" y1="16.129" x2="5.88518125" y2="16.21358125" layer="21"/>
<rectangle x1="10.287" y1="16.129" x2="11.303" y2="16.21358125" layer="21"/>
<rectangle x1="14.351" y1="16.129" x2="15.875" y2="16.21358125" layer="21"/>
<rectangle x1="17.907" y1="16.129" x2="19.685" y2="16.21358125" layer="21"/>
<rectangle x1="21.209" y1="16.129" x2="23.495" y2="16.21358125" layer="21"/>
<rectangle x1="25.019" y1="16.129" x2="39.32681875" y2="16.21358125" layer="21"/>
<rectangle x1="0.46481875" y1="16.21358125" x2="1.56718125" y2="16.29841875" layer="21"/>
<rectangle x1="4.78281875" y1="16.21358125" x2="5.88518125" y2="16.29841875" layer="21"/>
<rectangle x1="10.287" y1="16.21358125" x2="11.303" y2="16.29841875" layer="21"/>
<rectangle x1="14.18081875" y1="16.21358125" x2="15.875" y2="16.29841875" layer="21"/>
<rectangle x1="17.99081875" y1="16.21358125" x2="19.685" y2="16.29841875" layer="21"/>
<rectangle x1="21.29281875" y1="16.21358125" x2="23.41118125" y2="16.29841875" layer="21"/>
<rectangle x1="24.93518125" y1="16.21358125" x2="39.32681875" y2="16.29841875" layer="21"/>
<rectangle x1="0.46481875" y1="16.29841875" x2="1.56718125" y2="16.383" layer="21"/>
<rectangle x1="4.78281875" y1="16.29841875" x2="5.88518125" y2="16.383" layer="21"/>
<rectangle x1="10.287" y1="16.29841875" x2="11.303" y2="16.383" layer="21"/>
<rectangle x1="14.097" y1="16.29841875" x2="15.79118125" y2="16.383" layer="21"/>
<rectangle x1="17.99081875" y1="16.29841875" x2="19.76881875" y2="16.383" layer="21"/>
<rectangle x1="21.463" y1="16.29841875" x2="23.241" y2="16.383" layer="21"/>
<rectangle x1="24.93518125" y1="16.29841875" x2="39.32681875" y2="16.383" layer="21"/>
<rectangle x1="0.46481875" y1="16.383" x2="1.651" y2="16.46758125" layer="21"/>
<rectangle x1="4.78281875" y1="16.383" x2="5.88518125" y2="16.46758125" layer="21"/>
<rectangle x1="10.287" y1="16.383" x2="11.303" y2="16.46758125" layer="21"/>
<rectangle x1="13.843" y1="16.383" x2="15.70481875" y2="16.46758125" layer="21"/>
<rectangle x1="17.99081875" y1="16.383" x2="19.85518125" y2="16.46758125" layer="21"/>
<rectangle x1="21.80081875" y1="16.383" x2="22.987" y2="16.46758125" layer="21"/>
<rectangle x1="24.84881875" y1="16.383" x2="39.32681875" y2="16.46758125" layer="21"/>
<rectangle x1="0.46481875" y1="16.46758125" x2="1.651" y2="16.55241875" layer="21"/>
<rectangle x1="4.78281875" y1="16.46758125" x2="5.88518125" y2="16.55241875" layer="21"/>
<rectangle x1="10.287" y1="16.46758125" x2="11.47318125" y2="16.55241875" layer="21"/>
<rectangle x1="13.25118125" y1="16.46758125" x2="15.621" y2="16.55241875" layer="21"/>
<rectangle x1="17.99081875" y1="16.46758125" x2="19.939" y2="16.55241875" layer="21"/>
<rectangle x1="24.765" y1="16.46758125" x2="39.243" y2="16.55241875" layer="21"/>
<rectangle x1="0.55118125" y1="16.55241875" x2="1.651" y2="16.637" layer="21"/>
<rectangle x1="4.78281875" y1="16.55241875" x2="5.88518125" y2="16.637" layer="21"/>
<rectangle x1="10.287" y1="16.55241875" x2="15.53718125" y2="16.637" layer="21"/>
<rectangle x1="17.99081875" y1="16.55241875" x2="20.02281875" y2="16.637" layer="21"/>
<rectangle x1="24.68118125" y1="16.55241875" x2="39.243" y2="16.637" layer="21"/>
<rectangle x1="0.55118125" y1="16.637" x2="1.73481875" y2="16.72158125" layer="21"/>
<rectangle x1="4.78281875" y1="16.637" x2="5.88518125" y2="16.72158125" layer="21"/>
<rectangle x1="10.287" y1="16.637" x2="15.45081875" y2="16.72158125" layer="21"/>
<rectangle x1="17.99081875" y1="16.637" x2="20.10918125" y2="16.72158125" layer="21"/>
<rectangle x1="24.59481875" y1="16.637" x2="39.243" y2="16.72158125" layer="21"/>
<rectangle x1="0.55118125" y1="16.72158125" x2="1.73481875" y2="16.80641875" layer="21"/>
<rectangle x1="4.78281875" y1="16.72158125" x2="5.88518125" y2="16.80641875" layer="21"/>
<rectangle x1="10.287" y1="16.72158125" x2="15.367" y2="16.80641875" layer="21"/>
<rectangle x1="17.99081875" y1="16.72158125" x2="20.193" y2="16.80641875" layer="21"/>
<rectangle x1="24.511" y1="16.72158125" x2="39.15918125" y2="16.80641875" layer="21"/>
<rectangle x1="0.635" y1="16.80641875" x2="1.73481875" y2="16.891" layer="21"/>
<rectangle x1="4.78281875" y1="16.80641875" x2="5.88518125" y2="16.891" layer="21"/>
<rectangle x1="10.287" y1="16.80641875" x2="15.28318125" y2="16.891" layer="21"/>
<rectangle x1="18.07718125" y1="16.80641875" x2="20.27681875" y2="16.891" layer="21"/>
<rectangle x1="24.42718125" y1="16.80641875" x2="39.15918125" y2="16.891" layer="21"/>
<rectangle x1="0.635" y1="16.891" x2="1.82118125" y2="16.97558125" layer="21"/>
<rectangle x1="4.78281875" y1="16.891" x2="5.88518125" y2="16.97558125" layer="21"/>
<rectangle x1="10.287" y1="16.891" x2="15.19681875" y2="16.97558125" layer="21"/>
<rectangle x1="18.07718125" y1="16.891" x2="20.36318125" y2="16.97558125" layer="21"/>
<rectangle x1="24.34081875" y1="16.891" x2="39.15918125" y2="16.97558125" layer="21"/>
<rectangle x1="0.635" y1="16.97558125" x2="1.82118125" y2="17.06041875" layer="21"/>
<rectangle x1="4.78281875" y1="16.97558125" x2="5.88518125" y2="17.06041875" layer="21"/>
<rectangle x1="10.20318125" y1="16.97558125" x2="15.02918125" y2="17.06041875" layer="21"/>
<rectangle x1="18.07718125" y1="16.97558125" x2="20.53081875" y2="17.06041875" layer="21"/>
<rectangle x1="24.17318125" y1="16.97558125" x2="39.07281875" y2="17.06041875" layer="21"/>
<rectangle x1="0.71881875" y1="17.06041875" x2="1.905" y2="17.145" layer="21"/>
<rectangle x1="4.78281875" y1="17.06041875" x2="5.88518125" y2="17.145" layer="21"/>
<rectangle x1="10.287" y1="17.06041875" x2="14.94281875" y2="17.145" layer="21"/>
<rectangle x1="18.07718125" y1="17.06041875" x2="20.61718125" y2="17.145" layer="21"/>
<rectangle x1="24.08681875" y1="17.06041875" x2="39.07281875" y2="17.145" layer="21"/>
<rectangle x1="0.71881875" y1="17.145" x2="1.905" y2="17.22958125" layer="21"/>
<rectangle x1="4.86918125" y1="17.145" x2="5.88518125" y2="17.22958125" layer="21"/>
<rectangle x1="10.287" y1="17.145" x2="14.77518125" y2="17.22958125" layer="21"/>
<rectangle x1="18.07718125" y1="17.145" x2="20.78481875" y2="17.22958125" layer="21"/>
<rectangle x1="23.91918125" y1="17.145" x2="39.07281875" y2="17.22958125" layer="21"/>
<rectangle x1="0.80518125" y1="17.22958125" x2="1.98881875" y2="17.31441875" layer="21"/>
<rectangle x1="4.86918125" y1="17.22958125" x2="5.88518125" y2="17.31441875" layer="21"/>
<rectangle x1="10.287" y1="17.22958125" x2="14.605" y2="17.31441875" layer="21"/>
<rectangle x1="18.07718125" y1="17.22958125" x2="20.955" y2="17.31441875" layer="21"/>
<rectangle x1="23.749" y1="17.22958125" x2="38.989" y2="17.31441875" layer="21"/>
<rectangle x1="0.80518125" y1="17.31441875" x2="1.98881875" y2="17.399" layer="21"/>
<rectangle x1="4.86918125" y1="17.31441875" x2="5.79881875" y2="17.399" layer="21"/>
<rectangle x1="10.37081875" y1="17.31441875" x2="14.43481875" y2="17.399" layer="21"/>
<rectangle x1="18.161" y1="17.31441875" x2="21.12518125" y2="17.399" layer="21"/>
<rectangle x1="23.57881875" y1="17.31441875" x2="38.989" y2="17.399" layer="21"/>
<rectangle x1="0.80518125" y1="17.399" x2="2.07518125" y2="17.48358125" layer="21"/>
<rectangle x1="4.953" y1="17.399" x2="5.715" y2="17.48358125" layer="21"/>
<rectangle x1="10.37081875" y1="17.399" x2="14.18081875" y2="17.48358125" layer="21"/>
<rectangle x1="18.161" y1="17.399" x2="21.37918125" y2="17.48358125" layer="21"/>
<rectangle x1="23.32481875" y1="17.399" x2="38.90518125" y2="17.48358125" layer="21"/>
<rectangle x1="0.889" y1="17.48358125" x2="2.07518125" y2="17.56841875" layer="21"/>
<rectangle x1="5.12318125" y1="17.48358125" x2="5.63118125" y2="17.56841875" layer="21"/>
<rectangle x1="10.541" y1="17.48358125" x2="13.843" y2="17.56841875" layer="21"/>
<rectangle x1="18.161" y1="17.48358125" x2="21.717" y2="17.56841875" layer="21"/>
<rectangle x1="22.987" y1="17.48358125" x2="38.90518125" y2="17.56841875" layer="21"/>
<rectangle x1="0.97281875" y1="17.56841875" x2="2.159" y2="17.653" layer="21"/>
<rectangle x1="18.161" y1="17.56841875" x2="38.81881875" y2="17.653" layer="21"/>
<rectangle x1="0.97281875" y1="17.653" x2="2.159" y2="17.73758125" layer="21"/>
<rectangle x1="18.161" y1="17.653" x2="38.81881875" y2="17.73758125" layer="21"/>
<rectangle x1="0.97281875" y1="17.73758125" x2="2.24281875" y2="17.82241875" layer="21"/>
<rectangle x1="18.24481875" y1="17.73758125" x2="38.81881875" y2="17.82241875" layer="21"/>
<rectangle x1="1.05918125" y1="17.82241875" x2="2.24281875" y2="17.907" layer="21"/>
<rectangle x1="18.24481875" y1="17.82241875" x2="38.735" y2="17.907" layer="21"/>
<rectangle x1="1.05918125" y1="17.907" x2="2.32918125" y2="17.99158125" layer="21"/>
<rectangle x1="18.24481875" y1="17.907" x2="38.735" y2="17.99158125" layer="21"/>
<rectangle x1="1.143" y1="17.99158125" x2="2.413" y2="18.07641875" layer="21"/>
<rectangle x1="18.24481875" y1="17.99158125" x2="38.65118125" y2="18.07641875" layer="21"/>
<rectangle x1="1.143" y1="18.07641875" x2="2.413" y2="18.161" layer="21"/>
<rectangle x1="18.24481875" y1="18.07641875" x2="38.56481875" y2="18.161" layer="21"/>
<rectangle x1="1.22681875" y1="18.161" x2="2.49681875" y2="18.24558125" layer="21"/>
<rectangle x1="18.33118125" y1="18.161" x2="38.56481875" y2="18.24558125" layer="21"/>
<rectangle x1="1.22681875" y1="18.24558125" x2="2.58318125" y2="18.33041875" layer="21"/>
<rectangle x1="18.33118125" y1="18.24558125" x2="38.481" y2="18.33041875" layer="21"/>
<rectangle x1="1.31318125" y1="18.33041875" x2="2.667" y2="18.415" layer="21"/>
<rectangle x1="18.33118125" y1="18.33041875" x2="38.481" y2="18.415" layer="21"/>
<rectangle x1="1.397" y1="18.415" x2="2.667" y2="18.49958125" layer="21"/>
<rectangle x1="18.33118125" y1="18.415" x2="38.39718125" y2="18.49958125" layer="21"/>
<rectangle x1="1.397" y1="18.49958125" x2="2.75081875" y2="18.58441875" layer="21"/>
<rectangle x1="18.415" y1="18.49958125" x2="38.31081875" y2="18.58441875" layer="21"/>
<rectangle x1="1.48081875" y1="18.58441875" x2="2.83718125" y2="18.669" layer="21"/>
<rectangle x1="18.415" y1="18.58441875" x2="38.31081875" y2="18.669" layer="21"/>
<rectangle x1="1.56718125" y1="18.669" x2="2.921" y2="18.75358125" layer="21"/>
<rectangle x1="18.415" y1="18.669" x2="38.227" y2="18.75358125" layer="21"/>
<rectangle x1="1.56718125" y1="18.75358125" x2="3.00481875" y2="18.83841875" layer="21"/>
<rectangle x1="18.49881875" y1="18.75358125" x2="38.227" y2="18.83841875" layer="21"/>
<rectangle x1="1.651" y1="18.83841875" x2="3.00481875" y2="18.923" layer="21"/>
<rectangle x1="18.49881875" y1="18.83841875" x2="38.14318125" y2="18.923" layer="21"/>
<rectangle x1="1.73481875" y1="18.923" x2="3.09118125" y2="19.00758125" layer="21"/>
<rectangle x1="18.49881875" y1="18.923" x2="38.05681875" y2="19.00758125" layer="21"/>
<rectangle x1="1.73481875" y1="19.00758125" x2="3.175" y2="19.09241875" layer="21"/>
<rectangle x1="18.49881875" y1="19.00758125" x2="37.973" y2="19.09241875" layer="21"/>
<rectangle x1="1.82118125" y1="19.09241875" x2="3.25881875" y2="19.177" layer="21"/>
<rectangle x1="18.58518125" y1="19.09241875" x2="37.973" y2="19.177" layer="21"/>
<rectangle x1="1.905" y1="19.177" x2="3.34518125" y2="19.26158125" layer="21"/>
<rectangle x1="18.58518125" y1="19.177" x2="37.88918125" y2="19.26158125" layer="21"/>
<rectangle x1="1.98881875" y1="19.26158125" x2="3.429" y2="19.34641875" layer="21"/>
<rectangle x1="18.669" y1="19.26158125" x2="37.80281875" y2="19.34641875" layer="21"/>
<rectangle x1="2.07518125" y1="19.34641875" x2="3.51281875" y2="19.431" layer="21"/>
<rectangle x1="18.669" y1="19.34641875" x2="37.719" y2="19.431" layer="21"/>
<rectangle x1="2.07518125" y1="19.431" x2="3.683" y2="19.51558125" layer="21"/>
<rectangle x1="18.669" y1="19.431" x2="37.63518125" y2="19.51558125" layer="21"/>
<rectangle x1="2.159" y1="19.51558125" x2="3.76681875" y2="19.60041875" layer="21"/>
<rectangle x1="18.669" y1="19.51558125" x2="37.54881875" y2="19.60041875" layer="21"/>
<rectangle x1="2.24281875" y1="19.60041875" x2="3.85318125" y2="19.685" layer="21"/>
<rectangle x1="18.75281875" y1="19.60041875" x2="37.465" y2="19.685" layer="21"/>
<rectangle x1="2.32918125" y1="19.685" x2="3.937" y2="19.76958125" layer="21"/>
<rectangle x1="18.75281875" y1="19.685" x2="37.465" y2="19.76958125" layer="21"/>
<rectangle x1="2.413" y1="19.76958125" x2="4.10718125" y2="19.85441875" layer="21"/>
<rectangle x1="18.83918125" y1="19.76958125" x2="37.38118125" y2="19.85441875" layer="21"/>
<rectangle x1="2.49681875" y1="19.85441875" x2="4.191" y2="19.939" layer="21"/>
<rectangle x1="18.83918125" y1="19.85441875" x2="37.29481875" y2="19.939" layer="21"/>
<rectangle x1="2.58318125" y1="19.939" x2="4.27481875" y2="20.02358125" layer="21"/>
<rectangle x1="18.923" y1="19.939" x2="37.211" y2="20.02358125" layer="21"/>
<rectangle x1="2.667" y1="20.02358125" x2="4.445" y2="20.10841875" layer="21"/>
<rectangle x1="18.923" y1="20.02358125" x2="37.12718125" y2="20.10841875" layer="21"/>
<rectangle x1="2.75081875" y1="20.10841875" x2="4.52881875" y2="20.193" layer="21"/>
<rectangle x1="19.00681875" y1="20.10841875" x2="36.957" y2="20.193" layer="21"/>
<rectangle x1="2.83718125" y1="20.193" x2="4.699" y2="20.27758125" layer="21"/>
<rectangle x1="19.00681875" y1="20.193" x2="36.87318125" y2="20.27758125" layer="21"/>
<rectangle x1="3.00481875" y1="20.27758125" x2="4.86918125" y2="20.36241875" layer="21"/>
<rectangle x1="19.09318125" y1="20.27758125" x2="36.78681875" y2="20.36241875" layer="21"/>
<rectangle x1="3.09118125" y1="20.36241875" x2="5.03681875" y2="20.447" layer="21"/>
<rectangle x1="19.09318125" y1="20.36241875" x2="36.703" y2="20.447" layer="21"/>
<rectangle x1="3.175" y1="20.447" x2="5.207" y2="20.53158125" layer="21"/>
<rectangle x1="19.177" y1="20.447" x2="36.61918125" y2="20.53158125" layer="21"/>
<rectangle x1="3.25881875" y1="20.53158125" x2="5.37718125" y2="20.61641875" layer="21"/>
<rectangle x1="19.177" y1="20.53158125" x2="36.53281875" y2="20.61641875" layer="21"/>
<rectangle x1="3.429" y1="20.61641875" x2="5.54481875" y2="20.701" layer="21"/>
<rectangle x1="19.26081875" y1="20.61641875" x2="36.36518125" y2="20.701" layer="21"/>
<rectangle x1="3.51281875" y1="20.701" x2="5.79881875" y2="20.78558125" layer="21"/>
<rectangle x1="19.26081875" y1="20.701" x2="36.27881875" y2="20.78558125" layer="21"/>
<rectangle x1="3.683" y1="20.78558125" x2="6.05281875" y2="20.87041875" layer="21"/>
<rectangle x1="19.34718125" y1="20.78558125" x2="36.195" y2="20.87041875" layer="21"/>
<rectangle x1="3.76681875" y1="20.87041875" x2="6.30681875" y2="20.955" layer="21"/>
<rectangle x1="19.34718125" y1="20.87041875" x2="36.02481875" y2="20.955" layer="21"/>
<rectangle x1="3.85318125" y1="20.955" x2="6.731" y2="21.03958125" layer="21"/>
<rectangle x1="19.431" y1="20.955" x2="35.85718125" y2="21.03958125" layer="21"/>
<rectangle x1="4.02081875" y1="21.03958125" x2="7.239" y2="21.12441875" layer="21"/>
<rectangle x1="19.51481875" y1="21.03958125" x2="35.77081875" y2="21.12441875" layer="21"/>
<rectangle x1="4.191" y1="21.12441875" x2="35.60318125" y2="21.209" layer="21"/>
<rectangle x1="4.27481875" y1="21.209" x2="35.433" y2="21.29358125" layer="21"/>
<rectangle x1="4.445" y1="21.29358125" x2="35.26281875" y2="21.37841875" layer="21"/>
<rectangle x1="4.61518125" y1="21.37841875" x2="35.179" y2="21.463" layer="21"/>
<rectangle x1="4.78281875" y1="21.463" x2="34.925" y2="21.54758125" layer="21"/>
<rectangle x1="4.953" y1="21.54758125" x2="34.75481875" y2="21.63241875" layer="21"/>
<rectangle x1="5.207" y1="21.63241875" x2="34.58718125" y2="21.717" layer="21"/>
<rectangle x1="5.461" y1="21.717" x2="34.33318125" y2="21.80158125" layer="21"/>
<rectangle x1="5.715" y1="21.80158125" x2="34.07918125" y2="21.88641875" layer="21"/>
<rectangle x1="5.969" y1="21.88641875" x2="33.82518125" y2="21.971" layer="21"/>
<rectangle x1="6.30681875" y1="21.971" x2="33.48481875" y2="22.05558125" layer="21"/>
<rectangle x1="6.731" y1="22.05558125" x2="32.97681875" y2="22.14041875" layer="21"/>
</package>
<package name="UDO-LOGO-10MM" urn="urn:adsk.eagle:footprint:6649251/1">
<rectangle x1="1.397" y1="0.127" x2="1.82118125" y2="0.21158125" layer="21"/>
<rectangle x1="3.85318125" y1="0.127" x2="4.191" y2="0.21158125" layer="21"/>
<rectangle x1="4.953" y1="0.127" x2="5.03681875" y2="0.21158125" layer="21"/>
<rectangle x1="5.63118125" y1="0.127" x2="5.715" y2="0.21158125" layer="21"/>
<rectangle x1="6.223" y1="0.127" x2="6.30681875" y2="0.21158125" layer="21"/>
<rectangle x1="7.06881875" y1="0.127" x2="7.40918125" y2="0.21158125" layer="21"/>
<rectangle x1="8.84681875" y1="0.127" x2="8.93318125" y2="0.21158125" layer="21"/>
<rectangle x1="1.22681875" y1="0.21158125" x2="1.905" y2="0.29641875" layer="21"/>
<rectangle x1="2.159" y1="0.21158125" x2="2.32918125" y2="0.29641875" layer="21"/>
<rectangle x1="2.75081875" y1="0.21158125" x2="2.83718125" y2="0.29641875" layer="21"/>
<rectangle x1="2.921" y1="0.21158125" x2="3.09118125" y2="0.29641875" layer="21"/>
<rectangle x1="3.34518125" y1="0.21158125" x2="3.59918125" y2="0.29641875" layer="21"/>
<rectangle x1="3.683" y1="0.21158125" x2="4.36118125" y2="0.29641875" layer="21"/>
<rectangle x1="4.78281875" y1="0.21158125" x2="5.207" y2="0.29641875" layer="21"/>
<rectangle x1="5.461" y1="0.21158125" x2="5.79881875" y2="0.29641875" layer="21"/>
<rectangle x1="6.05281875" y1="0.21158125" x2="6.477" y2="0.29641875" layer="21"/>
<rectangle x1="6.90118125" y1="0.21158125" x2="7.57681875" y2="0.29641875" layer="21"/>
<rectangle x1="7.747" y1="0.21158125" x2="7.91718125" y2="0.29641875" layer="21"/>
<rectangle x1="8.33881875" y1="0.21158125" x2="8.509" y2="0.29641875" layer="21"/>
<rectangle x1="8.67918125" y1="0.21158125" x2="9.10081875" y2="0.29641875" layer="21"/>
<rectangle x1="1.22681875" y1="0.29641875" x2="1.397" y2="0.381" layer="21"/>
<rectangle x1="1.82118125" y1="0.29641875" x2="1.98881875" y2="0.381" layer="21"/>
<rectangle x1="2.159" y1="0.29641875" x2="2.32918125" y2="0.381" layer="21"/>
<rectangle x1="2.75081875" y1="0.29641875" x2="2.83718125" y2="0.381" layer="21"/>
<rectangle x1="2.921" y1="0.29641875" x2="3.09118125" y2="0.381" layer="21"/>
<rectangle x1="3.25881875" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="3.683" y1="0.29641875" x2="3.85318125" y2="0.381" layer="21"/>
<rectangle x1="4.27481875" y1="0.29641875" x2="4.445" y2="0.381" layer="21"/>
<rectangle x1="4.699" y1="0.29641875" x2="4.86918125" y2="0.381" layer="21"/>
<rectangle x1="5.12318125" y1="0.29641875" x2="5.29081875" y2="0.381" layer="21"/>
<rectangle x1="5.37718125" y1="0.29641875" x2="5.54481875" y2="0.381" layer="21"/>
<rectangle x1="5.715" y1="0.29641875" x2="5.88518125" y2="0.381" layer="21"/>
<rectangle x1="5.969" y1="0.29641875" x2="6.13918125" y2="0.381" layer="21"/>
<rectangle x1="6.39318125" y1="0.29641875" x2="6.56081875" y2="0.381" layer="21"/>
<rectangle x1="6.81481875" y1="0.29641875" x2="6.985" y2="0.381" layer="21"/>
<rectangle x1="7.493" y1="0.29641875" x2="7.66318125" y2="0.381" layer="21"/>
<rectangle x1="7.747" y1="0.29641875" x2="7.91718125" y2="0.381" layer="21"/>
<rectangle x1="8.33881875" y1="0.29641875" x2="8.509" y2="0.381" layer="21"/>
<rectangle x1="8.59281875" y1="0.29641875" x2="8.763" y2="0.381" layer="21"/>
<rectangle x1="8.93318125" y1="0.29641875" x2="9.18718125" y2="0.381" layer="21"/>
<rectangle x1="1.143" y1="0.381" x2="1.31318125" y2="0.46558125" layer="21"/>
<rectangle x1="1.905" y1="0.381" x2="2.07518125" y2="0.46558125" layer="21"/>
<rectangle x1="2.159" y1="0.381" x2="2.32918125" y2="0.46558125" layer="21"/>
<rectangle x1="2.75081875" y1="0.381" x2="2.83718125" y2="0.46558125" layer="21"/>
<rectangle x1="2.921" y1="0.381" x2="3.09118125" y2="0.46558125" layer="21"/>
<rectangle x1="3.175" y1="0.381" x2="3.34518125" y2="0.46558125" layer="21"/>
<rectangle x1="3.683" y1="0.381" x2="3.85318125" y2="0.46558125" layer="21"/>
<rectangle x1="4.36118125" y1="0.381" x2="4.52881875" y2="0.46558125" layer="21"/>
<rectangle x1="4.699" y1="0.381" x2="4.78281875" y2="0.46558125" layer="21"/>
<rectangle x1="5.207" y1="0.381" x2="5.37718125" y2="0.46558125" layer="21"/>
<rectangle x1="5.715" y1="0.381" x2="5.88518125" y2="0.46558125" layer="21"/>
<rectangle x1="5.969" y1="0.381" x2="6.05281875" y2="0.46558125" layer="21"/>
<rectangle x1="6.477" y1="0.381" x2="6.56081875" y2="0.46558125" layer="21"/>
<rectangle x1="6.81481875" y1="0.381" x2="6.90118125" y2="0.46558125" layer="21"/>
<rectangle x1="7.57681875" y1="0.381" x2="7.66318125" y2="0.46558125" layer="21"/>
<rectangle x1="7.747" y1="0.381" x2="7.91718125" y2="0.46558125" layer="21"/>
<rectangle x1="8.33881875" y1="0.381" x2="8.67918125" y2="0.46558125" layer="21"/>
<rectangle x1="9.017" y1="0.381" x2="9.18718125" y2="0.46558125" layer="21"/>
<rectangle x1="1.143" y1="0.46558125" x2="1.22681875" y2="0.55041875" layer="21"/>
<rectangle x1="1.905" y1="0.46558125" x2="2.07518125" y2="0.55041875" layer="21"/>
<rectangle x1="2.159" y1="0.46558125" x2="2.32918125" y2="0.55041875" layer="21"/>
<rectangle x1="2.75081875" y1="0.46558125" x2="2.83718125" y2="0.55041875" layer="21"/>
<rectangle x1="2.921" y1="0.46558125" x2="3.09118125" y2="0.55041875" layer="21"/>
<rectangle x1="3.175" y1="0.46558125" x2="3.34518125" y2="0.55041875" layer="21"/>
<rectangle x1="3.683" y1="0.46558125" x2="3.85318125" y2="0.55041875" layer="21"/>
<rectangle x1="4.445" y1="0.46558125" x2="4.78281875" y2="0.55041875" layer="21"/>
<rectangle x1="5.207" y1="0.46558125" x2="5.37718125" y2="0.55041875" layer="21"/>
<rectangle x1="5.54481875" y1="0.46558125" x2="5.79881875" y2="0.55041875" layer="21"/>
<rectangle x1="5.88518125" y1="0.46558125" x2="6.64718125" y2="0.55041875" layer="21"/>
<rectangle x1="6.731" y1="0.46558125" x2="6.90118125" y2="0.55041875" layer="21"/>
<rectangle x1="7.57681875" y1="0.46558125" x2="7.91718125" y2="0.55041875" layer="21"/>
<rectangle x1="8.33881875" y1="0.46558125" x2="9.18718125" y2="0.55041875" layer="21"/>
<rectangle x1="1.05918125" y1="0.55041875" x2="1.22681875" y2="0.635" layer="21"/>
<rectangle x1="1.98881875" y1="0.55041875" x2="2.07518125" y2="0.635" layer="21"/>
<rectangle x1="2.159" y1="0.55041875" x2="2.32918125" y2="0.635" layer="21"/>
<rectangle x1="2.75081875" y1="0.55041875" x2="2.83718125" y2="0.635" layer="21"/>
<rectangle x1="2.921" y1="0.55041875" x2="3.09118125" y2="0.635" layer="21"/>
<rectangle x1="3.175" y1="0.55041875" x2="3.34518125" y2="0.635" layer="21"/>
<rectangle x1="3.683" y1="0.55041875" x2="3.85318125" y2="0.635" layer="21"/>
<rectangle x1="4.445" y1="0.55041875" x2="4.78281875" y2="0.635" layer="21"/>
<rectangle x1="5.207" y1="0.55041875" x2="5.715" y2="0.635" layer="21"/>
<rectangle x1="5.88518125" y1="0.55041875" x2="6.05281875" y2="0.635" layer="21"/>
<rectangle x1="6.13918125" y1="0.55041875" x2="6.39318125" y2="0.635" layer="21"/>
<rectangle x1="6.477" y1="0.55041875" x2="6.64718125" y2="0.635" layer="21"/>
<rectangle x1="6.731" y1="0.55041875" x2="6.90118125" y2="0.635" layer="21"/>
<rectangle x1="7.57681875" y1="0.55041875" x2="7.91718125" y2="0.635" layer="21"/>
<rectangle x1="8.33881875" y1="0.55041875" x2="8.67918125" y2="0.635" layer="21"/>
<rectangle x1="8.763" y1="0.55041875" x2="9.18718125" y2="0.635" layer="21"/>
<rectangle x1="1.05918125" y1="0.635" x2="1.22681875" y2="0.71958125" layer="21"/>
<rectangle x1="1.98881875" y1="0.635" x2="2.07518125" y2="0.71958125" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.32918125" y2="0.71958125" layer="21"/>
<rectangle x1="2.667" y1="0.635" x2="2.83718125" y2="0.71958125" layer="21"/>
<rectangle x1="2.921" y1="0.635" x2="3.09118125" y2="0.71958125" layer="21"/>
<rectangle x1="3.175" y1="0.635" x2="3.34518125" y2="0.71958125" layer="21"/>
<rectangle x1="3.683" y1="0.635" x2="3.85318125" y2="0.71958125" layer="21"/>
<rectangle x1="4.445" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="4.699" y1="0.635" x2="4.86918125" y2="0.71958125" layer="21"/>
<rectangle x1="5.12318125" y1="0.635" x2="5.29081875" y2="0.71958125" layer="21"/>
<rectangle x1="5.37718125" y1="0.635" x2="5.54481875" y2="0.71958125" layer="21"/>
<rectangle x1="5.715" y1="0.635" x2="5.88518125" y2="0.71958125" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.13918125" y2="0.71958125" layer="21"/>
<rectangle x1="6.39318125" y1="0.635" x2="6.56081875" y2="0.71958125" layer="21"/>
<rectangle x1="6.731" y1="0.635" x2="6.90118125" y2="0.71958125" layer="21"/>
<rectangle x1="7.57681875" y1="0.635" x2="7.747" y2="0.71958125" layer="21"/>
<rectangle x1="7.83081875" y1="0.635" x2="8.001" y2="0.71958125" layer="21"/>
<rectangle x1="8.255" y1="0.635" x2="8.42518125" y2="0.71958125" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="8.67918125" y2="0.71958125" layer="21"/>
<rectangle x1="9.017" y1="0.635" x2="9.18718125" y2="0.71958125" layer="21"/>
<rectangle x1="1.05918125" y1="0.71958125" x2="1.22681875" y2="0.80441875" layer="21"/>
<rectangle x1="1.98881875" y1="0.71958125" x2="2.07518125" y2="0.80441875" layer="21"/>
<rectangle x1="2.159" y1="0.71958125" x2="2.75081875" y2="0.80441875" layer="21"/>
<rectangle x1="2.921" y1="0.71958125" x2="3.59918125" y2="0.80441875" layer="21"/>
<rectangle x1="3.683" y1="0.71958125" x2="3.85318125" y2="0.80441875" layer="21"/>
<rectangle x1="4.445" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="4.78281875" y1="0.71958125" x2="5.29081875" y2="0.80441875" layer="21"/>
<rectangle x1="5.37718125" y1="0.71958125" x2="5.88518125" y2="0.80441875" layer="21"/>
<rectangle x1="6.05281875" y1="0.71958125" x2="6.56081875" y2="0.80441875" layer="21"/>
<rectangle x1="6.731" y1="0.71958125" x2="6.90118125" y2="0.80441875" layer="21"/>
<rectangle x1="7.57681875" y1="0.71958125" x2="8.33881875" y2="0.80441875" layer="21"/>
<rectangle x1="8.59281875" y1="0.71958125" x2="9.10081875" y2="0.80441875" layer="21"/>
<rectangle x1="1.05918125" y1="0.80441875" x2="1.22681875" y2="0.889" layer="21"/>
<rectangle x1="1.98881875" y1="0.80441875" x2="2.07518125" y2="0.889" layer="21"/>
<rectangle x1="2.159" y1="0.80441875" x2="2.24281875" y2="0.889" layer="21"/>
<rectangle x1="2.413" y1="0.80441875" x2="2.667" y2="0.889" layer="21"/>
<rectangle x1="2.921" y1="0.80441875" x2="3.00481875" y2="0.889" layer="21"/>
<rectangle x1="3.09118125" y1="0.80441875" x2="3.59918125" y2="0.889" layer="21"/>
<rectangle x1="3.683" y1="0.80441875" x2="3.85318125" y2="0.889" layer="21"/>
<rectangle x1="4.445" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="4.86918125" y1="0.80441875" x2="5.12318125" y2="0.889" layer="21"/>
<rectangle x1="5.54481875" y1="0.80441875" x2="5.715" y2="0.889" layer="21"/>
<rectangle x1="6.13918125" y1="0.80441875" x2="6.39318125" y2="0.889" layer="21"/>
<rectangle x1="6.81481875" y1="0.80441875" x2="6.90118125" y2="0.889" layer="21"/>
<rectangle x1="7.57681875" y1="0.80441875" x2="7.66318125" y2="0.889" layer="21"/>
<rectangle x1="8.001" y1="0.80441875" x2="8.255" y2="0.889" layer="21"/>
<rectangle x1="8.67918125" y1="0.80441875" x2="9.017" y2="0.889" layer="21"/>
<rectangle x1="1.05918125" y1="0.889" x2="1.22681875" y2="0.97358125" layer="21"/>
<rectangle x1="1.98881875" y1="0.889" x2="2.07518125" y2="0.97358125" layer="21"/>
<rectangle x1="2.921" y1="0.889" x2="3.09118125" y2="0.97358125" layer="21"/>
<rectangle x1="3.175" y1="0.889" x2="3.34518125" y2="0.97358125" layer="21"/>
<rectangle x1="3.683" y1="0.889" x2="3.85318125" y2="0.97358125" layer="21"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.52881875" y2="0.97358125" layer="21"/>
<rectangle x1="6.81481875" y1="0.889" x2="6.985" y2="0.97358125" layer="21"/>
<rectangle x1="7.493" y1="0.889" x2="7.66318125" y2="0.97358125" layer="21"/>
<rectangle x1="1.05918125" y1="0.97358125" x2="1.22681875" y2="1.05841875" layer="21"/>
<rectangle x1="1.98881875" y1="0.97358125" x2="2.07518125" y2="1.05841875" layer="21"/>
<rectangle x1="2.921" y1="0.97358125" x2="3.00481875" y2="1.05841875" layer="21"/>
<rectangle x1="3.175" y1="0.97358125" x2="3.34518125" y2="1.05841875" layer="21"/>
<rectangle x1="3.683" y1="0.97358125" x2="3.85318125" y2="1.05841875" layer="21"/>
<rectangle x1="4.27481875" y1="0.97358125" x2="4.445" y2="1.05841875" layer="21"/>
<rectangle x1="6.90118125" y1="0.97358125" x2="7.57681875" y2="1.05841875" layer="21"/>
<rectangle x1="1.05918125" y1="1.05841875" x2="1.22681875" y2="1.143" layer="21"/>
<rectangle x1="1.98881875" y1="1.05841875" x2="2.07518125" y2="1.143" layer="21"/>
<rectangle x1="3.175" y1="1.05841875" x2="3.34518125" y2="1.143" layer="21"/>
<rectangle x1="3.683" y1="1.05841875" x2="4.36118125" y2="1.143" layer="21"/>
<rectangle x1="7.06881875" y1="1.05841875" x2="7.40918125" y2="1.143" layer="21"/>
<rectangle x1="3.76681875" y1="1.143" x2="4.191" y2="1.22758125" layer="21"/>
<rectangle x1="1.82118125" y1="1.56641875" x2="8.255" y2="1.651" layer="21"/>
<rectangle x1="1.397" y1="1.651" x2="8.59281875" y2="1.73558125" layer="21"/>
<rectangle x1="1.143" y1="1.73558125" x2="8.84681875" y2="1.82041875" layer="21"/>
<rectangle x1="1.05918125" y1="1.82041875" x2="9.017" y2="1.905" layer="21"/>
<rectangle x1="0.889" y1="1.905" x2="1.651" y2="1.98958125" layer="21"/>
<rectangle x1="4.86918125" y1="1.905" x2="9.10081875" y2="1.98958125" layer="21"/>
<rectangle x1="0.80518125" y1="1.98958125" x2="1.397" y2="2.07441875" layer="21"/>
<rectangle x1="4.78281875" y1="1.98958125" x2="9.271" y2="2.07441875" layer="21"/>
<rectangle x1="0.635" y1="2.07441875" x2="1.22681875" y2="2.159" layer="21"/>
<rectangle x1="4.78281875" y1="2.07441875" x2="9.35481875" y2="2.159" layer="21"/>
<rectangle x1="0.55118125" y1="2.159" x2="1.05918125" y2="2.24358125" layer="21"/>
<rectangle x1="4.699" y1="2.159" x2="9.44118125" y2="2.24358125" layer="21"/>
<rectangle x1="0.46481875" y1="2.24358125" x2="0.97281875" y2="2.32841875" layer="21"/>
<rectangle x1="4.699" y1="2.24358125" x2="9.525" y2="2.32841875" layer="21"/>
<rectangle x1="0.46481875" y1="2.32841875" x2="0.80518125" y2="2.413" layer="21"/>
<rectangle x1="4.699" y1="2.32841875" x2="9.60881875" y2="2.413" layer="21"/>
<rectangle x1="0.381" y1="2.413" x2="0.71881875" y2="2.49758125" layer="21"/>
<rectangle x1="4.61518125" y1="2.413" x2="9.60881875" y2="2.49758125" layer="21"/>
<rectangle x1="0.29718125" y1="2.49758125" x2="0.71881875" y2="2.58241875" layer="21"/>
<rectangle x1="4.61518125" y1="2.49758125" x2="9.69518125" y2="2.58241875" layer="21"/>
<rectangle x1="0.29718125" y1="2.58241875" x2="0.635" y2="2.667" layer="21"/>
<rectangle x1="4.61518125" y1="2.58241875" x2="9.779" y2="2.667" layer="21"/>
<rectangle x1="0.21081875" y1="2.667" x2="0.55118125" y2="2.75158125" layer="21"/>
<rectangle x1="4.52881875" y1="2.667" x2="9.779" y2="2.75158125" layer="21"/>
<rectangle x1="0.21081875" y1="2.75158125" x2="0.55118125" y2="2.83641875" layer="21"/>
<rectangle x1="1.82118125" y1="2.75158125" x2="2.24281875" y2="2.83641875" layer="21"/>
<rectangle x1="2.921" y1="2.75158125" x2="3.51281875" y2="2.83641875" layer="21"/>
<rectangle x1="4.52881875" y1="2.75158125" x2="9.86281875" y2="2.83641875" layer="21"/>
<rectangle x1="0.127" y1="2.83641875" x2="0.46481875" y2="2.921" layer="21"/>
<rectangle x1="1.56718125" y1="2.83641875" x2="2.413" y2="2.921" layer="21"/>
<rectangle x1="2.83718125" y1="2.83641875" x2="3.683" y2="2.921" layer="21"/>
<rectangle x1="4.52881875" y1="2.83641875" x2="5.29081875" y2="2.921" layer="21"/>
<rectangle x1="5.969" y1="2.83641875" x2="6.64718125" y2="2.921" layer="21"/>
<rectangle x1="6.81481875" y1="2.83641875" x2="7.493" y2="2.921" layer="21"/>
<rectangle x1="7.66318125" y1="2.83641875" x2="8.17118125" y2="2.921" layer="21"/>
<rectangle x1="8.67918125" y1="2.83641875" x2="9.86281875" y2="2.921" layer="21"/>
<rectangle x1="0.127" y1="2.921" x2="0.46481875" y2="3.00558125" layer="21"/>
<rectangle x1="1.48081875" y1="2.921" x2="2.58318125" y2="3.00558125" layer="21"/>
<rectangle x1="2.83718125" y1="2.921" x2="3.85318125" y2="3.00558125" layer="21"/>
<rectangle x1="4.52881875" y1="2.921" x2="5.12318125" y2="3.00558125" layer="21"/>
<rectangle x1="6.13918125" y1="2.921" x2="6.64718125" y2="3.00558125" layer="21"/>
<rectangle x1="6.81481875" y1="2.921" x2="7.493" y2="3.00558125" layer="21"/>
<rectangle x1="7.747" y1="2.921" x2="8.001" y2="3.00558125" layer="21"/>
<rectangle x1="8.763" y1="2.921" x2="9.86281875" y2="3.00558125" layer="21"/>
<rectangle x1="0.127" y1="3.00558125" x2="0.381" y2="3.09041875" layer="21"/>
<rectangle x1="1.397" y1="3.00558125" x2="2.667" y2="3.09041875" layer="21"/>
<rectangle x1="2.921" y1="3.00558125" x2="3.937" y2="3.09041875" layer="21"/>
<rectangle x1="4.52881875" y1="3.00558125" x2="5.03681875" y2="3.09041875" layer="21"/>
<rectangle x1="5.54481875" y1="3.00558125" x2="5.715" y2="3.09041875" layer="21"/>
<rectangle x1="6.223" y1="3.00558125" x2="6.64718125" y2="3.09041875" layer="21"/>
<rectangle x1="6.81481875" y1="3.00558125" x2="7.493" y2="3.09041875" layer="21"/>
<rectangle x1="7.747" y1="3.00558125" x2="7.91718125" y2="3.09041875" layer="21"/>
<rectangle x1="8.255" y1="3.00558125" x2="8.59281875" y2="3.09041875" layer="21"/>
<rectangle x1="8.84681875" y1="3.00558125" x2="9.94918125" y2="3.09041875" layer="21"/>
<rectangle x1="0.04318125" y1="3.09041875" x2="0.381" y2="3.175" layer="21"/>
<rectangle x1="1.31318125" y1="3.09041875" x2="1.73481875" y2="3.175" layer="21"/>
<rectangle x1="2.24281875" y1="3.09041875" x2="2.667" y2="3.175" layer="21"/>
<rectangle x1="3.59918125" y1="3.09041875" x2="4.02081875" y2="3.175" layer="21"/>
<rectangle x1="4.52881875" y1="3.09041875" x2="4.953" y2="3.175" layer="21"/>
<rectangle x1="5.29081875" y1="3.09041875" x2="5.969" y2="3.175" layer="21"/>
<rectangle x1="6.30681875" y1="3.09041875" x2="6.64718125" y2="3.175" layer="21"/>
<rectangle x1="6.81481875" y1="3.09041875" x2="7.493" y2="3.175" layer="21"/>
<rectangle x1="7.747" y1="3.09041875" x2="7.91718125" y2="3.175" layer="21"/>
<rectangle x1="8.08481875" y1="3.09041875" x2="8.67918125" y2="3.175" layer="21"/>
<rectangle x1="8.84681875" y1="3.09041875" x2="9.94918125" y2="3.175" layer="21"/>
<rectangle x1="0.04318125" y1="3.175" x2="0.381" y2="3.25958125" layer="21"/>
<rectangle x1="1.31318125" y1="3.175" x2="1.651" y2="3.25958125" layer="21"/>
<rectangle x1="2.413" y1="3.175" x2="2.75081875" y2="3.25958125" layer="21"/>
<rectangle x1="3.683" y1="3.175" x2="4.02081875" y2="3.25958125" layer="21"/>
<rectangle x1="4.52881875" y1="3.175" x2="4.953" y2="3.25958125" layer="21"/>
<rectangle x1="5.207" y1="3.175" x2="6.05281875" y2="3.25958125" layer="21"/>
<rectangle x1="6.30681875" y1="3.175" x2="6.64718125" y2="3.25958125" layer="21"/>
<rectangle x1="6.81481875" y1="3.175" x2="7.493" y2="3.25958125" layer="21"/>
<rectangle x1="7.66318125" y1="3.175" x2="7.83081875" y2="3.25958125" layer="21"/>
<rectangle x1="8.08481875" y1="3.175" x2="9.94918125" y2="3.25958125" layer="21"/>
<rectangle x1="0.04318125" y1="3.25958125" x2="0.29718125" y2="3.34441875" layer="21"/>
<rectangle x1="1.22681875" y1="3.25958125" x2="1.56718125" y2="3.34441875" layer="21"/>
<rectangle x1="2.49681875" y1="3.25958125" x2="2.75081875" y2="3.34441875" layer="21"/>
<rectangle x1="3.76681875" y1="3.25958125" x2="4.10718125" y2="3.34441875" layer="21"/>
<rectangle x1="4.445" y1="3.25958125" x2="4.86918125" y2="3.34441875" layer="21"/>
<rectangle x1="5.12318125" y1="3.25958125" x2="6.13918125" y2="3.34441875" layer="21"/>
<rectangle x1="6.39318125" y1="3.25958125" x2="6.64718125" y2="3.34441875" layer="21"/>
<rectangle x1="6.81481875" y1="3.25958125" x2="7.493" y2="3.34441875" layer="21"/>
<rectangle x1="7.66318125" y1="3.25958125" x2="7.83081875" y2="3.34441875" layer="21"/>
<rectangle x1="8.93318125" y1="3.25958125" x2="9.94918125" y2="3.34441875" layer="21"/>
<rectangle x1="0.04318125" y1="3.34441875" x2="0.29718125" y2="3.429" layer="21"/>
<rectangle x1="1.22681875" y1="3.34441875" x2="1.48081875" y2="3.429" layer="21"/>
<rectangle x1="2.49681875" y1="3.34441875" x2="2.83718125" y2="3.429" layer="21"/>
<rectangle x1="3.76681875" y1="3.34441875" x2="4.10718125" y2="3.429" layer="21"/>
<rectangle x1="4.445" y1="3.34441875" x2="4.86918125" y2="3.429" layer="21"/>
<rectangle x1="5.12318125" y1="3.34441875" x2="6.13918125" y2="3.429" layer="21"/>
<rectangle x1="6.39318125" y1="3.34441875" x2="6.64718125" y2="3.429" layer="21"/>
<rectangle x1="6.81481875" y1="3.34441875" x2="7.493" y2="3.429" layer="21"/>
<rectangle x1="7.66318125" y1="3.34441875" x2="7.83081875" y2="3.429" layer="21"/>
<rectangle x1="8.93318125" y1="3.34441875" x2="9.94918125" y2="3.429" layer="21"/>
<rectangle x1="0.04318125" y1="3.429" x2="0.29718125" y2="3.51358125" layer="21"/>
<rectangle x1="1.143" y1="3.429" x2="1.48081875" y2="3.51358125" layer="21"/>
<rectangle x1="2.49681875" y1="3.429" x2="2.83718125" y2="3.51358125" layer="21"/>
<rectangle x1="3.85318125" y1="3.429" x2="4.10718125" y2="3.51358125" layer="21"/>
<rectangle x1="4.445" y1="3.429" x2="4.78281875" y2="3.51358125" layer="21"/>
<rectangle x1="5.03681875" y1="3.429" x2="6.13918125" y2="3.51358125" layer="21"/>
<rectangle x1="6.39318125" y1="3.429" x2="6.64718125" y2="3.51358125" layer="21"/>
<rectangle x1="6.81481875" y1="3.429" x2="7.493" y2="3.51358125" layer="21"/>
<rectangle x1="7.66318125" y1="3.429" x2="7.83081875" y2="3.51358125" layer="21"/>
<rectangle x1="8.08481875" y1="3.429" x2="8.67918125" y2="3.51358125" layer="21"/>
<rectangle x1="8.93318125" y1="3.429" x2="9.94918125" y2="3.51358125" layer="21"/>
<rectangle x1="0.04318125" y1="3.51358125" x2="0.29718125" y2="3.59841875" layer="21"/>
<rectangle x1="1.143" y1="3.51358125" x2="1.48081875" y2="3.59841875" layer="21"/>
<rectangle x1="2.58318125" y1="3.51358125" x2="2.83718125" y2="3.59841875" layer="21"/>
<rectangle x1="3.85318125" y1="3.51358125" x2="4.191" y2="3.59841875" layer="21"/>
<rectangle x1="4.445" y1="3.51358125" x2="4.78281875" y2="3.59841875" layer="21"/>
<rectangle x1="5.03681875" y1="3.51358125" x2="6.223" y2="3.59841875" layer="21"/>
<rectangle x1="6.477" y1="3.51358125" x2="6.64718125" y2="3.59841875" layer="21"/>
<rectangle x1="6.90118125" y1="3.51358125" x2="7.40918125" y2="3.59841875" layer="21"/>
<rectangle x1="7.66318125" y1="3.51358125" x2="7.91718125" y2="3.59841875" layer="21"/>
<rectangle x1="8.08481875" y1="3.51358125" x2="8.67918125" y2="3.59841875" layer="21"/>
<rectangle x1="8.84681875" y1="3.51358125" x2="9.94918125" y2="3.59841875" layer="21"/>
<rectangle x1="0.04318125" y1="3.59841875" x2="0.29718125" y2="3.683" layer="21"/>
<rectangle x1="1.143" y1="3.59841875" x2="1.48081875" y2="3.683" layer="21"/>
<rectangle x1="2.58318125" y1="3.59841875" x2="2.83718125" y2="3.683" layer="21"/>
<rectangle x1="3.85318125" y1="3.59841875" x2="4.191" y2="3.683" layer="21"/>
<rectangle x1="4.445" y1="3.59841875" x2="4.78281875" y2="3.683" layer="21"/>
<rectangle x1="5.03681875" y1="3.59841875" x2="6.223" y2="3.683" layer="21"/>
<rectangle x1="6.477" y1="3.59841875" x2="6.64718125" y2="3.683" layer="21"/>
<rectangle x1="6.985" y1="3.59841875" x2="7.32281875" y2="3.683" layer="21"/>
<rectangle x1="7.66318125" y1="3.59841875" x2="7.91718125" y2="3.683" layer="21"/>
<rectangle x1="8.17118125" y1="3.59841875" x2="8.59281875" y2="3.683" layer="21"/>
<rectangle x1="8.84681875" y1="3.59841875" x2="9.94918125" y2="3.683" layer="21"/>
<rectangle x1="0.04318125" y1="3.683" x2="0.29718125" y2="3.76758125" layer="21"/>
<rectangle x1="1.143" y1="3.683" x2="1.48081875" y2="3.76758125" layer="21"/>
<rectangle x1="2.58318125" y1="3.683" x2="2.83718125" y2="3.76758125" layer="21"/>
<rectangle x1="3.85318125" y1="3.683" x2="4.10718125" y2="3.76758125" layer="21"/>
<rectangle x1="4.445" y1="3.683" x2="4.78281875" y2="3.76758125" layer="21"/>
<rectangle x1="5.03681875" y1="3.683" x2="6.13918125" y2="3.76758125" layer="21"/>
<rectangle x1="6.39318125" y1="3.683" x2="6.64718125" y2="3.76758125" layer="21"/>
<rectangle x1="7.57681875" y1="3.683" x2="8.001" y2="3.76758125" layer="21"/>
<rectangle x1="8.763" y1="3.683" x2="9.94918125" y2="3.76758125" layer="21"/>
<rectangle x1="0.04318125" y1="3.76758125" x2="0.29718125" y2="3.85241875" layer="21"/>
<rectangle x1="1.143" y1="3.76758125" x2="1.48081875" y2="3.85241875" layer="21"/>
<rectangle x1="2.58318125" y1="3.76758125" x2="2.83718125" y2="3.85241875" layer="21"/>
<rectangle x1="3.85318125" y1="3.76758125" x2="4.10718125" y2="3.85241875" layer="21"/>
<rectangle x1="4.445" y1="3.76758125" x2="4.86918125" y2="3.85241875" layer="21"/>
<rectangle x1="5.12318125" y1="3.76758125" x2="6.13918125" y2="3.85241875" layer="21"/>
<rectangle x1="6.39318125" y1="3.76758125" x2="6.64718125" y2="3.85241875" layer="21"/>
<rectangle x1="6.81481875" y1="3.76758125" x2="6.90118125" y2="3.85241875" layer="21"/>
<rectangle x1="7.40918125" y1="3.76758125" x2="8.08481875" y2="3.85241875" layer="21"/>
<rectangle x1="8.67918125" y1="3.76758125" x2="9.94918125" y2="3.85241875" layer="21"/>
<rectangle x1="0.04318125" y1="3.85241875" x2="0.29718125" y2="3.937" layer="21"/>
<rectangle x1="1.143" y1="3.85241875" x2="1.48081875" y2="3.937" layer="21"/>
<rectangle x1="2.58318125" y1="3.85241875" x2="2.83718125" y2="3.937" layer="21"/>
<rectangle x1="3.76681875" y1="3.85241875" x2="4.10718125" y2="3.937" layer="21"/>
<rectangle x1="4.445" y1="3.85241875" x2="4.86918125" y2="3.937" layer="21"/>
<rectangle x1="5.12318125" y1="3.85241875" x2="6.13918125" y2="3.937" layer="21"/>
<rectangle x1="6.39318125" y1="3.85241875" x2="7.06881875" y2="3.937" layer="21"/>
<rectangle x1="7.239" y1="3.85241875" x2="8.33881875" y2="3.937" layer="21"/>
<rectangle x1="8.42518125" y1="3.85241875" x2="9.94918125" y2="3.937" layer="21"/>
<rectangle x1="0.04318125" y1="3.937" x2="0.381" y2="4.02158125" layer="21"/>
<rectangle x1="1.143" y1="3.937" x2="1.48081875" y2="4.02158125" layer="21"/>
<rectangle x1="2.58318125" y1="3.937" x2="2.83718125" y2="4.02158125" layer="21"/>
<rectangle x1="3.683" y1="3.937" x2="4.02081875" y2="4.02158125" layer="21"/>
<rectangle x1="4.52881875" y1="3.937" x2="4.86918125" y2="4.02158125" layer="21"/>
<rectangle x1="5.207" y1="3.937" x2="6.05281875" y2="4.02158125" layer="21"/>
<rectangle x1="6.30681875" y1="3.937" x2="9.94918125" y2="4.02158125" layer="21"/>
<rectangle x1="0.04318125" y1="4.02158125" x2="0.381" y2="4.10641875" layer="21"/>
<rectangle x1="1.143" y1="4.02158125" x2="1.48081875" y2="4.10641875" layer="21"/>
<rectangle x1="2.58318125" y1="4.02158125" x2="2.83718125" y2="4.10641875" layer="21"/>
<rectangle x1="3.59918125" y1="4.02158125" x2="4.02081875" y2="4.10641875" layer="21"/>
<rectangle x1="4.52881875" y1="4.02158125" x2="4.953" y2="4.10641875" layer="21"/>
<rectangle x1="5.29081875" y1="4.02158125" x2="5.969" y2="4.10641875" layer="21"/>
<rectangle x1="6.30681875" y1="4.02158125" x2="9.94918125" y2="4.10641875" layer="21"/>
<rectangle x1="0.04318125" y1="4.10641875" x2="0.381" y2="4.191" layer="21"/>
<rectangle x1="1.143" y1="4.10641875" x2="1.48081875" y2="4.191" layer="21"/>
<rectangle x1="2.58318125" y1="4.10641875" x2="3.937" y2="4.191" layer="21"/>
<rectangle x1="4.52881875" y1="4.10641875" x2="5.03681875" y2="4.191" layer="21"/>
<rectangle x1="5.461" y1="4.10641875" x2="5.79881875" y2="4.191" layer="21"/>
<rectangle x1="6.223" y1="4.10641875" x2="9.94918125" y2="4.191" layer="21"/>
<rectangle x1="0.127" y1="4.191" x2="0.46481875" y2="4.27558125" layer="21"/>
<rectangle x1="1.143" y1="4.191" x2="1.48081875" y2="4.27558125" layer="21"/>
<rectangle x1="2.58318125" y1="4.191" x2="3.85318125" y2="4.27558125" layer="21"/>
<rectangle x1="4.52881875" y1="4.191" x2="5.12318125" y2="4.27558125" layer="21"/>
<rectangle x1="6.13918125" y1="4.191" x2="9.86281875" y2="4.27558125" layer="21"/>
<rectangle x1="0.127" y1="4.27558125" x2="0.46481875" y2="4.36041875" layer="21"/>
<rectangle x1="1.143" y1="4.27558125" x2="1.48081875" y2="4.36041875" layer="21"/>
<rectangle x1="2.58318125" y1="4.27558125" x2="3.76681875" y2="4.36041875" layer="21"/>
<rectangle x1="4.52881875" y1="4.27558125" x2="5.207" y2="4.36041875" layer="21"/>
<rectangle x1="5.969" y1="4.27558125" x2="9.86281875" y2="4.36041875" layer="21"/>
<rectangle x1="0.21081875" y1="4.36041875" x2="0.46481875" y2="4.445" layer="21"/>
<rectangle x1="1.22681875" y1="4.36041875" x2="1.397" y2="4.445" layer="21"/>
<rectangle x1="2.58318125" y1="4.36041875" x2="3.59918125" y2="4.445" layer="21"/>
<rectangle x1="4.52881875" y1="4.36041875" x2="5.461" y2="4.445" layer="21"/>
<rectangle x1="5.79881875" y1="4.36041875" x2="9.86281875" y2="4.445" layer="21"/>
<rectangle x1="0.21081875" y1="4.445" x2="0.55118125" y2="4.52958125" layer="21"/>
<rectangle x1="4.52881875" y1="4.445" x2="9.779" y2="4.52958125" layer="21"/>
<rectangle x1="0.29718125" y1="4.52958125" x2="0.635" y2="4.61441875" layer="21"/>
<rectangle x1="4.61518125" y1="4.52958125" x2="9.779" y2="4.61441875" layer="21"/>
<rectangle x1="0.29718125" y1="4.61441875" x2="0.635" y2="4.699" layer="21"/>
<rectangle x1="4.61518125" y1="4.61441875" x2="9.69518125" y2="4.699" layer="21"/>
<rectangle x1="0.381" y1="4.699" x2="0.71881875" y2="4.78358125" layer="21"/>
<rectangle x1="4.61518125" y1="4.699" x2="9.60881875" y2="4.78358125" layer="21"/>
<rectangle x1="0.46481875" y1="4.78358125" x2="0.80518125" y2="4.86841875" layer="21"/>
<rectangle x1="4.699" y1="4.78358125" x2="9.60881875" y2="4.86841875" layer="21"/>
<rectangle x1="0.46481875" y1="4.86841875" x2="0.889" y2="4.953" layer="21"/>
<rectangle x1="4.699" y1="4.86841875" x2="9.525" y2="4.953" layer="21"/>
<rectangle x1="0.55118125" y1="4.953" x2="1.05918125" y2="5.03758125" layer="21"/>
<rectangle x1="4.699" y1="4.953" x2="9.44118125" y2="5.03758125" layer="21"/>
<rectangle x1="0.635" y1="5.03758125" x2="1.143" y2="5.12241875" layer="21"/>
<rectangle x1="4.78281875" y1="5.03758125" x2="9.35481875" y2="5.12241875" layer="21"/>
<rectangle x1="0.71881875" y1="5.12241875" x2="1.31318125" y2="5.207" layer="21"/>
<rectangle x1="4.78281875" y1="5.12241875" x2="9.271" y2="5.207" layer="21"/>
<rectangle x1="0.889" y1="5.207" x2="1.56718125" y2="5.29158125" layer="21"/>
<rectangle x1="4.86918125" y1="5.207" x2="9.18718125" y2="5.29158125" layer="21"/>
<rectangle x1="0.97281875" y1="5.29158125" x2="9.017" y2="5.37641875" layer="21"/>
<rectangle x1="1.143" y1="5.37641875" x2="8.84681875" y2="5.461" layer="21"/>
<rectangle x1="1.31318125" y1="5.461" x2="8.67918125" y2="5.54558125" layer="21"/>
<rectangle x1="1.651" y1="5.54558125" x2="8.33881875" y2="5.63041875" layer="21"/>
</package>
<package name="UDO-LOGO-12MM" urn="urn:adsk.eagle:footprint:6649250/1">
<rectangle x1="1.651" y1="0.21158125" x2="2.24281875" y2="0.29641875" layer="21"/>
<rectangle x1="2.667" y1="0.21158125" x2="2.75081875" y2="0.29641875" layer="21"/>
<rectangle x1="3.34518125" y1="0.21158125" x2="3.429" y2="0.29641875" layer="21"/>
<rectangle x1="4.10718125" y1="0.21158125" x2="4.27481875" y2="0.29641875" layer="21"/>
<rectangle x1="4.52881875" y1="0.21158125" x2="5.12318125" y2="0.29641875" layer="21"/>
<rectangle x1="5.88518125" y1="0.21158125" x2="6.13918125" y2="0.29641875" layer="21"/>
<rectangle x1="6.64718125" y1="0.21158125" x2="6.90118125" y2="0.29641875" layer="21"/>
<rectangle x1="7.40918125" y1="0.21158125" x2="7.66318125" y2="0.29641875" layer="21"/>
<rectangle x1="8.42518125" y1="0.21158125" x2="9.017" y2="0.29641875" layer="21"/>
<rectangle x1="10.541" y1="0.21158125" x2="10.795" y2="0.29641875" layer="21"/>
<rectangle x1="1.56718125" y1="0.29641875" x2="1.73481875" y2="0.381" layer="21"/>
<rectangle x1="2.07518125" y1="0.29641875" x2="2.32918125" y2="0.381" layer="21"/>
<rectangle x1="2.58318125" y1="0.29641875" x2="2.75081875" y2="0.381" layer="21"/>
<rectangle x1="3.34518125" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="3.51281875" y1="0.29641875" x2="3.683" y2="0.381" layer="21"/>
<rectangle x1="4.02081875" y1="0.29641875" x2="4.27481875" y2="0.381" layer="21"/>
<rectangle x1="4.52881875" y1="0.29641875" x2="5.29081875" y2="0.381" layer="21"/>
<rectangle x1="5.79881875" y1="0.29641875" x2="6.30681875" y2="0.381" layer="21"/>
<rectangle x1="6.56081875" y1="0.29641875" x2="6.985" y2="0.381" layer="21"/>
<rectangle x1="7.32281875" y1="0.29641875" x2="7.83081875" y2="0.381" layer="21"/>
<rectangle x1="8.33881875" y1="0.29641875" x2="8.509" y2="0.381" layer="21"/>
<rectangle x1="8.93318125" y1="0.29641875" x2="9.10081875" y2="0.381" layer="21"/>
<rectangle x1="9.35481875" y1="0.29641875" x2="9.525" y2="0.381" layer="21"/>
<rectangle x1="10.033" y1="0.29641875" x2="10.20318125" y2="0.381" layer="21"/>
<rectangle x1="10.37081875" y1="0.29641875" x2="10.96518125" y2="0.381" layer="21"/>
<rectangle x1="1.48081875" y1="0.381" x2="1.651" y2="0.46558125" layer="21"/>
<rectangle x1="2.24281875" y1="0.381" x2="2.413" y2="0.46558125" layer="21"/>
<rectangle x1="2.58318125" y1="0.381" x2="2.75081875" y2="0.46558125" layer="21"/>
<rectangle x1="3.34518125" y1="0.381" x2="3.429" y2="0.46558125" layer="21"/>
<rectangle x1="3.51281875" y1="0.381" x2="3.683" y2="0.46558125" layer="21"/>
<rectangle x1="3.937" y1="0.381" x2="4.10718125" y2="0.46558125" layer="21"/>
<rectangle x1="4.52881875" y1="0.381" x2="4.61518125" y2="0.46558125" layer="21"/>
<rectangle x1="5.207" y1="0.381" x2="5.37718125" y2="0.46558125" layer="21"/>
<rectangle x1="5.715" y1="0.381" x2="5.88518125" y2="0.46558125" layer="21"/>
<rectangle x1="6.223" y1="0.381" x2="6.39318125" y2="0.46558125" layer="21"/>
<rectangle x1="6.56081875" y1="0.381" x2="6.64718125" y2="0.46558125" layer="21"/>
<rectangle x1="6.90118125" y1="0.381" x2="7.06881875" y2="0.46558125" layer="21"/>
<rectangle x1="7.239" y1="0.381" x2="7.40918125" y2="0.46558125" layer="21"/>
<rectangle x1="7.747" y1="0.381" x2="7.91718125" y2="0.46558125" layer="21"/>
<rectangle x1="8.255" y1="0.381" x2="8.42518125" y2="0.46558125" layer="21"/>
<rectangle x1="9.017" y1="0.381" x2="9.18718125" y2="0.46558125" layer="21"/>
<rectangle x1="9.35481875" y1="0.381" x2="9.525" y2="0.46558125" layer="21"/>
<rectangle x1="10.033" y1="0.381" x2="10.20318125" y2="0.46558125" layer="21"/>
<rectangle x1="10.37081875" y1="0.381" x2="10.45718125" y2="0.46558125" layer="21"/>
<rectangle x1="10.87881875" y1="0.381" x2="11.049" y2="0.46558125" layer="21"/>
<rectangle x1="1.397" y1="0.46558125" x2="1.56718125" y2="0.55041875" layer="21"/>
<rectangle x1="2.32918125" y1="0.46558125" x2="2.49681875" y2="0.55041875" layer="21"/>
<rectangle x1="2.58318125" y1="0.46558125" x2="2.75081875" y2="0.55041875" layer="21"/>
<rectangle x1="3.34518125" y1="0.46558125" x2="3.429" y2="0.55041875" layer="21"/>
<rectangle x1="3.51281875" y1="0.46558125" x2="3.683" y2="0.55041875" layer="21"/>
<rectangle x1="3.85318125" y1="0.46558125" x2="4.02081875" y2="0.55041875" layer="21"/>
<rectangle x1="4.52881875" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="21"/>
<rectangle x1="5.29081875" y1="0.46558125" x2="5.461" y2="0.55041875" layer="21"/>
<rectangle x1="5.63118125" y1="0.46558125" x2="5.79881875" y2="0.55041875" layer="21"/>
<rectangle x1="6.30681875" y1="0.46558125" x2="6.39318125" y2="0.55041875" layer="21"/>
<rectangle x1="6.90118125" y1="0.46558125" x2="7.06881875" y2="0.55041875" layer="21"/>
<rectangle x1="7.15518125" y1="0.46558125" x2="7.32281875" y2="0.55041875" layer="21"/>
<rectangle x1="7.83081875" y1="0.46558125" x2="7.91718125" y2="0.55041875" layer="21"/>
<rectangle x1="8.17118125" y1="0.46558125" x2="8.33881875" y2="0.55041875" layer="21"/>
<rectangle x1="9.10081875" y1="0.46558125" x2="9.271" y2="0.55041875" layer="21"/>
<rectangle x1="9.35481875" y1="0.46558125" x2="9.525" y2="0.55041875" layer="21"/>
<rectangle x1="10.033" y1="0.46558125" x2="10.20318125" y2="0.55041875" layer="21"/>
<rectangle x1="10.287" y1="0.46558125" x2="10.45718125" y2="0.55041875" layer="21"/>
<rectangle x1="1.397" y1="0.55041875" x2="1.48081875" y2="0.635" layer="21"/>
<rectangle x1="2.32918125" y1="0.55041875" x2="2.49681875" y2="0.635" layer="21"/>
<rectangle x1="2.667" y1="0.55041875" x2="2.75081875" y2="0.635" layer="21"/>
<rectangle x1="3.34518125" y1="0.55041875" x2="3.429" y2="0.635" layer="21"/>
<rectangle x1="3.51281875" y1="0.55041875" x2="3.683" y2="0.635" layer="21"/>
<rectangle x1="3.85318125" y1="0.55041875" x2="4.02081875" y2="0.635" layer="21"/>
<rectangle x1="4.52881875" y1="0.55041875" x2="4.61518125" y2="0.635" layer="21"/>
<rectangle x1="5.37718125" y1="0.55041875" x2="5.461" y2="0.635" layer="21"/>
<rectangle x1="5.63118125" y1="0.55041875" x2="5.715" y2="0.635" layer="21"/>
<rectangle x1="6.30681875" y1="0.55041875" x2="6.39318125" y2="0.635" layer="21"/>
<rectangle x1="6.731" y1="0.55041875" x2="6.985" y2="0.635" layer="21"/>
<rectangle x1="7.15518125" y1="0.55041875" x2="7.91718125" y2="0.635" layer="21"/>
<rectangle x1="8.17118125" y1="0.55041875" x2="8.255" y2="0.635" layer="21"/>
<rectangle x1="9.18718125" y1="0.55041875" x2="9.271" y2="0.635" layer="21"/>
<rectangle x1="9.35481875" y1="0.55041875" x2="9.525" y2="0.635" layer="21"/>
<rectangle x1="10.033" y1="0.55041875" x2="10.20318125" y2="0.635" layer="21"/>
<rectangle x1="10.287" y1="0.55041875" x2="10.96518125" y2="0.635" layer="21"/>
<rectangle x1="1.31318125" y1="0.635" x2="1.48081875" y2="0.71958125" layer="21"/>
<rectangle x1="2.413" y1="0.635" x2="2.49681875" y2="0.71958125" layer="21"/>
<rectangle x1="2.667" y1="0.635" x2="2.75081875" y2="0.71958125" layer="21"/>
<rectangle x1="3.25881875" y1="0.635" x2="3.429" y2="0.71958125" layer="21"/>
<rectangle x1="3.51281875" y1="0.635" x2="3.683" y2="0.71958125" layer="21"/>
<rectangle x1="3.85318125" y1="0.635" x2="4.02081875" y2="0.71958125" layer="21"/>
<rectangle x1="4.52881875" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="5.37718125" y1="0.635" x2="5.54481875" y2="0.71958125" layer="21"/>
<rectangle x1="5.63118125" y1="0.635" x2="5.715" y2="0.71958125" layer="21"/>
<rectangle x1="6.30681875" y1="0.635" x2="6.39318125" y2="0.71958125" layer="21"/>
<rectangle x1="6.56081875" y1="0.635" x2="6.90118125" y2="0.71958125" layer="21"/>
<rectangle x1="7.15518125" y1="0.635" x2="7.91718125" y2="0.71958125" layer="21"/>
<rectangle x1="8.17118125" y1="0.635" x2="8.255" y2="0.71958125" layer="21"/>
<rectangle x1="9.18718125" y1="0.635" x2="9.271" y2="0.71958125" layer="21"/>
<rectangle x1="9.35481875" y1="0.635" x2="9.525" y2="0.71958125" layer="21"/>
<rectangle x1="10.033" y1="0.635" x2="10.20318125" y2="0.71958125" layer="21"/>
<rectangle x1="10.287" y1="0.635" x2="11.049" y2="0.71958125" layer="21"/>
<rectangle x1="1.31318125" y1="0.71958125" x2="1.48081875" y2="0.80441875" layer="21"/>
<rectangle x1="2.413" y1="0.71958125" x2="2.49681875" y2="0.80441875" layer="21"/>
<rectangle x1="2.667" y1="0.71958125" x2="2.75081875" y2="0.80441875" layer="21"/>
<rectangle x1="3.25881875" y1="0.71958125" x2="3.429" y2="0.80441875" layer="21"/>
<rectangle x1="3.51281875" y1="0.71958125" x2="3.683" y2="0.80441875" layer="21"/>
<rectangle x1="3.85318125" y1="0.71958125" x2="4.02081875" y2="0.80441875" layer="21"/>
<rectangle x1="4.52881875" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="5.37718125" y1="0.71958125" x2="5.54481875" y2="0.80441875" layer="21"/>
<rectangle x1="5.63118125" y1="0.71958125" x2="5.79881875" y2="0.80441875" layer="21"/>
<rectangle x1="6.30681875" y1="0.71958125" x2="6.39318125" y2="0.80441875" layer="21"/>
<rectangle x1="6.477" y1="0.71958125" x2="6.64718125" y2="0.80441875" layer="21"/>
<rectangle x1="7.15518125" y1="0.71958125" x2="7.32281875" y2="0.80441875" layer="21"/>
<rectangle x1="7.83081875" y1="0.71958125" x2="7.91718125" y2="0.80441875" layer="21"/>
<rectangle x1="8.08481875" y1="0.71958125" x2="8.255" y2="0.80441875" layer="21"/>
<rectangle x1="9.18718125" y1="0.71958125" x2="9.271" y2="0.80441875" layer="21"/>
<rectangle x1="9.44118125" y1="0.71958125" x2="9.525" y2="0.80441875" layer="21"/>
<rectangle x1="10.033" y1="0.71958125" x2="10.20318125" y2="0.80441875" layer="21"/>
<rectangle x1="10.287" y1="0.71958125" x2="10.37081875" y2="0.80441875" layer="21"/>
<rectangle x1="10.87881875" y1="0.71958125" x2="11.049" y2="0.80441875" layer="21"/>
<rectangle x1="1.31318125" y1="0.80441875" x2="1.48081875" y2="0.889" layer="21"/>
<rectangle x1="2.413" y1="0.80441875" x2="2.49681875" y2="0.889" layer="21"/>
<rectangle x1="2.667" y1="0.80441875" x2="2.83718125" y2="0.889" layer="21"/>
<rectangle x1="3.175" y1="0.80441875" x2="3.34518125" y2="0.889" layer="21"/>
<rectangle x1="3.51281875" y1="0.80441875" x2="3.683" y2="0.889" layer="21"/>
<rectangle x1="3.85318125" y1="0.80441875" x2="4.02081875" y2="0.889" layer="21"/>
<rectangle x1="4.52881875" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="5.37718125" y1="0.80441875" x2="5.54481875" y2="0.889" layer="21"/>
<rectangle x1="5.715" y1="0.80441875" x2="5.79881875" y2="0.889" layer="21"/>
<rectangle x1="6.223" y1="0.80441875" x2="6.39318125" y2="0.889" layer="21"/>
<rectangle x1="6.477" y1="0.80441875" x2="6.64718125" y2="0.889" layer="21"/>
<rectangle x1="6.90118125" y1="0.80441875" x2="7.06881875" y2="0.889" layer="21"/>
<rectangle x1="7.239" y1="0.80441875" x2="7.40918125" y2="0.889" layer="21"/>
<rectangle x1="7.747" y1="0.80441875" x2="7.91718125" y2="0.889" layer="21"/>
<rectangle x1="8.17118125" y1="0.80441875" x2="8.255" y2="0.889" layer="21"/>
<rectangle x1="9.18718125" y1="0.80441875" x2="9.271" y2="0.889" layer="21"/>
<rectangle x1="9.44118125" y1="0.80441875" x2="9.60881875" y2="0.889" layer="21"/>
<rectangle x1="9.94918125" y1="0.80441875" x2="10.11681875" y2="0.889" layer="21"/>
<rectangle x1="10.287" y1="0.80441875" x2="10.45718125" y2="0.889" layer="21"/>
<rectangle x1="10.87881875" y1="0.80441875" x2="11.049" y2="0.889" layer="21"/>
<rectangle x1="1.31318125" y1="0.889" x2="1.48081875" y2="0.97358125" layer="21"/>
<rectangle x1="2.413" y1="0.889" x2="2.49681875" y2="0.97358125" layer="21"/>
<rectangle x1="2.667" y1="0.889" x2="3.25881875" y2="0.97358125" layer="21"/>
<rectangle x1="3.51281875" y1="0.889" x2="3.683" y2="0.97358125" layer="21"/>
<rectangle x1="3.76681875" y1="0.889" x2="4.27481875" y2="0.97358125" layer="21"/>
<rectangle x1="4.52881875" y1="0.889" x2="4.61518125" y2="0.97358125" layer="21"/>
<rectangle x1="5.37718125" y1="0.889" x2="5.54481875" y2="0.97358125" layer="21"/>
<rectangle x1="5.715" y1="0.889" x2="6.30681875" y2="0.97358125" layer="21"/>
<rectangle x1="6.56081875" y1="0.889" x2="6.985" y2="0.97358125" layer="21"/>
<rectangle x1="7.32281875" y1="0.889" x2="7.83081875" y2="0.97358125" layer="21"/>
<rectangle x1="8.17118125" y1="0.889" x2="8.255" y2="0.97358125" layer="21"/>
<rectangle x1="9.18718125" y1="0.889" x2="9.271" y2="0.97358125" layer="21"/>
<rectangle x1="9.35481875" y1="0.889" x2="10.033" y2="0.97358125" layer="21"/>
<rectangle x1="10.37081875" y1="0.889" x2="10.96518125" y2="0.97358125" layer="21"/>
<rectangle x1="1.31318125" y1="0.97358125" x2="1.48081875" y2="1.05841875" layer="21"/>
<rectangle x1="2.413" y1="0.97358125" x2="2.49681875" y2="1.05841875" layer="21"/>
<rectangle x1="2.667" y1="0.97358125" x2="2.75081875" y2="1.05841875" layer="21"/>
<rectangle x1="2.921" y1="0.97358125" x2="3.175" y2="1.05841875" layer="21"/>
<rectangle x1="3.76681875" y1="0.97358125" x2="4.27481875" y2="1.05841875" layer="21"/>
<rectangle x1="4.52881875" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="21"/>
<rectangle x1="5.37718125" y1="0.97358125" x2="5.54481875" y2="1.05841875" layer="21"/>
<rectangle x1="5.88518125" y1="0.97358125" x2="6.13918125" y2="1.05841875" layer="21"/>
<rectangle x1="6.64718125" y1="0.97358125" x2="6.90118125" y2="1.05841875" layer="21"/>
<rectangle x1="7.40918125" y1="0.97358125" x2="7.66318125" y2="1.05841875" layer="21"/>
<rectangle x1="8.17118125" y1="0.97358125" x2="8.33881875" y2="1.05841875" layer="21"/>
<rectangle x1="9.10081875" y1="0.97358125" x2="9.271" y2="1.05841875" layer="21"/>
<rectangle x1="9.60881875" y1="0.97358125" x2="9.94918125" y2="1.05841875" layer="21"/>
<rectangle x1="10.541" y1="0.97358125" x2="10.795" y2="1.05841875" layer="21"/>
<rectangle x1="1.31318125" y1="1.05841875" x2="1.48081875" y2="1.143" layer="21"/>
<rectangle x1="2.413" y1="1.05841875" x2="2.49681875" y2="1.143" layer="21"/>
<rectangle x1="3.51281875" y1="1.05841875" x2="3.683" y2="1.143" layer="21"/>
<rectangle x1="3.85318125" y1="1.05841875" x2="4.02081875" y2="1.143" layer="21"/>
<rectangle x1="4.52881875" y1="1.05841875" x2="4.61518125" y2="1.143" layer="21"/>
<rectangle x1="5.29081875" y1="1.05841875" x2="5.461" y2="1.143" layer="21"/>
<rectangle x1="8.255" y1="1.05841875" x2="8.42518125" y2="1.143" layer="21"/>
<rectangle x1="9.017" y1="1.05841875" x2="9.18718125" y2="1.143" layer="21"/>
<rectangle x1="1.31318125" y1="1.143" x2="1.48081875" y2="1.22758125" layer="21"/>
<rectangle x1="2.413" y1="1.143" x2="2.49681875" y2="1.22758125" layer="21"/>
<rectangle x1="3.51281875" y1="1.143" x2="3.683" y2="1.22758125" layer="21"/>
<rectangle x1="3.85318125" y1="1.143" x2="4.02081875" y2="1.22758125" layer="21"/>
<rectangle x1="4.52881875" y1="1.143" x2="4.61518125" y2="1.22758125" layer="21"/>
<rectangle x1="5.29081875" y1="1.143" x2="5.461" y2="1.22758125" layer="21"/>
<rectangle x1="8.33881875" y1="1.143" x2="8.509" y2="1.22758125" layer="21"/>
<rectangle x1="8.93318125" y1="1.143" x2="9.10081875" y2="1.22758125" layer="21"/>
<rectangle x1="1.31318125" y1="1.22758125" x2="1.48081875" y2="1.31241875" layer="21"/>
<rectangle x1="2.413" y1="1.22758125" x2="2.49681875" y2="1.31241875" layer="21"/>
<rectangle x1="3.85318125" y1="1.22758125" x2="4.02081875" y2="1.31241875" layer="21"/>
<rectangle x1="4.52881875" y1="1.22758125" x2="4.61518125" y2="1.31241875" layer="21"/>
<rectangle x1="5.12318125" y1="1.22758125" x2="5.37718125" y2="1.31241875" layer="21"/>
<rectangle x1="8.42518125" y1="1.22758125" x2="9.017" y2="1.31241875" layer="21"/>
<rectangle x1="4.52881875" y1="1.31241875" x2="5.207" y2="1.397" layer="21"/>
<rectangle x1="8.59281875" y1="1.31241875" x2="8.84681875" y2="1.397" layer="21"/>
<rectangle x1="2.24281875" y1="1.905" x2="9.779" y2="1.98958125" layer="21"/>
<rectangle x1="1.82118125" y1="1.98958125" x2="10.287" y2="2.07441875" layer="21"/>
<rectangle x1="1.56718125" y1="2.07441875" x2="10.541" y2="2.159" layer="21"/>
<rectangle x1="1.31318125" y1="2.159" x2="10.71118125" y2="2.24358125" layer="21"/>
<rectangle x1="1.22681875" y1="2.24358125" x2="2.07518125" y2="2.32841875" layer="21"/>
<rectangle x1="5.88518125" y1="2.24358125" x2="10.87881875" y2="2.32841875" layer="21"/>
<rectangle x1="1.05918125" y1="2.32841875" x2="1.73481875" y2="2.413" layer="21"/>
<rectangle x1="5.88518125" y1="2.32841875" x2="10.96518125" y2="2.413" layer="21"/>
<rectangle x1="0.97281875" y1="2.413" x2="1.56718125" y2="2.49758125" layer="21"/>
<rectangle x1="5.79881875" y1="2.413" x2="11.13281875" y2="2.49758125" layer="21"/>
<rectangle x1="0.889" y1="2.49758125" x2="1.397" y2="2.58241875" layer="21"/>
<rectangle x1="5.79881875" y1="2.49758125" x2="11.21918125" y2="2.58241875" layer="21"/>
<rectangle x1="0.71881875" y1="2.58241875" x2="1.22681875" y2="2.667" layer="21"/>
<rectangle x1="5.715" y1="2.58241875" x2="11.303" y2="2.667" layer="21"/>
<rectangle x1="0.635" y1="2.667" x2="1.143" y2="2.75158125" layer="21"/>
<rectangle x1="5.715" y1="2.667" x2="11.38681875" y2="2.75158125" layer="21"/>
<rectangle x1="0.635" y1="2.75158125" x2="1.05918125" y2="2.83641875" layer="21"/>
<rectangle x1="5.63118125" y1="2.75158125" x2="11.47318125" y2="2.83641875" layer="21"/>
<rectangle x1="0.55118125" y1="2.83641875" x2="0.97281875" y2="2.921" layer="21"/>
<rectangle x1="5.63118125" y1="2.83641875" x2="11.557" y2="2.921" layer="21"/>
<rectangle x1="0.46481875" y1="2.921" x2="0.889" y2="3.00558125" layer="21"/>
<rectangle x1="5.63118125" y1="2.921" x2="11.557" y2="3.00558125" layer="21"/>
<rectangle x1="0.381" y1="3.00558125" x2="0.80518125" y2="3.09041875" layer="21"/>
<rectangle x1="5.54481875" y1="3.00558125" x2="11.64081875" y2="3.09041875" layer="21"/>
<rectangle x1="0.381" y1="3.09041875" x2="0.71881875" y2="3.175" layer="21"/>
<rectangle x1="5.54481875" y1="3.09041875" x2="11.72718125" y2="3.175" layer="21"/>
<rectangle x1="0.29718125" y1="3.175" x2="0.71881875" y2="3.25958125" layer="21"/>
<rectangle x1="5.54481875" y1="3.175" x2="11.72718125" y2="3.25958125" layer="21"/>
<rectangle x1="0.29718125" y1="3.25958125" x2="0.635" y2="3.34441875" layer="21"/>
<rectangle x1="5.54481875" y1="3.25958125" x2="11.811" y2="3.34441875" layer="21"/>
<rectangle x1="0.21081875" y1="3.34441875" x2="0.55118125" y2="3.429" layer="21"/>
<rectangle x1="2.159" y1="3.34441875" x2="2.75081875" y2="3.429" layer="21"/>
<rectangle x1="3.51281875" y1="3.34441875" x2="4.27481875" y2="3.429" layer="21"/>
<rectangle x1="5.54481875" y1="3.34441875" x2="6.477" y2="3.429" layer="21"/>
<rectangle x1="7.06881875" y1="3.34441875" x2="8.001" y2="3.429" layer="21"/>
<rectangle x1="8.17118125" y1="3.34441875" x2="9.10081875" y2="3.429" layer="21"/>
<rectangle x1="9.271" y1="3.34441875" x2="9.86281875" y2="3.429" layer="21"/>
<rectangle x1="10.287" y1="3.34441875" x2="11.811" y2="3.429" layer="21"/>
<rectangle x1="0.21081875" y1="3.429" x2="0.55118125" y2="3.51358125" layer="21"/>
<rectangle x1="1.98881875" y1="3.429" x2="2.921" y2="3.51358125" layer="21"/>
<rectangle x1="3.429" y1="3.429" x2="4.445" y2="3.51358125" layer="21"/>
<rectangle x1="5.461" y1="3.429" x2="6.30681875" y2="3.51358125" layer="21"/>
<rectangle x1="7.32281875" y1="3.429" x2="8.001" y2="3.51358125" layer="21"/>
<rectangle x1="8.255" y1="3.429" x2="9.017" y2="3.51358125" layer="21"/>
<rectangle x1="9.271" y1="3.429" x2="9.69518125" y2="3.51358125" layer="21"/>
<rectangle x1="10.541" y1="3.429" x2="11.89481875" y2="3.51358125" layer="21"/>
<rectangle x1="0.127" y1="3.51358125" x2="0.55118125" y2="3.59841875" layer="21"/>
<rectangle x1="1.82118125" y1="3.51358125" x2="3.00481875" y2="3.59841875" layer="21"/>
<rectangle x1="3.429" y1="3.51358125" x2="4.61518125" y2="3.59841875" layer="21"/>
<rectangle x1="5.461" y1="3.51358125" x2="6.13918125" y2="3.59841875" layer="21"/>
<rectangle x1="7.40918125" y1="3.51358125" x2="7.91718125" y2="3.59841875" layer="21"/>
<rectangle x1="8.255" y1="3.51358125" x2="9.017" y2="3.59841875" layer="21"/>
<rectangle x1="9.271" y1="3.51358125" x2="9.60881875" y2="3.59841875" layer="21"/>
<rectangle x1="10.62481875" y1="3.51358125" x2="11.89481875" y2="3.59841875" layer="21"/>
<rectangle x1="0.127" y1="3.59841875" x2="0.46481875" y2="3.683" layer="21"/>
<rectangle x1="1.73481875" y1="3.59841875" x2="3.175" y2="3.683" layer="21"/>
<rectangle x1="3.51281875" y1="3.59841875" x2="4.699" y2="3.683" layer="21"/>
<rectangle x1="5.461" y1="3.59841875" x2="6.05281875" y2="3.683" layer="21"/>
<rectangle x1="7.493" y1="3.59841875" x2="7.91718125" y2="3.683" layer="21"/>
<rectangle x1="8.255" y1="3.59841875" x2="9.017" y2="3.683" layer="21"/>
<rectangle x1="9.271" y1="3.59841875" x2="9.525" y2="3.683" layer="21"/>
<rectangle x1="10.033" y1="3.59841875" x2="10.20318125" y2="3.683" layer="21"/>
<rectangle x1="10.71118125" y1="3.59841875" x2="11.89481875" y2="3.683" layer="21"/>
<rectangle x1="0.127" y1="3.683" x2="0.46481875" y2="3.76758125" layer="21"/>
<rectangle x1="1.651" y1="3.683" x2="2.159" y2="3.76758125" layer="21"/>
<rectangle x1="2.75081875" y1="3.683" x2="3.175" y2="3.76758125" layer="21"/>
<rectangle x1="4.27481875" y1="3.683" x2="4.78281875" y2="3.76758125" layer="21"/>
<rectangle x1="5.461" y1="3.683" x2="5.969" y2="3.76758125" layer="21"/>
<rectangle x1="6.477" y1="3.683" x2="7.06881875" y2="3.76758125" layer="21"/>
<rectangle x1="7.57681875" y1="3.683" x2="7.91718125" y2="3.76758125" layer="21"/>
<rectangle x1="8.255" y1="3.683" x2="9.017" y2="3.76758125" layer="21"/>
<rectangle x1="9.271" y1="3.683" x2="9.525" y2="3.76758125" layer="21"/>
<rectangle x1="9.86281875" y1="3.683" x2="10.37081875" y2="3.76758125" layer="21"/>
<rectangle x1="10.71118125" y1="3.683" x2="11.98118125" y2="3.76758125" layer="21"/>
<rectangle x1="0.127" y1="3.76758125" x2="0.381" y2="3.85241875" layer="21"/>
<rectangle x1="1.56718125" y1="3.76758125" x2="1.98881875" y2="3.85241875" layer="21"/>
<rectangle x1="2.83718125" y1="3.76758125" x2="3.25881875" y2="3.85241875" layer="21"/>
<rectangle x1="4.445" y1="3.76758125" x2="4.86918125" y2="3.85241875" layer="21"/>
<rectangle x1="5.461" y1="3.76758125" x2="5.969" y2="3.85241875" layer="21"/>
<rectangle x1="6.39318125" y1="3.76758125" x2="7.15518125" y2="3.85241875" layer="21"/>
<rectangle x1="7.66318125" y1="3.76758125" x2="7.91718125" y2="3.85241875" layer="21"/>
<rectangle x1="8.255" y1="3.76758125" x2="9.017" y2="3.85241875" layer="21"/>
<rectangle x1="9.271" y1="3.76758125" x2="9.44118125" y2="3.85241875" layer="21"/>
<rectangle x1="9.779" y1="3.76758125" x2="10.541" y2="3.85241875" layer="21"/>
<rectangle x1="10.62481875" y1="3.76758125" x2="11.98118125" y2="3.85241875" layer="21"/>
<rectangle x1="0.04318125" y1="3.85241875" x2="0.381" y2="3.937" layer="21"/>
<rectangle x1="1.56718125" y1="3.85241875" x2="1.905" y2="3.937" layer="21"/>
<rectangle x1="2.921" y1="3.85241875" x2="3.34518125" y2="3.937" layer="21"/>
<rectangle x1="4.52881875" y1="3.85241875" x2="4.86918125" y2="3.937" layer="21"/>
<rectangle x1="5.461" y1="3.85241875" x2="5.88518125" y2="3.937" layer="21"/>
<rectangle x1="6.30681875" y1="3.85241875" x2="7.239" y2="3.937" layer="21"/>
<rectangle x1="7.66318125" y1="3.85241875" x2="7.91718125" y2="3.937" layer="21"/>
<rectangle x1="8.255" y1="3.85241875" x2="9.017" y2="3.937" layer="21"/>
<rectangle x1="9.271" y1="3.85241875" x2="9.44118125" y2="3.937" layer="21"/>
<rectangle x1="9.779" y1="3.85241875" x2="9.86281875" y2="3.937" layer="21"/>
<rectangle x1="10.45718125" y1="3.85241875" x2="11.98118125" y2="3.937" layer="21"/>
<rectangle x1="0.04318125" y1="3.937" x2="0.381" y2="4.02158125" layer="21"/>
<rectangle x1="1.48081875" y1="3.937" x2="1.905" y2="4.02158125" layer="21"/>
<rectangle x1="3.00481875" y1="3.937" x2="3.34518125" y2="4.02158125" layer="21"/>
<rectangle x1="4.52881875" y1="3.937" x2="4.953" y2="4.02158125" layer="21"/>
<rectangle x1="5.461" y1="3.937" x2="5.88518125" y2="4.02158125" layer="21"/>
<rectangle x1="6.223" y1="3.937" x2="7.32281875" y2="4.02158125" layer="21"/>
<rectangle x1="7.747" y1="3.937" x2="7.91718125" y2="4.02158125" layer="21"/>
<rectangle x1="8.255" y1="3.937" x2="9.017" y2="4.02158125" layer="21"/>
<rectangle x1="9.271" y1="3.937" x2="9.44118125" y2="4.02158125" layer="21"/>
<rectangle x1="10.795" y1="3.937" x2="11.98118125" y2="4.02158125" layer="21"/>
<rectangle x1="0.04318125" y1="4.02158125" x2="0.381" y2="4.10641875" layer="21"/>
<rectangle x1="1.48081875" y1="4.02158125" x2="1.82118125" y2="4.10641875" layer="21"/>
<rectangle x1="3.00481875" y1="4.02158125" x2="3.34518125" y2="4.10641875" layer="21"/>
<rectangle x1="4.61518125" y1="4.02158125" x2="4.953" y2="4.10641875" layer="21"/>
<rectangle x1="5.461" y1="4.02158125" x2="5.79881875" y2="4.10641875" layer="21"/>
<rectangle x1="6.13918125" y1="4.02158125" x2="7.40918125" y2="4.10641875" layer="21"/>
<rectangle x1="7.747" y1="4.02158125" x2="7.91718125" y2="4.10641875" layer="21"/>
<rectangle x1="8.255" y1="4.02158125" x2="9.017" y2="4.10641875" layer="21"/>
<rectangle x1="9.271" y1="4.02158125" x2="9.44118125" y2="4.10641875" layer="21"/>
<rectangle x1="10.795" y1="4.02158125" x2="11.98118125" y2="4.10641875" layer="21"/>
<rectangle x1="0.04318125" y1="4.10641875" x2="0.381" y2="4.191" layer="21"/>
<rectangle x1="1.48081875" y1="4.10641875" x2="1.82118125" y2="4.191" layer="21"/>
<rectangle x1="3.09118125" y1="4.10641875" x2="3.429" y2="4.191" layer="21"/>
<rectangle x1="4.61518125" y1="4.10641875" x2="4.953" y2="4.191" layer="21"/>
<rectangle x1="5.37718125" y1="4.10641875" x2="5.79881875" y2="4.191" layer="21"/>
<rectangle x1="6.13918125" y1="4.10641875" x2="7.40918125" y2="4.191" layer="21"/>
<rectangle x1="7.747" y1="4.10641875" x2="7.91718125" y2="4.191" layer="21"/>
<rectangle x1="8.255" y1="4.10641875" x2="9.017" y2="4.191" layer="21"/>
<rectangle x1="9.271" y1="4.10641875" x2="9.44118125" y2="4.191" layer="21"/>
<rectangle x1="10.795" y1="4.10641875" x2="11.98118125" y2="4.191" layer="21"/>
<rectangle x1="0.04318125" y1="4.191" x2="0.381" y2="4.27558125" layer="21"/>
<rectangle x1="1.48081875" y1="4.191" x2="1.73481875" y2="4.27558125" layer="21"/>
<rectangle x1="3.09118125" y1="4.191" x2="3.429" y2="4.27558125" layer="21"/>
<rectangle x1="4.699" y1="4.191" x2="4.953" y2="4.27558125" layer="21"/>
<rectangle x1="5.37718125" y1="4.191" x2="5.79881875" y2="4.27558125" layer="21"/>
<rectangle x1="6.13918125" y1="4.191" x2="7.40918125" y2="4.27558125" layer="21"/>
<rectangle x1="7.747" y1="4.191" x2="7.91718125" y2="4.27558125" layer="21"/>
<rectangle x1="8.255" y1="4.191" x2="8.93318125" y2="4.27558125" layer="21"/>
<rectangle x1="9.271" y1="4.191" x2="9.44118125" y2="4.27558125" layer="21"/>
<rectangle x1="9.779" y1="4.191" x2="10.45718125" y2="4.27558125" layer="21"/>
<rectangle x1="10.71118125" y1="4.191" x2="11.98118125" y2="4.27558125" layer="21"/>
<rectangle x1="0.04318125" y1="4.27558125" x2="0.381" y2="4.36041875" layer="21"/>
<rectangle x1="1.48081875" y1="4.27558125" x2="1.73481875" y2="4.36041875" layer="21"/>
<rectangle x1="3.09118125" y1="4.27558125" x2="3.429" y2="4.36041875" layer="21"/>
<rectangle x1="4.699" y1="4.27558125" x2="4.953" y2="4.36041875" layer="21"/>
<rectangle x1="5.37718125" y1="4.27558125" x2="5.79881875" y2="4.36041875" layer="21"/>
<rectangle x1="6.13918125" y1="4.27558125" x2="7.40918125" y2="4.36041875" layer="21"/>
<rectangle x1="7.747" y1="4.27558125" x2="7.91718125" y2="4.36041875" layer="21"/>
<rectangle x1="8.33881875" y1="4.27558125" x2="8.93318125" y2="4.36041875" layer="21"/>
<rectangle x1="9.271" y1="4.27558125" x2="9.525" y2="4.36041875" layer="21"/>
<rectangle x1="9.86281875" y1="4.27558125" x2="10.37081875" y2="4.36041875" layer="21"/>
<rectangle x1="10.71118125" y1="4.27558125" x2="11.98118125" y2="4.36041875" layer="21"/>
<rectangle x1="0.04318125" y1="4.36041875" x2="0.381" y2="4.445" layer="21"/>
<rectangle x1="1.48081875" y1="4.36041875" x2="1.73481875" y2="4.445" layer="21"/>
<rectangle x1="3.09118125" y1="4.36041875" x2="3.429" y2="4.445" layer="21"/>
<rectangle x1="4.699" y1="4.36041875" x2="4.953" y2="4.445" layer="21"/>
<rectangle x1="5.37718125" y1="4.36041875" x2="5.79881875" y2="4.445" layer="21"/>
<rectangle x1="6.13918125" y1="4.36041875" x2="7.40918125" y2="4.445" layer="21"/>
<rectangle x1="7.747" y1="4.36041875" x2="7.91718125" y2="4.445" layer="21"/>
<rectangle x1="8.509" y1="4.36041875" x2="8.763" y2="4.445" layer="21"/>
<rectangle x1="9.18718125" y1="4.36041875" x2="9.525" y2="4.445" layer="21"/>
<rectangle x1="9.94918125" y1="4.36041875" x2="10.20318125" y2="4.445" layer="21"/>
<rectangle x1="10.62481875" y1="4.36041875" x2="11.98118125" y2="4.445" layer="21"/>
<rectangle x1="0.04318125" y1="4.445" x2="0.381" y2="4.52958125" layer="21"/>
<rectangle x1="1.48081875" y1="4.445" x2="1.73481875" y2="4.52958125" layer="21"/>
<rectangle x1="3.09118125" y1="4.445" x2="3.429" y2="4.52958125" layer="21"/>
<rectangle x1="4.61518125" y1="4.445" x2="4.953" y2="4.52958125" layer="21"/>
<rectangle x1="5.37718125" y1="4.445" x2="5.79881875" y2="4.52958125" layer="21"/>
<rectangle x1="6.13918125" y1="4.445" x2="7.40918125" y2="4.52958125" layer="21"/>
<rectangle x1="7.747" y1="4.445" x2="7.91718125" y2="4.52958125" layer="21"/>
<rectangle x1="9.10081875" y1="4.445" x2="9.60881875" y2="4.52958125" layer="21"/>
<rectangle x1="10.541" y1="4.445" x2="11.98118125" y2="4.52958125" layer="21"/>
<rectangle x1="0.04318125" y1="4.52958125" x2="0.381" y2="4.61441875" layer="21"/>
<rectangle x1="1.48081875" y1="4.52958125" x2="1.73481875" y2="4.61441875" layer="21"/>
<rectangle x1="3.09118125" y1="4.52958125" x2="3.429" y2="4.61441875" layer="21"/>
<rectangle x1="4.61518125" y1="4.52958125" x2="4.953" y2="4.61441875" layer="21"/>
<rectangle x1="5.461" y1="4.52958125" x2="5.79881875" y2="4.61441875" layer="21"/>
<rectangle x1="6.13918125" y1="4.52958125" x2="7.40918125" y2="4.61441875" layer="21"/>
<rectangle x1="7.747" y1="4.52958125" x2="8.001" y2="4.61441875" layer="21"/>
<rectangle x1="9.017" y1="4.52958125" x2="9.69518125" y2="4.61441875" layer="21"/>
<rectangle x1="10.45718125" y1="4.52958125" x2="11.98118125" y2="4.61441875" layer="21"/>
<rectangle x1="0.04318125" y1="4.61441875" x2="0.381" y2="4.699" layer="21"/>
<rectangle x1="1.48081875" y1="4.61441875" x2="1.73481875" y2="4.699" layer="21"/>
<rectangle x1="3.09118125" y1="4.61441875" x2="3.429" y2="4.699" layer="21"/>
<rectangle x1="4.52881875" y1="4.61441875" x2="4.953" y2="4.699" layer="21"/>
<rectangle x1="5.461" y1="4.61441875" x2="5.79881875" y2="4.699" layer="21"/>
<rectangle x1="6.223" y1="4.61441875" x2="7.32281875" y2="4.699" layer="21"/>
<rectangle x1="7.747" y1="4.61441875" x2="8.001" y2="4.699" layer="21"/>
<rectangle x1="8.17118125" y1="4.61441875" x2="8.42518125" y2="4.699" layer="21"/>
<rectangle x1="8.84681875" y1="4.61441875" x2="9.86281875" y2="4.699" layer="21"/>
<rectangle x1="10.287" y1="4.61441875" x2="11.98118125" y2="4.699" layer="21"/>
<rectangle x1="0.04318125" y1="4.699" x2="0.381" y2="4.78358125" layer="21"/>
<rectangle x1="1.48081875" y1="4.699" x2="1.73481875" y2="4.78358125" layer="21"/>
<rectangle x1="3.09118125" y1="4.699" x2="3.429" y2="4.78358125" layer="21"/>
<rectangle x1="4.52881875" y1="4.699" x2="4.86918125" y2="4.78358125" layer="21"/>
<rectangle x1="5.461" y1="4.699" x2="5.88518125" y2="4.78358125" layer="21"/>
<rectangle x1="6.30681875" y1="4.699" x2="7.32281875" y2="4.78358125" layer="21"/>
<rectangle x1="7.66318125" y1="4.699" x2="11.98118125" y2="4.78358125" layer="21"/>
<rectangle x1="0.127" y1="4.78358125" x2="0.381" y2="4.86841875" layer="21"/>
<rectangle x1="1.48081875" y1="4.78358125" x2="1.73481875" y2="4.86841875" layer="21"/>
<rectangle x1="3.09118125" y1="4.78358125" x2="3.429" y2="4.86841875" layer="21"/>
<rectangle x1="4.445" y1="4.78358125" x2="4.86918125" y2="4.86841875" layer="21"/>
<rectangle x1="5.461" y1="4.78358125" x2="5.88518125" y2="4.86841875" layer="21"/>
<rectangle x1="6.39318125" y1="4.78358125" x2="7.239" y2="4.86841875" layer="21"/>
<rectangle x1="7.66318125" y1="4.78358125" x2="11.98118125" y2="4.86841875" layer="21"/>
<rectangle x1="0.127" y1="4.86841875" x2="0.46481875" y2="4.953" layer="21"/>
<rectangle x1="1.48081875" y1="4.86841875" x2="1.73481875" y2="4.953" layer="21"/>
<rectangle x1="3.09118125" y1="4.86841875" x2="3.429" y2="4.953" layer="21"/>
<rectangle x1="4.27481875" y1="4.86841875" x2="4.78281875" y2="4.953" layer="21"/>
<rectangle x1="5.461" y1="4.86841875" x2="5.969" y2="4.953" layer="21"/>
<rectangle x1="6.477" y1="4.86841875" x2="7.06881875" y2="4.953" layer="21"/>
<rectangle x1="7.57681875" y1="4.86841875" x2="11.98118125" y2="4.953" layer="21"/>
<rectangle x1="0.127" y1="4.953" x2="0.46481875" y2="5.03758125" layer="21"/>
<rectangle x1="1.48081875" y1="4.953" x2="1.73481875" y2="5.03758125" layer="21"/>
<rectangle x1="3.09118125" y1="4.953" x2="4.699" y2="5.03758125" layer="21"/>
<rectangle x1="5.461" y1="4.953" x2="6.05281875" y2="5.03758125" layer="21"/>
<rectangle x1="7.493" y1="4.953" x2="11.89481875" y2="5.03758125" layer="21"/>
<rectangle x1="0.127" y1="5.03758125" x2="0.46481875" y2="5.12241875" layer="21"/>
<rectangle x1="1.48081875" y1="5.03758125" x2="1.73481875" y2="5.12241875" layer="21"/>
<rectangle x1="3.09118125" y1="5.03758125" x2="4.61518125" y2="5.12241875" layer="21"/>
<rectangle x1="5.461" y1="5.03758125" x2="6.13918125" y2="5.12241875" layer="21"/>
<rectangle x1="7.40918125" y1="5.03758125" x2="11.89481875" y2="5.12241875" layer="21"/>
<rectangle x1="0.21081875" y1="5.12241875" x2="0.55118125" y2="5.207" layer="21"/>
<rectangle x1="1.48081875" y1="5.12241875" x2="1.73481875" y2="5.207" layer="21"/>
<rectangle x1="3.09118125" y1="5.12241875" x2="4.52881875" y2="5.207" layer="21"/>
<rectangle x1="5.461" y1="5.12241875" x2="6.223" y2="5.207" layer="21"/>
<rectangle x1="7.32281875" y1="5.12241875" x2="11.89481875" y2="5.207" layer="21"/>
<rectangle x1="0.21081875" y1="5.207" x2="0.55118125" y2="5.29158125" layer="21"/>
<rectangle x1="1.48081875" y1="5.207" x2="1.73481875" y2="5.29158125" layer="21"/>
<rectangle x1="3.175" y1="5.207" x2="4.36118125" y2="5.29158125" layer="21"/>
<rectangle x1="5.461" y1="5.207" x2="6.39318125" y2="5.29158125" layer="21"/>
<rectangle x1="7.15518125" y1="5.207" x2="11.811" y2="5.29158125" layer="21"/>
<rectangle x1="0.29718125" y1="5.29158125" x2="0.635" y2="5.37641875" layer="21"/>
<rectangle x1="5.54481875" y1="5.29158125" x2="11.811" y2="5.37641875" layer="21"/>
<rectangle x1="0.29718125" y1="5.37641875" x2="0.71881875" y2="5.461" layer="21"/>
<rectangle x1="5.54481875" y1="5.37641875" x2="11.72718125" y2="5.461" layer="21"/>
<rectangle x1="0.381" y1="5.461" x2="0.71881875" y2="5.54558125" layer="21"/>
<rectangle x1="5.54481875" y1="5.461" x2="11.72718125" y2="5.54558125" layer="21"/>
<rectangle x1="0.381" y1="5.54558125" x2="0.80518125" y2="5.63041875" layer="21"/>
<rectangle x1="5.54481875" y1="5.54558125" x2="11.64081875" y2="5.63041875" layer="21"/>
<rectangle x1="0.46481875" y1="5.63041875" x2="0.889" y2="5.715" layer="21"/>
<rectangle x1="5.63118125" y1="5.63041875" x2="11.64081875" y2="5.715" layer="21"/>
<rectangle x1="0.55118125" y1="5.715" x2="0.97281875" y2="5.79958125" layer="21"/>
<rectangle x1="5.63118125" y1="5.715" x2="11.557" y2="5.79958125" layer="21"/>
<rectangle x1="0.55118125" y1="5.79958125" x2="1.05918125" y2="5.88441875" layer="21"/>
<rectangle x1="5.63118125" y1="5.79958125" x2="11.47318125" y2="5.88441875" layer="21"/>
<rectangle x1="0.635" y1="5.88441875" x2="1.143" y2="5.969" layer="21"/>
<rectangle x1="5.715" y1="5.88441875" x2="11.38681875" y2="5.969" layer="21"/>
<rectangle x1="0.71881875" y1="5.969" x2="1.22681875" y2="6.05358125" layer="21"/>
<rectangle x1="5.715" y1="5.969" x2="11.303" y2="6.05358125" layer="21"/>
<rectangle x1="0.80518125" y1="6.05358125" x2="1.397" y2="6.13841875" layer="21"/>
<rectangle x1="5.79881875" y1="6.05358125" x2="11.21918125" y2="6.13841875" layer="21"/>
<rectangle x1="0.889" y1="6.13841875" x2="1.56718125" y2="6.223" layer="21"/>
<rectangle x1="5.79881875" y1="6.13841875" x2="11.13281875" y2="6.223" layer="21"/>
<rectangle x1="1.05918125" y1="6.223" x2="1.73481875" y2="6.30758125" layer="21"/>
<rectangle x1="5.88518125" y1="6.223" x2="11.049" y2="6.30758125" layer="21"/>
<rectangle x1="1.143" y1="6.30758125" x2="1.98881875" y2="6.39241875" layer="21"/>
<rectangle x1="5.88518125" y1="6.30758125" x2="10.87881875" y2="6.39241875" layer="21"/>
<rectangle x1="1.31318125" y1="6.39241875" x2="10.71118125" y2="6.477" layer="21"/>
<rectangle x1="1.48081875" y1="6.477" x2="10.541" y2="6.56158125" layer="21"/>
<rectangle x1="1.73481875" y1="6.56158125" x2="10.37081875" y2="6.64641875" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="UDO-LOGO-15MM" urn="urn:adsk.eagle:package:6649256/2" type="box">
<packageinstances>
<packageinstance name="UDO-LOGO-15MM"/>
</packageinstances>
</package3d>
<package3d name="UDO-LOGO-10MM" urn="urn:adsk.eagle:package:6649258/2" type="box">
<packageinstances>
<packageinstance name="UDO-LOGO-10MM"/>
</packageinstances>
</package3d>
<package3d name="UDO-LOGO-12MM" urn="urn:adsk.eagle:package:6649257/3" type="box">
<packageinstances>
<packageinstance name="UDO-LOGO-12MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="UDO-LOGO-20MM">
<rectangle x1="3.00481875" y1="0.29641875" x2="3.429" y2="0.381" layer="94"/>
<rectangle x1="14.351" y1="0.29641875" x2="14.68881875" y2="0.381" layer="94"/>
<rectangle x1="2.75081875" y1="0.381" x2="3.683" y2="0.46558125" layer="94"/>
<rectangle x1="4.445" y1="0.381" x2="4.52881875" y2="0.46558125" layer="94"/>
<rectangle x1="5.54481875" y1="0.381" x2="5.715" y2="0.46558125" layer="94"/>
<rectangle x1="5.969" y1="0.381" x2="6.05281875" y2="0.46558125" layer="94"/>
<rectangle x1="6.985" y1="0.381" x2="7.15518125" y2="0.46558125" layer="94"/>
<rectangle x1="7.57681875" y1="0.381" x2="8.42518125" y2="0.46558125" layer="94"/>
<rectangle x1="9.94918125" y1="0.381" x2="10.20318125" y2="0.46558125" layer="94"/>
<rectangle x1="11.21918125" y1="0.381" x2="11.38681875" y2="0.46558125" layer="94"/>
<rectangle x1="12.40281875" y1="0.381" x2="12.74318125" y2="0.46558125" layer="94"/>
<rectangle x1="14.097" y1="0.381" x2="14.94281875" y2="0.46558125" layer="94"/>
<rectangle x1="15.70481875" y1="0.381" x2="15.79118125" y2="0.46558125" layer="94"/>
<rectangle x1="16.80718125" y1="0.381" x2="16.891" y2="0.46558125" layer="94"/>
<rectangle x1="17.653" y1="0.381" x2="17.907" y2="0.46558125" layer="94"/>
<rectangle x1="2.667" y1="0.46558125" x2="3.85318125" y2="0.55041875" layer="94"/>
<rectangle x1="4.36118125" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="94"/>
<rectangle x1="5.54481875" y1="0.46558125" x2="5.715" y2="0.55041875" layer="94"/>
<rectangle x1="5.88518125" y1="0.46558125" x2="6.13918125" y2="0.55041875" layer="94"/>
<rectangle x1="6.731" y1="0.46558125" x2="7.239" y2="0.55041875" layer="94"/>
<rectangle x1="7.493" y1="0.46558125" x2="8.67918125" y2="0.55041875" layer="94"/>
<rectangle x1="9.69518125" y1="0.46558125" x2="10.37081875" y2="0.55041875" layer="94"/>
<rectangle x1="11.049" y1="0.46558125" x2="11.64081875" y2="0.55041875" layer="94"/>
<rectangle x1="12.23518125" y1="0.46558125" x2="12.91081875" y2="0.55041875" layer="94"/>
<rectangle x1="13.92681875" y1="0.46558125" x2="15.113" y2="0.55041875" layer="94"/>
<rectangle x1="15.621" y1="0.46558125" x2="15.875" y2="0.55041875" layer="94"/>
<rectangle x1="16.72081875" y1="0.46558125" x2="16.97481875" y2="0.55041875" layer="94"/>
<rectangle x1="17.399" y1="0.46558125" x2="18.07718125" y2="0.55041875" layer="94"/>
<rectangle x1="2.58318125" y1="0.55041875" x2="3.00481875" y2="0.635" layer="94"/>
<rectangle x1="3.51281875" y1="0.55041875" x2="3.937" y2="0.635" layer="94"/>
<rectangle x1="4.36118125" y1="0.55041875" x2="4.61518125" y2="0.635" layer="94"/>
<rectangle x1="5.54481875" y1="0.55041875" x2="5.715" y2="0.635" layer="94"/>
<rectangle x1="5.88518125" y1="0.55041875" x2="6.13918125" y2="0.635" layer="94"/>
<rectangle x1="6.64718125" y1="0.55041875" x2="7.239" y2="0.635" layer="94"/>
<rectangle x1="7.493" y1="0.55041875" x2="8.84681875" y2="0.635" layer="94"/>
<rectangle x1="9.60881875" y1="0.55041875" x2="10.45718125" y2="0.635" layer="94"/>
<rectangle x1="10.96518125" y1="0.55041875" x2="11.72718125" y2="0.635" layer="94"/>
<rectangle x1="12.14881875" y1="0.55041875" x2="12.99718125" y2="0.635" layer="94"/>
<rectangle x1="13.843" y1="0.55041875" x2="14.26718125" y2="0.635" layer="94"/>
<rectangle x1="14.77518125" y1="0.55041875" x2="15.19681875" y2="0.635" layer="94"/>
<rectangle x1="15.621" y1="0.55041875" x2="15.875" y2="0.635" layer="94"/>
<rectangle x1="16.72081875" y1="0.55041875" x2="16.97481875" y2="0.635" layer="94"/>
<rectangle x1="17.31518125" y1="0.55041875" x2="18.24481875" y2="0.635" layer="94"/>
<rectangle x1="2.49681875" y1="0.635" x2="2.83718125" y2="0.71958125" layer="94"/>
<rectangle x1="3.683" y1="0.635" x2="4.02081875" y2="0.71958125" layer="94"/>
<rectangle x1="4.36118125" y1="0.635" x2="4.61518125" y2="0.71958125" layer="94"/>
<rectangle x1="5.54481875" y1="0.635" x2="5.715" y2="0.71958125" layer="94"/>
<rectangle x1="5.88518125" y1="0.635" x2="6.13918125" y2="0.71958125" layer="94"/>
<rectangle x1="6.56081875" y1="0.635" x2="6.90118125" y2="0.71958125" layer="94"/>
<rectangle x1="7.493" y1="0.635" x2="7.747" y2="0.71958125" layer="94"/>
<rectangle x1="8.509" y1="0.635" x2="8.93318125" y2="0.71958125" layer="94"/>
<rectangle x1="9.525" y1="0.635" x2="9.86281875" y2="0.71958125" layer="94"/>
<rectangle x1="10.20318125" y1="0.635" x2="10.541" y2="0.71958125" layer="94"/>
<rectangle x1="10.87881875" y1="0.635" x2="11.13281875" y2="0.71958125" layer="94"/>
<rectangle x1="11.47318125" y1="0.635" x2="11.811" y2="0.71958125" layer="94"/>
<rectangle x1="12.065" y1="0.635" x2="12.40281875" y2="0.71958125" layer="94"/>
<rectangle x1="12.74318125" y1="0.635" x2="13.081" y2="0.71958125" layer="94"/>
<rectangle x1="13.75918125" y1="0.635" x2="14.097" y2="0.71958125" layer="94"/>
<rectangle x1="14.94281875" y1="0.635" x2="15.28318125" y2="0.71958125" layer="94"/>
<rectangle x1="15.621" y1="0.635" x2="15.875" y2="0.71958125" layer="94"/>
<rectangle x1="16.72081875" y1="0.635" x2="16.97481875" y2="0.71958125" layer="94"/>
<rectangle x1="17.22881875" y1="0.635" x2="17.56918125" y2="0.71958125" layer="94"/>
<rectangle x1="17.907" y1="0.635" x2="18.33118125" y2="0.71958125" layer="94"/>
<rectangle x1="2.413" y1="0.71958125" x2="2.667" y2="0.80441875" layer="94"/>
<rectangle x1="3.76681875" y1="0.71958125" x2="4.02081875" y2="0.80441875" layer="94"/>
<rectangle x1="4.36118125" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="94"/>
<rectangle x1="5.54481875" y1="0.71958125" x2="5.715" y2="0.80441875" layer="94"/>
<rectangle x1="5.88518125" y1="0.71958125" x2="6.13918125" y2="0.80441875" layer="94"/>
<rectangle x1="6.56081875" y1="0.71958125" x2="6.81481875" y2="0.80441875" layer="94"/>
<rectangle x1="7.493" y1="0.71958125" x2="7.747" y2="0.80441875" layer="94"/>
<rectangle x1="8.67918125" y1="0.71958125" x2="9.017" y2="0.80441875" layer="94"/>
<rectangle x1="9.44118125" y1="0.71958125" x2="9.69518125" y2="0.80441875" layer="94"/>
<rectangle x1="10.37081875" y1="0.71958125" x2="10.62481875" y2="0.80441875" layer="94"/>
<rectangle x1="10.87881875" y1="0.71958125" x2="11.049" y2="0.80441875" layer="94"/>
<rectangle x1="11.557" y1="0.71958125" x2="11.811" y2="0.80441875" layer="94"/>
<rectangle x1="11.98118125" y1="0.71958125" x2="12.23518125" y2="0.80441875" layer="94"/>
<rectangle x1="12.91081875" y1="0.71958125" x2="13.16481875" y2="0.80441875" layer="94"/>
<rectangle x1="13.67281875" y1="0.71958125" x2="14.01318125" y2="0.80441875" layer="94"/>
<rectangle x1="15.02918125" y1="0.71958125" x2="15.367" y2="0.80441875" layer="94"/>
<rectangle x1="15.621" y1="0.71958125" x2="15.875" y2="0.80441875" layer="94"/>
<rectangle x1="16.72081875" y1="0.71958125" x2="16.97481875" y2="0.80441875" layer="94"/>
<rectangle x1="17.145" y1="0.71958125" x2="17.399" y2="0.80441875" layer="94"/>
<rectangle x1="18.07718125" y1="0.71958125" x2="18.33118125" y2="0.80441875" layer="94"/>
<rectangle x1="2.32918125" y1="0.80441875" x2="2.58318125" y2="0.889" layer="94"/>
<rectangle x1="3.85318125" y1="0.80441875" x2="4.10718125" y2="0.889" layer="94"/>
<rectangle x1="4.36118125" y1="0.80441875" x2="4.61518125" y2="0.889" layer="94"/>
<rectangle x1="5.54481875" y1="0.80441875" x2="5.715" y2="0.889" layer="94"/>
<rectangle x1="5.88518125" y1="0.80441875" x2="6.13918125" y2="0.889" layer="94"/>
<rectangle x1="6.477" y1="0.80441875" x2="6.731" y2="0.889" layer="94"/>
<rectangle x1="7.493" y1="0.80441875" x2="7.747" y2="0.889" layer="94"/>
<rectangle x1="8.763" y1="0.80441875" x2="9.10081875" y2="0.889" layer="94"/>
<rectangle x1="9.44118125" y1="0.80441875" x2="9.69518125" y2="0.889" layer="94"/>
<rectangle x1="10.45718125" y1="0.80441875" x2="10.71118125" y2="0.889" layer="94"/>
<rectangle x1="11.557" y1="0.80441875" x2="11.811" y2="0.889" layer="94"/>
<rectangle x1="11.98118125" y1="0.80441875" x2="12.14881875" y2="0.889" layer="94"/>
<rectangle x1="12.99718125" y1="0.80441875" x2="13.16481875" y2="0.889" layer="94"/>
<rectangle x1="13.67281875" y1="0.80441875" x2="13.92681875" y2="0.889" layer="94"/>
<rectangle x1="15.113" y1="0.80441875" x2="15.367" y2="0.889" layer="94"/>
<rectangle x1="15.621" y1="0.80441875" x2="15.875" y2="0.889" layer="94"/>
<rectangle x1="16.72081875" y1="0.80441875" x2="16.97481875" y2="0.889" layer="94"/>
<rectangle x1="17.145" y1="0.80441875" x2="17.399" y2="0.889" layer="94"/>
<rectangle x1="18.161" y1="0.80441875" x2="18.33118125" y2="0.889" layer="94"/>
<rectangle x1="2.32918125" y1="0.889" x2="2.58318125" y2="0.97358125" layer="94"/>
<rectangle x1="3.937" y1="0.889" x2="4.191" y2="0.97358125" layer="94"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.61518125" y2="0.97358125" layer="94"/>
<rectangle x1="5.54481875" y1="0.889" x2="5.715" y2="0.97358125" layer="94"/>
<rectangle x1="5.88518125" y1="0.889" x2="6.13918125" y2="0.97358125" layer="94"/>
<rectangle x1="6.477" y1="0.889" x2="6.731" y2="0.97358125" layer="94"/>
<rectangle x1="7.493" y1="0.889" x2="7.747" y2="0.97358125" layer="94"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.10081875" y2="0.97358125" layer="94"/>
<rectangle x1="9.35481875" y1="0.889" x2="9.60881875" y2="0.97358125" layer="94"/>
<rectangle x1="10.45718125" y1="0.889" x2="10.71118125" y2="0.97358125" layer="94"/>
<rectangle x1="11.47318125" y1="0.889" x2="11.72718125" y2="0.97358125" layer="94"/>
<rectangle x1="11.89481875" y1="0.889" x2="12.14881875" y2="0.97358125" layer="94"/>
<rectangle x1="13.589" y1="0.889" x2="13.843" y2="0.97358125" layer="94"/>
<rectangle x1="15.19681875" y1="0.889" x2="15.45081875" y2="0.97358125" layer="94"/>
<rectangle x1="15.621" y1="0.889" x2="15.875" y2="0.97358125" layer="94"/>
<rectangle x1="16.72081875" y1="0.889" x2="16.97481875" y2="0.97358125" layer="94"/>
<rectangle x1="17.06118125" y1="0.889" x2="17.31518125" y2="0.97358125" layer="94"/>
<rectangle x1="2.32918125" y1="0.97358125" x2="2.49681875" y2="1.05841875" layer="94"/>
<rectangle x1="3.937" y1="0.97358125" x2="4.191" y2="1.05841875" layer="94"/>
<rectangle x1="4.36118125" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="94"/>
<rectangle x1="5.54481875" y1="0.97358125" x2="5.715" y2="1.05841875" layer="94"/>
<rectangle x1="5.88518125" y1="0.97358125" x2="6.13918125" y2="1.05841875" layer="94"/>
<rectangle x1="6.477" y1="0.97358125" x2="6.64718125" y2="1.05841875" layer="94"/>
<rectangle x1="7.493" y1="0.97358125" x2="7.747" y2="1.05841875" layer="94"/>
<rectangle x1="8.93318125" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="94"/>
<rectangle x1="9.35481875" y1="0.97358125" x2="9.60881875" y2="1.05841875" layer="94"/>
<rectangle x1="10.45718125" y1="0.97358125" x2="10.71118125" y2="1.05841875" layer="94"/>
<rectangle x1="11.21918125" y1="0.97358125" x2="11.72718125" y2="1.05841875" layer="94"/>
<rectangle x1="11.89481875" y1="0.97358125" x2="13.25118125" y2="1.05841875" layer="94"/>
<rectangle x1="13.589" y1="0.97358125" x2="13.843" y2="1.05841875" layer="94"/>
<rectangle x1="15.19681875" y1="0.97358125" x2="15.45081875" y2="1.05841875" layer="94"/>
<rectangle x1="15.621" y1="0.97358125" x2="15.875" y2="1.05841875" layer="94"/>
<rectangle x1="16.72081875" y1="0.97358125" x2="16.97481875" y2="1.05841875" layer="94"/>
<rectangle x1="17.06118125" y1="0.97358125" x2="18.415" y2="1.05841875" layer="94"/>
<rectangle x1="2.24281875" y1="1.05841875" x2="2.49681875" y2="1.143" layer="94"/>
<rectangle x1="3.937" y1="1.05841875" x2="4.191" y2="1.143" layer="94"/>
<rectangle x1="4.36118125" y1="1.05841875" x2="4.61518125" y2="1.143" layer="94"/>
<rectangle x1="5.54481875" y1="1.05841875" x2="5.715" y2="1.143" layer="94"/>
<rectangle x1="5.88518125" y1="1.05841875" x2="6.13918125" y2="1.143" layer="94"/>
<rectangle x1="6.477" y1="1.05841875" x2="6.64718125" y2="1.143" layer="94"/>
<rectangle x1="7.493" y1="1.05841875" x2="7.747" y2="1.143" layer="94"/>
<rectangle x1="8.93318125" y1="1.05841875" x2="9.18718125" y2="1.143" layer="94"/>
<rectangle x1="9.35481875" y1="1.05841875" x2="9.60881875" y2="1.143" layer="94"/>
<rectangle x1="10.45718125" y1="1.05841875" x2="10.71118125" y2="1.143" layer="94"/>
<rectangle x1="11.049" y1="1.05841875" x2="11.557" y2="1.143" layer="94"/>
<rectangle x1="11.89481875" y1="1.05841875" x2="13.25118125" y2="1.143" layer="94"/>
<rectangle x1="13.589" y1="1.05841875" x2="13.75918125" y2="1.143" layer="94"/>
<rectangle x1="15.28318125" y1="1.05841875" x2="15.53718125" y2="1.143" layer="94"/>
<rectangle x1="15.621" y1="1.05841875" x2="15.875" y2="1.143" layer="94"/>
<rectangle x1="16.72081875" y1="1.05841875" x2="16.97481875" y2="1.143" layer="94"/>
<rectangle x1="17.06118125" y1="1.05841875" x2="18.415" y2="1.143" layer="94"/>
<rectangle x1="2.24281875" y1="1.143" x2="2.49681875" y2="1.22758125" layer="94"/>
<rectangle x1="4.02081875" y1="1.143" x2="4.191" y2="1.22758125" layer="94"/>
<rectangle x1="4.36118125" y1="1.143" x2="4.61518125" y2="1.22758125" layer="94"/>
<rectangle x1="5.54481875" y1="1.143" x2="5.715" y2="1.22758125" layer="94"/>
<rectangle x1="5.88518125" y1="1.143" x2="6.13918125" y2="1.22758125" layer="94"/>
<rectangle x1="6.477" y1="1.143" x2="6.64718125" y2="1.22758125" layer="94"/>
<rectangle x1="7.493" y1="1.143" x2="7.747" y2="1.22758125" layer="94"/>
<rectangle x1="9.017" y1="1.143" x2="9.271" y2="1.22758125" layer="94"/>
<rectangle x1="9.35481875" y1="1.143" x2="9.60881875" y2="1.22758125" layer="94"/>
<rectangle x1="10.45718125" y1="1.143" x2="10.71118125" y2="1.22758125" layer="94"/>
<rectangle x1="10.87881875" y1="1.143" x2="11.38681875" y2="1.22758125" layer="94"/>
<rectangle x1="11.89481875" y1="1.143" x2="13.25118125" y2="1.22758125" layer="94"/>
<rectangle x1="13.50518125" y1="1.143" x2="13.75918125" y2="1.22758125" layer="94"/>
<rectangle x1="15.28318125" y1="1.143" x2="15.53718125" y2="1.22758125" layer="94"/>
<rectangle x1="15.621" y1="1.143" x2="15.875" y2="1.22758125" layer="94"/>
<rectangle x1="16.72081875" y1="1.143" x2="16.97481875" y2="1.22758125" layer="94"/>
<rectangle x1="17.06118125" y1="1.143" x2="18.415" y2="1.22758125" layer="94"/>
<rectangle x1="2.24281875" y1="1.22758125" x2="2.49681875" y2="1.31241875" layer="94"/>
<rectangle x1="4.02081875" y1="1.22758125" x2="4.191" y2="1.31241875" layer="94"/>
<rectangle x1="4.36118125" y1="1.22758125" x2="4.61518125" y2="1.31241875" layer="94"/>
<rectangle x1="5.461" y1="1.22758125" x2="5.715" y2="1.31241875" layer="94"/>
<rectangle x1="5.88518125" y1="1.22758125" x2="6.13918125" y2="1.31241875" layer="94"/>
<rectangle x1="6.477" y1="1.22758125" x2="6.64718125" y2="1.31241875" layer="94"/>
<rectangle x1="7.493" y1="1.22758125" x2="7.747" y2="1.31241875" layer="94"/>
<rectangle x1="9.017" y1="1.22758125" x2="9.271" y2="1.31241875" layer="94"/>
<rectangle x1="9.35481875" y1="1.22758125" x2="9.60881875" y2="1.31241875" layer="94"/>
<rectangle x1="10.45718125" y1="1.22758125" x2="10.71118125" y2="1.31241875" layer="94"/>
<rectangle x1="10.795" y1="1.22758125" x2="11.21918125" y2="1.31241875" layer="94"/>
<rectangle x1="11.89481875" y1="1.22758125" x2="12.14881875" y2="1.31241875" layer="94"/>
<rectangle x1="12.99718125" y1="1.22758125" x2="13.25118125" y2="1.31241875" layer="94"/>
<rectangle x1="13.50518125" y1="1.22758125" x2="13.75918125" y2="1.31241875" layer="94"/>
<rectangle x1="15.28318125" y1="1.22758125" x2="15.53718125" y2="1.31241875" layer="94"/>
<rectangle x1="15.621" y1="1.22758125" x2="15.875" y2="1.31241875" layer="94"/>
<rectangle x1="16.72081875" y1="1.22758125" x2="16.97481875" y2="1.31241875" layer="94"/>
<rectangle x1="17.06118125" y1="1.22758125" x2="17.31518125" y2="1.31241875" layer="94"/>
<rectangle x1="18.161" y1="1.22758125" x2="18.415" y2="1.31241875" layer="94"/>
<rectangle x1="2.24281875" y1="1.31241875" x2="2.49681875" y2="1.397" layer="94"/>
<rectangle x1="4.02081875" y1="1.31241875" x2="4.191" y2="1.397" layer="94"/>
<rectangle x1="4.36118125" y1="1.31241875" x2="4.699" y2="1.397" layer="94"/>
<rectangle x1="5.461" y1="1.31241875" x2="5.715" y2="1.397" layer="94"/>
<rectangle x1="5.88518125" y1="1.31241875" x2="6.13918125" y2="1.397" layer="94"/>
<rectangle x1="6.477" y1="1.31241875" x2="6.64718125" y2="1.397" layer="94"/>
<rectangle x1="7.493" y1="1.31241875" x2="7.747" y2="1.397" layer="94"/>
<rectangle x1="9.017" y1="1.31241875" x2="9.271" y2="1.397" layer="94"/>
<rectangle x1="9.44118125" y1="1.31241875" x2="9.69518125" y2="1.397" layer="94"/>
<rectangle x1="10.45718125" y1="1.31241875" x2="10.62481875" y2="1.397" layer="94"/>
<rectangle x1="10.795" y1="1.31241875" x2="11.049" y2="1.397" layer="94"/>
<rectangle x1="11.64081875" y1="1.31241875" x2="11.72718125" y2="1.397" layer="94"/>
<rectangle x1="11.98118125" y1="1.31241875" x2="12.23518125" y2="1.397" layer="94"/>
<rectangle x1="12.99718125" y1="1.31241875" x2="13.16481875" y2="1.397" layer="94"/>
<rectangle x1="13.50518125" y1="1.31241875" x2="13.75918125" y2="1.397" layer="94"/>
<rectangle x1="15.28318125" y1="1.31241875" x2="15.53718125" y2="1.397" layer="94"/>
<rectangle x1="15.621" y1="1.31241875" x2="15.875" y2="1.397" layer="94"/>
<rectangle x1="16.637" y1="1.31241875" x2="16.891" y2="1.397" layer="94"/>
<rectangle x1="17.145" y1="1.31241875" x2="17.399" y2="1.397" layer="94"/>
<rectangle x1="18.161" y1="1.31241875" x2="18.415" y2="1.397" layer="94"/>
<rectangle x1="2.24281875" y1="1.397" x2="2.49681875" y2="1.48158125" layer="94"/>
<rectangle x1="4.02081875" y1="1.397" x2="4.191" y2="1.48158125" layer="94"/>
<rectangle x1="4.36118125" y1="1.397" x2="4.78281875" y2="1.48158125" layer="94"/>
<rectangle x1="5.37718125" y1="1.397" x2="5.63118125" y2="1.48158125" layer="94"/>
<rectangle x1="5.88518125" y1="1.397" x2="6.13918125" y2="1.48158125" layer="94"/>
<rectangle x1="6.477" y1="1.397" x2="6.64718125" y2="1.48158125" layer="94"/>
<rectangle x1="7.493" y1="1.397" x2="7.747" y2="1.48158125" layer="94"/>
<rectangle x1="9.017" y1="1.397" x2="9.271" y2="1.48158125" layer="94"/>
<rectangle x1="9.44118125" y1="1.397" x2="9.69518125" y2="1.48158125" layer="94"/>
<rectangle x1="10.37081875" y1="1.397" x2="10.62481875" y2="1.48158125" layer="94"/>
<rectangle x1="10.795" y1="1.397" x2="11.049" y2="1.48158125" layer="94"/>
<rectangle x1="11.557" y1="1.397" x2="11.811" y2="1.48158125" layer="94"/>
<rectangle x1="11.98118125" y1="1.397" x2="12.319" y2="1.48158125" layer="94"/>
<rectangle x1="12.91081875" y1="1.397" x2="13.16481875" y2="1.48158125" layer="94"/>
<rectangle x1="13.50518125" y1="1.397" x2="13.75918125" y2="1.48158125" layer="94"/>
<rectangle x1="15.28318125" y1="1.397" x2="15.53718125" y2="1.48158125" layer="94"/>
<rectangle x1="15.621" y1="1.397" x2="15.95881875" y2="1.48158125" layer="94"/>
<rectangle x1="16.637" y1="1.397" x2="16.891" y2="1.48158125" layer="94"/>
<rectangle x1="17.145" y1="1.397" x2="17.48281875" y2="1.48158125" layer="94"/>
<rectangle x1="18.07718125" y1="1.397" x2="18.33118125" y2="1.48158125" layer="94"/>
<rectangle x1="2.24281875" y1="1.48158125" x2="2.49681875" y2="1.56641875" layer="94"/>
<rectangle x1="4.02081875" y1="1.48158125" x2="4.191" y2="1.56641875" layer="94"/>
<rectangle x1="4.36118125" y1="1.48158125" x2="4.953" y2="1.56641875" layer="94"/>
<rectangle x1="5.207" y1="1.48158125" x2="5.63118125" y2="1.56641875" layer="94"/>
<rectangle x1="5.88518125" y1="1.48158125" x2="6.13918125" y2="1.56641875" layer="94"/>
<rectangle x1="6.477" y1="1.48158125" x2="6.64718125" y2="1.56641875" layer="94"/>
<rectangle x1="7.493" y1="1.48158125" x2="7.747" y2="1.56641875" layer="94"/>
<rectangle x1="9.017" y1="1.48158125" x2="9.271" y2="1.56641875" layer="94"/>
<rectangle x1="9.525" y1="1.48158125" x2="9.86281875" y2="1.56641875" layer="94"/>
<rectangle x1="10.20318125" y1="1.48158125" x2="10.541" y2="1.56641875" layer="94"/>
<rectangle x1="10.87881875" y1="1.48158125" x2="11.21918125" y2="1.56641875" layer="94"/>
<rectangle x1="11.47318125" y1="1.48158125" x2="11.72718125" y2="1.56641875" layer="94"/>
<rectangle x1="12.065" y1="1.48158125" x2="12.40281875" y2="1.56641875" layer="94"/>
<rectangle x1="12.74318125" y1="1.48158125" x2="13.081" y2="1.56641875" layer="94"/>
<rectangle x1="13.589" y1="1.48158125" x2="13.75918125" y2="1.56641875" layer="94"/>
<rectangle x1="15.28318125" y1="1.48158125" x2="15.53718125" y2="1.56641875" layer="94"/>
<rectangle x1="15.621" y1="1.48158125" x2="16.129" y2="1.56641875" layer="94"/>
<rectangle x1="16.46681875" y1="1.48158125" x2="16.80718125" y2="1.56641875" layer="94"/>
<rectangle x1="17.22881875" y1="1.48158125" x2="17.653" y2="1.56641875" layer="94"/>
<rectangle x1="17.907" y1="1.48158125" x2="18.24481875" y2="1.56641875" layer="94"/>
<rectangle x1="2.24281875" y1="1.56641875" x2="2.49681875" y2="1.651" layer="94"/>
<rectangle x1="4.02081875" y1="1.56641875" x2="4.191" y2="1.651" layer="94"/>
<rectangle x1="4.36118125" y1="1.56641875" x2="5.54481875" y2="1.651" layer="94"/>
<rectangle x1="5.88518125" y1="1.56641875" x2="6.13918125" y2="1.651" layer="94"/>
<rectangle x1="6.223" y1="1.56641875" x2="7.239" y2="1.651" layer="94"/>
<rectangle x1="7.493" y1="1.56641875" x2="7.747" y2="1.651" layer="94"/>
<rectangle x1="9.017" y1="1.56641875" x2="9.271" y2="1.651" layer="94"/>
<rectangle x1="9.60881875" y1="1.56641875" x2="10.45718125" y2="1.651" layer="94"/>
<rectangle x1="10.87881875" y1="1.56641875" x2="11.72718125" y2="1.651" layer="94"/>
<rectangle x1="12.14881875" y1="1.56641875" x2="12.99718125" y2="1.651" layer="94"/>
<rectangle x1="13.589" y1="1.56641875" x2="13.843" y2="1.651" layer="94"/>
<rectangle x1="15.19681875" y1="1.56641875" x2="15.45081875" y2="1.651" layer="94"/>
<rectangle x1="15.621" y1="1.56641875" x2="16.72081875" y2="1.651" layer="94"/>
<rectangle x1="17.31518125" y1="1.56641875" x2="18.161" y2="1.651" layer="94"/>
<rectangle x1="2.24281875" y1="1.651" x2="2.49681875" y2="1.73558125" layer="94"/>
<rectangle x1="4.02081875" y1="1.651" x2="4.191" y2="1.73558125" layer="94"/>
<rectangle x1="4.36118125" y1="1.651" x2="4.61518125" y2="1.73558125" layer="94"/>
<rectangle x1="4.78281875" y1="1.651" x2="5.37718125" y2="1.73558125" layer="94"/>
<rectangle x1="5.88518125" y1="1.651" x2="6.13918125" y2="1.73558125" layer="94"/>
<rectangle x1="6.223" y1="1.651" x2="7.239" y2="1.73558125" layer="94"/>
<rectangle x1="7.493" y1="1.651" x2="7.747" y2="1.73558125" layer="94"/>
<rectangle x1="9.017" y1="1.651" x2="9.18718125" y2="1.73558125" layer="94"/>
<rectangle x1="9.69518125" y1="1.651" x2="10.37081875" y2="1.73558125" layer="94"/>
<rectangle x1="11.049" y1="1.651" x2="11.557" y2="1.73558125" layer="94"/>
<rectangle x1="12.23518125" y1="1.651" x2="12.91081875" y2="1.73558125" layer="94"/>
<rectangle x1="13.589" y1="1.651" x2="13.843" y2="1.73558125" layer="94"/>
<rectangle x1="15.19681875" y1="1.651" x2="15.45081875" y2="1.73558125" layer="94"/>
<rectangle x1="15.621" y1="1.651" x2="15.79118125" y2="1.73558125" layer="94"/>
<rectangle x1="15.95881875" y1="1.651" x2="16.637" y2="1.73558125" layer="94"/>
<rectangle x1="17.399" y1="1.651" x2="18.07718125" y2="1.73558125" layer="94"/>
<rectangle x1="2.24281875" y1="1.73558125" x2="2.49681875" y2="1.82041875" layer="94"/>
<rectangle x1="4.02081875" y1="1.73558125" x2="4.191" y2="1.82041875" layer="94"/>
<rectangle x1="4.445" y1="1.73558125" x2="4.52881875" y2="1.82041875" layer="94"/>
<rectangle x1="4.953" y1="1.73558125" x2="5.12318125" y2="1.82041875" layer="94"/>
<rectangle x1="5.969" y1="1.73558125" x2="6.05281875" y2="1.82041875" layer="94"/>
<rectangle x1="6.30681875" y1="1.73558125" x2="7.15518125" y2="1.82041875" layer="94"/>
<rectangle x1="7.493" y1="1.73558125" x2="7.747" y2="1.82041875" layer="94"/>
<rectangle x1="8.93318125" y1="1.73558125" x2="9.18718125" y2="1.82041875" layer="94"/>
<rectangle x1="9.94918125" y1="1.73558125" x2="10.11681875" y2="1.82041875" layer="94"/>
<rectangle x1="11.21918125" y1="1.73558125" x2="11.38681875" y2="1.82041875" layer="94"/>
<rectangle x1="12.48918125" y1="1.73558125" x2="12.65681875" y2="1.82041875" layer="94"/>
<rectangle x1="13.67281875" y1="1.73558125" x2="13.92681875" y2="1.82041875" layer="94"/>
<rectangle x1="15.113" y1="1.73558125" x2="15.367" y2="1.82041875" layer="94"/>
<rectangle x1="16.21281875" y1="1.73558125" x2="16.383" y2="1.82041875" layer="94"/>
<rectangle x1="17.653" y1="1.73558125" x2="17.82318125" y2="1.82041875" layer="94"/>
<rectangle x1="2.24281875" y1="1.82041875" x2="2.49681875" y2="1.905" layer="94"/>
<rectangle x1="4.02081875" y1="1.82041875" x2="4.191" y2="1.905" layer="94"/>
<rectangle x1="5.88518125" y1="1.82041875" x2="6.13918125" y2="1.905" layer="94"/>
<rectangle x1="6.477" y1="1.82041875" x2="6.64718125" y2="1.905" layer="94"/>
<rectangle x1="7.493" y1="1.82041875" x2="7.747" y2="1.905" layer="94"/>
<rectangle x1="8.84681875" y1="1.82041875" x2="9.18718125" y2="1.905" layer="94"/>
<rectangle x1="13.67281875" y1="1.82041875" x2="14.01318125" y2="1.905" layer="94"/>
<rectangle x1="15.02918125" y1="1.82041875" x2="15.367" y2="1.905" layer="94"/>
<rectangle x1="2.24281875" y1="1.905" x2="2.49681875" y2="1.98958125" layer="94"/>
<rectangle x1="4.02081875" y1="1.905" x2="4.191" y2="1.98958125" layer="94"/>
<rectangle x1="5.88518125" y1="1.905" x2="6.13918125" y2="1.98958125" layer="94"/>
<rectangle x1="6.477" y1="1.905" x2="6.64718125" y2="1.98958125" layer="94"/>
<rectangle x1="7.493" y1="1.905" x2="7.747" y2="1.98958125" layer="94"/>
<rectangle x1="8.84681875" y1="1.905" x2="9.10081875" y2="1.98958125" layer="94"/>
<rectangle x1="13.75918125" y1="1.905" x2="14.097" y2="1.98958125" layer="94"/>
<rectangle x1="14.94281875" y1="1.905" x2="15.28318125" y2="1.98958125" layer="94"/>
<rectangle x1="2.24281875" y1="1.98958125" x2="2.49681875" y2="2.07441875" layer="94"/>
<rectangle x1="4.02081875" y1="1.98958125" x2="4.27481875" y2="2.07441875" layer="94"/>
<rectangle x1="5.88518125" y1="1.98958125" x2="6.05281875" y2="2.07441875" layer="94"/>
<rectangle x1="6.477" y1="1.98958125" x2="6.64718125" y2="2.07441875" layer="94"/>
<rectangle x1="7.493" y1="1.98958125" x2="7.747" y2="2.07441875" layer="94"/>
<rectangle x1="8.67918125" y1="1.98958125" x2="9.017" y2="2.07441875" layer="94"/>
<rectangle x1="13.843" y1="1.98958125" x2="14.26718125" y2="2.07441875" layer="94"/>
<rectangle x1="14.77518125" y1="1.98958125" x2="15.19681875" y2="2.07441875" layer="94"/>
<rectangle x1="2.24281875" y1="2.07441875" x2="2.49681875" y2="2.159" layer="94"/>
<rectangle x1="4.02081875" y1="2.07441875" x2="4.27481875" y2="2.159" layer="94"/>
<rectangle x1="6.477" y1="2.07441875" x2="6.64718125" y2="2.159" layer="94"/>
<rectangle x1="7.493" y1="2.07441875" x2="7.747" y2="2.159" layer="94"/>
<rectangle x1="8.59281875" y1="2.07441875" x2="8.93318125" y2="2.159" layer="94"/>
<rectangle x1="13.92681875" y1="2.07441875" x2="15.113" y2="2.159" layer="94"/>
<rectangle x1="2.24281875" y1="2.159" x2="2.49681875" y2="2.24358125" layer="94"/>
<rectangle x1="4.02081875" y1="2.159" x2="4.191" y2="2.24358125" layer="94"/>
<rectangle x1="6.477" y1="2.159" x2="6.64718125" y2="2.24358125" layer="94"/>
<rectangle x1="7.493" y1="2.159" x2="8.84681875" y2="2.24358125" layer="94"/>
<rectangle x1="14.097" y1="2.159" x2="14.94281875" y2="2.24358125" layer="94"/>
<rectangle x1="2.24281875" y1="2.24358125" x2="2.413" y2="2.32841875" layer="94"/>
<rectangle x1="4.02081875" y1="2.24358125" x2="4.191" y2="2.32841875" layer="94"/>
<rectangle x1="6.477" y1="2.24358125" x2="6.64718125" y2="2.32841875" layer="94"/>
<rectangle x1="7.493" y1="2.24358125" x2="8.763" y2="2.32841875" layer="94"/>
<rectangle x1="14.26718125" y1="2.24358125" x2="14.77518125" y2="2.32841875" layer="94"/>
<rectangle x1="7.493" y1="2.32841875" x2="8.509" y2="2.413" layer="94"/>
<rectangle x1="3.34518125" y1="3.25958125" x2="16.80718125" y2="3.34441875" layer="94"/>
<rectangle x1="3.00481875" y1="3.34441875" x2="17.145" y2="3.429" layer="94"/>
<rectangle x1="2.667" y1="3.429" x2="17.399" y2="3.51358125" layer="94"/>
<rectangle x1="2.49681875" y1="3.51358125" x2="17.56918125" y2="3.59841875" layer="94"/>
<rectangle x1="2.32918125" y1="3.59841875" x2="17.82318125" y2="3.683" layer="94"/>
<rectangle x1="2.159" y1="3.683" x2="17.99081875" y2="3.76758125" layer="94"/>
<rectangle x1="1.98881875" y1="3.76758125" x2="18.07718125" y2="3.85241875" layer="94"/>
<rectangle x1="1.905" y1="3.85241875" x2="3.51281875" y2="3.937" layer="94"/>
<rectangle x1="9.779" y1="3.85241875" x2="18.24481875" y2="3.937" layer="94"/>
<rectangle x1="1.73481875" y1="3.937" x2="3.175" y2="4.02158125" layer="94"/>
<rectangle x1="9.779" y1="3.937" x2="18.33118125" y2="4.02158125" layer="94"/>
<rectangle x1="1.651" y1="4.02158125" x2="2.83718125" y2="4.10641875" layer="94"/>
<rectangle x1="9.69518125" y1="4.02158125" x2="18.49881875" y2="4.10641875" layer="94"/>
<rectangle x1="1.56718125" y1="4.10641875" x2="2.667" y2="4.191" layer="94"/>
<rectangle x1="9.60881875" y1="4.10641875" x2="18.58518125" y2="4.191" layer="94"/>
<rectangle x1="1.397" y1="4.191" x2="2.49681875" y2="4.27558125" layer="94"/>
<rectangle x1="9.60881875" y1="4.191" x2="18.669" y2="4.27558125" layer="94"/>
<rectangle x1="1.31318125" y1="4.27558125" x2="2.32918125" y2="4.36041875" layer="94"/>
<rectangle x1="9.525" y1="4.27558125" x2="18.75281875" y2="4.36041875" layer="94"/>
<rectangle x1="1.22681875" y1="4.36041875" x2="2.159" y2="4.445" layer="94"/>
<rectangle x1="9.525" y1="4.36041875" x2="18.83918125" y2="4.445" layer="94"/>
<rectangle x1="1.143" y1="4.445" x2="2.07518125" y2="4.52958125" layer="94"/>
<rectangle x1="9.44118125" y1="4.445" x2="18.923" y2="4.52958125" layer="94"/>
<rectangle x1="1.05918125" y1="4.52958125" x2="1.98881875" y2="4.61441875" layer="94"/>
<rectangle x1="9.44118125" y1="4.52958125" x2="19.00681875" y2="4.61441875" layer="94"/>
<rectangle x1="0.97281875" y1="4.61441875" x2="1.905" y2="4.699" layer="94"/>
<rectangle x1="9.44118125" y1="4.61441875" x2="19.09318125" y2="4.699" layer="94"/>
<rectangle x1="0.97281875" y1="4.699" x2="1.73481875" y2="4.78358125" layer="94"/>
<rectangle x1="9.35481875" y1="4.699" x2="19.177" y2="4.78358125" layer="94"/>
<rectangle x1="0.889" y1="4.78358125" x2="1.651" y2="4.86841875" layer="94"/>
<rectangle x1="9.35481875" y1="4.78358125" x2="19.26081875" y2="4.86841875" layer="94"/>
<rectangle x1="0.80518125" y1="4.86841875" x2="1.56718125" y2="4.953" layer="94"/>
<rectangle x1="9.35481875" y1="4.86841875" x2="19.26081875" y2="4.953" layer="94"/>
<rectangle x1="0.71881875" y1="4.953" x2="1.48081875" y2="5.03758125" layer="94"/>
<rectangle x1="9.271" y1="4.953" x2="19.34718125" y2="5.03758125" layer="94"/>
<rectangle x1="0.71881875" y1="5.03758125" x2="1.48081875" y2="5.12241875" layer="94"/>
<rectangle x1="9.271" y1="5.03758125" x2="19.431" y2="5.12241875" layer="94"/>
<rectangle x1="0.635" y1="5.12241875" x2="1.397" y2="5.207" layer="94"/>
<rectangle x1="9.271" y1="5.12241875" x2="19.431" y2="5.207" layer="94"/>
<rectangle x1="0.55118125" y1="5.207" x2="1.31318125" y2="5.29158125" layer="94"/>
<rectangle x1="9.271" y1="5.207" x2="19.51481875" y2="5.29158125" layer="94"/>
<rectangle x1="0.55118125" y1="5.29158125" x2="1.22681875" y2="5.37641875" layer="94"/>
<rectangle x1="9.18718125" y1="5.29158125" x2="19.51481875" y2="5.37641875" layer="94"/>
<rectangle x1="0.46481875" y1="5.37641875" x2="1.22681875" y2="5.461" layer="94"/>
<rectangle x1="9.18718125" y1="5.37641875" x2="19.60118125" y2="5.461" layer="94"/>
<rectangle x1="0.46481875" y1="5.461" x2="1.143" y2="5.54558125" layer="94"/>
<rectangle x1="9.18718125" y1="5.461" x2="19.685" y2="5.54558125" layer="94"/>
<rectangle x1="0.381" y1="5.54558125" x2="1.05918125" y2="5.63041875" layer="94"/>
<rectangle x1="3.85318125" y1="5.54558125" x2="4.27481875" y2="5.63041875" layer="94"/>
<rectangle x1="5.88518125" y1="5.54558125" x2="6.81481875" y2="5.63041875" layer="94"/>
<rectangle x1="9.18718125" y1="5.54558125" x2="19.685" y2="5.63041875" layer="94"/>
<rectangle x1="0.381" y1="5.63041875" x2="1.05918125" y2="5.715" layer="94"/>
<rectangle x1="3.51281875" y1="5.63041875" x2="4.61518125" y2="5.715" layer="94"/>
<rectangle x1="5.79881875" y1="5.63041875" x2="7.239" y2="5.715" layer="94"/>
<rectangle x1="9.10081875" y1="5.63041875" x2="10.96518125" y2="5.715" layer="94"/>
<rectangle x1="11.64081875" y1="5.63041875" x2="19.685" y2="5.715" layer="94"/>
<rectangle x1="0.381" y1="5.715" x2="0.97281875" y2="5.79958125" layer="94"/>
<rectangle x1="3.34518125" y1="5.715" x2="4.78281875" y2="5.79958125" layer="94"/>
<rectangle x1="5.715" y1="5.715" x2="7.40918125" y2="5.79958125" layer="94"/>
<rectangle x1="9.10081875" y1="5.715" x2="10.71118125" y2="5.79958125" layer="94"/>
<rectangle x1="11.89481875" y1="5.715" x2="13.335" y2="5.79958125" layer="94"/>
<rectangle x1="13.67281875" y1="5.715" x2="15.113" y2="5.79958125" layer="94"/>
<rectangle x1="15.367" y1="5.715" x2="16.383" y2="5.79958125" layer="94"/>
<rectangle x1="17.22881875" y1="5.715" x2="19.76881875" y2="5.79958125" layer="94"/>
<rectangle x1="0.29718125" y1="5.79958125" x2="0.97281875" y2="5.88441875" layer="94"/>
<rectangle x1="3.175" y1="5.79958125" x2="4.953" y2="5.88441875" layer="94"/>
<rectangle x1="5.715" y1="5.79958125" x2="7.57681875" y2="5.88441875" layer="94"/>
<rectangle x1="9.10081875" y1="5.79958125" x2="10.541" y2="5.88441875" layer="94"/>
<rectangle x1="12.065" y1="5.79958125" x2="13.335" y2="5.88441875" layer="94"/>
<rectangle x1="13.67281875" y1="5.79958125" x2="15.113" y2="5.88441875" layer="94"/>
<rectangle x1="15.45081875" y1="5.79958125" x2="16.21281875" y2="5.88441875" layer="94"/>
<rectangle x1="17.48281875" y1="5.79958125" x2="19.76881875" y2="5.88441875" layer="94"/>
<rectangle x1="0.29718125" y1="5.88441875" x2="0.889" y2="5.969" layer="94"/>
<rectangle x1="3.09118125" y1="5.88441875" x2="5.12318125" y2="5.969" layer="94"/>
<rectangle x1="5.715" y1="5.88441875" x2="7.66318125" y2="5.969" layer="94"/>
<rectangle x1="9.10081875" y1="5.88441875" x2="10.37081875" y2="5.969" layer="94"/>
<rectangle x1="12.23518125" y1="5.88441875" x2="13.335" y2="5.969" layer="94"/>
<rectangle x1="13.67281875" y1="5.88441875" x2="15.113" y2="5.969" layer="94"/>
<rectangle x1="15.45081875" y1="5.88441875" x2="16.129" y2="5.969" layer="94"/>
<rectangle x1="17.56918125" y1="5.88441875" x2="19.85518125" y2="5.969" layer="94"/>
<rectangle x1="0.21081875" y1="5.969" x2="0.889" y2="6.05358125" layer="94"/>
<rectangle x1="2.921" y1="5.969" x2="5.207" y2="6.05358125" layer="94"/>
<rectangle x1="5.715" y1="5.969" x2="7.747" y2="6.05358125" layer="94"/>
<rectangle x1="9.10081875" y1="5.969" x2="10.287" y2="6.05358125" layer="94"/>
<rectangle x1="12.319" y1="5.969" x2="13.335" y2="6.05358125" layer="94"/>
<rectangle x1="13.67281875" y1="5.969" x2="15.113" y2="6.05358125" layer="94"/>
<rectangle x1="15.45081875" y1="5.969" x2="16.04518125" y2="6.05358125" layer="94"/>
<rectangle x1="17.653" y1="5.969" x2="19.85518125" y2="6.05358125" layer="94"/>
<rectangle x1="0.21081875" y1="6.05358125" x2="0.889" y2="6.13841875" layer="94"/>
<rectangle x1="2.83718125" y1="6.05358125" x2="5.29081875" y2="6.13841875" layer="94"/>
<rectangle x1="5.79881875" y1="6.05358125" x2="7.83081875" y2="6.13841875" layer="94"/>
<rectangle x1="9.10081875" y1="6.05358125" x2="10.11681875" y2="6.13841875" layer="94"/>
<rectangle x1="12.40281875" y1="6.05358125" x2="13.335" y2="6.13841875" layer="94"/>
<rectangle x1="13.67281875" y1="6.05358125" x2="15.113" y2="6.13841875" layer="94"/>
<rectangle x1="15.45081875" y1="6.05358125" x2="15.95881875" y2="6.13841875" layer="94"/>
<rectangle x1="16.55318125" y1="6.05358125" x2="17.06118125" y2="6.13841875" layer="94"/>
<rectangle x1="17.73681875" y1="6.05358125" x2="19.85518125" y2="6.13841875" layer="94"/>
<rectangle x1="0.21081875" y1="6.13841875" x2="0.80518125" y2="6.223" layer="94"/>
<rectangle x1="2.75081875" y1="6.13841875" x2="5.37718125" y2="6.223" layer="94"/>
<rectangle x1="5.88518125" y1="6.13841875" x2="7.91718125" y2="6.223" layer="94"/>
<rectangle x1="9.017" y1="6.13841875" x2="10.11681875" y2="6.223" layer="94"/>
<rectangle x1="10.96518125" y1="6.13841875" x2="11.64081875" y2="6.223" layer="94"/>
<rectangle x1="12.48918125" y1="6.13841875" x2="13.335" y2="6.223" layer="94"/>
<rectangle x1="13.67281875" y1="6.13841875" x2="15.02918125" y2="6.223" layer="94"/>
<rectangle x1="15.45081875" y1="6.13841875" x2="15.875" y2="6.223" layer="94"/>
<rectangle x1="16.46681875" y1="6.13841875" x2="17.22881875" y2="6.223" layer="94"/>
<rectangle x1="17.73681875" y1="6.13841875" x2="19.939" y2="6.223" layer="94"/>
<rectangle x1="0.21081875" y1="6.223" x2="0.80518125" y2="6.30758125" layer="94"/>
<rectangle x1="2.75081875" y1="6.223" x2="3.683" y2="6.30758125" layer="94"/>
<rectangle x1="4.445" y1="6.223" x2="5.37718125" y2="6.30758125" layer="94"/>
<rectangle x1="7.06881875" y1="6.223" x2="8.001" y2="6.30758125" layer="94"/>
<rectangle x1="9.017" y1="6.223" x2="10.033" y2="6.30758125" layer="94"/>
<rectangle x1="10.795" y1="6.223" x2="11.811" y2="6.30758125" layer="94"/>
<rectangle x1="12.573" y1="6.223" x2="13.335" y2="6.30758125" layer="94"/>
<rectangle x1="13.67281875" y1="6.223" x2="15.02918125" y2="6.30758125" layer="94"/>
<rectangle x1="15.45081875" y1="6.223" x2="15.875" y2="6.30758125" layer="94"/>
<rectangle x1="16.29918125" y1="6.223" x2="17.399" y2="6.30758125" layer="94"/>
<rectangle x1="17.73681875" y1="6.223" x2="19.939" y2="6.30758125" layer="94"/>
<rectangle x1="0.127" y1="6.30758125" x2="0.80518125" y2="6.39241875" layer="94"/>
<rectangle x1="2.667" y1="6.30758125" x2="3.51281875" y2="6.39241875" layer="94"/>
<rectangle x1="4.61518125" y1="6.30758125" x2="5.461" y2="6.39241875" layer="94"/>
<rectangle x1="7.239" y1="6.30758125" x2="8.08481875" y2="6.39241875" layer="94"/>
<rectangle x1="9.017" y1="6.30758125" x2="9.94918125" y2="6.39241875" layer="94"/>
<rectangle x1="10.62481875" y1="6.30758125" x2="11.98118125" y2="6.39241875" layer="94"/>
<rectangle x1="12.65681875" y1="6.30758125" x2="13.335" y2="6.39241875" layer="94"/>
<rectangle x1="13.67281875" y1="6.30758125" x2="15.113" y2="6.39241875" layer="94"/>
<rectangle x1="15.45081875" y1="6.30758125" x2="15.79118125" y2="6.39241875" layer="94"/>
<rectangle x1="16.21281875" y1="6.30758125" x2="17.48281875" y2="6.39241875" layer="94"/>
<rectangle x1="17.653" y1="6.30758125" x2="19.939" y2="6.39241875" layer="94"/>
<rectangle x1="0.127" y1="6.39241875" x2="0.71881875" y2="6.477" layer="94"/>
<rectangle x1="2.58318125" y1="6.39241875" x2="3.34518125" y2="6.477" layer="94"/>
<rectangle x1="4.78281875" y1="6.39241875" x2="5.54481875" y2="6.477" layer="94"/>
<rectangle x1="7.32281875" y1="6.39241875" x2="8.17118125" y2="6.477" layer="94"/>
<rectangle x1="9.017" y1="6.39241875" x2="9.86281875" y2="6.477" layer="94"/>
<rectangle x1="10.541" y1="6.39241875" x2="12.065" y2="6.477" layer="94"/>
<rectangle x1="12.65681875" y1="6.39241875" x2="13.335" y2="6.477" layer="94"/>
<rectangle x1="13.67281875" y1="6.39241875" x2="15.113" y2="6.477" layer="94"/>
<rectangle x1="15.45081875" y1="6.39241875" x2="15.79118125" y2="6.477" layer="94"/>
<rectangle x1="16.21281875" y1="6.39241875" x2="19.939" y2="6.477" layer="94"/>
<rectangle x1="0.127" y1="6.477" x2="0.71881875" y2="6.56158125" layer="94"/>
<rectangle x1="2.58318125" y1="6.477" x2="3.25881875" y2="6.56158125" layer="94"/>
<rectangle x1="4.86918125" y1="6.477" x2="5.54481875" y2="6.56158125" layer="94"/>
<rectangle x1="7.493" y1="6.477" x2="8.17118125" y2="6.56158125" layer="94"/>
<rectangle x1="9.017" y1="6.477" x2="9.86281875" y2="6.56158125" layer="94"/>
<rectangle x1="10.45718125" y1="6.477" x2="12.14881875" y2="6.56158125" layer="94"/>
<rectangle x1="12.74318125" y1="6.477" x2="13.335" y2="6.56158125" layer="94"/>
<rectangle x1="13.67281875" y1="6.477" x2="15.113" y2="6.56158125" layer="94"/>
<rectangle x1="15.45081875" y1="6.477" x2="15.79118125" y2="6.56158125" layer="94"/>
<rectangle x1="16.21281875" y1="6.477" x2="19.939" y2="6.56158125" layer="94"/>
<rectangle x1="0.127" y1="6.56158125" x2="0.71881875" y2="6.64641875" layer="94"/>
<rectangle x1="2.49681875" y1="6.56158125" x2="3.175" y2="6.64641875" layer="94"/>
<rectangle x1="4.953" y1="6.56158125" x2="5.63118125" y2="6.64641875" layer="94"/>
<rectangle x1="7.493" y1="6.56158125" x2="8.255" y2="6.64641875" layer="94"/>
<rectangle x1="9.017" y1="6.56158125" x2="9.779" y2="6.64641875" layer="94"/>
<rectangle x1="10.37081875" y1="6.56158125" x2="12.23518125" y2="6.64641875" layer="94"/>
<rectangle x1="12.74318125" y1="6.56158125" x2="13.335" y2="6.64641875" layer="94"/>
<rectangle x1="13.67281875" y1="6.56158125" x2="15.02918125" y2="6.64641875" layer="94"/>
<rectangle x1="15.45081875" y1="6.56158125" x2="15.79118125" y2="6.64641875" layer="94"/>
<rectangle x1="17.82318125" y1="6.56158125" x2="19.939" y2="6.64641875" layer="94"/>
<rectangle x1="0.127" y1="6.64641875" x2="0.71881875" y2="6.731" layer="94"/>
<rectangle x1="2.49681875" y1="6.64641875" x2="3.175" y2="6.731" layer="94"/>
<rectangle x1="4.953" y1="6.64641875" x2="5.63118125" y2="6.731" layer="94"/>
<rectangle x1="7.57681875" y1="6.64641875" x2="8.255" y2="6.731" layer="94"/>
<rectangle x1="9.017" y1="6.64641875" x2="9.779" y2="6.731" layer="94"/>
<rectangle x1="10.287" y1="6.64641875" x2="12.319" y2="6.731" layer="94"/>
<rectangle x1="12.827" y1="6.64641875" x2="13.335" y2="6.731" layer="94"/>
<rectangle x1="13.67281875" y1="6.64641875" x2="15.02918125" y2="6.731" layer="94"/>
<rectangle x1="15.45081875" y1="6.64641875" x2="15.70481875" y2="6.731" layer="94"/>
<rectangle x1="17.907" y1="6.64641875" x2="19.939" y2="6.731" layer="94"/>
<rectangle x1="0.04318125" y1="6.731" x2="0.71881875" y2="6.81558125" layer="94"/>
<rectangle x1="2.49681875" y1="6.731" x2="3.09118125" y2="6.81558125" layer="94"/>
<rectangle x1="5.03681875" y1="6.731" x2="5.715" y2="6.81558125" layer="94"/>
<rectangle x1="7.66318125" y1="6.731" x2="8.255" y2="6.81558125" layer="94"/>
<rectangle x1="9.017" y1="6.731" x2="9.779" y2="6.81558125" layer="94"/>
<rectangle x1="10.287" y1="6.731" x2="12.319" y2="6.81558125" layer="94"/>
<rectangle x1="12.827" y1="6.731" x2="13.335" y2="6.81558125" layer="94"/>
<rectangle x1="13.67281875" y1="6.731" x2="15.113" y2="6.81558125" layer="94"/>
<rectangle x1="15.45081875" y1="6.731" x2="15.70481875" y2="6.81558125" layer="94"/>
<rectangle x1="17.907" y1="6.731" x2="19.939" y2="6.81558125" layer="94"/>
<rectangle x1="0.04318125" y1="6.81558125" x2="0.71881875" y2="6.90041875" layer="94"/>
<rectangle x1="2.413" y1="6.81558125" x2="3.09118125" y2="6.90041875" layer="94"/>
<rectangle x1="5.03681875" y1="6.81558125" x2="5.715" y2="6.90041875" layer="94"/>
<rectangle x1="7.66318125" y1="6.81558125" x2="8.33881875" y2="6.90041875" layer="94"/>
<rectangle x1="9.017" y1="6.81558125" x2="9.69518125" y2="6.90041875" layer="94"/>
<rectangle x1="10.20318125" y1="6.81558125" x2="12.319" y2="6.90041875" layer="94"/>
<rectangle x1="12.827" y1="6.81558125" x2="13.335" y2="6.90041875" layer="94"/>
<rectangle x1="13.67281875" y1="6.81558125" x2="15.02918125" y2="6.90041875" layer="94"/>
<rectangle x1="15.45081875" y1="6.81558125" x2="15.70481875" y2="6.90041875" layer="94"/>
<rectangle x1="17.907" y1="6.81558125" x2="19.939" y2="6.90041875" layer="94"/>
<rectangle x1="0.04318125" y1="6.90041875" x2="0.635" y2="6.985" layer="94"/>
<rectangle x1="2.413" y1="6.90041875" x2="3.00481875" y2="6.985" layer="94"/>
<rectangle x1="5.12318125" y1="6.90041875" x2="5.715" y2="6.985" layer="94"/>
<rectangle x1="7.66318125" y1="6.90041875" x2="8.33881875" y2="6.985" layer="94"/>
<rectangle x1="9.017" y1="6.90041875" x2="9.69518125" y2="6.985" layer="94"/>
<rectangle x1="10.20318125" y1="6.90041875" x2="12.40281875" y2="6.985" layer="94"/>
<rectangle x1="12.91081875" y1="6.90041875" x2="13.335" y2="6.985" layer="94"/>
<rectangle x1="13.67281875" y1="6.90041875" x2="15.02918125" y2="6.985" layer="94"/>
<rectangle x1="15.45081875" y1="6.90041875" x2="15.79118125" y2="6.985" layer="94"/>
<rectangle x1="16.21281875" y1="6.90041875" x2="17.399" y2="6.985" layer="94"/>
<rectangle x1="17.907" y1="6.90041875" x2="19.939" y2="6.985" layer="94"/>
<rectangle x1="0.04318125" y1="6.985" x2="0.635" y2="7.06958125" layer="94"/>
<rectangle x1="2.413" y1="6.985" x2="3.00481875" y2="7.06958125" layer="94"/>
<rectangle x1="5.12318125" y1="6.985" x2="5.715" y2="7.06958125" layer="94"/>
<rectangle x1="7.747" y1="6.985" x2="8.33881875" y2="7.06958125" layer="94"/>
<rectangle x1="9.017" y1="6.985" x2="9.69518125" y2="7.06958125" layer="94"/>
<rectangle x1="10.20318125" y1="6.985" x2="12.40281875" y2="7.06958125" layer="94"/>
<rectangle x1="12.91081875" y1="6.985" x2="13.335" y2="7.06958125" layer="94"/>
<rectangle x1="13.75918125" y1="6.985" x2="15.02918125" y2="7.06958125" layer="94"/>
<rectangle x1="15.45081875" y1="6.985" x2="15.79118125" y2="7.06958125" layer="94"/>
<rectangle x1="16.21281875" y1="6.985" x2="17.48281875" y2="7.06958125" layer="94"/>
<rectangle x1="17.82318125" y1="6.985" x2="19.939" y2="7.06958125" layer="94"/>
<rectangle x1="0.04318125" y1="7.06958125" x2="0.635" y2="7.15441875" layer="94"/>
<rectangle x1="2.413" y1="7.06958125" x2="3.00481875" y2="7.15441875" layer="94"/>
<rectangle x1="5.12318125" y1="7.06958125" x2="5.715" y2="7.15441875" layer="94"/>
<rectangle x1="7.747" y1="7.06958125" x2="8.33881875" y2="7.15441875" layer="94"/>
<rectangle x1="9.017" y1="7.06958125" x2="9.69518125" y2="7.15441875" layer="94"/>
<rectangle x1="10.20318125" y1="7.06958125" x2="12.40281875" y2="7.15441875" layer="94"/>
<rectangle x1="12.91081875" y1="7.06958125" x2="13.335" y2="7.15441875" layer="94"/>
<rectangle x1="13.75918125" y1="7.06958125" x2="14.94281875" y2="7.15441875" layer="94"/>
<rectangle x1="15.367" y1="7.06958125" x2="15.79118125" y2="7.15441875" layer="94"/>
<rectangle x1="16.21281875" y1="7.06958125" x2="17.399" y2="7.15441875" layer="94"/>
<rectangle x1="17.82318125" y1="7.06958125" x2="19.939" y2="7.15441875" layer="94"/>
<rectangle x1="0.04318125" y1="7.15441875" x2="0.635" y2="7.239" layer="94"/>
<rectangle x1="2.413" y1="7.15441875" x2="3.00481875" y2="7.239" layer="94"/>
<rectangle x1="5.12318125" y1="7.15441875" x2="5.715" y2="7.239" layer="94"/>
<rectangle x1="7.747" y1="7.15441875" x2="8.33881875" y2="7.239" layer="94"/>
<rectangle x1="9.017" y1="7.15441875" x2="9.69518125" y2="7.239" layer="94"/>
<rectangle x1="10.20318125" y1="7.15441875" x2="12.40281875" y2="7.239" layer="94"/>
<rectangle x1="12.91081875" y1="7.15441875" x2="13.335" y2="7.239" layer="94"/>
<rectangle x1="13.843" y1="7.15441875" x2="14.94281875" y2="7.239" layer="94"/>
<rectangle x1="15.367" y1="7.15441875" x2="15.875" y2="7.239" layer="94"/>
<rectangle x1="16.29918125" y1="7.15441875" x2="17.31518125" y2="7.239" layer="94"/>
<rectangle x1="17.82318125" y1="7.15441875" x2="19.939" y2="7.239" layer="94"/>
<rectangle x1="0.04318125" y1="7.239" x2="0.635" y2="7.32358125" layer="94"/>
<rectangle x1="2.413" y1="7.239" x2="3.00481875" y2="7.32358125" layer="94"/>
<rectangle x1="5.12318125" y1="7.239" x2="5.715" y2="7.32358125" layer="94"/>
<rectangle x1="7.747" y1="7.239" x2="8.33881875" y2="7.32358125" layer="94"/>
<rectangle x1="9.017" y1="7.239" x2="9.69518125" y2="7.32358125" layer="94"/>
<rectangle x1="10.20318125" y1="7.239" x2="12.40281875" y2="7.32358125" layer="94"/>
<rectangle x1="12.91081875" y1="7.239" x2="13.335" y2="7.32358125" layer="94"/>
<rectangle x1="13.92681875" y1="7.239" x2="14.859" y2="7.32358125" layer="94"/>
<rectangle x1="15.28318125" y1="7.239" x2="15.875" y2="7.32358125" layer="94"/>
<rectangle x1="16.383" y1="7.239" x2="17.22881875" y2="7.32358125" layer="94"/>
<rectangle x1="17.73681875" y1="7.239" x2="19.939" y2="7.32358125" layer="94"/>
<rectangle x1="0.04318125" y1="7.32358125" x2="0.635" y2="7.40841875" layer="94"/>
<rectangle x1="2.413" y1="7.32358125" x2="3.00481875" y2="7.40841875" layer="94"/>
<rectangle x1="5.12318125" y1="7.32358125" x2="5.715" y2="7.40841875" layer="94"/>
<rectangle x1="7.747" y1="7.32358125" x2="8.33881875" y2="7.40841875" layer="94"/>
<rectangle x1="9.017" y1="7.32358125" x2="9.69518125" y2="7.40841875" layer="94"/>
<rectangle x1="10.20318125" y1="7.32358125" x2="12.40281875" y2="7.40841875" layer="94"/>
<rectangle x1="12.91081875" y1="7.32358125" x2="13.335" y2="7.40841875" layer="94"/>
<rectangle x1="14.01318125" y1="7.32358125" x2="14.68881875" y2="7.40841875" layer="94"/>
<rectangle x1="15.28318125" y1="7.32358125" x2="15.95881875" y2="7.40841875" layer="94"/>
<rectangle x1="16.46681875" y1="7.32358125" x2="17.145" y2="7.40841875" layer="94"/>
<rectangle x1="17.73681875" y1="7.32358125" x2="19.939" y2="7.40841875" layer="94"/>
<rectangle x1="0.04318125" y1="7.40841875" x2="0.635" y2="7.493" layer="94"/>
<rectangle x1="2.413" y1="7.40841875" x2="3.00481875" y2="7.493" layer="94"/>
<rectangle x1="5.12318125" y1="7.40841875" x2="5.715" y2="7.493" layer="94"/>
<rectangle x1="7.747" y1="7.40841875" x2="8.33881875" y2="7.493" layer="94"/>
<rectangle x1="9.017" y1="7.40841875" x2="9.69518125" y2="7.493" layer="94"/>
<rectangle x1="10.20318125" y1="7.40841875" x2="12.40281875" y2="7.493" layer="94"/>
<rectangle x1="12.91081875" y1="7.40841875" x2="13.335" y2="7.493" layer="94"/>
<rectangle x1="14.26718125" y1="7.40841875" x2="14.43481875" y2="7.493" layer="94"/>
<rectangle x1="15.19681875" y1="7.40841875" x2="15.95881875" y2="7.493" layer="94"/>
<rectangle x1="16.72081875" y1="7.40841875" x2="16.891" y2="7.493" layer="94"/>
<rectangle x1="17.653" y1="7.40841875" x2="19.939" y2="7.493" layer="94"/>
<rectangle x1="0.04318125" y1="7.493" x2="0.635" y2="7.57758125" layer="94"/>
<rectangle x1="2.413" y1="7.493" x2="3.00481875" y2="7.57758125" layer="94"/>
<rectangle x1="5.12318125" y1="7.493" x2="5.715" y2="7.57758125" layer="94"/>
<rectangle x1="7.66318125" y1="7.493" x2="8.33881875" y2="7.57758125" layer="94"/>
<rectangle x1="9.017" y1="7.493" x2="9.69518125" y2="7.57758125" layer="94"/>
<rectangle x1="10.20318125" y1="7.493" x2="12.40281875" y2="7.57758125" layer="94"/>
<rectangle x1="12.91081875" y1="7.493" x2="13.335" y2="7.57758125" layer="94"/>
<rectangle x1="15.113" y1="7.493" x2="16.04518125" y2="7.57758125" layer="94"/>
<rectangle x1="17.56918125" y1="7.493" x2="19.939" y2="7.57758125" layer="94"/>
<rectangle x1="0.04318125" y1="7.57758125" x2="0.71881875" y2="7.66241875" layer="94"/>
<rectangle x1="2.413" y1="7.57758125" x2="3.00481875" y2="7.66241875" layer="94"/>
<rectangle x1="5.12318125" y1="7.57758125" x2="5.715" y2="7.66241875" layer="94"/>
<rectangle x1="7.66318125" y1="7.57758125" x2="8.33881875" y2="7.66241875" layer="94"/>
<rectangle x1="9.017" y1="7.57758125" x2="9.69518125" y2="7.66241875" layer="94"/>
<rectangle x1="10.20318125" y1="7.57758125" x2="12.40281875" y2="7.66241875" layer="94"/>
<rectangle x1="12.91081875" y1="7.57758125" x2="13.335" y2="7.66241875" layer="94"/>
<rectangle x1="15.02918125" y1="7.57758125" x2="16.21281875" y2="7.66241875" layer="94"/>
<rectangle x1="17.48281875" y1="7.57758125" x2="19.939" y2="7.66241875" layer="94"/>
<rectangle x1="0.04318125" y1="7.66241875" x2="0.71881875" y2="7.747" layer="94"/>
<rectangle x1="2.413" y1="7.66241875" x2="3.00481875" y2="7.747" layer="94"/>
<rectangle x1="5.12318125" y1="7.66241875" x2="5.715" y2="7.747" layer="94"/>
<rectangle x1="7.66318125" y1="7.66241875" x2="8.255" y2="7.747" layer="94"/>
<rectangle x1="9.017" y1="7.66241875" x2="9.779" y2="7.747" layer="94"/>
<rectangle x1="10.287" y1="7.66241875" x2="12.319" y2="7.747" layer="94"/>
<rectangle x1="12.827" y1="7.66241875" x2="13.335" y2="7.747" layer="94"/>
<rectangle x1="13.67281875" y1="7.66241875" x2="13.843" y2="7.747" layer="94"/>
<rectangle x1="14.859" y1="7.66241875" x2="16.29918125" y2="7.747" layer="94"/>
<rectangle x1="17.31518125" y1="7.66241875" x2="19.939" y2="7.747" layer="94"/>
<rectangle x1="0.127" y1="7.747" x2="0.71881875" y2="7.83158125" layer="94"/>
<rectangle x1="2.413" y1="7.747" x2="3.00481875" y2="7.83158125" layer="94"/>
<rectangle x1="5.12318125" y1="7.747" x2="5.715" y2="7.83158125" layer="94"/>
<rectangle x1="7.57681875" y1="7.747" x2="8.255" y2="7.83158125" layer="94"/>
<rectangle x1="9.017" y1="7.747" x2="9.779" y2="7.83158125" layer="94"/>
<rectangle x1="10.287" y1="7.747" x2="12.319" y2="7.83158125" layer="94"/>
<rectangle x1="12.827" y1="7.747" x2="13.335" y2="7.83158125" layer="94"/>
<rectangle x1="13.589" y1="7.747" x2="14.097" y2="7.83158125" layer="94"/>
<rectangle x1="14.68881875" y1="7.747" x2="16.55318125" y2="7.83158125" layer="94"/>
<rectangle x1="17.06118125" y1="7.747" x2="19.939" y2="7.83158125" layer="94"/>
<rectangle x1="0.127" y1="7.83158125" x2="0.71881875" y2="7.91641875" layer="94"/>
<rectangle x1="2.413" y1="7.83158125" x2="3.00481875" y2="7.91641875" layer="94"/>
<rectangle x1="5.12318125" y1="7.83158125" x2="5.715" y2="7.91641875" layer="94"/>
<rectangle x1="7.57681875" y1="7.83158125" x2="8.255" y2="7.91641875" layer="94"/>
<rectangle x1="9.017" y1="7.83158125" x2="9.779" y2="7.91641875" layer="94"/>
<rectangle x1="10.37081875" y1="7.83158125" x2="12.23518125" y2="7.91641875" layer="94"/>
<rectangle x1="12.827" y1="7.83158125" x2="19.939" y2="7.91641875" layer="94"/>
<rectangle x1="0.127" y1="7.91641875" x2="0.71881875" y2="8.001" layer="94"/>
<rectangle x1="2.413" y1="7.91641875" x2="3.00481875" y2="8.001" layer="94"/>
<rectangle x1="5.12318125" y1="7.91641875" x2="5.715" y2="8.001" layer="94"/>
<rectangle x1="7.493" y1="7.91641875" x2="8.17118125" y2="8.001" layer="94"/>
<rectangle x1="9.017" y1="7.91641875" x2="9.86281875" y2="8.001" layer="94"/>
<rectangle x1="10.45718125" y1="7.91641875" x2="12.14881875" y2="8.001" layer="94"/>
<rectangle x1="12.74318125" y1="7.91641875" x2="19.939" y2="8.001" layer="94"/>
<rectangle x1="0.127" y1="8.001" x2="0.71881875" y2="8.08558125" layer="94"/>
<rectangle x1="2.413" y1="8.001" x2="3.00481875" y2="8.08558125" layer="94"/>
<rectangle x1="5.12318125" y1="8.001" x2="5.715" y2="8.08558125" layer="94"/>
<rectangle x1="7.40918125" y1="8.001" x2="8.17118125" y2="8.08558125" layer="94"/>
<rectangle x1="9.017" y1="8.001" x2="9.86281875" y2="8.08558125" layer="94"/>
<rectangle x1="10.45718125" y1="8.001" x2="12.065" y2="8.08558125" layer="94"/>
<rectangle x1="12.74318125" y1="8.001" x2="19.939" y2="8.08558125" layer="94"/>
<rectangle x1="0.127" y1="8.08558125" x2="0.80518125" y2="8.17041875" layer="94"/>
<rectangle x1="2.413" y1="8.08558125" x2="3.00481875" y2="8.17041875" layer="94"/>
<rectangle x1="5.12318125" y1="8.08558125" x2="5.715" y2="8.17041875" layer="94"/>
<rectangle x1="7.32281875" y1="8.08558125" x2="8.08481875" y2="8.17041875" layer="94"/>
<rectangle x1="9.017" y1="8.08558125" x2="9.94918125" y2="8.17041875" layer="94"/>
<rectangle x1="10.62481875" y1="8.08558125" x2="11.98118125" y2="8.17041875" layer="94"/>
<rectangle x1="12.65681875" y1="8.08558125" x2="19.939" y2="8.17041875" layer="94"/>
<rectangle x1="0.127" y1="8.17041875" x2="0.80518125" y2="8.255" layer="94"/>
<rectangle x1="2.413" y1="8.17041875" x2="3.00481875" y2="8.255" layer="94"/>
<rectangle x1="5.12318125" y1="8.17041875" x2="5.715" y2="8.255" layer="94"/>
<rectangle x1="7.15518125" y1="8.17041875" x2="8.001" y2="8.255" layer="94"/>
<rectangle x1="9.017" y1="8.17041875" x2="9.94918125" y2="8.255" layer="94"/>
<rectangle x1="10.71118125" y1="8.17041875" x2="11.89481875" y2="8.255" layer="94"/>
<rectangle x1="12.573" y1="8.17041875" x2="19.939" y2="8.255" layer="94"/>
<rectangle x1="0.21081875" y1="8.255" x2="0.80518125" y2="8.33958125" layer="94"/>
<rectangle x1="2.413" y1="8.255" x2="3.00481875" y2="8.33958125" layer="94"/>
<rectangle x1="5.12318125" y1="8.255" x2="5.715" y2="8.33958125" layer="94"/>
<rectangle x1="6.90118125" y1="8.255" x2="8.001" y2="8.33958125" layer="94"/>
<rectangle x1="9.017" y1="8.255" x2="10.033" y2="8.33958125" layer="94"/>
<rectangle x1="10.87881875" y1="8.255" x2="11.72718125" y2="8.33958125" layer="94"/>
<rectangle x1="12.573" y1="8.255" x2="19.939" y2="8.33958125" layer="94"/>
<rectangle x1="0.21081875" y1="8.33958125" x2="0.889" y2="8.42441875" layer="94"/>
<rectangle x1="2.413" y1="8.33958125" x2="3.00481875" y2="8.42441875" layer="94"/>
<rectangle x1="5.12318125" y1="8.33958125" x2="7.91718125" y2="8.42441875" layer="94"/>
<rectangle x1="9.10081875" y1="8.33958125" x2="10.11681875" y2="8.42441875" layer="94"/>
<rectangle x1="11.21918125" y1="8.33958125" x2="11.38681875" y2="8.42441875" layer="94"/>
<rectangle x1="12.48918125" y1="8.33958125" x2="19.85518125" y2="8.42441875" layer="94"/>
<rectangle x1="0.21081875" y1="8.42441875" x2="0.889" y2="8.509" layer="94"/>
<rectangle x1="2.413" y1="8.42441875" x2="3.00481875" y2="8.509" layer="94"/>
<rectangle x1="5.12318125" y1="8.42441875" x2="7.83081875" y2="8.509" layer="94"/>
<rectangle x1="9.10081875" y1="8.42441875" x2="10.20318125" y2="8.509" layer="94"/>
<rectangle x1="12.40281875" y1="8.42441875" x2="19.85518125" y2="8.509" layer="94"/>
<rectangle x1="0.29718125" y1="8.509" x2="0.889" y2="8.59358125" layer="94"/>
<rectangle x1="2.413" y1="8.509" x2="3.00481875" y2="8.59358125" layer="94"/>
<rectangle x1="5.12318125" y1="8.509" x2="7.747" y2="8.59358125" layer="94"/>
<rectangle x1="9.10081875" y1="8.509" x2="10.287" y2="8.59358125" layer="94"/>
<rectangle x1="12.23518125" y1="8.509" x2="19.85518125" y2="8.59358125" layer="94"/>
<rectangle x1="0.29718125" y1="8.59358125" x2="0.97281875" y2="8.67841875" layer="94"/>
<rectangle x1="2.413" y1="8.59358125" x2="3.00481875" y2="8.67841875" layer="94"/>
<rectangle x1="5.12318125" y1="8.59358125" x2="7.57681875" y2="8.67841875" layer="94"/>
<rectangle x1="9.10081875" y1="8.59358125" x2="10.45718125" y2="8.67841875" layer="94"/>
<rectangle x1="12.14881875" y1="8.59358125" x2="19.76881875" y2="8.67841875" layer="94"/>
<rectangle x1="0.29718125" y1="8.67841875" x2="0.97281875" y2="8.763" layer="94"/>
<rectangle x1="2.413" y1="8.67841875" x2="3.00481875" y2="8.763" layer="94"/>
<rectangle x1="5.12318125" y1="8.67841875" x2="7.493" y2="8.763" layer="94"/>
<rectangle x1="9.10081875" y1="8.67841875" x2="10.62481875" y2="8.763" layer="94"/>
<rectangle x1="11.98118125" y1="8.67841875" x2="19.76881875" y2="8.763" layer="94"/>
<rectangle x1="0.381" y1="8.763" x2="1.05918125" y2="8.84758125" layer="94"/>
<rectangle x1="2.413" y1="8.763" x2="2.921" y2="8.84758125" layer="94"/>
<rectangle x1="5.207" y1="8.763" x2="7.32281875" y2="8.84758125" layer="94"/>
<rectangle x1="9.10081875" y1="8.763" x2="10.795" y2="8.84758125" layer="94"/>
<rectangle x1="11.72718125" y1="8.763" x2="19.76881875" y2="8.84758125" layer="94"/>
<rectangle x1="0.381" y1="8.84758125" x2="1.05918125" y2="8.93241875" layer="94"/>
<rectangle x1="2.58318125" y1="8.84758125" x2="2.83718125" y2="8.93241875" layer="94"/>
<rectangle x1="5.29081875" y1="8.84758125" x2="7.06881875" y2="8.93241875" layer="94"/>
<rectangle x1="9.18718125" y1="8.84758125" x2="19.685" y2="8.93241875" layer="94"/>
<rectangle x1="0.46481875" y1="8.93241875" x2="1.143" y2="9.017" layer="94"/>
<rectangle x1="9.18718125" y1="8.93241875" x2="19.685" y2="9.017" layer="94"/>
<rectangle x1="0.46481875" y1="9.017" x2="1.143" y2="9.10158125" layer="94"/>
<rectangle x1="9.18718125" y1="9.017" x2="19.60118125" y2="9.10158125" layer="94"/>
<rectangle x1="0.55118125" y1="9.10158125" x2="1.22681875" y2="9.18641875" layer="94"/>
<rectangle x1="9.18718125" y1="9.10158125" x2="19.60118125" y2="9.18641875" layer="94"/>
<rectangle x1="0.55118125" y1="9.18641875" x2="1.31318125" y2="9.271" layer="94"/>
<rectangle x1="9.18718125" y1="9.18641875" x2="19.51481875" y2="9.271" layer="94"/>
<rectangle x1="0.635" y1="9.271" x2="1.397" y2="9.35558125" layer="94"/>
<rectangle x1="9.271" y1="9.271" x2="19.51481875" y2="9.35558125" layer="94"/>
<rectangle x1="0.71881875" y1="9.35558125" x2="1.397" y2="9.44041875" layer="94"/>
<rectangle x1="9.271" y1="9.35558125" x2="19.431" y2="9.44041875" layer="94"/>
<rectangle x1="0.71881875" y1="9.44041875" x2="1.48081875" y2="9.525" layer="94"/>
<rectangle x1="9.271" y1="9.44041875" x2="19.34718125" y2="9.525" layer="94"/>
<rectangle x1="0.80518125" y1="9.525" x2="1.56718125" y2="9.60958125" layer="94"/>
<rectangle x1="9.35481875" y1="9.525" x2="19.34718125" y2="9.60958125" layer="94"/>
<rectangle x1="0.889" y1="9.60958125" x2="1.651" y2="9.69441875" layer="94"/>
<rectangle x1="9.35481875" y1="9.60958125" x2="19.26081875" y2="9.69441875" layer="94"/>
<rectangle x1="0.889" y1="9.69441875" x2="1.73481875" y2="9.779" layer="94"/>
<rectangle x1="9.35481875" y1="9.69441875" x2="19.177" y2="9.779" layer="94"/>
<rectangle x1="0.97281875" y1="9.779" x2="1.82118125" y2="9.86358125" layer="94"/>
<rectangle x1="9.44118125" y1="9.779" x2="19.09318125" y2="9.86358125" layer="94"/>
<rectangle x1="1.05918125" y1="9.86358125" x2="1.905" y2="9.94841875" layer="94"/>
<rectangle x1="9.44118125" y1="9.86358125" x2="19.00681875" y2="9.94841875" layer="94"/>
<rectangle x1="1.143" y1="9.94841875" x2="2.07518125" y2="10.033" layer="94"/>
<rectangle x1="9.44118125" y1="9.94841875" x2="18.923" y2="10.033" layer="94"/>
<rectangle x1="1.22681875" y1="10.033" x2="2.159" y2="10.11758125" layer="94"/>
<rectangle x1="9.525" y1="10.033" x2="18.923" y2="10.11758125" layer="94"/>
<rectangle x1="1.31318125" y1="10.11758125" x2="2.24281875" y2="10.20241875" layer="94"/>
<rectangle x1="9.525" y1="10.11758125" x2="18.75281875" y2="10.20241875" layer="94"/>
<rectangle x1="1.397" y1="10.20241875" x2="2.413" y2="10.287" layer="94"/>
<rectangle x1="9.60881875" y1="10.20241875" x2="18.669" y2="10.287" layer="94"/>
<rectangle x1="1.48081875" y1="10.287" x2="2.58318125" y2="10.37158125" layer="94"/>
<rectangle x1="9.60881875" y1="10.287" x2="18.58518125" y2="10.37158125" layer="94"/>
<rectangle x1="1.56718125" y1="10.37158125" x2="2.75081875" y2="10.45641875" layer="94"/>
<rectangle x1="9.69518125" y1="10.37158125" x2="18.49881875" y2="10.45641875" layer="94"/>
<rectangle x1="1.73481875" y1="10.45641875" x2="3.00481875" y2="10.541" layer="94"/>
<rectangle x1="9.69518125" y1="10.45641875" x2="18.415" y2="10.541" layer="94"/>
<rectangle x1="1.82118125" y1="10.541" x2="3.34518125" y2="10.62558125" layer="94"/>
<rectangle x1="9.779" y1="10.541" x2="18.24481875" y2="10.62558125" layer="94"/>
<rectangle x1="1.98881875" y1="10.62558125" x2="9.69518125" y2="10.71041875" layer="94"/>
<rectangle x1="9.779" y1="10.62558125" x2="18.161" y2="10.71041875" layer="94"/>
<rectangle x1="2.07518125" y1="10.71041875" x2="17.99081875" y2="10.795" layer="94"/>
<rectangle x1="2.24281875" y1="10.795" x2="17.82318125" y2="10.87958125" layer="94"/>
<rectangle x1="2.413" y1="10.87958125" x2="17.653" y2="10.96441875" layer="94"/>
<rectangle x1="2.58318125" y1="10.96441875" x2="17.48281875" y2="11.049" layer="94"/>
<rectangle x1="2.83718125" y1="11.049" x2="17.22881875" y2="11.13358125" layer="94"/>
<rectangle x1="3.175" y1="11.13358125" x2="16.891" y2="11.21841875" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="UDO-LOGO-" prefix="LOGO" uservalue="yes">
<gates>
<gate name="LOGO" symbol="UDO-LOGO-20MM" x="0" y="0"/>
</gates>
<devices>
<device name="10MM" package="UDO-LOGO-10MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6649258/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="12MM" package="UDO-LOGO-12MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6649257/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="15MM" package="UDO-LOGO-15MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6649256/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="20MM" package="UDO-LOGO-20MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="30MM" package="UDO-LOGO-30MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="40MM" package="UDO-LOGO-40MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="blebox-dummy">
<packages>
<package name="EXTRA-BOM-LINE">
<text x="0" y="0" size="1.778" layer="48">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="48">&gt;VALUE</text>
<circle x="-2.54" y="0" radius="2.300065625" width="0.127" layer="48"/>
<wire x1="-4.064" y1="0" x2="-1.016" y2="0" width="0.127" layer="48"/>
<wire x1="-2.54" y1="1.524" x2="-2.54" y2="-1.27" width="0.127" layer="48"/>
</package>
</packages>
<symbols>
<symbol name="BOM">
<text x="2.54" y="2.54" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="2.54" y="0" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<circle x="0" y="0" radius="2.0478125" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="-2.54" size="1.778" layer="97" align="center-left">&gt;DESCRIPTION</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BOM" prefix="X" uservalue="yes">
<description>Dummy symbol for inserting extra BOM lines.</description>
<gates>
<gate name="X" symbol="BOM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EXTRA-BOM-LINE">
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="diffpair" width="0.4064" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="X4" library="avx-idc" deviceset="00917?002*" device="6" package3d_urn="urn:adsk.eagle:package:6228045/2" technology="001006"/>
<part name="X10" library="avx-idc" deviceset="00917500270*" device="" package3d_urn="urn:adsk.eagle:package:6339550/3" technology="2996"/>
<part name="X11" library="avx-idc" deviceset="00917500270*" device="" package3d_urn="urn:adsk.eagle:package:6339550/3" technology="2996"/>
<part name="X5" library="avx-idc" deviceset="00917500270*" device="" package3d_urn="urn:adsk.eagle:package:6339550/3" technology="2996"/>
<part name="X6" library="avx-idc" deviceset="00917500270*" device="" package3d_urn="urn:adsk.eagle:package:6339550/3" technology="2996"/>
<part name="AGND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="AGND" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="X7" library="avx-idc" deviceset="00917?002*" device="6" package3d_urn="urn:adsk.eagle:package:6228045/2" technology="001006"/>
<part name="X3" library="avx-idc" deviceset="00917?003*" device="6" package3d_urn="urn:adsk.eagle:package:6228026/2" technology="001006"/>
<part name="LOGO1" library="udo-logo" deviceset="UDO-LOGO-" device="12MM" package3d_urn="urn:adsk.eagle:package:6649257/3"/>
<part name="X28" library="blebox-dummy" deviceset="BOM" device="" value="609176002021099">
<attribute name="DESCRIPTION" value="9176 cap, 2-way, wire stop, AWG 20-18"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="609176002021099"/>
</part>
<part name="X30" library="blebox-dummy" deviceset="BOM" device="" value="609176003021099">
<attribute name="DESCRIPTION" value="9176 cap, 3-way, wire stop, AWG 20-18"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="609176003021099"/>
</part>
<part name="X1" library="blebox-dummy" deviceset="BOM" device="" value="609176002021099">
<attribute name="DESCRIPTION" value="9176 cap, 2-way, wire stop, AWG 20-18"/>
<attribute name="MF" value="AVX"/>
<attribute name="MPN" value="609176002021099"/>
</part>
</parts>
<sheets>
<sheet>
<plain>
<text x="142.24" y="121.92" size="1.778" layer="100" align="top-left">2x heater
Wiring: TBD (AWG26 default)</text>
<text x="142.24" y="101.6" size="1.778" layer="100" align="top-left">2x thermocouple
Wiring: TBD (1mm^2 / AWG18 default)</text>
<text x="259.08" y="12.7" size="2.54" layer="94" align="center-left">A1</text>
<text x="20.32" y="116.84" size="1.778" layer="97" align="top-left">HEATER cable
3x AWG18</text>
<text x="20.32" y="101.6" size="1.778" layer="97" align="top-left">TC cable
4x AWG26 STP</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME1" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="X4" gate="G$1" x="111.76" y="121.92" smashed="yes">
<attribute name="NAME" x="116.84" y="121.92" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="116.84" y="119.38" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="X10" gate="X" x="71.12" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="66.04" y="93.98" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="66.04" y="96.52" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="X11" gate="X" x="71.12" y="99.06" smashed="yes" rot="R180">
<attribute name="NAME" x="66.04" y="99.06" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="66.04" y="101.6" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="X5" gate="X" x="111.76" y="101.6" smashed="yes">
<attribute name="NAME" x="116.84" y="101.6" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="116.84" y="99.06" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="X6" gate="X" x="111.76" y="96.52" smashed="yes">
<attribute name="NAME" x="116.84" y="96.52" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="116.84" y="93.98" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="AGND1" gate="VR1" x="73.66" y="86.36" smashed="yes">
<attribute name="VALUE" x="71.12" y="81.28" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND1" gate="1" x="76.2" y="106.68" smashed="yes">
<attribute name="VALUE" x="73.66" y="104.14" size="1.778" layer="96"/>
</instance>
<instance part="X7" gate="G$1" x="111.76" y="114.3" smashed="yes">
<attribute name="NAME" x="116.84" y="114.3" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="116.84" y="111.76" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="X3" gate="G$1" x="71.12" y="116.84" smashed="yes" rot="MR0">
<attribute name="NAME" x="66.04" y="116.84" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="66.04" y="114.3" size="1.778" layer="96" rot="MR0" align="center-left"/>
</instance>
<instance part="LOGO1" gate="LOGO" x="243.332" y="23.368" smashed="yes"/>
<instance part="X28" gate="X" x="111.76" y="142.24" smashed="yes">
<attribute name="NAME" x="114.3" y="144.78" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="114.3" y="142.24" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="114.3" y="139.7" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="111.76" y="142.24" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="111.76" y="142.24" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X30" gate="X" x="68.58" y="124.46" smashed="yes" rot="MR0">
<attribute name="NAME" x="66.04" y="127" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="66.04" y="124.46" size="1.778" layer="96" rot="MR0" align="center-left"/>
<attribute name="DESCRIPTION" x="66.04" y="121.92" size="1.778" layer="97" rot="MR0" align="center-left"/>
<attribute name="MF" x="68.58" y="124.46" size="1.27" layer="96" rot="MR0" display="off"/>
<attribute name="MPN" x="68.58" y="124.46" size="1.27" layer="96" rot="MR0" display="off"/>
</instance>
<instance part="X1" gate="X" x="111.76" y="132.08" smashed="yes">
<attribute name="NAME" x="114.3" y="134.62" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="114.3" y="132.08" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="114.3" y="129.54" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="111.76" y="132.08" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="111.76" y="132.08" size="1.27" layer="96" display="off"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="TC1_N" class="1">
<segment>
<label x="76.2" y="99.06" size="1.27" layer="95"/>
<wire x1="73.66" y1="99.06" x2="109.22" y2="99.06" width="0.1524" layer="91"/>
<pinref part="X5" gate="X" pin="2"/>
<pinref part="X11" gate="X" pin="1"/>
</segment>
</net>
<net name="TC1_P" class="1">
<segment>
<label x="76.2" y="101.6" size="1.27" layer="95"/>
<wire x1="73.66" y1="101.6" x2="109.22" y2="101.6" width="0.1524" layer="91"/>
<pinref part="X5" gate="X" pin="1"/>
<pinref part="X11" gate="X" pin="2"/>
</segment>
</net>
<net name="TC2_P" class="1">
<segment>
<label x="76.2" y="96.52" size="1.27" layer="95"/>
<wire x1="73.66" y1="96.52" x2="109.22" y2="96.52" width="0.1524" layer="91"/>
<pinref part="X6" gate="X" pin="1"/>
<pinref part="X10" gate="X" pin="2"/>
</segment>
</net>
<net name="TC2_N" class="1">
<segment>
<label x="76.2" y="93.98" size="1.27" layer="95"/>
<wire x1="73.66" y1="93.98" x2="109.22" y2="93.98" width="0.1524" layer="91"/>
<pinref part="X6" gate="X" pin="2"/>
<pinref part="X10" gate="X" pin="1"/>
</segment>
</net>
<net name="AGND" class="0">
<segment>
<pinref part="AGND1" gate="VR1" pin="AGND"/>
<wire x1="73.66" y1="88.9" x2="73.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="73.66" y1="91.44" x2="111.76" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HEAT1" class="0">
<segment>
<wire x1="73.66" y1="116.84" x2="101.6" y2="116.84" width="0.1524" layer="91"/>
<wire x1="101.6" y1="116.84" x2="101.6" y2="121.92" width="0.1524" layer="91"/>
<pinref part="X4" gate="G$1" pin="1"/>
<wire x1="101.6" y1="121.92" x2="109.22" y2="121.92" width="0.1524" layer="91"/>
<label x="76.2" y="116.84" size="1.27" layer="95"/>
<pinref part="X3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="HEAT2" class="0">
<segment>
<pinref part="X7" gate="G$1" pin="1"/>
<wire x1="73.66" y1="114.3" x2="109.22" y2="114.3" width="0.1524" layer="91"/>
<label x="76.2" y="114.3" size="1.27" layer="95"/>
<pinref part="X3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="73.66" y1="111.76" x2="76.2" y2="111.76" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="76.2" y1="111.76" x2="76.2" y2="109.22" width="0.1524" layer="91"/>
<pinref part="X7" gate="G$1" pin="2"/>
<wire x1="76.2" y1="111.76" x2="106.68" y2="111.76" width="0.1524" layer="91"/>
<junction x="76.2" y="111.76"/>
<wire x1="106.68" y1="111.76" x2="109.22" y2="111.76" width="0.1524" layer="91"/>
<wire x1="106.68" y1="111.76" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<junction x="106.68" y="111.76"/>
<pinref part="X4" gate="G$1" pin="2"/>
<wire x1="106.68" y1="119.38" x2="109.22" y2="119.38" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="3"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
