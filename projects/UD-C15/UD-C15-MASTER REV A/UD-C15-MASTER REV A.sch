<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.4.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="yes"/>
<layer number="100" name="Warning" color="59" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="udo-logo">
<packages>
<package name="UDO-LOGO-10MM" urn="urn:adsk.eagle:footprint:6649251/1">
<rectangle x1="1.397" y1="0.127" x2="1.82118125" y2="0.21158125" layer="21"/>
<rectangle x1="3.85318125" y1="0.127" x2="4.191" y2="0.21158125" layer="21"/>
<rectangle x1="4.953" y1="0.127" x2="5.03681875" y2="0.21158125" layer="21"/>
<rectangle x1="5.63118125" y1="0.127" x2="5.715" y2="0.21158125" layer="21"/>
<rectangle x1="6.223" y1="0.127" x2="6.30681875" y2="0.21158125" layer="21"/>
<rectangle x1="7.06881875" y1="0.127" x2="7.40918125" y2="0.21158125" layer="21"/>
<rectangle x1="8.84681875" y1="0.127" x2="8.93318125" y2="0.21158125" layer="21"/>
<rectangle x1="1.22681875" y1="0.21158125" x2="1.905" y2="0.29641875" layer="21"/>
<rectangle x1="2.159" y1="0.21158125" x2="2.32918125" y2="0.29641875" layer="21"/>
<rectangle x1="2.75081875" y1="0.21158125" x2="2.83718125" y2="0.29641875" layer="21"/>
<rectangle x1="2.921" y1="0.21158125" x2="3.09118125" y2="0.29641875" layer="21"/>
<rectangle x1="3.34518125" y1="0.21158125" x2="3.59918125" y2="0.29641875" layer="21"/>
<rectangle x1="3.683" y1="0.21158125" x2="4.36118125" y2="0.29641875" layer="21"/>
<rectangle x1="4.78281875" y1="0.21158125" x2="5.207" y2="0.29641875" layer="21"/>
<rectangle x1="5.461" y1="0.21158125" x2="5.79881875" y2="0.29641875" layer="21"/>
<rectangle x1="6.05281875" y1="0.21158125" x2="6.477" y2="0.29641875" layer="21"/>
<rectangle x1="6.90118125" y1="0.21158125" x2="7.57681875" y2="0.29641875" layer="21"/>
<rectangle x1="7.747" y1="0.21158125" x2="7.91718125" y2="0.29641875" layer="21"/>
<rectangle x1="8.33881875" y1="0.21158125" x2="8.509" y2="0.29641875" layer="21"/>
<rectangle x1="8.67918125" y1="0.21158125" x2="9.10081875" y2="0.29641875" layer="21"/>
<rectangle x1="1.22681875" y1="0.29641875" x2="1.397" y2="0.381" layer="21"/>
<rectangle x1="1.82118125" y1="0.29641875" x2="1.98881875" y2="0.381" layer="21"/>
<rectangle x1="2.159" y1="0.29641875" x2="2.32918125" y2="0.381" layer="21"/>
<rectangle x1="2.75081875" y1="0.29641875" x2="2.83718125" y2="0.381" layer="21"/>
<rectangle x1="2.921" y1="0.29641875" x2="3.09118125" y2="0.381" layer="21"/>
<rectangle x1="3.25881875" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="3.683" y1="0.29641875" x2="3.85318125" y2="0.381" layer="21"/>
<rectangle x1="4.27481875" y1="0.29641875" x2="4.445" y2="0.381" layer="21"/>
<rectangle x1="4.699" y1="0.29641875" x2="4.86918125" y2="0.381" layer="21"/>
<rectangle x1="5.12318125" y1="0.29641875" x2="5.29081875" y2="0.381" layer="21"/>
<rectangle x1="5.37718125" y1="0.29641875" x2="5.54481875" y2="0.381" layer="21"/>
<rectangle x1="5.715" y1="0.29641875" x2="5.88518125" y2="0.381" layer="21"/>
<rectangle x1="5.969" y1="0.29641875" x2="6.13918125" y2="0.381" layer="21"/>
<rectangle x1="6.39318125" y1="0.29641875" x2="6.56081875" y2="0.381" layer="21"/>
<rectangle x1="6.81481875" y1="0.29641875" x2="6.985" y2="0.381" layer="21"/>
<rectangle x1="7.493" y1="0.29641875" x2="7.66318125" y2="0.381" layer="21"/>
<rectangle x1="7.747" y1="0.29641875" x2="7.91718125" y2="0.381" layer="21"/>
<rectangle x1="8.33881875" y1="0.29641875" x2="8.509" y2="0.381" layer="21"/>
<rectangle x1="8.59281875" y1="0.29641875" x2="8.763" y2="0.381" layer="21"/>
<rectangle x1="8.93318125" y1="0.29641875" x2="9.18718125" y2="0.381" layer="21"/>
<rectangle x1="1.143" y1="0.381" x2="1.31318125" y2="0.46558125" layer="21"/>
<rectangle x1="1.905" y1="0.381" x2="2.07518125" y2="0.46558125" layer="21"/>
<rectangle x1="2.159" y1="0.381" x2="2.32918125" y2="0.46558125" layer="21"/>
<rectangle x1="2.75081875" y1="0.381" x2="2.83718125" y2="0.46558125" layer="21"/>
<rectangle x1="2.921" y1="0.381" x2="3.09118125" y2="0.46558125" layer="21"/>
<rectangle x1="3.175" y1="0.381" x2="3.34518125" y2="0.46558125" layer="21"/>
<rectangle x1="3.683" y1="0.381" x2="3.85318125" y2="0.46558125" layer="21"/>
<rectangle x1="4.36118125" y1="0.381" x2="4.52881875" y2="0.46558125" layer="21"/>
<rectangle x1="4.699" y1="0.381" x2="4.78281875" y2="0.46558125" layer="21"/>
<rectangle x1="5.207" y1="0.381" x2="5.37718125" y2="0.46558125" layer="21"/>
<rectangle x1="5.715" y1="0.381" x2="5.88518125" y2="0.46558125" layer="21"/>
<rectangle x1="5.969" y1="0.381" x2="6.05281875" y2="0.46558125" layer="21"/>
<rectangle x1="6.477" y1="0.381" x2="6.56081875" y2="0.46558125" layer="21"/>
<rectangle x1="6.81481875" y1="0.381" x2="6.90118125" y2="0.46558125" layer="21"/>
<rectangle x1="7.57681875" y1="0.381" x2="7.66318125" y2="0.46558125" layer="21"/>
<rectangle x1="7.747" y1="0.381" x2="7.91718125" y2="0.46558125" layer="21"/>
<rectangle x1="8.33881875" y1="0.381" x2="8.67918125" y2="0.46558125" layer="21"/>
<rectangle x1="9.017" y1="0.381" x2="9.18718125" y2="0.46558125" layer="21"/>
<rectangle x1="1.143" y1="0.46558125" x2="1.22681875" y2="0.55041875" layer="21"/>
<rectangle x1="1.905" y1="0.46558125" x2="2.07518125" y2="0.55041875" layer="21"/>
<rectangle x1="2.159" y1="0.46558125" x2="2.32918125" y2="0.55041875" layer="21"/>
<rectangle x1="2.75081875" y1="0.46558125" x2="2.83718125" y2="0.55041875" layer="21"/>
<rectangle x1="2.921" y1="0.46558125" x2="3.09118125" y2="0.55041875" layer="21"/>
<rectangle x1="3.175" y1="0.46558125" x2="3.34518125" y2="0.55041875" layer="21"/>
<rectangle x1="3.683" y1="0.46558125" x2="3.85318125" y2="0.55041875" layer="21"/>
<rectangle x1="4.445" y1="0.46558125" x2="4.78281875" y2="0.55041875" layer="21"/>
<rectangle x1="5.207" y1="0.46558125" x2="5.37718125" y2="0.55041875" layer="21"/>
<rectangle x1="5.54481875" y1="0.46558125" x2="5.79881875" y2="0.55041875" layer="21"/>
<rectangle x1="5.88518125" y1="0.46558125" x2="6.64718125" y2="0.55041875" layer="21"/>
<rectangle x1="6.731" y1="0.46558125" x2="6.90118125" y2="0.55041875" layer="21"/>
<rectangle x1="7.57681875" y1="0.46558125" x2="7.91718125" y2="0.55041875" layer="21"/>
<rectangle x1="8.33881875" y1="0.46558125" x2="9.18718125" y2="0.55041875" layer="21"/>
<rectangle x1="1.05918125" y1="0.55041875" x2="1.22681875" y2="0.635" layer="21"/>
<rectangle x1="1.98881875" y1="0.55041875" x2="2.07518125" y2="0.635" layer="21"/>
<rectangle x1="2.159" y1="0.55041875" x2="2.32918125" y2="0.635" layer="21"/>
<rectangle x1="2.75081875" y1="0.55041875" x2="2.83718125" y2="0.635" layer="21"/>
<rectangle x1="2.921" y1="0.55041875" x2="3.09118125" y2="0.635" layer="21"/>
<rectangle x1="3.175" y1="0.55041875" x2="3.34518125" y2="0.635" layer="21"/>
<rectangle x1="3.683" y1="0.55041875" x2="3.85318125" y2="0.635" layer="21"/>
<rectangle x1="4.445" y1="0.55041875" x2="4.78281875" y2="0.635" layer="21"/>
<rectangle x1="5.207" y1="0.55041875" x2="5.715" y2="0.635" layer="21"/>
<rectangle x1="5.88518125" y1="0.55041875" x2="6.05281875" y2="0.635" layer="21"/>
<rectangle x1="6.13918125" y1="0.55041875" x2="6.39318125" y2="0.635" layer="21"/>
<rectangle x1="6.477" y1="0.55041875" x2="6.64718125" y2="0.635" layer="21"/>
<rectangle x1="6.731" y1="0.55041875" x2="6.90118125" y2="0.635" layer="21"/>
<rectangle x1="7.57681875" y1="0.55041875" x2="7.91718125" y2="0.635" layer="21"/>
<rectangle x1="8.33881875" y1="0.55041875" x2="8.67918125" y2="0.635" layer="21"/>
<rectangle x1="8.763" y1="0.55041875" x2="9.18718125" y2="0.635" layer="21"/>
<rectangle x1="1.05918125" y1="0.635" x2="1.22681875" y2="0.71958125" layer="21"/>
<rectangle x1="1.98881875" y1="0.635" x2="2.07518125" y2="0.71958125" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.32918125" y2="0.71958125" layer="21"/>
<rectangle x1="2.667" y1="0.635" x2="2.83718125" y2="0.71958125" layer="21"/>
<rectangle x1="2.921" y1="0.635" x2="3.09118125" y2="0.71958125" layer="21"/>
<rectangle x1="3.175" y1="0.635" x2="3.34518125" y2="0.71958125" layer="21"/>
<rectangle x1="3.683" y1="0.635" x2="3.85318125" y2="0.71958125" layer="21"/>
<rectangle x1="4.445" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="4.699" y1="0.635" x2="4.86918125" y2="0.71958125" layer="21"/>
<rectangle x1="5.12318125" y1="0.635" x2="5.29081875" y2="0.71958125" layer="21"/>
<rectangle x1="5.37718125" y1="0.635" x2="5.54481875" y2="0.71958125" layer="21"/>
<rectangle x1="5.715" y1="0.635" x2="5.88518125" y2="0.71958125" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.13918125" y2="0.71958125" layer="21"/>
<rectangle x1="6.39318125" y1="0.635" x2="6.56081875" y2="0.71958125" layer="21"/>
<rectangle x1="6.731" y1="0.635" x2="6.90118125" y2="0.71958125" layer="21"/>
<rectangle x1="7.57681875" y1="0.635" x2="7.747" y2="0.71958125" layer="21"/>
<rectangle x1="7.83081875" y1="0.635" x2="8.001" y2="0.71958125" layer="21"/>
<rectangle x1="8.255" y1="0.635" x2="8.42518125" y2="0.71958125" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="8.67918125" y2="0.71958125" layer="21"/>
<rectangle x1="9.017" y1="0.635" x2="9.18718125" y2="0.71958125" layer="21"/>
<rectangle x1="1.05918125" y1="0.71958125" x2="1.22681875" y2="0.80441875" layer="21"/>
<rectangle x1="1.98881875" y1="0.71958125" x2="2.07518125" y2="0.80441875" layer="21"/>
<rectangle x1="2.159" y1="0.71958125" x2="2.75081875" y2="0.80441875" layer="21"/>
<rectangle x1="2.921" y1="0.71958125" x2="3.59918125" y2="0.80441875" layer="21"/>
<rectangle x1="3.683" y1="0.71958125" x2="3.85318125" y2="0.80441875" layer="21"/>
<rectangle x1="4.445" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="4.78281875" y1="0.71958125" x2="5.29081875" y2="0.80441875" layer="21"/>
<rectangle x1="5.37718125" y1="0.71958125" x2="5.88518125" y2="0.80441875" layer="21"/>
<rectangle x1="6.05281875" y1="0.71958125" x2="6.56081875" y2="0.80441875" layer="21"/>
<rectangle x1="6.731" y1="0.71958125" x2="6.90118125" y2="0.80441875" layer="21"/>
<rectangle x1="7.57681875" y1="0.71958125" x2="8.33881875" y2="0.80441875" layer="21"/>
<rectangle x1="8.59281875" y1="0.71958125" x2="9.10081875" y2="0.80441875" layer="21"/>
<rectangle x1="1.05918125" y1="0.80441875" x2="1.22681875" y2="0.889" layer="21"/>
<rectangle x1="1.98881875" y1="0.80441875" x2="2.07518125" y2="0.889" layer="21"/>
<rectangle x1="2.159" y1="0.80441875" x2="2.24281875" y2="0.889" layer="21"/>
<rectangle x1="2.413" y1="0.80441875" x2="2.667" y2="0.889" layer="21"/>
<rectangle x1="2.921" y1="0.80441875" x2="3.00481875" y2="0.889" layer="21"/>
<rectangle x1="3.09118125" y1="0.80441875" x2="3.59918125" y2="0.889" layer="21"/>
<rectangle x1="3.683" y1="0.80441875" x2="3.85318125" y2="0.889" layer="21"/>
<rectangle x1="4.445" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="4.86918125" y1="0.80441875" x2="5.12318125" y2="0.889" layer="21"/>
<rectangle x1="5.54481875" y1="0.80441875" x2="5.715" y2="0.889" layer="21"/>
<rectangle x1="6.13918125" y1="0.80441875" x2="6.39318125" y2="0.889" layer="21"/>
<rectangle x1="6.81481875" y1="0.80441875" x2="6.90118125" y2="0.889" layer="21"/>
<rectangle x1="7.57681875" y1="0.80441875" x2="7.66318125" y2="0.889" layer="21"/>
<rectangle x1="8.001" y1="0.80441875" x2="8.255" y2="0.889" layer="21"/>
<rectangle x1="8.67918125" y1="0.80441875" x2="9.017" y2="0.889" layer="21"/>
<rectangle x1="1.05918125" y1="0.889" x2="1.22681875" y2="0.97358125" layer="21"/>
<rectangle x1="1.98881875" y1="0.889" x2="2.07518125" y2="0.97358125" layer="21"/>
<rectangle x1="2.921" y1="0.889" x2="3.09118125" y2="0.97358125" layer="21"/>
<rectangle x1="3.175" y1="0.889" x2="3.34518125" y2="0.97358125" layer="21"/>
<rectangle x1="3.683" y1="0.889" x2="3.85318125" y2="0.97358125" layer="21"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.52881875" y2="0.97358125" layer="21"/>
<rectangle x1="6.81481875" y1="0.889" x2="6.985" y2="0.97358125" layer="21"/>
<rectangle x1="7.493" y1="0.889" x2="7.66318125" y2="0.97358125" layer="21"/>
<rectangle x1="1.05918125" y1="0.97358125" x2="1.22681875" y2="1.05841875" layer="21"/>
<rectangle x1="1.98881875" y1="0.97358125" x2="2.07518125" y2="1.05841875" layer="21"/>
<rectangle x1="2.921" y1="0.97358125" x2="3.00481875" y2="1.05841875" layer="21"/>
<rectangle x1="3.175" y1="0.97358125" x2="3.34518125" y2="1.05841875" layer="21"/>
<rectangle x1="3.683" y1="0.97358125" x2="3.85318125" y2="1.05841875" layer="21"/>
<rectangle x1="4.27481875" y1="0.97358125" x2="4.445" y2="1.05841875" layer="21"/>
<rectangle x1="6.90118125" y1="0.97358125" x2="7.57681875" y2="1.05841875" layer="21"/>
<rectangle x1="1.05918125" y1="1.05841875" x2="1.22681875" y2="1.143" layer="21"/>
<rectangle x1="1.98881875" y1="1.05841875" x2="2.07518125" y2="1.143" layer="21"/>
<rectangle x1="3.175" y1="1.05841875" x2="3.34518125" y2="1.143" layer="21"/>
<rectangle x1="3.683" y1="1.05841875" x2="4.36118125" y2="1.143" layer="21"/>
<rectangle x1="7.06881875" y1="1.05841875" x2="7.40918125" y2="1.143" layer="21"/>
<rectangle x1="3.76681875" y1="1.143" x2="4.191" y2="1.22758125" layer="21"/>
<rectangle x1="1.82118125" y1="1.56641875" x2="8.255" y2="1.651" layer="21"/>
<rectangle x1="1.397" y1="1.651" x2="8.59281875" y2="1.73558125" layer="21"/>
<rectangle x1="1.143" y1="1.73558125" x2="8.84681875" y2="1.82041875" layer="21"/>
<rectangle x1="1.05918125" y1="1.82041875" x2="9.017" y2="1.905" layer="21"/>
<rectangle x1="0.889" y1="1.905" x2="1.651" y2="1.98958125" layer="21"/>
<rectangle x1="4.86918125" y1="1.905" x2="9.10081875" y2="1.98958125" layer="21"/>
<rectangle x1="0.80518125" y1="1.98958125" x2="1.397" y2="2.07441875" layer="21"/>
<rectangle x1="4.78281875" y1="1.98958125" x2="9.271" y2="2.07441875" layer="21"/>
<rectangle x1="0.635" y1="2.07441875" x2="1.22681875" y2="2.159" layer="21"/>
<rectangle x1="4.78281875" y1="2.07441875" x2="9.35481875" y2="2.159" layer="21"/>
<rectangle x1="0.55118125" y1="2.159" x2="1.05918125" y2="2.24358125" layer="21"/>
<rectangle x1="4.699" y1="2.159" x2="9.44118125" y2="2.24358125" layer="21"/>
<rectangle x1="0.46481875" y1="2.24358125" x2="0.97281875" y2="2.32841875" layer="21"/>
<rectangle x1="4.699" y1="2.24358125" x2="9.525" y2="2.32841875" layer="21"/>
<rectangle x1="0.46481875" y1="2.32841875" x2="0.80518125" y2="2.413" layer="21"/>
<rectangle x1="4.699" y1="2.32841875" x2="9.60881875" y2="2.413" layer="21"/>
<rectangle x1="0.381" y1="2.413" x2="0.71881875" y2="2.49758125" layer="21"/>
<rectangle x1="4.61518125" y1="2.413" x2="9.60881875" y2="2.49758125" layer="21"/>
<rectangle x1="0.29718125" y1="2.49758125" x2="0.71881875" y2="2.58241875" layer="21"/>
<rectangle x1="4.61518125" y1="2.49758125" x2="9.69518125" y2="2.58241875" layer="21"/>
<rectangle x1="0.29718125" y1="2.58241875" x2="0.635" y2="2.667" layer="21"/>
<rectangle x1="4.61518125" y1="2.58241875" x2="9.779" y2="2.667" layer="21"/>
<rectangle x1="0.21081875" y1="2.667" x2="0.55118125" y2="2.75158125" layer="21"/>
<rectangle x1="4.52881875" y1="2.667" x2="9.779" y2="2.75158125" layer="21"/>
<rectangle x1="0.21081875" y1="2.75158125" x2="0.55118125" y2="2.83641875" layer="21"/>
<rectangle x1="1.82118125" y1="2.75158125" x2="2.24281875" y2="2.83641875" layer="21"/>
<rectangle x1="2.921" y1="2.75158125" x2="3.51281875" y2="2.83641875" layer="21"/>
<rectangle x1="4.52881875" y1="2.75158125" x2="9.86281875" y2="2.83641875" layer="21"/>
<rectangle x1="0.127" y1="2.83641875" x2="0.46481875" y2="2.921" layer="21"/>
<rectangle x1="1.56718125" y1="2.83641875" x2="2.413" y2="2.921" layer="21"/>
<rectangle x1="2.83718125" y1="2.83641875" x2="3.683" y2="2.921" layer="21"/>
<rectangle x1="4.52881875" y1="2.83641875" x2="5.29081875" y2="2.921" layer="21"/>
<rectangle x1="5.969" y1="2.83641875" x2="6.64718125" y2="2.921" layer="21"/>
<rectangle x1="6.81481875" y1="2.83641875" x2="7.493" y2="2.921" layer="21"/>
<rectangle x1="7.66318125" y1="2.83641875" x2="8.17118125" y2="2.921" layer="21"/>
<rectangle x1="8.67918125" y1="2.83641875" x2="9.86281875" y2="2.921" layer="21"/>
<rectangle x1="0.127" y1="2.921" x2="0.46481875" y2="3.00558125" layer="21"/>
<rectangle x1="1.48081875" y1="2.921" x2="2.58318125" y2="3.00558125" layer="21"/>
<rectangle x1="2.83718125" y1="2.921" x2="3.85318125" y2="3.00558125" layer="21"/>
<rectangle x1="4.52881875" y1="2.921" x2="5.12318125" y2="3.00558125" layer="21"/>
<rectangle x1="6.13918125" y1="2.921" x2="6.64718125" y2="3.00558125" layer="21"/>
<rectangle x1="6.81481875" y1="2.921" x2="7.493" y2="3.00558125" layer="21"/>
<rectangle x1="7.747" y1="2.921" x2="8.001" y2="3.00558125" layer="21"/>
<rectangle x1="8.763" y1="2.921" x2="9.86281875" y2="3.00558125" layer="21"/>
<rectangle x1="0.127" y1="3.00558125" x2="0.381" y2="3.09041875" layer="21"/>
<rectangle x1="1.397" y1="3.00558125" x2="2.667" y2="3.09041875" layer="21"/>
<rectangle x1="2.921" y1="3.00558125" x2="3.937" y2="3.09041875" layer="21"/>
<rectangle x1="4.52881875" y1="3.00558125" x2="5.03681875" y2="3.09041875" layer="21"/>
<rectangle x1="5.54481875" y1="3.00558125" x2="5.715" y2="3.09041875" layer="21"/>
<rectangle x1="6.223" y1="3.00558125" x2="6.64718125" y2="3.09041875" layer="21"/>
<rectangle x1="6.81481875" y1="3.00558125" x2="7.493" y2="3.09041875" layer="21"/>
<rectangle x1="7.747" y1="3.00558125" x2="7.91718125" y2="3.09041875" layer="21"/>
<rectangle x1="8.255" y1="3.00558125" x2="8.59281875" y2="3.09041875" layer="21"/>
<rectangle x1="8.84681875" y1="3.00558125" x2="9.94918125" y2="3.09041875" layer="21"/>
<rectangle x1="0.04318125" y1="3.09041875" x2="0.381" y2="3.175" layer="21"/>
<rectangle x1="1.31318125" y1="3.09041875" x2="1.73481875" y2="3.175" layer="21"/>
<rectangle x1="2.24281875" y1="3.09041875" x2="2.667" y2="3.175" layer="21"/>
<rectangle x1="3.59918125" y1="3.09041875" x2="4.02081875" y2="3.175" layer="21"/>
<rectangle x1="4.52881875" y1="3.09041875" x2="4.953" y2="3.175" layer="21"/>
<rectangle x1="5.29081875" y1="3.09041875" x2="5.969" y2="3.175" layer="21"/>
<rectangle x1="6.30681875" y1="3.09041875" x2="6.64718125" y2="3.175" layer="21"/>
<rectangle x1="6.81481875" y1="3.09041875" x2="7.493" y2="3.175" layer="21"/>
<rectangle x1="7.747" y1="3.09041875" x2="7.91718125" y2="3.175" layer="21"/>
<rectangle x1="8.08481875" y1="3.09041875" x2="8.67918125" y2="3.175" layer="21"/>
<rectangle x1="8.84681875" y1="3.09041875" x2="9.94918125" y2="3.175" layer="21"/>
<rectangle x1="0.04318125" y1="3.175" x2="0.381" y2="3.25958125" layer="21"/>
<rectangle x1="1.31318125" y1="3.175" x2="1.651" y2="3.25958125" layer="21"/>
<rectangle x1="2.413" y1="3.175" x2="2.75081875" y2="3.25958125" layer="21"/>
<rectangle x1="3.683" y1="3.175" x2="4.02081875" y2="3.25958125" layer="21"/>
<rectangle x1="4.52881875" y1="3.175" x2="4.953" y2="3.25958125" layer="21"/>
<rectangle x1="5.207" y1="3.175" x2="6.05281875" y2="3.25958125" layer="21"/>
<rectangle x1="6.30681875" y1="3.175" x2="6.64718125" y2="3.25958125" layer="21"/>
<rectangle x1="6.81481875" y1="3.175" x2="7.493" y2="3.25958125" layer="21"/>
<rectangle x1="7.66318125" y1="3.175" x2="7.83081875" y2="3.25958125" layer="21"/>
<rectangle x1="8.08481875" y1="3.175" x2="9.94918125" y2="3.25958125" layer="21"/>
<rectangle x1="0.04318125" y1="3.25958125" x2="0.29718125" y2="3.34441875" layer="21"/>
<rectangle x1="1.22681875" y1="3.25958125" x2="1.56718125" y2="3.34441875" layer="21"/>
<rectangle x1="2.49681875" y1="3.25958125" x2="2.75081875" y2="3.34441875" layer="21"/>
<rectangle x1="3.76681875" y1="3.25958125" x2="4.10718125" y2="3.34441875" layer="21"/>
<rectangle x1="4.445" y1="3.25958125" x2="4.86918125" y2="3.34441875" layer="21"/>
<rectangle x1="5.12318125" y1="3.25958125" x2="6.13918125" y2="3.34441875" layer="21"/>
<rectangle x1="6.39318125" y1="3.25958125" x2="6.64718125" y2="3.34441875" layer="21"/>
<rectangle x1="6.81481875" y1="3.25958125" x2="7.493" y2="3.34441875" layer="21"/>
<rectangle x1="7.66318125" y1="3.25958125" x2="7.83081875" y2="3.34441875" layer="21"/>
<rectangle x1="8.93318125" y1="3.25958125" x2="9.94918125" y2="3.34441875" layer="21"/>
<rectangle x1="0.04318125" y1="3.34441875" x2="0.29718125" y2="3.429" layer="21"/>
<rectangle x1="1.22681875" y1="3.34441875" x2="1.48081875" y2="3.429" layer="21"/>
<rectangle x1="2.49681875" y1="3.34441875" x2="2.83718125" y2="3.429" layer="21"/>
<rectangle x1="3.76681875" y1="3.34441875" x2="4.10718125" y2="3.429" layer="21"/>
<rectangle x1="4.445" y1="3.34441875" x2="4.86918125" y2="3.429" layer="21"/>
<rectangle x1="5.12318125" y1="3.34441875" x2="6.13918125" y2="3.429" layer="21"/>
<rectangle x1="6.39318125" y1="3.34441875" x2="6.64718125" y2="3.429" layer="21"/>
<rectangle x1="6.81481875" y1="3.34441875" x2="7.493" y2="3.429" layer="21"/>
<rectangle x1="7.66318125" y1="3.34441875" x2="7.83081875" y2="3.429" layer="21"/>
<rectangle x1="8.93318125" y1="3.34441875" x2="9.94918125" y2="3.429" layer="21"/>
<rectangle x1="0.04318125" y1="3.429" x2="0.29718125" y2="3.51358125" layer="21"/>
<rectangle x1="1.143" y1="3.429" x2="1.48081875" y2="3.51358125" layer="21"/>
<rectangle x1="2.49681875" y1="3.429" x2="2.83718125" y2="3.51358125" layer="21"/>
<rectangle x1="3.85318125" y1="3.429" x2="4.10718125" y2="3.51358125" layer="21"/>
<rectangle x1="4.445" y1="3.429" x2="4.78281875" y2="3.51358125" layer="21"/>
<rectangle x1="5.03681875" y1="3.429" x2="6.13918125" y2="3.51358125" layer="21"/>
<rectangle x1="6.39318125" y1="3.429" x2="6.64718125" y2="3.51358125" layer="21"/>
<rectangle x1="6.81481875" y1="3.429" x2="7.493" y2="3.51358125" layer="21"/>
<rectangle x1="7.66318125" y1="3.429" x2="7.83081875" y2="3.51358125" layer="21"/>
<rectangle x1="8.08481875" y1="3.429" x2="8.67918125" y2="3.51358125" layer="21"/>
<rectangle x1="8.93318125" y1="3.429" x2="9.94918125" y2="3.51358125" layer="21"/>
<rectangle x1="0.04318125" y1="3.51358125" x2="0.29718125" y2="3.59841875" layer="21"/>
<rectangle x1="1.143" y1="3.51358125" x2="1.48081875" y2="3.59841875" layer="21"/>
<rectangle x1="2.58318125" y1="3.51358125" x2="2.83718125" y2="3.59841875" layer="21"/>
<rectangle x1="3.85318125" y1="3.51358125" x2="4.191" y2="3.59841875" layer="21"/>
<rectangle x1="4.445" y1="3.51358125" x2="4.78281875" y2="3.59841875" layer="21"/>
<rectangle x1="5.03681875" y1="3.51358125" x2="6.223" y2="3.59841875" layer="21"/>
<rectangle x1="6.477" y1="3.51358125" x2="6.64718125" y2="3.59841875" layer="21"/>
<rectangle x1="6.90118125" y1="3.51358125" x2="7.40918125" y2="3.59841875" layer="21"/>
<rectangle x1="7.66318125" y1="3.51358125" x2="7.91718125" y2="3.59841875" layer="21"/>
<rectangle x1="8.08481875" y1="3.51358125" x2="8.67918125" y2="3.59841875" layer="21"/>
<rectangle x1="8.84681875" y1="3.51358125" x2="9.94918125" y2="3.59841875" layer="21"/>
<rectangle x1="0.04318125" y1="3.59841875" x2="0.29718125" y2="3.683" layer="21"/>
<rectangle x1="1.143" y1="3.59841875" x2="1.48081875" y2="3.683" layer="21"/>
<rectangle x1="2.58318125" y1="3.59841875" x2="2.83718125" y2="3.683" layer="21"/>
<rectangle x1="3.85318125" y1="3.59841875" x2="4.191" y2="3.683" layer="21"/>
<rectangle x1="4.445" y1="3.59841875" x2="4.78281875" y2="3.683" layer="21"/>
<rectangle x1="5.03681875" y1="3.59841875" x2="6.223" y2="3.683" layer="21"/>
<rectangle x1="6.477" y1="3.59841875" x2="6.64718125" y2="3.683" layer="21"/>
<rectangle x1="6.985" y1="3.59841875" x2="7.32281875" y2="3.683" layer="21"/>
<rectangle x1="7.66318125" y1="3.59841875" x2="7.91718125" y2="3.683" layer="21"/>
<rectangle x1="8.17118125" y1="3.59841875" x2="8.59281875" y2="3.683" layer="21"/>
<rectangle x1="8.84681875" y1="3.59841875" x2="9.94918125" y2="3.683" layer="21"/>
<rectangle x1="0.04318125" y1="3.683" x2="0.29718125" y2="3.76758125" layer="21"/>
<rectangle x1="1.143" y1="3.683" x2="1.48081875" y2="3.76758125" layer="21"/>
<rectangle x1="2.58318125" y1="3.683" x2="2.83718125" y2="3.76758125" layer="21"/>
<rectangle x1="3.85318125" y1="3.683" x2="4.10718125" y2="3.76758125" layer="21"/>
<rectangle x1="4.445" y1="3.683" x2="4.78281875" y2="3.76758125" layer="21"/>
<rectangle x1="5.03681875" y1="3.683" x2="6.13918125" y2="3.76758125" layer="21"/>
<rectangle x1="6.39318125" y1="3.683" x2="6.64718125" y2="3.76758125" layer="21"/>
<rectangle x1="7.57681875" y1="3.683" x2="8.001" y2="3.76758125" layer="21"/>
<rectangle x1="8.763" y1="3.683" x2="9.94918125" y2="3.76758125" layer="21"/>
<rectangle x1="0.04318125" y1="3.76758125" x2="0.29718125" y2="3.85241875" layer="21"/>
<rectangle x1="1.143" y1="3.76758125" x2="1.48081875" y2="3.85241875" layer="21"/>
<rectangle x1="2.58318125" y1="3.76758125" x2="2.83718125" y2="3.85241875" layer="21"/>
<rectangle x1="3.85318125" y1="3.76758125" x2="4.10718125" y2="3.85241875" layer="21"/>
<rectangle x1="4.445" y1="3.76758125" x2="4.86918125" y2="3.85241875" layer="21"/>
<rectangle x1="5.12318125" y1="3.76758125" x2="6.13918125" y2="3.85241875" layer="21"/>
<rectangle x1="6.39318125" y1="3.76758125" x2="6.64718125" y2="3.85241875" layer="21"/>
<rectangle x1="6.81481875" y1="3.76758125" x2="6.90118125" y2="3.85241875" layer="21"/>
<rectangle x1="7.40918125" y1="3.76758125" x2="8.08481875" y2="3.85241875" layer="21"/>
<rectangle x1="8.67918125" y1="3.76758125" x2="9.94918125" y2="3.85241875" layer="21"/>
<rectangle x1="0.04318125" y1="3.85241875" x2="0.29718125" y2="3.937" layer="21"/>
<rectangle x1="1.143" y1="3.85241875" x2="1.48081875" y2="3.937" layer="21"/>
<rectangle x1="2.58318125" y1="3.85241875" x2="2.83718125" y2="3.937" layer="21"/>
<rectangle x1="3.76681875" y1="3.85241875" x2="4.10718125" y2="3.937" layer="21"/>
<rectangle x1="4.445" y1="3.85241875" x2="4.86918125" y2="3.937" layer="21"/>
<rectangle x1="5.12318125" y1="3.85241875" x2="6.13918125" y2="3.937" layer="21"/>
<rectangle x1="6.39318125" y1="3.85241875" x2="7.06881875" y2="3.937" layer="21"/>
<rectangle x1="7.239" y1="3.85241875" x2="8.33881875" y2="3.937" layer="21"/>
<rectangle x1="8.42518125" y1="3.85241875" x2="9.94918125" y2="3.937" layer="21"/>
<rectangle x1="0.04318125" y1="3.937" x2="0.381" y2="4.02158125" layer="21"/>
<rectangle x1="1.143" y1="3.937" x2="1.48081875" y2="4.02158125" layer="21"/>
<rectangle x1="2.58318125" y1="3.937" x2="2.83718125" y2="4.02158125" layer="21"/>
<rectangle x1="3.683" y1="3.937" x2="4.02081875" y2="4.02158125" layer="21"/>
<rectangle x1="4.52881875" y1="3.937" x2="4.86918125" y2="4.02158125" layer="21"/>
<rectangle x1="5.207" y1="3.937" x2="6.05281875" y2="4.02158125" layer="21"/>
<rectangle x1="6.30681875" y1="3.937" x2="9.94918125" y2="4.02158125" layer="21"/>
<rectangle x1="0.04318125" y1="4.02158125" x2="0.381" y2="4.10641875" layer="21"/>
<rectangle x1="1.143" y1="4.02158125" x2="1.48081875" y2="4.10641875" layer="21"/>
<rectangle x1="2.58318125" y1="4.02158125" x2="2.83718125" y2="4.10641875" layer="21"/>
<rectangle x1="3.59918125" y1="4.02158125" x2="4.02081875" y2="4.10641875" layer="21"/>
<rectangle x1="4.52881875" y1="4.02158125" x2="4.953" y2="4.10641875" layer="21"/>
<rectangle x1="5.29081875" y1="4.02158125" x2="5.969" y2="4.10641875" layer="21"/>
<rectangle x1="6.30681875" y1="4.02158125" x2="9.94918125" y2="4.10641875" layer="21"/>
<rectangle x1="0.04318125" y1="4.10641875" x2="0.381" y2="4.191" layer="21"/>
<rectangle x1="1.143" y1="4.10641875" x2="1.48081875" y2="4.191" layer="21"/>
<rectangle x1="2.58318125" y1="4.10641875" x2="3.937" y2="4.191" layer="21"/>
<rectangle x1="4.52881875" y1="4.10641875" x2="5.03681875" y2="4.191" layer="21"/>
<rectangle x1="5.461" y1="4.10641875" x2="5.79881875" y2="4.191" layer="21"/>
<rectangle x1="6.223" y1="4.10641875" x2="9.94918125" y2="4.191" layer="21"/>
<rectangle x1="0.127" y1="4.191" x2="0.46481875" y2="4.27558125" layer="21"/>
<rectangle x1="1.143" y1="4.191" x2="1.48081875" y2="4.27558125" layer="21"/>
<rectangle x1="2.58318125" y1="4.191" x2="3.85318125" y2="4.27558125" layer="21"/>
<rectangle x1="4.52881875" y1="4.191" x2="5.12318125" y2="4.27558125" layer="21"/>
<rectangle x1="6.13918125" y1="4.191" x2="9.86281875" y2="4.27558125" layer="21"/>
<rectangle x1="0.127" y1="4.27558125" x2="0.46481875" y2="4.36041875" layer="21"/>
<rectangle x1="1.143" y1="4.27558125" x2="1.48081875" y2="4.36041875" layer="21"/>
<rectangle x1="2.58318125" y1="4.27558125" x2="3.76681875" y2="4.36041875" layer="21"/>
<rectangle x1="4.52881875" y1="4.27558125" x2="5.207" y2="4.36041875" layer="21"/>
<rectangle x1="5.969" y1="4.27558125" x2="9.86281875" y2="4.36041875" layer="21"/>
<rectangle x1="0.21081875" y1="4.36041875" x2="0.46481875" y2="4.445" layer="21"/>
<rectangle x1="1.22681875" y1="4.36041875" x2="1.397" y2="4.445" layer="21"/>
<rectangle x1="2.58318125" y1="4.36041875" x2="3.59918125" y2="4.445" layer="21"/>
<rectangle x1="4.52881875" y1="4.36041875" x2="5.461" y2="4.445" layer="21"/>
<rectangle x1="5.79881875" y1="4.36041875" x2="9.86281875" y2="4.445" layer="21"/>
<rectangle x1="0.21081875" y1="4.445" x2="0.55118125" y2="4.52958125" layer="21"/>
<rectangle x1="4.52881875" y1="4.445" x2="9.779" y2="4.52958125" layer="21"/>
<rectangle x1="0.29718125" y1="4.52958125" x2="0.635" y2="4.61441875" layer="21"/>
<rectangle x1="4.61518125" y1="4.52958125" x2="9.779" y2="4.61441875" layer="21"/>
<rectangle x1="0.29718125" y1="4.61441875" x2="0.635" y2="4.699" layer="21"/>
<rectangle x1="4.61518125" y1="4.61441875" x2="9.69518125" y2="4.699" layer="21"/>
<rectangle x1="0.381" y1="4.699" x2="0.71881875" y2="4.78358125" layer="21"/>
<rectangle x1="4.61518125" y1="4.699" x2="9.60881875" y2="4.78358125" layer="21"/>
<rectangle x1="0.46481875" y1="4.78358125" x2="0.80518125" y2="4.86841875" layer="21"/>
<rectangle x1="4.699" y1="4.78358125" x2="9.60881875" y2="4.86841875" layer="21"/>
<rectangle x1="0.46481875" y1="4.86841875" x2="0.889" y2="4.953" layer="21"/>
<rectangle x1="4.699" y1="4.86841875" x2="9.525" y2="4.953" layer="21"/>
<rectangle x1="0.55118125" y1="4.953" x2="1.05918125" y2="5.03758125" layer="21"/>
<rectangle x1="4.699" y1="4.953" x2="9.44118125" y2="5.03758125" layer="21"/>
<rectangle x1="0.635" y1="5.03758125" x2="1.143" y2="5.12241875" layer="21"/>
<rectangle x1="4.78281875" y1="5.03758125" x2="9.35481875" y2="5.12241875" layer="21"/>
<rectangle x1="0.71881875" y1="5.12241875" x2="1.31318125" y2="5.207" layer="21"/>
<rectangle x1="4.78281875" y1="5.12241875" x2="9.271" y2="5.207" layer="21"/>
<rectangle x1="0.889" y1="5.207" x2="1.56718125" y2="5.29158125" layer="21"/>
<rectangle x1="4.86918125" y1="5.207" x2="9.18718125" y2="5.29158125" layer="21"/>
<rectangle x1="0.97281875" y1="5.29158125" x2="9.017" y2="5.37641875" layer="21"/>
<rectangle x1="1.143" y1="5.37641875" x2="8.84681875" y2="5.461" layer="21"/>
<rectangle x1="1.31318125" y1="5.461" x2="8.67918125" y2="5.54558125" layer="21"/>
<rectangle x1="1.651" y1="5.54558125" x2="8.33881875" y2="5.63041875" layer="21"/>
</package>
<package name="UDO-LOGO-12MM" urn="urn:adsk.eagle:footprint:6649250/1">
<rectangle x1="1.651" y1="0.21158125" x2="2.24281875" y2="0.29641875" layer="21"/>
<rectangle x1="2.667" y1="0.21158125" x2="2.75081875" y2="0.29641875" layer="21"/>
<rectangle x1="3.34518125" y1="0.21158125" x2="3.429" y2="0.29641875" layer="21"/>
<rectangle x1="4.10718125" y1="0.21158125" x2="4.27481875" y2="0.29641875" layer="21"/>
<rectangle x1="4.52881875" y1="0.21158125" x2="5.12318125" y2="0.29641875" layer="21"/>
<rectangle x1="5.88518125" y1="0.21158125" x2="6.13918125" y2="0.29641875" layer="21"/>
<rectangle x1="6.64718125" y1="0.21158125" x2="6.90118125" y2="0.29641875" layer="21"/>
<rectangle x1="7.40918125" y1="0.21158125" x2="7.66318125" y2="0.29641875" layer="21"/>
<rectangle x1="8.42518125" y1="0.21158125" x2="9.017" y2="0.29641875" layer="21"/>
<rectangle x1="10.541" y1="0.21158125" x2="10.795" y2="0.29641875" layer="21"/>
<rectangle x1="1.56718125" y1="0.29641875" x2="1.73481875" y2="0.381" layer="21"/>
<rectangle x1="2.07518125" y1="0.29641875" x2="2.32918125" y2="0.381" layer="21"/>
<rectangle x1="2.58318125" y1="0.29641875" x2="2.75081875" y2="0.381" layer="21"/>
<rectangle x1="3.34518125" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="3.51281875" y1="0.29641875" x2="3.683" y2="0.381" layer="21"/>
<rectangle x1="4.02081875" y1="0.29641875" x2="4.27481875" y2="0.381" layer="21"/>
<rectangle x1="4.52881875" y1="0.29641875" x2="5.29081875" y2="0.381" layer="21"/>
<rectangle x1="5.79881875" y1="0.29641875" x2="6.30681875" y2="0.381" layer="21"/>
<rectangle x1="6.56081875" y1="0.29641875" x2="6.985" y2="0.381" layer="21"/>
<rectangle x1="7.32281875" y1="0.29641875" x2="7.83081875" y2="0.381" layer="21"/>
<rectangle x1="8.33881875" y1="0.29641875" x2="8.509" y2="0.381" layer="21"/>
<rectangle x1="8.93318125" y1="0.29641875" x2="9.10081875" y2="0.381" layer="21"/>
<rectangle x1="9.35481875" y1="0.29641875" x2="9.525" y2="0.381" layer="21"/>
<rectangle x1="10.033" y1="0.29641875" x2="10.20318125" y2="0.381" layer="21"/>
<rectangle x1="10.37081875" y1="0.29641875" x2="10.96518125" y2="0.381" layer="21"/>
<rectangle x1="1.48081875" y1="0.381" x2="1.651" y2="0.46558125" layer="21"/>
<rectangle x1="2.24281875" y1="0.381" x2="2.413" y2="0.46558125" layer="21"/>
<rectangle x1="2.58318125" y1="0.381" x2="2.75081875" y2="0.46558125" layer="21"/>
<rectangle x1="3.34518125" y1="0.381" x2="3.429" y2="0.46558125" layer="21"/>
<rectangle x1="3.51281875" y1="0.381" x2="3.683" y2="0.46558125" layer="21"/>
<rectangle x1="3.937" y1="0.381" x2="4.10718125" y2="0.46558125" layer="21"/>
<rectangle x1="4.52881875" y1="0.381" x2="4.61518125" y2="0.46558125" layer="21"/>
<rectangle x1="5.207" y1="0.381" x2="5.37718125" y2="0.46558125" layer="21"/>
<rectangle x1="5.715" y1="0.381" x2="5.88518125" y2="0.46558125" layer="21"/>
<rectangle x1="6.223" y1="0.381" x2="6.39318125" y2="0.46558125" layer="21"/>
<rectangle x1="6.56081875" y1="0.381" x2="6.64718125" y2="0.46558125" layer="21"/>
<rectangle x1="6.90118125" y1="0.381" x2="7.06881875" y2="0.46558125" layer="21"/>
<rectangle x1="7.239" y1="0.381" x2="7.40918125" y2="0.46558125" layer="21"/>
<rectangle x1="7.747" y1="0.381" x2="7.91718125" y2="0.46558125" layer="21"/>
<rectangle x1="8.255" y1="0.381" x2="8.42518125" y2="0.46558125" layer="21"/>
<rectangle x1="9.017" y1="0.381" x2="9.18718125" y2="0.46558125" layer="21"/>
<rectangle x1="9.35481875" y1="0.381" x2="9.525" y2="0.46558125" layer="21"/>
<rectangle x1="10.033" y1="0.381" x2="10.20318125" y2="0.46558125" layer="21"/>
<rectangle x1="10.37081875" y1="0.381" x2="10.45718125" y2="0.46558125" layer="21"/>
<rectangle x1="10.87881875" y1="0.381" x2="11.049" y2="0.46558125" layer="21"/>
<rectangle x1="1.397" y1="0.46558125" x2="1.56718125" y2="0.55041875" layer="21"/>
<rectangle x1="2.32918125" y1="0.46558125" x2="2.49681875" y2="0.55041875" layer="21"/>
<rectangle x1="2.58318125" y1="0.46558125" x2="2.75081875" y2="0.55041875" layer="21"/>
<rectangle x1="3.34518125" y1="0.46558125" x2="3.429" y2="0.55041875" layer="21"/>
<rectangle x1="3.51281875" y1="0.46558125" x2="3.683" y2="0.55041875" layer="21"/>
<rectangle x1="3.85318125" y1="0.46558125" x2="4.02081875" y2="0.55041875" layer="21"/>
<rectangle x1="4.52881875" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="21"/>
<rectangle x1="5.29081875" y1="0.46558125" x2="5.461" y2="0.55041875" layer="21"/>
<rectangle x1="5.63118125" y1="0.46558125" x2="5.79881875" y2="0.55041875" layer="21"/>
<rectangle x1="6.30681875" y1="0.46558125" x2="6.39318125" y2="0.55041875" layer="21"/>
<rectangle x1="6.90118125" y1="0.46558125" x2="7.06881875" y2="0.55041875" layer="21"/>
<rectangle x1="7.15518125" y1="0.46558125" x2="7.32281875" y2="0.55041875" layer="21"/>
<rectangle x1="7.83081875" y1="0.46558125" x2="7.91718125" y2="0.55041875" layer="21"/>
<rectangle x1="8.17118125" y1="0.46558125" x2="8.33881875" y2="0.55041875" layer="21"/>
<rectangle x1="9.10081875" y1="0.46558125" x2="9.271" y2="0.55041875" layer="21"/>
<rectangle x1="9.35481875" y1="0.46558125" x2="9.525" y2="0.55041875" layer="21"/>
<rectangle x1="10.033" y1="0.46558125" x2="10.20318125" y2="0.55041875" layer="21"/>
<rectangle x1="10.287" y1="0.46558125" x2="10.45718125" y2="0.55041875" layer="21"/>
<rectangle x1="1.397" y1="0.55041875" x2="1.48081875" y2="0.635" layer="21"/>
<rectangle x1="2.32918125" y1="0.55041875" x2="2.49681875" y2="0.635" layer="21"/>
<rectangle x1="2.667" y1="0.55041875" x2="2.75081875" y2="0.635" layer="21"/>
<rectangle x1="3.34518125" y1="0.55041875" x2="3.429" y2="0.635" layer="21"/>
<rectangle x1="3.51281875" y1="0.55041875" x2="3.683" y2="0.635" layer="21"/>
<rectangle x1="3.85318125" y1="0.55041875" x2="4.02081875" y2="0.635" layer="21"/>
<rectangle x1="4.52881875" y1="0.55041875" x2="4.61518125" y2="0.635" layer="21"/>
<rectangle x1="5.37718125" y1="0.55041875" x2="5.461" y2="0.635" layer="21"/>
<rectangle x1="5.63118125" y1="0.55041875" x2="5.715" y2="0.635" layer="21"/>
<rectangle x1="6.30681875" y1="0.55041875" x2="6.39318125" y2="0.635" layer="21"/>
<rectangle x1="6.731" y1="0.55041875" x2="6.985" y2="0.635" layer="21"/>
<rectangle x1="7.15518125" y1="0.55041875" x2="7.91718125" y2="0.635" layer="21"/>
<rectangle x1="8.17118125" y1="0.55041875" x2="8.255" y2="0.635" layer="21"/>
<rectangle x1="9.18718125" y1="0.55041875" x2="9.271" y2="0.635" layer="21"/>
<rectangle x1="9.35481875" y1="0.55041875" x2="9.525" y2="0.635" layer="21"/>
<rectangle x1="10.033" y1="0.55041875" x2="10.20318125" y2="0.635" layer="21"/>
<rectangle x1="10.287" y1="0.55041875" x2="10.96518125" y2="0.635" layer="21"/>
<rectangle x1="1.31318125" y1="0.635" x2="1.48081875" y2="0.71958125" layer="21"/>
<rectangle x1="2.413" y1="0.635" x2="2.49681875" y2="0.71958125" layer="21"/>
<rectangle x1="2.667" y1="0.635" x2="2.75081875" y2="0.71958125" layer="21"/>
<rectangle x1="3.25881875" y1="0.635" x2="3.429" y2="0.71958125" layer="21"/>
<rectangle x1="3.51281875" y1="0.635" x2="3.683" y2="0.71958125" layer="21"/>
<rectangle x1="3.85318125" y1="0.635" x2="4.02081875" y2="0.71958125" layer="21"/>
<rectangle x1="4.52881875" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="5.37718125" y1="0.635" x2="5.54481875" y2="0.71958125" layer="21"/>
<rectangle x1="5.63118125" y1="0.635" x2="5.715" y2="0.71958125" layer="21"/>
<rectangle x1="6.30681875" y1="0.635" x2="6.39318125" y2="0.71958125" layer="21"/>
<rectangle x1="6.56081875" y1="0.635" x2="6.90118125" y2="0.71958125" layer="21"/>
<rectangle x1="7.15518125" y1="0.635" x2="7.91718125" y2="0.71958125" layer="21"/>
<rectangle x1="8.17118125" y1="0.635" x2="8.255" y2="0.71958125" layer="21"/>
<rectangle x1="9.18718125" y1="0.635" x2="9.271" y2="0.71958125" layer="21"/>
<rectangle x1="9.35481875" y1="0.635" x2="9.525" y2="0.71958125" layer="21"/>
<rectangle x1="10.033" y1="0.635" x2="10.20318125" y2="0.71958125" layer="21"/>
<rectangle x1="10.287" y1="0.635" x2="11.049" y2="0.71958125" layer="21"/>
<rectangle x1="1.31318125" y1="0.71958125" x2="1.48081875" y2="0.80441875" layer="21"/>
<rectangle x1="2.413" y1="0.71958125" x2="2.49681875" y2="0.80441875" layer="21"/>
<rectangle x1="2.667" y1="0.71958125" x2="2.75081875" y2="0.80441875" layer="21"/>
<rectangle x1="3.25881875" y1="0.71958125" x2="3.429" y2="0.80441875" layer="21"/>
<rectangle x1="3.51281875" y1="0.71958125" x2="3.683" y2="0.80441875" layer="21"/>
<rectangle x1="3.85318125" y1="0.71958125" x2="4.02081875" y2="0.80441875" layer="21"/>
<rectangle x1="4.52881875" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="5.37718125" y1="0.71958125" x2="5.54481875" y2="0.80441875" layer="21"/>
<rectangle x1="5.63118125" y1="0.71958125" x2="5.79881875" y2="0.80441875" layer="21"/>
<rectangle x1="6.30681875" y1="0.71958125" x2="6.39318125" y2="0.80441875" layer="21"/>
<rectangle x1="6.477" y1="0.71958125" x2="6.64718125" y2="0.80441875" layer="21"/>
<rectangle x1="7.15518125" y1="0.71958125" x2="7.32281875" y2="0.80441875" layer="21"/>
<rectangle x1="7.83081875" y1="0.71958125" x2="7.91718125" y2="0.80441875" layer="21"/>
<rectangle x1="8.08481875" y1="0.71958125" x2="8.255" y2="0.80441875" layer="21"/>
<rectangle x1="9.18718125" y1="0.71958125" x2="9.271" y2="0.80441875" layer="21"/>
<rectangle x1="9.44118125" y1="0.71958125" x2="9.525" y2="0.80441875" layer="21"/>
<rectangle x1="10.033" y1="0.71958125" x2="10.20318125" y2="0.80441875" layer="21"/>
<rectangle x1="10.287" y1="0.71958125" x2="10.37081875" y2="0.80441875" layer="21"/>
<rectangle x1="10.87881875" y1="0.71958125" x2="11.049" y2="0.80441875" layer="21"/>
<rectangle x1="1.31318125" y1="0.80441875" x2="1.48081875" y2="0.889" layer="21"/>
<rectangle x1="2.413" y1="0.80441875" x2="2.49681875" y2="0.889" layer="21"/>
<rectangle x1="2.667" y1="0.80441875" x2="2.83718125" y2="0.889" layer="21"/>
<rectangle x1="3.175" y1="0.80441875" x2="3.34518125" y2="0.889" layer="21"/>
<rectangle x1="3.51281875" y1="0.80441875" x2="3.683" y2="0.889" layer="21"/>
<rectangle x1="3.85318125" y1="0.80441875" x2="4.02081875" y2="0.889" layer="21"/>
<rectangle x1="4.52881875" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="5.37718125" y1="0.80441875" x2="5.54481875" y2="0.889" layer="21"/>
<rectangle x1="5.715" y1="0.80441875" x2="5.79881875" y2="0.889" layer="21"/>
<rectangle x1="6.223" y1="0.80441875" x2="6.39318125" y2="0.889" layer="21"/>
<rectangle x1="6.477" y1="0.80441875" x2="6.64718125" y2="0.889" layer="21"/>
<rectangle x1="6.90118125" y1="0.80441875" x2="7.06881875" y2="0.889" layer="21"/>
<rectangle x1="7.239" y1="0.80441875" x2="7.40918125" y2="0.889" layer="21"/>
<rectangle x1="7.747" y1="0.80441875" x2="7.91718125" y2="0.889" layer="21"/>
<rectangle x1="8.17118125" y1="0.80441875" x2="8.255" y2="0.889" layer="21"/>
<rectangle x1="9.18718125" y1="0.80441875" x2="9.271" y2="0.889" layer="21"/>
<rectangle x1="9.44118125" y1="0.80441875" x2="9.60881875" y2="0.889" layer="21"/>
<rectangle x1="9.94918125" y1="0.80441875" x2="10.11681875" y2="0.889" layer="21"/>
<rectangle x1="10.287" y1="0.80441875" x2="10.45718125" y2="0.889" layer="21"/>
<rectangle x1="10.87881875" y1="0.80441875" x2="11.049" y2="0.889" layer="21"/>
<rectangle x1="1.31318125" y1="0.889" x2="1.48081875" y2="0.97358125" layer="21"/>
<rectangle x1="2.413" y1="0.889" x2="2.49681875" y2="0.97358125" layer="21"/>
<rectangle x1="2.667" y1="0.889" x2="3.25881875" y2="0.97358125" layer="21"/>
<rectangle x1="3.51281875" y1="0.889" x2="3.683" y2="0.97358125" layer="21"/>
<rectangle x1="3.76681875" y1="0.889" x2="4.27481875" y2="0.97358125" layer="21"/>
<rectangle x1="4.52881875" y1="0.889" x2="4.61518125" y2="0.97358125" layer="21"/>
<rectangle x1="5.37718125" y1="0.889" x2="5.54481875" y2="0.97358125" layer="21"/>
<rectangle x1="5.715" y1="0.889" x2="6.30681875" y2="0.97358125" layer="21"/>
<rectangle x1="6.56081875" y1="0.889" x2="6.985" y2="0.97358125" layer="21"/>
<rectangle x1="7.32281875" y1="0.889" x2="7.83081875" y2="0.97358125" layer="21"/>
<rectangle x1="8.17118125" y1="0.889" x2="8.255" y2="0.97358125" layer="21"/>
<rectangle x1="9.18718125" y1="0.889" x2="9.271" y2="0.97358125" layer="21"/>
<rectangle x1="9.35481875" y1="0.889" x2="10.033" y2="0.97358125" layer="21"/>
<rectangle x1="10.37081875" y1="0.889" x2="10.96518125" y2="0.97358125" layer="21"/>
<rectangle x1="1.31318125" y1="0.97358125" x2="1.48081875" y2="1.05841875" layer="21"/>
<rectangle x1="2.413" y1="0.97358125" x2="2.49681875" y2="1.05841875" layer="21"/>
<rectangle x1="2.667" y1="0.97358125" x2="2.75081875" y2="1.05841875" layer="21"/>
<rectangle x1="2.921" y1="0.97358125" x2="3.175" y2="1.05841875" layer="21"/>
<rectangle x1="3.76681875" y1="0.97358125" x2="4.27481875" y2="1.05841875" layer="21"/>
<rectangle x1="4.52881875" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="21"/>
<rectangle x1="5.37718125" y1="0.97358125" x2="5.54481875" y2="1.05841875" layer="21"/>
<rectangle x1="5.88518125" y1="0.97358125" x2="6.13918125" y2="1.05841875" layer="21"/>
<rectangle x1="6.64718125" y1="0.97358125" x2="6.90118125" y2="1.05841875" layer="21"/>
<rectangle x1="7.40918125" y1="0.97358125" x2="7.66318125" y2="1.05841875" layer="21"/>
<rectangle x1="8.17118125" y1="0.97358125" x2="8.33881875" y2="1.05841875" layer="21"/>
<rectangle x1="9.10081875" y1="0.97358125" x2="9.271" y2="1.05841875" layer="21"/>
<rectangle x1="9.60881875" y1="0.97358125" x2="9.94918125" y2="1.05841875" layer="21"/>
<rectangle x1="10.541" y1="0.97358125" x2="10.795" y2="1.05841875" layer="21"/>
<rectangle x1="1.31318125" y1="1.05841875" x2="1.48081875" y2="1.143" layer="21"/>
<rectangle x1="2.413" y1="1.05841875" x2="2.49681875" y2="1.143" layer="21"/>
<rectangle x1="3.51281875" y1="1.05841875" x2="3.683" y2="1.143" layer="21"/>
<rectangle x1="3.85318125" y1="1.05841875" x2="4.02081875" y2="1.143" layer="21"/>
<rectangle x1="4.52881875" y1="1.05841875" x2="4.61518125" y2="1.143" layer="21"/>
<rectangle x1="5.29081875" y1="1.05841875" x2="5.461" y2="1.143" layer="21"/>
<rectangle x1="8.255" y1="1.05841875" x2="8.42518125" y2="1.143" layer="21"/>
<rectangle x1="9.017" y1="1.05841875" x2="9.18718125" y2="1.143" layer="21"/>
<rectangle x1="1.31318125" y1="1.143" x2="1.48081875" y2="1.22758125" layer="21"/>
<rectangle x1="2.413" y1="1.143" x2="2.49681875" y2="1.22758125" layer="21"/>
<rectangle x1="3.51281875" y1="1.143" x2="3.683" y2="1.22758125" layer="21"/>
<rectangle x1="3.85318125" y1="1.143" x2="4.02081875" y2="1.22758125" layer="21"/>
<rectangle x1="4.52881875" y1="1.143" x2="4.61518125" y2="1.22758125" layer="21"/>
<rectangle x1="5.29081875" y1="1.143" x2="5.461" y2="1.22758125" layer="21"/>
<rectangle x1="8.33881875" y1="1.143" x2="8.509" y2="1.22758125" layer="21"/>
<rectangle x1="8.93318125" y1="1.143" x2="9.10081875" y2="1.22758125" layer="21"/>
<rectangle x1="1.31318125" y1="1.22758125" x2="1.48081875" y2="1.31241875" layer="21"/>
<rectangle x1="2.413" y1="1.22758125" x2="2.49681875" y2="1.31241875" layer="21"/>
<rectangle x1="3.85318125" y1="1.22758125" x2="4.02081875" y2="1.31241875" layer="21"/>
<rectangle x1="4.52881875" y1="1.22758125" x2="4.61518125" y2="1.31241875" layer="21"/>
<rectangle x1="5.12318125" y1="1.22758125" x2="5.37718125" y2="1.31241875" layer="21"/>
<rectangle x1="8.42518125" y1="1.22758125" x2="9.017" y2="1.31241875" layer="21"/>
<rectangle x1="4.52881875" y1="1.31241875" x2="5.207" y2="1.397" layer="21"/>
<rectangle x1="8.59281875" y1="1.31241875" x2="8.84681875" y2="1.397" layer="21"/>
<rectangle x1="2.24281875" y1="1.905" x2="9.779" y2="1.98958125" layer="21"/>
<rectangle x1="1.82118125" y1="1.98958125" x2="10.287" y2="2.07441875" layer="21"/>
<rectangle x1="1.56718125" y1="2.07441875" x2="10.541" y2="2.159" layer="21"/>
<rectangle x1="1.31318125" y1="2.159" x2="10.71118125" y2="2.24358125" layer="21"/>
<rectangle x1="1.22681875" y1="2.24358125" x2="2.07518125" y2="2.32841875" layer="21"/>
<rectangle x1="5.88518125" y1="2.24358125" x2="10.87881875" y2="2.32841875" layer="21"/>
<rectangle x1="1.05918125" y1="2.32841875" x2="1.73481875" y2="2.413" layer="21"/>
<rectangle x1="5.88518125" y1="2.32841875" x2="10.96518125" y2="2.413" layer="21"/>
<rectangle x1="0.97281875" y1="2.413" x2="1.56718125" y2="2.49758125" layer="21"/>
<rectangle x1="5.79881875" y1="2.413" x2="11.13281875" y2="2.49758125" layer="21"/>
<rectangle x1="0.889" y1="2.49758125" x2="1.397" y2="2.58241875" layer="21"/>
<rectangle x1="5.79881875" y1="2.49758125" x2="11.21918125" y2="2.58241875" layer="21"/>
<rectangle x1="0.71881875" y1="2.58241875" x2="1.22681875" y2="2.667" layer="21"/>
<rectangle x1="5.715" y1="2.58241875" x2="11.303" y2="2.667" layer="21"/>
<rectangle x1="0.635" y1="2.667" x2="1.143" y2="2.75158125" layer="21"/>
<rectangle x1="5.715" y1="2.667" x2="11.38681875" y2="2.75158125" layer="21"/>
<rectangle x1="0.635" y1="2.75158125" x2="1.05918125" y2="2.83641875" layer="21"/>
<rectangle x1="5.63118125" y1="2.75158125" x2="11.47318125" y2="2.83641875" layer="21"/>
<rectangle x1="0.55118125" y1="2.83641875" x2="0.97281875" y2="2.921" layer="21"/>
<rectangle x1="5.63118125" y1="2.83641875" x2="11.557" y2="2.921" layer="21"/>
<rectangle x1="0.46481875" y1="2.921" x2="0.889" y2="3.00558125" layer="21"/>
<rectangle x1="5.63118125" y1="2.921" x2="11.557" y2="3.00558125" layer="21"/>
<rectangle x1="0.381" y1="3.00558125" x2="0.80518125" y2="3.09041875" layer="21"/>
<rectangle x1="5.54481875" y1="3.00558125" x2="11.64081875" y2="3.09041875" layer="21"/>
<rectangle x1="0.381" y1="3.09041875" x2="0.71881875" y2="3.175" layer="21"/>
<rectangle x1="5.54481875" y1="3.09041875" x2="11.72718125" y2="3.175" layer="21"/>
<rectangle x1="0.29718125" y1="3.175" x2="0.71881875" y2="3.25958125" layer="21"/>
<rectangle x1="5.54481875" y1="3.175" x2="11.72718125" y2="3.25958125" layer="21"/>
<rectangle x1="0.29718125" y1="3.25958125" x2="0.635" y2="3.34441875" layer="21"/>
<rectangle x1="5.54481875" y1="3.25958125" x2="11.811" y2="3.34441875" layer="21"/>
<rectangle x1="0.21081875" y1="3.34441875" x2="0.55118125" y2="3.429" layer="21"/>
<rectangle x1="2.159" y1="3.34441875" x2="2.75081875" y2="3.429" layer="21"/>
<rectangle x1="3.51281875" y1="3.34441875" x2="4.27481875" y2="3.429" layer="21"/>
<rectangle x1="5.54481875" y1="3.34441875" x2="6.477" y2="3.429" layer="21"/>
<rectangle x1="7.06881875" y1="3.34441875" x2="8.001" y2="3.429" layer="21"/>
<rectangle x1="8.17118125" y1="3.34441875" x2="9.10081875" y2="3.429" layer="21"/>
<rectangle x1="9.271" y1="3.34441875" x2="9.86281875" y2="3.429" layer="21"/>
<rectangle x1="10.287" y1="3.34441875" x2="11.811" y2="3.429" layer="21"/>
<rectangle x1="0.21081875" y1="3.429" x2="0.55118125" y2="3.51358125" layer="21"/>
<rectangle x1="1.98881875" y1="3.429" x2="2.921" y2="3.51358125" layer="21"/>
<rectangle x1="3.429" y1="3.429" x2="4.445" y2="3.51358125" layer="21"/>
<rectangle x1="5.461" y1="3.429" x2="6.30681875" y2="3.51358125" layer="21"/>
<rectangle x1="7.32281875" y1="3.429" x2="8.001" y2="3.51358125" layer="21"/>
<rectangle x1="8.255" y1="3.429" x2="9.017" y2="3.51358125" layer="21"/>
<rectangle x1="9.271" y1="3.429" x2="9.69518125" y2="3.51358125" layer="21"/>
<rectangle x1="10.541" y1="3.429" x2="11.89481875" y2="3.51358125" layer="21"/>
<rectangle x1="0.127" y1="3.51358125" x2="0.55118125" y2="3.59841875" layer="21"/>
<rectangle x1="1.82118125" y1="3.51358125" x2="3.00481875" y2="3.59841875" layer="21"/>
<rectangle x1="3.429" y1="3.51358125" x2="4.61518125" y2="3.59841875" layer="21"/>
<rectangle x1="5.461" y1="3.51358125" x2="6.13918125" y2="3.59841875" layer="21"/>
<rectangle x1="7.40918125" y1="3.51358125" x2="7.91718125" y2="3.59841875" layer="21"/>
<rectangle x1="8.255" y1="3.51358125" x2="9.017" y2="3.59841875" layer="21"/>
<rectangle x1="9.271" y1="3.51358125" x2="9.60881875" y2="3.59841875" layer="21"/>
<rectangle x1="10.62481875" y1="3.51358125" x2="11.89481875" y2="3.59841875" layer="21"/>
<rectangle x1="0.127" y1="3.59841875" x2="0.46481875" y2="3.683" layer="21"/>
<rectangle x1="1.73481875" y1="3.59841875" x2="3.175" y2="3.683" layer="21"/>
<rectangle x1="3.51281875" y1="3.59841875" x2="4.699" y2="3.683" layer="21"/>
<rectangle x1="5.461" y1="3.59841875" x2="6.05281875" y2="3.683" layer="21"/>
<rectangle x1="7.493" y1="3.59841875" x2="7.91718125" y2="3.683" layer="21"/>
<rectangle x1="8.255" y1="3.59841875" x2="9.017" y2="3.683" layer="21"/>
<rectangle x1="9.271" y1="3.59841875" x2="9.525" y2="3.683" layer="21"/>
<rectangle x1="10.033" y1="3.59841875" x2="10.20318125" y2="3.683" layer="21"/>
<rectangle x1="10.71118125" y1="3.59841875" x2="11.89481875" y2="3.683" layer="21"/>
<rectangle x1="0.127" y1="3.683" x2="0.46481875" y2="3.76758125" layer="21"/>
<rectangle x1="1.651" y1="3.683" x2="2.159" y2="3.76758125" layer="21"/>
<rectangle x1="2.75081875" y1="3.683" x2="3.175" y2="3.76758125" layer="21"/>
<rectangle x1="4.27481875" y1="3.683" x2="4.78281875" y2="3.76758125" layer="21"/>
<rectangle x1="5.461" y1="3.683" x2="5.969" y2="3.76758125" layer="21"/>
<rectangle x1="6.477" y1="3.683" x2="7.06881875" y2="3.76758125" layer="21"/>
<rectangle x1="7.57681875" y1="3.683" x2="7.91718125" y2="3.76758125" layer="21"/>
<rectangle x1="8.255" y1="3.683" x2="9.017" y2="3.76758125" layer="21"/>
<rectangle x1="9.271" y1="3.683" x2="9.525" y2="3.76758125" layer="21"/>
<rectangle x1="9.86281875" y1="3.683" x2="10.37081875" y2="3.76758125" layer="21"/>
<rectangle x1="10.71118125" y1="3.683" x2="11.98118125" y2="3.76758125" layer="21"/>
<rectangle x1="0.127" y1="3.76758125" x2="0.381" y2="3.85241875" layer="21"/>
<rectangle x1="1.56718125" y1="3.76758125" x2="1.98881875" y2="3.85241875" layer="21"/>
<rectangle x1="2.83718125" y1="3.76758125" x2="3.25881875" y2="3.85241875" layer="21"/>
<rectangle x1="4.445" y1="3.76758125" x2="4.86918125" y2="3.85241875" layer="21"/>
<rectangle x1="5.461" y1="3.76758125" x2="5.969" y2="3.85241875" layer="21"/>
<rectangle x1="6.39318125" y1="3.76758125" x2="7.15518125" y2="3.85241875" layer="21"/>
<rectangle x1="7.66318125" y1="3.76758125" x2="7.91718125" y2="3.85241875" layer="21"/>
<rectangle x1="8.255" y1="3.76758125" x2="9.017" y2="3.85241875" layer="21"/>
<rectangle x1="9.271" y1="3.76758125" x2="9.44118125" y2="3.85241875" layer="21"/>
<rectangle x1="9.779" y1="3.76758125" x2="10.541" y2="3.85241875" layer="21"/>
<rectangle x1="10.62481875" y1="3.76758125" x2="11.98118125" y2="3.85241875" layer="21"/>
<rectangle x1="0.04318125" y1="3.85241875" x2="0.381" y2="3.937" layer="21"/>
<rectangle x1="1.56718125" y1="3.85241875" x2="1.905" y2="3.937" layer="21"/>
<rectangle x1="2.921" y1="3.85241875" x2="3.34518125" y2="3.937" layer="21"/>
<rectangle x1="4.52881875" y1="3.85241875" x2="4.86918125" y2="3.937" layer="21"/>
<rectangle x1="5.461" y1="3.85241875" x2="5.88518125" y2="3.937" layer="21"/>
<rectangle x1="6.30681875" y1="3.85241875" x2="7.239" y2="3.937" layer="21"/>
<rectangle x1="7.66318125" y1="3.85241875" x2="7.91718125" y2="3.937" layer="21"/>
<rectangle x1="8.255" y1="3.85241875" x2="9.017" y2="3.937" layer="21"/>
<rectangle x1="9.271" y1="3.85241875" x2="9.44118125" y2="3.937" layer="21"/>
<rectangle x1="9.779" y1="3.85241875" x2="9.86281875" y2="3.937" layer="21"/>
<rectangle x1="10.45718125" y1="3.85241875" x2="11.98118125" y2="3.937" layer="21"/>
<rectangle x1="0.04318125" y1="3.937" x2="0.381" y2="4.02158125" layer="21"/>
<rectangle x1="1.48081875" y1="3.937" x2="1.905" y2="4.02158125" layer="21"/>
<rectangle x1="3.00481875" y1="3.937" x2="3.34518125" y2="4.02158125" layer="21"/>
<rectangle x1="4.52881875" y1="3.937" x2="4.953" y2="4.02158125" layer="21"/>
<rectangle x1="5.461" y1="3.937" x2="5.88518125" y2="4.02158125" layer="21"/>
<rectangle x1="6.223" y1="3.937" x2="7.32281875" y2="4.02158125" layer="21"/>
<rectangle x1="7.747" y1="3.937" x2="7.91718125" y2="4.02158125" layer="21"/>
<rectangle x1="8.255" y1="3.937" x2="9.017" y2="4.02158125" layer="21"/>
<rectangle x1="9.271" y1="3.937" x2="9.44118125" y2="4.02158125" layer="21"/>
<rectangle x1="10.795" y1="3.937" x2="11.98118125" y2="4.02158125" layer="21"/>
<rectangle x1="0.04318125" y1="4.02158125" x2="0.381" y2="4.10641875" layer="21"/>
<rectangle x1="1.48081875" y1="4.02158125" x2="1.82118125" y2="4.10641875" layer="21"/>
<rectangle x1="3.00481875" y1="4.02158125" x2="3.34518125" y2="4.10641875" layer="21"/>
<rectangle x1="4.61518125" y1="4.02158125" x2="4.953" y2="4.10641875" layer="21"/>
<rectangle x1="5.461" y1="4.02158125" x2="5.79881875" y2="4.10641875" layer="21"/>
<rectangle x1="6.13918125" y1="4.02158125" x2="7.40918125" y2="4.10641875" layer="21"/>
<rectangle x1="7.747" y1="4.02158125" x2="7.91718125" y2="4.10641875" layer="21"/>
<rectangle x1="8.255" y1="4.02158125" x2="9.017" y2="4.10641875" layer="21"/>
<rectangle x1="9.271" y1="4.02158125" x2="9.44118125" y2="4.10641875" layer="21"/>
<rectangle x1="10.795" y1="4.02158125" x2="11.98118125" y2="4.10641875" layer="21"/>
<rectangle x1="0.04318125" y1="4.10641875" x2="0.381" y2="4.191" layer="21"/>
<rectangle x1="1.48081875" y1="4.10641875" x2="1.82118125" y2="4.191" layer="21"/>
<rectangle x1="3.09118125" y1="4.10641875" x2="3.429" y2="4.191" layer="21"/>
<rectangle x1="4.61518125" y1="4.10641875" x2="4.953" y2="4.191" layer="21"/>
<rectangle x1="5.37718125" y1="4.10641875" x2="5.79881875" y2="4.191" layer="21"/>
<rectangle x1="6.13918125" y1="4.10641875" x2="7.40918125" y2="4.191" layer="21"/>
<rectangle x1="7.747" y1="4.10641875" x2="7.91718125" y2="4.191" layer="21"/>
<rectangle x1="8.255" y1="4.10641875" x2="9.017" y2="4.191" layer="21"/>
<rectangle x1="9.271" y1="4.10641875" x2="9.44118125" y2="4.191" layer="21"/>
<rectangle x1="10.795" y1="4.10641875" x2="11.98118125" y2="4.191" layer="21"/>
<rectangle x1="0.04318125" y1="4.191" x2="0.381" y2="4.27558125" layer="21"/>
<rectangle x1="1.48081875" y1="4.191" x2="1.73481875" y2="4.27558125" layer="21"/>
<rectangle x1="3.09118125" y1="4.191" x2="3.429" y2="4.27558125" layer="21"/>
<rectangle x1="4.699" y1="4.191" x2="4.953" y2="4.27558125" layer="21"/>
<rectangle x1="5.37718125" y1="4.191" x2="5.79881875" y2="4.27558125" layer="21"/>
<rectangle x1="6.13918125" y1="4.191" x2="7.40918125" y2="4.27558125" layer="21"/>
<rectangle x1="7.747" y1="4.191" x2="7.91718125" y2="4.27558125" layer="21"/>
<rectangle x1="8.255" y1="4.191" x2="8.93318125" y2="4.27558125" layer="21"/>
<rectangle x1="9.271" y1="4.191" x2="9.44118125" y2="4.27558125" layer="21"/>
<rectangle x1="9.779" y1="4.191" x2="10.45718125" y2="4.27558125" layer="21"/>
<rectangle x1="10.71118125" y1="4.191" x2="11.98118125" y2="4.27558125" layer="21"/>
<rectangle x1="0.04318125" y1="4.27558125" x2="0.381" y2="4.36041875" layer="21"/>
<rectangle x1="1.48081875" y1="4.27558125" x2="1.73481875" y2="4.36041875" layer="21"/>
<rectangle x1="3.09118125" y1="4.27558125" x2="3.429" y2="4.36041875" layer="21"/>
<rectangle x1="4.699" y1="4.27558125" x2="4.953" y2="4.36041875" layer="21"/>
<rectangle x1="5.37718125" y1="4.27558125" x2="5.79881875" y2="4.36041875" layer="21"/>
<rectangle x1="6.13918125" y1="4.27558125" x2="7.40918125" y2="4.36041875" layer="21"/>
<rectangle x1="7.747" y1="4.27558125" x2="7.91718125" y2="4.36041875" layer="21"/>
<rectangle x1="8.33881875" y1="4.27558125" x2="8.93318125" y2="4.36041875" layer="21"/>
<rectangle x1="9.271" y1="4.27558125" x2="9.525" y2="4.36041875" layer="21"/>
<rectangle x1="9.86281875" y1="4.27558125" x2="10.37081875" y2="4.36041875" layer="21"/>
<rectangle x1="10.71118125" y1="4.27558125" x2="11.98118125" y2="4.36041875" layer="21"/>
<rectangle x1="0.04318125" y1="4.36041875" x2="0.381" y2="4.445" layer="21"/>
<rectangle x1="1.48081875" y1="4.36041875" x2="1.73481875" y2="4.445" layer="21"/>
<rectangle x1="3.09118125" y1="4.36041875" x2="3.429" y2="4.445" layer="21"/>
<rectangle x1="4.699" y1="4.36041875" x2="4.953" y2="4.445" layer="21"/>
<rectangle x1="5.37718125" y1="4.36041875" x2="5.79881875" y2="4.445" layer="21"/>
<rectangle x1="6.13918125" y1="4.36041875" x2="7.40918125" y2="4.445" layer="21"/>
<rectangle x1="7.747" y1="4.36041875" x2="7.91718125" y2="4.445" layer="21"/>
<rectangle x1="8.509" y1="4.36041875" x2="8.763" y2="4.445" layer="21"/>
<rectangle x1="9.18718125" y1="4.36041875" x2="9.525" y2="4.445" layer="21"/>
<rectangle x1="9.94918125" y1="4.36041875" x2="10.20318125" y2="4.445" layer="21"/>
<rectangle x1="10.62481875" y1="4.36041875" x2="11.98118125" y2="4.445" layer="21"/>
<rectangle x1="0.04318125" y1="4.445" x2="0.381" y2="4.52958125" layer="21"/>
<rectangle x1="1.48081875" y1="4.445" x2="1.73481875" y2="4.52958125" layer="21"/>
<rectangle x1="3.09118125" y1="4.445" x2="3.429" y2="4.52958125" layer="21"/>
<rectangle x1="4.61518125" y1="4.445" x2="4.953" y2="4.52958125" layer="21"/>
<rectangle x1="5.37718125" y1="4.445" x2="5.79881875" y2="4.52958125" layer="21"/>
<rectangle x1="6.13918125" y1="4.445" x2="7.40918125" y2="4.52958125" layer="21"/>
<rectangle x1="7.747" y1="4.445" x2="7.91718125" y2="4.52958125" layer="21"/>
<rectangle x1="9.10081875" y1="4.445" x2="9.60881875" y2="4.52958125" layer="21"/>
<rectangle x1="10.541" y1="4.445" x2="11.98118125" y2="4.52958125" layer="21"/>
<rectangle x1="0.04318125" y1="4.52958125" x2="0.381" y2="4.61441875" layer="21"/>
<rectangle x1="1.48081875" y1="4.52958125" x2="1.73481875" y2="4.61441875" layer="21"/>
<rectangle x1="3.09118125" y1="4.52958125" x2="3.429" y2="4.61441875" layer="21"/>
<rectangle x1="4.61518125" y1="4.52958125" x2="4.953" y2="4.61441875" layer="21"/>
<rectangle x1="5.461" y1="4.52958125" x2="5.79881875" y2="4.61441875" layer="21"/>
<rectangle x1="6.13918125" y1="4.52958125" x2="7.40918125" y2="4.61441875" layer="21"/>
<rectangle x1="7.747" y1="4.52958125" x2="8.001" y2="4.61441875" layer="21"/>
<rectangle x1="9.017" y1="4.52958125" x2="9.69518125" y2="4.61441875" layer="21"/>
<rectangle x1="10.45718125" y1="4.52958125" x2="11.98118125" y2="4.61441875" layer="21"/>
<rectangle x1="0.04318125" y1="4.61441875" x2="0.381" y2="4.699" layer="21"/>
<rectangle x1="1.48081875" y1="4.61441875" x2="1.73481875" y2="4.699" layer="21"/>
<rectangle x1="3.09118125" y1="4.61441875" x2="3.429" y2="4.699" layer="21"/>
<rectangle x1="4.52881875" y1="4.61441875" x2="4.953" y2="4.699" layer="21"/>
<rectangle x1="5.461" y1="4.61441875" x2="5.79881875" y2="4.699" layer="21"/>
<rectangle x1="6.223" y1="4.61441875" x2="7.32281875" y2="4.699" layer="21"/>
<rectangle x1="7.747" y1="4.61441875" x2="8.001" y2="4.699" layer="21"/>
<rectangle x1="8.17118125" y1="4.61441875" x2="8.42518125" y2="4.699" layer="21"/>
<rectangle x1="8.84681875" y1="4.61441875" x2="9.86281875" y2="4.699" layer="21"/>
<rectangle x1="10.287" y1="4.61441875" x2="11.98118125" y2="4.699" layer="21"/>
<rectangle x1="0.04318125" y1="4.699" x2="0.381" y2="4.78358125" layer="21"/>
<rectangle x1="1.48081875" y1="4.699" x2="1.73481875" y2="4.78358125" layer="21"/>
<rectangle x1="3.09118125" y1="4.699" x2="3.429" y2="4.78358125" layer="21"/>
<rectangle x1="4.52881875" y1="4.699" x2="4.86918125" y2="4.78358125" layer="21"/>
<rectangle x1="5.461" y1="4.699" x2="5.88518125" y2="4.78358125" layer="21"/>
<rectangle x1="6.30681875" y1="4.699" x2="7.32281875" y2="4.78358125" layer="21"/>
<rectangle x1="7.66318125" y1="4.699" x2="11.98118125" y2="4.78358125" layer="21"/>
<rectangle x1="0.127" y1="4.78358125" x2="0.381" y2="4.86841875" layer="21"/>
<rectangle x1="1.48081875" y1="4.78358125" x2="1.73481875" y2="4.86841875" layer="21"/>
<rectangle x1="3.09118125" y1="4.78358125" x2="3.429" y2="4.86841875" layer="21"/>
<rectangle x1="4.445" y1="4.78358125" x2="4.86918125" y2="4.86841875" layer="21"/>
<rectangle x1="5.461" y1="4.78358125" x2="5.88518125" y2="4.86841875" layer="21"/>
<rectangle x1="6.39318125" y1="4.78358125" x2="7.239" y2="4.86841875" layer="21"/>
<rectangle x1="7.66318125" y1="4.78358125" x2="11.98118125" y2="4.86841875" layer="21"/>
<rectangle x1="0.127" y1="4.86841875" x2="0.46481875" y2="4.953" layer="21"/>
<rectangle x1="1.48081875" y1="4.86841875" x2="1.73481875" y2="4.953" layer="21"/>
<rectangle x1="3.09118125" y1="4.86841875" x2="3.429" y2="4.953" layer="21"/>
<rectangle x1="4.27481875" y1="4.86841875" x2="4.78281875" y2="4.953" layer="21"/>
<rectangle x1="5.461" y1="4.86841875" x2="5.969" y2="4.953" layer="21"/>
<rectangle x1="6.477" y1="4.86841875" x2="7.06881875" y2="4.953" layer="21"/>
<rectangle x1="7.57681875" y1="4.86841875" x2="11.98118125" y2="4.953" layer="21"/>
<rectangle x1="0.127" y1="4.953" x2="0.46481875" y2="5.03758125" layer="21"/>
<rectangle x1="1.48081875" y1="4.953" x2="1.73481875" y2="5.03758125" layer="21"/>
<rectangle x1="3.09118125" y1="4.953" x2="4.699" y2="5.03758125" layer="21"/>
<rectangle x1="5.461" y1="4.953" x2="6.05281875" y2="5.03758125" layer="21"/>
<rectangle x1="7.493" y1="4.953" x2="11.89481875" y2="5.03758125" layer="21"/>
<rectangle x1="0.127" y1="5.03758125" x2="0.46481875" y2="5.12241875" layer="21"/>
<rectangle x1="1.48081875" y1="5.03758125" x2="1.73481875" y2="5.12241875" layer="21"/>
<rectangle x1="3.09118125" y1="5.03758125" x2="4.61518125" y2="5.12241875" layer="21"/>
<rectangle x1="5.461" y1="5.03758125" x2="6.13918125" y2="5.12241875" layer="21"/>
<rectangle x1="7.40918125" y1="5.03758125" x2="11.89481875" y2="5.12241875" layer="21"/>
<rectangle x1="0.21081875" y1="5.12241875" x2="0.55118125" y2="5.207" layer="21"/>
<rectangle x1="1.48081875" y1="5.12241875" x2="1.73481875" y2="5.207" layer="21"/>
<rectangle x1="3.09118125" y1="5.12241875" x2="4.52881875" y2="5.207" layer="21"/>
<rectangle x1="5.461" y1="5.12241875" x2="6.223" y2="5.207" layer="21"/>
<rectangle x1="7.32281875" y1="5.12241875" x2="11.89481875" y2="5.207" layer="21"/>
<rectangle x1="0.21081875" y1="5.207" x2="0.55118125" y2="5.29158125" layer="21"/>
<rectangle x1="1.48081875" y1="5.207" x2="1.73481875" y2="5.29158125" layer="21"/>
<rectangle x1="3.175" y1="5.207" x2="4.36118125" y2="5.29158125" layer="21"/>
<rectangle x1="5.461" y1="5.207" x2="6.39318125" y2="5.29158125" layer="21"/>
<rectangle x1="7.15518125" y1="5.207" x2="11.811" y2="5.29158125" layer="21"/>
<rectangle x1="0.29718125" y1="5.29158125" x2="0.635" y2="5.37641875" layer="21"/>
<rectangle x1="5.54481875" y1="5.29158125" x2="11.811" y2="5.37641875" layer="21"/>
<rectangle x1="0.29718125" y1="5.37641875" x2="0.71881875" y2="5.461" layer="21"/>
<rectangle x1="5.54481875" y1="5.37641875" x2="11.72718125" y2="5.461" layer="21"/>
<rectangle x1="0.381" y1="5.461" x2="0.71881875" y2="5.54558125" layer="21"/>
<rectangle x1="5.54481875" y1="5.461" x2="11.72718125" y2="5.54558125" layer="21"/>
<rectangle x1="0.381" y1="5.54558125" x2="0.80518125" y2="5.63041875" layer="21"/>
<rectangle x1="5.54481875" y1="5.54558125" x2="11.64081875" y2="5.63041875" layer="21"/>
<rectangle x1="0.46481875" y1="5.63041875" x2="0.889" y2="5.715" layer="21"/>
<rectangle x1="5.63118125" y1="5.63041875" x2="11.64081875" y2="5.715" layer="21"/>
<rectangle x1="0.55118125" y1="5.715" x2="0.97281875" y2="5.79958125" layer="21"/>
<rectangle x1="5.63118125" y1="5.715" x2="11.557" y2="5.79958125" layer="21"/>
<rectangle x1="0.55118125" y1="5.79958125" x2="1.05918125" y2="5.88441875" layer="21"/>
<rectangle x1="5.63118125" y1="5.79958125" x2="11.47318125" y2="5.88441875" layer="21"/>
<rectangle x1="0.635" y1="5.88441875" x2="1.143" y2="5.969" layer="21"/>
<rectangle x1="5.715" y1="5.88441875" x2="11.38681875" y2="5.969" layer="21"/>
<rectangle x1="0.71881875" y1="5.969" x2="1.22681875" y2="6.05358125" layer="21"/>
<rectangle x1="5.715" y1="5.969" x2="11.303" y2="6.05358125" layer="21"/>
<rectangle x1="0.80518125" y1="6.05358125" x2="1.397" y2="6.13841875" layer="21"/>
<rectangle x1="5.79881875" y1="6.05358125" x2="11.21918125" y2="6.13841875" layer="21"/>
<rectangle x1="0.889" y1="6.13841875" x2="1.56718125" y2="6.223" layer="21"/>
<rectangle x1="5.79881875" y1="6.13841875" x2="11.13281875" y2="6.223" layer="21"/>
<rectangle x1="1.05918125" y1="6.223" x2="1.73481875" y2="6.30758125" layer="21"/>
<rectangle x1="5.88518125" y1="6.223" x2="11.049" y2="6.30758125" layer="21"/>
<rectangle x1="1.143" y1="6.30758125" x2="1.98881875" y2="6.39241875" layer="21"/>
<rectangle x1="5.88518125" y1="6.30758125" x2="10.87881875" y2="6.39241875" layer="21"/>
<rectangle x1="1.31318125" y1="6.39241875" x2="10.71118125" y2="6.477" layer="21"/>
<rectangle x1="1.48081875" y1="6.477" x2="10.541" y2="6.56158125" layer="21"/>
<rectangle x1="1.73481875" y1="6.56158125" x2="10.37081875" y2="6.64641875" layer="21"/>
</package>
<package name="UDO-LOGO-15MM" urn="urn:adsk.eagle:footprint:6649249/1">
<rectangle x1="2.24281875" y1="0.21158125" x2="2.58318125" y2="0.29641875" layer="21"/>
<rectangle x1="10.71118125" y1="0.21158125" x2="11.049" y2="0.29641875" layer="21"/>
<rectangle x1="1.98881875" y1="0.29641875" x2="2.83718125" y2="0.381" layer="21"/>
<rectangle x1="3.25881875" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="4.10718125" y1="0.29641875" x2="4.27481875" y2="0.381" layer="21"/>
<rectangle x1="4.445" y1="0.29641875" x2="4.52881875" y2="0.381" layer="21"/>
<rectangle x1="5.12318125" y1="0.29641875" x2="5.37718125" y2="0.381" layer="21"/>
<rectangle x1="5.63118125" y1="0.29641875" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="7.32281875" y1="0.29641875" x2="7.747" y2="0.381" layer="21"/>
<rectangle x1="8.33881875" y1="0.29641875" x2="8.67918125" y2="0.381" layer="21"/>
<rectangle x1="9.18718125" y1="0.29641875" x2="9.60881875" y2="0.381" layer="21"/>
<rectangle x1="10.45718125" y1="0.29641875" x2="11.303" y2="0.381" layer="21"/>
<rectangle x1="11.72718125" y1="0.29641875" x2="11.89481875" y2="0.381" layer="21"/>
<rectangle x1="12.573" y1="0.29641875" x2="12.74318125" y2="0.381" layer="21"/>
<rectangle x1="13.081" y1="0.29641875" x2="13.50518125" y2="0.381" layer="21"/>
<rectangle x1="1.905" y1="0.381" x2="2.24281875" y2="0.46558125" layer="21"/>
<rectangle x1="2.58318125" y1="0.381" x2="2.921" y2="0.46558125" layer="21"/>
<rectangle x1="3.25881875" y1="0.381" x2="3.429" y2="0.46558125" layer="21"/>
<rectangle x1="4.10718125" y1="0.381" x2="4.27481875" y2="0.46558125" layer="21"/>
<rectangle x1="4.445" y1="0.381" x2="4.61518125" y2="0.46558125" layer="21"/>
<rectangle x1="5.03681875" y1="0.381" x2="5.37718125" y2="0.46558125" layer="21"/>
<rectangle x1="5.63118125" y1="0.381" x2="6.56081875" y2="0.46558125" layer="21"/>
<rectangle x1="7.15518125" y1="0.381" x2="7.83081875" y2="0.46558125" layer="21"/>
<rectangle x1="8.17118125" y1="0.381" x2="8.763" y2="0.46558125" layer="21"/>
<rectangle x1="9.10081875" y1="0.381" x2="9.779" y2="0.46558125" layer="21"/>
<rectangle x1="10.37081875" y1="0.381" x2="10.71118125" y2="0.46558125" layer="21"/>
<rectangle x1="11.049" y1="0.381" x2="11.38681875" y2="0.46558125" layer="21"/>
<rectangle x1="11.72718125" y1="0.381" x2="11.89481875" y2="0.46558125" layer="21"/>
<rectangle x1="12.573" y1="0.381" x2="12.74318125" y2="0.46558125" layer="21"/>
<rectangle x1="12.99718125" y1="0.381" x2="13.67281875" y2="0.46558125" layer="21"/>
<rectangle x1="1.82118125" y1="0.46558125" x2="2.07518125" y2="0.55041875" layer="21"/>
<rectangle x1="2.75081875" y1="0.46558125" x2="3.00481875" y2="0.55041875" layer="21"/>
<rectangle x1="3.25881875" y1="0.46558125" x2="3.429" y2="0.55041875" layer="21"/>
<rectangle x1="4.10718125" y1="0.46558125" x2="4.27481875" y2="0.55041875" layer="21"/>
<rectangle x1="4.445" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="21"/>
<rectangle x1="4.953" y1="0.46558125" x2="5.12318125" y2="0.55041875" layer="21"/>
<rectangle x1="5.63118125" y1="0.46558125" x2="5.79881875" y2="0.55041875" layer="21"/>
<rectangle x1="6.39318125" y1="0.46558125" x2="6.731" y2="0.55041875" layer="21"/>
<rectangle x1="7.06881875" y1="0.46558125" x2="7.32281875" y2="0.55041875" layer="21"/>
<rectangle x1="7.66318125" y1="0.46558125" x2="7.91718125" y2="0.55041875" layer="21"/>
<rectangle x1="8.17118125" y1="0.46558125" x2="8.33881875" y2="0.55041875" layer="21"/>
<rectangle x1="8.59281875" y1="0.46558125" x2="8.84681875" y2="0.55041875" layer="21"/>
<rectangle x1="9.017" y1="0.46558125" x2="9.271" y2="0.55041875" layer="21"/>
<rectangle x1="9.60881875" y1="0.46558125" x2="9.86281875" y2="0.55041875" layer="21"/>
<rectangle x1="10.287" y1="0.46558125" x2="10.541" y2="0.55041875" layer="21"/>
<rectangle x1="11.21918125" y1="0.46558125" x2="11.47318125" y2="0.55041875" layer="21"/>
<rectangle x1="11.72718125" y1="0.46558125" x2="11.89481875" y2="0.55041875" layer="21"/>
<rectangle x1="12.573" y1="0.46558125" x2="12.74318125" y2="0.55041875" layer="21"/>
<rectangle x1="12.91081875" y1="0.46558125" x2="13.16481875" y2="0.55041875" layer="21"/>
<rectangle x1="13.50518125" y1="0.46558125" x2="13.75918125" y2="0.55041875" layer="21"/>
<rectangle x1="1.73481875" y1="0.55041875" x2="1.98881875" y2="0.635" layer="21"/>
<rectangle x1="2.83718125" y1="0.55041875" x2="3.09118125" y2="0.635" layer="21"/>
<rectangle x1="3.25881875" y1="0.55041875" x2="3.429" y2="0.635" layer="21"/>
<rectangle x1="4.10718125" y1="0.55041875" x2="4.27481875" y2="0.635" layer="21"/>
<rectangle x1="4.445" y1="0.55041875" x2="4.61518125" y2="0.635" layer="21"/>
<rectangle x1="4.86918125" y1="0.55041875" x2="5.03681875" y2="0.635" layer="21"/>
<rectangle x1="5.63118125" y1="0.55041875" x2="5.79881875" y2="0.635" layer="21"/>
<rectangle x1="6.56081875" y1="0.55041875" x2="6.81481875" y2="0.635" layer="21"/>
<rectangle x1="7.06881875" y1="0.55041875" x2="7.239" y2="0.635" layer="21"/>
<rectangle x1="7.747" y1="0.55041875" x2="8.001" y2="0.635" layer="21"/>
<rectangle x1="8.17118125" y1="0.55041875" x2="8.255" y2="0.635" layer="21"/>
<rectangle x1="8.67918125" y1="0.55041875" x2="8.84681875" y2="0.635" layer="21"/>
<rectangle x1="8.93318125" y1="0.55041875" x2="9.18718125" y2="0.635" layer="21"/>
<rectangle x1="9.69518125" y1="0.55041875" x2="9.86281875" y2="0.635" layer="21"/>
<rectangle x1="10.20318125" y1="0.55041875" x2="10.45718125" y2="0.635" layer="21"/>
<rectangle x1="11.303" y1="0.55041875" x2="11.557" y2="0.635" layer="21"/>
<rectangle x1="11.72718125" y1="0.55041875" x2="11.89481875" y2="0.635" layer="21"/>
<rectangle x1="12.573" y1="0.55041875" x2="12.74318125" y2="0.635" layer="21"/>
<rectangle x1="12.827" y1="0.55041875" x2="13.081" y2="0.635" layer="21"/>
<rectangle x1="13.589" y1="0.55041875" x2="13.75918125" y2="0.635" layer="21"/>
<rectangle x1="1.73481875" y1="0.635" x2="1.905" y2="0.71958125" layer="21"/>
<rectangle x1="2.921" y1="0.635" x2="3.09118125" y2="0.71958125" layer="21"/>
<rectangle x1="3.25881875" y1="0.635" x2="3.429" y2="0.71958125" layer="21"/>
<rectangle x1="4.10718125" y1="0.635" x2="4.27481875" y2="0.71958125" layer="21"/>
<rectangle x1="4.445" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="4.86918125" y1="0.635" x2="5.03681875" y2="0.71958125" layer="21"/>
<rectangle x1="5.63118125" y1="0.635" x2="5.79881875" y2="0.71958125" layer="21"/>
<rectangle x1="6.64718125" y1="0.635" x2="6.81481875" y2="0.71958125" layer="21"/>
<rectangle x1="6.985" y1="0.635" x2="7.239" y2="0.71958125" layer="21"/>
<rectangle x1="7.83081875" y1="0.635" x2="8.001" y2="0.71958125" layer="21"/>
<rectangle x1="8.59281875" y1="0.635" x2="8.84681875" y2="0.71958125" layer="21"/>
<rectangle x1="8.93318125" y1="0.635" x2="9.10081875" y2="0.71958125" layer="21"/>
<rectangle x1="10.20318125" y1="0.635" x2="10.37081875" y2="0.71958125" layer="21"/>
<rectangle x1="11.38681875" y1="0.635" x2="11.557" y2="0.71958125" layer="21"/>
<rectangle x1="11.72718125" y1="0.635" x2="11.89481875" y2="0.71958125" layer="21"/>
<rectangle x1="12.573" y1="0.635" x2="12.74318125" y2="0.71958125" layer="21"/>
<rectangle x1="12.827" y1="0.635" x2="12.99718125" y2="0.71958125" layer="21"/>
<rectangle x1="1.73481875" y1="0.71958125" x2="1.905" y2="0.80441875" layer="21"/>
<rectangle x1="2.921" y1="0.71958125" x2="3.175" y2="0.80441875" layer="21"/>
<rectangle x1="3.25881875" y1="0.71958125" x2="3.429" y2="0.80441875" layer="21"/>
<rectangle x1="4.10718125" y1="0.71958125" x2="4.27481875" y2="0.80441875" layer="21"/>
<rectangle x1="4.445" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="4.78281875" y1="0.71958125" x2="4.953" y2="0.80441875" layer="21"/>
<rectangle x1="5.63118125" y1="0.71958125" x2="5.79881875" y2="0.80441875" layer="21"/>
<rectangle x1="6.731" y1="0.71958125" x2="6.90118125" y2="0.80441875" layer="21"/>
<rectangle x1="6.985" y1="0.71958125" x2="7.15518125" y2="0.80441875" layer="21"/>
<rectangle x1="7.83081875" y1="0.71958125" x2="8.001" y2="0.80441875" layer="21"/>
<rectangle x1="8.33881875" y1="0.71958125" x2="8.763" y2="0.80441875" layer="21"/>
<rectangle x1="8.93318125" y1="0.71958125" x2="9.94918125" y2="0.80441875" layer="21"/>
<rectangle x1="10.20318125" y1="0.71958125" x2="10.37081875" y2="0.80441875" layer="21"/>
<rectangle x1="11.38681875" y1="0.71958125" x2="11.557" y2="0.80441875" layer="21"/>
<rectangle x1="11.72718125" y1="0.71958125" x2="11.89481875" y2="0.80441875" layer="21"/>
<rectangle x1="12.573" y1="0.71958125" x2="12.74318125" y2="0.80441875" layer="21"/>
<rectangle x1="12.827" y1="0.71958125" x2="13.843" y2="0.80441875" layer="21"/>
<rectangle x1="1.651" y1="0.80441875" x2="1.82118125" y2="0.889" layer="21"/>
<rectangle x1="3.00481875" y1="0.80441875" x2="3.175" y2="0.889" layer="21"/>
<rectangle x1="3.25881875" y1="0.80441875" x2="3.429" y2="0.889" layer="21"/>
<rectangle x1="4.10718125" y1="0.80441875" x2="4.27481875" y2="0.889" layer="21"/>
<rectangle x1="4.445" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="4.78281875" y1="0.80441875" x2="4.953" y2="0.889" layer="21"/>
<rectangle x1="5.63118125" y1="0.80441875" x2="5.79881875" y2="0.889" layer="21"/>
<rectangle x1="6.731" y1="0.80441875" x2="6.90118125" y2="0.889" layer="21"/>
<rectangle x1="6.985" y1="0.80441875" x2="7.15518125" y2="0.889" layer="21"/>
<rectangle x1="7.83081875" y1="0.80441875" x2="8.001" y2="0.889" layer="21"/>
<rectangle x1="8.17118125" y1="0.80441875" x2="8.59281875" y2="0.889" layer="21"/>
<rectangle x1="8.93318125" y1="0.80441875" x2="9.94918125" y2="0.889" layer="21"/>
<rectangle x1="10.11681875" y1="0.80441875" x2="10.287" y2="0.889" layer="21"/>
<rectangle x1="11.47318125" y1="0.80441875" x2="11.64081875" y2="0.889" layer="21"/>
<rectangle x1="11.72718125" y1="0.80441875" x2="11.89481875" y2="0.889" layer="21"/>
<rectangle x1="12.573" y1="0.80441875" x2="12.74318125" y2="0.889" layer="21"/>
<rectangle x1="12.827" y1="0.80441875" x2="13.843" y2="0.889" layer="21"/>
<rectangle x1="1.651" y1="0.889" x2="1.82118125" y2="0.97358125" layer="21"/>
<rectangle x1="3.00481875" y1="0.889" x2="3.175" y2="0.97358125" layer="21"/>
<rectangle x1="3.25881875" y1="0.889" x2="3.51281875" y2="0.97358125" layer="21"/>
<rectangle x1="4.10718125" y1="0.889" x2="4.27481875" y2="0.97358125" layer="21"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.61518125" y2="0.97358125" layer="21"/>
<rectangle x1="4.78281875" y1="0.889" x2="4.953" y2="0.97358125" layer="21"/>
<rectangle x1="5.63118125" y1="0.889" x2="5.79881875" y2="0.97358125" layer="21"/>
<rectangle x1="6.731" y1="0.889" x2="6.90118125" y2="0.97358125" layer="21"/>
<rectangle x1="6.985" y1="0.889" x2="7.239" y2="0.97358125" layer="21"/>
<rectangle x1="7.83081875" y1="0.889" x2="8.001" y2="0.97358125" layer="21"/>
<rectangle x1="8.08481875" y1="0.889" x2="8.42518125" y2="0.97358125" layer="21"/>
<rectangle x1="8.93318125" y1="0.889" x2="9.10081875" y2="0.97358125" layer="21"/>
<rectangle x1="9.779" y1="0.889" x2="9.94918125" y2="0.97358125" layer="21"/>
<rectangle x1="10.11681875" y1="0.889" x2="10.287" y2="0.97358125" layer="21"/>
<rectangle x1="11.47318125" y1="0.889" x2="11.64081875" y2="0.97358125" layer="21"/>
<rectangle x1="11.72718125" y1="0.889" x2="11.89481875" y2="0.97358125" layer="21"/>
<rectangle x1="12.48918125" y1="0.889" x2="12.74318125" y2="0.97358125" layer="21"/>
<rectangle x1="12.827" y1="0.889" x2="12.99718125" y2="0.97358125" layer="21"/>
<rectangle x1="13.589" y1="0.889" x2="13.843" y2="0.97358125" layer="21"/>
<rectangle x1="1.651" y1="0.97358125" x2="1.82118125" y2="1.05841875" layer="21"/>
<rectangle x1="3.00481875" y1="0.97358125" x2="3.175" y2="1.05841875" layer="21"/>
<rectangle x1="3.25881875" y1="0.97358125" x2="3.51281875" y2="1.05841875" layer="21"/>
<rectangle x1="4.02081875" y1="0.97358125" x2="4.27481875" y2="1.05841875" layer="21"/>
<rectangle x1="4.36118125" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="21"/>
<rectangle x1="4.78281875" y1="0.97358125" x2="4.953" y2="1.05841875" layer="21"/>
<rectangle x1="5.63118125" y1="0.97358125" x2="5.79881875" y2="1.05841875" layer="21"/>
<rectangle x1="6.731" y1="0.97358125" x2="6.90118125" y2="1.05841875" layer="21"/>
<rectangle x1="7.06881875" y1="0.97358125" x2="7.239" y2="1.05841875" layer="21"/>
<rectangle x1="7.747" y1="0.97358125" x2="8.001" y2="1.05841875" layer="21"/>
<rectangle x1="8.08481875" y1="0.97358125" x2="8.255" y2="1.05841875" layer="21"/>
<rectangle x1="8.67918125" y1="0.97358125" x2="8.84681875" y2="1.05841875" layer="21"/>
<rectangle x1="8.93318125" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="21"/>
<rectangle x1="9.69518125" y1="0.97358125" x2="9.86281875" y2="1.05841875" layer="21"/>
<rectangle x1="10.11681875" y1="0.97358125" x2="10.287" y2="1.05841875" layer="21"/>
<rectangle x1="11.47318125" y1="0.97358125" x2="11.64081875" y2="1.05841875" layer="21"/>
<rectangle x1="11.72718125" y1="0.97358125" x2="11.98118125" y2="1.05841875" layer="21"/>
<rectangle x1="12.48918125" y1="0.97358125" x2="12.65681875" y2="1.05841875" layer="21"/>
<rectangle x1="12.827" y1="0.97358125" x2="12.99718125" y2="1.05841875" layer="21"/>
<rectangle x1="13.589" y1="0.97358125" x2="13.75918125" y2="1.05841875" layer="21"/>
<rectangle x1="1.651" y1="1.05841875" x2="1.82118125" y2="1.143" layer="21"/>
<rectangle x1="3.00481875" y1="1.05841875" x2="3.175" y2="1.143" layer="21"/>
<rectangle x1="3.25881875" y1="1.05841875" x2="3.59918125" y2="1.143" layer="21"/>
<rectangle x1="3.937" y1="1.05841875" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="4.36118125" y1="1.05841875" x2="4.61518125" y2="1.143" layer="21"/>
<rectangle x1="4.78281875" y1="1.05841875" x2="4.953" y2="1.143" layer="21"/>
<rectangle x1="5.63118125" y1="1.05841875" x2="5.79881875" y2="1.143" layer="21"/>
<rectangle x1="6.731" y1="1.05841875" x2="6.90118125" y2="1.143" layer="21"/>
<rectangle x1="7.06881875" y1="1.05841875" x2="7.32281875" y2="1.143" layer="21"/>
<rectangle x1="7.66318125" y1="1.05841875" x2="7.91718125" y2="1.143" layer="21"/>
<rectangle x1="8.08481875" y1="1.05841875" x2="8.33881875" y2="1.143" layer="21"/>
<rectangle x1="8.59281875" y1="1.05841875" x2="8.84681875" y2="1.143" layer="21"/>
<rectangle x1="9.017" y1="1.05841875" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="9.60881875" y1="1.05841875" x2="9.86281875" y2="1.143" layer="21"/>
<rectangle x1="10.11681875" y1="1.05841875" x2="10.287" y2="1.143" layer="21"/>
<rectangle x1="11.47318125" y1="1.05841875" x2="11.64081875" y2="1.143" layer="21"/>
<rectangle x1="11.72718125" y1="1.05841875" x2="12.065" y2="1.143" layer="21"/>
<rectangle x1="12.40281875" y1="1.05841875" x2="12.65681875" y2="1.143" layer="21"/>
<rectangle x1="12.91081875" y1="1.05841875" x2="13.16481875" y2="1.143" layer="21"/>
<rectangle x1="13.50518125" y1="1.05841875" x2="13.75918125" y2="1.143" layer="21"/>
<rectangle x1="1.651" y1="1.143" x2="1.82118125" y2="1.22758125" layer="21"/>
<rectangle x1="3.00481875" y1="1.143" x2="3.175" y2="1.22758125" layer="21"/>
<rectangle x1="3.25881875" y1="1.143" x2="4.10718125" y2="1.22758125" layer="21"/>
<rectangle x1="4.36118125" y1="1.143" x2="4.61518125" y2="1.22758125" layer="21"/>
<rectangle x1="4.699" y1="1.143" x2="5.37718125" y2="1.22758125" layer="21"/>
<rectangle x1="5.63118125" y1="1.143" x2="5.79881875" y2="1.22758125" layer="21"/>
<rectangle x1="6.731" y1="1.143" x2="6.90118125" y2="1.22758125" layer="21"/>
<rectangle x1="7.15518125" y1="1.143" x2="7.83081875" y2="1.22758125" layer="21"/>
<rectangle x1="8.17118125" y1="1.143" x2="8.763" y2="1.22758125" layer="21"/>
<rectangle x1="9.10081875" y1="1.143" x2="9.779" y2="1.22758125" layer="21"/>
<rectangle x1="10.20318125" y1="1.143" x2="10.37081875" y2="1.22758125" layer="21"/>
<rectangle x1="11.38681875" y1="1.143" x2="11.557" y2="1.22758125" layer="21"/>
<rectangle x1="11.72718125" y1="1.143" x2="12.573" y2="1.22758125" layer="21"/>
<rectangle x1="12.99718125" y1="1.143" x2="13.67281875" y2="1.22758125" layer="21"/>
<rectangle x1="1.651" y1="1.22758125" x2="1.82118125" y2="1.31241875" layer="21"/>
<rectangle x1="3.00481875" y1="1.22758125" x2="3.175" y2="1.31241875" layer="21"/>
<rectangle x1="3.25881875" y1="1.22758125" x2="3.429" y2="1.31241875" layer="21"/>
<rectangle x1="3.59918125" y1="1.22758125" x2="4.02081875" y2="1.31241875" layer="21"/>
<rectangle x1="4.445" y1="1.22758125" x2="4.52881875" y2="1.31241875" layer="21"/>
<rectangle x1="4.699" y1="1.22758125" x2="5.37718125" y2="1.31241875" layer="21"/>
<rectangle x1="5.63118125" y1="1.22758125" x2="5.79881875" y2="1.31241875" layer="21"/>
<rectangle x1="6.731" y1="1.22758125" x2="6.90118125" y2="1.31241875" layer="21"/>
<rectangle x1="7.32281875" y1="1.22758125" x2="7.747" y2="1.31241875" layer="21"/>
<rectangle x1="8.255" y1="1.22758125" x2="8.67918125" y2="1.31241875" layer="21"/>
<rectangle x1="9.18718125" y1="1.22758125" x2="9.60881875" y2="1.31241875" layer="21"/>
<rectangle x1="10.20318125" y1="1.22758125" x2="10.37081875" y2="1.31241875" layer="21"/>
<rectangle x1="11.38681875" y1="1.22758125" x2="11.557" y2="1.31241875" layer="21"/>
<rectangle x1="11.72718125" y1="1.22758125" x2="11.89481875" y2="1.31241875" layer="21"/>
<rectangle x1="11.98118125" y1="1.22758125" x2="12.40281875" y2="1.31241875" layer="21"/>
<rectangle x1="13.081" y1="1.22758125" x2="13.50518125" y2="1.31241875" layer="21"/>
<rectangle x1="1.651" y1="1.31241875" x2="1.82118125" y2="1.397" layer="21"/>
<rectangle x1="3.00481875" y1="1.31241875" x2="3.175" y2="1.397" layer="21"/>
<rectangle x1="4.445" y1="1.31241875" x2="4.52881875" y2="1.397" layer="21"/>
<rectangle x1="4.78281875" y1="1.31241875" x2="4.953" y2="1.397" layer="21"/>
<rectangle x1="5.63118125" y1="1.31241875" x2="5.79881875" y2="1.397" layer="21"/>
<rectangle x1="6.64718125" y1="1.31241875" x2="6.90118125" y2="1.397" layer="21"/>
<rectangle x1="10.20318125" y1="1.31241875" x2="10.45718125" y2="1.397" layer="21"/>
<rectangle x1="11.303" y1="1.31241875" x2="11.557" y2="1.397" layer="21"/>
<rectangle x1="1.651" y1="1.397" x2="1.82118125" y2="1.48158125" layer="21"/>
<rectangle x1="3.00481875" y1="1.397" x2="3.175" y2="1.48158125" layer="21"/>
<rectangle x1="4.36118125" y1="1.397" x2="4.61518125" y2="1.48158125" layer="21"/>
<rectangle x1="4.78281875" y1="1.397" x2="4.953" y2="1.48158125" layer="21"/>
<rectangle x1="5.63118125" y1="1.397" x2="5.79881875" y2="1.48158125" layer="21"/>
<rectangle x1="6.56081875" y1="1.397" x2="6.81481875" y2="1.48158125" layer="21"/>
<rectangle x1="10.287" y1="1.397" x2="10.541" y2="1.48158125" layer="21"/>
<rectangle x1="11.21918125" y1="1.397" x2="11.47318125" y2="1.48158125" layer="21"/>
<rectangle x1="1.651" y1="1.48158125" x2="1.82118125" y2="1.56641875" layer="21"/>
<rectangle x1="3.00481875" y1="1.48158125" x2="3.175" y2="1.56641875" layer="21"/>
<rectangle x1="4.445" y1="1.48158125" x2="4.52881875" y2="1.56641875" layer="21"/>
<rectangle x1="4.78281875" y1="1.48158125" x2="4.953" y2="1.56641875" layer="21"/>
<rectangle x1="5.63118125" y1="1.48158125" x2="5.79881875" y2="1.56641875" layer="21"/>
<rectangle x1="6.477" y1="1.48158125" x2="6.731" y2="1.56641875" layer="21"/>
<rectangle x1="10.37081875" y1="1.48158125" x2="10.71118125" y2="1.56641875" layer="21"/>
<rectangle x1="11.049" y1="1.48158125" x2="11.38681875" y2="1.56641875" layer="21"/>
<rectangle x1="1.651" y1="1.56641875" x2="1.82118125" y2="1.651" layer="21"/>
<rectangle x1="3.00481875" y1="1.56641875" x2="3.175" y2="1.651" layer="21"/>
<rectangle x1="4.78281875" y1="1.56641875" x2="4.953" y2="1.651" layer="21"/>
<rectangle x1="5.63118125" y1="1.56641875" x2="5.79881875" y2="1.651" layer="21"/>
<rectangle x1="5.88518125" y1="1.56641875" x2="6.64718125" y2="1.651" layer="21"/>
<rectangle x1="10.541" y1="1.56641875" x2="11.21918125" y2="1.651" layer="21"/>
<rectangle x1="1.651" y1="1.651" x2="1.82118125" y2="1.73558125" layer="21"/>
<rectangle x1="3.00481875" y1="1.651" x2="3.175" y2="1.73558125" layer="21"/>
<rectangle x1="4.86918125" y1="1.651" x2="4.953" y2="1.73558125" layer="21"/>
<rectangle x1="5.63118125" y1="1.651" x2="6.56081875" y2="1.73558125" layer="21"/>
<rectangle x1="10.71118125" y1="1.651" x2="11.049" y2="1.73558125" layer="21"/>
<rectangle x1="5.715" y1="1.73558125" x2="6.30681875" y2="1.82041875" layer="21"/>
<rectangle x1="2.49681875" y1="2.413" x2="12.573" y2="2.49758125" layer="21"/>
<rectangle x1="2.159" y1="2.49758125" x2="12.91081875" y2="2.58241875" layer="21"/>
<rectangle x1="1.905" y1="2.58241875" x2="13.16481875" y2="2.667" layer="21"/>
<rectangle x1="1.73481875" y1="2.667" x2="13.335" y2="2.75158125" layer="21"/>
<rectangle x1="1.56718125" y1="2.75158125" x2="13.50518125" y2="2.83641875" layer="21"/>
<rectangle x1="1.397" y1="2.83641875" x2="2.667" y2="2.921" layer="21"/>
<rectangle x1="7.32281875" y1="2.83641875" x2="13.589" y2="2.921" layer="21"/>
<rectangle x1="1.31318125" y1="2.921" x2="2.32918125" y2="3.00558125" layer="21"/>
<rectangle x1="7.32281875" y1="2.921" x2="13.75918125" y2="3.00558125" layer="21"/>
<rectangle x1="1.22681875" y1="3.00558125" x2="2.07518125" y2="3.09041875" layer="21"/>
<rectangle x1="7.239" y1="3.00558125" x2="13.843" y2="3.09041875" layer="21"/>
<rectangle x1="1.05918125" y1="3.09041875" x2="1.905" y2="3.175" layer="21"/>
<rectangle x1="7.15518125" y1="3.09041875" x2="13.92681875" y2="3.175" layer="21"/>
<rectangle x1="0.97281875" y1="3.175" x2="1.73481875" y2="3.25958125" layer="21"/>
<rectangle x1="7.15518125" y1="3.175" x2="14.097" y2="3.25958125" layer="21"/>
<rectangle x1="0.889" y1="3.25958125" x2="1.56718125" y2="3.34441875" layer="21"/>
<rectangle x1="7.15518125" y1="3.25958125" x2="14.18081875" y2="3.34441875" layer="21"/>
<rectangle x1="0.80518125" y1="3.34441875" x2="1.48081875" y2="3.429" layer="21"/>
<rectangle x1="7.06881875" y1="3.34441875" x2="14.26718125" y2="3.429" layer="21"/>
<rectangle x1="0.71881875" y1="3.429" x2="1.397" y2="3.51358125" layer="21"/>
<rectangle x1="7.06881875" y1="3.429" x2="14.26718125" y2="3.51358125" layer="21"/>
<rectangle x1="0.71881875" y1="3.51358125" x2="1.31318125" y2="3.59841875" layer="21"/>
<rectangle x1="6.985" y1="3.51358125" x2="14.351" y2="3.59841875" layer="21"/>
<rectangle x1="0.635" y1="3.59841875" x2="1.22681875" y2="3.683" layer="21"/>
<rectangle x1="6.985" y1="3.59841875" x2="14.43481875" y2="3.683" layer="21"/>
<rectangle x1="0.55118125" y1="3.683" x2="1.143" y2="3.76758125" layer="21"/>
<rectangle x1="6.985" y1="3.683" x2="14.52118125" y2="3.76758125" layer="21"/>
<rectangle x1="0.46481875" y1="3.76758125" x2="1.05918125" y2="3.85241875" layer="21"/>
<rectangle x1="6.90118125" y1="3.76758125" x2="14.52118125" y2="3.85241875" layer="21"/>
<rectangle x1="0.46481875" y1="3.85241875" x2="0.97281875" y2="3.937" layer="21"/>
<rectangle x1="6.90118125" y1="3.85241875" x2="14.605" y2="3.937" layer="21"/>
<rectangle x1="0.381" y1="3.937" x2="0.889" y2="4.02158125" layer="21"/>
<rectangle x1="6.90118125" y1="3.937" x2="14.68881875" y2="4.02158125" layer="21"/>
<rectangle x1="0.381" y1="4.02158125" x2="0.889" y2="4.10641875" layer="21"/>
<rectangle x1="6.90118125" y1="4.02158125" x2="14.68881875" y2="4.10641875" layer="21"/>
<rectangle x1="0.29718125" y1="4.10641875" x2="0.80518125" y2="4.191" layer="21"/>
<rectangle x1="6.81481875" y1="4.10641875" x2="14.77518125" y2="4.191" layer="21"/>
<rectangle x1="0.29718125" y1="4.191" x2="0.80518125" y2="4.27558125" layer="21"/>
<rectangle x1="2.58318125" y1="4.191" x2="3.51281875" y2="4.27558125" layer="21"/>
<rectangle x1="4.27481875" y1="4.191" x2="5.461" y2="4.27558125" layer="21"/>
<rectangle x1="6.81481875" y1="4.191" x2="8.17118125" y2="4.27558125" layer="21"/>
<rectangle x1="8.763" y1="4.191" x2="10.033" y2="4.27558125" layer="21"/>
<rectangle x1="10.11681875" y1="4.191" x2="11.38681875" y2="4.27558125" layer="21"/>
<rectangle x1="11.47318125" y1="4.191" x2="12.48918125" y2="4.27558125" layer="21"/>
<rectangle x1="12.65681875" y1="4.191" x2="14.77518125" y2="4.27558125" layer="21"/>
<rectangle x1="0.21081875" y1="4.27558125" x2="0.71881875" y2="4.36041875" layer="21"/>
<rectangle x1="2.413" y1="4.27558125" x2="3.683" y2="4.36041875" layer="21"/>
<rectangle x1="4.27481875" y1="4.27558125" x2="5.63118125" y2="4.36041875" layer="21"/>
<rectangle x1="6.81481875" y1="4.27558125" x2="7.91718125" y2="4.36041875" layer="21"/>
<rectangle x1="9.017" y1="4.27558125" x2="9.94918125" y2="4.36041875" layer="21"/>
<rectangle x1="10.20318125" y1="4.27558125" x2="11.303" y2="4.36041875" layer="21"/>
<rectangle x1="11.557" y1="4.27558125" x2="12.23518125" y2="4.36041875" layer="21"/>
<rectangle x1="12.99718125" y1="4.27558125" x2="14.859" y2="4.36041875" layer="21"/>
<rectangle x1="0.21081875" y1="4.36041875" x2="0.71881875" y2="4.445" layer="21"/>
<rectangle x1="2.32918125" y1="4.36041875" x2="3.76681875" y2="4.445" layer="21"/>
<rectangle x1="4.27481875" y1="4.36041875" x2="5.715" y2="4.445" layer="21"/>
<rectangle x1="6.81481875" y1="4.36041875" x2="7.747" y2="4.445" layer="21"/>
<rectangle x1="9.18718125" y1="4.36041875" x2="9.94918125" y2="4.445" layer="21"/>
<rectangle x1="10.287" y1="4.36041875" x2="11.303" y2="4.445" layer="21"/>
<rectangle x1="11.557" y1="4.36041875" x2="12.065" y2="4.445" layer="21"/>
<rectangle x1="13.16481875" y1="4.36041875" x2="14.859" y2="4.445" layer="21"/>
<rectangle x1="0.21081875" y1="4.445" x2="0.635" y2="4.52958125" layer="21"/>
<rectangle x1="2.159" y1="4.445" x2="3.85318125" y2="4.52958125" layer="21"/>
<rectangle x1="4.27481875" y1="4.445" x2="5.79881875" y2="4.52958125" layer="21"/>
<rectangle x1="6.81481875" y1="4.445" x2="7.66318125" y2="4.52958125" layer="21"/>
<rectangle x1="9.271" y1="4.445" x2="9.94918125" y2="4.52958125" layer="21"/>
<rectangle x1="10.287" y1="4.445" x2="11.303" y2="4.52958125" layer="21"/>
<rectangle x1="11.557" y1="4.445" x2="11.98118125" y2="4.52958125" layer="21"/>
<rectangle x1="13.25118125" y1="4.445" x2="14.859" y2="4.52958125" layer="21"/>
<rectangle x1="0.127" y1="4.52958125" x2="0.635" y2="4.61441875" layer="21"/>
<rectangle x1="2.07518125" y1="4.52958125" x2="3.937" y2="4.61441875" layer="21"/>
<rectangle x1="4.36118125" y1="4.52958125" x2="5.88518125" y2="4.61441875" layer="21"/>
<rectangle x1="6.81481875" y1="4.52958125" x2="7.57681875" y2="4.61441875" layer="21"/>
<rectangle x1="8.33881875" y1="4.52958125" x2="8.59281875" y2="4.61441875" layer="21"/>
<rectangle x1="9.35481875" y1="4.52958125" x2="9.94918125" y2="4.61441875" layer="21"/>
<rectangle x1="10.287" y1="4.52958125" x2="11.303" y2="4.61441875" layer="21"/>
<rectangle x1="11.557" y1="4.52958125" x2="11.89481875" y2="4.61441875" layer="21"/>
<rectangle x1="12.40281875" y1="4.52958125" x2="12.91081875" y2="4.61441875" layer="21"/>
<rectangle x1="13.335" y1="4.52958125" x2="14.94281875" y2="4.61441875" layer="21"/>
<rectangle x1="0.127" y1="4.61441875" x2="0.635" y2="4.699" layer="21"/>
<rectangle x1="2.07518125" y1="4.61441875" x2="2.75081875" y2="4.699" layer="21"/>
<rectangle x1="3.34518125" y1="4.61441875" x2="4.02081875" y2="4.699" layer="21"/>
<rectangle x1="5.29081875" y1="4.61441875" x2="5.969" y2="4.699" layer="21"/>
<rectangle x1="6.81481875" y1="4.61441875" x2="7.493" y2="4.699" layer="21"/>
<rectangle x1="8.08481875" y1="4.61441875" x2="8.84681875" y2="4.699" layer="21"/>
<rectangle x1="9.44118125" y1="4.61441875" x2="9.94918125" y2="4.699" layer="21"/>
<rectangle x1="10.287" y1="4.61441875" x2="11.303" y2="4.699" layer="21"/>
<rectangle x1="11.557" y1="4.61441875" x2="11.89481875" y2="4.699" layer="21"/>
<rectangle x1="12.23518125" y1="4.61441875" x2="12.99718125" y2="4.699" layer="21"/>
<rectangle x1="13.335" y1="4.61441875" x2="14.94281875" y2="4.699" layer="21"/>
<rectangle x1="0.127" y1="4.699" x2="0.55118125" y2="4.78358125" layer="21"/>
<rectangle x1="1.98881875" y1="4.699" x2="2.58318125" y2="4.78358125" layer="21"/>
<rectangle x1="3.51281875" y1="4.699" x2="4.10718125" y2="4.78358125" layer="21"/>
<rectangle x1="5.461" y1="4.699" x2="6.05281875" y2="4.78358125" layer="21"/>
<rectangle x1="6.731" y1="4.699" x2="7.40918125" y2="4.78358125" layer="21"/>
<rectangle x1="7.91718125" y1="4.699" x2="9.017" y2="4.78358125" layer="21"/>
<rectangle x1="9.44118125" y1="4.699" x2="9.94918125" y2="4.78358125" layer="21"/>
<rectangle x1="10.287" y1="4.699" x2="11.303" y2="4.78358125" layer="21"/>
<rectangle x1="11.557" y1="4.699" x2="11.811" y2="4.78358125" layer="21"/>
<rectangle x1="12.14881875" y1="4.699" x2="13.081" y2="4.78358125" layer="21"/>
<rectangle x1="13.25118125" y1="4.699" x2="14.94281875" y2="4.78358125" layer="21"/>
<rectangle x1="0.04318125" y1="4.78358125" x2="0.55118125" y2="4.86841875" layer="21"/>
<rectangle x1="1.905" y1="4.78358125" x2="2.49681875" y2="4.86841875" layer="21"/>
<rectangle x1="3.59918125" y1="4.78358125" x2="4.191" y2="4.86841875" layer="21"/>
<rectangle x1="5.54481875" y1="4.78358125" x2="6.13918125" y2="4.86841875" layer="21"/>
<rectangle x1="6.731" y1="4.78358125" x2="7.40918125" y2="4.86841875" layer="21"/>
<rectangle x1="7.83081875" y1="4.78358125" x2="9.10081875" y2="4.86841875" layer="21"/>
<rectangle x1="9.525" y1="4.78358125" x2="9.94918125" y2="4.86841875" layer="21"/>
<rectangle x1="10.287" y1="4.78358125" x2="11.303" y2="4.86841875" layer="21"/>
<rectangle x1="11.557" y1="4.78358125" x2="11.811" y2="4.86841875" layer="21"/>
<rectangle x1="12.14881875" y1="4.78358125" x2="14.94281875" y2="4.86841875" layer="21"/>
<rectangle x1="0.04318125" y1="4.86841875" x2="0.55118125" y2="4.953" layer="21"/>
<rectangle x1="1.905" y1="4.86841875" x2="2.413" y2="4.953" layer="21"/>
<rectangle x1="3.683" y1="4.86841875" x2="4.191" y2="4.953" layer="21"/>
<rectangle x1="5.63118125" y1="4.86841875" x2="6.13918125" y2="4.953" layer="21"/>
<rectangle x1="6.731" y1="4.86841875" x2="7.32281875" y2="4.953" layer="21"/>
<rectangle x1="7.747" y1="4.86841875" x2="9.10081875" y2="4.953" layer="21"/>
<rectangle x1="9.60881875" y1="4.86841875" x2="9.94918125" y2="4.953" layer="21"/>
<rectangle x1="10.287" y1="4.86841875" x2="11.303" y2="4.953" layer="21"/>
<rectangle x1="11.557" y1="4.86841875" x2="11.811" y2="4.953" layer="21"/>
<rectangle x1="13.335" y1="4.86841875" x2="14.94281875" y2="4.953" layer="21"/>
<rectangle x1="0.04318125" y1="4.953" x2="0.55118125" y2="5.03758125" layer="21"/>
<rectangle x1="1.82118125" y1="4.953" x2="2.32918125" y2="5.03758125" layer="21"/>
<rectangle x1="3.76681875" y1="4.953" x2="4.191" y2="5.03758125" layer="21"/>
<rectangle x1="5.715" y1="4.953" x2="6.13918125" y2="5.03758125" layer="21"/>
<rectangle x1="6.731" y1="4.953" x2="7.32281875" y2="5.03758125" layer="21"/>
<rectangle x1="7.747" y1="4.953" x2="9.18718125" y2="5.03758125" layer="21"/>
<rectangle x1="9.60881875" y1="4.953" x2="9.94918125" y2="5.03758125" layer="21"/>
<rectangle x1="10.287" y1="4.953" x2="11.303" y2="5.03758125" layer="21"/>
<rectangle x1="11.557" y1="4.953" x2="11.811" y2="5.03758125" layer="21"/>
<rectangle x1="13.41881875" y1="4.953" x2="14.94281875" y2="5.03758125" layer="21"/>
<rectangle x1="0.04318125" y1="5.03758125" x2="0.46481875" y2="5.12241875" layer="21"/>
<rectangle x1="1.82118125" y1="5.03758125" x2="2.32918125" y2="5.12241875" layer="21"/>
<rectangle x1="3.76681875" y1="5.03758125" x2="4.27481875" y2="5.12241875" layer="21"/>
<rectangle x1="5.715" y1="5.03758125" x2="6.223" y2="5.12241875" layer="21"/>
<rectangle x1="6.731" y1="5.03758125" x2="7.32281875" y2="5.12241875" layer="21"/>
<rectangle x1="7.66318125" y1="5.03758125" x2="9.271" y2="5.12241875" layer="21"/>
<rectangle x1="9.60881875" y1="5.03758125" x2="9.94918125" y2="5.12241875" layer="21"/>
<rectangle x1="10.287" y1="5.03758125" x2="11.303" y2="5.12241875" layer="21"/>
<rectangle x1="11.557" y1="5.03758125" x2="11.811" y2="5.12241875" layer="21"/>
<rectangle x1="13.41881875" y1="5.03758125" x2="14.94281875" y2="5.12241875" layer="21"/>
<rectangle x1="0.04318125" y1="5.12241875" x2="0.46481875" y2="5.207" layer="21"/>
<rectangle x1="1.82118125" y1="5.12241875" x2="2.24281875" y2="5.207" layer="21"/>
<rectangle x1="3.76681875" y1="5.12241875" x2="4.27481875" y2="5.207" layer="21"/>
<rectangle x1="5.79881875" y1="5.12241875" x2="6.223" y2="5.207" layer="21"/>
<rectangle x1="6.731" y1="5.12241875" x2="7.239" y2="5.207" layer="21"/>
<rectangle x1="7.66318125" y1="5.12241875" x2="9.271" y2="5.207" layer="21"/>
<rectangle x1="9.69518125" y1="5.12241875" x2="9.94918125" y2="5.207" layer="21"/>
<rectangle x1="10.287" y1="5.12241875" x2="11.303" y2="5.207" layer="21"/>
<rectangle x1="11.557" y1="5.12241875" x2="11.811" y2="5.207" layer="21"/>
<rectangle x1="13.41881875" y1="5.12241875" x2="14.94281875" y2="5.207" layer="21"/>
<rectangle x1="0.04318125" y1="5.207" x2="0.46481875" y2="5.29158125" layer="21"/>
<rectangle x1="1.82118125" y1="5.207" x2="2.24281875" y2="5.29158125" layer="21"/>
<rectangle x1="3.85318125" y1="5.207" x2="4.27481875" y2="5.29158125" layer="21"/>
<rectangle x1="5.79881875" y1="5.207" x2="6.223" y2="5.29158125" layer="21"/>
<rectangle x1="6.731" y1="5.207" x2="7.239" y2="5.29158125" layer="21"/>
<rectangle x1="7.66318125" y1="5.207" x2="9.271" y2="5.29158125" layer="21"/>
<rectangle x1="9.69518125" y1="5.207" x2="9.94918125" y2="5.29158125" layer="21"/>
<rectangle x1="10.287" y1="5.207" x2="11.21918125" y2="5.29158125" layer="21"/>
<rectangle x1="11.557" y1="5.207" x2="11.811" y2="5.29158125" layer="21"/>
<rectangle x1="12.14881875" y1="5.207" x2="13.081" y2="5.29158125" layer="21"/>
<rectangle x1="13.335" y1="5.207" x2="14.94281875" y2="5.29158125" layer="21"/>
<rectangle x1="0.04318125" y1="5.29158125" x2="0.46481875" y2="5.37641875" layer="21"/>
<rectangle x1="1.82118125" y1="5.29158125" x2="2.24281875" y2="5.37641875" layer="21"/>
<rectangle x1="3.85318125" y1="5.29158125" x2="4.27481875" y2="5.37641875" layer="21"/>
<rectangle x1="5.79881875" y1="5.29158125" x2="6.223" y2="5.37641875" layer="21"/>
<rectangle x1="6.731" y1="5.29158125" x2="7.239" y2="5.37641875" layer="21"/>
<rectangle x1="7.57681875" y1="5.29158125" x2="9.271" y2="5.37641875" layer="21"/>
<rectangle x1="9.69518125" y1="5.29158125" x2="9.94918125" y2="5.37641875" layer="21"/>
<rectangle x1="10.37081875" y1="5.29158125" x2="11.21918125" y2="5.37641875" layer="21"/>
<rectangle x1="11.557" y1="5.29158125" x2="11.811" y2="5.37641875" layer="21"/>
<rectangle x1="12.14881875" y1="5.29158125" x2="12.99718125" y2="5.37641875" layer="21"/>
<rectangle x1="13.335" y1="5.29158125" x2="14.94281875" y2="5.37641875" layer="21"/>
<rectangle x1="0.04318125" y1="5.37641875" x2="0.46481875" y2="5.461" layer="21"/>
<rectangle x1="1.82118125" y1="5.37641875" x2="2.24281875" y2="5.461" layer="21"/>
<rectangle x1="3.85318125" y1="5.37641875" x2="4.27481875" y2="5.461" layer="21"/>
<rectangle x1="5.79881875" y1="5.37641875" x2="6.223" y2="5.461" layer="21"/>
<rectangle x1="6.731" y1="5.37641875" x2="7.239" y2="5.461" layer="21"/>
<rectangle x1="7.57681875" y1="5.37641875" x2="9.271" y2="5.461" layer="21"/>
<rectangle x1="9.69518125" y1="5.37641875" x2="9.94918125" y2="5.461" layer="21"/>
<rectangle x1="10.45718125" y1="5.37641875" x2="11.13281875" y2="5.461" layer="21"/>
<rectangle x1="11.47318125" y1="5.37641875" x2="11.89481875" y2="5.461" layer="21"/>
<rectangle x1="12.23518125" y1="5.37641875" x2="12.91081875" y2="5.461" layer="21"/>
<rectangle x1="13.335" y1="5.37641875" x2="14.94281875" y2="5.461" layer="21"/>
<rectangle x1="0.04318125" y1="5.461" x2="0.46481875" y2="5.54558125" layer="21"/>
<rectangle x1="1.82118125" y1="5.461" x2="2.24281875" y2="5.54558125" layer="21"/>
<rectangle x1="3.85318125" y1="5.461" x2="4.27481875" y2="5.54558125" layer="21"/>
<rectangle x1="5.79881875" y1="5.461" x2="6.223" y2="5.54558125" layer="21"/>
<rectangle x1="6.731" y1="5.461" x2="7.239" y2="5.54558125" layer="21"/>
<rectangle x1="7.57681875" y1="5.461" x2="9.271" y2="5.54558125" layer="21"/>
<rectangle x1="9.69518125" y1="5.461" x2="9.94918125" y2="5.54558125" layer="21"/>
<rectangle x1="10.541" y1="5.461" x2="10.96518125" y2="5.54558125" layer="21"/>
<rectangle x1="11.38681875" y1="5.461" x2="11.98118125" y2="5.54558125" layer="21"/>
<rectangle x1="12.40281875" y1="5.461" x2="12.827" y2="5.54558125" layer="21"/>
<rectangle x1="13.25118125" y1="5.461" x2="14.94281875" y2="5.54558125" layer="21"/>
<rectangle x1="0.04318125" y1="5.54558125" x2="0.46481875" y2="5.63041875" layer="21"/>
<rectangle x1="1.82118125" y1="5.54558125" x2="2.24281875" y2="5.63041875" layer="21"/>
<rectangle x1="3.85318125" y1="5.54558125" x2="4.27481875" y2="5.63041875" layer="21"/>
<rectangle x1="5.79881875" y1="5.54558125" x2="6.223" y2="5.63041875" layer="21"/>
<rectangle x1="6.731" y1="5.54558125" x2="7.239" y2="5.63041875" layer="21"/>
<rectangle x1="7.66318125" y1="5.54558125" x2="9.271" y2="5.63041875" layer="21"/>
<rectangle x1="9.69518125" y1="5.54558125" x2="9.94918125" y2="5.63041875" layer="21"/>
<rectangle x1="11.38681875" y1="5.54558125" x2="11.98118125" y2="5.63041875" layer="21"/>
<rectangle x1="13.16481875" y1="5.54558125" x2="14.94281875" y2="5.63041875" layer="21"/>
<rectangle x1="0.04318125" y1="5.63041875" x2="0.46481875" y2="5.715" layer="21"/>
<rectangle x1="1.82118125" y1="5.63041875" x2="2.24281875" y2="5.715" layer="21"/>
<rectangle x1="3.85318125" y1="5.63041875" x2="4.27481875" y2="5.715" layer="21"/>
<rectangle x1="5.715" y1="5.63041875" x2="6.223" y2="5.715" layer="21"/>
<rectangle x1="6.731" y1="5.63041875" x2="7.239" y2="5.715" layer="21"/>
<rectangle x1="7.66318125" y1="5.63041875" x2="9.271" y2="5.715" layer="21"/>
<rectangle x1="9.60881875" y1="5.63041875" x2="9.94918125" y2="5.715" layer="21"/>
<rectangle x1="11.21918125" y1="5.63041875" x2="12.14881875" y2="5.715" layer="21"/>
<rectangle x1="13.081" y1="5.63041875" x2="14.94281875" y2="5.715" layer="21"/>
<rectangle x1="0.04318125" y1="5.715" x2="0.55118125" y2="5.79958125" layer="21"/>
<rectangle x1="1.82118125" y1="5.715" x2="2.24281875" y2="5.79958125" layer="21"/>
<rectangle x1="3.85318125" y1="5.715" x2="4.27481875" y2="5.79958125" layer="21"/>
<rectangle x1="5.715" y1="5.715" x2="6.223" y2="5.79958125" layer="21"/>
<rectangle x1="6.731" y1="5.715" x2="7.32281875" y2="5.79958125" layer="21"/>
<rectangle x1="7.66318125" y1="5.715" x2="9.271" y2="5.79958125" layer="21"/>
<rectangle x1="9.60881875" y1="5.715" x2="9.94918125" y2="5.79958125" layer="21"/>
<rectangle x1="10.20318125" y1="5.715" x2="10.45718125" y2="5.79958125" layer="21"/>
<rectangle x1="11.13281875" y1="5.715" x2="12.23518125" y2="5.79958125" layer="21"/>
<rectangle x1="12.91081875" y1="5.715" x2="14.94281875" y2="5.79958125" layer="21"/>
<rectangle x1="0.04318125" y1="5.79958125" x2="0.55118125" y2="5.88441875" layer="21"/>
<rectangle x1="1.82118125" y1="5.79958125" x2="2.24281875" y2="5.88441875" layer="21"/>
<rectangle x1="3.85318125" y1="5.79958125" x2="4.27481875" y2="5.88441875" layer="21"/>
<rectangle x1="5.63118125" y1="5.79958125" x2="6.13918125" y2="5.88441875" layer="21"/>
<rectangle x1="6.731" y1="5.79958125" x2="7.32281875" y2="5.88441875" layer="21"/>
<rectangle x1="7.747" y1="5.79958125" x2="9.18718125" y2="5.88441875" layer="21"/>
<rectangle x1="9.60881875" y1="5.79958125" x2="14.94281875" y2="5.88441875" layer="21"/>
<rectangle x1="0.04318125" y1="5.88441875" x2="0.55118125" y2="5.969" layer="21"/>
<rectangle x1="1.82118125" y1="5.88441875" x2="2.24281875" y2="5.969" layer="21"/>
<rectangle x1="3.85318125" y1="5.88441875" x2="4.27481875" y2="5.969" layer="21"/>
<rectangle x1="5.63118125" y1="5.88441875" x2="6.13918125" y2="5.969" layer="21"/>
<rectangle x1="6.731" y1="5.88441875" x2="7.40918125" y2="5.969" layer="21"/>
<rectangle x1="7.83081875" y1="5.88441875" x2="9.10081875" y2="5.969" layer="21"/>
<rectangle x1="9.525" y1="5.88441875" x2="14.94281875" y2="5.969" layer="21"/>
<rectangle x1="0.127" y1="5.969" x2="0.55118125" y2="6.05358125" layer="21"/>
<rectangle x1="1.82118125" y1="5.969" x2="2.24281875" y2="6.05358125" layer="21"/>
<rectangle x1="3.85318125" y1="5.969" x2="4.27481875" y2="6.05358125" layer="21"/>
<rectangle x1="5.54481875" y1="5.969" x2="6.05281875" y2="6.05358125" layer="21"/>
<rectangle x1="6.731" y1="5.969" x2="7.40918125" y2="6.05358125" layer="21"/>
<rectangle x1="7.91718125" y1="5.969" x2="9.017" y2="6.05358125" layer="21"/>
<rectangle x1="9.525" y1="5.969" x2="14.94281875" y2="6.05358125" layer="21"/>
<rectangle x1="0.127" y1="6.05358125" x2="0.55118125" y2="6.13841875" layer="21"/>
<rectangle x1="1.82118125" y1="6.05358125" x2="2.24281875" y2="6.13841875" layer="21"/>
<rectangle x1="3.85318125" y1="6.05358125" x2="4.27481875" y2="6.13841875" layer="21"/>
<rectangle x1="5.37718125" y1="6.05358125" x2="6.05281875" y2="6.13841875" layer="21"/>
<rectangle x1="6.731" y1="6.05358125" x2="7.493" y2="6.13841875" layer="21"/>
<rectangle x1="8.001" y1="6.05358125" x2="8.93318125" y2="6.13841875" layer="21"/>
<rectangle x1="9.44118125" y1="6.05358125" x2="14.94281875" y2="6.13841875" layer="21"/>
<rectangle x1="0.127" y1="6.13841875" x2="0.635" y2="6.223" layer="21"/>
<rectangle x1="1.82118125" y1="6.13841875" x2="2.24281875" y2="6.223" layer="21"/>
<rectangle x1="3.85318125" y1="6.13841875" x2="4.27481875" y2="6.223" layer="21"/>
<rectangle x1="5.207" y1="6.13841875" x2="5.969" y2="6.223" layer="21"/>
<rectangle x1="6.81481875" y1="6.13841875" x2="7.493" y2="6.223" layer="21"/>
<rectangle x1="8.17118125" y1="6.13841875" x2="8.763" y2="6.223" layer="21"/>
<rectangle x1="9.35481875" y1="6.13841875" x2="14.94281875" y2="6.223" layer="21"/>
<rectangle x1="0.127" y1="6.223" x2="0.635" y2="6.30758125" layer="21"/>
<rectangle x1="1.82118125" y1="6.223" x2="2.24281875" y2="6.30758125" layer="21"/>
<rectangle x1="3.85318125" y1="6.223" x2="5.88518125" y2="6.30758125" layer="21"/>
<rectangle x1="6.81481875" y1="6.223" x2="7.57681875" y2="6.30758125" layer="21"/>
<rectangle x1="9.35481875" y1="6.223" x2="14.859" y2="6.30758125" layer="21"/>
<rectangle x1="0.21081875" y1="6.30758125" x2="0.635" y2="6.39241875" layer="21"/>
<rectangle x1="1.82118125" y1="6.30758125" x2="2.24281875" y2="6.39241875" layer="21"/>
<rectangle x1="3.85318125" y1="6.30758125" x2="5.79881875" y2="6.39241875" layer="21"/>
<rectangle x1="6.81481875" y1="6.30758125" x2="7.66318125" y2="6.39241875" layer="21"/>
<rectangle x1="9.18718125" y1="6.30758125" x2="14.859" y2="6.39241875" layer="21"/>
<rectangle x1="0.21081875" y1="6.39241875" x2="0.71881875" y2="6.477" layer="21"/>
<rectangle x1="1.82118125" y1="6.39241875" x2="2.24281875" y2="6.477" layer="21"/>
<rectangle x1="3.85318125" y1="6.39241875" x2="5.715" y2="6.477" layer="21"/>
<rectangle x1="6.81481875" y1="6.39241875" x2="7.83081875" y2="6.477" layer="21"/>
<rectangle x1="9.10081875" y1="6.39241875" x2="14.859" y2="6.477" layer="21"/>
<rectangle x1="0.21081875" y1="6.477" x2="0.71881875" y2="6.56158125" layer="21"/>
<rectangle x1="1.82118125" y1="6.477" x2="2.24281875" y2="6.56158125" layer="21"/>
<rectangle x1="3.85318125" y1="6.477" x2="5.54481875" y2="6.56158125" layer="21"/>
<rectangle x1="6.81481875" y1="6.477" x2="8.001" y2="6.56158125" layer="21"/>
<rectangle x1="8.93318125" y1="6.477" x2="14.77518125" y2="6.56158125" layer="21"/>
<rectangle x1="0.29718125" y1="6.56158125" x2="0.80518125" y2="6.64641875" layer="21"/>
<rectangle x1="1.905" y1="6.56158125" x2="2.159" y2="6.64641875" layer="21"/>
<rectangle x1="3.937" y1="6.56158125" x2="5.37718125" y2="6.64641875" layer="21"/>
<rectangle x1="6.81481875" y1="6.56158125" x2="8.33881875" y2="6.64641875" layer="21"/>
<rectangle x1="8.59281875" y1="6.56158125" x2="14.77518125" y2="6.64641875" layer="21"/>
<rectangle x1="0.29718125" y1="6.64641875" x2="0.80518125" y2="6.731" layer="21"/>
<rectangle x1="6.90118125" y1="6.64641875" x2="14.77518125" y2="6.731" layer="21"/>
<rectangle x1="0.381" y1="6.731" x2="0.889" y2="6.81558125" layer="21"/>
<rectangle x1="6.90118125" y1="6.731" x2="14.68881875" y2="6.81558125" layer="21"/>
<rectangle x1="0.381" y1="6.81558125" x2="0.97281875" y2="6.90041875" layer="21"/>
<rectangle x1="6.90118125" y1="6.81558125" x2="14.605" y2="6.90041875" layer="21"/>
<rectangle x1="0.46481875" y1="6.90041875" x2="0.97281875" y2="6.985" layer="21"/>
<rectangle x1="6.90118125" y1="6.90041875" x2="14.605" y2="6.985" layer="21"/>
<rectangle x1="0.55118125" y1="6.985" x2="1.05918125" y2="7.06958125" layer="21"/>
<rectangle x1="6.985" y1="6.985" x2="14.52118125" y2="7.06958125" layer="21"/>
<rectangle x1="0.55118125" y1="7.06958125" x2="1.143" y2="7.15441875" layer="21"/>
<rectangle x1="6.985" y1="7.06958125" x2="14.52118125" y2="7.15441875" layer="21"/>
<rectangle x1="0.635" y1="7.15441875" x2="1.22681875" y2="7.239" layer="21"/>
<rectangle x1="6.985" y1="7.15441875" x2="14.43481875" y2="7.239" layer="21"/>
<rectangle x1="0.71881875" y1="7.239" x2="1.31318125" y2="7.32358125" layer="21"/>
<rectangle x1="6.985" y1="7.239" x2="14.351" y2="7.32358125" layer="21"/>
<rectangle x1="0.80518125" y1="7.32358125" x2="1.397" y2="7.40841875" layer="21"/>
<rectangle x1="7.06881875" y1="7.32358125" x2="14.26718125" y2="7.40841875" layer="21"/>
<rectangle x1="0.889" y1="7.40841875" x2="1.48081875" y2="7.493" layer="21"/>
<rectangle x1="7.06881875" y1="7.40841875" x2="14.18081875" y2="7.493" layer="21"/>
<rectangle x1="0.889" y1="7.493" x2="1.651" y2="7.57758125" layer="21"/>
<rectangle x1="7.15518125" y1="7.493" x2="14.097" y2="7.57758125" layer="21"/>
<rectangle x1="1.05918125" y1="7.57758125" x2="1.73481875" y2="7.66241875" layer="21"/>
<rectangle x1="7.15518125" y1="7.57758125" x2="14.01318125" y2="7.66241875" layer="21"/>
<rectangle x1="1.143" y1="7.66241875" x2="1.905" y2="7.747" layer="21"/>
<rectangle x1="7.239" y1="7.66241875" x2="13.92681875" y2="7.747" layer="21"/>
<rectangle x1="1.22681875" y1="7.747" x2="2.159" y2="7.83158125" layer="21"/>
<rectangle x1="7.239" y1="7.747" x2="13.843" y2="7.83158125" layer="21"/>
<rectangle x1="1.31318125" y1="7.83158125" x2="2.413" y2="7.91641875" layer="21"/>
<rectangle x1="7.32281875" y1="7.83158125" x2="13.67281875" y2="7.91641875" layer="21"/>
<rectangle x1="1.48081875" y1="7.91641875" x2="7.239" y2="8.001" layer="21"/>
<rectangle x1="7.32281875" y1="7.91641875" x2="13.589" y2="8.001" layer="21"/>
<rectangle x1="1.651" y1="8.001" x2="13.41881875" y2="8.08558125" layer="21"/>
<rectangle x1="1.82118125" y1="8.08558125" x2="13.25118125" y2="8.17041875" layer="21"/>
<rectangle x1="1.98881875" y1="8.17041875" x2="13.081" y2="8.255" layer="21"/>
<rectangle x1="2.24281875" y1="8.255" x2="12.827" y2="8.33958125" layer="21"/>
</package>
<package name="UDO-LOGO-20MM">
<rectangle x1="3.00481875" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="14.351" y1="0.29641875" x2="14.68881875" y2="0.381" layer="21"/>
<rectangle x1="2.75081875" y1="0.381" x2="3.683" y2="0.46558125" layer="21"/>
<rectangle x1="4.445" y1="0.381" x2="4.52881875" y2="0.46558125" layer="21"/>
<rectangle x1="5.54481875" y1="0.381" x2="5.715" y2="0.46558125" layer="21"/>
<rectangle x1="5.969" y1="0.381" x2="6.05281875" y2="0.46558125" layer="21"/>
<rectangle x1="6.985" y1="0.381" x2="7.15518125" y2="0.46558125" layer="21"/>
<rectangle x1="7.57681875" y1="0.381" x2="8.42518125" y2="0.46558125" layer="21"/>
<rectangle x1="9.94918125" y1="0.381" x2="10.20318125" y2="0.46558125" layer="21"/>
<rectangle x1="11.21918125" y1="0.381" x2="11.38681875" y2="0.46558125" layer="21"/>
<rectangle x1="12.40281875" y1="0.381" x2="12.74318125" y2="0.46558125" layer="21"/>
<rectangle x1="14.097" y1="0.381" x2="14.94281875" y2="0.46558125" layer="21"/>
<rectangle x1="15.70481875" y1="0.381" x2="15.79118125" y2="0.46558125" layer="21"/>
<rectangle x1="16.80718125" y1="0.381" x2="16.891" y2="0.46558125" layer="21"/>
<rectangle x1="17.653" y1="0.381" x2="17.907" y2="0.46558125" layer="21"/>
<rectangle x1="2.667" y1="0.46558125" x2="3.85318125" y2="0.55041875" layer="21"/>
<rectangle x1="4.36118125" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="21"/>
<rectangle x1="5.54481875" y1="0.46558125" x2="5.715" y2="0.55041875" layer="21"/>
<rectangle x1="5.88518125" y1="0.46558125" x2="6.13918125" y2="0.55041875" layer="21"/>
<rectangle x1="6.731" y1="0.46558125" x2="7.239" y2="0.55041875" layer="21"/>
<rectangle x1="7.493" y1="0.46558125" x2="8.67918125" y2="0.55041875" layer="21"/>
<rectangle x1="9.69518125" y1="0.46558125" x2="10.37081875" y2="0.55041875" layer="21"/>
<rectangle x1="11.049" y1="0.46558125" x2="11.64081875" y2="0.55041875" layer="21"/>
<rectangle x1="12.23518125" y1="0.46558125" x2="12.91081875" y2="0.55041875" layer="21"/>
<rectangle x1="13.92681875" y1="0.46558125" x2="15.113" y2="0.55041875" layer="21"/>
<rectangle x1="15.621" y1="0.46558125" x2="15.875" y2="0.55041875" layer="21"/>
<rectangle x1="16.72081875" y1="0.46558125" x2="16.97481875" y2="0.55041875" layer="21"/>
<rectangle x1="17.399" y1="0.46558125" x2="18.07718125" y2="0.55041875" layer="21"/>
<rectangle x1="2.58318125" y1="0.55041875" x2="3.00481875" y2="0.635" layer="21"/>
<rectangle x1="3.51281875" y1="0.55041875" x2="3.937" y2="0.635" layer="21"/>
<rectangle x1="4.36118125" y1="0.55041875" x2="4.61518125" y2="0.635" layer="21"/>
<rectangle x1="5.54481875" y1="0.55041875" x2="5.715" y2="0.635" layer="21"/>
<rectangle x1="5.88518125" y1="0.55041875" x2="6.13918125" y2="0.635" layer="21"/>
<rectangle x1="6.64718125" y1="0.55041875" x2="7.239" y2="0.635" layer="21"/>
<rectangle x1="7.493" y1="0.55041875" x2="8.84681875" y2="0.635" layer="21"/>
<rectangle x1="9.60881875" y1="0.55041875" x2="10.45718125" y2="0.635" layer="21"/>
<rectangle x1="10.96518125" y1="0.55041875" x2="11.72718125" y2="0.635" layer="21"/>
<rectangle x1="12.14881875" y1="0.55041875" x2="12.99718125" y2="0.635" layer="21"/>
<rectangle x1="13.843" y1="0.55041875" x2="14.26718125" y2="0.635" layer="21"/>
<rectangle x1="14.77518125" y1="0.55041875" x2="15.19681875" y2="0.635" layer="21"/>
<rectangle x1="15.621" y1="0.55041875" x2="15.875" y2="0.635" layer="21"/>
<rectangle x1="16.72081875" y1="0.55041875" x2="16.97481875" y2="0.635" layer="21"/>
<rectangle x1="17.31518125" y1="0.55041875" x2="18.24481875" y2="0.635" layer="21"/>
<rectangle x1="2.49681875" y1="0.635" x2="2.83718125" y2="0.71958125" layer="21"/>
<rectangle x1="3.683" y1="0.635" x2="4.02081875" y2="0.71958125" layer="21"/>
<rectangle x1="4.36118125" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="5.54481875" y1="0.635" x2="5.715" y2="0.71958125" layer="21"/>
<rectangle x1="5.88518125" y1="0.635" x2="6.13918125" y2="0.71958125" layer="21"/>
<rectangle x1="6.56081875" y1="0.635" x2="6.90118125" y2="0.71958125" layer="21"/>
<rectangle x1="7.493" y1="0.635" x2="7.747" y2="0.71958125" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="8.93318125" y2="0.71958125" layer="21"/>
<rectangle x1="9.525" y1="0.635" x2="9.86281875" y2="0.71958125" layer="21"/>
<rectangle x1="10.20318125" y1="0.635" x2="10.541" y2="0.71958125" layer="21"/>
<rectangle x1="10.87881875" y1="0.635" x2="11.13281875" y2="0.71958125" layer="21"/>
<rectangle x1="11.47318125" y1="0.635" x2="11.811" y2="0.71958125" layer="21"/>
<rectangle x1="12.065" y1="0.635" x2="12.40281875" y2="0.71958125" layer="21"/>
<rectangle x1="12.74318125" y1="0.635" x2="13.081" y2="0.71958125" layer="21"/>
<rectangle x1="13.75918125" y1="0.635" x2="14.097" y2="0.71958125" layer="21"/>
<rectangle x1="14.94281875" y1="0.635" x2="15.28318125" y2="0.71958125" layer="21"/>
<rectangle x1="15.621" y1="0.635" x2="15.875" y2="0.71958125" layer="21"/>
<rectangle x1="16.72081875" y1="0.635" x2="16.97481875" y2="0.71958125" layer="21"/>
<rectangle x1="17.22881875" y1="0.635" x2="17.56918125" y2="0.71958125" layer="21"/>
<rectangle x1="17.907" y1="0.635" x2="18.33118125" y2="0.71958125" layer="21"/>
<rectangle x1="2.413" y1="0.71958125" x2="2.667" y2="0.80441875" layer="21"/>
<rectangle x1="3.76681875" y1="0.71958125" x2="4.02081875" y2="0.80441875" layer="21"/>
<rectangle x1="4.36118125" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="5.54481875" y1="0.71958125" x2="5.715" y2="0.80441875" layer="21"/>
<rectangle x1="5.88518125" y1="0.71958125" x2="6.13918125" y2="0.80441875" layer="21"/>
<rectangle x1="6.56081875" y1="0.71958125" x2="6.81481875" y2="0.80441875" layer="21"/>
<rectangle x1="7.493" y1="0.71958125" x2="7.747" y2="0.80441875" layer="21"/>
<rectangle x1="8.67918125" y1="0.71958125" x2="9.017" y2="0.80441875" layer="21"/>
<rectangle x1="9.44118125" y1="0.71958125" x2="9.69518125" y2="0.80441875" layer="21"/>
<rectangle x1="10.37081875" y1="0.71958125" x2="10.62481875" y2="0.80441875" layer="21"/>
<rectangle x1="10.87881875" y1="0.71958125" x2="11.049" y2="0.80441875" layer="21"/>
<rectangle x1="11.557" y1="0.71958125" x2="11.811" y2="0.80441875" layer="21"/>
<rectangle x1="11.98118125" y1="0.71958125" x2="12.23518125" y2="0.80441875" layer="21"/>
<rectangle x1="12.91081875" y1="0.71958125" x2="13.16481875" y2="0.80441875" layer="21"/>
<rectangle x1="13.67281875" y1="0.71958125" x2="14.01318125" y2="0.80441875" layer="21"/>
<rectangle x1="15.02918125" y1="0.71958125" x2="15.367" y2="0.80441875" layer="21"/>
<rectangle x1="15.621" y1="0.71958125" x2="15.875" y2="0.80441875" layer="21"/>
<rectangle x1="16.72081875" y1="0.71958125" x2="16.97481875" y2="0.80441875" layer="21"/>
<rectangle x1="17.145" y1="0.71958125" x2="17.399" y2="0.80441875" layer="21"/>
<rectangle x1="18.07718125" y1="0.71958125" x2="18.33118125" y2="0.80441875" layer="21"/>
<rectangle x1="2.32918125" y1="0.80441875" x2="2.58318125" y2="0.889" layer="21"/>
<rectangle x1="3.85318125" y1="0.80441875" x2="4.10718125" y2="0.889" layer="21"/>
<rectangle x1="4.36118125" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="5.54481875" y1="0.80441875" x2="5.715" y2="0.889" layer="21"/>
<rectangle x1="5.88518125" y1="0.80441875" x2="6.13918125" y2="0.889" layer="21"/>
<rectangle x1="6.477" y1="0.80441875" x2="6.731" y2="0.889" layer="21"/>
<rectangle x1="7.493" y1="0.80441875" x2="7.747" y2="0.889" layer="21"/>
<rectangle x1="8.763" y1="0.80441875" x2="9.10081875" y2="0.889" layer="21"/>
<rectangle x1="9.44118125" y1="0.80441875" x2="9.69518125" y2="0.889" layer="21"/>
<rectangle x1="10.45718125" y1="0.80441875" x2="10.71118125" y2="0.889" layer="21"/>
<rectangle x1="11.557" y1="0.80441875" x2="11.811" y2="0.889" layer="21"/>
<rectangle x1="11.98118125" y1="0.80441875" x2="12.14881875" y2="0.889" layer="21"/>
<rectangle x1="12.99718125" y1="0.80441875" x2="13.16481875" y2="0.889" layer="21"/>
<rectangle x1="13.67281875" y1="0.80441875" x2="13.92681875" y2="0.889" layer="21"/>
<rectangle x1="15.113" y1="0.80441875" x2="15.367" y2="0.889" layer="21"/>
<rectangle x1="15.621" y1="0.80441875" x2="15.875" y2="0.889" layer="21"/>
<rectangle x1="16.72081875" y1="0.80441875" x2="16.97481875" y2="0.889" layer="21"/>
<rectangle x1="17.145" y1="0.80441875" x2="17.399" y2="0.889" layer="21"/>
<rectangle x1="18.161" y1="0.80441875" x2="18.33118125" y2="0.889" layer="21"/>
<rectangle x1="2.32918125" y1="0.889" x2="2.58318125" y2="0.97358125" layer="21"/>
<rectangle x1="3.937" y1="0.889" x2="4.191" y2="0.97358125" layer="21"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.61518125" y2="0.97358125" layer="21"/>
<rectangle x1="5.54481875" y1="0.889" x2="5.715" y2="0.97358125" layer="21"/>
<rectangle x1="5.88518125" y1="0.889" x2="6.13918125" y2="0.97358125" layer="21"/>
<rectangle x1="6.477" y1="0.889" x2="6.731" y2="0.97358125" layer="21"/>
<rectangle x1="7.493" y1="0.889" x2="7.747" y2="0.97358125" layer="21"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.10081875" y2="0.97358125" layer="21"/>
<rectangle x1="9.35481875" y1="0.889" x2="9.60881875" y2="0.97358125" layer="21"/>
<rectangle x1="10.45718125" y1="0.889" x2="10.71118125" y2="0.97358125" layer="21"/>
<rectangle x1="11.47318125" y1="0.889" x2="11.72718125" y2="0.97358125" layer="21"/>
<rectangle x1="11.89481875" y1="0.889" x2="12.14881875" y2="0.97358125" layer="21"/>
<rectangle x1="13.589" y1="0.889" x2="13.843" y2="0.97358125" layer="21"/>
<rectangle x1="15.19681875" y1="0.889" x2="15.45081875" y2="0.97358125" layer="21"/>
<rectangle x1="15.621" y1="0.889" x2="15.875" y2="0.97358125" layer="21"/>
<rectangle x1="16.72081875" y1="0.889" x2="16.97481875" y2="0.97358125" layer="21"/>
<rectangle x1="17.06118125" y1="0.889" x2="17.31518125" y2="0.97358125" layer="21"/>
<rectangle x1="2.32918125" y1="0.97358125" x2="2.49681875" y2="1.05841875" layer="21"/>
<rectangle x1="3.937" y1="0.97358125" x2="4.191" y2="1.05841875" layer="21"/>
<rectangle x1="4.36118125" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="21"/>
<rectangle x1="5.54481875" y1="0.97358125" x2="5.715" y2="1.05841875" layer="21"/>
<rectangle x1="5.88518125" y1="0.97358125" x2="6.13918125" y2="1.05841875" layer="21"/>
<rectangle x1="6.477" y1="0.97358125" x2="6.64718125" y2="1.05841875" layer="21"/>
<rectangle x1="7.493" y1="0.97358125" x2="7.747" y2="1.05841875" layer="21"/>
<rectangle x1="8.93318125" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="21"/>
<rectangle x1="9.35481875" y1="0.97358125" x2="9.60881875" y2="1.05841875" layer="21"/>
<rectangle x1="10.45718125" y1="0.97358125" x2="10.71118125" y2="1.05841875" layer="21"/>
<rectangle x1="11.21918125" y1="0.97358125" x2="11.72718125" y2="1.05841875" layer="21"/>
<rectangle x1="11.89481875" y1="0.97358125" x2="13.25118125" y2="1.05841875" layer="21"/>
<rectangle x1="13.589" y1="0.97358125" x2="13.843" y2="1.05841875" layer="21"/>
<rectangle x1="15.19681875" y1="0.97358125" x2="15.45081875" y2="1.05841875" layer="21"/>
<rectangle x1="15.621" y1="0.97358125" x2="15.875" y2="1.05841875" layer="21"/>
<rectangle x1="16.72081875" y1="0.97358125" x2="16.97481875" y2="1.05841875" layer="21"/>
<rectangle x1="17.06118125" y1="0.97358125" x2="18.415" y2="1.05841875" layer="21"/>
<rectangle x1="2.24281875" y1="1.05841875" x2="2.49681875" y2="1.143" layer="21"/>
<rectangle x1="3.937" y1="1.05841875" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="4.36118125" y1="1.05841875" x2="4.61518125" y2="1.143" layer="21"/>
<rectangle x1="5.54481875" y1="1.05841875" x2="5.715" y2="1.143" layer="21"/>
<rectangle x1="5.88518125" y1="1.05841875" x2="6.13918125" y2="1.143" layer="21"/>
<rectangle x1="6.477" y1="1.05841875" x2="6.64718125" y2="1.143" layer="21"/>
<rectangle x1="7.493" y1="1.05841875" x2="7.747" y2="1.143" layer="21"/>
<rectangle x1="8.93318125" y1="1.05841875" x2="9.18718125" y2="1.143" layer="21"/>
<rectangle x1="9.35481875" y1="1.05841875" x2="9.60881875" y2="1.143" layer="21"/>
<rectangle x1="10.45718125" y1="1.05841875" x2="10.71118125" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="1.05841875" x2="11.557" y2="1.143" layer="21"/>
<rectangle x1="11.89481875" y1="1.05841875" x2="13.25118125" y2="1.143" layer="21"/>
<rectangle x1="13.589" y1="1.05841875" x2="13.75918125" y2="1.143" layer="21"/>
<rectangle x1="15.28318125" y1="1.05841875" x2="15.53718125" y2="1.143" layer="21"/>
<rectangle x1="15.621" y1="1.05841875" x2="15.875" y2="1.143" layer="21"/>
<rectangle x1="16.72081875" y1="1.05841875" x2="16.97481875" y2="1.143" layer="21"/>
<rectangle x1="17.06118125" y1="1.05841875" x2="18.415" y2="1.143" layer="21"/>
<rectangle x1="2.24281875" y1="1.143" x2="2.49681875" y2="1.22758125" layer="21"/>
<rectangle x1="4.02081875" y1="1.143" x2="4.191" y2="1.22758125" layer="21"/>
<rectangle x1="4.36118125" y1="1.143" x2="4.61518125" y2="1.22758125" layer="21"/>
<rectangle x1="5.54481875" y1="1.143" x2="5.715" y2="1.22758125" layer="21"/>
<rectangle x1="5.88518125" y1="1.143" x2="6.13918125" y2="1.22758125" layer="21"/>
<rectangle x1="6.477" y1="1.143" x2="6.64718125" y2="1.22758125" layer="21"/>
<rectangle x1="7.493" y1="1.143" x2="7.747" y2="1.22758125" layer="21"/>
<rectangle x1="9.017" y1="1.143" x2="9.271" y2="1.22758125" layer="21"/>
<rectangle x1="9.35481875" y1="1.143" x2="9.60881875" y2="1.22758125" layer="21"/>
<rectangle x1="10.45718125" y1="1.143" x2="10.71118125" y2="1.22758125" layer="21"/>
<rectangle x1="10.87881875" y1="1.143" x2="11.38681875" y2="1.22758125" layer="21"/>
<rectangle x1="11.89481875" y1="1.143" x2="13.25118125" y2="1.22758125" layer="21"/>
<rectangle x1="13.50518125" y1="1.143" x2="13.75918125" y2="1.22758125" layer="21"/>
<rectangle x1="15.28318125" y1="1.143" x2="15.53718125" y2="1.22758125" layer="21"/>
<rectangle x1="15.621" y1="1.143" x2="15.875" y2="1.22758125" layer="21"/>
<rectangle x1="16.72081875" y1="1.143" x2="16.97481875" y2="1.22758125" layer="21"/>
<rectangle x1="17.06118125" y1="1.143" x2="18.415" y2="1.22758125" layer="21"/>
<rectangle x1="2.24281875" y1="1.22758125" x2="2.49681875" y2="1.31241875" layer="21"/>
<rectangle x1="4.02081875" y1="1.22758125" x2="4.191" y2="1.31241875" layer="21"/>
<rectangle x1="4.36118125" y1="1.22758125" x2="4.61518125" y2="1.31241875" layer="21"/>
<rectangle x1="5.461" y1="1.22758125" x2="5.715" y2="1.31241875" layer="21"/>
<rectangle x1="5.88518125" y1="1.22758125" x2="6.13918125" y2="1.31241875" layer="21"/>
<rectangle x1="6.477" y1="1.22758125" x2="6.64718125" y2="1.31241875" layer="21"/>
<rectangle x1="7.493" y1="1.22758125" x2="7.747" y2="1.31241875" layer="21"/>
<rectangle x1="9.017" y1="1.22758125" x2="9.271" y2="1.31241875" layer="21"/>
<rectangle x1="9.35481875" y1="1.22758125" x2="9.60881875" y2="1.31241875" layer="21"/>
<rectangle x1="10.45718125" y1="1.22758125" x2="10.71118125" y2="1.31241875" layer="21"/>
<rectangle x1="10.795" y1="1.22758125" x2="11.21918125" y2="1.31241875" layer="21"/>
<rectangle x1="11.89481875" y1="1.22758125" x2="12.14881875" y2="1.31241875" layer="21"/>
<rectangle x1="12.99718125" y1="1.22758125" x2="13.25118125" y2="1.31241875" layer="21"/>
<rectangle x1="13.50518125" y1="1.22758125" x2="13.75918125" y2="1.31241875" layer="21"/>
<rectangle x1="15.28318125" y1="1.22758125" x2="15.53718125" y2="1.31241875" layer="21"/>
<rectangle x1="15.621" y1="1.22758125" x2="15.875" y2="1.31241875" layer="21"/>
<rectangle x1="16.72081875" y1="1.22758125" x2="16.97481875" y2="1.31241875" layer="21"/>
<rectangle x1="17.06118125" y1="1.22758125" x2="17.31518125" y2="1.31241875" layer="21"/>
<rectangle x1="18.161" y1="1.22758125" x2="18.415" y2="1.31241875" layer="21"/>
<rectangle x1="2.24281875" y1="1.31241875" x2="2.49681875" y2="1.397" layer="21"/>
<rectangle x1="4.02081875" y1="1.31241875" x2="4.191" y2="1.397" layer="21"/>
<rectangle x1="4.36118125" y1="1.31241875" x2="4.699" y2="1.397" layer="21"/>
<rectangle x1="5.461" y1="1.31241875" x2="5.715" y2="1.397" layer="21"/>
<rectangle x1="5.88518125" y1="1.31241875" x2="6.13918125" y2="1.397" layer="21"/>
<rectangle x1="6.477" y1="1.31241875" x2="6.64718125" y2="1.397" layer="21"/>
<rectangle x1="7.493" y1="1.31241875" x2="7.747" y2="1.397" layer="21"/>
<rectangle x1="9.017" y1="1.31241875" x2="9.271" y2="1.397" layer="21"/>
<rectangle x1="9.44118125" y1="1.31241875" x2="9.69518125" y2="1.397" layer="21"/>
<rectangle x1="10.45718125" y1="1.31241875" x2="10.62481875" y2="1.397" layer="21"/>
<rectangle x1="10.795" y1="1.31241875" x2="11.049" y2="1.397" layer="21"/>
<rectangle x1="11.64081875" y1="1.31241875" x2="11.72718125" y2="1.397" layer="21"/>
<rectangle x1="11.98118125" y1="1.31241875" x2="12.23518125" y2="1.397" layer="21"/>
<rectangle x1="12.99718125" y1="1.31241875" x2="13.16481875" y2="1.397" layer="21"/>
<rectangle x1="13.50518125" y1="1.31241875" x2="13.75918125" y2="1.397" layer="21"/>
<rectangle x1="15.28318125" y1="1.31241875" x2="15.53718125" y2="1.397" layer="21"/>
<rectangle x1="15.621" y1="1.31241875" x2="15.875" y2="1.397" layer="21"/>
<rectangle x1="16.637" y1="1.31241875" x2="16.891" y2="1.397" layer="21"/>
<rectangle x1="17.145" y1="1.31241875" x2="17.399" y2="1.397" layer="21"/>
<rectangle x1="18.161" y1="1.31241875" x2="18.415" y2="1.397" layer="21"/>
<rectangle x1="2.24281875" y1="1.397" x2="2.49681875" y2="1.48158125" layer="21"/>
<rectangle x1="4.02081875" y1="1.397" x2="4.191" y2="1.48158125" layer="21"/>
<rectangle x1="4.36118125" y1="1.397" x2="4.78281875" y2="1.48158125" layer="21"/>
<rectangle x1="5.37718125" y1="1.397" x2="5.63118125" y2="1.48158125" layer="21"/>
<rectangle x1="5.88518125" y1="1.397" x2="6.13918125" y2="1.48158125" layer="21"/>
<rectangle x1="6.477" y1="1.397" x2="6.64718125" y2="1.48158125" layer="21"/>
<rectangle x1="7.493" y1="1.397" x2="7.747" y2="1.48158125" layer="21"/>
<rectangle x1="9.017" y1="1.397" x2="9.271" y2="1.48158125" layer="21"/>
<rectangle x1="9.44118125" y1="1.397" x2="9.69518125" y2="1.48158125" layer="21"/>
<rectangle x1="10.37081875" y1="1.397" x2="10.62481875" y2="1.48158125" layer="21"/>
<rectangle x1="10.795" y1="1.397" x2="11.049" y2="1.48158125" layer="21"/>
<rectangle x1="11.557" y1="1.397" x2="11.811" y2="1.48158125" layer="21"/>
<rectangle x1="11.98118125" y1="1.397" x2="12.319" y2="1.48158125" layer="21"/>
<rectangle x1="12.91081875" y1="1.397" x2="13.16481875" y2="1.48158125" layer="21"/>
<rectangle x1="13.50518125" y1="1.397" x2="13.75918125" y2="1.48158125" layer="21"/>
<rectangle x1="15.28318125" y1="1.397" x2="15.53718125" y2="1.48158125" layer="21"/>
<rectangle x1="15.621" y1="1.397" x2="15.95881875" y2="1.48158125" layer="21"/>
<rectangle x1="16.637" y1="1.397" x2="16.891" y2="1.48158125" layer="21"/>
<rectangle x1="17.145" y1="1.397" x2="17.48281875" y2="1.48158125" layer="21"/>
<rectangle x1="18.07718125" y1="1.397" x2="18.33118125" y2="1.48158125" layer="21"/>
<rectangle x1="2.24281875" y1="1.48158125" x2="2.49681875" y2="1.56641875" layer="21"/>
<rectangle x1="4.02081875" y1="1.48158125" x2="4.191" y2="1.56641875" layer="21"/>
<rectangle x1="4.36118125" y1="1.48158125" x2="4.953" y2="1.56641875" layer="21"/>
<rectangle x1="5.207" y1="1.48158125" x2="5.63118125" y2="1.56641875" layer="21"/>
<rectangle x1="5.88518125" y1="1.48158125" x2="6.13918125" y2="1.56641875" layer="21"/>
<rectangle x1="6.477" y1="1.48158125" x2="6.64718125" y2="1.56641875" layer="21"/>
<rectangle x1="7.493" y1="1.48158125" x2="7.747" y2="1.56641875" layer="21"/>
<rectangle x1="9.017" y1="1.48158125" x2="9.271" y2="1.56641875" layer="21"/>
<rectangle x1="9.525" y1="1.48158125" x2="9.86281875" y2="1.56641875" layer="21"/>
<rectangle x1="10.20318125" y1="1.48158125" x2="10.541" y2="1.56641875" layer="21"/>
<rectangle x1="10.87881875" y1="1.48158125" x2="11.21918125" y2="1.56641875" layer="21"/>
<rectangle x1="11.47318125" y1="1.48158125" x2="11.72718125" y2="1.56641875" layer="21"/>
<rectangle x1="12.065" y1="1.48158125" x2="12.40281875" y2="1.56641875" layer="21"/>
<rectangle x1="12.74318125" y1="1.48158125" x2="13.081" y2="1.56641875" layer="21"/>
<rectangle x1="13.589" y1="1.48158125" x2="13.75918125" y2="1.56641875" layer="21"/>
<rectangle x1="15.28318125" y1="1.48158125" x2="15.53718125" y2="1.56641875" layer="21"/>
<rectangle x1="15.621" y1="1.48158125" x2="16.129" y2="1.56641875" layer="21"/>
<rectangle x1="16.46681875" y1="1.48158125" x2="16.80718125" y2="1.56641875" layer="21"/>
<rectangle x1="17.22881875" y1="1.48158125" x2="17.653" y2="1.56641875" layer="21"/>
<rectangle x1="17.907" y1="1.48158125" x2="18.24481875" y2="1.56641875" layer="21"/>
<rectangle x1="2.24281875" y1="1.56641875" x2="2.49681875" y2="1.651" layer="21"/>
<rectangle x1="4.02081875" y1="1.56641875" x2="4.191" y2="1.651" layer="21"/>
<rectangle x1="4.36118125" y1="1.56641875" x2="5.54481875" y2="1.651" layer="21"/>
<rectangle x1="5.88518125" y1="1.56641875" x2="6.13918125" y2="1.651" layer="21"/>
<rectangle x1="6.223" y1="1.56641875" x2="7.239" y2="1.651" layer="21"/>
<rectangle x1="7.493" y1="1.56641875" x2="7.747" y2="1.651" layer="21"/>
<rectangle x1="9.017" y1="1.56641875" x2="9.271" y2="1.651" layer="21"/>
<rectangle x1="9.60881875" y1="1.56641875" x2="10.45718125" y2="1.651" layer="21"/>
<rectangle x1="10.87881875" y1="1.56641875" x2="11.72718125" y2="1.651" layer="21"/>
<rectangle x1="12.14881875" y1="1.56641875" x2="12.99718125" y2="1.651" layer="21"/>
<rectangle x1="13.589" y1="1.56641875" x2="13.843" y2="1.651" layer="21"/>
<rectangle x1="15.19681875" y1="1.56641875" x2="15.45081875" y2="1.651" layer="21"/>
<rectangle x1="15.621" y1="1.56641875" x2="16.72081875" y2="1.651" layer="21"/>
<rectangle x1="17.31518125" y1="1.56641875" x2="18.161" y2="1.651" layer="21"/>
<rectangle x1="2.24281875" y1="1.651" x2="2.49681875" y2="1.73558125" layer="21"/>
<rectangle x1="4.02081875" y1="1.651" x2="4.191" y2="1.73558125" layer="21"/>
<rectangle x1="4.36118125" y1="1.651" x2="4.61518125" y2="1.73558125" layer="21"/>
<rectangle x1="4.78281875" y1="1.651" x2="5.37718125" y2="1.73558125" layer="21"/>
<rectangle x1="5.88518125" y1="1.651" x2="6.13918125" y2="1.73558125" layer="21"/>
<rectangle x1="6.223" y1="1.651" x2="7.239" y2="1.73558125" layer="21"/>
<rectangle x1="7.493" y1="1.651" x2="7.747" y2="1.73558125" layer="21"/>
<rectangle x1="9.017" y1="1.651" x2="9.18718125" y2="1.73558125" layer="21"/>
<rectangle x1="9.69518125" y1="1.651" x2="10.37081875" y2="1.73558125" layer="21"/>
<rectangle x1="11.049" y1="1.651" x2="11.557" y2="1.73558125" layer="21"/>
<rectangle x1="12.23518125" y1="1.651" x2="12.91081875" y2="1.73558125" layer="21"/>
<rectangle x1="13.589" y1="1.651" x2="13.843" y2="1.73558125" layer="21"/>
<rectangle x1="15.19681875" y1="1.651" x2="15.45081875" y2="1.73558125" layer="21"/>
<rectangle x1="15.621" y1="1.651" x2="15.79118125" y2="1.73558125" layer="21"/>
<rectangle x1="15.95881875" y1="1.651" x2="16.637" y2="1.73558125" layer="21"/>
<rectangle x1="17.399" y1="1.651" x2="18.07718125" y2="1.73558125" layer="21"/>
<rectangle x1="2.24281875" y1="1.73558125" x2="2.49681875" y2="1.82041875" layer="21"/>
<rectangle x1="4.02081875" y1="1.73558125" x2="4.191" y2="1.82041875" layer="21"/>
<rectangle x1="4.445" y1="1.73558125" x2="4.52881875" y2="1.82041875" layer="21"/>
<rectangle x1="4.953" y1="1.73558125" x2="5.12318125" y2="1.82041875" layer="21"/>
<rectangle x1="5.969" y1="1.73558125" x2="6.05281875" y2="1.82041875" layer="21"/>
<rectangle x1="6.30681875" y1="1.73558125" x2="7.15518125" y2="1.82041875" layer="21"/>
<rectangle x1="7.493" y1="1.73558125" x2="7.747" y2="1.82041875" layer="21"/>
<rectangle x1="8.93318125" y1="1.73558125" x2="9.18718125" y2="1.82041875" layer="21"/>
<rectangle x1="9.94918125" y1="1.73558125" x2="10.11681875" y2="1.82041875" layer="21"/>
<rectangle x1="11.21918125" y1="1.73558125" x2="11.38681875" y2="1.82041875" layer="21"/>
<rectangle x1="12.48918125" y1="1.73558125" x2="12.65681875" y2="1.82041875" layer="21"/>
<rectangle x1="13.67281875" y1="1.73558125" x2="13.92681875" y2="1.82041875" layer="21"/>
<rectangle x1="15.113" y1="1.73558125" x2="15.367" y2="1.82041875" layer="21"/>
<rectangle x1="16.21281875" y1="1.73558125" x2="16.383" y2="1.82041875" layer="21"/>
<rectangle x1="17.653" y1="1.73558125" x2="17.82318125" y2="1.82041875" layer="21"/>
<rectangle x1="2.24281875" y1="1.82041875" x2="2.49681875" y2="1.905" layer="21"/>
<rectangle x1="4.02081875" y1="1.82041875" x2="4.191" y2="1.905" layer="21"/>
<rectangle x1="5.88518125" y1="1.82041875" x2="6.13918125" y2="1.905" layer="21"/>
<rectangle x1="6.477" y1="1.82041875" x2="6.64718125" y2="1.905" layer="21"/>
<rectangle x1="7.493" y1="1.82041875" x2="7.747" y2="1.905" layer="21"/>
<rectangle x1="8.84681875" y1="1.82041875" x2="9.18718125" y2="1.905" layer="21"/>
<rectangle x1="13.67281875" y1="1.82041875" x2="14.01318125" y2="1.905" layer="21"/>
<rectangle x1="15.02918125" y1="1.82041875" x2="15.367" y2="1.905" layer="21"/>
<rectangle x1="2.24281875" y1="1.905" x2="2.49681875" y2="1.98958125" layer="21"/>
<rectangle x1="4.02081875" y1="1.905" x2="4.191" y2="1.98958125" layer="21"/>
<rectangle x1="5.88518125" y1="1.905" x2="6.13918125" y2="1.98958125" layer="21"/>
<rectangle x1="6.477" y1="1.905" x2="6.64718125" y2="1.98958125" layer="21"/>
<rectangle x1="7.493" y1="1.905" x2="7.747" y2="1.98958125" layer="21"/>
<rectangle x1="8.84681875" y1="1.905" x2="9.10081875" y2="1.98958125" layer="21"/>
<rectangle x1="13.75918125" y1="1.905" x2="14.097" y2="1.98958125" layer="21"/>
<rectangle x1="14.94281875" y1="1.905" x2="15.28318125" y2="1.98958125" layer="21"/>
<rectangle x1="2.24281875" y1="1.98958125" x2="2.49681875" y2="2.07441875" layer="21"/>
<rectangle x1="4.02081875" y1="1.98958125" x2="4.27481875" y2="2.07441875" layer="21"/>
<rectangle x1="5.88518125" y1="1.98958125" x2="6.05281875" y2="2.07441875" layer="21"/>
<rectangle x1="6.477" y1="1.98958125" x2="6.64718125" y2="2.07441875" layer="21"/>
<rectangle x1="7.493" y1="1.98958125" x2="7.747" y2="2.07441875" layer="21"/>
<rectangle x1="8.67918125" y1="1.98958125" x2="9.017" y2="2.07441875" layer="21"/>
<rectangle x1="13.843" y1="1.98958125" x2="14.26718125" y2="2.07441875" layer="21"/>
<rectangle x1="14.77518125" y1="1.98958125" x2="15.19681875" y2="2.07441875" layer="21"/>
<rectangle x1="2.24281875" y1="2.07441875" x2="2.49681875" y2="2.159" layer="21"/>
<rectangle x1="4.02081875" y1="2.07441875" x2="4.27481875" y2="2.159" layer="21"/>
<rectangle x1="6.477" y1="2.07441875" x2="6.64718125" y2="2.159" layer="21"/>
<rectangle x1="7.493" y1="2.07441875" x2="7.747" y2="2.159" layer="21"/>
<rectangle x1="8.59281875" y1="2.07441875" x2="8.93318125" y2="2.159" layer="21"/>
<rectangle x1="13.92681875" y1="2.07441875" x2="15.113" y2="2.159" layer="21"/>
<rectangle x1="2.24281875" y1="2.159" x2="2.49681875" y2="2.24358125" layer="21"/>
<rectangle x1="4.02081875" y1="2.159" x2="4.191" y2="2.24358125" layer="21"/>
<rectangle x1="6.477" y1="2.159" x2="6.64718125" y2="2.24358125" layer="21"/>
<rectangle x1="7.493" y1="2.159" x2="8.84681875" y2="2.24358125" layer="21"/>
<rectangle x1="14.097" y1="2.159" x2="14.94281875" y2="2.24358125" layer="21"/>
<rectangle x1="2.24281875" y1="2.24358125" x2="2.413" y2="2.32841875" layer="21"/>
<rectangle x1="4.02081875" y1="2.24358125" x2="4.191" y2="2.32841875" layer="21"/>
<rectangle x1="6.477" y1="2.24358125" x2="6.64718125" y2="2.32841875" layer="21"/>
<rectangle x1="7.493" y1="2.24358125" x2="8.763" y2="2.32841875" layer="21"/>
<rectangle x1="14.26718125" y1="2.24358125" x2="14.77518125" y2="2.32841875" layer="21"/>
<rectangle x1="7.493" y1="2.32841875" x2="8.509" y2="2.413" layer="21"/>
<rectangle x1="3.34518125" y1="3.25958125" x2="16.80718125" y2="3.34441875" layer="21"/>
<rectangle x1="3.00481875" y1="3.34441875" x2="17.145" y2="3.429" layer="21"/>
<rectangle x1="2.667" y1="3.429" x2="17.399" y2="3.51358125" layer="21"/>
<rectangle x1="2.49681875" y1="3.51358125" x2="17.56918125" y2="3.59841875" layer="21"/>
<rectangle x1="2.32918125" y1="3.59841875" x2="17.82318125" y2="3.683" layer="21"/>
<rectangle x1="2.159" y1="3.683" x2="17.99081875" y2="3.76758125" layer="21"/>
<rectangle x1="1.98881875" y1="3.76758125" x2="18.07718125" y2="3.85241875" layer="21"/>
<rectangle x1="1.905" y1="3.85241875" x2="3.51281875" y2="3.937" layer="21"/>
<rectangle x1="9.779" y1="3.85241875" x2="18.24481875" y2="3.937" layer="21"/>
<rectangle x1="1.73481875" y1="3.937" x2="3.175" y2="4.02158125" layer="21"/>
<rectangle x1="9.779" y1="3.937" x2="18.33118125" y2="4.02158125" layer="21"/>
<rectangle x1="1.651" y1="4.02158125" x2="2.83718125" y2="4.10641875" layer="21"/>
<rectangle x1="9.69518125" y1="4.02158125" x2="18.49881875" y2="4.10641875" layer="21"/>
<rectangle x1="1.56718125" y1="4.10641875" x2="2.667" y2="4.191" layer="21"/>
<rectangle x1="9.60881875" y1="4.10641875" x2="18.58518125" y2="4.191" layer="21"/>
<rectangle x1="1.397" y1="4.191" x2="2.49681875" y2="4.27558125" layer="21"/>
<rectangle x1="9.60881875" y1="4.191" x2="18.669" y2="4.27558125" layer="21"/>
<rectangle x1="1.31318125" y1="4.27558125" x2="2.32918125" y2="4.36041875" layer="21"/>
<rectangle x1="9.525" y1="4.27558125" x2="18.75281875" y2="4.36041875" layer="21"/>
<rectangle x1="1.22681875" y1="4.36041875" x2="2.159" y2="4.445" layer="21"/>
<rectangle x1="9.525" y1="4.36041875" x2="18.83918125" y2="4.445" layer="21"/>
<rectangle x1="1.143" y1="4.445" x2="2.07518125" y2="4.52958125" layer="21"/>
<rectangle x1="9.44118125" y1="4.445" x2="18.923" y2="4.52958125" layer="21"/>
<rectangle x1="1.05918125" y1="4.52958125" x2="1.98881875" y2="4.61441875" layer="21"/>
<rectangle x1="9.44118125" y1="4.52958125" x2="19.00681875" y2="4.61441875" layer="21"/>
<rectangle x1="0.97281875" y1="4.61441875" x2="1.905" y2="4.699" layer="21"/>
<rectangle x1="9.44118125" y1="4.61441875" x2="19.09318125" y2="4.699" layer="21"/>
<rectangle x1="0.97281875" y1="4.699" x2="1.73481875" y2="4.78358125" layer="21"/>
<rectangle x1="9.35481875" y1="4.699" x2="19.177" y2="4.78358125" layer="21"/>
<rectangle x1="0.889" y1="4.78358125" x2="1.651" y2="4.86841875" layer="21"/>
<rectangle x1="9.35481875" y1="4.78358125" x2="19.26081875" y2="4.86841875" layer="21"/>
<rectangle x1="0.80518125" y1="4.86841875" x2="1.56718125" y2="4.953" layer="21"/>
<rectangle x1="9.35481875" y1="4.86841875" x2="19.26081875" y2="4.953" layer="21"/>
<rectangle x1="0.71881875" y1="4.953" x2="1.48081875" y2="5.03758125" layer="21"/>
<rectangle x1="9.271" y1="4.953" x2="19.34718125" y2="5.03758125" layer="21"/>
<rectangle x1="0.71881875" y1="5.03758125" x2="1.48081875" y2="5.12241875" layer="21"/>
<rectangle x1="9.271" y1="5.03758125" x2="19.431" y2="5.12241875" layer="21"/>
<rectangle x1="0.635" y1="5.12241875" x2="1.397" y2="5.207" layer="21"/>
<rectangle x1="9.271" y1="5.12241875" x2="19.431" y2="5.207" layer="21"/>
<rectangle x1="0.55118125" y1="5.207" x2="1.31318125" y2="5.29158125" layer="21"/>
<rectangle x1="9.271" y1="5.207" x2="19.51481875" y2="5.29158125" layer="21"/>
<rectangle x1="0.55118125" y1="5.29158125" x2="1.22681875" y2="5.37641875" layer="21"/>
<rectangle x1="9.18718125" y1="5.29158125" x2="19.51481875" y2="5.37641875" layer="21"/>
<rectangle x1="0.46481875" y1="5.37641875" x2="1.22681875" y2="5.461" layer="21"/>
<rectangle x1="9.18718125" y1="5.37641875" x2="19.60118125" y2="5.461" layer="21"/>
<rectangle x1="0.46481875" y1="5.461" x2="1.143" y2="5.54558125" layer="21"/>
<rectangle x1="9.18718125" y1="5.461" x2="19.685" y2="5.54558125" layer="21"/>
<rectangle x1="0.381" y1="5.54558125" x2="1.05918125" y2="5.63041875" layer="21"/>
<rectangle x1="3.85318125" y1="5.54558125" x2="4.27481875" y2="5.63041875" layer="21"/>
<rectangle x1="5.88518125" y1="5.54558125" x2="6.81481875" y2="5.63041875" layer="21"/>
<rectangle x1="9.18718125" y1="5.54558125" x2="19.685" y2="5.63041875" layer="21"/>
<rectangle x1="0.381" y1="5.63041875" x2="1.05918125" y2="5.715" layer="21"/>
<rectangle x1="3.51281875" y1="5.63041875" x2="4.61518125" y2="5.715" layer="21"/>
<rectangle x1="5.79881875" y1="5.63041875" x2="7.239" y2="5.715" layer="21"/>
<rectangle x1="9.10081875" y1="5.63041875" x2="10.96518125" y2="5.715" layer="21"/>
<rectangle x1="11.64081875" y1="5.63041875" x2="19.685" y2="5.715" layer="21"/>
<rectangle x1="0.381" y1="5.715" x2="0.97281875" y2="5.79958125" layer="21"/>
<rectangle x1="3.34518125" y1="5.715" x2="4.78281875" y2="5.79958125" layer="21"/>
<rectangle x1="5.715" y1="5.715" x2="7.40918125" y2="5.79958125" layer="21"/>
<rectangle x1="9.10081875" y1="5.715" x2="10.71118125" y2="5.79958125" layer="21"/>
<rectangle x1="11.89481875" y1="5.715" x2="13.335" y2="5.79958125" layer="21"/>
<rectangle x1="13.67281875" y1="5.715" x2="15.113" y2="5.79958125" layer="21"/>
<rectangle x1="15.367" y1="5.715" x2="16.383" y2="5.79958125" layer="21"/>
<rectangle x1="17.22881875" y1="5.715" x2="19.76881875" y2="5.79958125" layer="21"/>
<rectangle x1="0.29718125" y1="5.79958125" x2="0.97281875" y2="5.88441875" layer="21"/>
<rectangle x1="3.175" y1="5.79958125" x2="4.953" y2="5.88441875" layer="21"/>
<rectangle x1="5.715" y1="5.79958125" x2="7.57681875" y2="5.88441875" layer="21"/>
<rectangle x1="9.10081875" y1="5.79958125" x2="10.541" y2="5.88441875" layer="21"/>
<rectangle x1="12.065" y1="5.79958125" x2="13.335" y2="5.88441875" layer="21"/>
<rectangle x1="13.67281875" y1="5.79958125" x2="15.113" y2="5.88441875" layer="21"/>
<rectangle x1="15.45081875" y1="5.79958125" x2="16.21281875" y2="5.88441875" layer="21"/>
<rectangle x1="17.48281875" y1="5.79958125" x2="19.76881875" y2="5.88441875" layer="21"/>
<rectangle x1="0.29718125" y1="5.88441875" x2="0.889" y2="5.969" layer="21"/>
<rectangle x1="3.09118125" y1="5.88441875" x2="5.12318125" y2="5.969" layer="21"/>
<rectangle x1="5.715" y1="5.88441875" x2="7.66318125" y2="5.969" layer="21"/>
<rectangle x1="9.10081875" y1="5.88441875" x2="10.37081875" y2="5.969" layer="21"/>
<rectangle x1="12.23518125" y1="5.88441875" x2="13.335" y2="5.969" layer="21"/>
<rectangle x1="13.67281875" y1="5.88441875" x2="15.113" y2="5.969" layer="21"/>
<rectangle x1="15.45081875" y1="5.88441875" x2="16.129" y2="5.969" layer="21"/>
<rectangle x1="17.56918125" y1="5.88441875" x2="19.85518125" y2="5.969" layer="21"/>
<rectangle x1="0.21081875" y1="5.969" x2="0.889" y2="6.05358125" layer="21"/>
<rectangle x1="2.921" y1="5.969" x2="5.207" y2="6.05358125" layer="21"/>
<rectangle x1="5.715" y1="5.969" x2="7.747" y2="6.05358125" layer="21"/>
<rectangle x1="9.10081875" y1="5.969" x2="10.287" y2="6.05358125" layer="21"/>
<rectangle x1="12.319" y1="5.969" x2="13.335" y2="6.05358125" layer="21"/>
<rectangle x1="13.67281875" y1="5.969" x2="15.113" y2="6.05358125" layer="21"/>
<rectangle x1="15.45081875" y1="5.969" x2="16.04518125" y2="6.05358125" layer="21"/>
<rectangle x1="17.653" y1="5.969" x2="19.85518125" y2="6.05358125" layer="21"/>
<rectangle x1="0.21081875" y1="6.05358125" x2="0.889" y2="6.13841875" layer="21"/>
<rectangle x1="2.83718125" y1="6.05358125" x2="5.29081875" y2="6.13841875" layer="21"/>
<rectangle x1="5.79881875" y1="6.05358125" x2="7.83081875" y2="6.13841875" layer="21"/>
<rectangle x1="9.10081875" y1="6.05358125" x2="10.11681875" y2="6.13841875" layer="21"/>
<rectangle x1="12.40281875" y1="6.05358125" x2="13.335" y2="6.13841875" layer="21"/>
<rectangle x1="13.67281875" y1="6.05358125" x2="15.113" y2="6.13841875" layer="21"/>
<rectangle x1="15.45081875" y1="6.05358125" x2="15.95881875" y2="6.13841875" layer="21"/>
<rectangle x1="16.55318125" y1="6.05358125" x2="17.06118125" y2="6.13841875" layer="21"/>
<rectangle x1="17.73681875" y1="6.05358125" x2="19.85518125" y2="6.13841875" layer="21"/>
<rectangle x1="0.21081875" y1="6.13841875" x2="0.80518125" y2="6.223" layer="21"/>
<rectangle x1="2.75081875" y1="6.13841875" x2="5.37718125" y2="6.223" layer="21"/>
<rectangle x1="5.88518125" y1="6.13841875" x2="7.91718125" y2="6.223" layer="21"/>
<rectangle x1="9.017" y1="6.13841875" x2="10.11681875" y2="6.223" layer="21"/>
<rectangle x1="10.96518125" y1="6.13841875" x2="11.64081875" y2="6.223" layer="21"/>
<rectangle x1="12.48918125" y1="6.13841875" x2="13.335" y2="6.223" layer="21"/>
<rectangle x1="13.67281875" y1="6.13841875" x2="15.02918125" y2="6.223" layer="21"/>
<rectangle x1="15.45081875" y1="6.13841875" x2="15.875" y2="6.223" layer="21"/>
<rectangle x1="16.46681875" y1="6.13841875" x2="17.22881875" y2="6.223" layer="21"/>
<rectangle x1="17.73681875" y1="6.13841875" x2="19.939" y2="6.223" layer="21"/>
<rectangle x1="0.21081875" y1="6.223" x2="0.80518125" y2="6.30758125" layer="21"/>
<rectangle x1="2.75081875" y1="6.223" x2="3.683" y2="6.30758125" layer="21"/>
<rectangle x1="4.445" y1="6.223" x2="5.37718125" y2="6.30758125" layer="21"/>
<rectangle x1="7.06881875" y1="6.223" x2="8.001" y2="6.30758125" layer="21"/>
<rectangle x1="9.017" y1="6.223" x2="10.033" y2="6.30758125" layer="21"/>
<rectangle x1="10.795" y1="6.223" x2="11.811" y2="6.30758125" layer="21"/>
<rectangle x1="12.573" y1="6.223" x2="13.335" y2="6.30758125" layer="21"/>
<rectangle x1="13.67281875" y1="6.223" x2="15.02918125" y2="6.30758125" layer="21"/>
<rectangle x1="15.45081875" y1="6.223" x2="15.875" y2="6.30758125" layer="21"/>
<rectangle x1="16.29918125" y1="6.223" x2="17.399" y2="6.30758125" layer="21"/>
<rectangle x1="17.73681875" y1="6.223" x2="19.939" y2="6.30758125" layer="21"/>
<rectangle x1="0.127" y1="6.30758125" x2="0.80518125" y2="6.39241875" layer="21"/>
<rectangle x1="2.667" y1="6.30758125" x2="3.51281875" y2="6.39241875" layer="21"/>
<rectangle x1="4.61518125" y1="6.30758125" x2="5.461" y2="6.39241875" layer="21"/>
<rectangle x1="7.239" y1="6.30758125" x2="8.08481875" y2="6.39241875" layer="21"/>
<rectangle x1="9.017" y1="6.30758125" x2="9.94918125" y2="6.39241875" layer="21"/>
<rectangle x1="10.62481875" y1="6.30758125" x2="11.98118125" y2="6.39241875" layer="21"/>
<rectangle x1="12.65681875" y1="6.30758125" x2="13.335" y2="6.39241875" layer="21"/>
<rectangle x1="13.67281875" y1="6.30758125" x2="15.113" y2="6.39241875" layer="21"/>
<rectangle x1="15.45081875" y1="6.30758125" x2="15.79118125" y2="6.39241875" layer="21"/>
<rectangle x1="16.21281875" y1="6.30758125" x2="17.48281875" y2="6.39241875" layer="21"/>
<rectangle x1="17.653" y1="6.30758125" x2="19.939" y2="6.39241875" layer="21"/>
<rectangle x1="0.127" y1="6.39241875" x2="0.71881875" y2="6.477" layer="21"/>
<rectangle x1="2.58318125" y1="6.39241875" x2="3.34518125" y2="6.477" layer="21"/>
<rectangle x1="4.78281875" y1="6.39241875" x2="5.54481875" y2="6.477" layer="21"/>
<rectangle x1="7.32281875" y1="6.39241875" x2="8.17118125" y2="6.477" layer="21"/>
<rectangle x1="9.017" y1="6.39241875" x2="9.86281875" y2="6.477" layer="21"/>
<rectangle x1="10.541" y1="6.39241875" x2="12.065" y2="6.477" layer="21"/>
<rectangle x1="12.65681875" y1="6.39241875" x2="13.335" y2="6.477" layer="21"/>
<rectangle x1="13.67281875" y1="6.39241875" x2="15.113" y2="6.477" layer="21"/>
<rectangle x1="15.45081875" y1="6.39241875" x2="15.79118125" y2="6.477" layer="21"/>
<rectangle x1="16.21281875" y1="6.39241875" x2="19.939" y2="6.477" layer="21"/>
<rectangle x1="0.127" y1="6.477" x2="0.71881875" y2="6.56158125" layer="21"/>
<rectangle x1="2.58318125" y1="6.477" x2="3.25881875" y2="6.56158125" layer="21"/>
<rectangle x1="4.86918125" y1="6.477" x2="5.54481875" y2="6.56158125" layer="21"/>
<rectangle x1="7.493" y1="6.477" x2="8.17118125" y2="6.56158125" layer="21"/>
<rectangle x1="9.017" y1="6.477" x2="9.86281875" y2="6.56158125" layer="21"/>
<rectangle x1="10.45718125" y1="6.477" x2="12.14881875" y2="6.56158125" layer="21"/>
<rectangle x1="12.74318125" y1="6.477" x2="13.335" y2="6.56158125" layer="21"/>
<rectangle x1="13.67281875" y1="6.477" x2="15.113" y2="6.56158125" layer="21"/>
<rectangle x1="15.45081875" y1="6.477" x2="15.79118125" y2="6.56158125" layer="21"/>
<rectangle x1="16.21281875" y1="6.477" x2="19.939" y2="6.56158125" layer="21"/>
<rectangle x1="0.127" y1="6.56158125" x2="0.71881875" y2="6.64641875" layer="21"/>
<rectangle x1="2.49681875" y1="6.56158125" x2="3.175" y2="6.64641875" layer="21"/>
<rectangle x1="4.953" y1="6.56158125" x2="5.63118125" y2="6.64641875" layer="21"/>
<rectangle x1="7.493" y1="6.56158125" x2="8.255" y2="6.64641875" layer="21"/>
<rectangle x1="9.017" y1="6.56158125" x2="9.779" y2="6.64641875" layer="21"/>
<rectangle x1="10.37081875" y1="6.56158125" x2="12.23518125" y2="6.64641875" layer="21"/>
<rectangle x1="12.74318125" y1="6.56158125" x2="13.335" y2="6.64641875" layer="21"/>
<rectangle x1="13.67281875" y1="6.56158125" x2="15.02918125" y2="6.64641875" layer="21"/>
<rectangle x1="15.45081875" y1="6.56158125" x2="15.79118125" y2="6.64641875" layer="21"/>
<rectangle x1="17.82318125" y1="6.56158125" x2="19.939" y2="6.64641875" layer="21"/>
<rectangle x1="0.127" y1="6.64641875" x2="0.71881875" y2="6.731" layer="21"/>
<rectangle x1="2.49681875" y1="6.64641875" x2="3.175" y2="6.731" layer="21"/>
<rectangle x1="4.953" y1="6.64641875" x2="5.63118125" y2="6.731" layer="21"/>
<rectangle x1="7.57681875" y1="6.64641875" x2="8.255" y2="6.731" layer="21"/>
<rectangle x1="9.017" y1="6.64641875" x2="9.779" y2="6.731" layer="21"/>
<rectangle x1="10.287" y1="6.64641875" x2="12.319" y2="6.731" layer="21"/>
<rectangle x1="12.827" y1="6.64641875" x2="13.335" y2="6.731" layer="21"/>
<rectangle x1="13.67281875" y1="6.64641875" x2="15.02918125" y2="6.731" layer="21"/>
<rectangle x1="15.45081875" y1="6.64641875" x2="15.70481875" y2="6.731" layer="21"/>
<rectangle x1="17.907" y1="6.64641875" x2="19.939" y2="6.731" layer="21"/>
<rectangle x1="0.04318125" y1="6.731" x2="0.71881875" y2="6.81558125" layer="21"/>
<rectangle x1="2.49681875" y1="6.731" x2="3.09118125" y2="6.81558125" layer="21"/>
<rectangle x1="5.03681875" y1="6.731" x2="5.715" y2="6.81558125" layer="21"/>
<rectangle x1="7.66318125" y1="6.731" x2="8.255" y2="6.81558125" layer="21"/>
<rectangle x1="9.017" y1="6.731" x2="9.779" y2="6.81558125" layer="21"/>
<rectangle x1="10.287" y1="6.731" x2="12.319" y2="6.81558125" layer="21"/>
<rectangle x1="12.827" y1="6.731" x2="13.335" y2="6.81558125" layer="21"/>
<rectangle x1="13.67281875" y1="6.731" x2="15.113" y2="6.81558125" layer="21"/>
<rectangle x1="15.45081875" y1="6.731" x2="15.70481875" y2="6.81558125" layer="21"/>
<rectangle x1="17.907" y1="6.731" x2="19.939" y2="6.81558125" layer="21"/>
<rectangle x1="0.04318125" y1="6.81558125" x2="0.71881875" y2="6.90041875" layer="21"/>
<rectangle x1="2.413" y1="6.81558125" x2="3.09118125" y2="6.90041875" layer="21"/>
<rectangle x1="5.03681875" y1="6.81558125" x2="5.715" y2="6.90041875" layer="21"/>
<rectangle x1="7.66318125" y1="6.81558125" x2="8.33881875" y2="6.90041875" layer="21"/>
<rectangle x1="9.017" y1="6.81558125" x2="9.69518125" y2="6.90041875" layer="21"/>
<rectangle x1="10.20318125" y1="6.81558125" x2="12.319" y2="6.90041875" layer="21"/>
<rectangle x1="12.827" y1="6.81558125" x2="13.335" y2="6.90041875" layer="21"/>
<rectangle x1="13.67281875" y1="6.81558125" x2="15.02918125" y2="6.90041875" layer="21"/>
<rectangle x1="15.45081875" y1="6.81558125" x2="15.70481875" y2="6.90041875" layer="21"/>
<rectangle x1="17.907" y1="6.81558125" x2="19.939" y2="6.90041875" layer="21"/>
<rectangle x1="0.04318125" y1="6.90041875" x2="0.635" y2="6.985" layer="21"/>
<rectangle x1="2.413" y1="6.90041875" x2="3.00481875" y2="6.985" layer="21"/>
<rectangle x1="5.12318125" y1="6.90041875" x2="5.715" y2="6.985" layer="21"/>
<rectangle x1="7.66318125" y1="6.90041875" x2="8.33881875" y2="6.985" layer="21"/>
<rectangle x1="9.017" y1="6.90041875" x2="9.69518125" y2="6.985" layer="21"/>
<rectangle x1="10.20318125" y1="6.90041875" x2="12.40281875" y2="6.985" layer="21"/>
<rectangle x1="12.91081875" y1="6.90041875" x2="13.335" y2="6.985" layer="21"/>
<rectangle x1="13.67281875" y1="6.90041875" x2="15.02918125" y2="6.985" layer="21"/>
<rectangle x1="15.45081875" y1="6.90041875" x2="15.79118125" y2="6.985" layer="21"/>
<rectangle x1="16.21281875" y1="6.90041875" x2="17.399" y2="6.985" layer="21"/>
<rectangle x1="17.907" y1="6.90041875" x2="19.939" y2="6.985" layer="21"/>
<rectangle x1="0.04318125" y1="6.985" x2="0.635" y2="7.06958125" layer="21"/>
<rectangle x1="2.413" y1="6.985" x2="3.00481875" y2="7.06958125" layer="21"/>
<rectangle x1="5.12318125" y1="6.985" x2="5.715" y2="7.06958125" layer="21"/>
<rectangle x1="7.747" y1="6.985" x2="8.33881875" y2="7.06958125" layer="21"/>
<rectangle x1="9.017" y1="6.985" x2="9.69518125" y2="7.06958125" layer="21"/>
<rectangle x1="10.20318125" y1="6.985" x2="12.40281875" y2="7.06958125" layer="21"/>
<rectangle x1="12.91081875" y1="6.985" x2="13.335" y2="7.06958125" layer="21"/>
<rectangle x1="13.75918125" y1="6.985" x2="15.02918125" y2="7.06958125" layer="21"/>
<rectangle x1="15.45081875" y1="6.985" x2="15.79118125" y2="7.06958125" layer="21"/>
<rectangle x1="16.21281875" y1="6.985" x2="17.48281875" y2="7.06958125" layer="21"/>
<rectangle x1="17.82318125" y1="6.985" x2="19.939" y2="7.06958125" layer="21"/>
<rectangle x1="0.04318125" y1="7.06958125" x2="0.635" y2="7.15441875" layer="21"/>
<rectangle x1="2.413" y1="7.06958125" x2="3.00481875" y2="7.15441875" layer="21"/>
<rectangle x1="5.12318125" y1="7.06958125" x2="5.715" y2="7.15441875" layer="21"/>
<rectangle x1="7.747" y1="7.06958125" x2="8.33881875" y2="7.15441875" layer="21"/>
<rectangle x1="9.017" y1="7.06958125" x2="9.69518125" y2="7.15441875" layer="21"/>
<rectangle x1="10.20318125" y1="7.06958125" x2="12.40281875" y2="7.15441875" layer="21"/>
<rectangle x1="12.91081875" y1="7.06958125" x2="13.335" y2="7.15441875" layer="21"/>
<rectangle x1="13.75918125" y1="7.06958125" x2="14.94281875" y2="7.15441875" layer="21"/>
<rectangle x1="15.367" y1="7.06958125" x2="15.79118125" y2="7.15441875" layer="21"/>
<rectangle x1="16.21281875" y1="7.06958125" x2="17.399" y2="7.15441875" layer="21"/>
<rectangle x1="17.82318125" y1="7.06958125" x2="19.939" y2="7.15441875" layer="21"/>
<rectangle x1="0.04318125" y1="7.15441875" x2="0.635" y2="7.239" layer="21"/>
<rectangle x1="2.413" y1="7.15441875" x2="3.00481875" y2="7.239" layer="21"/>
<rectangle x1="5.12318125" y1="7.15441875" x2="5.715" y2="7.239" layer="21"/>
<rectangle x1="7.747" y1="7.15441875" x2="8.33881875" y2="7.239" layer="21"/>
<rectangle x1="9.017" y1="7.15441875" x2="9.69518125" y2="7.239" layer="21"/>
<rectangle x1="10.20318125" y1="7.15441875" x2="12.40281875" y2="7.239" layer="21"/>
<rectangle x1="12.91081875" y1="7.15441875" x2="13.335" y2="7.239" layer="21"/>
<rectangle x1="13.843" y1="7.15441875" x2="14.94281875" y2="7.239" layer="21"/>
<rectangle x1="15.367" y1="7.15441875" x2="15.875" y2="7.239" layer="21"/>
<rectangle x1="16.29918125" y1="7.15441875" x2="17.31518125" y2="7.239" layer="21"/>
<rectangle x1="17.82318125" y1="7.15441875" x2="19.939" y2="7.239" layer="21"/>
<rectangle x1="0.04318125" y1="7.239" x2="0.635" y2="7.32358125" layer="21"/>
<rectangle x1="2.413" y1="7.239" x2="3.00481875" y2="7.32358125" layer="21"/>
<rectangle x1="5.12318125" y1="7.239" x2="5.715" y2="7.32358125" layer="21"/>
<rectangle x1="7.747" y1="7.239" x2="8.33881875" y2="7.32358125" layer="21"/>
<rectangle x1="9.017" y1="7.239" x2="9.69518125" y2="7.32358125" layer="21"/>
<rectangle x1="10.20318125" y1="7.239" x2="12.40281875" y2="7.32358125" layer="21"/>
<rectangle x1="12.91081875" y1="7.239" x2="13.335" y2="7.32358125" layer="21"/>
<rectangle x1="13.92681875" y1="7.239" x2="14.859" y2="7.32358125" layer="21"/>
<rectangle x1="15.28318125" y1="7.239" x2="15.875" y2="7.32358125" layer="21"/>
<rectangle x1="16.383" y1="7.239" x2="17.22881875" y2="7.32358125" layer="21"/>
<rectangle x1="17.73681875" y1="7.239" x2="19.939" y2="7.32358125" layer="21"/>
<rectangle x1="0.04318125" y1="7.32358125" x2="0.635" y2="7.40841875" layer="21"/>
<rectangle x1="2.413" y1="7.32358125" x2="3.00481875" y2="7.40841875" layer="21"/>
<rectangle x1="5.12318125" y1="7.32358125" x2="5.715" y2="7.40841875" layer="21"/>
<rectangle x1="7.747" y1="7.32358125" x2="8.33881875" y2="7.40841875" layer="21"/>
<rectangle x1="9.017" y1="7.32358125" x2="9.69518125" y2="7.40841875" layer="21"/>
<rectangle x1="10.20318125" y1="7.32358125" x2="12.40281875" y2="7.40841875" layer="21"/>
<rectangle x1="12.91081875" y1="7.32358125" x2="13.335" y2="7.40841875" layer="21"/>
<rectangle x1="14.01318125" y1="7.32358125" x2="14.68881875" y2="7.40841875" layer="21"/>
<rectangle x1="15.28318125" y1="7.32358125" x2="15.95881875" y2="7.40841875" layer="21"/>
<rectangle x1="16.46681875" y1="7.32358125" x2="17.145" y2="7.40841875" layer="21"/>
<rectangle x1="17.73681875" y1="7.32358125" x2="19.939" y2="7.40841875" layer="21"/>
<rectangle x1="0.04318125" y1="7.40841875" x2="0.635" y2="7.493" layer="21"/>
<rectangle x1="2.413" y1="7.40841875" x2="3.00481875" y2="7.493" layer="21"/>
<rectangle x1="5.12318125" y1="7.40841875" x2="5.715" y2="7.493" layer="21"/>
<rectangle x1="7.747" y1="7.40841875" x2="8.33881875" y2="7.493" layer="21"/>
<rectangle x1="9.017" y1="7.40841875" x2="9.69518125" y2="7.493" layer="21"/>
<rectangle x1="10.20318125" y1="7.40841875" x2="12.40281875" y2="7.493" layer="21"/>
<rectangle x1="12.91081875" y1="7.40841875" x2="13.335" y2="7.493" layer="21"/>
<rectangle x1="14.26718125" y1="7.40841875" x2="14.43481875" y2="7.493" layer="21"/>
<rectangle x1="15.19681875" y1="7.40841875" x2="15.95881875" y2="7.493" layer="21"/>
<rectangle x1="16.72081875" y1="7.40841875" x2="16.891" y2="7.493" layer="21"/>
<rectangle x1="17.653" y1="7.40841875" x2="19.939" y2="7.493" layer="21"/>
<rectangle x1="0.04318125" y1="7.493" x2="0.635" y2="7.57758125" layer="21"/>
<rectangle x1="2.413" y1="7.493" x2="3.00481875" y2="7.57758125" layer="21"/>
<rectangle x1="5.12318125" y1="7.493" x2="5.715" y2="7.57758125" layer="21"/>
<rectangle x1="7.66318125" y1="7.493" x2="8.33881875" y2="7.57758125" layer="21"/>
<rectangle x1="9.017" y1="7.493" x2="9.69518125" y2="7.57758125" layer="21"/>
<rectangle x1="10.20318125" y1="7.493" x2="12.40281875" y2="7.57758125" layer="21"/>
<rectangle x1="12.91081875" y1="7.493" x2="13.335" y2="7.57758125" layer="21"/>
<rectangle x1="15.113" y1="7.493" x2="16.04518125" y2="7.57758125" layer="21"/>
<rectangle x1="17.56918125" y1="7.493" x2="19.939" y2="7.57758125" layer="21"/>
<rectangle x1="0.04318125" y1="7.57758125" x2="0.71881875" y2="7.66241875" layer="21"/>
<rectangle x1="2.413" y1="7.57758125" x2="3.00481875" y2="7.66241875" layer="21"/>
<rectangle x1="5.12318125" y1="7.57758125" x2="5.715" y2="7.66241875" layer="21"/>
<rectangle x1="7.66318125" y1="7.57758125" x2="8.33881875" y2="7.66241875" layer="21"/>
<rectangle x1="9.017" y1="7.57758125" x2="9.69518125" y2="7.66241875" layer="21"/>
<rectangle x1="10.20318125" y1="7.57758125" x2="12.40281875" y2="7.66241875" layer="21"/>
<rectangle x1="12.91081875" y1="7.57758125" x2="13.335" y2="7.66241875" layer="21"/>
<rectangle x1="15.02918125" y1="7.57758125" x2="16.21281875" y2="7.66241875" layer="21"/>
<rectangle x1="17.48281875" y1="7.57758125" x2="19.939" y2="7.66241875" layer="21"/>
<rectangle x1="0.04318125" y1="7.66241875" x2="0.71881875" y2="7.747" layer="21"/>
<rectangle x1="2.413" y1="7.66241875" x2="3.00481875" y2="7.747" layer="21"/>
<rectangle x1="5.12318125" y1="7.66241875" x2="5.715" y2="7.747" layer="21"/>
<rectangle x1="7.66318125" y1="7.66241875" x2="8.255" y2="7.747" layer="21"/>
<rectangle x1="9.017" y1="7.66241875" x2="9.779" y2="7.747" layer="21"/>
<rectangle x1="10.287" y1="7.66241875" x2="12.319" y2="7.747" layer="21"/>
<rectangle x1="12.827" y1="7.66241875" x2="13.335" y2="7.747" layer="21"/>
<rectangle x1="13.67281875" y1="7.66241875" x2="13.843" y2="7.747" layer="21"/>
<rectangle x1="14.859" y1="7.66241875" x2="16.29918125" y2="7.747" layer="21"/>
<rectangle x1="17.31518125" y1="7.66241875" x2="19.939" y2="7.747" layer="21"/>
<rectangle x1="0.127" y1="7.747" x2="0.71881875" y2="7.83158125" layer="21"/>
<rectangle x1="2.413" y1="7.747" x2="3.00481875" y2="7.83158125" layer="21"/>
<rectangle x1="5.12318125" y1="7.747" x2="5.715" y2="7.83158125" layer="21"/>
<rectangle x1="7.57681875" y1="7.747" x2="8.255" y2="7.83158125" layer="21"/>
<rectangle x1="9.017" y1="7.747" x2="9.779" y2="7.83158125" layer="21"/>
<rectangle x1="10.287" y1="7.747" x2="12.319" y2="7.83158125" layer="21"/>
<rectangle x1="12.827" y1="7.747" x2="13.335" y2="7.83158125" layer="21"/>
<rectangle x1="13.589" y1="7.747" x2="14.097" y2="7.83158125" layer="21"/>
<rectangle x1="14.68881875" y1="7.747" x2="16.55318125" y2="7.83158125" layer="21"/>
<rectangle x1="17.06118125" y1="7.747" x2="19.939" y2="7.83158125" layer="21"/>
<rectangle x1="0.127" y1="7.83158125" x2="0.71881875" y2="7.91641875" layer="21"/>
<rectangle x1="2.413" y1="7.83158125" x2="3.00481875" y2="7.91641875" layer="21"/>
<rectangle x1="5.12318125" y1="7.83158125" x2="5.715" y2="7.91641875" layer="21"/>
<rectangle x1="7.57681875" y1="7.83158125" x2="8.255" y2="7.91641875" layer="21"/>
<rectangle x1="9.017" y1="7.83158125" x2="9.779" y2="7.91641875" layer="21"/>
<rectangle x1="10.37081875" y1="7.83158125" x2="12.23518125" y2="7.91641875" layer="21"/>
<rectangle x1="12.827" y1="7.83158125" x2="19.939" y2="7.91641875" layer="21"/>
<rectangle x1="0.127" y1="7.91641875" x2="0.71881875" y2="8.001" layer="21"/>
<rectangle x1="2.413" y1="7.91641875" x2="3.00481875" y2="8.001" layer="21"/>
<rectangle x1="5.12318125" y1="7.91641875" x2="5.715" y2="8.001" layer="21"/>
<rectangle x1="7.493" y1="7.91641875" x2="8.17118125" y2="8.001" layer="21"/>
<rectangle x1="9.017" y1="7.91641875" x2="9.86281875" y2="8.001" layer="21"/>
<rectangle x1="10.45718125" y1="7.91641875" x2="12.14881875" y2="8.001" layer="21"/>
<rectangle x1="12.74318125" y1="7.91641875" x2="19.939" y2="8.001" layer="21"/>
<rectangle x1="0.127" y1="8.001" x2="0.71881875" y2="8.08558125" layer="21"/>
<rectangle x1="2.413" y1="8.001" x2="3.00481875" y2="8.08558125" layer="21"/>
<rectangle x1="5.12318125" y1="8.001" x2="5.715" y2="8.08558125" layer="21"/>
<rectangle x1="7.40918125" y1="8.001" x2="8.17118125" y2="8.08558125" layer="21"/>
<rectangle x1="9.017" y1="8.001" x2="9.86281875" y2="8.08558125" layer="21"/>
<rectangle x1="10.45718125" y1="8.001" x2="12.065" y2="8.08558125" layer="21"/>
<rectangle x1="12.74318125" y1="8.001" x2="19.939" y2="8.08558125" layer="21"/>
<rectangle x1="0.127" y1="8.08558125" x2="0.80518125" y2="8.17041875" layer="21"/>
<rectangle x1="2.413" y1="8.08558125" x2="3.00481875" y2="8.17041875" layer="21"/>
<rectangle x1="5.12318125" y1="8.08558125" x2="5.715" y2="8.17041875" layer="21"/>
<rectangle x1="7.32281875" y1="8.08558125" x2="8.08481875" y2="8.17041875" layer="21"/>
<rectangle x1="9.017" y1="8.08558125" x2="9.94918125" y2="8.17041875" layer="21"/>
<rectangle x1="10.62481875" y1="8.08558125" x2="11.98118125" y2="8.17041875" layer="21"/>
<rectangle x1="12.65681875" y1="8.08558125" x2="19.939" y2="8.17041875" layer="21"/>
<rectangle x1="0.127" y1="8.17041875" x2="0.80518125" y2="8.255" layer="21"/>
<rectangle x1="2.413" y1="8.17041875" x2="3.00481875" y2="8.255" layer="21"/>
<rectangle x1="5.12318125" y1="8.17041875" x2="5.715" y2="8.255" layer="21"/>
<rectangle x1="7.15518125" y1="8.17041875" x2="8.001" y2="8.255" layer="21"/>
<rectangle x1="9.017" y1="8.17041875" x2="9.94918125" y2="8.255" layer="21"/>
<rectangle x1="10.71118125" y1="8.17041875" x2="11.89481875" y2="8.255" layer="21"/>
<rectangle x1="12.573" y1="8.17041875" x2="19.939" y2="8.255" layer="21"/>
<rectangle x1="0.21081875" y1="8.255" x2="0.80518125" y2="8.33958125" layer="21"/>
<rectangle x1="2.413" y1="8.255" x2="3.00481875" y2="8.33958125" layer="21"/>
<rectangle x1="5.12318125" y1="8.255" x2="5.715" y2="8.33958125" layer="21"/>
<rectangle x1="6.90118125" y1="8.255" x2="8.001" y2="8.33958125" layer="21"/>
<rectangle x1="9.017" y1="8.255" x2="10.033" y2="8.33958125" layer="21"/>
<rectangle x1="10.87881875" y1="8.255" x2="11.72718125" y2="8.33958125" layer="21"/>
<rectangle x1="12.573" y1="8.255" x2="19.939" y2="8.33958125" layer="21"/>
<rectangle x1="0.21081875" y1="8.33958125" x2="0.889" y2="8.42441875" layer="21"/>
<rectangle x1="2.413" y1="8.33958125" x2="3.00481875" y2="8.42441875" layer="21"/>
<rectangle x1="5.12318125" y1="8.33958125" x2="7.91718125" y2="8.42441875" layer="21"/>
<rectangle x1="9.10081875" y1="8.33958125" x2="10.11681875" y2="8.42441875" layer="21"/>
<rectangle x1="11.21918125" y1="8.33958125" x2="11.38681875" y2="8.42441875" layer="21"/>
<rectangle x1="12.48918125" y1="8.33958125" x2="19.85518125" y2="8.42441875" layer="21"/>
<rectangle x1="0.21081875" y1="8.42441875" x2="0.889" y2="8.509" layer="21"/>
<rectangle x1="2.413" y1="8.42441875" x2="3.00481875" y2="8.509" layer="21"/>
<rectangle x1="5.12318125" y1="8.42441875" x2="7.83081875" y2="8.509" layer="21"/>
<rectangle x1="9.10081875" y1="8.42441875" x2="10.20318125" y2="8.509" layer="21"/>
<rectangle x1="12.40281875" y1="8.42441875" x2="19.85518125" y2="8.509" layer="21"/>
<rectangle x1="0.29718125" y1="8.509" x2="0.889" y2="8.59358125" layer="21"/>
<rectangle x1="2.413" y1="8.509" x2="3.00481875" y2="8.59358125" layer="21"/>
<rectangle x1="5.12318125" y1="8.509" x2="7.747" y2="8.59358125" layer="21"/>
<rectangle x1="9.10081875" y1="8.509" x2="10.287" y2="8.59358125" layer="21"/>
<rectangle x1="12.23518125" y1="8.509" x2="19.85518125" y2="8.59358125" layer="21"/>
<rectangle x1="0.29718125" y1="8.59358125" x2="0.97281875" y2="8.67841875" layer="21"/>
<rectangle x1="2.413" y1="8.59358125" x2="3.00481875" y2="8.67841875" layer="21"/>
<rectangle x1="5.12318125" y1="8.59358125" x2="7.57681875" y2="8.67841875" layer="21"/>
<rectangle x1="9.10081875" y1="8.59358125" x2="10.45718125" y2="8.67841875" layer="21"/>
<rectangle x1="12.14881875" y1="8.59358125" x2="19.76881875" y2="8.67841875" layer="21"/>
<rectangle x1="0.29718125" y1="8.67841875" x2="0.97281875" y2="8.763" layer="21"/>
<rectangle x1="2.413" y1="8.67841875" x2="3.00481875" y2="8.763" layer="21"/>
<rectangle x1="5.12318125" y1="8.67841875" x2="7.493" y2="8.763" layer="21"/>
<rectangle x1="9.10081875" y1="8.67841875" x2="10.62481875" y2="8.763" layer="21"/>
<rectangle x1="11.98118125" y1="8.67841875" x2="19.76881875" y2="8.763" layer="21"/>
<rectangle x1="0.381" y1="8.763" x2="1.05918125" y2="8.84758125" layer="21"/>
<rectangle x1="2.413" y1="8.763" x2="2.921" y2="8.84758125" layer="21"/>
<rectangle x1="5.207" y1="8.763" x2="7.32281875" y2="8.84758125" layer="21"/>
<rectangle x1="9.10081875" y1="8.763" x2="10.795" y2="8.84758125" layer="21"/>
<rectangle x1="11.72718125" y1="8.763" x2="19.76881875" y2="8.84758125" layer="21"/>
<rectangle x1="0.381" y1="8.84758125" x2="1.05918125" y2="8.93241875" layer="21"/>
<rectangle x1="2.58318125" y1="8.84758125" x2="2.83718125" y2="8.93241875" layer="21"/>
<rectangle x1="5.29081875" y1="8.84758125" x2="7.06881875" y2="8.93241875" layer="21"/>
<rectangle x1="9.18718125" y1="8.84758125" x2="19.685" y2="8.93241875" layer="21"/>
<rectangle x1="0.46481875" y1="8.93241875" x2="1.143" y2="9.017" layer="21"/>
<rectangle x1="9.18718125" y1="8.93241875" x2="19.685" y2="9.017" layer="21"/>
<rectangle x1="0.46481875" y1="9.017" x2="1.143" y2="9.10158125" layer="21"/>
<rectangle x1="9.18718125" y1="9.017" x2="19.60118125" y2="9.10158125" layer="21"/>
<rectangle x1="0.55118125" y1="9.10158125" x2="1.22681875" y2="9.18641875" layer="21"/>
<rectangle x1="9.18718125" y1="9.10158125" x2="19.60118125" y2="9.18641875" layer="21"/>
<rectangle x1="0.55118125" y1="9.18641875" x2="1.31318125" y2="9.271" layer="21"/>
<rectangle x1="9.18718125" y1="9.18641875" x2="19.51481875" y2="9.271" layer="21"/>
<rectangle x1="0.635" y1="9.271" x2="1.397" y2="9.35558125" layer="21"/>
<rectangle x1="9.271" y1="9.271" x2="19.51481875" y2="9.35558125" layer="21"/>
<rectangle x1="0.71881875" y1="9.35558125" x2="1.397" y2="9.44041875" layer="21"/>
<rectangle x1="9.271" y1="9.35558125" x2="19.431" y2="9.44041875" layer="21"/>
<rectangle x1="0.71881875" y1="9.44041875" x2="1.48081875" y2="9.525" layer="21"/>
<rectangle x1="9.271" y1="9.44041875" x2="19.34718125" y2="9.525" layer="21"/>
<rectangle x1="0.80518125" y1="9.525" x2="1.56718125" y2="9.60958125" layer="21"/>
<rectangle x1="9.35481875" y1="9.525" x2="19.34718125" y2="9.60958125" layer="21"/>
<rectangle x1="0.889" y1="9.60958125" x2="1.651" y2="9.69441875" layer="21"/>
<rectangle x1="9.35481875" y1="9.60958125" x2="19.26081875" y2="9.69441875" layer="21"/>
<rectangle x1="0.889" y1="9.69441875" x2="1.73481875" y2="9.779" layer="21"/>
<rectangle x1="9.35481875" y1="9.69441875" x2="19.177" y2="9.779" layer="21"/>
<rectangle x1="0.97281875" y1="9.779" x2="1.82118125" y2="9.86358125" layer="21"/>
<rectangle x1="9.44118125" y1="9.779" x2="19.09318125" y2="9.86358125" layer="21"/>
<rectangle x1="1.05918125" y1="9.86358125" x2="1.905" y2="9.94841875" layer="21"/>
<rectangle x1="9.44118125" y1="9.86358125" x2="19.00681875" y2="9.94841875" layer="21"/>
<rectangle x1="1.143" y1="9.94841875" x2="2.07518125" y2="10.033" layer="21"/>
<rectangle x1="9.44118125" y1="9.94841875" x2="18.923" y2="10.033" layer="21"/>
<rectangle x1="1.22681875" y1="10.033" x2="2.159" y2="10.11758125" layer="21"/>
<rectangle x1="9.525" y1="10.033" x2="18.923" y2="10.11758125" layer="21"/>
<rectangle x1="1.31318125" y1="10.11758125" x2="2.24281875" y2="10.20241875" layer="21"/>
<rectangle x1="9.525" y1="10.11758125" x2="18.75281875" y2="10.20241875" layer="21"/>
<rectangle x1="1.397" y1="10.20241875" x2="2.413" y2="10.287" layer="21"/>
<rectangle x1="9.60881875" y1="10.20241875" x2="18.669" y2="10.287" layer="21"/>
<rectangle x1="1.48081875" y1="10.287" x2="2.58318125" y2="10.37158125" layer="21"/>
<rectangle x1="9.60881875" y1="10.287" x2="18.58518125" y2="10.37158125" layer="21"/>
<rectangle x1="1.56718125" y1="10.37158125" x2="2.75081875" y2="10.45641875" layer="21"/>
<rectangle x1="9.69518125" y1="10.37158125" x2="18.49881875" y2="10.45641875" layer="21"/>
<rectangle x1="1.73481875" y1="10.45641875" x2="3.00481875" y2="10.541" layer="21"/>
<rectangle x1="9.69518125" y1="10.45641875" x2="18.415" y2="10.541" layer="21"/>
<rectangle x1="1.82118125" y1="10.541" x2="3.34518125" y2="10.62558125" layer="21"/>
<rectangle x1="9.779" y1="10.541" x2="18.24481875" y2="10.62558125" layer="21"/>
<rectangle x1="1.98881875" y1="10.62558125" x2="9.69518125" y2="10.71041875" layer="21"/>
<rectangle x1="9.779" y1="10.62558125" x2="18.161" y2="10.71041875" layer="21"/>
<rectangle x1="2.07518125" y1="10.71041875" x2="17.99081875" y2="10.795" layer="21"/>
<rectangle x1="2.24281875" y1="10.795" x2="17.82318125" y2="10.87958125" layer="21"/>
<rectangle x1="2.413" y1="10.87958125" x2="17.653" y2="10.96441875" layer="21"/>
<rectangle x1="2.58318125" y1="10.96441875" x2="17.48281875" y2="11.049" layer="21"/>
<rectangle x1="2.83718125" y1="11.049" x2="17.22881875" y2="11.13358125" layer="21"/>
<rectangle x1="3.175" y1="11.13358125" x2="16.891" y2="11.21841875" layer="21"/>
</package>
<package name="UDO-LOGO-30MM">
<rectangle x1="4.36118125" y1="0.55041875" x2="5.29081875" y2="0.635" layer="21"/>
<rectangle x1="21.29281875" y1="0.55041875" x2="22.30881875" y2="0.635" layer="21"/>
<rectangle x1="4.191" y1="0.635" x2="5.54481875" y2="0.71958125" layer="21"/>
<rectangle x1="6.731" y1="0.635" x2="6.81481875" y2="0.71958125" layer="21"/>
<rectangle x1="8.42518125" y1="0.635" x2="8.509" y2="0.71958125" layer="21"/>
<rectangle x1="8.93318125" y1="0.635" x2="9.10081875" y2="0.71958125" layer="21"/>
<rectangle x1="10.541" y1="0.635" x2="10.795" y2="0.71958125" layer="21"/>
<rectangle x1="11.38681875" y1="0.635" x2="12.65681875" y2="0.71958125" layer="21"/>
<rectangle x1="14.859" y1="0.635" x2="15.28318125" y2="0.71958125" layer="21"/>
<rectangle x1="16.80718125" y1="0.635" x2="17.145" y2="0.71958125" layer="21"/>
<rectangle x1="18.669" y1="0.635" x2="19.09318125" y2="0.71958125" layer="21"/>
<rectangle x1="21.12518125" y1="0.635" x2="22.479" y2="0.71958125" layer="21"/>
<rectangle x1="23.495" y1="0.635" x2="23.66518125" y2="0.71958125" layer="21"/>
<rectangle x1="25.273" y1="0.635" x2="25.35681875" y2="0.71958125" layer="21"/>
<rectangle x1="26.45918125" y1="0.635" x2="26.797" y2="0.71958125" layer="21"/>
<rectangle x1="4.10718125" y1="0.71958125" x2="5.715" y2="0.80441875" layer="21"/>
<rectangle x1="6.64718125" y1="0.71958125" x2="6.90118125" y2="0.80441875" layer="21"/>
<rectangle x1="8.33881875" y1="0.71958125" x2="8.59281875" y2="0.80441875" layer="21"/>
<rectangle x1="8.84681875" y1="0.71958125" x2="9.18718125" y2="0.80441875" layer="21"/>
<rectangle x1="10.287" y1="0.71958125" x2="10.87881875" y2="0.80441875" layer="21"/>
<rectangle x1="11.303" y1="0.71958125" x2="12.91081875" y2="0.80441875" layer="21"/>
<rectangle x1="14.605" y1="0.71958125" x2="15.53718125" y2="0.80441875" layer="21"/>
<rectangle x1="16.637" y1="0.71958125" x2="17.399" y2="0.80441875" layer="21"/>
<rectangle x1="18.415" y1="0.71958125" x2="19.34718125" y2="0.80441875" layer="21"/>
<rectangle x1="20.955" y1="0.71958125" x2="22.64918125" y2="0.80441875" layer="21"/>
<rectangle x1="23.495" y1="0.71958125" x2="23.749" y2="0.80441875" layer="21"/>
<rectangle x1="25.18918125" y1="0.71958125" x2="25.44318125" y2="0.80441875" layer="21"/>
<rectangle x1="26.20518125" y1="0.71958125" x2="27.13481875" y2="0.80441875" layer="21"/>
<rectangle x1="3.937" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="5.12318125" y1="0.80441875" x2="5.79881875" y2="0.889" layer="21"/>
<rectangle x1="6.64718125" y1="0.80441875" x2="6.90118125" y2="0.889" layer="21"/>
<rectangle x1="8.33881875" y1="0.80441875" x2="8.59281875" y2="0.889" layer="21"/>
<rectangle x1="8.84681875" y1="0.80441875" x2="9.18718125" y2="0.889" layer="21"/>
<rectangle x1="10.11681875" y1="0.80441875" x2="10.87881875" y2="0.889" layer="21"/>
<rectangle x1="11.303" y1="0.80441875" x2="13.081" y2="0.889" layer="21"/>
<rectangle x1="14.52118125" y1="0.80441875" x2="15.621" y2="0.889" layer="21"/>
<rectangle x1="16.46681875" y1="0.80441875" x2="17.48281875" y2="0.889" layer="21"/>
<rectangle x1="18.33118125" y1="0.80441875" x2="19.431" y2="0.889" layer="21"/>
<rectangle x1="20.87118125" y1="0.80441875" x2="21.54681875" y2="0.889" layer="21"/>
<rectangle x1="22.05481875" y1="0.80441875" x2="22.733" y2="0.889" layer="21"/>
<rectangle x1="23.495" y1="0.80441875" x2="23.749" y2="0.889" layer="21"/>
<rectangle x1="25.18918125" y1="0.80441875" x2="25.44318125" y2="0.889" layer="21"/>
<rectangle x1="26.035" y1="0.80441875" x2="27.22118125" y2="0.889" layer="21"/>
<rectangle x1="3.85318125" y1="0.889" x2="4.36118125" y2="0.97358125" layer="21"/>
<rectangle x1="5.37718125" y1="0.889" x2="5.88518125" y2="0.97358125" layer="21"/>
<rectangle x1="6.64718125" y1="0.889" x2="6.90118125" y2="0.97358125" layer="21"/>
<rectangle x1="8.33881875" y1="0.889" x2="8.59281875" y2="0.97358125" layer="21"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.18718125" y2="0.97358125" layer="21"/>
<rectangle x1="10.033" y1="0.889" x2="10.795" y2="0.97358125" layer="21"/>
<rectangle x1="11.303" y1="0.889" x2="13.25118125" y2="0.97358125" layer="21"/>
<rectangle x1="14.351" y1="0.889" x2="15.79118125" y2="0.97358125" layer="21"/>
<rectangle x1="16.383" y1="0.889" x2="17.56918125" y2="0.97358125" layer="21"/>
<rectangle x1="18.24481875" y1="0.889" x2="19.60118125" y2="0.97358125" layer="21"/>
<rectangle x1="20.78481875" y1="0.889" x2="21.29281875" y2="0.97358125" layer="21"/>
<rectangle x1="22.30881875" y1="0.889" x2="22.81681875" y2="0.97358125" layer="21"/>
<rectangle x1="23.495" y1="0.889" x2="23.749" y2="0.97358125" layer="21"/>
<rectangle x1="25.18918125" y1="0.889" x2="25.44318125" y2="0.97358125" layer="21"/>
<rectangle x1="25.95118125" y1="0.889" x2="27.305" y2="0.97358125" layer="21"/>
<rectangle x1="3.76681875" y1="0.97358125" x2="4.191" y2="1.05841875" layer="21"/>
<rectangle x1="5.54481875" y1="0.97358125" x2="5.969" y2="1.05841875" layer="21"/>
<rectangle x1="6.64718125" y1="0.97358125" x2="6.90118125" y2="1.05841875" layer="21"/>
<rectangle x1="8.33881875" y1="0.97358125" x2="8.59281875" y2="1.05841875" layer="21"/>
<rectangle x1="8.84681875" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="21"/>
<rectangle x1="9.94918125" y1="0.97358125" x2="10.45718125" y2="1.05841875" layer="21"/>
<rectangle x1="11.303" y1="0.97358125" x2="11.557" y2="1.05841875" layer="21"/>
<rectangle x1="12.827" y1="0.97358125" x2="13.335" y2="1.05841875" layer="21"/>
<rectangle x1="14.351" y1="0.97358125" x2="14.859" y2="1.05841875" layer="21"/>
<rectangle x1="15.367" y1="0.97358125" x2="15.875" y2="1.05841875" layer="21"/>
<rectangle x1="16.383" y1="0.97358125" x2="16.80718125" y2="1.05841875" layer="21"/>
<rectangle x1="17.22881875" y1="0.97358125" x2="17.653" y2="1.05841875" layer="21"/>
<rectangle x1="18.161" y1="0.97358125" x2="18.58518125" y2="1.05841875" layer="21"/>
<rectangle x1="19.177" y1="0.97358125" x2="19.685" y2="1.05841875" layer="21"/>
<rectangle x1="20.701" y1="0.97358125" x2="21.12518125" y2="1.05841875" layer="21"/>
<rectangle x1="22.479" y1="0.97358125" x2="22.90318125" y2="1.05841875" layer="21"/>
<rectangle x1="23.495" y1="0.97358125" x2="23.749" y2="1.05841875" layer="21"/>
<rectangle x1="25.18918125" y1="0.97358125" x2="25.44318125" y2="1.05841875" layer="21"/>
<rectangle x1="25.86481875" y1="0.97358125" x2="26.37281875" y2="1.05841875" layer="21"/>
<rectangle x1="26.96718125" y1="0.97358125" x2="27.38881875" y2="1.05841875" layer="21"/>
<rectangle x1="3.683" y1="1.05841875" x2="4.10718125" y2="1.143" layer="21"/>
<rectangle x1="5.63118125" y1="1.05841875" x2="6.05281875" y2="1.143" layer="21"/>
<rectangle x1="6.64718125" y1="1.05841875" x2="6.90118125" y2="1.143" layer="21"/>
<rectangle x1="8.33881875" y1="1.05841875" x2="8.59281875" y2="1.143" layer="21"/>
<rectangle x1="8.84681875" y1="1.05841875" x2="9.18718125" y2="1.143" layer="21"/>
<rectangle x1="9.86281875" y1="1.05841875" x2="10.287" y2="1.143" layer="21"/>
<rectangle x1="11.303" y1="1.05841875" x2="11.557" y2="1.143" layer="21"/>
<rectangle x1="12.99718125" y1="1.05841875" x2="13.41881875" y2="1.143" layer="21"/>
<rectangle x1="14.26718125" y1="1.05841875" x2="14.68881875" y2="1.143" layer="21"/>
<rectangle x1="15.53718125" y1="1.05841875" x2="15.875" y2="1.143" layer="21"/>
<rectangle x1="16.29918125" y1="1.05841875" x2="16.637" y2="1.143" layer="21"/>
<rectangle x1="17.399" y1="1.05841875" x2="17.653" y2="1.143" layer="21"/>
<rectangle x1="18.07718125" y1="1.05841875" x2="18.49881875" y2="1.143" layer="21"/>
<rectangle x1="19.34718125" y1="1.05841875" x2="19.685" y2="1.143" layer="21"/>
<rectangle x1="20.61718125" y1="1.05841875" x2="21.03881875" y2="1.143" layer="21"/>
<rectangle x1="22.56281875" y1="1.05841875" x2="22.987" y2="1.143" layer="21"/>
<rectangle x1="23.495" y1="1.05841875" x2="23.749" y2="1.143" layer="21"/>
<rectangle x1="25.18918125" y1="1.05841875" x2="25.44318125" y2="1.143" layer="21"/>
<rectangle x1="25.86481875" y1="1.05841875" x2="26.20518125" y2="1.143" layer="21"/>
<rectangle x1="27.051" y1="1.05841875" x2="27.47518125" y2="1.143" layer="21"/>
<rectangle x1="3.683" y1="1.143" x2="4.02081875" y2="1.22758125" layer="21"/>
<rectangle x1="5.715" y1="1.143" x2="6.13918125" y2="1.22758125" layer="21"/>
<rectangle x1="6.64718125" y1="1.143" x2="6.90118125" y2="1.22758125" layer="21"/>
<rectangle x1="8.33881875" y1="1.143" x2="8.59281875" y2="1.22758125" layer="21"/>
<rectangle x1="8.84681875" y1="1.143" x2="9.18718125" y2="1.22758125" layer="21"/>
<rectangle x1="9.779" y1="1.143" x2="10.20318125" y2="1.22758125" layer="21"/>
<rectangle x1="11.303" y1="1.143" x2="11.557" y2="1.22758125" layer="21"/>
<rectangle x1="13.081" y1="1.143" x2="13.50518125" y2="1.22758125" layer="21"/>
<rectangle x1="14.18081875" y1="1.143" x2="14.52118125" y2="1.22758125" layer="21"/>
<rectangle x1="15.621" y1="1.143" x2="15.95881875" y2="1.22758125" layer="21"/>
<rectangle x1="16.383" y1="1.143" x2="16.637" y2="1.22758125" layer="21"/>
<rectangle x1="17.399" y1="1.143" x2="17.73681875" y2="1.22758125" layer="21"/>
<rectangle x1="17.99081875" y1="1.143" x2="18.33118125" y2="1.22758125" layer="21"/>
<rectangle x1="19.431" y1="1.143" x2="19.76881875" y2="1.22758125" layer="21"/>
<rectangle x1="20.61718125" y1="1.143" x2="20.955" y2="1.22758125" layer="21"/>
<rectangle x1="22.64918125" y1="1.143" x2="23.07081875" y2="1.22758125" layer="21"/>
<rectangle x1="23.495" y1="1.143" x2="23.749" y2="1.22758125" layer="21"/>
<rectangle x1="25.18918125" y1="1.143" x2="25.44318125" y2="1.22758125" layer="21"/>
<rectangle x1="25.781" y1="1.143" x2="26.11881875" y2="1.22758125" layer="21"/>
<rectangle x1="27.22118125" y1="1.143" x2="27.559" y2="1.22758125" layer="21"/>
<rectangle x1="3.59918125" y1="1.22758125" x2="3.937" y2="1.31241875" layer="21"/>
<rectangle x1="5.79881875" y1="1.22758125" x2="6.13918125" y2="1.31241875" layer="21"/>
<rectangle x1="6.64718125" y1="1.22758125" x2="6.90118125" y2="1.31241875" layer="21"/>
<rectangle x1="8.33881875" y1="1.22758125" x2="8.59281875" y2="1.31241875" layer="21"/>
<rectangle x1="8.84681875" y1="1.22758125" x2="9.18718125" y2="1.31241875" layer="21"/>
<rectangle x1="9.779" y1="1.22758125" x2="10.11681875" y2="1.31241875" layer="21"/>
<rectangle x1="11.303" y1="1.22758125" x2="11.557" y2="1.31241875" layer="21"/>
<rectangle x1="13.25118125" y1="1.22758125" x2="13.589" y2="1.31241875" layer="21"/>
<rectangle x1="14.18081875" y1="1.22758125" x2="14.52118125" y2="1.31241875" layer="21"/>
<rectangle x1="15.70481875" y1="1.22758125" x2="15.95881875" y2="1.31241875" layer="21"/>
<rectangle x1="17.399" y1="1.22758125" x2="17.653" y2="1.31241875" layer="21"/>
<rectangle x1="17.99081875" y1="1.22758125" x2="18.33118125" y2="1.31241875" layer="21"/>
<rectangle x1="19.51481875" y1="1.22758125" x2="19.76881875" y2="1.31241875" layer="21"/>
<rectangle x1="20.53081875" y1="1.22758125" x2="20.87118125" y2="1.31241875" layer="21"/>
<rectangle x1="22.733" y1="1.22758125" x2="23.07081875" y2="1.31241875" layer="21"/>
<rectangle x1="23.495" y1="1.22758125" x2="23.749" y2="1.31241875" layer="21"/>
<rectangle x1="25.18918125" y1="1.22758125" x2="25.44318125" y2="1.31241875" layer="21"/>
<rectangle x1="25.69718125" y1="1.22758125" x2="26.035" y2="1.31241875" layer="21"/>
<rectangle x1="27.22118125" y1="1.22758125" x2="27.559" y2="1.31241875" layer="21"/>
<rectangle x1="3.51281875" y1="1.31241875" x2="3.85318125" y2="1.397" layer="21"/>
<rectangle x1="5.88518125" y1="1.31241875" x2="6.223" y2="1.397" layer="21"/>
<rectangle x1="6.64718125" y1="1.31241875" x2="6.90118125" y2="1.397" layer="21"/>
<rectangle x1="8.33881875" y1="1.31241875" x2="8.59281875" y2="1.397" layer="21"/>
<rectangle x1="8.84681875" y1="1.31241875" x2="9.18718125" y2="1.397" layer="21"/>
<rectangle x1="9.779" y1="1.31241875" x2="10.033" y2="1.397" layer="21"/>
<rectangle x1="11.303" y1="1.31241875" x2="11.557" y2="1.397" layer="21"/>
<rectangle x1="13.335" y1="1.31241875" x2="13.67281875" y2="1.397" layer="21"/>
<rectangle x1="14.097" y1="1.31241875" x2="14.43481875" y2="1.397" layer="21"/>
<rectangle x1="15.70481875" y1="1.31241875" x2="16.04518125" y2="1.397" layer="21"/>
<rectangle x1="17.31518125" y1="1.31241875" x2="17.653" y2="1.397" layer="21"/>
<rectangle x1="17.907" y1="1.31241875" x2="18.24481875" y2="1.397" layer="21"/>
<rectangle x1="19.51481875" y1="1.31241875" x2="19.76881875" y2="1.397" layer="21"/>
<rectangle x1="20.447" y1="1.31241875" x2="20.78481875" y2="1.397" layer="21"/>
<rectangle x1="22.81681875" y1="1.31241875" x2="23.15718125" y2="1.397" layer="21"/>
<rectangle x1="23.495" y1="1.31241875" x2="23.749" y2="1.397" layer="21"/>
<rectangle x1="25.18918125" y1="1.31241875" x2="25.44318125" y2="1.397" layer="21"/>
<rectangle x1="25.69718125" y1="1.31241875" x2="26.035" y2="1.397" layer="21"/>
<rectangle x1="27.305" y1="1.31241875" x2="27.47518125" y2="1.397" layer="21"/>
<rectangle x1="3.51281875" y1="1.397" x2="3.85318125" y2="1.48158125" layer="21"/>
<rectangle x1="5.88518125" y1="1.397" x2="6.223" y2="1.48158125" layer="21"/>
<rectangle x1="6.64718125" y1="1.397" x2="6.90118125" y2="1.48158125" layer="21"/>
<rectangle x1="8.33881875" y1="1.397" x2="8.59281875" y2="1.48158125" layer="21"/>
<rectangle x1="8.84681875" y1="1.397" x2="9.18718125" y2="1.48158125" layer="21"/>
<rectangle x1="9.69518125" y1="1.397" x2="10.033" y2="1.48158125" layer="21"/>
<rectangle x1="11.303" y1="1.397" x2="11.557" y2="1.48158125" layer="21"/>
<rectangle x1="13.335" y1="1.397" x2="13.67281875" y2="1.48158125" layer="21"/>
<rectangle x1="14.097" y1="1.397" x2="14.43481875" y2="1.48158125" layer="21"/>
<rectangle x1="15.79118125" y1="1.397" x2="16.04518125" y2="1.48158125" layer="21"/>
<rectangle x1="17.22881875" y1="1.397" x2="17.653" y2="1.48158125" layer="21"/>
<rectangle x1="17.907" y1="1.397" x2="18.24481875" y2="1.48158125" layer="21"/>
<rectangle x1="20.447" y1="1.397" x2="20.78481875" y2="1.48158125" layer="21"/>
<rectangle x1="22.81681875" y1="1.397" x2="23.15718125" y2="1.48158125" layer="21"/>
<rectangle x1="23.495" y1="1.397" x2="23.749" y2="1.48158125" layer="21"/>
<rectangle x1="25.18918125" y1="1.397" x2="25.44318125" y2="1.48158125" layer="21"/>
<rectangle x1="25.69718125" y1="1.397" x2="25.95118125" y2="1.48158125" layer="21"/>
<rectangle x1="3.51281875" y1="1.48158125" x2="3.76681875" y2="1.56641875" layer="21"/>
<rectangle x1="5.969" y1="1.48158125" x2="6.30681875" y2="1.56641875" layer="21"/>
<rectangle x1="6.64718125" y1="1.48158125" x2="6.90118125" y2="1.56641875" layer="21"/>
<rectangle x1="8.33881875" y1="1.48158125" x2="8.59281875" y2="1.56641875" layer="21"/>
<rectangle x1="8.84681875" y1="1.48158125" x2="9.18718125" y2="1.56641875" layer="21"/>
<rectangle x1="9.69518125" y1="1.48158125" x2="10.033" y2="1.56641875" layer="21"/>
<rectangle x1="11.303" y1="1.48158125" x2="11.557" y2="1.56641875" layer="21"/>
<rectangle x1="13.41881875" y1="1.48158125" x2="13.75918125" y2="1.56641875" layer="21"/>
<rectangle x1="14.097" y1="1.48158125" x2="14.351" y2="1.56641875" layer="21"/>
<rectangle x1="15.79118125" y1="1.48158125" x2="16.04518125" y2="1.56641875" layer="21"/>
<rectangle x1="16.97481875" y1="1.48158125" x2="17.56918125" y2="1.56641875" layer="21"/>
<rectangle x1="17.907" y1="1.48158125" x2="19.76881875" y2="1.56641875" layer="21"/>
<rectangle x1="20.447" y1="1.48158125" x2="20.701" y2="1.56641875" layer="21"/>
<rectangle x1="22.90318125" y1="1.48158125" x2="23.241" y2="1.56641875" layer="21"/>
<rectangle x1="23.495" y1="1.48158125" x2="23.749" y2="1.56641875" layer="21"/>
<rectangle x1="25.18918125" y1="1.48158125" x2="25.44318125" y2="1.56641875" layer="21"/>
<rectangle x1="25.69718125" y1="1.48158125" x2="27.559" y2="1.56641875" layer="21"/>
<rectangle x1="3.429" y1="1.56641875" x2="3.76681875" y2="1.651" layer="21"/>
<rectangle x1="5.969" y1="1.56641875" x2="6.30681875" y2="1.651" layer="21"/>
<rectangle x1="6.64718125" y1="1.56641875" x2="6.90118125" y2="1.651" layer="21"/>
<rectangle x1="8.33881875" y1="1.56641875" x2="8.59281875" y2="1.651" layer="21"/>
<rectangle x1="8.84681875" y1="1.56641875" x2="9.18718125" y2="1.651" layer="21"/>
<rectangle x1="9.69518125" y1="1.56641875" x2="10.033" y2="1.651" layer="21"/>
<rectangle x1="11.303" y1="1.56641875" x2="11.557" y2="1.651" layer="21"/>
<rectangle x1="13.50518125" y1="1.56641875" x2="13.75918125" y2="1.651" layer="21"/>
<rectangle x1="14.097" y1="1.56641875" x2="14.351" y2="1.651" layer="21"/>
<rectangle x1="15.79118125" y1="1.56641875" x2="16.04518125" y2="1.651" layer="21"/>
<rectangle x1="16.72081875" y1="1.56641875" x2="17.48281875" y2="1.651" layer="21"/>
<rectangle x1="17.907" y1="1.56641875" x2="19.85518125" y2="1.651" layer="21"/>
<rectangle x1="20.36318125" y1="1.56641875" x2="20.701" y2="1.651" layer="21"/>
<rectangle x1="22.90318125" y1="1.56641875" x2="23.241" y2="1.651" layer="21"/>
<rectangle x1="23.495" y1="1.56641875" x2="23.749" y2="1.651" layer="21"/>
<rectangle x1="25.18918125" y1="1.56641875" x2="25.44318125" y2="1.651" layer="21"/>
<rectangle x1="25.61081875" y1="1.56641875" x2="27.64281875" y2="1.651" layer="21"/>
<rectangle x1="3.429" y1="1.651" x2="3.76681875" y2="1.73558125" layer="21"/>
<rectangle x1="5.969" y1="1.651" x2="6.30681875" y2="1.73558125" layer="21"/>
<rectangle x1="6.64718125" y1="1.651" x2="6.90118125" y2="1.73558125" layer="21"/>
<rectangle x1="8.33881875" y1="1.651" x2="8.59281875" y2="1.73558125" layer="21"/>
<rectangle x1="8.84681875" y1="1.651" x2="9.18718125" y2="1.73558125" layer="21"/>
<rectangle x1="9.69518125" y1="1.651" x2="10.033" y2="1.73558125" layer="21"/>
<rectangle x1="11.303" y1="1.651" x2="11.557" y2="1.73558125" layer="21"/>
<rectangle x1="13.50518125" y1="1.651" x2="13.843" y2="1.73558125" layer="21"/>
<rectangle x1="14.097" y1="1.651" x2="14.351" y2="1.73558125" layer="21"/>
<rectangle x1="15.79118125" y1="1.651" x2="16.04518125" y2="1.73558125" layer="21"/>
<rectangle x1="16.55318125" y1="1.651" x2="17.31518125" y2="1.73558125" layer="21"/>
<rectangle x1="17.907" y1="1.651" x2="19.939" y2="1.73558125" layer="21"/>
<rectangle x1="20.36318125" y1="1.651" x2="20.701" y2="1.73558125" layer="21"/>
<rectangle x1="22.90318125" y1="1.651" x2="23.241" y2="1.73558125" layer="21"/>
<rectangle x1="23.495" y1="1.651" x2="23.749" y2="1.73558125" layer="21"/>
<rectangle x1="25.18918125" y1="1.651" x2="25.44318125" y2="1.73558125" layer="21"/>
<rectangle x1="25.61081875" y1="1.651" x2="27.64281875" y2="1.73558125" layer="21"/>
<rectangle x1="3.429" y1="1.73558125" x2="3.683" y2="1.82041875" layer="21"/>
<rectangle x1="6.05281875" y1="1.73558125" x2="6.30681875" y2="1.82041875" layer="21"/>
<rectangle x1="6.64718125" y1="1.73558125" x2="6.90118125" y2="1.82041875" layer="21"/>
<rectangle x1="8.33881875" y1="1.73558125" x2="8.59281875" y2="1.82041875" layer="21"/>
<rectangle x1="8.84681875" y1="1.73558125" x2="9.18718125" y2="1.82041875" layer="21"/>
<rectangle x1="9.69518125" y1="1.73558125" x2="10.033" y2="1.82041875" layer="21"/>
<rectangle x1="11.303" y1="1.73558125" x2="11.557" y2="1.82041875" layer="21"/>
<rectangle x1="13.50518125" y1="1.73558125" x2="13.843" y2="1.82041875" layer="21"/>
<rectangle x1="14.097" y1="1.73558125" x2="14.351" y2="1.82041875" layer="21"/>
<rectangle x1="15.79118125" y1="1.73558125" x2="16.04518125" y2="1.82041875" layer="21"/>
<rectangle x1="16.383" y1="1.73558125" x2="17.145" y2="1.82041875" layer="21"/>
<rectangle x1="17.907" y1="1.73558125" x2="19.85518125" y2="1.82041875" layer="21"/>
<rectangle x1="20.36318125" y1="1.73558125" x2="20.61718125" y2="1.82041875" layer="21"/>
<rectangle x1="22.987" y1="1.73558125" x2="23.241" y2="1.82041875" layer="21"/>
<rectangle x1="23.495" y1="1.73558125" x2="23.749" y2="1.82041875" layer="21"/>
<rectangle x1="25.18918125" y1="1.73558125" x2="25.44318125" y2="1.82041875" layer="21"/>
<rectangle x1="25.69718125" y1="1.73558125" x2="27.64281875" y2="1.82041875" layer="21"/>
<rectangle x1="3.429" y1="1.82041875" x2="3.683" y2="1.905" layer="21"/>
<rectangle x1="6.05281875" y1="1.82041875" x2="6.30681875" y2="1.905" layer="21"/>
<rectangle x1="6.64718125" y1="1.82041875" x2="6.90118125" y2="1.905" layer="21"/>
<rectangle x1="8.33881875" y1="1.82041875" x2="8.59281875" y2="1.905" layer="21"/>
<rectangle x1="8.84681875" y1="1.82041875" x2="9.18718125" y2="1.905" layer="21"/>
<rectangle x1="9.69518125" y1="1.82041875" x2="10.033" y2="1.905" layer="21"/>
<rectangle x1="11.303" y1="1.82041875" x2="11.557" y2="1.905" layer="21"/>
<rectangle x1="13.589" y1="1.82041875" x2="13.843" y2="1.905" layer="21"/>
<rectangle x1="14.097" y1="1.82041875" x2="14.43481875" y2="1.905" layer="21"/>
<rectangle x1="15.79118125" y1="1.82041875" x2="16.04518125" y2="1.905" layer="21"/>
<rectangle x1="16.383" y1="1.82041875" x2="16.891" y2="1.905" layer="21"/>
<rectangle x1="17.907" y1="1.82041875" x2="18.24481875" y2="1.905" layer="21"/>
<rectangle x1="19.60118125" y1="1.82041875" x2="19.85518125" y2="1.905" layer="21"/>
<rectangle x1="20.36318125" y1="1.82041875" x2="20.61718125" y2="1.905" layer="21"/>
<rectangle x1="22.987" y1="1.82041875" x2="23.241" y2="1.905" layer="21"/>
<rectangle x1="23.495" y1="1.82041875" x2="23.749" y2="1.905" layer="21"/>
<rectangle x1="25.10281875" y1="1.82041875" x2="25.44318125" y2="1.905" layer="21"/>
<rectangle x1="25.69718125" y1="1.82041875" x2="25.95118125" y2="1.905" layer="21"/>
<rectangle x1="27.305" y1="1.82041875" x2="27.64281875" y2="1.905" layer="21"/>
<rectangle x1="3.429" y1="1.905" x2="3.683" y2="1.98958125" layer="21"/>
<rectangle x1="6.05281875" y1="1.905" x2="6.30681875" y2="1.98958125" layer="21"/>
<rectangle x1="6.64718125" y1="1.905" x2="6.985" y2="1.98958125" layer="21"/>
<rectangle x1="8.255" y1="1.905" x2="8.59281875" y2="1.98958125" layer="21"/>
<rectangle x1="8.84681875" y1="1.905" x2="9.18718125" y2="1.98958125" layer="21"/>
<rectangle x1="9.69518125" y1="1.905" x2="10.033" y2="1.98958125" layer="21"/>
<rectangle x1="11.303" y1="1.905" x2="11.557" y2="1.98958125" layer="21"/>
<rectangle x1="13.589" y1="1.905" x2="13.843" y2="1.98958125" layer="21"/>
<rectangle x1="14.097" y1="1.905" x2="14.43481875" y2="1.98958125" layer="21"/>
<rectangle x1="15.70481875" y1="1.905" x2="16.04518125" y2="1.98958125" layer="21"/>
<rectangle x1="16.29918125" y1="1.905" x2="16.72081875" y2="1.98958125" layer="21"/>
<rectangle x1="17.907" y1="1.905" x2="18.24481875" y2="1.98958125" layer="21"/>
<rectangle x1="19.51481875" y1="1.905" x2="19.85518125" y2="1.98958125" layer="21"/>
<rectangle x1="20.36318125" y1="1.905" x2="20.61718125" y2="1.98958125" layer="21"/>
<rectangle x1="22.987" y1="1.905" x2="23.241" y2="1.98958125" layer="21"/>
<rectangle x1="23.495" y1="1.905" x2="23.83281875" y2="1.98958125" layer="21"/>
<rectangle x1="25.10281875" y1="1.905" x2="25.44318125" y2="1.98958125" layer="21"/>
<rectangle x1="25.69718125" y1="1.905" x2="26.035" y2="1.98958125" layer="21"/>
<rectangle x1="27.305" y1="1.905" x2="27.64281875" y2="1.98958125" layer="21"/>
<rectangle x1="3.429" y1="1.98958125" x2="3.683" y2="2.07441875" layer="21"/>
<rectangle x1="6.05281875" y1="1.98958125" x2="6.30681875" y2="2.07441875" layer="21"/>
<rectangle x1="6.64718125" y1="1.98958125" x2="6.985" y2="2.07441875" layer="21"/>
<rectangle x1="8.255" y1="1.98958125" x2="8.509" y2="2.07441875" layer="21"/>
<rectangle x1="8.84681875" y1="1.98958125" x2="9.18718125" y2="2.07441875" layer="21"/>
<rectangle x1="9.69518125" y1="1.98958125" x2="10.033" y2="2.07441875" layer="21"/>
<rectangle x1="11.303" y1="1.98958125" x2="11.557" y2="2.07441875" layer="21"/>
<rectangle x1="13.589" y1="1.98958125" x2="13.843" y2="2.07441875" layer="21"/>
<rectangle x1="14.18081875" y1="1.98958125" x2="14.43481875" y2="2.07441875" layer="21"/>
<rectangle x1="15.70481875" y1="1.98958125" x2="16.04518125" y2="2.07441875" layer="21"/>
<rectangle x1="16.29918125" y1="1.98958125" x2="16.55318125" y2="2.07441875" layer="21"/>
<rectangle x1="17.99081875" y1="1.98958125" x2="18.24481875" y2="2.07441875" layer="21"/>
<rectangle x1="19.51481875" y1="1.98958125" x2="19.85518125" y2="2.07441875" layer="21"/>
<rectangle x1="20.36318125" y1="1.98958125" x2="20.61718125" y2="2.07441875" layer="21"/>
<rectangle x1="22.987" y1="1.98958125" x2="23.241" y2="2.07441875" layer="21"/>
<rectangle x1="23.495" y1="1.98958125" x2="23.83281875" y2="2.07441875" layer="21"/>
<rectangle x1="25.019" y1="1.98958125" x2="25.35681875" y2="2.07441875" layer="21"/>
<rectangle x1="25.69718125" y1="1.98958125" x2="26.035" y2="2.07441875" layer="21"/>
<rectangle x1="27.22118125" y1="1.98958125" x2="27.559" y2="2.07441875" layer="21"/>
<rectangle x1="3.429" y1="2.07441875" x2="3.683" y2="2.159" layer="21"/>
<rectangle x1="6.05281875" y1="2.07441875" x2="6.30681875" y2="2.159" layer="21"/>
<rectangle x1="6.64718125" y1="2.07441875" x2="7.06881875" y2="2.159" layer="21"/>
<rectangle x1="8.17118125" y1="2.07441875" x2="8.509" y2="2.159" layer="21"/>
<rectangle x1="8.84681875" y1="2.07441875" x2="9.18718125" y2="2.159" layer="21"/>
<rectangle x1="9.69518125" y1="2.07441875" x2="10.033" y2="2.159" layer="21"/>
<rectangle x1="11.303" y1="2.07441875" x2="11.557" y2="2.159" layer="21"/>
<rectangle x1="13.589" y1="2.07441875" x2="13.92681875" y2="2.159" layer="21"/>
<rectangle x1="14.18081875" y1="2.07441875" x2="14.52118125" y2="2.159" layer="21"/>
<rectangle x1="15.621" y1="2.07441875" x2="15.95881875" y2="2.159" layer="21"/>
<rectangle x1="16.29918125" y1="2.07441875" x2="16.55318125" y2="2.159" layer="21"/>
<rectangle x1="17.399" y1="2.07441875" x2="17.653" y2="2.159" layer="21"/>
<rectangle x1="17.99081875" y1="2.07441875" x2="18.33118125" y2="2.159" layer="21"/>
<rectangle x1="19.431" y1="2.07441875" x2="19.76881875" y2="2.159" layer="21"/>
<rectangle x1="20.36318125" y1="2.07441875" x2="20.61718125" y2="2.159" layer="21"/>
<rectangle x1="22.987" y1="2.07441875" x2="23.241" y2="2.159" layer="21"/>
<rectangle x1="23.495" y1="2.07441875" x2="23.91918125" y2="2.159" layer="21"/>
<rectangle x1="25.019" y1="2.07441875" x2="25.35681875" y2="2.159" layer="21"/>
<rectangle x1="25.781" y1="2.07441875" x2="26.11881875" y2="2.159" layer="21"/>
<rectangle x1="27.22118125" y1="2.07441875" x2="27.559" y2="2.159" layer="21"/>
<rectangle x1="3.429" y1="2.159" x2="3.683" y2="2.24358125" layer="21"/>
<rectangle x1="6.05281875" y1="2.159" x2="6.30681875" y2="2.24358125" layer="21"/>
<rectangle x1="6.64718125" y1="2.159" x2="7.15518125" y2="2.24358125" layer="21"/>
<rectangle x1="8.08481875" y1="2.159" x2="8.42518125" y2="2.24358125" layer="21"/>
<rectangle x1="8.84681875" y1="2.159" x2="9.18718125" y2="2.24358125" layer="21"/>
<rectangle x1="9.69518125" y1="2.159" x2="10.033" y2="2.24358125" layer="21"/>
<rectangle x1="11.303" y1="2.159" x2="11.557" y2="2.24358125" layer="21"/>
<rectangle x1="13.589" y1="2.159" x2="13.92681875" y2="2.24358125" layer="21"/>
<rectangle x1="14.26718125" y1="2.159" x2="14.605" y2="2.24358125" layer="21"/>
<rectangle x1="15.53718125" y1="2.159" x2="15.95881875" y2="2.24358125" layer="21"/>
<rectangle x1="16.29918125" y1="2.159" x2="16.637" y2="2.24358125" layer="21"/>
<rectangle x1="17.31518125" y1="2.159" x2="17.653" y2="2.24358125" layer="21"/>
<rectangle x1="18.07718125" y1="2.159" x2="18.415" y2="2.24358125" layer="21"/>
<rectangle x1="19.34718125" y1="2.159" x2="19.76881875" y2="2.24358125" layer="21"/>
<rectangle x1="20.36318125" y1="2.159" x2="20.61718125" y2="2.24358125" layer="21"/>
<rectangle x1="22.987" y1="2.159" x2="23.241" y2="2.24358125" layer="21"/>
<rectangle x1="23.495" y1="2.159" x2="24.003" y2="2.24358125" layer="21"/>
<rectangle x1="24.93518125" y1="2.159" x2="25.273" y2="2.24358125" layer="21"/>
<rectangle x1="25.781" y1="2.159" x2="26.20518125" y2="2.24358125" layer="21"/>
<rectangle x1="27.13481875" y1="2.159" x2="27.47518125" y2="2.24358125" layer="21"/>
<rectangle x1="3.429" y1="2.24358125" x2="3.683" y2="2.32841875" layer="21"/>
<rectangle x1="6.05281875" y1="2.24358125" x2="6.30681875" y2="2.32841875" layer="21"/>
<rectangle x1="6.64718125" y1="2.24358125" x2="7.32281875" y2="2.32841875" layer="21"/>
<rectangle x1="7.91718125" y1="2.24358125" x2="8.42518125" y2="2.32841875" layer="21"/>
<rectangle x1="8.84681875" y1="2.24358125" x2="9.18718125" y2="2.32841875" layer="21"/>
<rectangle x1="9.69518125" y1="2.24358125" x2="10.033" y2="2.32841875" layer="21"/>
<rectangle x1="11.303" y1="2.24358125" x2="11.557" y2="2.32841875" layer="21"/>
<rectangle x1="13.589" y1="2.24358125" x2="13.843" y2="2.32841875" layer="21"/>
<rectangle x1="14.26718125" y1="2.24358125" x2="14.77518125" y2="2.32841875" layer="21"/>
<rectangle x1="15.45081875" y1="2.24358125" x2="15.875" y2="2.32841875" layer="21"/>
<rectangle x1="16.29918125" y1="2.24358125" x2="16.72081875" y2="2.32841875" layer="21"/>
<rectangle x1="17.22881875" y1="2.24358125" x2="17.653" y2="2.32841875" layer="21"/>
<rectangle x1="18.07718125" y1="2.24358125" x2="18.58518125" y2="2.32841875" layer="21"/>
<rectangle x1="19.177" y1="2.24358125" x2="19.685" y2="2.32841875" layer="21"/>
<rectangle x1="20.36318125" y1="2.24358125" x2="20.701" y2="2.32841875" layer="21"/>
<rectangle x1="22.90318125" y1="2.24358125" x2="23.241" y2="2.32841875" layer="21"/>
<rectangle x1="23.495" y1="2.24358125" x2="24.17318125" y2="2.32841875" layer="21"/>
<rectangle x1="24.765" y1="2.24358125" x2="25.273" y2="2.32841875" layer="21"/>
<rectangle x1="25.86481875" y1="2.24358125" x2="26.289" y2="2.32841875" layer="21"/>
<rectangle x1="26.96718125" y1="2.24358125" x2="27.47518125" y2="2.32841875" layer="21"/>
<rectangle x1="3.429" y1="2.32841875" x2="3.683" y2="2.413" layer="21"/>
<rectangle x1="6.05281875" y1="2.32841875" x2="6.30681875" y2="2.413" layer="21"/>
<rectangle x1="6.64718125" y1="2.32841875" x2="7.493" y2="2.413" layer="21"/>
<rectangle x1="7.66318125" y1="2.32841875" x2="8.33881875" y2="2.413" layer="21"/>
<rectangle x1="8.84681875" y1="2.32841875" x2="9.18718125" y2="2.413" layer="21"/>
<rectangle x1="9.69518125" y1="2.32841875" x2="10.033" y2="2.413" layer="21"/>
<rectangle x1="11.303" y1="2.32841875" x2="11.557" y2="2.413" layer="21"/>
<rectangle x1="13.589" y1="2.32841875" x2="13.843" y2="2.413" layer="21"/>
<rectangle x1="14.351" y1="2.32841875" x2="15.02918125" y2="2.413" layer="21"/>
<rectangle x1="15.19681875" y1="2.32841875" x2="15.79118125" y2="2.413" layer="21"/>
<rectangle x1="16.383" y1="2.32841875" x2="16.891" y2="2.413" layer="21"/>
<rectangle x1="17.06118125" y1="2.32841875" x2="17.56918125" y2="2.413" layer="21"/>
<rectangle x1="18.161" y1="2.32841875" x2="18.83918125" y2="2.413" layer="21"/>
<rectangle x1="19.00681875" y1="2.32841875" x2="19.60118125" y2="2.413" layer="21"/>
<rectangle x1="20.36318125" y1="2.32841875" x2="20.701" y2="2.413" layer="21"/>
<rectangle x1="22.90318125" y1="2.32841875" x2="23.241" y2="2.413" layer="21"/>
<rectangle x1="23.495" y1="2.32841875" x2="24.34081875" y2="2.413" layer="21"/>
<rectangle x1="24.511" y1="2.32841875" x2="25.18918125" y2="2.413" layer="21"/>
<rectangle x1="25.95118125" y1="2.32841875" x2="26.543" y2="2.413" layer="21"/>
<rectangle x1="26.71318125" y1="2.32841875" x2="27.38881875" y2="2.413" layer="21"/>
<rectangle x1="3.429" y1="2.413" x2="3.683" y2="2.49758125" layer="21"/>
<rectangle x1="6.05281875" y1="2.413" x2="6.30681875" y2="2.49758125" layer="21"/>
<rectangle x1="6.64718125" y1="2.413" x2="6.90118125" y2="2.49758125" layer="21"/>
<rectangle x1="6.985" y1="2.413" x2="8.255" y2="2.49758125" layer="21"/>
<rectangle x1="8.84681875" y1="2.413" x2="9.18718125" y2="2.49758125" layer="21"/>
<rectangle x1="9.35481875" y1="2.413" x2="10.795" y2="2.49758125" layer="21"/>
<rectangle x1="11.303" y1="2.413" x2="11.557" y2="2.49758125" layer="21"/>
<rectangle x1="13.50518125" y1="2.413" x2="13.843" y2="2.49758125" layer="21"/>
<rectangle x1="14.43481875" y1="2.413" x2="15.70481875" y2="2.49758125" layer="21"/>
<rectangle x1="16.46681875" y1="2.413" x2="17.48281875" y2="2.49758125" layer="21"/>
<rectangle x1="18.24481875" y1="2.413" x2="19.51481875" y2="2.49758125" layer="21"/>
<rectangle x1="20.447" y1="2.413" x2="20.701" y2="2.49758125" layer="21"/>
<rectangle x1="22.90318125" y1="2.413" x2="23.241" y2="2.49758125" layer="21"/>
<rectangle x1="23.495" y1="2.413" x2="23.749" y2="2.49758125" layer="21"/>
<rectangle x1="23.83281875" y1="2.413" x2="25.10281875" y2="2.49758125" layer="21"/>
<rectangle x1="26.035" y1="2.413" x2="27.305" y2="2.49758125" layer="21"/>
<rectangle x1="3.429" y1="2.49758125" x2="3.683" y2="2.58241875" layer="21"/>
<rectangle x1="6.05281875" y1="2.49758125" x2="6.30681875" y2="2.58241875" layer="21"/>
<rectangle x1="6.64718125" y1="2.49758125" x2="6.90118125" y2="2.58241875" layer="21"/>
<rectangle x1="7.15518125" y1="2.49758125" x2="8.08481875" y2="2.58241875" layer="21"/>
<rectangle x1="8.84681875" y1="2.49758125" x2="9.18718125" y2="2.58241875" layer="21"/>
<rectangle x1="9.35481875" y1="2.49758125" x2="10.87881875" y2="2.58241875" layer="21"/>
<rectangle x1="11.303" y1="2.49758125" x2="11.557" y2="2.58241875" layer="21"/>
<rectangle x1="13.50518125" y1="2.49758125" x2="13.843" y2="2.58241875" layer="21"/>
<rectangle x1="14.605" y1="2.49758125" x2="15.53718125" y2="2.58241875" layer="21"/>
<rectangle x1="16.55318125" y1="2.49758125" x2="17.399" y2="2.58241875" layer="21"/>
<rectangle x1="18.415" y1="2.49758125" x2="19.34718125" y2="2.58241875" layer="21"/>
<rectangle x1="20.447" y1="2.49758125" x2="20.78481875" y2="2.58241875" layer="21"/>
<rectangle x1="22.81681875" y1="2.49758125" x2="23.15718125" y2="2.58241875" layer="21"/>
<rectangle x1="23.495" y1="2.49758125" x2="23.749" y2="2.58241875" layer="21"/>
<rectangle x1="24.003" y1="2.49758125" x2="24.93518125" y2="2.58241875" layer="21"/>
<rectangle x1="26.20518125" y1="2.49758125" x2="27.13481875" y2="2.58241875" layer="21"/>
<rectangle x1="3.429" y1="2.58241875" x2="3.683" y2="2.667" layer="21"/>
<rectangle x1="6.05281875" y1="2.58241875" x2="6.30681875" y2="2.667" layer="21"/>
<rectangle x1="6.64718125" y1="2.58241875" x2="6.81481875" y2="2.667" layer="21"/>
<rectangle x1="7.32281875" y1="2.58241875" x2="7.91718125" y2="2.667" layer="21"/>
<rectangle x1="8.93318125" y1="2.58241875" x2="9.10081875" y2="2.667" layer="21"/>
<rectangle x1="9.44118125" y1="2.58241875" x2="10.795" y2="2.667" layer="21"/>
<rectangle x1="11.303" y1="2.58241875" x2="11.557" y2="2.667" layer="21"/>
<rectangle x1="13.50518125" y1="2.58241875" x2="13.843" y2="2.667" layer="21"/>
<rectangle x1="14.77518125" y1="2.58241875" x2="15.367" y2="2.667" layer="21"/>
<rectangle x1="16.72081875" y1="2.58241875" x2="17.22881875" y2="2.667" layer="21"/>
<rectangle x1="18.58518125" y1="2.58241875" x2="19.177" y2="2.667" layer="21"/>
<rectangle x1="20.447" y1="2.58241875" x2="20.78481875" y2="2.667" layer="21"/>
<rectangle x1="22.81681875" y1="2.58241875" x2="23.15718125" y2="2.667" layer="21"/>
<rectangle x1="23.495" y1="2.58241875" x2="23.66518125" y2="2.667" layer="21"/>
<rectangle x1="24.17318125" y1="2.58241875" x2="24.765" y2="2.667" layer="21"/>
<rectangle x1="26.37281875" y1="2.58241875" x2="26.96718125" y2="2.667" layer="21"/>
<rectangle x1="3.429" y1="2.667" x2="3.683" y2="2.75158125" layer="21"/>
<rectangle x1="6.05281875" y1="2.667" x2="6.30681875" y2="2.75158125" layer="21"/>
<rectangle x1="9.69518125" y1="2.667" x2="10.033" y2="2.75158125" layer="21"/>
<rectangle x1="11.303" y1="2.667" x2="11.557" y2="2.75158125" layer="21"/>
<rectangle x1="13.41881875" y1="2.667" x2="13.75918125" y2="2.75158125" layer="21"/>
<rectangle x1="20.53081875" y1="2.667" x2="20.87118125" y2="2.75158125" layer="21"/>
<rectangle x1="22.733" y1="2.667" x2="23.07081875" y2="2.75158125" layer="21"/>
<rectangle x1="3.429" y1="2.75158125" x2="3.683" y2="2.83641875" layer="21"/>
<rectangle x1="6.05281875" y1="2.75158125" x2="6.30681875" y2="2.83641875" layer="21"/>
<rectangle x1="8.93318125" y1="2.75158125" x2="9.10081875" y2="2.83641875" layer="21"/>
<rectangle x1="9.69518125" y1="2.75158125" x2="10.033" y2="2.83641875" layer="21"/>
<rectangle x1="11.303" y1="2.75158125" x2="11.557" y2="2.83641875" layer="21"/>
<rectangle x1="13.41881875" y1="2.75158125" x2="13.75918125" y2="2.83641875" layer="21"/>
<rectangle x1="20.61718125" y1="2.75158125" x2="20.955" y2="2.83641875" layer="21"/>
<rectangle x1="22.64918125" y1="2.75158125" x2="23.07081875" y2="2.83641875" layer="21"/>
<rectangle x1="3.429" y1="2.83641875" x2="3.683" y2="2.921" layer="21"/>
<rectangle x1="6.05281875" y1="2.83641875" x2="6.30681875" y2="2.921" layer="21"/>
<rectangle x1="8.84681875" y1="2.83641875" x2="9.18718125" y2="2.921" layer="21"/>
<rectangle x1="9.69518125" y1="2.83641875" x2="10.033" y2="2.921" layer="21"/>
<rectangle x1="11.303" y1="2.83641875" x2="11.557" y2="2.921" layer="21"/>
<rectangle x1="13.335" y1="2.83641875" x2="13.67281875" y2="2.921" layer="21"/>
<rectangle x1="20.61718125" y1="2.83641875" x2="21.03881875" y2="2.921" layer="21"/>
<rectangle x1="22.56281875" y1="2.83641875" x2="22.987" y2="2.921" layer="21"/>
<rectangle x1="3.429" y1="2.921" x2="3.683" y2="3.00558125" layer="21"/>
<rectangle x1="6.05281875" y1="2.921" x2="6.30681875" y2="3.00558125" layer="21"/>
<rectangle x1="8.84681875" y1="2.921" x2="9.18718125" y2="3.00558125" layer="21"/>
<rectangle x1="9.69518125" y1="2.921" x2="10.033" y2="3.00558125" layer="21"/>
<rectangle x1="11.303" y1="2.921" x2="11.557" y2="3.00558125" layer="21"/>
<rectangle x1="13.25118125" y1="2.921" x2="13.589" y2="3.00558125" layer="21"/>
<rectangle x1="20.701" y1="2.921" x2="21.12518125" y2="3.00558125" layer="21"/>
<rectangle x1="22.479" y1="2.921" x2="22.90318125" y2="3.00558125" layer="21"/>
<rectangle x1="3.429" y1="3.00558125" x2="3.683" y2="3.09041875" layer="21"/>
<rectangle x1="6.05281875" y1="3.00558125" x2="6.30681875" y2="3.09041875" layer="21"/>
<rectangle x1="8.93318125" y1="3.00558125" x2="9.18718125" y2="3.09041875" layer="21"/>
<rectangle x1="9.69518125" y1="3.00558125" x2="10.033" y2="3.09041875" layer="21"/>
<rectangle x1="11.303" y1="3.00558125" x2="11.557" y2="3.09041875" layer="21"/>
<rectangle x1="13.16481875" y1="3.00558125" x2="13.589" y2="3.09041875" layer="21"/>
<rectangle x1="20.78481875" y1="3.00558125" x2="21.29281875" y2="3.09041875" layer="21"/>
<rectangle x1="22.30881875" y1="3.00558125" x2="22.81681875" y2="3.09041875" layer="21"/>
<rectangle x1="3.429" y1="3.09041875" x2="3.683" y2="3.175" layer="21"/>
<rectangle x1="6.05281875" y1="3.09041875" x2="6.39318125" y2="3.175" layer="21"/>
<rectangle x1="9.69518125" y1="3.09041875" x2="10.033" y2="3.175" layer="21"/>
<rectangle x1="11.303" y1="3.09041875" x2="11.557" y2="3.175" layer="21"/>
<rectangle x1="12.99718125" y1="3.09041875" x2="13.50518125" y2="3.175" layer="21"/>
<rectangle x1="20.87118125" y1="3.09041875" x2="21.54681875" y2="3.175" layer="21"/>
<rectangle x1="22.05481875" y1="3.09041875" x2="22.733" y2="3.175" layer="21"/>
<rectangle x1="3.429" y1="3.175" x2="3.683" y2="3.25958125" layer="21"/>
<rectangle x1="6.05281875" y1="3.175" x2="6.30681875" y2="3.25958125" layer="21"/>
<rectangle x1="9.69518125" y1="3.175" x2="10.033" y2="3.25958125" layer="21"/>
<rectangle x1="11.303" y1="3.175" x2="11.557" y2="3.25958125" layer="21"/>
<rectangle x1="12.827" y1="3.175" x2="13.41881875" y2="3.25958125" layer="21"/>
<rectangle x1="20.955" y1="3.175" x2="22.64918125" y2="3.25958125" layer="21"/>
<rectangle x1="3.429" y1="3.25958125" x2="3.683" y2="3.34441875" layer="21"/>
<rectangle x1="6.05281875" y1="3.25958125" x2="6.30681875" y2="3.34441875" layer="21"/>
<rectangle x1="9.69518125" y1="3.25958125" x2="10.033" y2="3.34441875" layer="21"/>
<rectangle x1="11.303" y1="3.25958125" x2="13.335" y2="3.34441875" layer="21"/>
<rectangle x1="21.12518125" y1="3.25958125" x2="22.479" y2="3.34441875" layer="21"/>
<rectangle x1="3.429" y1="3.34441875" x2="3.683" y2="3.429" layer="21"/>
<rectangle x1="6.05281875" y1="3.34441875" x2="6.30681875" y2="3.429" layer="21"/>
<rectangle x1="9.69518125" y1="3.34441875" x2="9.94918125" y2="3.429" layer="21"/>
<rectangle x1="11.303" y1="3.34441875" x2="13.16481875" y2="3.429" layer="21"/>
<rectangle x1="21.29281875" y1="3.34441875" x2="22.225" y2="3.429" layer="21"/>
<rectangle x1="11.303" y1="3.429" x2="12.99718125" y2="3.51358125" layer="21"/>
<rectangle x1="11.303" y1="3.51358125" x2="12.827" y2="3.59841875" layer="21"/>
<rectangle x1="5.63118125" y1="4.86841875" x2="24.59481875" y2="4.953" layer="21"/>
<rectangle x1="5.03681875" y1="4.953" x2="25.18918125" y2="5.03758125" layer="21"/>
<rectangle x1="4.699" y1="5.03758125" x2="25.527" y2="5.12241875" layer="21"/>
<rectangle x1="4.36118125" y1="5.12241875" x2="25.86481875" y2="5.207" layer="21"/>
<rectangle x1="4.10718125" y1="5.207" x2="26.11881875" y2="5.29158125" layer="21"/>
<rectangle x1="3.85318125" y1="5.29158125" x2="26.289" y2="5.37641875" layer="21"/>
<rectangle x1="3.683" y1="5.37641875" x2="26.543" y2="5.461" layer="21"/>
<rectangle x1="3.51281875" y1="5.461" x2="26.71318125" y2="5.54558125" layer="21"/>
<rectangle x1="3.34518125" y1="5.54558125" x2="26.88081875" y2="5.63041875" layer="21"/>
<rectangle x1="3.175" y1="5.63041875" x2="27.051" y2="5.715" layer="21"/>
<rectangle x1="3.00481875" y1="5.715" x2="27.13481875" y2="5.79958125" layer="21"/>
<rectangle x1="2.921" y1="5.79958125" x2="5.29081875" y2="5.88441875" layer="21"/>
<rectangle x1="14.77518125" y1="5.79958125" x2="27.305" y2="5.88441875" layer="21"/>
<rectangle x1="2.75081875" y1="5.88441875" x2="4.86918125" y2="5.969" layer="21"/>
<rectangle x1="14.68881875" y1="5.88441875" x2="27.38881875" y2="5.969" layer="21"/>
<rectangle x1="2.667" y1="5.969" x2="4.52881875" y2="6.05358125" layer="21"/>
<rectangle x1="14.605" y1="5.969" x2="27.559" y2="6.05358125" layer="21"/>
<rectangle x1="2.49681875" y1="6.05358125" x2="4.36118125" y2="6.13841875" layer="21"/>
<rectangle x1="14.605" y1="6.05358125" x2="27.64281875" y2="6.13841875" layer="21"/>
<rectangle x1="2.413" y1="6.13841875" x2="4.10718125" y2="6.223" layer="21"/>
<rectangle x1="14.52118125" y1="6.13841875" x2="27.72918125" y2="6.223" layer="21"/>
<rectangle x1="2.32918125" y1="6.223" x2="3.937" y2="6.30758125" layer="21"/>
<rectangle x1="14.52118125" y1="6.223" x2="27.89681875" y2="6.30758125" layer="21"/>
<rectangle x1="2.24281875" y1="6.30758125" x2="3.76681875" y2="6.39241875" layer="21"/>
<rectangle x1="14.43481875" y1="6.30758125" x2="27.98318125" y2="6.39241875" layer="21"/>
<rectangle x1="2.159" y1="6.39241875" x2="3.59918125" y2="6.477" layer="21"/>
<rectangle x1="14.43481875" y1="6.39241875" x2="28.067" y2="6.477" layer="21"/>
<rectangle x1="2.07518125" y1="6.477" x2="3.429" y2="6.56158125" layer="21"/>
<rectangle x1="14.351" y1="6.477" x2="28.15081875" y2="6.56158125" layer="21"/>
<rectangle x1="1.905" y1="6.56158125" x2="3.34518125" y2="6.64641875" layer="21"/>
<rectangle x1="14.351" y1="6.56158125" x2="28.23718125" y2="6.64641875" layer="21"/>
<rectangle x1="1.905" y1="6.64641875" x2="3.175" y2="6.731" layer="21"/>
<rectangle x1="14.26718125" y1="6.64641875" x2="28.321" y2="6.731" layer="21"/>
<rectangle x1="1.73481875" y1="6.731" x2="3.09118125" y2="6.81558125" layer="21"/>
<rectangle x1="14.26718125" y1="6.731" x2="28.40481875" y2="6.81558125" layer="21"/>
<rectangle x1="1.73481875" y1="6.81558125" x2="3.00481875" y2="6.90041875" layer="21"/>
<rectangle x1="14.18081875" y1="6.81558125" x2="28.49118125" y2="6.90041875" layer="21"/>
<rectangle x1="1.651" y1="6.90041875" x2="2.83718125" y2="6.985" layer="21"/>
<rectangle x1="14.18081875" y1="6.90041875" x2="28.575" y2="6.985" layer="21"/>
<rectangle x1="1.56718125" y1="6.985" x2="2.75081875" y2="7.06958125" layer="21"/>
<rectangle x1="14.18081875" y1="6.985" x2="28.65881875" y2="7.06958125" layer="21"/>
<rectangle x1="1.48081875" y1="7.06958125" x2="2.667" y2="7.15441875" layer="21"/>
<rectangle x1="14.097" y1="7.06958125" x2="28.74518125" y2="7.15441875" layer="21"/>
<rectangle x1="1.397" y1="7.15441875" x2="2.58318125" y2="7.239" layer="21"/>
<rectangle x1="14.097" y1="7.15441875" x2="28.74518125" y2="7.239" layer="21"/>
<rectangle x1="1.31318125" y1="7.239" x2="2.49681875" y2="7.32358125" layer="21"/>
<rectangle x1="14.097" y1="7.239" x2="28.829" y2="7.32358125" layer="21"/>
<rectangle x1="1.31318125" y1="7.32358125" x2="2.413" y2="7.40841875" layer="21"/>
<rectangle x1="14.01318125" y1="7.32358125" x2="28.91281875" y2="7.40841875" layer="21"/>
<rectangle x1="1.22681875" y1="7.40841875" x2="2.32918125" y2="7.493" layer="21"/>
<rectangle x1="14.01318125" y1="7.40841875" x2="28.99918125" y2="7.493" layer="21"/>
<rectangle x1="1.143" y1="7.493" x2="2.24281875" y2="7.57758125" layer="21"/>
<rectangle x1="14.01318125" y1="7.493" x2="28.99918125" y2="7.57758125" layer="21"/>
<rectangle x1="1.143" y1="7.57758125" x2="2.159" y2="7.66241875" layer="21"/>
<rectangle x1="13.92681875" y1="7.57758125" x2="29.083" y2="7.66241875" layer="21"/>
<rectangle x1="1.05918125" y1="7.66241875" x2="2.07518125" y2="7.747" layer="21"/>
<rectangle x1="13.92681875" y1="7.66241875" x2="29.16681875" y2="7.747" layer="21"/>
<rectangle x1="0.97281875" y1="7.747" x2="2.07518125" y2="7.83158125" layer="21"/>
<rectangle x1="13.92681875" y1="7.747" x2="29.16681875" y2="7.83158125" layer="21"/>
<rectangle x1="0.97281875" y1="7.83158125" x2="1.98881875" y2="7.91641875" layer="21"/>
<rectangle x1="13.92681875" y1="7.83158125" x2="29.25318125" y2="7.91641875" layer="21"/>
<rectangle x1="0.889" y1="7.91641875" x2="1.905" y2="8.001" layer="21"/>
<rectangle x1="13.843" y1="7.91641875" x2="29.337" y2="8.001" layer="21"/>
<rectangle x1="0.889" y1="8.001" x2="1.82118125" y2="8.08558125" layer="21"/>
<rectangle x1="13.843" y1="8.001" x2="29.337" y2="8.08558125" layer="21"/>
<rectangle x1="0.80518125" y1="8.08558125" x2="1.82118125" y2="8.17041875" layer="21"/>
<rectangle x1="13.843" y1="8.08558125" x2="29.42081875" y2="8.17041875" layer="21"/>
<rectangle x1="0.71881875" y1="8.17041875" x2="1.73481875" y2="8.255" layer="21"/>
<rectangle x1="13.843" y1="8.17041875" x2="29.42081875" y2="8.255" layer="21"/>
<rectangle x1="0.71881875" y1="8.255" x2="1.73481875" y2="8.33958125" layer="21"/>
<rectangle x1="13.75918125" y1="8.255" x2="29.50718125" y2="8.33958125" layer="21"/>
<rectangle x1="0.71881875" y1="8.33958125" x2="1.651" y2="8.42441875" layer="21"/>
<rectangle x1="13.75918125" y1="8.33958125" x2="29.50718125" y2="8.42441875" layer="21"/>
<rectangle x1="0.635" y1="8.42441875" x2="1.651" y2="8.509" layer="21"/>
<rectangle x1="5.63118125" y1="8.42441875" x2="6.64718125" y2="8.509" layer="21"/>
<rectangle x1="8.763" y1="8.42441875" x2="10.541" y2="8.509" layer="21"/>
<rectangle x1="13.75918125" y1="8.42441875" x2="16.80718125" y2="8.509" layer="21"/>
<rectangle x1="17.145" y1="8.42441875" x2="29.591" y2="8.509" layer="21"/>
<rectangle x1="0.635" y1="8.509" x2="1.56718125" y2="8.59358125" layer="21"/>
<rectangle x1="5.29081875" y1="8.509" x2="6.90118125" y2="8.59358125" layer="21"/>
<rectangle x1="8.67918125" y1="8.509" x2="10.87881875" y2="8.59358125" layer="21"/>
<rectangle x1="13.75918125" y1="8.509" x2="16.29918125" y2="8.59358125" layer="21"/>
<rectangle x1="17.653" y1="8.509" x2="20.10918125" y2="8.59358125" layer="21"/>
<rectangle x1="20.447" y1="8.509" x2="22.733" y2="8.59358125" layer="21"/>
<rectangle x1="23.07081875" y1="8.509" x2="24.84881875" y2="8.59358125" layer="21"/>
<rectangle x1="25.61081875" y1="8.509" x2="29.591" y2="8.59358125" layer="21"/>
<rectangle x1="0.55118125" y1="8.59358125" x2="1.48081875" y2="8.67841875" layer="21"/>
<rectangle x1="5.12318125" y1="8.59358125" x2="7.15518125" y2="8.67841875" layer="21"/>
<rectangle x1="8.59281875" y1="8.59358125" x2="11.049" y2="8.67841875" layer="21"/>
<rectangle x1="13.75918125" y1="8.59358125" x2="16.04518125" y2="8.67841875" layer="21"/>
<rectangle x1="17.907" y1="8.59358125" x2="20.02281875" y2="8.67841875" layer="21"/>
<rectangle x1="20.53081875" y1="8.59358125" x2="22.64918125" y2="8.67841875" layer="21"/>
<rectangle x1="23.15718125" y1="8.59358125" x2="24.59481875" y2="8.67841875" layer="21"/>
<rectangle x1="25.86481875" y1="8.59358125" x2="29.591" y2="8.67841875" layer="21"/>
<rectangle x1="0.55118125" y1="8.67841875" x2="1.48081875" y2="8.763" layer="21"/>
<rectangle x1="4.953" y1="8.67841875" x2="7.32281875" y2="8.763" layer="21"/>
<rectangle x1="8.59281875" y1="8.67841875" x2="11.21918125" y2="8.763" layer="21"/>
<rectangle x1="13.67281875" y1="8.67841875" x2="15.875" y2="8.763" layer="21"/>
<rectangle x1="18.07718125" y1="8.67841875" x2="19.939" y2="8.763" layer="21"/>
<rectangle x1="20.53081875" y1="8.67841875" x2="22.64918125" y2="8.763" layer="21"/>
<rectangle x1="23.15718125" y1="8.67841875" x2="24.42718125" y2="8.763" layer="21"/>
<rectangle x1="26.11881875" y1="8.67841875" x2="29.67481875" y2="8.763" layer="21"/>
<rectangle x1="0.46481875" y1="8.763" x2="1.397" y2="8.84758125" layer="21"/>
<rectangle x1="4.78281875" y1="8.763" x2="7.493" y2="8.84758125" layer="21"/>
<rectangle x1="8.59281875" y1="8.763" x2="11.38681875" y2="8.84758125" layer="21"/>
<rectangle x1="13.67281875" y1="8.763" x2="15.70481875" y2="8.84758125" layer="21"/>
<rectangle x1="18.24481875" y1="8.763" x2="19.939" y2="8.84758125" layer="21"/>
<rectangle x1="20.53081875" y1="8.763" x2="22.64918125" y2="8.84758125" layer="21"/>
<rectangle x1="23.241" y1="8.763" x2="24.34081875" y2="8.84758125" layer="21"/>
<rectangle x1="26.289" y1="8.763" x2="29.67481875" y2="8.84758125" layer="21"/>
<rectangle x1="0.46481875" y1="8.84758125" x2="1.397" y2="8.93241875" layer="21"/>
<rectangle x1="4.699" y1="8.84758125" x2="7.57681875" y2="8.93241875" layer="21"/>
<rectangle x1="8.59281875" y1="8.84758125" x2="11.47318125" y2="8.93241875" layer="21"/>
<rectangle x1="13.67281875" y1="8.84758125" x2="15.621" y2="8.93241875" layer="21"/>
<rectangle x1="18.33118125" y1="8.84758125" x2="19.939" y2="8.93241875" layer="21"/>
<rectangle x1="20.53081875" y1="8.84758125" x2="22.56281875" y2="8.93241875" layer="21"/>
<rectangle x1="23.241" y1="8.84758125" x2="24.17318125" y2="8.93241875" layer="21"/>
<rectangle x1="26.37281875" y1="8.84758125" x2="29.76118125" y2="8.93241875" layer="21"/>
<rectangle x1="0.46481875" y1="8.93241875" x2="1.397" y2="9.017" layer="21"/>
<rectangle x1="4.52881875" y1="8.93241875" x2="7.66318125" y2="9.017" layer="21"/>
<rectangle x1="8.59281875" y1="8.93241875" x2="11.557" y2="9.017" layer="21"/>
<rectangle x1="13.67281875" y1="8.93241875" x2="15.45081875" y2="9.017" layer="21"/>
<rectangle x1="18.49881875" y1="8.93241875" x2="19.939" y2="9.017" layer="21"/>
<rectangle x1="20.53081875" y1="8.93241875" x2="22.56281875" y2="9.017" layer="21"/>
<rectangle x1="23.241" y1="8.93241875" x2="24.08681875" y2="9.017" layer="21"/>
<rectangle x1="26.45918125" y1="8.93241875" x2="29.76118125" y2="9.017" layer="21"/>
<rectangle x1="0.381" y1="9.017" x2="1.31318125" y2="9.10158125" layer="21"/>
<rectangle x1="4.445" y1="9.017" x2="7.83081875" y2="9.10158125" layer="21"/>
<rectangle x1="8.59281875" y1="9.017" x2="11.72718125" y2="9.10158125" layer="21"/>
<rectangle x1="13.67281875" y1="9.017" x2="15.367" y2="9.10158125" layer="21"/>
<rectangle x1="18.58518125" y1="9.017" x2="19.939" y2="9.10158125" layer="21"/>
<rectangle x1="20.53081875" y1="9.017" x2="22.56281875" y2="9.10158125" layer="21"/>
<rectangle x1="23.241" y1="9.017" x2="24.003" y2="9.10158125" layer="21"/>
<rectangle x1="26.543" y1="9.017" x2="29.76118125" y2="9.10158125" layer="21"/>
<rectangle x1="0.381" y1="9.10158125" x2="1.31318125" y2="9.18641875" layer="21"/>
<rectangle x1="4.36118125" y1="9.10158125" x2="7.91718125" y2="9.18641875" layer="21"/>
<rectangle x1="8.67918125" y1="9.10158125" x2="11.811" y2="9.18641875" layer="21"/>
<rectangle x1="13.67281875" y1="9.10158125" x2="15.28318125" y2="9.18641875" layer="21"/>
<rectangle x1="18.669" y1="9.10158125" x2="19.939" y2="9.18641875" layer="21"/>
<rectangle x1="20.53081875" y1="9.10158125" x2="22.56281875" y2="9.18641875" layer="21"/>
<rectangle x1="23.241" y1="9.10158125" x2="23.91918125" y2="9.18641875" layer="21"/>
<rectangle x1="25.019" y1="9.10158125" x2="25.44318125" y2="9.18641875" layer="21"/>
<rectangle x1="26.62681875" y1="9.10158125" x2="29.845" y2="9.18641875" layer="21"/>
<rectangle x1="0.381" y1="9.18641875" x2="1.31318125" y2="9.271" layer="21"/>
<rectangle x1="4.27481875" y1="9.18641875" x2="8.001" y2="9.271" layer="21"/>
<rectangle x1="8.763" y1="9.18641875" x2="11.89481875" y2="9.271" layer="21"/>
<rectangle x1="13.67281875" y1="9.18641875" x2="15.19681875" y2="9.271" layer="21"/>
<rectangle x1="16.891" y1="9.18641875" x2="17.06118125" y2="9.271" layer="21"/>
<rectangle x1="18.75281875" y1="9.18641875" x2="19.939" y2="9.271" layer="21"/>
<rectangle x1="20.53081875" y1="9.18641875" x2="22.56281875" y2="9.271" layer="21"/>
<rectangle x1="23.241" y1="9.18641875" x2="23.91918125" y2="9.271" layer="21"/>
<rectangle x1="24.765" y1="9.18641875" x2="25.781" y2="9.271" layer="21"/>
<rectangle x1="26.71318125" y1="9.18641875" x2="29.845" y2="9.271" layer="21"/>
<rectangle x1="0.29718125" y1="9.271" x2="1.22681875" y2="9.35558125" layer="21"/>
<rectangle x1="4.191" y1="9.271" x2="5.79881875" y2="9.35558125" layer="21"/>
<rectangle x1="6.477" y1="9.271" x2="8.08481875" y2="9.35558125" layer="21"/>
<rectangle x1="10.287" y1="9.271" x2="11.89481875" y2="9.35558125" layer="21"/>
<rectangle x1="13.589" y1="9.271" x2="15.113" y2="9.35558125" layer="21"/>
<rectangle x1="16.46681875" y1="9.271" x2="17.48281875" y2="9.35558125" layer="21"/>
<rectangle x1="18.83918125" y1="9.271" x2="19.939" y2="9.35558125" layer="21"/>
<rectangle x1="20.53081875" y1="9.271" x2="22.56281875" y2="9.35558125" layer="21"/>
<rectangle x1="23.241" y1="9.271" x2="23.83281875" y2="9.35558125" layer="21"/>
<rectangle x1="24.68118125" y1="9.271" x2="25.95118125" y2="9.35558125" layer="21"/>
<rectangle x1="26.71318125" y1="9.271" x2="29.845" y2="9.35558125" layer="21"/>
<rectangle x1="0.29718125" y1="9.35558125" x2="1.22681875" y2="9.44041875" layer="21"/>
<rectangle x1="4.10718125" y1="9.35558125" x2="5.54481875" y2="9.44041875" layer="21"/>
<rectangle x1="6.731" y1="9.35558125" x2="8.08481875" y2="9.44041875" layer="21"/>
<rectangle x1="10.62481875" y1="9.35558125" x2="11.98118125" y2="9.44041875" layer="21"/>
<rectangle x1="13.589" y1="9.35558125" x2="15.02918125" y2="9.44041875" layer="21"/>
<rectangle x1="16.21281875" y1="9.35558125" x2="17.653" y2="9.44041875" layer="21"/>
<rectangle x1="18.923" y1="9.35558125" x2="19.939" y2="9.44041875" layer="21"/>
<rectangle x1="20.53081875" y1="9.35558125" x2="22.56281875" y2="9.44041875" layer="21"/>
<rectangle x1="23.241" y1="9.35558125" x2="23.749" y2="9.44041875" layer="21"/>
<rectangle x1="24.59481875" y1="9.35558125" x2="26.035" y2="9.44041875" layer="21"/>
<rectangle x1="26.71318125" y1="9.35558125" x2="29.845" y2="9.44041875" layer="21"/>
<rectangle x1="0.29718125" y1="9.44041875" x2="1.22681875" y2="9.525" layer="21"/>
<rectangle x1="4.10718125" y1="9.44041875" x2="5.37718125" y2="9.525" layer="21"/>
<rectangle x1="6.90118125" y1="9.44041875" x2="8.17118125" y2="9.525" layer="21"/>
<rectangle x1="10.795" y1="9.44041875" x2="12.065" y2="9.525" layer="21"/>
<rectangle x1="13.589" y1="9.44041875" x2="14.94281875" y2="9.525" layer="21"/>
<rectangle x1="16.129" y1="9.44041875" x2="17.82318125" y2="9.525" layer="21"/>
<rectangle x1="18.923" y1="9.44041875" x2="19.939" y2="9.525" layer="21"/>
<rectangle x1="20.53081875" y1="9.44041875" x2="22.56281875" y2="9.525" layer="21"/>
<rectangle x1="23.241" y1="9.44041875" x2="23.749" y2="9.525" layer="21"/>
<rectangle x1="24.511" y1="9.44041875" x2="26.11881875" y2="9.525" layer="21"/>
<rectangle x1="26.62681875" y1="9.44041875" x2="29.92881875" y2="9.525" layer="21"/>
<rectangle x1="0.29718125" y1="9.525" x2="1.143" y2="9.60958125" layer="21"/>
<rectangle x1="4.02081875" y1="9.525" x2="5.207" y2="9.60958125" layer="21"/>
<rectangle x1="7.06881875" y1="9.525" x2="8.255" y2="9.60958125" layer="21"/>
<rectangle x1="10.96518125" y1="9.525" x2="12.14881875" y2="9.60958125" layer="21"/>
<rectangle x1="13.589" y1="9.525" x2="14.94281875" y2="9.60958125" layer="21"/>
<rectangle x1="15.95881875" y1="9.525" x2="17.99081875" y2="9.60958125" layer="21"/>
<rectangle x1="19.00681875" y1="9.525" x2="19.939" y2="9.60958125" layer="21"/>
<rectangle x1="20.53081875" y1="9.525" x2="22.56281875" y2="9.60958125" layer="21"/>
<rectangle x1="23.241" y1="9.525" x2="23.749" y2="9.60958125" layer="21"/>
<rectangle x1="24.42718125" y1="9.525" x2="26.20518125" y2="9.60958125" layer="21"/>
<rectangle x1="26.543" y1="9.525" x2="29.92881875" y2="9.60958125" layer="21"/>
<rectangle x1="0.21081875" y1="9.60958125" x2="1.143" y2="9.69441875" layer="21"/>
<rectangle x1="3.937" y1="9.60958125" x2="5.12318125" y2="9.69441875" layer="21"/>
<rectangle x1="7.15518125" y1="9.60958125" x2="8.255" y2="9.69441875" layer="21"/>
<rectangle x1="11.049" y1="9.60958125" x2="12.14881875" y2="9.69441875" layer="21"/>
<rectangle x1="13.589" y1="9.60958125" x2="14.859" y2="9.69441875" layer="21"/>
<rectangle x1="15.875" y1="9.60958125" x2="18.07718125" y2="9.69441875" layer="21"/>
<rectangle x1="19.09318125" y1="9.60958125" x2="19.939" y2="9.69441875" layer="21"/>
<rectangle x1="20.53081875" y1="9.60958125" x2="22.56281875" y2="9.69441875" layer="21"/>
<rectangle x1="23.241" y1="9.60958125" x2="23.66518125" y2="9.69441875" layer="21"/>
<rectangle x1="24.34081875" y1="9.60958125" x2="29.92881875" y2="9.69441875" layer="21"/>
<rectangle x1="0.21081875" y1="9.69441875" x2="1.143" y2="9.779" layer="21"/>
<rectangle x1="3.937" y1="9.69441875" x2="4.953" y2="9.779" layer="21"/>
<rectangle x1="7.239" y1="9.69441875" x2="8.33881875" y2="9.779" layer="21"/>
<rectangle x1="11.13281875" y1="9.69441875" x2="12.23518125" y2="9.779" layer="21"/>
<rectangle x1="13.589" y1="9.69441875" x2="14.77518125" y2="9.779" layer="21"/>
<rectangle x1="15.79118125" y1="9.69441875" x2="18.161" y2="9.779" layer="21"/>
<rectangle x1="19.09318125" y1="9.69441875" x2="19.939" y2="9.779" layer="21"/>
<rectangle x1="20.53081875" y1="9.69441875" x2="22.56281875" y2="9.779" layer="21"/>
<rectangle x1="23.241" y1="9.69441875" x2="23.66518125" y2="9.779" layer="21"/>
<rectangle x1="24.257" y1="9.69441875" x2="29.92881875" y2="9.779" layer="21"/>
<rectangle x1="0.21081875" y1="9.779" x2="1.143" y2="9.86358125" layer="21"/>
<rectangle x1="3.85318125" y1="9.779" x2="4.86918125" y2="9.86358125" layer="21"/>
<rectangle x1="7.32281875" y1="9.779" x2="8.33881875" y2="9.86358125" layer="21"/>
<rectangle x1="11.21918125" y1="9.779" x2="12.23518125" y2="9.86358125" layer="21"/>
<rectangle x1="13.589" y1="9.779" x2="14.77518125" y2="9.86358125" layer="21"/>
<rectangle x1="15.70481875" y1="9.779" x2="18.24481875" y2="9.86358125" layer="21"/>
<rectangle x1="19.177" y1="9.779" x2="19.939" y2="9.86358125" layer="21"/>
<rectangle x1="20.53081875" y1="9.779" x2="22.56281875" y2="9.86358125" layer="21"/>
<rectangle x1="23.241" y1="9.779" x2="23.66518125" y2="9.86358125" layer="21"/>
<rectangle x1="24.34081875" y1="9.779" x2="29.92881875" y2="9.86358125" layer="21"/>
<rectangle x1="0.21081875" y1="9.86358125" x2="1.05918125" y2="9.94841875" layer="21"/>
<rectangle x1="3.85318125" y1="9.86358125" x2="4.86918125" y2="9.94841875" layer="21"/>
<rectangle x1="7.40918125" y1="9.86358125" x2="8.42518125" y2="9.94841875" layer="21"/>
<rectangle x1="11.303" y1="9.86358125" x2="12.319" y2="9.94841875" layer="21"/>
<rectangle x1="13.589" y1="9.86358125" x2="14.68881875" y2="9.94841875" layer="21"/>
<rectangle x1="15.621" y1="9.86358125" x2="18.33118125" y2="9.94841875" layer="21"/>
<rectangle x1="19.177" y1="9.86358125" x2="19.939" y2="9.94841875" layer="21"/>
<rectangle x1="20.53081875" y1="9.86358125" x2="22.56281875" y2="9.94841875" layer="21"/>
<rectangle x1="23.241" y1="9.86358125" x2="23.66518125" y2="9.94841875" layer="21"/>
<rectangle x1="26.71318125" y1="9.86358125" x2="29.92881875" y2="9.94841875" layer="21"/>
<rectangle x1="0.21081875" y1="9.94841875" x2="1.05918125" y2="10.033" layer="21"/>
<rectangle x1="3.76681875" y1="9.94841875" x2="4.78281875" y2="10.033" layer="21"/>
<rectangle x1="7.493" y1="9.94841875" x2="8.42518125" y2="10.033" layer="21"/>
<rectangle x1="11.38681875" y1="9.94841875" x2="12.319" y2="10.033" layer="21"/>
<rectangle x1="13.589" y1="9.94841875" x2="14.68881875" y2="10.033" layer="21"/>
<rectangle x1="15.53718125" y1="9.94841875" x2="18.33118125" y2="10.033" layer="21"/>
<rectangle x1="19.26081875" y1="9.94841875" x2="19.939" y2="10.033" layer="21"/>
<rectangle x1="20.53081875" y1="9.94841875" x2="22.56281875" y2="10.033" layer="21"/>
<rectangle x1="23.241" y1="9.94841875" x2="23.57881875" y2="10.033" layer="21"/>
<rectangle x1="26.797" y1="9.94841875" x2="29.92881875" y2="10.033" layer="21"/>
<rectangle x1="0.21081875" y1="10.033" x2="1.05918125" y2="10.11758125" layer="21"/>
<rectangle x1="3.76681875" y1="10.033" x2="4.699" y2="10.11758125" layer="21"/>
<rectangle x1="7.493" y1="10.033" x2="8.509" y2="10.11758125" layer="21"/>
<rectangle x1="11.38681875" y1="10.033" x2="12.40281875" y2="10.11758125" layer="21"/>
<rectangle x1="13.589" y1="10.033" x2="14.68881875" y2="10.11758125" layer="21"/>
<rectangle x1="15.53718125" y1="10.033" x2="18.415" y2="10.11758125" layer="21"/>
<rectangle x1="19.26081875" y1="10.033" x2="19.939" y2="10.11758125" layer="21"/>
<rectangle x1="20.53081875" y1="10.033" x2="22.56281875" y2="10.11758125" layer="21"/>
<rectangle x1="23.241" y1="10.033" x2="23.57881875" y2="10.11758125" layer="21"/>
<rectangle x1="26.88081875" y1="10.033" x2="29.92881875" y2="10.11758125" layer="21"/>
<rectangle x1="0.127" y1="10.11758125" x2="1.05918125" y2="10.20241875" layer="21"/>
<rectangle x1="3.76681875" y1="10.11758125" x2="4.699" y2="10.20241875" layer="21"/>
<rectangle x1="7.57681875" y1="10.11758125" x2="8.509" y2="10.20241875" layer="21"/>
<rectangle x1="11.47318125" y1="10.11758125" x2="12.40281875" y2="10.20241875" layer="21"/>
<rectangle x1="13.589" y1="10.11758125" x2="14.605" y2="10.20241875" layer="21"/>
<rectangle x1="15.45081875" y1="10.11758125" x2="18.49881875" y2="10.20241875" layer="21"/>
<rectangle x1="19.34718125" y1="10.11758125" x2="19.939" y2="10.20241875" layer="21"/>
<rectangle x1="20.53081875" y1="10.11758125" x2="22.56281875" y2="10.20241875" layer="21"/>
<rectangle x1="23.241" y1="10.11758125" x2="23.57881875" y2="10.20241875" layer="21"/>
<rectangle x1="26.88081875" y1="10.11758125" x2="29.92881875" y2="10.20241875" layer="21"/>
<rectangle x1="0.127" y1="10.20241875" x2="1.05918125" y2="10.287" layer="21"/>
<rectangle x1="3.683" y1="10.20241875" x2="4.61518125" y2="10.287" layer="21"/>
<rectangle x1="7.57681875" y1="10.20241875" x2="8.509" y2="10.287" layer="21"/>
<rectangle x1="11.557" y1="10.20241875" x2="12.40281875" y2="10.287" layer="21"/>
<rectangle x1="13.50518125" y1="10.20241875" x2="14.605" y2="10.287" layer="21"/>
<rectangle x1="15.45081875" y1="10.20241875" x2="18.49881875" y2="10.287" layer="21"/>
<rectangle x1="19.34718125" y1="10.20241875" x2="19.939" y2="10.287" layer="21"/>
<rectangle x1="20.53081875" y1="10.20241875" x2="22.56281875" y2="10.287" layer="21"/>
<rectangle x1="23.241" y1="10.20241875" x2="23.57881875" y2="10.287" layer="21"/>
<rectangle x1="26.88081875" y1="10.20241875" x2="29.92881875" y2="10.287" layer="21"/>
<rectangle x1="0.127" y1="10.287" x2="0.97281875" y2="10.37158125" layer="21"/>
<rectangle x1="3.683" y1="10.287" x2="4.61518125" y2="10.37158125" layer="21"/>
<rectangle x1="7.66318125" y1="10.287" x2="8.509" y2="10.37158125" layer="21"/>
<rectangle x1="11.557" y1="10.287" x2="12.48918125" y2="10.37158125" layer="21"/>
<rectangle x1="13.50518125" y1="10.287" x2="14.605" y2="10.37158125" layer="21"/>
<rectangle x1="15.367" y1="10.287" x2="18.49881875" y2="10.37158125" layer="21"/>
<rectangle x1="19.34718125" y1="10.287" x2="19.939" y2="10.37158125" layer="21"/>
<rectangle x1="20.53081875" y1="10.287" x2="22.56281875" y2="10.37158125" layer="21"/>
<rectangle x1="23.241" y1="10.287" x2="23.57881875" y2="10.37158125" layer="21"/>
<rectangle x1="26.88081875" y1="10.287" x2="29.92881875" y2="10.37158125" layer="21"/>
<rectangle x1="0.127" y1="10.37158125" x2="0.97281875" y2="10.45641875" layer="21"/>
<rectangle x1="3.683" y1="10.37158125" x2="4.61518125" y2="10.45641875" layer="21"/>
<rectangle x1="7.66318125" y1="10.37158125" x2="8.59281875" y2="10.45641875" layer="21"/>
<rectangle x1="11.557" y1="10.37158125" x2="12.48918125" y2="10.45641875" layer="21"/>
<rectangle x1="13.50518125" y1="10.37158125" x2="14.605" y2="10.45641875" layer="21"/>
<rectangle x1="15.367" y1="10.37158125" x2="18.58518125" y2="10.45641875" layer="21"/>
<rectangle x1="19.34718125" y1="10.37158125" x2="19.939" y2="10.45641875" layer="21"/>
<rectangle x1="20.61718125" y1="10.37158125" x2="22.56281875" y2="10.45641875" layer="21"/>
<rectangle x1="23.15718125" y1="10.37158125" x2="23.66518125" y2="10.45641875" layer="21"/>
<rectangle x1="26.88081875" y1="10.37158125" x2="29.92881875" y2="10.45641875" layer="21"/>
<rectangle x1="0.127" y1="10.45641875" x2="0.97281875" y2="10.541" layer="21"/>
<rectangle x1="3.683" y1="10.45641875" x2="4.52881875" y2="10.541" layer="21"/>
<rectangle x1="7.747" y1="10.45641875" x2="8.59281875" y2="10.541" layer="21"/>
<rectangle x1="11.64081875" y1="10.45641875" x2="12.48918125" y2="10.541" layer="21"/>
<rectangle x1="13.50518125" y1="10.45641875" x2="14.52118125" y2="10.541" layer="21"/>
<rectangle x1="15.367" y1="10.45641875" x2="18.58518125" y2="10.541" layer="21"/>
<rectangle x1="19.34718125" y1="10.45641875" x2="19.939" y2="10.541" layer="21"/>
<rectangle x1="20.61718125" y1="10.45641875" x2="22.56281875" y2="10.541" layer="21"/>
<rectangle x1="23.15718125" y1="10.45641875" x2="23.66518125" y2="10.541" layer="21"/>
<rectangle x1="24.34081875" y1="10.45641875" x2="26.20518125" y2="10.541" layer="21"/>
<rectangle x1="26.797" y1="10.45641875" x2="29.92881875" y2="10.541" layer="21"/>
<rectangle x1="0.127" y1="10.541" x2="0.97281875" y2="10.62558125" layer="21"/>
<rectangle x1="3.683" y1="10.541" x2="4.52881875" y2="10.62558125" layer="21"/>
<rectangle x1="7.747" y1="10.541" x2="8.59281875" y2="10.62558125" layer="21"/>
<rectangle x1="11.64081875" y1="10.541" x2="12.48918125" y2="10.62558125" layer="21"/>
<rectangle x1="13.50518125" y1="10.541" x2="14.52118125" y2="10.62558125" layer="21"/>
<rectangle x1="15.28318125" y1="10.541" x2="18.58518125" y2="10.62558125" layer="21"/>
<rectangle x1="19.431" y1="10.541" x2="19.939" y2="10.62558125" layer="21"/>
<rectangle x1="20.61718125" y1="10.541" x2="22.479" y2="10.62558125" layer="21"/>
<rectangle x1="23.15718125" y1="10.541" x2="23.66518125" y2="10.62558125" layer="21"/>
<rectangle x1="24.34081875" y1="10.541" x2="26.20518125" y2="10.62558125" layer="21"/>
<rectangle x1="26.797" y1="10.541" x2="29.92881875" y2="10.62558125" layer="21"/>
<rectangle x1="0.127" y1="10.62558125" x2="0.97281875" y2="10.71041875" layer="21"/>
<rectangle x1="3.59918125" y1="10.62558125" x2="4.52881875" y2="10.71041875" layer="21"/>
<rectangle x1="7.747" y1="10.62558125" x2="8.59281875" y2="10.71041875" layer="21"/>
<rectangle x1="11.64081875" y1="10.62558125" x2="12.48918125" y2="10.71041875" layer="21"/>
<rectangle x1="13.50518125" y1="10.62558125" x2="14.52118125" y2="10.71041875" layer="21"/>
<rectangle x1="15.28318125" y1="10.62558125" x2="18.669" y2="10.71041875" layer="21"/>
<rectangle x1="19.431" y1="10.62558125" x2="19.939" y2="10.71041875" layer="21"/>
<rectangle x1="20.701" y1="10.62558125" x2="22.479" y2="10.71041875" layer="21"/>
<rectangle x1="23.15718125" y1="10.62558125" x2="23.66518125" y2="10.71041875" layer="21"/>
<rectangle x1="24.34081875" y1="10.62558125" x2="26.11881875" y2="10.71041875" layer="21"/>
<rectangle x1="26.797" y1="10.62558125" x2="29.92881875" y2="10.71041875" layer="21"/>
<rectangle x1="0.127" y1="10.71041875" x2="0.97281875" y2="10.795" layer="21"/>
<rectangle x1="3.59918125" y1="10.71041875" x2="4.52881875" y2="10.795" layer="21"/>
<rectangle x1="7.747" y1="10.71041875" x2="8.59281875" y2="10.795" layer="21"/>
<rectangle x1="11.64081875" y1="10.71041875" x2="12.48918125" y2="10.795" layer="21"/>
<rectangle x1="13.50518125" y1="10.71041875" x2="14.52118125" y2="10.795" layer="21"/>
<rectangle x1="15.28318125" y1="10.71041875" x2="18.669" y2="10.795" layer="21"/>
<rectangle x1="19.431" y1="10.71041875" x2="19.939" y2="10.795" layer="21"/>
<rectangle x1="20.78481875" y1="10.71041875" x2="22.39518125" y2="10.795" layer="21"/>
<rectangle x1="23.07081875" y1="10.71041875" x2="23.749" y2="10.795" layer="21"/>
<rectangle x1="24.42718125" y1="10.71041875" x2="26.035" y2="10.795" layer="21"/>
<rectangle x1="26.797" y1="10.71041875" x2="29.92881875" y2="10.795" layer="21"/>
<rectangle x1="0.127" y1="10.795" x2="0.97281875" y2="10.87958125" layer="21"/>
<rectangle x1="3.59918125" y1="10.795" x2="4.52881875" y2="10.87958125" layer="21"/>
<rectangle x1="7.747" y1="10.795" x2="8.59281875" y2="10.87958125" layer="21"/>
<rectangle x1="11.64081875" y1="10.795" x2="12.48918125" y2="10.87958125" layer="21"/>
<rectangle x1="13.50518125" y1="10.795" x2="14.52118125" y2="10.87958125" layer="21"/>
<rectangle x1="15.28318125" y1="10.795" x2="18.669" y2="10.87958125" layer="21"/>
<rectangle x1="19.431" y1="10.795" x2="19.939" y2="10.87958125" layer="21"/>
<rectangle x1="20.78481875" y1="10.795" x2="22.30881875" y2="10.87958125" layer="21"/>
<rectangle x1="23.07081875" y1="10.795" x2="23.749" y2="10.87958125" layer="21"/>
<rectangle x1="24.511" y1="10.795" x2="26.035" y2="10.87958125" layer="21"/>
<rectangle x1="26.71318125" y1="10.795" x2="29.92881875" y2="10.87958125" layer="21"/>
<rectangle x1="0.127" y1="10.87958125" x2="0.97281875" y2="10.96441875" layer="21"/>
<rectangle x1="3.59918125" y1="10.87958125" x2="4.52881875" y2="10.96441875" layer="21"/>
<rectangle x1="7.747" y1="10.87958125" x2="8.59281875" y2="10.96441875" layer="21"/>
<rectangle x1="11.64081875" y1="10.87958125" x2="12.48918125" y2="10.96441875" layer="21"/>
<rectangle x1="13.50518125" y1="10.87958125" x2="14.52118125" y2="10.96441875" layer="21"/>
<rectangle x1="15.28318125" y1="10.87958125" x2="18.669" y2="10.96441875" layer="21"/>
<rectangle x1="19.431" y1="10.87958125" x2="19.939" y2="10.96441875" layer="21"/>
<rectangle x1="20.87118125" y1="10.87958125" x2="22.225" y2="10.96441875" layer="21"/>
<rectangle x1="22.987" y1="10.87958125" x2="23.83281875" y2="10.96441875" layer="21"/>
<rectangle x1="24.59481875" y1="10.87958125" x2="25.95118125" y2="10.96441875" layer="21"/>
<rectangle x1="26.71318125" y1="10.87958125" x2="29.92881875" y2="10.96441875" layer="21"/>
<rectangle x1="0.127" y1="10.96441875" x2="0.97281875" y2="11.049" layer="21"/>
<rectangle x1="3.59918125" y1="10.96441875" x2="4.52881875" y2="11.049" layer="21"/>
<rectangle x1="7.747" y1="10.96441875" x2="8.59281875" y2="11.049" layer="21"/>
<rectangle x1="11.64081875" y1="10.96441875" x2="12.48918125" y2="11.049" layer="21"/>
<rectangle x1="13.50518125" y1="10.96441875" x2="14.52118125" y2="11.049" layer="21"/>
<rectangle x1="15.28318125" y1="10.96441875" x2="18.669" y2="11.049" layer="21"/>
<rectangle x1="19.431" y1="10.96441875" x2="19.939" y2="11.049" layer="21"/>
<rectangle x1="21.03881875" y1="10.96441875" x2="22.14118125" y2="11.049" layer="21"/>
<rectangle x1="22.987" y1="10.96441875" x2="23.83281875" y2="11.049" layer="21"/>
<rectangle x1="24.68118125" y1="10.96441875" x2="25.781" y2="11.049" layer="21"/>
<rectangle x1="26.62681875" y1="10.96441875" x2="29.92881875" y2="11.049" layer="21"/>
<rectangle x1="0.127" y1="11.049" x2="0.97281875" y2="11.13358125" layer="21"/>
<rectangle x1="3.59918125" y1="11.049" x2="4.52881875" y2="11.13358125" layer="21"/>
<rectangle x1="7.747" y1="11.049" x2="8.59281875" y2="11.13358125" layer="21"/>
<rectangle x1="11.64081875" y1="11.049" x2="12.48918125" y2="11.13358125" layer="21"/>
<rectangle x1="13.50518125" y1="11.049" x2="14.52118125" y2="11.13358125" layer="21"/>
<rectangle x1="15.28318125" y1="11.049" x2="18.669" y2="11.13358125" layer="21"/>
<rectangle x1="19.431" y1="11.049" x2="19.939" y2="11.13358125" layer="21"/>
<rectangle x1="21.209" y1="11.049" x2="21.971" y2="11.13358125" layer="21"/>
<rectangle x1="22.90318125" y1="11.049" x2="23.91918125" y2="11.13358125" layer="21"/>
<rectangle x1="24.84881875" y1="11.049" x2="25.61081875" y2="11.13358125" layer="21"/>
<rectangle x1="26.543" y1="11.049" x2="29.92881875" y2="11.13358125" layer="21"/>
<rectangle x1="0.127" y1="11.13358125" x2="0.97281875" y2="11.21841875" layer="21"/>
<rectangle x1="3.59918125" y1="11.13358125" x2="4.52881875" y2="11.21841875" layer="21"/>
<rectangle x1="7.747" y1="11.13358125" x2="8.59281875" y2="11.21841875" layer="21"/>
<rectangle x1="11.64081875" y1="11.13358125" x2="12.48918125" y2="11.21841875" layer="21"/>
<rectangle x1="13.50518125" y1="11.13358125" x2="14.52118125" y2="11.21841875" layer="21"/>
<rectangle x1="15.28318125" y1="11.13358125" x2="18.58518125" y2="11.21841875" layer="21"/>
<rectangle x1="19.431" y1="11.13358125" x2="19.939" y2="11.21841875" layer="21"/>
<rectangle x1="21.463" y1="11.13358125" x2="21.63318125" y2="11.21841875" layer="21"/>
<rectangle x1="22.81681875" y1="11.13358125" x2="24.003" y2="11.21841875" layer="21"/>
<rectangle x1="25.18918125" y1="11.13358125" x2="25.273" y2="11.21841875" layer="21"/>
<rectangle x1="26.543" y1="11.13358125" x2="29.92881875" y2="11.21841875" layer="21"/>
<rectangle x1="0.127" y1="11.21841875" x2="0.97281875" y2="11.303" layer="21"/>
<rectangle x1="3.59918125" y1="11.21841875" x2="4.52881875" y2="11.303" layer="21"/>
<rectangle x1="7.747" y1="11.21841875" x2="8.59281875" y2="11.303" layer="21"/>
<rectangle x1="11.64081875" y1="11.21841875" x2="12.48918125" y2="11.303" layer="21"/>
<rectangle x1="13.50518125" y1="11.21841875" x2="14.52118125" y2="11.303" layer="21"/>
<rectangle x1="15.28318125" y1="11.21841875" x2="18.58518125" y2="11.303" layer="21"/>
<rectangle x1="19.34718125" y1="11.21841875" x2="19.939" y2="11.303" layer="21"/>
<rectangle x1="22.81681875" y1="11.21841875" x2="24.003" y2="11.303" layer="21"/>
<rectangle x1="26.45918125" y1="11.21841875" x2="29.92881875" y2="11.303" layer="21"/>
<rectangle x1="0.127" y1="11.303" x2="0.97281875" y2="11.38758125" layer="21"/>
<rectangle x1="3.59918125" y1="11.303" x2="4.52881875" y2="11.38758125" layer="21"/>
<rectangle x1="7.747" y1="11.303" x2="8.59281875" y2="11.38758125" layer="21"/>
<rectangle x1="11.557" y1="11.303" x2="12.48918125" y2="11.38758125" layer="21"/>
<rectangle x1="13.50518125" y1="11.303" x2="14.52118125" y2="11.38758125" layer="21"/>
<rectangle x1="15.367" y1="11.303" x2="18.58518125" y2="11.38758125" layer="21"/>
<rectangle x1="19.34718125" y1="11.303" x2="19.939" y2="11.38758125" layer="21"/>
<rectangle x1="22.64918125" y1="11.303" x2="24.08681875" y2="11.38758125" layer="21"/>
<rectangle x1="26.37281875" y1="11.303" x2="29.92881875" y2="11.38758125" layer="21"/>
<rectangle x1="0.127" y1="11.38758125" x2="0.97281875" y2="11.47241875" layer="21"/>
<rectangle x1="3.59918125" y1="11.38758125" x2="4.52881875" y2="11.47241875" layer="21"/>
<rectangle x1="7.747" y1="11.38758125" x2="8.59281875" y2="11.47241875" layer="21"/>
<rectangle x1="11.557" y1="11.38758125" x2="12.48918125" y2="11.47241875" layer="21"/>
<rectangle x1="13.50518125" y1="11.38758125" x2="14.605" y2="11.47241875" layer="21"/>
<rectangle x1="15.367" y1="11.38758125" x2="18.58518125" y2="11.47241875" layer="21"/>
<rectangle x1="19.34718125" y1="11.38758125" x2="19.939" y2="11.47241875" layer="21"/>
<rectangle x1="22.56281875" y1="11.38758125" x2="24.257" y2="11.47241875" layer="21"/>
<rectangle x1="26.289" y1="11.38758125" x2="29.92881875" y2="11.47241875" layer="21"/>
<rectangle x1="0.127" y1="11.47241875" x2="1.05918125" y2="11.557" layer="21"/>
<rectangle x1="3.59918125" y1="11.47241875" x2="4.52881875" y2="11.557" layer="21"/>
<rectangle x1="7.747" y1="11.47241875" x2="8.59281875" y2="11.557" layer="21"/>
<rectangle x1="11.557" y1="11.47241875" x2="12.40281875" y2="11.557" layer="21"/>
<rectangle x1="13.50518125" y1="11.47241875" x2="14.605" y2="11.557" layer="21"/>
<rectangle x1="15.45081875" y1="11.47241875" x2="18.49881875" y2="11.557" layer="21"/>
<rectangle x1="19.34718125" y1="11.47241875" x2="19.939" y2="11.557" layer="21"/>
<rectangle x1="20.53081875" y1="11.47241875" x2="20.701" y2="11.557" layer="21"/>
<rectangle x1="22.479" y1="11.47241875" x2="24.34081875" y2="11.557" layer="21"/>
<rectangle x1="26.11881875" y1="11.47241875" x2="29.92881875" y2="11.557" layer="21"/>
<rectangle x1="0.127" y1="11.557" x2="1.05918125" y2="11.64158125" layer="21"/>
<rectangle x1="3.59918125" y1="11.557" x2="4.52881875" y2="11.64158125" layer="21"/>
<rectangle x1="7.747" y1="11.557" x2="8.59281875" y2="11.64158125" layer="21"/>
<rectangle x1="11.47318125" y1="11.557" x2="12.40281875" y2="11.64158125" layer="21"/>
<rectangle x1="13.50518125" y1="11.557" x2="14.605" y2="11.64158125" layer="21"/>
<rectangle x1="15.45081875" y1="11.557" x2="18.49881875" y2="11.64158125" layer="21"/>
<rectangle x1="19.34718125" y1="11.557" x2="19.939" y2="11.64158125" layer="21"/>
<rectangle x1="20.53081875" y1="11.557" x2="20.78481875" y2="11.64158125" layer="21"/>
<rectangle x1="22.30881875" y1="11.557" x2="24.511" y2="11.64158125" layer="21"/>
<rectangle x1="26.035" y1="11.557" x2="29.92881875" y2="11.64158125" layer="21"/>
<rectangle x1="0.21081875" y1="11.64158125" x2="1.05918125" y2="11.72641875" layer="21"/>
<rectangle x1="3.59918125" y1="11.64158125" x2="4.52881875" y2="11.72641875" layer="21"/>
<rectangle x1="7.747" y1="11.64158125" x2="8.59281875" y2="11.72641875" layer="21"/>
<rectangle x1="11.47318125" y1="11.64158125" x2="12.40281875" y2="11.72641875" layer="21"/>
<rectangle x1="13.589" y1="11.64158125" x2="14.605" y2="11.72641875" layer="21"/>
<rectangle x1="15.45081875" y1="11.64158125" x2="18.415" y2="11.72641875" layer="21"/>
<rectangle x1="19.26081875" y1="11.64158125" x2="20.02281875" y2="11.72641875" layer="21"/>
<rectangle x1="20.447" y1="11.64158125" x2="21.03881875" y2="11.72641875" layer="21"/>
<rectangle x1="22.14118125" y1="11.64158125" x2="24.68118125" y2="11.72641875" layer="21"/>
<rectangle x1="25.781" y1="11.64158125" x2="29.92881875" y2="11.72641875" layer="21"/>
<rectangle x1="0.21081875" y1="11.72641875" x2="1.05918125" y2="11.811" layer="21"/>
<rectangle x1="3.59918125" y1="11.72641875" x2="4.52881875" y2="11.811" layer="21"/>
<rectangle x1="7.747" y1="11.72641875" x2="8.59281875" y2="11.811" layer="21"/>
<rectangle x1="11.38681875" y1="11.72641875" x2="12.40281875" y2="11.811" layer="21"/>
<rectangle x1="13.589" y1="11.72641875" x2="14.68881875" y2="11.811" layer="21"/>
<rectangle x1="15.53718125" y1="11.72641875" x2="18.415" y2="11.811" layer="21"/>
<rectangle x1="19.26081875" y1="11.72641875" x2="20.10918125" y2="11.811" layer="21"/>
<rectangle x1="20.36318125" y1="11.72641875" x2="21.29281875" y2="11.811" layer="21"/>
<rectangle x1="21.88718125" y1="11.72641875" x2="24.93518125" y2="11.811" layer="21"/>
<rectangle x1="25.527" y1="11.72641875" x2="29.92881875" y2="11.811" layer="21"/>
<rectangle x1="0.21081875" y1="11.811" x2="1.05918125" y2="11.89558125" layer="21"/>
<rectangle x1="3.59918125" y1="11.811" x2="4.52881875" y2="11.89558125" layer="21"/>
<rectangle x1="7.747" y1="11.811" x2="8.59281875" y2="11.89558125" layer="21"/>
<rectangle x1="11.38681875" y1="11.811" x2="12.319" y2="11.89558125" layer="21"/>
<rectangle x1="13.589" y1="11.811" x2="14.68881875" y2="11.89558125" layer="21"/>
<rectangle x1="15.621" y1="11.811" x2="18.33118125" y2="11.89558125" layer="21"/>
<rectangle x1="19.26081875" y1="11.811" x2="29.92881875" y2="11.89558125" layer="21"/>
<rectangle x1="0.21081875" y1="11.89558125" x2="1.05918125" y2="11.98041875" layer="21"/>
<rectangle x1="3.59918125" y1="11.89558125" x2="4.52881875" y2="11.98041875" layer="21"/>
<rectangle x1="7.747" y1="11.89558125" x2="8.59281875" y2="11.98041875" layer="21"/>
<rectangle x1="11.303" y1="11.89558125" x2="12.319" y2="11.98041875" layer="21"/>
<rectangle x1="13.589" y1="11.89558125" x2="14.77518125" y2="11.98041875" layer="21"/>
<rectangle x1="15.621" y1="11.89558125" x2="18.24481875" y2="11.98041875" layer="21"/>
<rectangle x1="19.177" y1="11.89558125" x2="29.92881875" y2="11.98041875" layer="21"/>
<rectangle x1="0.21081875" y1="11.98041875" x2="1.143" y2="12.065" layer="21"/>
<rectangle x1="3.59918125" y1="11.98041875" x2="4.52881875" y2="12.065" layer="21"/>
<rectangle x1="7.747" y1="11.98041875" x2="8.59281875" y2="12.065" layer="21"/>
<rectangle x1="11.21918125" y1="11.98041875" x2="12.23518125" y2="12.065" layer="21"/>
<rectangle x1="13.589" y1="11.98041875" x2="14.77518125" y2="12.065" layer="21"/>
<rectangle x1="15.70481875" y1="11.98041875" x2="18.161" y2="12.065" layer="21"/>
<rectangle x1="19.177" y1="11.98041875" x2="29.92881875" y2="12.065" layer="21"/>
<rectangle x1="0.21081875" y1="12.065" x2="1.143" y2="12.14958125" layer="21"/>
<rectangle x1="3.59918125" y1="12.065" x2="4.52881875" y2="12.14958125" layer="21"/>
<rectangle x1="7.747" y1="12.065" x2="8.59281875" y2="12.14958125" layer="21"/>
<rectangle x1="11.13281875" y1="12.065" x2="12.23518125" y2="12.14958125" layer="21"/>
<rectangle x1="13.589" y1="12.065" x2="14.859" y2="12.14958125" layer="21"/>
<rectangle x1="15.79118125" y1="12.065" x2="18.07718125" y2="12.14958125" layer="21"/>
<rectangle x1="19.09318125" y1="12.065" x2="29.92881875" y2="12.14958125" layer="21"/>
<rectangle x1="0.29718125" y1="12.14958125" x2="1.143" y2="12.23441875" layer="21"/>
<rectangle x1="3.59918125" y1="12.14958125" x2="4.52881875" y2="12.23441875" layer="21"/>
<rectangle x1="7.747" y1="12.14958125" x2="8.59281875" y2="12.23441875" layer="21"/>
<rectangle x1="11.049" y1="12.14958125" x2="12.14881875" y2="12.23441875" layer="21"/>
<rectangle x1="13.589" y1="12.14958125" x2="14.859" y2="12.23441875" layer="21"/>
<rectangle x1="15.875" y1="12.14958125" x2="17.99081875" y2="12.23441875" layer="21"/>
<rectangle x1="19.09318125" y1="12.14958125" x2="29.92881875" y2="12.23441875" layer="21"/>
<rectangle x1="0.29718125" y1="12.23441875" x2="1.143" y2="12.319" layer="21"/>
<rectangle x1="3.59918125" y1="12.23441875" x2="4.52881875" y2="12.319" layer="21"/>
<rectangle x1="7.747" y1="12.23441875" x2="8.59281875" y2="12.319" layer="21"/>
<rectangle x1="10.87881875" y1="12.23441875" x2="12.065" y2="12.319" layer="21"/>
<rectangle x1="13.589" y1="12.23441875" x2="14.94281875" y2="12.319" layer="21"/>
<rectangle x1="16.04518125" y1="12.23441875" x2="17.907" y2="12.319" layer="21"/>
<rectangle x1="19.00681875" y1="12.23441875" x2="29.92881875" y2="12.319" layer="21"/>
<rectangle x1="0.29718125" y1="12.319" x2="1.22681875" y2="12.40358125" layer="21"/>
<rectangle x1="3.59918125" y1="12.319" x2="4.52881875" y2="12.40358125" layer="21"/>
<rectangle x1="7.747" y1="12.319" x2="8.59281875" y2="12.40358125" layer="21"/>
<rectangle x1="10.71118125" y1="12.319" x2="12.065" y2="12.40358125" layer="21"/>
<rectangle x1="13.589" y1="12.319" x2="15.02918125" y2="12.40358125" layer="21"/>
<rectangle x1="16.129" y1="12.319" x2="17.82318125" y2="12.40358125" layer="21"/>
<rectangle x1="18.923" y1="12.319" x2="29.92881875" y2="12.40358125" layer="21"/>
<rectangle x1="0.29718125" y1="12.40358125" x2="1.22681875" y2="12.48841875" layer="21"/>
<rectangle x1="3.59918125" y1="12.40358125" x2="4.52881875" y2="12.48841875" layer="21"/>
<rectangle x1="7.747" y1="12.40358125" x2="8.59281875" y2="12.48841875" layer="21"/>
<rectangle x1="10.541" y1="12.40358125" x2="11.98118125" y2="12.48841875" layer="21"/>
<rectangle x1="13.589" y1="12.40358125" x2="15.02918125" y2="12.48841875" layer="21"/>
<rectangle x1="16.29918125" y1="12.40358125" x2="17.653" y2="12.48841875" layer="21"/>
<rectangle x1="18.83918125" y1="12.40358125" x2="29.845" y2="12.48841875" layer="21"/>
<rectangle x1="0.381" y1="12.48841875" x2="1.22681875" y2="12.573" layer="21"/>
<rectangle x1="3.59918125" y1="12.48841875" x2="4.52881875" y2="12.573" layer="21"/>
<rectangle x1="7.747" y1="12.48841875" x2="11.89481875" y2="12.573" layer="21"/>
<rectangle x1="13.67281875" y1="12.48841875" x2="15.113" y2="12.573" layer="21"/>
<rectangle x1="16.55318125" y1="12.48841875" x2="17.31518125" y2="12.573" layer="21"/>
<rectangle x1="18.83918125" y1="12.48841875" x2="29.845" y2="12.573" layer="21"/>
<rectangle x1="0.381" y1="12.573" x2="1.31318125" y2="12.65758125" layer="21"/>
<rectangle x1="3.59918125" y1="12.573" x2="4.52881875" y2="12.65758125" layer="21"/>
<rectangle x1="7.747" y1="12.573" x2="11.811" y2="12.65758125" layer="21"/>
<rectangle x1="13.67281875" y1="12.573" x2="15.19681875" y2="12.65758125" layer="21"/>
<rectangle x1="18.75281875" y1="12.573" x2="29.845" y2="12.65758125" layer="21"/>
<rectangle x1="0.381" y1="12.65758125" x2="1.31318125" y2="12.74241875" layer="21"/>
<rectangle x1="3.59918125" y1="12.65758125" x2="4.52881875" y2="12.74241875" layer="21"/>
<rectangle x1="7.747" y1="12.65758125" x2="11.72718125" y2="12.74241875" layer="21"/>
<rectangle x1="13.67281875" y1="12.65758125" x2="15.28318125" y2="12.74241875" layer="21"/>
<rectangle x1="18.669" y1="12.65758125" x2="29.76118125" y2="12.74241875" layer="21"/>
<rectangle x1="0.381" y1="12.74241875" x2="1.31318125" y2="12.827" layer="21"/>
<rectangle x1="3.59918125" y1="12.74241875" x2="4.52881875" y2="12.827" layer="21"/>
<rectangle x1="7.747" y1="12.74241875" x2="11.64081875" y2="12.827" layer="21"/>
<rectangle x1="13.67281875" y1="12.74241875" x2="15.367" y2="12.827" layer="21"/>
<rectangle x1="18.49881875" y1="12.74241875" x2="29.76118125" y2="12.827" layer="21"/>
<rectangle x1="0.46481875" y1="12.827" x2="1.397" y2="12.91158125" layer="21"/>
<rectangle x1="3.59918125" y1="12.827" x2="4.52881875" y2="12.91158125" layer="21"/>
<rectangle x1="7.747" y1="12.827" x2="11.557" y2="12.91158125" layer="21"/>
<rectangle x1="13.67281875" y1="12.827" x2="15.53718125" y2="12.91158125" layer="21"/>
<rectangle x1="18.415" y1="12.827" x2="29.76118125" y2="12.91158125" layer="21"/>
<rectangle x1="0.46481875" y1="12.91158125" x2="1.397" y2="12.99641875" layer="21"/>
<rectangle x1="3.59918125" y1="12.91158125" x2="4.52881875" y2="12.99641875" layer="21"/>
<rectangle x1="7.747" y1="12.91158125" x2="11.38681875" y2="12.99641875" layer="21"/>
<rectangle x1="13.67281875" y1="12.91158125" x2="15.621" y2="12.99641875" layer="21"/>
<rectangle x1="18.33118125" y1="12.91158125" x2="29.67481875" y2="12.99641875" layer="21"/>
<rectangle x1="0.55118125" y1="12.99641875" x2="1.48081875" y2="13.081" layer="21"/>
<rectangle x1="3.59918125" y1="12.99641875" x2="4.445" y2="13.081" layer="21"/>
<rectangle x1="7.747" y1="12.99641875" x2="11.303" y2="13.081" layer="21"/>
<rectangle x1="13.67281875" y1="12.99641875" x2="15.79118125" y2="13.081" layer="21"/>
<rectangle x1="18.161" y1="12.99641875" x2="29.67481875" y2="13.081" layer="21"/>
<rectangle x1="0.55118125" y1="13.081" x2="1.48081875" y2="13.16558125" layer="21"/>
<rectangle x1="3.683" y1="13.081" x2="4.445" y2="13.16558125" layer="21"/>
<rectangle x1="7.83081875" y1="13.081" x2="11.13281875" y2="13.16558125" layer="21"/>
<rectangle x1="13.75918125" y1="13.081" x2="15.95881875" y2="13.16558125" layer="21"/>
<rectangle x1="17.99081875" y1="13.081" x2="29.67481875" y2="13.16558125" layer="21"/>
<rectangle x1="0.55118125" y1="13.16558125" x2="1.56718125" y2="13.25041875" layer="21"/>
<rectangle x1="3.683" y1="13.16558125" x2="4.445" y2="13.25041875" layer="21"/>
<rectangle x1="7.83081875" y1="13.16558125" x2="10.96518125" y2="13.25041875" layer="21"/>
<rectangle x1="13.75918125" y1="13.16558125" x2="16.129" y2="13.25041875" layer="21"/>
<rectangle x1="17.82318125" y1="13.16558125" x2="29.591" y2="13.25041875" layer="21"/>
<rectangle x1="0.635" y1="13.25041875" x2="1.56718125" y2="13.335" layer="21"/>
<rectangle x1="3.76681875" y1="13.25041875" x2="4.36118125" y2="13.335" layer="21"/>
<rectangle x1="7.91718125" y1="13.25041875" x2="10.71118125" y2="13.335" layer="21"/>
<rectangle x1="13.75918125" y1="13.25041875" x2="16.46681875" y2="13.335" layer="21"/>
<rectangle x1="17.48281875" y1="13.25041875" x2="29.591" y2="13.335" layer="21"/>
<rectangle x1="0.635" y1="13.335" x2="1.651" y2="13.41958125" layer="21"/>
<rectangle x1="3.937" y1="13.335" x2="4.191" y2="13.41958125" layer="21"/>
<rectangle x1="8.08481875" y1="13.335" x2="10.37081875" y2="13.41958125" layer="21"/>
<rectangle x1="13.75918125" y1="13.335" x2="29.50718125" y2="13.41958125" layer="21"/>
<rectangle x1="0.71881875" y1="13.41958125" x2="1.651" y2="13.50441875" layer="21"/>
<rectangle x1="13.75918125" y1="13.41958125" x2="29.50718125" y2="13.50441875" layer="21"/>
<rectangle x1="0.71881875" y1="13.50441875" x2="1.73481875" y2="13.589" layer="21"/>
<rectangle x1="13.843" y1="13.50441875" x2="29.42081875" y2="13.589" layer="21"/>
<rectangle x1="0.80518125" y1="13.589" x2="1.82118125" y2="13.67358125" layer="21"/>
<rectangle x1="13.843" y1="13.589" x2="29.42081875" y2="13.67358125" layer="21"/>
<rectangle x1="0.80518125" y1="13.67358125" x2="1.82118125" y2="13.75841875" layer="21"/>
<rectangle x1="13.843" y1="13.67358125" x2="29.337" y2="13.75841875" layer="21"/>
<rectangle x1="0.889" y1="13.75841875" x2="1.905" y2="13.843" layer="21"/>
<rectangle x1="13.843" y1="13.75841875" x2="29.337" y2="13.843" layer="21"/>
<rectangle x1="0.889" y1="13.843" x2="1.905" y2="13.92758125" layer="21"/>
<rectangle x1="13.92681875" y1="13.843" x2="29.25318125" y2="13.92758125" layer="21"/>
<rectangle x1="0.97281875" y1="13.92758125" x2="1.98881875" y2="14.01241875" layer="21"/>
<rectangle x1="13.92681875" y1="13.92758125" x2="29.25318125" y2="14.01241875" layer="21"/>
<rectangle x1="1.05918125" y1="14.01241875" x2="2.07518125" y2="14.097" layer="21"/>
<rectangle x1="13.92681875" y1="14.01241875" x2="29.16681875" y2="14.097" layer="21"/>
<rectangle x1="1.05918125" y1="14.097" x2="2.159" y2="14.18158125" layer="21"/>
<rectangle x1="13.92681875" y1="14.097" x2="29.083" y2="14.18158125" layer="21"/>
<rectangle x1="1.143" y1="14.18158125" x2="2.24281875" y2="14.26641875" layer="21"/>
<rectangle x1="13.92681875" y1="14.18158125" x2="29.083" y2="14.26641875" layer="21"/>
<rectangle x1="1.143" y1="14.26641875" x2="2.24281875" y2="14.351" layer="21"/>
<rectangle x1="14.01318125" y1="14.26641875" x2="28.99918125" y2="14.351" layer="21"/>
<rectangle x1="1.22681875" y1="14.351" x2="2.32918125" y2="14.43558125" layer="21"/>
<rectangle x1="14.01318125" y1="14.351" x2="28.91281875" y2="14.43558125" layer="21"/>
<rectangle x1="1.31318125" y1="14.43558125" x2="2.413" y2="14.52041875" layer="21"/>
<rectangle x1="14.01318125" y1="14.43558125" x2="28.91281875" y2="14.52041875" layer="21"/>
<rectangle x1="1.397" y1="14.52041875" x2="2.49681875" y2="14.605" layer="21"/>
<rectangle x1="14.097" y1="14.52041875" x2="28.829" y2="14.605" layer="21"/>
<rectangle x1="1.48081875" y1="14.605" x2="2.58318125" y2="14.68958125" layer="21"/>
<rectangle x1="14.097" y1="14.605" x2="28.74518125" y2="14.68958125" layer="21"/>
<rectangle x1="1.48081875" y1="14.68958125" x2="2.667" y2="14.77441875" layer="21"/>
<rectangle x1="14.18081875" y1="14.68958125" x2="28.65881875" y2="14.77441875" layer="21"/>
<rectangle x1="1.56718125" y1="14.77441875" x2="2.83718125" y2="14.859" layer="21"/>
<rectangle x1="14.18081875" y1="14.77441875" x2="28.575" y2="14.859" layer="21"/>
<rectangle x1="1.651" y1="14.859" x2="2.921" y2="14.94358125" layer="21"/>
<rectangle x1="14.18081875" y1="14.859" x2="28.49118125" y2="14.94358125" layer="21"/>
<rectangle x1="1.73481875" y1="14.94358125" x2="3.00481875" y2="15.02841875" layer="21"/>
<rectangle x1="14.26718125" y1="14.94358125" x2="28.49118125" y2="15.02841875" layer="21"/>
<rectangle x1="1.82118125" y1="15.02841875" x2="3.09118125" y2="15.113" layer="21"/>
<rectangle x1="14.26718125" y1="15.02841875" x2="28.40481875" y2="15.113" layer="21"/>
<rectangle x1="1.905" y1="15.113" x2="3.25881875" y2="15.19758125" layer="21"/>
<rectangle x1="14.26718125" y1="15.113" x2="28.321" y2="15.19758125" layer="21"/>
<rectangle x1="1.98881875" y1="15.19758125" x2="3.34518125" y2="15.28241875" layer="21"/>
<rectangle x1="14.351" y1="15.19758125" x2="28.23718125" y2="15.28241875" layer="21"/>
<rectangle x1="2.07518125" y1="15.28241875" x2="3.51281875" y2="15.367" layer="21"/>
<rectangle x1="14.351" y1="15.28241875" x2="28.15081875" y2="15.367" layer="21"/>
<rectangle x1="2.159" y1="15.367" x2="3.683" y2="15.45158125" layer="21"/>
<rectangle x1="14.43481875" y1="15.367" x2="28.067" y2="15.45158125" layer="21"/>
<rectangle x1="2.24281875" y1="15.45158125" x2="3.76681875" y2="15.53641875" layer="21"/>
<rectangle x1="14.43481875" y1="15.45158125" x2="27.89681875" y2="15.53641875" layer="21"/>
<rectangle x1="2.32918125" y1="15.53641875" x2="4.02081875" y2="15.621" layer="21"/>
<rectangle x1="14.52118125" y1="15.53641875" x2="27.813" y2="15.621" layer="21"/>
<rectangle x1="2.49681875" y1="15.621" x2="4.191" y2="15.70558125" layer="21"/>
<rectangle x1="14.52118125" y1="15.621" x2="27.72918125" y2="15.70558125" layer="21"/>
<rectangle x1="2.58318125" y1="15.70558125" x2="4.445" y2="15.79041875" layer="21"/>
<rectangle x1="14.605" y1="15.70558125" x2="27.64281875" y2="15.79041875" layer="21"/>
<rectangle x1="2.75081875" y1="15.79041875" x2="4.61518125" y2="15.875" layer="21"/>
<rectangle x1="14.68881875" y1="15.79041875" x2="27.47518125" y2="15.875" layer="21"/>
<rectangle x1="2.83718125" y1="15.875" x2="5.03681875" y2="15.95958125" layer="21"/>
<rectangle x1="14.68881875" y1="15.875" x2="27.38881875" y2="15.95958125" layer="21"/>
<rectangle x1="3.00481875" y1="15.95958125" x2="5.461" y2="16.04441875" layer="21"/>
<rectangle x1="14.77518125" y1="15.95958125" x2="27.22118125" y2="16.04441875" layer="21"/>
<rectangle x1="3.09118125" y1="16.04441875" x2="27.13481875" y2="16.129" layer="21"/>
<rectangle x1="3.25881875" y1="16.129" x2="26.96718125" y2="16.21358125" layer="21"/>
<rectangle x1="3.429" y1="16.21358125" x2="26.797" y2="16.29841875" layer="21"/>
<rectangle x1="3.59918125" y1="16.29841875" x2="26.62681875" y2="16.383" layer="21"/>
<rectangle x1="3.76681875" y1="16.383" x2="26.45918125" y2="16.46758125" layer="21"/>
<rectangle x1="3.937" y1="16.46758125" x2="26.20518125" y2="16.55241875" layer="21"/>
<rectangle x1="4.191" y1="16.55241875" x2="26.035" y2="16.637" layer="21"/>
<rectangle x1="4.445" y1="16.637" x2="25.69718125" y2="16.72158125" layer="21"/>
<rectangle x1="4.78281875" y1="16.72158125" x2="25.35681875" y2="16.80641875" layer="21"/>
<rectangle x1="5.207" y1="16.80641875" x2="25.019" y2="16.891" layer="21"/>
</package>
<package name="UDO-LOGO-40MM">
<rectangle x1="5.969" y1="0.71958125" x2="6.81481875" y2="0.80441875" layer="21"/>
<rectangle x1="28.321" y1="0.71958125" x2="29.083" y2="0.80441875" layer="21"/>
<rectangle x1="5.715" y1="0.80441875" x2="7.06881875" y2="0.889" layer="21"/>
<rectangle x1="28.067" y1="0.80441875" x2="29.42081875" y2="0.889" layer="21"/>
<rectangle x1="5.54481875" y1="0.889" x2="7.32281875" y2="0.97358125" layer="21"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.017" y2="0.97358125" layer="21"/>
<rectangle x1="11.049" y1="0.889" x2="11.303" y2="0.97358125" layer="21"/>
<rectangle x1="11.811" y1="0.889" x2="11.98118125" y2="0.97358125" layer="21"/>
<rectangle x1="13.843" y1="0.889" x2="14.18081875" y2="0.97358125" layer="21"/>
<rectangle x1="14.94281875" y1="0.889" x2="16.72081875" y2="0.97358125" layer="21"/>
<rectangle x1="19.60118125" y1="0.889" x2="20.10918125" y2="0.97358125" layer="21"/>
<rectangle x1="22.14118125" y1="0.889" x2="22.56281875" y2="0.97358125" layer="21"/>
<rectangle x1="24.59481875" y1="0.889" x2="25.10281875" y2="0.97358125" layer="21"/>
<rectangle x1="27.813" y1="0.889" x2="29.591" y2="0.97358125" layer="21"/>
<rectangle x1="30.94481875" y1="0.889" x2="31.19881875" y2="0.97358125" layer="21"/>
<rectangle x1="33.23081875" y1="0.889" x2="33.401" y2="0.97358125" layer="21"/>
<rectangle x1="34.84118125" y1="0.889" x2="35.34918125" y2="0.97358125" layer="21"/>
<rectangle x1="5.37718125" y1="0.97358125" x2="7.493" y2="1.05841875" layer="21"/>
<rectangle x1="8.763" y1="0.97358125" x2="9.10081875" y2="1.05841875" layer="21"/>
<rectangle x1="10.96518125" y1="0.97358125" x2="11.303" y2="1.05841875" layer="21"/>
<rectangle x1="11.72718125" y1="0.97358125" x2="12.065" y2="1.05841875" layer="21"/>
<rectangle x1="13.589" y1="0.97358125" x2="14.26718125" y2="1.05841875" layer="21"/>
<rectangle x1="14.94281875" y1="0.97358125" x2="16.97481875" y2="1.05841875" layer="21"/>
<rectangle x1="19.34718125" y1="0.97358125" x2="20.36318125" y2="1.05841875" layer="21"/>
<rectangle x1="21.971" y1="0.97358125" x2="22.90318125" y2="1.05841875" layer="21"/>
<rectangle x1="24.34081875" y1="0.97358125" x2="25.44318125" y2="1.05841875" layer="21"/>
<rectangle x1="27.64281875" y1="0.97358125" x2="29.76118125" y2="1.05841875" layer="21"/>
<rectangle x1="30.94481875" y1="0.97358125" x2="31.28518125" y2="1.05841875" layer="21"/>
<rectangle x1="33.147" y1="0.97358125" x2="33.48481875" y2="1.05841875" layer="21"/>
<rectangle x1="34.58718125" y1="0.97358125" x2="35.60318125" y2="1.05841875" layer="21"/>
<rectangle x1="5.29081875" y1="1.05841875" x2="6.05281875" y2="1.143" layer="21"/>
<rectangle x1="6.81481875" y1="1.05841875" x2="7.57681875" y2="1.143" layer="21"/>
<rectangle x1="8.763" y1="1.05841875" x2="9.10081875" y2="1.143" layer="21"/>
<rectangle x1="10.96518125" y1="1.05841875" x2="11.303" y2="1.143" layer="21"/>
<rectangle x1="11.72718125" y1="1.05841875" x2="12.065" y2="1.143" layer="21"/>
<rectangle x1="13.41881875" y1="1.05841875" x2="14.26718125" y2="1.143" layer="21"/>
<rectangle x1="14.859" y1="1.05841875" x2="17.22881875" y2="1.143" layer="21"/>
<rectangle x1="19.177" y1="1.05841875" x2="20.53081875" y2="1.143" layer="21"/>
<rectangle x1="21.80081875" y1="1.05841875" x2="23.07081875" y2="1.143" layer="21"/>
<rectangle x1="24.17318125" y1="1.05841875" x2="25.61081875" y2="1.143" layer="21"/>
<rectangle x1="27.559" y1="1.05841875" x2="28.40481875" y2="1.143" layer="21"/>
<rectangle x1="29.083" y1="1.05841875" x2="29.845" y2="1.143" layer="21"/>
<rectangle x1="30.94481875" y1="1.05841875" x2="31.28518125" y2="1.143" layer="21"/>
<rectangle x1="33.147" y1="1.05841875" x2="33.48481875" y2="1.143" layer="21"/>
<rectangle x1="34.417" y1="1.05841875" x2="35.77081875" y2="1.143" layer="21"/>
<rectangle x1="5.207" y1="1.143" x2="5.79881875" y2="1.22758125" layer="21"/>
<rectangle x1="6.985" y1="1.143" x2="7.66318125" y2="1.22758125" layer="21"/>
<rectangle x1="8.763" y1="1.143" x2="9.10081875" y2="1.22758125" layer="21"/>
<rectangle x1="10.96518125" y1="1.143" x2="11.303" y2="1.22758125" layer="21"/>
<rectangle x1="11.72718125" y1="1.143" x2="12.065" y2="1.22758125" layer="21"/>
<rectangle x1="13.25118125" y1="1.143" x2="14.26718125" y2="1.22758125" layer="21"/>
<rectangle x1="14.859" y1="1.143" x2="17.31518125" y2="1.22758125" layer="21"/>
<rectangle x1="19.09318125" y1="1.143" x2="20.701" y2="1.22758125" layer="21"/>
<rectangle x1="21.717" y1="1.143" x2="23.07081875" y2="1.22758125" layer="21"/>
<rectangle x1="24.08681875" y1="1.143" x2="25.69718125" y2="1.22758125" layer="21"/>
<rectangle x1="27.47518125" y1="1.143" x2="28.067" y2="1.22758125" layer="21"/>
<rectangle x1="29.337" y1="1.143" x2="29.92881875" y2="1.22758125" layer="21"/>
<rectangle x1="30.94481875" y1="1.143" x2="31.28518125" y2="1.22758125" layer="21"/>
<rectangle x1="33.147" y1="1.143" x2="33.48481875" y2="1.22758125" layer="21"/>
<rectangle x1="34.33318125" y1="1.143" x2="35.941" y2="1.22758125" layer="21"/>
<rectangle x1="5.03681875" y1="1.22758125" x2="5.63118125" y2="1.31241875" layer="21"/>
<rectangle x1="7.239" y1="1.22758125" x2="7.747" y2="1.31241875" layer="21"/>
<rectangle x1="8.763" y1="1.22758125" x2="9.10081875" y2="1.31241875" layer="21"/>
<rectangle x1="10.96518125" y1="1.22758125" x2="11.303" y2="1.31241875" layer="21"/>
<rectangle x1="11.72718125" y1="1.22758125" x2="12.065" y2="1.31241875" layer="21"/>
<rectangle x1="13.16481875" y1="1.22758125" x2="13.843" y2="1.31241875" layer="21"/>
<rectangle x1="14.859" y1="1.22758125" x2="15.28318125" y2="1.31241875" layer="21"/>
<rectangle x1="16.637" y1="1.22758125" x2="17.48281875" y2="1.31241875" layer="21"/>
<rectangle x1="18.923" y1="1.22758125" x2="19.685" y2="1.31241875" layer="21"/>
<rectangle x1="20.02281875" y1="1.22758125" x2="20.78481875" y2="1.31241875" layer="21"/>
<rectangle x1="21.63318125" y1="1.22758125" x2="22.225" y2="1.31241875" layer="21"/>
<rectangle x1="22.56281875" y1="1.22758125" x2="23.15718125" y2="1.31241875" layer="21"/>
<rectangle x1="24.003" y1="1.22758125" x2="24.68118125" y2="1.31241875" layer="21"/>
<rectangle x1="25.10281875" y1="1.22758125" x2="25.781" y2="1.31241875" layer="21"/>
<rectangle x1="27.38881875" y1="1.22758125" x2="27.98318125" y2="1.31241875" layer="21"/>
<rectangle x1="29.50718125" y1="1.22758125" x2="30.01518125" y2="1.31241875" layer="21"/>
<rectangle x1="30.94481875" y1="1.22758125" x2="31.28518125" y2="1.31241875" layer="21"/>
<rectangle x1="33.147" y1="1.22758125" x2="33.48481875" y2="1.31241875" layer="21"/>
<rectangle x1="34.163" y1="1.22758125" x2="34.925" y2="1.31241875" layer="21"/>
<rectangle x1="35.26281875" y1="1.22758125" x2="36.02481875" y2="1.31241875" layer="21"/>
<rectangle x1="5.03681875" y1="1.31241875" x2="5.54481875" y2="1.397" layer="21"/>
<rectangle x1="7.32281875" y1="1.31241875" x2="7.83081875" y2="1.397" layer="21"/>
<rectangle x1="8.763" y1="1.31241875" x2="9.10081875" y2="1.397" layer="21"/>
<rectangle x1="10.96518125" y1="1.31241875" x2="11.303" y2="1.397" layer="21"/>
<rectangle x1="11.72718125" y1="1.31241875" x2="12.065" y2="1.397" layer="21"/>
<rectangle x1="13.081" y1="1.31241875" x2="13.67281875" y2="1.397" layer="21"/>
<rectangle x1="14.94281875" y1="1.31241875" x2="15.19681875" y2="1.397" layer="21"/>
<rectangle x1="16.891" y1="1.31241875" x2="17.56918125" y2="1.397" layer="21"/>
<rectangle x1="18.923" y1="1.31241875" x2="19.431" y2="1.397" layer="21"/>
<rectangle x1="20.27681875" y1="1.31241875" x2="20.87118125" y2="1.397" layer="21"/>
<rectangle x1="21.54681875" y1="1.31241875" x2="22.05481875" y2="1.397" layer="21"/>
<rectangle x1="22.733" y1="1.31241875" x2="23.241" y2="1.397" layer="21"/>
<rectangle x1="23.91918125" y1="1.31241875" x2="24.42718125" y2="1.397" layer="21"/>
<rectangle x1="25.273" y1="1.31241875" x2="25.86481875" y2="1.397" layer="21"/>
<rectangle x1="27.305" y1="1.31241875" x2="27.813" y2="1.397" layer="21"/>
<rectangle x1="29.591" y1="1.31241875" x2="30.099" y2="1.397" layer="21"/>
<rectangle x1="30.94481875" y1="1.31241875" x2="31.28518125" y2="1.397" layer="21"/>
<rectangle x1="33.147" y1="1.31241875" x2="33.48481875" y2="1.397" layer="21"/>
<rectangle x1="34.163" y1="1.31241875" x2="34.671" y2="1.397" layer="21"/>
<rectangle x1="35.51681875" y1="1.31241875" x2="36.11118125" y2="1.397" layer="21"/>
<rectangle x1="4.953" y1="1.397" x2="5.37718125" y2="1.48158125" layer="21"/>
<rectangle x1="7.40918125" y1="1.397" x2="7.91718125" y2="1.48158125" layer="21"/>
<rectangle x1="8.763" y1="1.397" x2="9.10081875" y2="1.48158125" layer="21"/>
<rectangle x1="10.96518125" y1="1.397" x2="11.303" y2="1.48158125" layer="21"/>
<rectangle x1="11.72718125" y1="1.397" x2="12.065" y2="1.48158125" layer="21"/>
<rectangle x1="13.081" y1="1.397" x2="13.50518125" y2="1.48158125" layer="21"/>
<rectangle x1="14.859" y1="1.397" x2="15.19681875" y2="1.48158125" layer="21"/>
<rectangle x1="17.06118125" y1="1.397" x2="17.653" y2="1.48158125" layer="21"/>
<rectangle x1="18.83918125" y1="1.397" x2="19.26081875" y2="1.48158125" layer="21"/>
<rectangle x1="20.447" y1="1.397" x2="20.87118125" y2="1.48158125" layer="21"/>
<rectangle x1="21.54681875" y1="1.397" x2="21.971" y2="1.48158125" layer="21"/>
<rectangle x1="22.90318125" y1="1.397" x2="23.241" y2="1.48158125" layer="21"/>
<rectangle x1="23.83281875" y1="1.397" x2="24.34081875" y2="1.48158125" layer="21"/>
<rectangle x1="25.44318125" y1="1.397" x2="25.95118125" y2="1.48158125" layer="21"/>
<rectangle x1="27.22118125" y1="1.397" x2="27.72918125" y2="1.48158125" layer="21"/>
<rectangle x1="29.76118125" y1="1.397" x2="30.18281875" y2="1.48158125" layer="21"/>
<rectangle x1="30.94481875" y1="1.397" x2="31.28518125" y2="1.48158125" layer="21"/>
<rectangle x1="33.147" y1="1.397" x2="33.48481875" y2="1.48158125" layer="21"/>
<rectangle x1="34.07918125" y1="1.397" x2="34.50081875" y2="1.48158125" layer="21"/>
<rectangle x1="35.687" y1="1.397" x2="36.11118125" y2="1.48158125" layer="21"/>
<rectangle x1="4.86918125" y1="1.48158125" x2="5.29081875" y2="1.56641875" layer="21"/>
<rectangle x1="7.57681875" y1="1.48158125" x2="8.001" y2="1.56641875" layer="21"/>
<rectangle x1="8.763" y1="1.48158125" x2="9.10081875" y2="1.56641875" layer="21"/>
<rectangle x1="10.96518125" y1="1.48158125" x2="11.303" y2="1.56641875" layer="21"/>
<rectangle x1="11.72718125" y1="1.48158125" x2="12.065" y2="1.56641875" layer="21"/>
<rectangle x1="12.99718125" y1="1.48158125" x2="13.41881875" y2="1.56641875" layer="21"/>
<rectangle x1="14.859" y1="1.48158125" x2="15.28318125" y2="1.56641875" layer="21"/>
<rectangle x1="17.22881875" y1="1.48158125" x2="17.73681875" y2="1.56641875" layer="21"/>
<rectangle x1="18.75281875" y1="1.48158125" x2="19.177" y2="1.56641875" layer="21"/>
<rectangle x1="20.53081875" y1="1.48158125" x2="20.955" y2="1.56641875" layer="21"/>
<rectangle x1="21.54681875" y1="1.48158125" x2="21.88718125" y2="1.56641875" layer="21"/>
<rectangle x1="22.90318125" y1="1.48158125" x2="23.241" y2="1.56641875" layer="21"/>
<rectangle x1="23.749" y1="1.48158125" x2="24.257" y2="1.56641875" layer="21"/>
<rectangle x1="25.527" y1="1.48158125" x2="25.95118125" y2="1.56641875" layer="21"/>
<rectangle x1="27.13481875" y1="1.48158125" x2="27.64281875" y2="1.56641875" layer="21"/>
<rectangle x1="29.845" y1="1.48158125" x2="30.26918125" y2="1.56641875" layer="21"/>
<rectangle x1="30.94481875" y1="1.48158125" x2="31.28518125" y2="1.56641875" layer="21"/>
<rectangle x1="33.147" y1="1.48158125" x2="33.48481875" y2="1.56641875" layer="21"/>
<rectangle x1="33.99281875" y1="1.48158125" x2="34.417" y2="1.56641875" layer="21"/>
<rectangle x1="35.77081875" y1="1.48158125" x2="36.195" y2="1.56641875" layer="21"/>
<rectangle x1="4.78281875" y1="1.56641875" x2="5.207" y2="1.651" layer="21"/>
<rectangle x1="7.57681875" y1="1.56641875" x2="8.001" y2="1.651" layer="21"/>
<rectangle x1="8.763" y1="1.56641875" x2="9.10081875" y2="1.651" layer="21"/>
<rectangle x1="10.96518125" y1="1.56641875" x2="11.303" y2="1.651" layer="21"/>
<rectangle x1="11.72718125" y1="1.56641875" x2="12.065" y2="1.651" layer="21"/>
<rectangle x1="12.91081875" y1="1.56641875" x2="13.335" y2="1.651" layer="21"/>
<rectangle x1="14.859" y1="1.56641875" x2="15.28318125" y2="1.651" layer="21"/>
<rectangle x1="17.31518125" y1="1.56641875" x2="17.82318125" y2="1.651" layer="21"/>
<rectangle x1="18.669" y1="1.56641875" x2="19.09318125" y2="1.651" layer="21"/>
<rectangle x1="20.61718125" y1="1.56641875" x2="21.03881875" y2="1.651" layer="21"/>
<rectangle x1="21.54681875" y1="1.56641875" x2="21.80081875" y2="1.651" layer="21"/>
<rectangle x1="22.987" y1="1.56641875" x2="23.241" y2="1.651" layer="21"/>
<rectangle x1="23.749" y1="1.56641875" x2="24.17318125" y2="1.651" layer="21"/>
<rectangle x1="25.61081875" y1="1.56641875" x2="26.035" y2="1.651" layer="21"/>
<rectangle x1="27.13481875" y1="1.56641875" x2="27.47518125" y2="1.651" layer="21"/>
<rectangle x1="29.92881875" y1="1.56641875" x2="30.353" y2="1.651" layer="21"/>
<rectangle x1="30.94481875" y1="1.56641875" x2="31.28518125" y2="1.651" layer="21"/>
<rectangle x1="33.147" y1="1.56641875" x2="33.48481875" y2="1.651" layer="21"/>
<rectangle x1="33.99281875" y1="1.56641875" x2="34.33318125" y2="1.651" layer="21"/>
<rectangle x1="35.85718125" y1="1.56641875" x2="36.27881875" y2="1.651" layer="21"/>
<rectangle x1="4.78281875" y1="1.651" x2="5.207" y2="1.73558125" layer="21"/>
<rectangle x1="7.66318125" y1="1.651" x2="8.08481875" y2="1.73558125" layer="21"/>
<rectangle x1="8.763" y1="1.651" x2="9.10081875" y2="1.73558125" layer="21"/>
<rectangle x1="10.96518125" y1="1.651" x2="11.303" y2="1.73558125" layer="21"/>
<rectangle x1="11.72718125" y1="1.651" x2="12.065" y2="1.73558125" layer="21"/>
<rectangle x1="12.91081875" y1="1.651" x2="13.25118125" y2="1.73558125" layer="21"/>
<rectangle x1="14.859" y1="1.651" x2="15.19681875" y2="1.73558125" layer="21"/>
<rectangle x1="17.48281875" y1="1.651" x2="17.907" y2="1.73558125" layer="21"/>
<rectangle x1="18.669" y1="1.651" x2="19.09318125" y2="1.73558125" layer="21"/>
<rectangle x1="20.701" y1="1.651" x2="21.03881875" y2="1.73558125" layer="21"/>
<rectangle x1="22.90318125" y1="1.651" x2="23.241" y2="1.73558125" layer="21"/>
<rectangle x1="23.66518125" y1="1.651" x2="24.08681875" y2="1.73558125" layer="21"/>
<rectangle x1="25.69718125" y1="1.651" x2="26.035" y2="1.73558125" layer="21"/>
<rectangle x1="27.051" y1="1.651" x2="27.47518125" y2="1.73558125" layer="21"/>
<rectangle x1="30.01518125" y1="1.651" x2="30.353" y2="1.73558125" layer="21"/>
<rectangle x1="30.94481875" y1="1.651" x2="31.28518125" y2="1.73558125" layer="21"/>
<rectangle x1="33.147" y1="1.651" x2="33.48481875" y2="1.73558125" layer="21"/>
<rectangle x1="33.909" y1="1.651" x2="34.33318125" y2="1.73558125" layer="21"/>
<rectangle x1="35.941" y1="1.651" x2="36.27881875" y2="1.73558125" layer="21"/>
<rectangle x1="4.699" y1="1.73558125" x2="5.12318125" y2="1.82041875" layer="21"/>
<rectangle x1="7.747" y1="1.73558125" x2="8.17118125" y2="1.82041875" layer="21"/>
<rectangle x1="8.763" y1="1.73558125" x2="9.10081875" y2="1.82041875" layer="21"/>
<rectangle x1="10.96518125" y1="1.73558125" x2="11.303" y2="1.82041875" layer="21"/>
<rectangle x1="11.72718125" y1="1.73558125" x2="12.065" y2="1.82041875" layer="21"/>
<rectangle x1="12.91081875" y1="1.73558125" x2="13.25118125" y2="1.82041875" layer="21"/>
<rectangle x1="14.859" y1="1.73558125" x2="15.28318125" y2="1.82041875" layer="21"/>
<rectangle x1="17.56918125" y1="1.73558125" x2="17.99081875" y2="1.82041875" layer="21"/>
<rectangle x1="18.669" y1="1.73558125" x2="19.00681875" y2="1.82041875" layer="21"/>
<rectangle x1="20.701" y1="1.73558125" x2="21.12518125" y2="1.82041875" layer="21"/>
<rectangle x1="22.90318125" y1="1.73558125" x2="23.241" y2="1.82041875" layer="21"/>
<rectangle x1="23.66518125" y1="1.73558125" x2="24.003" y2="1.82041875" layer="21"/>
<rectangle x1="25.781" y1="1.73558125" x2="25.95118125" y2="1.82041875" layer="21"/>
<rectangle x1="26.96718125" y1="1.73558125" x2="27.38881875" y2="1.82041875" layer="21"/>
<rectangle x1="30.01518125" y1="1.73558125" x2="30.43681875" y2="1.82041875" layer="21"/>
<rectangle x1="30.94481875" y1="1.73558125" x2="31.28518125" y2="1.82041875" layer="21"/>
<rectangle x1="33.147" y1="1.73558125" x2="33.48481875" y2="1.82041875" layer="21"/>
<rectangle x1="33.909" y1="1.73558125" x2="34.24681875" y2="1.82041875" layer="21"/>
<rectangle x1="35.941" y1="1.73558125" x2="36.195" y2="1.82041875" layer="21"/>
<rectangle x1="4.699" y1="1.82041875" x2="5.03681875" y2="1.905" layer="21"/>
<rectangle x1="7.83081875" y1="1.82041875" x2="8.17118125" y2="1.905" layer="21"/>
<rectangle x1="8.763" y1="1.82041875" x2="9.10081875" y2="1.905" layer="21"/>
<rectangle x1="10.96518125" y1="1.82041875" x2="11.303" y2="1.905" layer="21"/>
<rectangle x1="11.72718125" y1="1.82041875" x2="12.065" y2="1.905" layer="21"/>
<rectangle x1="12.827" y1="1.82041875" x2="13.16481875" y2="1.905" layer="21"/>
<rectangle x1="14.859" y1="1.82041875" x2="15.28318125" y2="1.905" layer="21"/>
<rectangle x1="17.56918125" y1="1.82041875" x2="17.99081875" y2="1.905" layer="21"/>
<rectangle x1="18.58518125" y1="1.82041875" x2="19.00681875" y2="1.905" layer="21"/>
<rectangle x1="20.78481875" y1="1.82041875" x2="21.12518125" y2="1.905" layer="21"/>
<rectangle x1="22.733" y1="1.82041875" x2="23.241" y2="1.905" layer="21"/>
<rectangle x1="23.66518125" y1="1.82041875" x2="24.003" y2="1.905" layer="21"/>
<rectangle x1="26.96718125" y1="1.82041875" x2="27.305" y2="1.905" layer="21"/>
<rectangle x1="30.099" y1="1.82041875" x2="30.43681875" y2="1.905" layer="21"/>
<rectangle x1="30.94481875" y1="1.82041875" x2="31.28518125" y2="1.905" layer="21"/>
<rectangle x1="33.147" y1="1.82041875" x2="33.48481875" y2="1.905" layer="21"/>
<rectangle x1="33.82518125" y1="1.82041875" x2="34.163" y2="1.905" layer="21"/>
<rectangle x1="4.61518125" y1="1.905" x2="5.03681875" y2="1.98958125" layer="21"/>
<rectangle x1="7.83081875" y1="1.905" x2="8.255" y2="1.98958125" layer="21"/>
<rectangle x1="8.763" y1="1.905" x2="9.10081875" y2="1.98958125" layer="21"/>
<rectangle x1="10.96518125" y1="1.905" x2="11.303" y2="1.98958125" layer="21"/>
<rectangle x1="11.72718125" y1="1.905" x2="12.065" y2="1.98958125" layer="21"/>
<rectangle x1="12.827" y1="1.905" x2="13.16481875" y2="1.98958125" layer="21"/>
<rectangle x1="14.859" y1="1.905" x2="15.28318125" y2="1.98958125" layer="21"/>
<rectangle x1="17.653" y1="1.905" x2="18.07718125" y2="1.98958125" layer="21"/>
<rectangle x1="18.58518125" y1="1.905" x2="18.923" y2="1.98958125" layer="21"/>
<rectangle x1="20.78481875" y1="1.905" x2="21.12518125" y2="1.98958125" layer="21"/>
<rectangle x1="22.64918125" y1="1.905" x2="23.15718125" y2="1.98958125" layer="21"/>
<rectangle x1="23.57881875" y1="1.905" x2="23.91918125" y2="1.98958125" layer="21"/>
<rectangle x1="26.96718125" y1="1.905" x2="27.305" y2="1.98958125" layer="21"/>
<rectangle x1="30.099" y1="1.905" x2="30.52318125" y2="1.98958125" layer="21"/>
<rectangle x1="30.94481875" y1="1.905" x2="31.28518125" y2="1.98958125" layer="21"/>
<rectangle x1="33.147" y1="1.905" x2="33.48481875" y2="1.98958125" layer="21"/>
<rectangle x1="33.82518125" y1="1.905" x2="34.163" y2="1.98958125" layer="21"/>
<rectangle x1="4.61518125" y1="1.98958125" x2="4.953" y2="2.07441875" layer="21"/>
<rectangle x1="7.91718125" y1="1.98958125" x2="8.255" y2="2.07441875" layer="21"/>
<rectangle x1="8.763" y1="1.98958125" x2="9.10081875" y2="2.07441875" layer="21"/>
<rectangle x1="10.96518125" y1="1.98958125" x2="11.303" y2="2.07441875" layer="21"/>
<rectangle x1="11.72718125" y1="1.98958125" x2="12.065" y2="2.07441875" layer="21"/>
<rectangle x1="12.827" y1="1.98958125" x2="13.16481875" y2="2.07441875" layer="21"/>
<rectangle x1="14.859" y1="1.98958125" x2="15.19681875" y2="2.07441875" layer="21"/>
<rectangle x1="17.73681875" y1="1.98958125" x2="18.07718125" y2="2.07441875" layer="21"/>
<rectangle x1="18.58518125" y1="1.98958125" x2="18.923" y2="2.07441875" layer="21"/>
<rectangle x1="20.78481875" y1="1.98958125" x2="21.12518125" y2="2.07441875" layer="21"/>
<rectangle x1="22.39518125" y1="1.98958125" x2="23.07081875" y2="2.07441875" layer="21"/>
<rectangle x1="23.57881875" y1="1.98958125" x2="25.86481875" y2="2.07441875" layer="21"/>
<rectangle x1="26.88081875" y1="1.98958125" x2="27.22118125" y2="2.07441875" layer="21"/>
<rectangle x1="30.18281875" y1="1.98958125" x2="30.52318125" y2="2.07441875" layer="21"/>
<rectangle x1="30.94481875" y1="1.98958125" x2="31.28518125" y2="2.07441875" layer="21"/>
<rectangle x1="33.147" y1="1.98958125" x2="33.48481875" y2="2.07441875" layer="21"/>
<rectangle x1="33.82518125" y1="1.98958125" x2="34.163" y2="2.07441875" layer="21"/>
<rectangle x1="34.33318125" y1="1.98958125" x2="35.941" y2="2.07441875" layer="21"/>
<rectangle x1="36.02481875" y1="1.98958125" x2="36.27881875" y2="2.07441875" layer="21"/>
<rectangle x1="4.61518125" y1="2.07441875" x2="4.953" y2="2.159" layer="21"/>
<rectangle x1="7.91718125" y1="2.07441875" x2="8.255" y2="2.159" layer="21"/>
<rectangle x1="8.763" y1="2.07441875" x2="9.10081875" y2="2.159" layer="21"/>
<rectangle x1="10.96518125" y1="2.07441875" x2="11.303" y2="2.159" layer="21"/>
<rectangle x1="11.72718125" y1="2.07441875" x2="12.065" y2="2.159" layer="21"/>
<rectangle x1="12.827" y1="2.07441875" x2="13.16481875" y2="2.159" layer="21"/>
<rectangle x1="14.859" y1="2.07441875" x2="15.19681875" y2="2.159" layer="21"/>
<rectangle x1="17.73681875" y1="2.07441875" x2="18.161" y2="2.159" layer="21"/>
<rectangle x1="18.58518125" y1="2.07441875" x2="18.923" y2="2.159" layer="21"/>
<rectangle x1="20.78481875" y1="2.07441875" x2="21.12518125" y2="2.159" layer="21"/>
<rectangle x1="22.14118125" y1="2.07441875" x2="22.987" y2="2.159" layer="21"/>
<rectangle x1="23.57881875" y1="2.07441875" x2="26.11881875" y2="2.159" layer="21"/>
<rectangle x1="26.88081875" y1="2.07441875" x2="27.22118125" y2="2.159" layer="21"/>
<rectangle x1="30.18281875" y1="2.07441875" x2="30.52318125" y2="2.159" layer="21"/>
<rectangle x1="30.94481875" y1="2.07441875" x2="31.28518125" y2="2.159" layer="21"/>
<rectangle x1="33.147" y1="2.07441875" x2="33.48481875" y2="2.159" layer="21"/>
<rectangle x1="33.82518125" y1="2.07441875" x2="36.36518125" y2="2.159" layer="21"/>
<rectangle x1="4.52881875" y1="2.159" x2="4.953" y2="2.24358125" layer="21"/>
<rectangle x1="7.91718125" y1="2.159" x2="8.255" y2="2.24358125" layer="21"/>
<rectangle x1="8.763" y1="2.159" x2="9.10081875" y2="2.24358125" layer="21"/>
<rectangle x1="10.96518125" y1="2.159" x2="11.303" y2="2.24358125" layer="21"/>
<rectangle x1="11.72718125" y1="2.159" x2="12.065" y2="2.24358125" layer="21"/>
<rectangle x1="12.827" y1="2.159" x2="13.16481875" y2="2.24358125" layer="21"/>
<rectangle x1="14.859" y1="2.159" x2="15.19681875" y2="2.24358125" layer="21"/>
<rectangle x1="17.82318125" y1="2.159" x2="18.161" y2="2.24358125" layer="21"/>
<rectangle x1="18.58518125" y1="2.159" x2="18.923" y2="2.24358125" layer="21"/>
<rectangle x1="20.78481875" y1="2.159" x2="21.12518125" y2="2.24358125" layer="21"/>
<rectangle x1="21.971" y1="2.159" x2="22.90318125" y2="2.24358125" layer="21"/>
<rectangle x1="23.57881875" y1="2.159" x2="26.20518125" y2="2.24358125" layer="21"/>
<rectangle x1="26.88081875" y1="2.159" x2="27.22118125" y2="2.24358125" layer="21"/>
<rectangle x1="30.18281875" y1="2.159" x2="30.607" y2="2.24358125" layer="21"/>
<rectangle x1="30.94481875" y1="2.159" x2="31.28518125" y2="2.24358125" layer="21"/>
<rectangle x1="33.147" y1="2.159" x2="33.48481875" y2="2.24358125" layer="21"/>
<rectangle x1="33.82518125" y1="2.159" x2="36.36518125" y2="2.24358125" layer="21"/>
<rectangle x1="4.52881875" y1="2.24358125" x2="4.86918125" y2="2.32841875" layer="21"/>
<rectangle x1="7.91718125" y1="2.24358125" x2="8.33881875" y2="2.32841875" layer="21"/>
<rectangle x1="8.763" y1="2.24358125" x2="9.10081875" y2="2.32841875" layer="21"/>
<rectangle x1="10.96518125" y1="2.24358125" x2="11.303" y2="2.32841875" layer="21"/>
<rectangle x1="11.72718125" y1="2.24358125" x2="12.065" y2="2.32841875" layer="21"/>
<rectangle x1="12.827" y1="2.24358125" x2="13.16481875" y2="2.32841875" layer="21"/>
<rectangle x1="14.859" y1="2.24358125" x2="15.19681875" y2="2.32841875" layer="21"/>
<rectangle x1="17.82318125" y1="2.24358125" x2="18.161" y2="2.32841875" layer="21"/>
<rectangle x1="18.58518125" y1="2.24358125" x2="18.923" y2="2.32841875" layer="21"/>
<rectangle x1="20.78481875" y1="2.24358125" x2="21.12518125" y2="2.32841875" layer="21"/>
<rectangle x1="21.717" y1="2.24358125" x2="22.64918125" y2="2.32841875" layer="21"/>
<rectangle x1="23.57881875" y1="2.24358125" x2="26.20518125" y2="2.32841875" layer="21"/>
<rectangle x1="26.797" y1="2.24358125" x2="27.22118125" y2="2.32841875" layer="21"/>
<rectangle x1="30.26918125" y1="2.24358125" x2="30.607" y2="2.32841875" layer="21"/>
<rectangle x1="30.94481875" y1="2.24358125" x2="31.28518125" y2="2.32841875" layer="21"/>
<rectangle x1="33.147" y1="2.24358125" x2="33.48481875" y2="2.32841875" layer="21"/>
<rectangle x1="33.82518125" y1="2.24358125" x2="36.36518125" y2="2.32841875" layer="21"/>
<rectangle x1="4.52881875" y1="2.32841875" x2="4.86918125" y2="2.413" layer="21"/>
<rectangle x1="8.001" y1="2.32841875" x2="8.33881875" y2="2.413" layer="21"/>
<rectangle x1="8.763" y1="2.32841875" x2="9.10081875" y2="2.413" layer="21"/>
<rectangle x1="10.96518125" y1="2.32841875" x2="11.303" y2="2.413" layer="21"/>
<rectangle x1="11.72718125" y1="2.32841875" x2="12.065" y2="2.413" layer="21"/>
<rectangle x1="12.827" y1="2.32841875" x2="13.16481875" y2="2.413" layer="21"/>
<rectangle x1="14.859" y1="2.32841875" x2="15.19681875" y2="2.413" layer="21"/>
<rectangle x1="17.82318125" y1="2.32841875" x2="18.24481875" y2="2.413" layer="21"/>
<rectangle x1="18.58518125" y1="2.32841875" x2="18.923" y2="2.413" layer="21"/>
<rectangle x1="20.78481875" y1="2.32841875" x2="21.12518125" y2="2.413" layer="21"/>
<rectangle x1="21.63318125" y1="2.32841875" x2="22.479" y2="2.413" layer="21"/>
<rectangle x1="23.57881875" y1="2.32841875" x2="24.257" y2="2.413" layer="21"/>
<rectangle x1="24.34081875" y1="2.32841875" x2="26.20518125" y2="2.413" layer="21"/>
<rectangle x1="26.797" y1="2.32841875" x2="27.13481875" y2="2.413" layer="21"/>
<rectangle x1="30.26918125" y1="2.32841875" x2="30.607" y2="2.413" layer="21"/>
<rectangle x1="30.94481875" y1="2.32841875" x2="31.28518125" y2="2.413" layer="21"/>
<rectangle x1="33.147" y1="2.32841875" x2="33.48481875" y2="2.413" layer="21"/>
<rectangle x1="33.82518125" y1="2.32841875" x2="35.179" y2="2.413" layer="21"/>
<rectangle x1="35.34918125" y1="2.32841875" x2="35.60318125" y2="2.413" layer="21"/>
<rectangle x1="35.77081875" y1="2.32841875" x2="35.941" y2="2.413" layer="21"/>
<rectangle x1="36.02481875" y1="2.32841875" x2="36.36518125" y2="2.413" layer="21"/>
<rectangle x1="4.52881875" y1="2.413" x2="4.86918125" y2="2.49758125" layer="21"/>
<rectangle x1="8.001" y1="2.413" x2="8.33881875" y2="2.49758125" layer="21"/>
<rectangle x1="8.763" y1="2.413" x2="9.10081875" y2="2.49758125" layer="21"/>
<rectangle x1="10.96518125" y1="2.413" x2="11.303" y2="2.49758125" layer="21"/>
<rectangle x1="11.72718125" y1="2.413" x2="12.065" y2="2.49758125" layer="21"/>
<rectangle x1="12.827" y1="2.413" x2="13.16481875" y2="2.49758125" layer="21"/>
<rectangle x1="14.859" y1="2.413" x2="15.19681875" y2="2.49758125" layer="21"/>
<rectangle x1="17.907" y1="2.413" x2="18.24481875" y2="2.49758125" layer="21"/>
<rectangle x1="18.58518125" y1="2.413" x2="18.923" y2="2.49758125" layer="21"/>
<rectangle x1="20.78481875" y1="2.413" x2="21.12518125" y2="2.49758125" layer="21"/>
<rectangle x1="21.54681875" y1="2.413" x2="22.225" y2="2.49758125" layer="21"/>
<rectangle x1="23.57881875" y1="2.413" x2="23.91918125" y2="2.49758125" layer="21"/>
<rectangle x1="25.781" y1="2.413" x2="26.11881875" y2="2.49758125" layer="21"/>
<rectangle x1="26.797" y1="2.413" x2="27.13481875" y2="2.49758125" layer="21"/>
<rectangle x1="30.26918125" y1="2.413" x2="30.607" y2="2.49758125" layer="21"/>
<rectangle x1="30.94481875" y1="2.413" x2="31.28518125" y2="2.49758125" layer="21"/>
<rectangle x1="33.147" y1="2.413" x2="33.48481875" y2="2.49758125" layer="21"/>
<rectangle x1="33.82518125" y1="2.413" x2="34.163" y2="2.49758125" layer="21"/>
<rectangle x1="36.02481875" y1="2.413" x2="36.36518125" y2="2.49758125" layer="21"/>
<rectangle x1="4.52881875" y1="2.49758125" x2="4.86918125" y2="2.58241875" layer="21"/>
<rectangle x1="8.001" y1="2.49758125" x2="8.33881875" y2="2.58241875" layer="21"/>
<rectangle x1="8.763" y1="2.49758125" x2="9.18718125" y2="2.58241875" layer="21"/>
<rectangle x1="10.96518125" y1="2.49758125" x2="11.303" y2="2.58241875" layer="21"/>
<rectangle x1="11.72718125" y1="2.49758125" x2="12.065" y2="2.58241875" layer="21"/>
<rectangle x1="12.827" y1="2.49758125" x2="13.16481875" y2="2.58241875" layer="21"/>
<rectangle x1="14.859" y1="2.49758125" x2="15.19681875" y2="2.58241875" layer="21"/>
<rectangle x1="17.907" y1="2.49758125" x2="18.24481875" y2="2.58241875" layer="21"/>
<rectangle x1="18.58518125" y1="2.49758125" x2="19.00681875" y2="2.58241875" layer="21"/>
<rectangle x1="20.78481875" y1="2.49758125" x2="21.12518125" y2="2.58241875" layer="21"/>
<rectangle x1="21.463" y1="2.49758125" x2="21.971" y2="2.58241875" layer="21"/>
<rectangle x1="23.66518125" y1="2.49758125" x2="24.003" y2="2.58241875" layer="21"/>
<rectangle x1="25.781" y1="2.49758125" x2="26.11881875" y2="2.58241875" layer="21"/>
<rectangle x1="26.797" y1="2.49758125" x2="27.13481875" y2="2.58241875" layer="21"/>
<rectangle x1="30.26918125" y1="2.49758125" x2="30.607" y2="2.58241875" layer="21"/>
<rectangle x1="30.94481875" y1="2.49758125" x2="31.369" y2="2.58241875" layer="21"/>
<rectangle x1="33.06318125" y1="2.49758125" x2="33.48481875" y2="2.58241875" layer="21"/>
<rectangle x1="33.82518125" y1="2.49758125" x2="34.24681875" y2="2.58241875" layer="21"/>
<rectangle x1="36.02481875" y1="2.49758125" x2="36.36518125" y2="2.58241875" layer="21"/>
<rectangle x1="4.52881875" y1="2.58241875" x2="4.86918125" y2="2.667" layer="21"/>
<rectangle x1="8.001" y1="2.58241875" x2="8.33881875" y2="2.667" layer="21"/>
<rectangle x1="8.763" y1="2.58241875" x2="9.18718125" y2="2.667" layer="21"/>
<rectangle x1="10.87881875" y1="2.58241875" x2="11.303" y2="2.667" layer="21"/>
<rectangle x1="11.72718125" y1="2.58241875" x2="12.065" y2="2.667" layer="21"/>
<rectangle x1="12.827" y1="2.58241875" x2="13.16481875" y2="2.667" layer="21"/>
<rectangle x1="14.859" y1="2.58241875" x2="15.19681875" y2="2.667" layer="21"/>
<rectangle x1="17.907" y1="2.58241875" x2="18.24481875" y2="2.667" layer="21"/>
<rectangle x1="18.669" y1="2.58241875" x2="19.00681875" y2="2.667" layer="21"/>
<rectangle x1="20.701" y1="2.58241875" x2="21.12518125" y2="2.667" layer="21"/>
<rectangle x1="21.463" y1="2.58241875" x2="21.88718125" y2="2.667" layer="21"/>
<rectangle x1="23.66518125" y1="2.58241875" x2="24.003" y2="2.667" layer="21"/>
<rectangle x1="25.69718125" y1="2.58241875" x2="26.11881875" y2="2.667" layer="21"/>
<rectangle x1="26.797" y1="2.58241875" x2="27.13481875" y2="2.667" layer="21"/>
<rectangle x1="30.26918125" y1="2.58241875" x2="30.607" y2="2.667" layer="21"/>
<rectangle x1="30.94481875" y1="2.58241875" x2="31.369" y2="2.667" layer="21"/>
<rectangle x1="33.06318125" y1="2.58241875" x2="33.401" y2="2.667" layer="21"/>
<rectangle x1="33.909" y1="2.58241875" x2="34.24681875" y2="2.667" layer="21"/>
<rectangle x1="35.941" y1="2.58241875" x2="36.36518125" y2="2.667" layer="21"/>
<rectangle x1="4.52881875" y1="2.667" x2="4.86918125" y2="2.75158125" layer="21"/>
<rectangle x1="8.001" y1="2.667" x2="8.33881875" y2="2.75158125" layer="21"/>
<rectangle x1="8.763" y1="2.667" x2="9.271" y2="2.75158125" layer="21"/>
<rectangle x1="10.87881875" y1="2.667" x2="11.21918125" y2="2.75158125" layer="21"/>
<rectangle x1="11.72718125" y1="2.667" x2="12.065" y2="2.75158125" layer="21"/>
<rectangle x1="12.827" y1="2.667" x2="13.16481875" y2="2.75158125" layer="21"/>
<rectangle x1="14.859" y1="2.667" x2="15.19681875" y2="2.75158125" layer="21"/>
<rectangle x1="17.907" y1="2.667" x2="18.24481875" y2="2.75158125" layer="21"/>
<rectangle x1="18.669" y1="2.667" x2="19.09318125" y2="2.75158125" layer="21"/>
<rectangle x1="20.701" y1="2.667" x2="21.03881875" y2="2.75158125" layer="21"/>
<rectangle x1="21.463" y1="2.667" x2="21.80081875" y2="2.75158125" layer="21"/>
<rectangle x1="23.66518125" y1="2.667" x2="24.08681875" y2="2.75158125" layer="21"/>
<rectangle x1="25.69718125" y1="2.667" x2="26.035" y2="2.75158125" layer="21"/>
<rectangle x1="26.797" y1="2.667" x2="27.13481875" y2="2.75158125" layer="21"/>
<rectangle x1="30.26918125" y1="2.667" x2="30.607" y2="2.75158125" layer="21"/>
<rectangle x1="30.94481875" y1="2.667" x2="31.45281875" y2="2.75158125" layer="21"/>
<rectangle x1="32.97681875" y1="2.667" x2="33.401" y2="2.75158125" layer="21"/>
<rectangle x1="33.909" y1="2.667" x2="34.33318125" y2="2.75158125" layer="21"/>
<rectangle x1="35.941" y1="2.667" x2="36.27881875" y2="2.75158125" layer="21"/>
<rectangle x1="4.52881875" y1="2.75158125" x2="4.86918125" y2="2.83641875" layer="21"/>
<rectangle x1="8.001" y1="2.75158125" x2="8.33881875" y2="2.83641875" layer="21"/>
<rectangle x1="8.763" y1="2.75158125" x2="9.271" y2="2.83641875" layer="21"/>
<rectangle x1="10.795" y1="2.75158125" x2="11.21918125" y2="2.83641875" layer="21"/>
<rectangle x1="11.72718125" y1="2.75158125" x2="12.065" y2="2.83641875" layer="21"/>
<rectangle x1="12.827" y1="2.75158125" x2="13.16481875" y2="2.83641875" layer="21"/>
<rectangle x1="14.859" y1="2.75158125" x2="15.19681875" y2="2.83641875" layer="21"/>
<rectangle x1="17.907" y1="2.75158125" x2="18.24481875" y2="2.83641875" layer="21"/>
<rectangle x1="18.75281875" y1="2.75158125" x2="19.09318125" y2="2.83641875" layer="21"/>
<rectangle x1="20.61718125" y1="2.75158125" x2="21.03881875" y2="2.83641875" layer="21"/>
<rectangle x1="21.463" y1="2.75158125" x2="21.80081875" y2="2.83641875" layer="21"/>
<rectangle x1="22.987" y1="2.75158125" x2="23.15718125" y2="2.83641875" layer="21"/>
<rectangle x1="23.749" y1="2.75158125" x2="24.17318125" y2="2.83641875" layer="21"/>
<rectangle x1="25.61081875" y1="2.75158125" x2="26.035" y2="2.83641875" layer="21"/>
<rectangle x1="26.797" y1="2.75158125" x2="27.13481875" y2="2.83641875" layer="21"/>
<rectangle x1="30.26918125" y1="2.75158125" x2="30.607" y2="2.83641875" layer="21"/>
<rectangle x1="30.94481875" y1="2.75158125" x2="31.45281875" y2="2.83641875" layer="21"/>
<rectangle x1="32.97681875" y1="2.75158125" x2="33.401" y2="2.83641875" layer="21"/>
<rectangle x1="33.909" y1="2.75158125" x2="34.33318125" y2="2.83641875" layer="21"/>
<rectangle x1="35.85718125" y1="2.75158125" x2="36.27881875" y2="2.83641875" layer="21"/>
<rectangle x1="4.52881875" y1="2.83641875" x2="4.86918125" y2="2.921" layer="21"/>
<rectangle x1="8.001" y1="2.83641875" x2="8.33881875" y2="2.921" layer="21"/>
<rectangle x1="8.763" y1="2.83641875" x2="9.35481875" y2="2.921" layer="21"/>
<rectangle x1="10.71118125" y1="2.83641875" x2="11.13281875" y2="2.921" layer="21"/>
<rectangle x1="11.72718125" y1="2.83641875" x2="12.065" y2="2.921" layer="21"/>
<rectangle x1="12.827" y1="2.83641875" x2="13.16481875" y2="2.921" layer="21"/>
<rectangle x1="14.859" y1="2.83641875" x2="15.19681875" y2="2.921" layer="21"/>
<rectangle x1="17.907" y1="2.83641875" x2="18.24481875" y2="2.921" layer="21"/>
<rectangle x1="18.75281875" y1="2.83641875" x2="19.177" y2="2.921" layer="21"/>
<rectangle x1="20.53081875" y1="2.83641875" x2="20.955" y2="2.921" layer="21"/>
<rectangle x1="21.463" y1="2.83641875" x2="21.80081875" y2="2.921" layer="21"/>
<rectangle x1="22.90318125" y1="2.83641875" x2="23.241" y2="2.921" layer="21"/>
<rectangle x1="23.749" y1="2.83641875" x2="24.257" y2="2.921" layer="21"/>
<rectangle x1="25.527" y1="2.83641875" x2="25.95118125" y2="2.921" layer="21"/>
<rectangle x1="26.797" y1="2.83641875" x2="27.13481875" y2="2.921" layer="21"/>
<rectangle x1="30.26918125" y1="2.83641875" x2="30.607" y2="2.921" layer="21"/>
<rectangle x1="30.94481875" y1="2.83641875" x2="31.53918125" y2="2.921" layer="21"/>
<rectangle x1="32.893" y1="2.83641875" x2="33.31718125" y2="2.921" layer="21"/>
<rectangle x1="33.99281875" y1="2.83641875" x2="34.417" y2="2.921" layer="21"/>
<rectangle x1="35.77081875" y1="2.83641875" x2="36.195" y2="2.921" layer="21"/>
<rectangle x1="4.52881875" y1="2.921" x2="4.86918125" y2="3.00558125" layer="21"/>
<rectangle x1="8.001" y1="2.921" x2="8.33881875" y2="3.00558125" layer="21"/>
<rectangle x1="8.763" y1="2.921" x2="9.44118125" y2="3.00558125" layer="21"/>
<rectangle x1="10.62481875" y1="2.921" x2="11.049" y2="3.00558125" layer="21"/>
<rectangle x1="11.72718125" y1="2.921" x2="12.065" y2="3.00558125" layer="21"/>
<rectangle x1="12.827" y1="2.921" x2="13.16481875" y2="3.00558125" layer="21"/>
<rectangle x1="14.859" y1="2.921" x2="15.19681875" y2="3.00558125" layer="21"/>
<rectangle x1="17.907" y1="2.921" x2="18.24481875" y2="3.00558125" layer="21"/>
<rectangle x1="18.83918125" y1="2.921" x2="19.26081875" y2="3.00558125" layer="21"/>
<rectangle x1="20.447" y1="2.921" x2="20.87118125" y2="3.00558125" layer="21"/>
<rectangle x1="21.463" y1="2.921" x2="21.88718125" y2="3.00558125" layer="21"/>
<rectangle x1="22.81681875" y1="2.921" x2="23.241" y2="3.00558125" layer="21"/>
<rectangle x1="23.83281875" y1="2.921" x2="24.34081875" y2="3.00558125" layer="21"/>
<rectangle x1="25.44318125" y1="2.921" x2="25.95118125" y2="3.00558125" layer="21"/>
<rectangle x1="26.797" y1="2.921" x2="27.22118125" y2="3.00558125" layer="21"/>
<rectangle x1="30.26918125" y1="2.921" x2="30.607" y2="3.00558125" layer="21"/>
<rectangle x1="30.94481875" y1="2.921" x2="31.623" y2="3.00558125" layer="21"/>
<rectangle x1="32.80918125" y1="2.921" x2="33.23081875" y2="3.00558125" layer="21"/>
<rectangle x1="34.07918125" y1="2.921" x2="34.50081875" y2="3.00558125" layer="21"/>
<rectangle x1="35.687" y1="2.921" x2="36.195" y2="3.00558125" layer="21"/>
<rectangle x1="4.52881875" y1="3.00558125" x2="4.86918125" y2="3.09041875" layer="21"/>
<rectangle x1="8.001" y1="3.00558125" x2="8.33881875" y2="3.09041875" layer="21"/>
<rectangle x1="8.763" y1="3.00558125" x2="9.60881875" y2="3.09041875" layer="21"/>
<rectangle x1="10.541" y1="3.00558125" x2="11.049" y2="3.09041875" layer="21"/>
<rectangle x1="11.72718125" y1="3.00558125" x2="12.065" y2="3.09041875" layer="21"/>
<rectangle x1="12.827" y1="3.00558125" x2="13.16481875" y2="3.09041875" layer="21"/>
<rectangle x1="14.859" y1="3.00558125" x2="15.19681875" y2="3.09041875" layer="21"/>
<rectangle x1="17.907" y1="3.00558125" x2="18.24481875" y2="3.09041875" layer="21"/>
<rectangle x1="18.83918125" y1="3.00558125" x2="19.431" y2="3.09041875" layer="21"/>
<rectangle x1="20.27681875" y1="3.00558125" x2="20.87118125" y2="3.09041875" layer="21"/>
<rectangle x1="21.54681875" y1="3.00558125" x2="21.971" y2="3.09041875" layer="21"/>
<rectangle x1="22.733" y1="3.00558125" x2="23.15718125" y2="3.09041875" layer="21"/>
<rectangle x1="23.91918125" y1="3.00558125" x2="24.42718125" y2="3.09041875" layer="21"/>
<rectangle x1="25.273" y1="3.00558125" x2="25.86481875" y2="3.09041875" layer="21"/>
<rectangle x1="26.88081875" y1="3.00558125" x2="27.22118125" y2="3.09041875" layer="21"/>
<rectangle x1="30.18281875" y1="3.00558125" x2="30.607" y2="3.09041875" layer="21"/>
<rectangle x1="30.94481875" y1="3.00558125" x2="31.79318125" y2="3.09041875" layer="21"/>
<rectangle x1="32.639" y1="3.00558125" x2="33.23081875" y2="3.09041875" layer="21"/>
<rectangle x1="34.163" y1="3.00558125" x2="34.671" y2="3.09041875" layer="21"/>
<rectangle x1="35.60318125" y1="3.00558125" x2="36.11118125" y2="3.09041875" layer="21"/>
<rectangle x1="4.52881875" y1="3.09041875" x2="4.86918125" y2="3.175" layer="21"/>
<rectangle x1="8.001" y1="3.09041875" x2="8.33881875" y2="3.175" layer="21"/>
<rectangle x1="8.763" y1="3.09041875" x2="9.10081875" y2="3.175" layer="21"/>
<rectangle x1="9.18718125" y1="3.09041875" x2="9.86281875" y2="3.175" layer="21"/>
<rectangle x1="10.20318125" y1="3.09041875" x2="10.96518125" y2="3.175" layer="21"/>
<rectangle x1="11.72718125" y1="3.09041875" x2="12.065" y2="3.175" layer="21"/>
<rectangle x1="12.827" y1="3.09041875" x2="13.16481875" y2="3.175" layer="21"/>
<rectangle x1="14.859" y1="3.09041875" x2="15.19681875" y2="3.175" layer="21"/>
<rectangle x1="17.907" y1="3.09041875" x2="18.24481875" y2="3.175" layer="21"/>
<rectangle x1="19.00681875" y1="3.09041875" x2="19.685" y2="3.175" layer="21"/>
<rectangle x1="20.02281875" y1="3.09041875" x2="20.78481875" y2="3.175" layer="21"/>
<rectangle x1="21.54681875" y1="3.09041875" x2="22.225" y2="3.175" layer="21"/>
<rectangle x1="22.56281875" y1="3.09041875" x2="23.15718125" y2="3.175" layer="21"/>
<rectangle x1="24.003" y1="3.09041875" x2="24.68118125" y2="3.175" layer="21"/>
<rectangle x1="25.10281875" y1="3.09041875" x2="25.781" y2="3.175" layer="21"/>
<rectangle x1="26.88081875" y1="3.09041875" x2="27.22118125" y2="3.175" layer="21"/>
<rectangle x1="30.18281875" y1="3.09041875" x2="30.52318125" y2="3.175" layer="21"/>
<rectangle x1="30.94481875" y1="3.09041875" x2="32.04718125" y2="3.175" layer="21"/>
<rectangle x1="32.385" y1="3.09041875" x2="33.06318125" y2="3.175" layer="21"/>
<rectangle x1="34.24681875" y1="3.09041875" x2="34.925" y2="3.175" layer="21"/>
<rectangle x1="35.26281875" y1="3.09041875" x2="36.02481875" y2="3.175" layer="21"/>
<rectangle x1="4.52881875" y1="3.175" x2="4.86918125" y2="3.25958125" layer="21"/>
<rectangle x1="8.001" y1="3.175" x2="8.33881875" y2="3.25958125" layer="21"/>
<rectangle x1="8.763" y1="3.175" x2="9.10081875" y2="3.25958125" layer="21"/>
<rectangle x1="9.271" y1="3.175" x2="10.87881875" y2="3.25958125" layer="21"/>
<rectangle x1="11.72718125" y1="3.175" x2="12.065" y2="3.25958125" layer="21"/>
<rectangle x1="12.40281875" y1="3.175" x2="14.26718125" y2="3.25958125" layer="21"/>
<rectangle x1="14.859" y1="3.175" x2="15.19681875" y2="3.25958125" layer="21"/>
<rectangle x1="17.907" y1="3.175" x2="18.24481875" y2="3.25958125" layer="21"/>
<rectangle x1="19.09318125" y1="3.175" x2="20.701" y2="3.25958125" layer="21"/>
<rectangle x1="21.63318125" y1="3.175" x2="23.07081875" y2="3.25958125" layer="21"/>
<rectangle x1="24.08681875" y1="3.175" x2="25.69718125" y2="3.25958125" layer="21"/>
<rectangle x1="26.88081875" y1="3.175" x2="27.22118125" y2="3.25958125" layer="21"/>
<rectangle x1="30.18281875" y1="3.175" x2="30.52318125" y2="3.25958125" layer="21"/>
<rectangle x1="30.94481875" y1="3.175" x2="31.28518125" y2="3.25958125" layer="21"/>
<rectangle x1="31.45281875" y1="3.175" x2="33.06318125" y2="3.25958125" layer="21"/>
<rectangle x1="34.33318125" y1="3.175" x2="35.941" y2="3.25958125" layer="21"/>
<rectangle x1="4.52881875" y1="3.25958125" x2="4.86918125" y2="3.34441875" layer="21"/>
<rectangle x1="8.001" y1="3.25958125" x2="8.33881875" y2="3.34441875" layer="21"/>
<rectangle x1="8.763" y1="3.25958125" x2="9.10081875" y2="3.34441875" layer="21"/>
<rectangle x1="9.35481875" y1="3.25958125" x2="10.71118125" y2="3.34441875" layer="21"/>
<rectangle x1="11.72718125" y1="3.25958125" x2="12.065" y2="3.34441875" layer="21"/>
<rectangle x1="12.319" y1="3.25958125" x2="14.26718125" y2="3.34441875" layer="21"/>
<rectangle x1="14.859" y1="3.25958125" x2="15.19681875" y2="3.34441875" layer="21"/>
<rectangle x1="17.82318125" y1="3.25958125" x2="18.161" y2="3.34441875" layer="21"/>
<rectangle x1="19.177" y1="3.25958125" x2="20.53081875" y2="3.34441875" layer="21"/>
<rectangle x1="21.717" y1="3.25958125" x2="22.987" y2="3.34441875" layer="21"/>
<rectangle x1="24.17318125" y1="3.25958125" x2="25.61081875" y2="3.34441875" layer="21"/>
<rectangle x1="26.88081875" y1="3.25958125" x2="27.305" y2="3.34441875" layer="21"/>
<rectangle x1="30.099" y1="3.25958125" x2="30.52318125" y2="3.34441875" layer="21"/>
<rectangle x1="30.94481875" y1="3.25958125" x2="31.28518125" y2="3.34441875" layer="21"/>
<rectangle x1="31.53918125" y1="3.25958125" x2="32.893" y2="3.34441875" layer="21"/>
<rectangle x1="34.417" y1="3.25958125" x2="35.77081875" y2="3.34441875" layer="21"/>
<rectangle x1="4.52881875" y1="3.34441875" x2="4.86918125" y2="3.429" layer="21"/>
<rectangle x1="8.001" y1="3.34441875" x2="8.33881875" y2="3.429" layer="21"/>
<rectangle x1="8.763" y1="3.34441875" x2="9.10081875" y2="3.429" layer="21"/>
<rectangle x1="9.525" y1="3.34441875" x2="10.541" y2="3.429" layer="21"/>
<rectangle x1="11.72718125" y1="3.34441875" x2="12.065" y2="3.429" layer="21"/>
<rectangle x1="12.40281875" y1="3.34441875" x2="14.26718125" y2="3.429" layer="21"/>
<rectangle x1="14.859" y1="3.34441875" x2="15.19681875" y2="3.429" layer="21"/>
<rectangle x1="17.82318125" y1="3.34441875" x2="18.161" y2="3.429" layer="21"/>
<rectangle x1="19.34718125" y1="3.34441875" x2="20.36318125" y2="3.429" layer="21"/>
<rectangle x1="21.88718125" y1="3.34441875" x2="22.81681875" y2="3.429" layer="21"/>
<rectangle x1="24.34081875" y1="3.34441875" x2="25.35681875" y2="3.429" layer="21"/>
<rectangle x1="26.96718125" y1="3.34441875" x2="27.305" y2="3.429" layer="21"/>
<rectangle x1="30.099" y1="3.34441875" x2="30.43681875" y2="3.429" layer="21"/>
<rectangle x1="30.94481875" y1="3.34441875" x2="31.28518125" y2="3.429" layer="21"/>
<rectangle x1="31.70681875" y1="3.34441875" x2="32.72281875" y2="3.429" layer="21"/>
<rectangle x1="34.58718125" y1="3.34441875" x2="35.60318125" y2="3.429" layer="21"/>
<rectangle x1="4.52881875" y1="3.429" x2="4.86918125" y2="3.51358125" layer="21"/>
<rectangle x1="8.001" y1="3.429" x2="8.33881875" y2="3.51358125" layer="21"/>
<rectangle x1="8.84681875" y1="3.429" x2="9.017" y2="3.51358125" layer="21"/>
<rectangle x1="9.779" y1="3.429" x2="10.287" y2="3.51358125" layer="21"/>
<rectangle x1="11.811" y1="3.429" x2="11.98118125" y2="3.51358125" layer="21"/>
<rectangle x1="12.40281875" y1="3.429" x2="14.18081875" y2="3.51358125" layer="21"/>
<rectangle x1="14.859" y1="3.429" x2="15.19681875" y2="3.51358125" layer="21"/>
<rectangle x1="17.82318125" y1="3.429" x2="18.161" y2="3.51358125" layer="21"/>
<rectangle x1="19.60118125" y1="3.429" x2="20.10918125" y2="3.51358125" layer="21"/>
<rectangle x1="22.14118125" y1="3.429" x2="22.56281875" y2="3.51358125" layer="21"/>
<rectangle x1="24.59481875" y1="3.429" x2="25.10281875" y2="3.51358125" layer="21"/>
<rectangle x1="26.96718125" y1="3.429" x2="27.38881875" y2="3.51358125" layer="21"/>
<rectangle x1="30.01518125" y1="3.429" x2="30.43681875" y2="3.51358125" layer="21"/>
<rectangle x1="31.03118125" y1="3.429" x2="31.19881875" y2="3.51358125" layer="21"/>
<rectangle x1="31.96081875" y1="3.429" x2="32.46881875" y2="3.51358125" layer="21"/>
<rectangle x1="34.84118125" y1="3.429" x2="35.34918125" y2="3.51358125" layer="21"/>
<rectangle x1="4.52881875" y1="3.51358125" x2="4.86918125" y2="3.59841875" layer="21"/>
<rectangle x1="8.001" y1="3.51358125" x2="8.33881875" y2="3.59841875" layer="21"/>
<rectangle x1="12.827" y1="3.51358125" x2="13.16481875" y2="3.59841875" layer="21"/>
<rectangle x1="14.859" y1="3.51358125" x2="15.19681875" y2="3.59841875" layer="21"/>
<rectangle x1="17.73681875" y1="3.51358125" x2="18.07718125" y2="3.59841875" layer="21"/>
<rectangle x1="27.051" y1="3.51358125" x2="27.47518125" y2="3.59841875" layer="21"/>
<rectangle x1="30.01518125" y1="3.51358125" x2="30.353" y2="3.59841875" layer="21"/>
<rectangle x1="4.52881875" y1="3.59841875" x2="4.86918125" y2="3.683" layer="21"/>
<rectangle x1="8.001" y1="3.59841875" x2="8.33881875" y2="3.683" layer="21"/>
<rectangle x1="11.811" y1="3.59841875" x2="11.98118125" y2="3.683" layer="21"/>
<rectangle x1="12.827" y1="3.59841875" x2="13.16481875" y2="3.683" layer="21"/>
<rectangle x1="14.859" y1="3.59841875" x2="15.19681875" y2="3.683" layer="21"/>
<rectangle x1="17.653" y1="3.59841875" x2="18.07718125" y2="3.683" layer="21"/>
<rectangle x1="27.13481875" y1="3.59841875" x2="27.47518125" y2="3.683" layer="21"/>
<rectangle x1="29.92881875" y1="3.59841875" x2="30.353" y2="3.683" layer="21"/>
<rectangle x1="4.52881875" y1="3.683" x2="4.86918125" y2="3.76758125" layer="21"/>
<rectangle x1="8.001" y1="3.683" x2="8.33881875" y2="3.76758125" layer="21"/>
<rectangle x1="11.72718125" y1="3.683" x2="12.065" y2="3.76758125" layer="21"/>
<rectangle x1="12.827" y1="3.683" x2="13.16481875" y2="3.76758125" layer="21"/>
<rectangle x1="14.859" y1="3.683" x2="15.19681875" y2="3.76758125" layer="21"/>
<rectangle x1="17.653" y1="3.683" x2="18.07718125" y2="3.76758125" layer="21"/>
<rectangle x1="27.13481875" y1="3.683" x2="27.559" y2="3.76758125" layer="21"/>
<rectangle x1="29.845" y1="3.683" x2="30.26918125" y2="3.76758125" layer="21"/>
<rectangle x1="4.52881875" y1="3.76758125" x2="4.86918125" y2="3.85241875" layer="21"/>
<rectangle x1="8.001" y1="3.76758125" x2="8.33881875" y2="3.85241875" layer="21"/>
<rectangle x1="11.72718125" y1="3.76758125" x2="12.065" y2="3.85241875" layer="21"/>
<rectangle x1="12.827" y1="3.76758125" x2="13.16481875" y2="3.85241875" layer="21"/>
<rectangle x1="14.859" y1="3.76758125" x2="15.19681875" y2="3.85241875" layer="21"/>
<rectangle x1="17.56918125" y1="3.76758125" x2="17.99081875" y2="3.85241875" layer="21"/>
<rectangle x1="27.22118125" y1="3.76758125" x2="27.72918125" y2="3.85241875" layer="21"/>
<rectangle x1="29.76118125" y1="3.76758125" x2="30.18281875" y2="3.85241875" layer="21"/>
<rectangle x1="4.52881875" y1="3.85241875" x2="4.86918125" y2="3.937" layer="21"/>
<rectangle x1="8.001" y1="3.85241875" x2="8.33881875" y2="3.937" layer="21"/>
<rectangle x1="11.72718125" y1="3.85241875" x2="12.065" y2="3.937" layer="21"/>
<rectangle x1="12.827" y1="3.85241875" x2="13.16481875" y2="3.937" layer="21"/>
<rectangle x1="14.859" y1="3.85241875" x2="15.19681875" y2="3.937" layer="21"/>
<rectangle x1="17.48281875" y1="3.85241875" x2="17.907" y2="3.937" layer="21"/>
<rectangle x1="27.305" y1="3.85241875" x2="27.813" y2="3.937" layer="21"/>
<rectangle x1="29.591" y1="3.85241875" x2="30.099" y2="3.937" layer="21"/>
<rectangle x1="4.52881875" y1="3.937" x2="4.86918125" y2="4.02158125" layer="21"/>
<rectangle x1="8.001" y1="3.937" x2="8.33881875" y2="4.02158125" layer="21"/>
<rectangle x1="11.72718125" y1="3.937" x2="12.065" y2="4.02158125" layer="21"/>
<rectangle x1="12.827" y1="3.937" x2="13.16481875" y2="4.02158125" layer="21"/>
<rectangle x1="14.859" y1="3.937" x2="15.19681875" y2="4.02158125" layer="21"/>
<rectangle x1="17.399" y1="3.937" x2="17.907" y2="4.02158125" layer="21"/>
<rectangle x1="27.38881875" y1="3.937" x2="27.89681875" y2="4.02158125" layer="21"/>
<rectangle x1="29.50718125" y1="3.937" x2="30.099" y2="4.02158125" layer="21"/>
<rectangle x1="4.52881875" y1="4.02158125" x2="4.86918125" y2="4.10641875" layer="21"/>
<rectangle x1="8.001" y1="4.02158125" x2="8.33881875" y2="4.10641875" layer="21"/>
<rectangle x1="11.811" y1="4.02158125" x2="11.98118125" y2="4.10641875" layer="21"/>
<rectangle x1="12.827" y1="4.02158125" x2="13.16481875" y2="4.10641875" layer="21"/>
<rectangle x1="14.859" y1="4.02158125" x2="15.19681875" y2="4.10641875" layer="21"/>
<rectangle x1="17.31518125" y1="4.02158125" x2="17.82318125" y2="4.10641875" layer="21"/>
<rectangle x1="27.47518125" y1="4.02158125" x2="28.067" y2="4.10641875" layer="21"/>
<rectangle x1="29.337" y1="4.02158125" x2="30.01518125" y2="4.10641875" layer="21"/>
<rectangle x1="4.52881875" y1="4.10641875" x2="4.86918125" y2="4.191" layer="21"/>
<rectangle x1="8.001" y1="4.10641875" x2="8.33881875" y2="4.191" layer="21"/>
<rectangle x1="12.827" y1="4.10641875" x2="13.16481875" y2="4.191" layer="21"/>
<rectangle x1="14.859" y1="4.10641875" x2="15.19681875" y2="4.191" layer="21"/>
<rectangle x1="17.22881875" y1="4.10641875" x2="17.73681875" y2="4.191" layer="21"/>
<rectangle x1="27.559" y1="4.10641875" x2="28.321" y2="4.191" layer="21"/>
<rectangle x1="29.083" y1="4.10641875" x2="29.845" y2="4.191" layer="21"/>
<rectangle x1="4.52881875" y1="4.191" x2="4.86918125" y2="4.27558125" layer="21"/>
<rectangle x1="8.001" y1="4.191" x2="8.33881875" y2="4.27558125" layer="21"/>
<rectangle x1="12.827" y1="4.191" x2="13.16481875" y2="4.27558125" layer="21"/>
<rectangle x1="14.859" y1="4.191" x2="15.19681875" y2="4.27558125" layer="21"/>
<rectangle x1="17.06118125" y1="4.191" x2="17.653" y2="4.27558125" layer="21"/>
<rectangle x1="27.64281875" y1="4.191" x2="29.76118125" y2="4.27558125" layer="21"/>
<rectangle x1="4.52881875" y1="4.27558125" x2="4.86918125" y2="4.36041875" layer="21"/>
<rectangle x1="8.001" y1="4.27558125" x2="8.33881875" y2="4.36041875" layer="21"/>
<rectangle x1="12.827" y1="4.27558125" x2="13.16481875" y2="4.36041875" layer="21"/>
<rectangle x1="14.94281875" y1="4.27558125" x2="15.28318125" y2="4.36041875" layer="21"/>
<rectangle x1="16.80718125" y1="4.27558125" x2="17.56918125" y2="4.36041875" layer="21"/>
<rectangle x1="27.813" y1="4.27558125" x2="29.591" y2="4.36041875" layer="21"/>
<rectangle x1="4.52881875" y1="4.36041875" x2="4.86918125" y2="4.445" layer="21"/>
<rectangle x1="8.001" y1="4.36041875" x2="8.33881875" y2="4.445" layer="21"/>
<rectangle x1="12.827" y1="4.36041875" x2="13.16481875" y2="4.445" layer="21"/>
<rectangle x1="14.859" y1="4.36041875" x2="17.399" y2="4.445" layer="21"/>
<rectangle x1="27.98318125" y1="4.36041875" x2="29.42081875" y2="4.445" layer="21"/>
<rectangle x1="4.52881875" y1="4.445" x2="4.86918125" y2="4.52958125" layer="21"/>
<rectangle x1="8.001" y1="4.445" x2="8.255" y2="4.52958125" layer="21"/>
<rectangle x1="12.827" y1="4.445" x2="13.081" y2="4.52958125" layer="21"/>
<rectangle x1="14.859" y1="4.445" x2="17.31518125" y2="4.52958125" layer="21"/>
<rectangle x1="28.321" y1="4.445" x2="29.16681875" y2="4.52958125" layer="21"/>
<rectangle x1="14.859" y1="4.52958125" x2="17.145" y2="4.61441875" layer="21"/>
<rectangle x1="14.94281875" y1="4.61441875" x2="16.891" y2="4.699" layer="21"/>
<rectangle x1="7.239" y1="6.477" x2="32.46881875" y2="6.56158125" layer="21"/>
<rectangle x1="6.64718125" y1="6.56158125" x2="33.147" y2="6.64641875" layer="21"/>
<rectangle x1="6.30681875" y1="6.64641875" x2="33.48481875" y2="6.731" layer="21"/>
<rectangle x1="5.969" y1="6.731" x2="33.82518125" y2="6.81558125" layer="21"/>
<rectangle x1="5.63118125" y1="6.81558125" x2="34.07918125" y2="6.90041875" layer="21"/>
<rectangle x1="5.37718125" y1="6.90041875" x2="34.33318125" y2="6.985" layer="21"/>
<rectangle x1="5.207" y1="6.985" x2="34.58718125" y2="7.06958125" layer="21"/>
<rectangle x1="4.953" y1="7.06958125" x2="34.75481875" y2="7.15441875" layer="21"/>
<rectangle x1="4.78281875" y1="7.15441875" x2="34.925" y2="7.239" layer="21"/>
<rectangle x1="4.61518125" y1="7.239" x2="35.179" y2="7.32358125" layer="21"/>
<rectangle x1="4.445" y1="7.32358125" x2="35.34918125" y2="7.40841875" layer="21"/>
<rectangle x1="4.27481875" y1="7.40841875" x2="35.51681875" y2="7.493" layer="21"/>
<rectangle x1="4.10718125" y1="7.493" x2="35.60318125" y2="7.57758125" layer="21"/>
<rectangle x1="4.02081875" y1="7.57758125" x2="7.15518125" y2="7.66241875" layer="21"/>
<rectangle x1="19.51481875" y1="7.57758125" x2="35.77081875" y2="7.66241875" layer="21"/>
<rectangle x1="3.85318125" y1="7.66241875" x2="6.64718125" y2="7.747" layer="21"/>
<rectangle x1="19.431" y1="7.66241875" x2="35.941" y2="7.747" layer="21"/>
<rectangle x1="3.76681875" y1="7.747" x2="6.30681875" y2="7.83158125" layer="21"/>
<rectangle x1="19.431" y1="7.747" x2="36.02481875" y2="7.83158125" layer="21"/>
<rectangle x1="3.59918125" y1="7.83158125" x2="6.05281875" y2="7.91641875" layer="21"/>
<rectangle x1="19.34718125" y1="7.83158125" x2="36.195" y2="7.91641875" layer="21"/>
<rectangle x1="3.51281875" y1="7.91641875" x2="5.79881875" y2="8.001" layer="21"/>
<rectangle x1="19.26081875" y1="7.91641875" x2="36.27881875" y2="8.001" layer="21"/>
<rectangle x1="3.34518125" y1="8.001" x2="5.54481875" y2="8.08558125" layer="21"/>
<rectangle x1="19.26081875" y1="8.001" x2="36.36518125" y2="8.08558125" layer="21"/>
<rectangle x1="3.25881875" y1="8.08558125" x2="5.37718125" y2="8.17041875" layer="21"/>
<rectangle x1="19.177" y1="8.08558125" x2="36.53281875" y2="8.17041875" layer="21"/>
<rectangle x1="3.175" y1="8.17041875" x2="5.207" y2="8.255" layer="21"/>
<rectangle x1="19.177" y1="8.17041875" x2="36.61918125" y2="8.255" layer="21"/>
<rectangle x1="3.00481875" y1="8.255" x2="5.03681875" y2="8.33958125" layer="21"/>
<rectangle x1="19.09318125" y1="8.255" x2="36.703" y2="8.33958125" layer="21"/>
<rectangle x1="3.00481875" y1="8.33958125" x2="4.86918125" y2="8.42441875" layer="21"/>
<rectangle x1="19.09318125" y1="8.33958125" x2="36.78681875" y2="8.42441875" layer="21"/>
<rectangle x1="2.83718125" y1="8.42441875" x2="4.699" y2="8.509" layer="21"/>
<rectangle x1="19.00681875" y1="8.42441875" x2="36.957" y2="8.509" layer="21"/>
<rectangle x1="2.75081875" y1="8.509" x2="4.61518125" y2="8.59358125" layer="21"/>
<rectangle x1="19.00681875" y1="8.509" x2="37.04081875" y2="8.59358125" layer="21"/>
<rectangle x1="2.667" y1="8.59358125" x2="4.445" y2="8.67841875" layer="21"/>
<rectangle x1="18.923" y1="8.59358125" x2="37.12718125" y2="8.67841875" layer="21"/>
<rectangle x1="2.58318125" y1="8.67841875" x2="4.27481875" y2="8.763" layer="21"/>
<rectangle x1="18.923" y1="8.67841875" x2="37.211" y2="8.763" layer="21"/>
<rectangle x1="2.49681875" y1="8.763" x2="4.191" y2="8.84758125" layer="21"/>
<rectangle x1="18.83918125" y1="8.763" x2="37.29481875" y2="8.84758125" layer="21"/>
<rectangle x1="2.413" y1="8.84758125" x2="4.02081875" y2="8.93241875" layer="21"/>
<rectangle x1="18.83918125" y1="8.84758125" x2="37.38118125" y2="8.93241875" layer="21"/>
<rectangle x1="2.32918125" y1="8.93241875" x2="3.937" y2="9.017" layer="21"/>
<rectangle x1="18.75281875" y1="8.93241875" x2="37.465" y2="9.017" layer="21"/>
<rectangle x1="2.24281875" y1="9.017" x2="3.85318125" y2="9.10158125" layer="21"/>
<rectangle x1="18.75281875" y1="9.017" x2="37.54881875" y2="9.10158125" layer="21"/>
<rectangle x1="2.159" y1="9.10158125" x2="3.76681875" y2="9.18641875" layer="21"/>
<rectangle x1="18.75281875" y1="9.10158125" x2="37.63518125" y2="9.18641875" layer="21"/>
<rectangle x1="2.07518125" y1="9.18641875" x2="3.683" y2="9.271" layer="21"/>
<rectangle x1="18.669" y1="9.18641875" x2="37.63518125" y2="9.271" layer="21"/>
<rectangle x1="2.07518125" y1="9.271" x2="3.51281875" y2="9.35558125" layer="21"/>
<rectangle x1="18.669" y1="9.271" x2="37.719" y2="9.35558125" layer="21"/>
<rectangle x1="1.98881875" y1="9.35558125" x2="3.429" y2="9.44041875" layer="21"/>
<rectangle x1="18.669" y1="9.35558125" x2="37.80281875" y2="9.44041875" layer="21"/>
<rectangle x1="1.905" y1="9.44041875" x2="3.34518125" y2="9.525" layer="21"/>
<rectangle x1="18.58518125" y1="9.44041875" x2="37.88918125" y2="9.525" layer="21"/>
<rectangle x1="1.82118125" y1="9.525" x2="3.25881875" y2="9.60958125" layer="21"/>
<rectangle x1="18.58518125" y1="9.525" x2="37.973" y2="9.60958125" layer="21"/>
<rectangle x1="1.73481875" y1="9.60958125" x2="3.175" y2="9.69441875" layer="21"/>
<rectangle x1="18.49881875" y1="9.60958125" x2="37.973" y2="9.69441875" layer="21"/>
<rectangle x1="1.73481875" y1="9.69441875" x2="3.09118125" y2="9.779" layer="21"/>
<rectangle x1="18.49881875" y1="9.69441875" x2="38.05681875" y2="9.779" layer="21"/>
<rectangle x1="1.651" y1="9.779" x2="3.00481875" y2="9.86358125" layer="21"/>
<rectangle x1="18.49881875" y1="9.779" x2="38.14318125" y2="9.86358125" layer="21"/>
<rectangle x1="1.56718125" y1="9.86358125" x2="2.921" y2="9.94841875" layer="21"/>
<rectangle x1="18.415" y1="9.86358125" x2="38.227" y2="9.94841875" layer="21"/>
<rectangle x1="1.56718125" y1="9.94841875" x2="2.921" y2="10.033" layer="21"/>
<rectangle x1="18.415" y1="9.94841875" x2="38.227" y2="10.033" layer="21"/>
<rectangle x1="1.48081875" y1="10.033" x2="2.83718125" y2="10.11758125" layer="21"/>
<rectangle x1="18.415" y1="10.033" x2="38.31081875" y2="10.11758125" layer="21"/>
<rectangle x1="1.397" y1="10.11758125" x2="2.75081875" y2="10.20241875" layer="21"/>
<rectangle x1="18.415" y1="10.11758125" x2="38.39718125" y2="10.20241875" layer="21"/>
<rectangle x1="1.397" y1="10.20241875" x2="2.667" y2="10.287" layer="21"/>
<rectangle x1="18.33118125" y1="10.20241875" x2="38.39718125" y2="10.287" layer="21"/>
<rectangle x1="1.31318125" y1="10.287" x2="2.58318125" y2="10.37158125" layer="21"/>
<rectangle x1="18.33118125" y1="10.287" x2="38.481" y2="10.37158125" layer="21"/>
<rectangle x1="1.22681875" y1="10.37158125" x2="2.58318125" y2="10.45641875" layer="21"/>
<rectangle x1="18.33118125" y1="10.37158125" x2="38.481" y2="10.45641875" layer="21"/>
<rectangle x1="1.22681875" y1="10.45641875" x2="2.49681875" y2="10.541" layer="21"/>
<rectangle x1="18.33118125" y1="10.45641875" x2="38.56481875" y2="10.541" layer="21"/>
<rectangle x1="1.143" y1="10.541" x2="2.413" y2="10.62558125" layer="21"/>
<rectangle x1="18.24481875" y1="10.541" x2="38.56481875" y2="10.62558125" layer="21"/>
<rectangle x1="1.143" y1="10.62558125" x2="2.413" y2="10.71041875" layer="21"/>
<rectangle x1="18.24481875" y1="10.62558125" x2="38.65118125" y2="10.71041875" layer="21"/>
<rectangle x1="1.05918125" y1="10.71041875" x2="2.32918125" y2="10.795" layer="21"/>
<rectangle x1="18.24481875" y1="10.71041875" x2="38.65118125" y2="10.795" layer="21"/>
<rectangle x1="1.05918125" y1="10.795" x2="2.24281875" y2="10.87958125" layer="21"/>
<rectangle x1="18.24481875" y1="10.795" x2="38.735" y2="10.87958125" layer="21"/>
<rectangle x1="0.97281875" y1="10.87958125" x2="2.24281875" y2="10.96441875" layer="21"/>
<rectangle x1="18.24481875" y1="10.87958125" x2="38.81881875" y2="10.96441875" layer="21"/>
<rectangle x1="0.97281875" y1="10.96441875" x2="2.159" y2="11.049" layer="21"/>
<rectangle x1="18.161" y1="10.96441875" x2="38.81881875" y2="11.049" layer="21"/>
<rectangle x1="0.889" y1="11.049" x2="2.159" y2="11.13358125" layer="21"/>
<rectangle x1="18.161" y1="11.049" x2="38.90518125" y2="11.13358125" layer="21"/>
<rectangle x1="0.889" y1="11.13358125" x2="2.07518125" y2="11.21841875" layer="21"/>
<rectangle x1="7.40918125" y1="11.13358125" x2="8.763" y2="11.21841875" layer="21"/>
<rectangle x1="11.557" y1="11.13358125" x2="13.92681875" y2="11.21841875" layer="21"/>
<rectangle x1="18.161" y1="11.13358125" x2="21.63318125" y2="11.21841875" layer="21"/>
<rectangle x1="23.07081875" y1="11.13358125" x2="33.06318125" y2="11.21841875" layer="21"/>
<rectangle x1="33.147" y1="11.13358125" x2="38.90518125" y2="11.21841875" layer="21"/>
<rectangle x1="0.80518125" y1="11.21841875" x2="2.07518125" y2="11.303" layer="21"/>
<rectangle x1="7.06881875" y1="11.21841875" x2="9.10081875" y2="11.303" layer="21"/>
<rectangle x1="11.47318125" y1="11.21841875" x2="14.18081875" y2="11.303" layer="21"/>
<rectangle x1="18.161" y1="11.21841875" x2="21.29281875" y2="11.303" layer="21"/>
<rectangle x1="23.32481875" y1="11.21841875" x2="26.45918125" y2="11.303" layer="21"/>
<rectangle x1="26.88081875" y1="11.21841875" x2="29.92881875" y2="11.303" layer="21"/>
<rectangle x1="30.43681875" y1="11.21841875" x2="32.639" y2="11.303" layer="21"/>
<rectangle x1="33.82518125" y1="11.21841875" x2="38.90518125" y2="11.303" layer="21"/>
<rectangle x1="0.80518125" y1="11.303" x2="1.98881875" y2="11.38758125" layer="21"/>
<rectangle x1="6.81481875" y1="11.303" x2="9.35481875" y2="11.38758125" layer="21"/>
<rectangle x1="11.38681875" y1="11.303" x2="14.43481875" y2="11.38758125" layer="21"/>
<rectangle x1="18.161" y1="11.303" x2="21.12518125" y2="11.38758125" layer="21"/>
<rectangle x1="23.57881875" y1="11.303" x2="26.37281875" y2="11.38758125" layer="21"/>
<rectangle x1="27.051" y1="11.303" x2="29.845" y2="11.38758125" layer="21"/>
<rectangle x1="30.52318125" y1="11.303" x2="32.385" y2="11.38758125" layer="21"/>
<rectangle x1="34.163" y1="11.303" x2="38.989" y2="11.38758125" layer="21"/>
<rectangle x1="0.80518125" y1="11.38758125" x2="1.98881875" y2="11.47241875" layer="21"/>
<rectangle x1="6.64718125" y1="11.38758125" x2="9.525" y2="11.47241875" layer="21"/>
<rectangle x1="11.38681875" y1="11.38758125" x2="14.605" y2="11.47241875" layer="21"/>
<rectangle x1="18.07718125" y1="11.38758125" x2="20.955" y2="11.47241875" layer="21"/>
<rectangle x1="23.749" y1="11.38758125" x2="26.289" y2="11.47241875" layer="21"/>
<rectangle x1="27.051" y1="11.38758125" x2="29.76118125" y2="11.47241875" layer="21"/>
<rectangle x1="30.52318125" y1="11.38758125" x2="32.21481875" y2="11.47241875" layer="21"/>
<rectangle x1="34.33318125" y1="11.38758125" x2="38.989" y2="11.47241875" layer="21"/>
<rectangle x1="0.71881875" y1="11.47241875" x2="1.905" y2="11.557" layer="21"/>
<rectangle x1="6.477" y1="11.47241875" x2="9.69518125" y2="11.557" layer="21"/>
<rectangle x1="11.303" y1="11.47241875" x2="14.859" y2="11.557" layer="21"/>
<rectangle x1="18.07718125" y1="11.47241875" x2="20.78481875" y2="11.557" layer="21"/>
<rectangle x1="23.91918125" y1="11.47241875" x2="26.20518125" y2="11.557" layer="21"/>
<rectangle x1="27.13481875" y1="11.47241875" x2="29.76118125" y2="11.557" layer="21"/>
<rectangle x1="30.607" y1="11.47241875" x2="32.04718125" y2="11.557" layer="21"/>
<rectangle x1="34.50081875" y1="11.47241875" x2="39.07281875" y2="11.557" layer="21"/>
<rectangle x1="0.71881875" y1="11.557" x2="1.905" y2="11.64158125" layer="21"/>
<rectangle x1="6.39318125" y1="11.557" x2="9.779" y2="11.64158125" layer="21"/>
<rectangle x1="11.303" y1="11.557" x2="14.94281875" y2="11.64158125" layer="21"/>
<rectangle x1="18.07718125" y1="11.557" x2="20.61718125" y2="11.64158125" layer="21"/>
<rectangle x1="24.08681875" y1="11.557" x2="26.20518125" y2="11.64158125" layer="21"/>
<rectangle x1="27.13481875" y1="11.557" x2="29.76118125" y2="11.64158125" layer="21"/>
<rectangle x1="30.607" y1="11.557" x2="31.96081875" y2="11.64158125" layer="21"/>
<rectangle x1="34.671" y1="11.557" x2="39.07281875" y2="11.64158125" layer="21"/>
<rectangle x1="0.635" y1="11.64158125" x2="1.82118125" y2="11.72641875" layer="21"/>
<rectangle x1="6.223" y1="11.64158125" x2="9.94918125" y2="11.72641875" layer="21"/>
<rectangle x1="11.303" y1="11.64158125" x2="15.02918125" y2="11.72641875" layer="21"/>
<rectangle x1="18.07718125" y1="11.64158125" x2="20.53081875" y2="11.72641875" layer="21"/>
<rectangle x1="24.17318125" y1="11.64158125" x2="26.20518125" y2="11.72641875" layer="21"/>
<rectangle x1="27.13481875" y1="11.64158125" x2="29.76118125" y2="11.72641875" layer="21"/>
<rectangle x1="30.607" y1="11.64158125" x2="31.877" y2="11.72641875" layer="21"/>
<rectangle x1="34.75481875" y1="11.64158125" x2="39.07281875" y2="11.72641875" layer="21"/>
<rectangle x1="0.635" y1="11.72641875" x2="1.82118125" y2="11.811" layer="21"/>
<rectangle x1="6.13918125" y1="11.72641875" x2="10.033" y2="11.811" layer="21"/>
<rectangle x1="11.303" y1="11.72641875" x2="15.19681875" y2="11.811" layer="21"/>
<rectangle x1="18.07718125" y1="11.72641875" x2="20.36318125" y2="11.811" layer="21"/>
<rectangle x1="24.34081875" y1="11.72641875" x2="26.20518125" y2="11.811" layer="21"/>
<rectangle x1="27.13481875" y1="11.72641875" x2="29.76118125" y2="11.811" layer="21"/>
<rectangle x1="30.607" y1="11.72641875" x2="31.79318125" y2="11.811" layer="21"/>
<rectangle x1="34.84118125" y1="11.72641875" x2="39.15918125" y2="11.811" layer="21"/>
<rectangle x1="0.635" y1="11.811" x2="1.73481875" y2="11.89558125" layer="21"/>
<rectangle x1="5.969" y1="11.811" x2="10.11681875" y2="11.89558125" layer="21"/>
<rectangle x1="11.38681875" y1="11.811" x2="15.28318125" y2="11.89558125" layer="21"/>
<rectangle x1="18.07718125" y1="11.811" x2="20.27681875" y2="11.89558125" layer="21"/>
<rectangle x1="24.42718125" y1="11.811" x2="26.20518125" y2="11.89558125" layer="21"/>
<rectangle x1="27.13481875" y1="11.811" x2="29.76118125" y2="11.89558125" layer="21"/>
<rectangle x1="30.607" y1="11.811" x2="31.623" y2="11.89558125" layer="21"/>
<rectangle x1="35.00881875" y1="11.811" x2="39.15918125" y2="11.89558125" layer="21"/>
<rectangle x1="0.55118125" y1="11.89558125" x2="1.73481875" y2="11.98041875" layer="21"/>
<rectangle x1="5.88518125" y1="11.89558125" x2="10.287" y2="11.98041875" layer="21"/>
<rectangle x1="11.38681875" y1="11.89558125" x2="15.367" y2="11.98041875" layer="21"/>
<rectangle x1="17.99081875" y1="11.89558125" x2="20.193" y2="11.98041875" layer="21"/>
<rectangle x1="24.511" y1="11.89558125" x2="26.20518125" y2="11.98041875" layer="21"/>
<rectangle x1="27.13481875" y1="11.89558125" x2="29.76118125" y2="11.98041875" layer="21"/>
<rectangle x1="30.607" y1="11.89558125" x2="31.623" y2="11.98041875" layer="21"/>
<rectangle x1="35.00881875" y1="11.89558125" x2="39.15918125" y2="11.98041875" layer="21"/>
<rectangle x1="0.55118125" y1="11.98041875" x2="1.73481875" y2="12.065" layer="21"/>
<rectangle x1="5.79881875" y1="11.98041875" x2="10.37081875" y2="12.065" layer="21"/>
<rectangle x1="11.47318125" y1="11.98041875" x2="15.45081875" y2="12.065" layer="21"/>
<rectangle x1="17.99081875" y1="11.98041875" x2="20.10918125" y2="12.065" layer="21"/>
<rectangle x1="24.59481875" y1="11.98041875" x2="26.20518125" y2="12.065" layer="21"/>
<rectangle x1="27.13481875" y1="11.98041875" x2="29.76118125" y2="12.065" layer="21"/>
<rectangle x1="30.607" y1="11.98041875" x2="31.53918125" y2="12.065" layer="21"/>
<rectangle x1="35.09518125" y1="11.98041875" x2="39.243" y2="12.065" layer="21"/>
<rectangle x1="0.55118125" y1="12.065" x2="1.651" y2="12.14958125" layer="21"/>
<rectangle x1="5.715" y1="12.065" x2="10.45718125" y2="12.14958125" layer="21"/>
<rectangle x1="11.557" y1="12.065" x2="15.53718125" y2="12.14958125" layer="21"/>
<rectangle x1="17.99081875" y1="12.065" x2="20.02281875" y2="12.14958125" layer="21"/>
<rectangle x1="24.68118125" y1="12.065" x2="26.20518125" y2="12.14958125" layer="21"/>
<rectangle x1="27.13481875" y1="12.065" x2="29.76118125" y2="12.14958125" layer="21"/>
<rectangle x1="30.607" y1="12.065" x2="31.45281875" y2="12.14958125" layer="21"/>
<rectangle x1="32.80918125" y1="12.065" x2="33.655" y2="12.14958125" layer="21"/>
<rectangle x1="35.179" y1="12.065" x2="39.243" y2="12.14958125" layer="21"/>
<rectangle x1="0.46481875" y1="12.14958125" x2="1.651" y2="12.23441875" layer="21"/>
<rectangle x1="5.63118125" y1="12.14958125" x2="7.91718125" y2="12.23441875" layer="21"/>
<rectangle x1="8.255" y1="12.14958125" x2="10.541" y2="12.23441875" layer="21"/>
<rectangle x1="13.335" y1="12.14958125" x2="15.621" y2="12.23441875" layer="21"/>
<rectangle x1="17.99081875" y1="12.14958125" x2="19.939" y2="12.23441875" layer="21"/>
<rectangle x1="22.225" y1="12.14958125" x2="22.56281875" y2="12.23441875" layer="21"/>
<rectangle x1="24.765" y1="12.14958125" x2="26.20518125" y2="12.23441875" layer="21"/>
<rectangle x1="27.13481875" y1="12.14958125" x2="29.76118125" y2="12.23441875" layer="21"/>
<rectangle x1="30.607" y1="12.14958125" x2="31.369" y2="12.23441875" layer="21"/>
<rectangle x1="32.639" y1="12.14958125" x2="33.909" y2="12.23441875" layer="21"/>
<rectangle x1="35.179" y1="12.14958125" x2="39.243" y2="12.23441875" layer="21"/>
<rectangle x1="0.46481875" y1="12.23441875" x2="1.651" y2="12.319" layer="21"/>
<rectangle x1="5.54481875" y1="12.23441875" x2="7.40918125" y2="12.319" layer="21"/>
<rectangle x1="8.763" y1="12.23441875" x2="10.62481875" y2="12.319" layer="21"/>
<rectangle x1="13.843" y1="12.23441875" x2="15.70481875" y2="12.319" layer="21"/>
<rectangle x1="17.99081875" y1="12.23441875" x2="19.85518125" y2="12.319" layer="21"/>
<rectangle x1="21.717" y1="12.23441875" x2="22.987" y2="12.319" layer="21"/>
<rectangle x1="24.84881875" y1="12.23441875" x2="26.20518125" y2="12.319" layer="21"/>
<rectangle x1="27.13481875" y1="12.23441875" x2="29.76118125" y2="12.319" layer="21"/>
<rectangle x1="30.607" y1="12.23441875" x2="31.369" y2="12.319" layer="21"/>
<rectangle x1="32.46881875" y1="12.23441875" x2="34.07918125" y2="12.319" layer="21"/>
<rectangle x1="35.179" y1="12.23441875" x2="39.32681875" y2="12.319" layer="21"/>
<rectangle x1="0.46481875" y1="12.319" x2="1.56718125" y2="12.40358125" layer="21"/>
<rectangle x1="5.54481875" y1="12.319" x2="7.239" y2="12.40358125" layer="21"/>
<rectangle x1="8.93318125" y1="12.319" x2="10.62481875" y2="12.40358125" layer="21"/>
<rectangle x1="14.097" y1="12.319" x2="15.79118125" y2="12.40358125" layer="21"/>
<rectangle x1="17.99081875" y1="12.319" x2="19.76881875" y2="12.40358125" layer="21"/>
<rectangle x1="21.463" y1="12.319" x2="23.241" y2="12.40358125" layer="21"/>
<rectangle x1="24.93518125" y1="12.319" x2="26.20518125" y2="12.40358125" layer="21"/>
<rectangle x1="27.13481875" y1="12.319" x2="29.76118125" y2="12.40358125" layer="21"/>
<rectangle x1="30.607" y1="12.319" x2="31.28518125" y2="12.40358125" layer="21"/>
<rectangle x1="32.385" y1="12.319" x2="34.24681875" y2="12.40358125" layer="21"/>
<rectangle x1="35.179" y1="12.319" x2="39.32681875" y2="12.40358125" layer="21"/>
<rectangle x1="0.46481875" y1="12.40358125" x2="1.56718125" y2="12.48841875" layer="21"/>
<rectangle x1="5.461" y1="12.40358125" x2="7.06881875" y2="12.48841875" layer="21"/>
<rectangle x1="9.10081875" y1="12.40358125" x2="10.71118125" y2="12.48841875" layer="21"/>
<rectangle x1="14.26718125" y1="12.40358125" x2="15.875" y2="12.48841875" layer="21"/>
<rectangle x1="17.99081875" y1="12.40358125" x2="19.685" y2="12.48841875" layer="21"/>
<rectangle x1="21.29281875" y1="12.40358125" x2="23.41118125" y2="12.48841875" layer="21"/>
<rectangle x1="25.019" y1="12.40358125" x2="26.20518125" y2="12.48841875" layer="21"/>
<rectangle x1="27.13481875" y1="12.40358125" x2="29.76118125" y2="12.48841875" layer="21"/>
<rectangle x1="30.607" y1="12.40358125" x2="31.28518125" y2="12.48841875" layer="21"/>
<rectangle x1="32.30118125" y1="12.40358125" x2="34.33318125" y2="12.48841875" layer="21"/>
<rectangle x1="35.09518125" y1="12.40358125" x2="39.32681875" y2="12.48841875" layer="21"/>
<rectangle x1="0.46481875" y1="12.48841875" x2="1.56718125" y2="12.573" layer="21"/>
<rectangle x1="5.37718125" y1="12.48841875" x2="6.90118125" y2="12.573" layer="21"/>
<rectangle x1="9.271" y1="12.48841875" x2="10.795" y2="12.573" layer="21"/>
<rectangle x1="14.351" y1="12.48841875" x2="15.875" y2="12.573" layer="21"/>
<rectangle x1="17.907" y1="12.48841875" x2="19.685" y2="12.573" layer="21"/>
<rectangle x1="21.209" y1="12.48841875" x2="23.495" y2="12.573" layer="21"/>
<rectangle x1="25.019" y1="12.48841875" x2="26.20518125" y2="12.573" layer="21"/>
<rectangle x1="27.13481875" y1="12.48841875" x2="29.76118125" y2="12.573" layer="21"/>
<rectangle x1="30.607" y1="12.48841875" x2="31.19881875" y2="12.573" layer="21"/>
<rectangle x1="32.21481875" y1="12.48841875" x2="34.417" y2="12.573" layer="21"/>
<rectangle x1="35.09518125" y1="12.48841875" x2="39.32681875" y2="12.573" layer="21"/>
<rectangle x1="0.381" y1="12.573" x2="1.48081875" y2="12.65758125" layer="21"/>
<rectangle x1="5.29081875" y1="12.573" x2="6.731" y2="12.65758125" layer="21"/>
<rectangle x1="9.35481875" y1="12.573" x2="10.87881875" y2="12.65758125" layer="21"/>
<rectangle x1="14.52118125" y1="12.573" x2="15.95881875" y2="12.65758125" layer="21"/>
<rectangle x1="17.907" y1="12.573" x2="19.60118125" y2="12.65758125" layer="21"/>
<rectangle x1="21.03881875" y1="12.573" x2="23.66518125" y2="12.65758125" layer="21"/>
<rectangle x1="25.10281875" y1="12.573" x2="26.20518125" y2="12.65758125" layer="21"/>
<rectangle x1="27.13481875" y1="12.573" x2="29.76118125" y2="12.65758125" layer="21"/>
<rectangle x1="30.607" y1="12.573" x2="31.19881875" y2="12.65758125" layer="21"/>
<rectangle x1="32.131" y1="12.573" x2="34.50081875" y2="12.65758125" layer="21"/>
<rectangle x1="35.00881875" y1="12.573" x2="39.32681875" y2="12.65758125" layer="21"/>
<rectangle x1="0.381" y1="12.65758125" x2="1.48081875" y2="12.74241875" layer="21"/>
<rectangle x1="5.29081875" y1="12.65758125" x2="6.64718125" y2="12.74241875" layer="21"/>
<rectangle x1="9.525" y1="12.65758125" x2="10.87881875" y2="12.74241875" layer="21"/>
<rectangle x1="14.605" y1="12.65758125" x2="16.04518125" y2="12.74241875" layer="21"/>
<rectangle x1="17.907" y1="12.65758125" x2="19.51481875" y2="12.74241875" layer="21"/>
<rectangle x1="20.955" y1="12.65758125" x2="23.749" y2="12.74241875" layer="21"/>
<rectangle x1="25.18918125" y1="12.65758125" x2="26.20518125" y2="12.74241875" layer="21"/>
<rectangle x1="27.13481875" y1="12.65758125" x2="29.76118125" y2="12.74241875" layer="21"/>
<rectangle x1="30.607" y1="12.65758125" x2="31.19881875" y2="12.74241875" layer="21"/>
<rectangle x1="32.131" y1="12.65758125" x2="39.41318125" y2="12.74241875" layer="21"/>
<rectangle x1="0.381" y1="12.74241875" x2="1.48081875" y2="12.827" layer="21"/>
<rectangle x1="5.207" y1="12.74241875" x2="6.56081875" y2="12.827" layer="21"/>
<rectangle x1="9.60881875" y1="12.74241875" x2="10.87881875" y2="12.827" layer="21"/>
<rectangle x1="14.68881875" y1="12.74241875" x2="16.04518125" y2="12.827" layer="21"/>
<rectangle x1="17.907" y1="12.74241875" x2="19.51481875" y2="12.827" layer="21"/>
<rectangle x1="20.87118125" y1="12.74241875" x2="23.83281875" y2="12.827" layer="21"/>
<rectangle x1="25.18918125" y1="12.74241875" x2="26.20518125" y2="12.827" layer="21"/>
<rectangle x1="27.13481875" y1="12.74241875" x2="29.76118125" y2="12.827" layer="21"/>
<rectangle x1="30.607" y1="12.74241875" x2="31.115" y2="12.827" layer="21"/>
<rectangle x1="32.04718125" y1="12.74241875" x2="39.41318125" y2="12.827" layer="21"/>
<rectangle x1="0.381" y1="12.827" x2="1.48081875" y2="12.91158125" layer="21"/>
<rectangle x1="5.207" y1="12.827" x2="6.477" y2="12.91158125" layer="21"/>
<rectangle x1="9.69518125" y1="12.827" x2="10.96518125" y2="12.91158125" layer="21"/>
<rectangle x1="14.77518125" y1="12.827" x2="16.129" y2="12.91158125" layer="21"/>
<rectangle x1="17.907" y1="12.827" x2="19.431" y2="12.91158125" layer="21"/>
<rectangle x1="20.701" y1="12.827" x2="23.91918125" y2="12.91158125" layer="21"/>
<rectangle x1="25.273" y1="12.827" x2="26.20518125" y2="12.91158125" layer="21"/>
<rectangle x1="27.13481875" y1="12.827" x2="29.76118125" y2="12.91158125" layer="21"/>
<rectangle x1="30.607" y1="12.827" x2="31.115" y2="12.91158125" layer="21"/>
<rectangle x1="32.04718125" y1="12.827" x2="39.41318125" y2="12.91158125" layer="21"/>
<rectangle x1="0.29718125" y1="12.91158125" x2="1.397" y2="12.99641875" layer="21"/>
<rectangle x1="5.12318125" y1="12.91158125" x2="6.39318125" y2="12.99641875" layer="21"/>
<rectangle x1="9.779" y1="12.91158125" x2="11.049" y2="12.99641875" layer="21"/>
<rectangle x1="14.859" y1="12.91158125" x2="16.129" y2="12.99641875" layer="21"/>
<rectangle x1="17.907" y1="12.91158125" x2="19.431" y2="12.99641875" layer="21"/>
<rectangle x1="20.701" y1="12.91158125" x2="24.003" y2="12.99641875" layer="21"/>
<rectangle x1="25.273" y1="12.91158125" x2="26.20518125" y2="12.99641875" layer="21"/>
<rectangle x1="27.13481875" y1="12.91158125" x2="29.76118125" y2="12.99641875" layer="21"/>
<rectangle x1="30.607" y1="12.91158125" x2="31.115" y2="12.99641875" layer="21"/>
<rectangle x1="35.179" y1="12.91158125" x2="39.41318125" y2="12.99641875" layer="21"/>
<rectangle x1="0.29718125" y1="12.99641875" x2="1.397" y2="13.081" layer="21"/>
<rectangle x1="5.12318125" y1="12.99641875" x2="6.30681875" y2="13.081" layer="21"/>
<rectangle x1="9.779" y1="12.99641875" x2="11.049" y2="13.081" layer="21"/>
<rectangle x1="14.94281875" y1="12.99641875" x2="16.21281875" y2="13.081" layer="21"/>
<rectangle x1="17.907" y1="12.99641875" x2="19.34718125" y2="13.081" layer="21"/>
<rectangle x1="20.61718125" y1="12.99641875" x2="24.08681875" y2="13.081" layer="21"/>
<rectangle x1="25.35681875" y1="12.99641875" x2="26.20518125" y2="13.081" layer="21"/>
<rectangle x1="27.13481875" y1="12.99641875" x2="29.76118125" y2="13.081" layer="21"/>
<rectangle x1="30.607" y1="12.99641875" x2="31.115" y2="13.081" layer="21"/>
<rectangle x1="35.26281875" y1="12.99641875" x2="39.41318125" y2="13.081" layer="21"/>
<rectangle x1="0.29718125" y1="13.081" x2="1.397" y2="13.16558125" layer="21"/>
<rectangle x1="5.03681875" y1="13.081" x2="6.30681875" y2="13.16558125" layer="21"/>
<rectangle x1="9.86281875" y1="13.081" x2="11.049" y2="13.16558125" layer="21"/>
<rectangle x1="15.02918125" y1="13.081" x2="16.21281875" y2="13.16558125" layer="21"/>
<rectangle x1="17.907" y1="13.081" x2="19.34718125" y2="13.16558125" layer="21"/>
<rectangle x1="20.53081875" y1="13.081" x2="24.17318125" y2="13.16558125" layer="21"/>
<rectangle x1="25.35681875" y1="13.081" x2="26.20518125" y2="13.16558125" layer="21"/>
<rectangle x1="27.13481875" y1="13.081" x2="29.76118125" y2="13.16558125" layer="21"/>
<rectangle x1="30.607" y1="13.081" x2="31.115" y2="13.16558125" layer="21"/>
<rectangle x1="35.34918125" y1="13.081" x2="39.41318125" y2="13.16558125" layer="21"/>
<rectangle x1="0.29718125" y1="13.16558125" x2="1.397" y2="13.25041875" layer="21"/>
<rectangle x1="5.03681875" y1="13.16558125" x2="6.223" y2="13.25041875" layer="21"/>
<rectangle x1="9.94918125" y1="13.16558125" x2="11.13281875" y2="13.25041875" layer="21"/>
<rectangle x1="15.02918125" y1="13.16558125" x2="16.21281875" y2="13.25041875" layer="21"/>
<rectangle x1="17.907" y1="13.16558125" x2="19.26081875" y2="13.25041875" layer="21"/>
<rectangle x1="20.53081875" y1="13.16558125" x2="24.17318125" y2="13.25041875" layer="21"/>
<rectangle x1="25.44318125" y1="13.16558125" x2="26.20518125" y2="13.25041875" layer="21"/>
<rectangle x1="27.13481875" y1="13.16558125" x2="29.76118125" y2="13.25041875" layer="21"/>
<rectangle x1="30.607" y1="13.16558125" x2="31.03118125" y2="13.25041875" layer="21"/>
<rectangle x1="35.34918125" y1="13.16558125" x2="39.41318125" y2="13.25041875" layer="21"/>
<rectangle x1="0.29718125" y1="13.25041875" x2="1.397" y2="13.335" layer="21"/>
<rectangle x1="5.03681875" y1="13.25041875" x2="6.13918125" y2="13.335" layer="21"/>
<rectangle x1="9.94918125" y1="13.25041875" x2="11.13281875" y2="13.335" layer="21"/>
<rectangle x1="15.113" y1="13.25041875" x2="16.29918125" y2="13.335" layer="21"/>
<rectangle x1="17.907" y1="13.25041875" x2="19.26081875" y2="13.335" layer="21"/>
<rectangle x1="20.447" y1="13.25041875" x2="24.257" y2="13.335" layer="21"/>
<rectangle x1="25.44318125" y1="13.25041875" x2="26.20518125" y2="13.335" layer="21"/>
<rectangle x1="27.13481875" y1="13.25041875" x2="29.76118125" y2="13.335" layer="21"/>
<rectangle x1="30.607" y1="13.25041875" x2="31.03118125" y2="13.335" layer="21"/>
<rectangle x1="35.433" y1="13.25041875" x2="39.41318125" y2="13.335" layer="21"/>
<rectangle x1="0.29718125" y1="13.335" x2="1.31318125" y2="13.41958125" layer="21"/>
<rectangle x1="4.953" y1="13.335" x2="6.13918125" y2="13.41958125" layer="21"/>
<rectangle x1="10.033" y1="13.335" x2="11.21918125" y2="13.41958125" layer="21"/>
<rectangle x1="15.19681875" y1="13.335" x2="16.29918125" y2="13.41958125" layer="21"/>
<rectangle x1="17.907" y1="13.335" x2="19.26081875" y2="13.41958125" layer="21"/>
<rectangle x1="20.36318125" y1="13.335" x2="24.34081875" y2="13.41958125" layer="21"/>
<rectangle x1="25.44318125" y1="13.335" x2="26.20518125" y2="13.41958125" layer="21"/>
<rectangle x1="27.13481875" y1="13.335" x2="29.76118125" y2="13.41958125" layer="21"/>
<rectangle x1="30.607" y1="13.335" x2="31.03118125" y2="13.41958125" layer="21"/>
<rectangle x1="35.433" y1="13.335" x2="39.41318125" y2="13.41958125" layer="21"/>
<rectangle x1="0.29718125" y1="13.41958125" x2="1.31318125" y2="13.50441875" layer="21"/>
<rectangle x1="4.953" y1="13.41958125" x2="6.05281875" y2="13.50441875" layer="21"/>
<rectangle x1="10.033" y1="13.41958125" x2="11.21918125" y2="13.50441875" layer="21"/>
<rectangle x1="15.19681875" y1="13.41958125" x2="16.29918125" y2="13.50441875" layer="21"/>
<rectangle x1="17.907" y1="13.41958125" x2="19.177" y2="13.50441875" layer="21"/>
<rectangle x1="20.36318125" y1="13.41958125" x2="24.34081875" y2="13.50441875" layer="21"/>
<rectangle x1="25.44318125" y1="13.41958125" x2="26.20518125" y2="13.50441875" layer="21"/>
<rectangle x1="27.13481875" y1="13.41958125" x2="29.76118125" y2="13.50441875" layer="21"/>
<rectangle x1="30.607" y1="13.41958125" x2="31.03118125" y2="13.50441875" layer="21"/>
<rectangle x1="35.433" y1="13.41958125" x2="39.41318125" y2="13.50441875" layer="21"/>
<rectangle x1="0.21081875" y1="13.50441875" x2="1.31318125" y2="13.589" layer="21"/>
<rectangle x1="4.953" y1="13.50441875" x2="6.05281875" y2="13.589" layer="21"/>
<rectangle x1="10.11681875" y1="13.50441875" x2="11.21918125" y2="13.589" layer="21"/>
<rectangle x1="15.19681875" y1="13.50441875" x2="16.383" y2="13.589" layer="21"/>
<rectangle x1="17.907" y1="13.50441875" x2="19.177" y2="13.589" layer="21"/>
<rectangle x1="20.36318125" y1="13.50441875" x2="24.34081875" y2="13.589" layer="21"/>
<rectangle x1="25.527" y1="13.50441875" x2="26.20518125" y2="13.589" layer="21"/>
<rectangle x1="27.13481875" y1="13.50441875" x2="29.67481875" y2="13.589" layer="21"/>
<rectangle x1="30.607" y1="13.50441875" x2="31.03118125" y2="13.589" layer="21"/>
<rectangle x1="35.433" y1="13.50441875" x2="39.41318125" y2="13.589" layer="21"/>
<rectangle x1="0.21081875" y1="13.589" x2="1.31318125" y2="13.67358125" layer="21"/>
<rectangle x1="4.86918125" y1="13.589" x2="6.05281875" y2="13.67358125" layer="21"/>
<rectangle x1="10.11681875" y1="13.589" x2="11.21918125" y2="13.67358125" layer="21"/>
<rectangle x1="15.28318125" y1="13.589" x2="16.383" y2="13.67358125" layer="21"/>
<rectangle x1="17.907" y1="13.589" x2="19.177" y2="13.67358125" layer="21"/>
<rectangle x1="20.27681875" y1="13.589" x2="24.42718125" y2="13.67358125" layer="21"/>
<rectangle x1="25.527" y1="13.589" x2="26.20518125" y2="13.67358125" layer="21"/>
<rectangle x1="27.13481875" y1="13.589" x2="29.67481875" y2="13.67358125" layer="21"/>
<rectangle x1="30.607" y1="13.589" x2="31.115" y2="13.67358125" layer="21"/>
<rectangle x1="35.433" y1="13.589" x2="39.41318125" y2="13.67358125" layer="21"/>
<rectangle x1="0.21081875" y1="13.67358125" x2="1.31318125" y2="13.75841875" layer="21"/>
<rectangle x1="4.86918125" y1="13.67358125" x2="5.969" y2="13.75841875" layer="21"/>
<rectangle x1="10.20318125" y1="13.67358125" x2="11.303" y2="13.75841875" layer="21"/>
<rectangle x1="15.28318125" y1="13.67358125" x2="16.383" y2="13.75841875" layer="21"/>
<rectangle x1="17.82318125" y1="13.67358125" x2="19.177" y2="13.75841875" layer="21"/>
<rectangle x1="20.27681875" y1="13.67358125" x2="24.42718125" y2="13.75841875" layer="21"/>
<rectangle x1="25.527" y1="13.67358125" x2="26.20518125" y2="13.75841875" layer="21"/>
<rectangle x1="27.13481875" y1="13.67358125" x2="29.67481875" y2="13.75841875" layer="21"/>
<rectangle x1="30.52318125" y1="13.67358125" x2="31.115" y2="13.75841875" layer="21"/>
<rectangle x1="35.433" y1="13.67358125" x2="39.41318125" y2="13.75841875" layer="21"/>
<rectangle x1="0.21081875" y1="13.75841875" x2="1.31318125" y2="13.843" layer="21"/>
<rectangle x1="4.86918125" y1="13.75841875" x2="5.969" y2="13.843" layer="21"/>
<rectangle x1="10.20318125" y1="13.75841875" x2="11.303" y2="13.843" layer="21"/>
<rectangle x1="15.28318125" y1="13.75841875" x2="16.383" y2="13.843" layer="21"/>
<rectangle x1="17.82318125" y1="13.75841875" x2="19.177" y2="13.843" layer="21"/>
<rectangle x1="20.27681875" y1="13.75841875" x2="24.42718125" y2="13.843" layer="21"/>
<rectangle x1="25.527" y1="13.75841875" x2="26.20518125" y2="13.843" layer="21"/>
<rectangle x1="27.22118125" y1="13.75841875" x2="29.67481875" y2="13.843" layer="21"/>
<rectangle x1="30.52318125" y1="13.75841875" x2="31.115" y2="13.843" layer="21"/>
<rectangle x1="32.04718125" y1="13.75841875" x2="34.417" y2="13.843" layer="21"/>
<rectangle x1="35.34918125" y1="13.75841875" x2="39.41318125" y2="13.843" layer="21"/>
<rectangle x1="0.21081875" y1="13.843" x2="1.31318125" y2="13.92758125" layer="21"/>
<rectangle x1="4.86918125" y1="13.843" x2="5.969" y2="13.92758125" layer="21"/>
<rectangle x1="10.20318125" y1="13.843" x2="11.303" y2="13.92758125" layer="21"/>
<rectangle x1="15.367" y1="13.843" x2="16.46681875" y2="13.92758125" layer="21"/>
<rectangle x1="17.82318125" y1="13.843" x2="19.09318125" y2="13.92758125" layer="21"/>
<rectangle x1="20.193" y1="13.843" x2="24.42718125" y2="13.92758125" layer="21"/>
<rectangle x1="25.61081875" y1="13.843" x2="26.20518125" y2="13.92758125" layer="21"/>
<rectangle x1="27.22118125" y1="13.843" x2="29.591" y2="13.92758125" layer="21"/>
<rectangle x1="30.52318125" y1="13.843" x2="31.115" y2="13.92758125" layer="21"/>
<rectangle x1="32.04718125" y1="13.843" x2="34.417" y2="13.92758125" layer="21"/>
<rectangle x1="35.34918125" y1="13.843" x2="39.41318125" y2="13.92758125" layer="21"/>
<rectangle x1="0.21081875" y1="13.92758125" x2="1.31318125" y2="14.01241875" layer="21"/>
<rectangle x1="4.86918125" y1="13.92758125" x2="5.969" y2="14.01241875" layer="21"/>
<rectangle x1="10.20318125" y1="13.92758125" x2="11.303" y2="14.01241875" layer="21"/>
<rectangle x1="15.367" y1="13.92758125" x2="16.46681875" y2="14.01241875" layer="21"/>
<rectangle x1="17.82318125" y1="13.92758125" x2="19.09318125" y2="14.01241875" layer="21"/>
<rectangle x1="20.193" y1="13.92758125" x2="24.511" y2="14.01241875" layer="21"/>
<rectangle x1="25.61081875" y1="13.92758125" x2="26.20518125" y2="14.01241875" layer="21"/>
<rectangle x1="27.22118125" y1="13.92758125" x2="29.591" y2="14.01241875" layer="21"/>
<rectangle x1="30.52318125" y1="13.92758125" x2="31.115" y2="14.01241875" layer="21"/>
<rectangle x1="32.04718125" y1="13.92758125" x2="34.417" y2="14.01241875" layer="21"/>
<rectangle x1="35.34918125" y1="13.92758125" x2="39.41318125" y2="14.01241875" layer="21"/>
<rectangle x1="0.21081875" y1="14.01241875" x2="1.31318125" y2="14.097" layer="21"/>
<rectangle x1="4.86918125" y1="14.01241875" x2="5.88518125" y2="14.097" layer="21"/>
<rectangle x1="10.20318125" y1="14.01241875" x2="11.303" y2="14.097" layer="21"/>
<rectangle x1="15.367" y1="14.01241875" x2="16.46681875" y2="14.097" layer="21"/>
<rectangle x1="17.82318125" y1="14.01241875" x2="19.09318125" y2="14.097" layer="21"/>
<rectangle x1="20.193" y1="14.01241875" x2="24.511" y2="14.097" layer="21"/>
<rectangle x1="25.61081875" y1="14.01241875" x2="26.20518125" y2="14.097" layer="21"/>
<rectangle x1="27.305" y1="14.01241875" x2="29.50718125" y2="14.097" layer="21"/>
<rectangle x1="30.52318125" y1="14.01241875" x2="31.19881875" y2="14.097" layer="21"/>
<rectangle x1="32.131" y1="14.01241875" x2="34.33318125" y2="14.097" layer="21"/>
<rectangle x1="35.26281875" y1="14.01241875" x2="39.41318125" y2="14.097" layer="21"/>
<rectangle x1="0.21081875" y1="14.097" x2="1.31318125" y2="14.18158125" layer="21"/>
<rectangle x1="4.78281875" y1="14.097" x2="5.88518125" y2="14.18158125" layer="21"/>
<rectangle x1="10.287" y1="14.097" x2="11.303" y2="14.18158125" layer="21"/>
<rectangle x1="15.367" y1="14.097" x2="16.46681875" y2="14.18158125" layer="21"/>
<rectangle x1="17.82318125" y1="14.097" x2="19.09318125" y2="14.18158125" layer="21"/>
<rectangle x1="20.193" y1="14.097" x2="24.511" y2="14.18158125" layer="21"/>
<rectangle x1="25.61081875" y1="14.097" x2="26.20518125" y2="14.18158125" layer="21"/>
<rectangle x1="27.38881875" y1="14.097" x2="29.50718125" y2="14.18158125" layer="21"/>
<rectangle x1="30.43681875" y1="14.097" x2="31.19881875" y2="14.18158125" layer="21"/>
<rectangle x1="32.21481875" y1="14.097" x2="34.33318125" y2="14.18158125" layer="21"/>
<rectangle x1="35.26281875" y1="14.097" x2="39.41318125" y2="14.18158125" layer="21"/>
<rectangle x1="0.21081875" y1="14.18158125" x2="1.31318125" y2="14.26641875" layer="21"/>
<rectangle x1="4.78281875" y1="14.18158125" x2="5.88518125" y2="14.26641875" layer="21"/>
<rectangle x1="10.287" y1="14.18158125" x2="11.303" y2="14.26641875" layer="21"/>
<rectangle x1="15.367" y1="14.18158125" x2="16.46681875" y2="14.26641875" layer="21"/>
<rectangle x1="17.82318125" y1="14.18158125" x2="19.09318125" y2="14.26641875" layer="21"/>
<rectangle x1="20.193" y1="14.18158125" x2="24.511" y2="14.26641875" layer="21"/>
<rectangle x1="25.61081875" y1="14.18158125" x2="26.20518125" y2="14.26641875" layer="21"/>
<rectangle x1="27.38881875" y1="14.18158125" x2="29.42081875" y2="14.26641875" layer="21"/>
<rectangle x1="30.43681875" y1="14.18158125" x2="31.19881875" y2="14.26641875" layer="21"/>
<rectangle x1="32.21481875" y1="14.18158125" x2="34.24681875" y2="14.26641875" layer="21"/>
<rectangle x1="35.26281875" y1="14.18158125" x2="39.41318125" y2="14.26641875" layer="21"/>
<rectangle x1="0.21081875" y1="14.26641875" x2="1.31318125" y2="14.351" layer="21"/>
<rectangle x1="4.78281875" y1="14.26641875" x2="5.88518125" y2="14.351" layer="21"/>
<rectangle x1="10.287" y1="14.26641875" x2="11.303" y2="14.351" layer="21"/>
<rectangle x1="15.367" y1="14.26641875" x2="16.46681875" y2="14.351" layer="21"/>
<rectangle x1="17.82318125" y1="14.26641875" x2="19.09318125" y2="14.351" layer="21"/>
<rectangle x1="20.193" y1="14.26641875" x2="24.511" y2="14.351" layer="21"/>
<rectangle x1="25.61081875" y1="14.26641875" x2="26.20518125" y2="14.351" layer="21"/>
<rectangle x1="27.47518125" y1="14.26641875" x2="29.337" y2="14.351" layer="21"/>
<rectangle x1="30.353" y1="14.26641875" x2="31.28518125" y2="14.351" layer="21"/>
<rectangle x1="32.30118125" y1="14.26641875" x2="34.163" y2="14.351" layer="21"/>
<rectangle x1="35.179" y1="14.26641875" x2="39.41318125" y2="14.351" layer="21"/>
<rectangle x1="0.21081875" y1="14.351" x2="1.31318125" y2="14.43558125" layer="21"/>
<rectangle x1="4.78281875" y1="14.351" x2="5.88518125" y2="14.43558125" layer="21"/>
<rectangle x1="10.287" y1="14.351" x2="11.303" y2="14.43558125" layer="21"/>
<rectangle x1="15.367" y1="14.351" x2="16.46681875" y2="14.43558125" layer="21"/>
<rectangle x1="17.82318125" y1="14.351" x2="19.09318125" y2="14.43558125" layer="21"/>
<rectangle x1="20.193" y1="14.351" x2="24.511" y2="14.43558125" layer="21"/>
<rectangle x1="25.61081875" y1="14.351" x2="26.20518125" y2="14.43558125" layer="21"/>
<rectangle x1="27.559" y1="14.351" x2="29.25318125" y2="14.43558125" layer="21"/>
<rectangle x1="30.353" y1="14.351" x2="31.28518125" y2="14.43558125" layer="21"/>
<rectangle x1="32.385" y1="14.351" x2="34.07918125" y2="14.43558125" layer="21"/>
<rectangle x1="35.179" y1="14.351" x2="39.41318125" y2="14.43558125" layer="21"/>
<rectangle x1="0.21081875" y1="14.43558125" x2="1.31318125" y2="14.52041875" layer="21"/>
<rectangle x1="4.78281875" y1="14.43558125" x2="5.88518125" y2="14.52041875" layer="21"/>
<rectangle x1="10.287" y1="14.43558125" x2="11.303" y2="14.52041875" layer="21"/>
<rectangle x1="15.367" y1="14.43558125" x2="16.46681875" y2="14.52041875" layer="21"/>
<rectangle x1="17.82318125" y1="14.43558125" x2="19.09318125" y2="14.52041875" layer="21"/>
<rectangle x1="20.193" y1="14.43558125" x2="24.511" y2="14.52041875" layer="21"/>
<rectangle x1="25.61081875" y1="14.43558125" x2="26.20518125" y2="14.52041875" layer="21"/>
<rectangle x1="27.72918125" y1="14.43558125" x2="29.083" y2="14.52041875" layer="21"/>
<rectangle x1="30.26918125" y1="14.43558125" x2="31.369" y2="14.52041875" layer="21"/>
<rectangle x1="32.55518125" y1="14.43558125" x2="33.909" y2="14.52041875" layer="21"/>
<rectangle x1="35.09518125" y1="14.43558125" x2="39.41318125" y2="14.52041875" layer="21"/>
<rectangle x1="0.21081875" y1="14.52041875" x2="1.31318125" y2="14.605" layer="21"/>
<rectangle x1="4.78281875" y1="14.52041875" x2="5.88518125" y2="14.605" layer="21"/>
<rectangle x1="10.287" y1="14.52041875" x2="11.303" y2="14.605" layer="21"/>
<rectangle x1="15.367" y1="14.52041875" x2="16.46681875" y2="14.605" layer="21"/>
<rectangle x1="17.82318125" y1="14.52041875" x2="19.09318125" y2="14.605" layer="21"/>
<rectangle x1="20.193" y1="14.52041875" x2="24.511" y2="14.605" layer="21"/>
<rectangle x1="25.61081875" y1="14.52041875" x2="26.20518125" y2="14.605" layer="21"/>
<rectangle x1="27.89681875" y1="14.52041875" x2="28.91281875" y2="14.605" layer="21"/>
<rectangle x1="30.26918125" y1="14.52041875" x2="31.45281875" y2="14.605" layer="21"/>
<rectangle x1="32.72281875" y1="14.52041875" x2="33.82518125" y2="14.605" layer="21"/>
<rectangle x1="35.00881875" y1="14.52041875" x2="39.41318125" y2="14.605" layer="21"/>
<rectangle x1="0.21081875" y1="14.605" x2="1.31318125" y2="14.68958125" layer="21"/>
<rectangle x1="4.78281875" y1="14.605" x2="5.88518125" y2="14.68958125" layer="21"/>
<rectangle x1="10.287" y1="14.605" x2="11.303" y2="14.68958125" layer="21"/>
<rectangle x1="15.367" y1="14.605" x2="16.46681875" y2="14.68958125" layer="21"/>
<rectangle x1="17.82318125" y1="14.605" x2="19.09318125" y2="14.68958125" layer="21"/>
<rectangle x1="20.193" y1="14.605" x2="24.511" y2="14.68958125" layer="21"/>
<rectangle x1="25.61081875" y1="14.605" x2="26.20518125" y2="14.68958125" layer="21"/>
<rectangle x1="28.15081875" y1="14.605" x2="28.65881875" y2="14.68958125" layer="21"/>
<rectangle x1="30.18281875" y1="14.605" x2="31.45281875" y2="14.68958125" layer="21"/>
<rectangle x1="32.97681875" y1="14.605" x2="33.48481875" y2="14.68958125" layer="21"/>
<rectangle x1="35.00881875" y1="14.605" x2="39.41318125" y2="14.68958125" layer="21"/>
<rectangle x1="0.21081875" y1="14.68958125" x2="1.31318125" y2="14.77441875" layer="21"/>
<rectangle x1="4.78281875" y1="14.68958125" x2="5.88518125" y2="14.77441875" layer="21"/>
<rectangle x1="10.287" y1="14.68958125" x2="11.303" y2="14.77441875" layer="21"/>
<rectangle x1="15.367" y1="14.68958125" x2="16.46681875" y2="14.77441875" layer="21"/>
<rectangle x1="17.82318125" y1="14.68958125" x2="19.09318125" y2="14.77441875" layer="21"/>
<rectangle x1="20.193" y1="14.68958125" x2="24.511" y2="14.77441875" layer="21"/>
<rectangle x1="25.61081875" y1="14.68958125" x2="26.20518125" y2="14.77441875" layer="21"/>
<rectangle x1="30.099" y1="14.68958125" x2="31.53918125" y2="14.77441875" layer="21"/>
<rectangle x1="34.925" y1="14.68958125" x2="39.41318125" y2="14.77441875" layer="21"/>
<rectangle x1="0.21081875" y1="14.77441875" x2="1.31318125" y2="14.859" layer="21"/>
<rectangle x1="4.78281875" y1="14.77441875" x2="5.88518125" y2="14.859" layer="21"/>
<rectangle x1="10.287" y1="14.77441875" x2="11.303" y2="14.859" layer="21"/>
<rectangle x1="15.367" y1="14.77441875" x2="16.46681875" y2="14.859" layer="21"/>
<rectangle x1="17.82318125" y1="14.77441875" x2="19.09318125" y2="14.859" layer="21"/>
<rectangle x1="20.193" y1="14.77441875" x2="24.42718125" y2="14.859" layer="21"/>
<rectangle x1="25.61081875" y1="14.77441875" x2="26.20518125" y2="14.859" layer="21"/>
<rectangle x1="30.01518125" y1="14.77441875" x2="31.623" y2="14.859" layer="21"/>
<rectangle x1="34.84118125" y1="14.77441875" x2="39.41318125" y2="14.859" layer="21"/>
<rectangle x1="0.21081875" y1="14.859" x2="1.31318125" y2="14.94358125" layer="21"/>
<rectangle x1="4.78281875" y1="14.859" x2="5.88518125" y2="14.94358125" layer="21"/>
<rectangle x1="10.287" y1="14.859" x2="11.303" y2="14.94358125" layer="21"/>
<rectangle x1="15.28318125" y1="14.859" x2="16.383" y2="14.94358125" layer="21"/>
<rectangle x1="17.82318125" y1="14.859" x2="19.09318125" y2="14.94358125" layer="21"/>
<rectangle x1="20.27681875" y1="14.859" x2="24.42718125" y2="14.94358125" layer="21"/>
<rectangle x1="25.527" y1="14.859" x2="26.20518125" y2="14.94358125" layer="21"/>
<rectangle x1="29.92881875" y1="14.859" x2="31.70681875" y2="14.94358125" layer="21"/>
<rectangle x1="34.75481875" y1="14.859" x2="39.41318125" y2="14.94358125" layer="21"/>
<rectangle x1="0.21081875" y1="14.94358125" x2="1.31318125" y2="15.02841875" layer="21"/>
<rectangle x1="4.78281875" y1="14.94358125" x2="5.88518125" y2="15.02841875" layer="21"/>
<rectangle x1="10.287" y1="14.94358125" x2="11.303" y2="15.02841875" layer="21"/>
<rectangle x1="15.28318125" y1="14.94358125" x2="16.383" y2="15.02841875" layer="21"/>
<rectangle x1="17.82318125" y1="14.94358125" x2="19.177" y2="15.02841875" layer="21"/>
<rectangle x1="20.27681875" y1="14.94358125" x2="24.42718125" y2="15.02841875" layer="21"/>
<rectangle x1="25.527" y1="14.94358125" x2="26.20518125" y2="15.02841875" layer="21"/>
<rectangle x1="29.845" y1="14.94358125" x2="31.79318125" y2="15.02841875" layer="21"/>
<rectangle x1="34.671" y1="14.94358125" x2="39.41318125" y2="15.02841875" layer="21"/>
<rectangle x1="0.21081875" y1="15.02841875" x2="1.31318125" y2="15.113" layer="21"/>
<rectangle x1="4.78281875" y1="15.02841875" x2="5.88518125" y2="15.113" layer="21"/>
<rectangle x1="10.287" y1="15.02841875" x2="11.303" y2="15.113" layer="21"/>
<rectangle x1="15.28318125" y1="15.02841875" x2="16.383" y2="15.113" layer="21"/>
<rectangle x1="17.82318125" y1="15.02841875" x2="19.177" y2="15.113" layer="21"/>
<rectangle x1="20.27681875" y1="15.02841875" x2="24.42718125" y2="15.113" layer="21"/>
<rectangle x1="25.527" y1="15.02841875" x2="26.20518125" y2="15.113" layer="21"/>
<rectangle x1="29.76118125" y1="15.02841875" x2="31.877" y2="15.113" layer="21"/>
<rectangle x1="34.58718125" y1="15.02841875" x2="39.41318125" y2="15.113" layer="21"/>
<rectangle x1="0.29718125" y1="15.113" x2="1.31318125" y2="15.19758125" layer="21"/>
<rectangle x1="4.78281875" y1="15.113" x2="5.88518125" y2="15.19758125" layer="21"/>
<rectangle x1="10.287" y1="15.113" x2="11.303" y2="15.19758125" layer="21"/>
<rectangle x1="15.19681875" y1="15.113" x2="16.383" y2="15.19758125" layer="21"/>
<rectangle x1="17.907" y1="15.113" x2="19.177" y2="15.19758125" layer="21"/>
<rectangle x1="20.36318125" y1="15.113" x2="24.34081875" y2="15.19758125" layer="21"/>
<rectangle x1="25.527" y1="15.113" x2="26.20518125" y2="15.19758125" layer="21"/>
<rectangle x1="27.051" y1="15.113" x2="27.22118125" y2="15.19758125" layer="21"/>
<rectangle x1="29.67481875" y1="15.113" x2="32.04718125" y2="15.19758125" layer="21"/>
<rectangle x1="34.50081875" y1="15.113" x2="39.41318125" y2="15.19758125" layer="21"/>
<rectangle x1="0.29718125" y1="15.19758125" x2="1.31318125" y2="15.28241875" layer="21"/>
<rectangle x1="4.78281875" y1="15.19758125" x2="5.88518125" y2="15.28241875" layer="21"/>
<rectangle x1="10.287" y1="15.19758125" x2="11.303" y2="15.28241875" layer="21"/>
<rectangle x1="15.19681875" y1="15.19758125" x2="16.29918125" y2="15.28241875" layer="21"/>
<rectangle x1="17.907" y1="15.19758125" x2="19.177" y2="15.28241875" layer="21"/>
<rectangle x1="20.36318125" y1="15.19758125" x2="24.34081875" y2="15.28241875" layer="21"/>
<rectangle x1="25.44318125" y1="15.19758125" x2="26.289" y2="15.28241875" layer="21"/>
<rectangle x1="27.051" y1="15.19758125" x2="27.305" y2="15.28241875" layer="21"/>
<rectangle x1="29.50718125" y1="15.19758125" x2="32.131" y2="15.28241875" layer="21"/>
<rectangle x1="34.33318125" y1="15.19758125" x2="39.41318125" y2="15.28241875" layer="21"/>
<rectangle x1="0.29718125" y1="15.28241875" x2="1.397" y2="15.367" layer="21"/>
<rectangle x1="4.78281875" y1="15.28241875" x2="5.88518125" y2="15.367" layer="21"/>
<rectangle x1="10.287" y1="15.28241875" x2="11.303" y2="15.367" layer="21"/>
<rectangle x1="15.113" y1="15.28241875" x2="16.29918125" y2="15.367" layer="21"/>
<rectangle x1="17.907" y1="15.28241875" x2="19.26081875" y2="15.367" layer="21"/>
<rectangle x1="20.36318125" y1="15.28241875" x2="24.257" y2="15.367" layer="21"/>
<rectangle x1="25.44318125" y1="15.28241875" x2="26.289" y2="15.367" layer="21"/>
<rectangle x1="27.051" y1="15.28241875" x2="27.47518125" y2="15.367" layer="21"/>
<rectangle x1="29.337" y1="15.28241875" x2="32.30118125" y2="15.367" layer="21"/>
<rectangle x1="34.163" y1="15.28241875" x2="39.41318125" y2="15.367" layer="21"/>
<rectangle x1="0.29718125" y1="15.367" x2="1.397" y2="15.45158125" layer="21"/>
<rectangle x1="4.78281875" y1="15.367" x2="5.88518125" y2="15.45158125" layer="21"/>
<rectangle x1="10.287" y1="15.367" x2="11.303" y2="15.45158125" layer="21"/>
<rectangle x1="15.113" y1="15.367" x2="16.29918125" y2="15.45158125" layer="21"/>
<rectangle x1="17.907" y1="15.367" x2="19.26081875" y2="15.45158125" layer="21"/>
<rectangle x1="20.447" y1="15.367" x2="24.257" y2="15.45158125" layer="21"/>
<rectangle x1="25.44318125" y1="15.367" x2="26.37281875" y2="15.45158125" layer="21"/>
<rectangle x1="26.96718125" y1="15.367" x2="27.64281875" y2="15.45158125" layer="21"/>
<rectangle x1="29.16681875" y1="15.367" x2="32.46881875" y2="15.45158125" layer="21"/>
<rectangle x1="33.99281875" y1="15.367" x2="39.41318125" y2="15.45158125" layer="21"/>
<rectangle x1="0.29718125" y1="15.45158125" x2="1.397" y2="15.53641875" layer="21"/>
<rectangle x1="4.78281875" y1="15.45158125" x2="5.88518125" y2="15.53641875" layer="21"/>
<rectangle x1="10.287" y1="15.45158125" x2="11.303" y2="15.53641875" layer="21"/>
<rectangle x1="15.02918125" y1="15.45158125" x2="16.21281875" y2="15.53641875" layer="21"/>
<rectangle x1="17.907" y1="15.45158125" x2="19.26081875" y2="15.53641875" layer="21"/>
<rectangle x1="20.53081875" y1="15.45158125" x2="24.17318125" y2="15.53641875" layer="21"/>
<rectangle x1="25.35681875" y1="15.45158125" x2="26.45918125" y2="15.53641875" layer="21"/>
<rectangle x1="26.88081875" y1="15.45158125" x2="27.98318125" y2="15.53641875" layer="21"/>
<rectangle x1="28.91281875" y1="15.45158125" x2="32.80918125" y2="15.53641875" layer="21"/>
<rectangle x1="33.655" y1="15.45158125" x2="39.41318125" y2="15.53641875" layer="21"/>
<rectangle x1="0.29718125" y1="15.53641875" x2="1.397" y2="15.621" layer="21"/>
<rectangle x1="4.78281875" y1="15.53641875" x2="5.88518125" y2="15.621" layer="21"/>
<rectangle x1="10.287" y1="15.53641875" x2="11.303" y2="15.621" layer="21"/>
<rectangle x1="15.02918125" y1="15.53641875" x2="16.21281875" y2="15.621" layer="21"/>
<rectangle x1="17.907" y1="15.53641875" x2="19.34718125" y2="15.621" layer="21"/>
<rectangle x1="20.53081875" y1="15.53641875" x2="24.17318125" y2="15.621" layer="21"/>
<rectangle x1="25.35681875" y1="15.53641875" x2="39.41318125" y2="15.621" layer="21"/>
<rectangle x1="0.29718125" y1="15.621" x2="1.397" y2="15.70558125" layer="21"/>
<rectangle x1="4.78281875" y1="15.621" x2="5.88518125" y2="15.70558125" layer="21"/>
<rectangle x1="10.287" y1="15.621" x2="11.303" y2="15.70558125" layer="21"/>
<rectangle x1="14.94281875" y1="15.621" x2="16.21281875" y2="15.70558125" layer="21"/>
<rectangle x1="17.907" y1="15.621" x2="19.34718125" y2="15.70558125" layer="21"/>
<rectangle x1="20.61718125" y1="15.621" x2="24.08681875" y2="15.70558125" layer="21"/>
<rectangle x1="25.35681875" y1="15.621" x2="39.41318125" y2="15.70558125" layer="21"/>
<rectangle x1="0.29718125" y1="15.70558125" x2="1.397" y2="15.79041875" layer="21"/>
<rectangle x1="4.78281875" y1="15.70558125" x2="5.88518125" y2="15.79041875" layer="21"/>
<rectangle x1="10.287" y1="15.70558125" x2="11.303" y2="15.79041875" layer="21"/>
<rectangle x1="14.859" y1="15.70558125" x2="16.129" y2="15.79041875" layer="21"/>
<rectangle x1="17.907" y1="15.70558125" x2="19.431" y2="15.79041875" layer="21"/>
<rectangle x1="20.701" y1="15.70558125" x2="24.003" y2="15.79041875" layer="21"/>
<rectangle x1="25.273" y1="15.70558125" x2="39.41318125" y2="15.79041875" layer="21"/>
<rectangle x1="0.381" y1="15.79041875" x2="1.48081875" y2="15.875" layer="21"/>
<rectangle x1="4.78281875" y1="15.79041875" x2="5.88518125" y2="15.875" layer="21"/>
<rectangle x1="10.287" y1="15.79041875" x2="11.303" y2="15.875" layer="21"/>
<rectangle x1="14.77518125" y1="15.79041875" x2="16.129" y2="15.875" layer="21"/>
<rectangle x1="17.907" y1="15.79041875" x2="19.431" y2="15.875" layer="21"/>
<rectangle x1="20.78481875" y1="15.79041875" x2="23.91918125" y2="15.875" layer="21"/>
<rectangle x1="25.273" y1="15.79041875" x2="39.41318125" y2="15.875" layer="21"/>
<rectangle x1="0.381" y1="15.875" x2="1.48081875" y2="15.95958125" layer="21"/>
<rectangle x1="4.78281875" y1="15.875" x2="5.88518125" y2="15.95958125" layer="21"/>
<rectangle x1="10.287" y1="15.875" x2="11.303" y2="15.95958125" layer="21"/>
<rectangle x1="14.68881875" y1="15.875" x2="16.04518125" y2="15.95958125" layer="21"/>
<rectangle x1="17.907" y1="15.875" x2="19.51481875" y2="15.95958125" layer="21"/>
<rectangle x1="20.87118125" y1="15.875" x2="23.83281875" y2="15.95958125" layer="21"/>
<rectangle x1="25.18918125" y1="15.875" x2="39.41318125" y2="15.95958125" layer="21"/>
<rectangle x1="0.381" y1="15.95958125" x2="1.48081875" y2="16.04441875" layer="21"/>
<rectangle x1="4.78281875" y1="15.95958125" x2="5.88518125" y2="16.04441875" layer="21"/>
<rectangle x1="10.287" y1="15.95958125" x2="11.303" y2="16.04441875" layer="21"/>
<rectangle x1="14.605" y1="15.95958125" x2="16.04518125" y2="16.04441875" layer="21"/>
<rectangle x1="17.907" y1="15.95958125" x2="19.51481875" y2="16.04441875" layer="21"/>
<rectangle x1="20.955" y1="15.95958125" x2="23.749" y2="16.04441875" layer="21"/>
<rectangle x1="25.10281875" y1="15.95958125" x2="39.41318125" y2="16.04441875" layer="21"/>
<rectangle x1="0.381" y1="16.04441875" x2="1.48081875" y2="16.129" layer="21"/>
<rectangle x1="4.78281875" y1="16.04441875" x2="5.88518125" y2="16.129" layer="21"/>
<rectangle x1="10.287" y1="16.04441875" x2="11.303" y2="16.129" layer="21"/>
<rectangle x1="14.52118125" y1="16.04441875" x2="15.95881875" y2="16.129" layer="21"/>
<rectangle x1="17.907" y1="16.04441875" x2="19.60118125" y2="16.129" layer="21"/>
<rectangle x1="21.03881875" y1="16.04441875" x2="23.66518125" y2="16.129" layer="21"/>
<rectangle x1="25.10281875" y1="16.04441875" x2="39.32681875" y2="16.129" layer="21"/>
<rectangle x1="0.381" y1="16.129" x2="1.56718125" y2="16.21358125" layer="21"/>
<rectangle x1="4.78281875" y1="16.129" x2="5.88518125" y2="16.21358125" layer="21"/>
<rectangle x1="10.287" y1="16.129" x2="11.303" y2="16.21358125" layer="21"/>
<rectangle x1="14.351" y1="16.129" x2="15.875" y2="16.21358125" layer="21"/>
<rectangle x1="17.907" y1="16.129" x2="19.685" y2="16.21358125" layer="21"/>
<rectangle x1="21.209" y1="16.129" x2="23.495" y2="16.21358125" layer="21"/>
<rectangle x1="25.019" y1="16.129" x2="39.32681875" y2="16.21358125" layer="21"/>
<rectangle x1="0.46481875" y1="16.21358125" x2="1.56718125" y2="16.29841875" layer="21"/>
<rectangle x1="4.78281875" y1="16.21358125" x2="5.88518125" y2="16.29841875" layer="21"/>
<rectangle x1="10.287" y1="16.21358125" x2="11.303" y2="16.29841875" layer="21"/>
<rectangle x1="14.18081875" y1="16.21358125" x2="15.875" y2="16.29841875" layer="21"/>
<rectangle x1="17.99081875" y1="16.21358125" x2="19.685" y2="16.29841875" layer="21"/>
<rectangle x1="21.29281875" y1="16.21358125" x2="23.41118125" y2="16.29841875" layer="21"/>
<rectangle x1="24.93518125" y1="16.21358125" x2="39.32681875" y2="16.29841875" layer="21"/>
<rectangle x1="0.46481875" y1="16.29841875" x2="1.56718125" y2="16.383" layer="21"/>
<rectangle x1="4.78281875" y1="16.29841875" x2="5.88518125" y2="16.383" layer="21"/>
<rectangle x1="10.287" y1="16.29841875" x2="11.303" y2="16.383" layer="21"/>
<rectangle x1="14.097" y1="16.29841875" x2="15.79118125" y2="16.383" layer="21"/>
<rectangle x1="17.99081875" y1="16.29841875" x2="19.76881875" y2="16.383" layer="21"/>
<rectangle x1="21.463" y1="16.29841875" x2="23.241" y2="16.383" layer="21"/>
<rectangle x1="24.93518125" y1="16.29841875" x2="39.32681875" y2="16.383" layer="21"/>
<rectangle x1="0.46481875" y1="16.383" x2="1.651" y2="16.46758125" layer="21"/>
<rectangle x1="4.78281875" y1="16.383" x2="5.88518125" y2="16.46758125" layer="21"/>
<rectangle x1="10.287" y1="16.383" x2="11.303" y2="16.46758125" layer="21"/>
<rectangle x1="13.843" y1="16.383" x2="15.70481875" y2="16.46758125" layer="21"/>
<rectangle x1="17.99081875" y1="16.383" x2="19.85518125" y2="16.46758125" layer="21"/>
<rectangle x1="21.80081875" y1="16.383" x2="22.987" y2="16.46758125" layer="21"/>
<rectangle x1="24.84881875" y1="16.383" x2="39.32681875" y2="16.46758125" layer="21"/>
<rectangle x1="0.46481875" y1="16.46758125" x2="1.651" y2="16.55241875" layer="21"/>
<rectangle x1="4.78281875" y1="16.46758125" x2="5.88518125" y2="16.55241875" layer="21"/>
<rectangle x1="10.287" y1="16.46758125" x2="11.47318125" y2="16.55241875" layer="21"/>
<rectangle x1="13.25118125" y1="16.46758125" x2="15.621" y2="16.55241875" layer="21"/>
<rectangle x1="17.99081875" y1="16.46758125" x2="19.939" y2="16.55241875" layer="21"/>
<rectangle x1="24.765" y1="16.46758125" x2="39.243" y2="16.55241875" layer="21"/>
<rectangle x1="0.55118125" y1="16.55241875" x2="1.651" y2="16.637" layer="21"/>
<rectangle x1="4.78281875" y1="16.55241875" x2="5.88518125" y2="16.637" layer="21"/>
<rectangle x1="10.287" y1="16.55241875" x2="15.53718125" y2="16.637" layer="21"/>
<rectangle x1="17.99081875" y1="16.55241875" x2="20.02281875" y2="16.637" layer="21"/>
<rectangle x1="24.68118125" y1="16.55241875" x2="39.243" y2="16.637" layer="21"/>
<rectangle x1="0.55118125" y1="16.637" x2="1.73481875" y2="16.72158125" layer="21"/>
<rectangle x1="4.78281875" y1="16.637" x2="5.88518125" y2="16.72158125" layer="21"/>
<rectangle x1="10.287" y1="16.637" x2="15.45081875" y2="16.72158125" layer="21"/>
<rectangle x1="17.99081875" y1="16.637" x2="20.10918125" y2="16.72158125" layer="21"/>
<rectangle x1="24.59481875" y1="16.637" x2="39.243" y2="16.72158125" layer="21"/>
<rectangle x1="0.55118125" y1="16.72158125" x2="1.73481875" y2="16.80641875" layer="21"/>
<rectangle x1="4.78281875" y1="16.72158125" x2="5.88518125" y2="16.80641875" layer="21"/>
<rectangle x1="10.287" y1="16.72158125" x2="15.367" y2="16.80641875" layer="21"/>
<rectangle x1="17.99081875" y1="16.72158125" x2="20.193" y2="16.80641875" layer="21"/>
<rectangle x1="24.511" y1="16.72158125" x2="39.15918125" y2="16.80641875" layer="21"/>
<rectangle x1="0.635" y1="16.80641875" x2="1.73481875" y2="16.891" layer="21"/>
<rectangle x1="4.78281875" y1="16.80641875" x2="5.88518125" y2="16.891" layer="21"/>
<rectangle x1="10.287" y1="16.80641875" x2="15.28318125" y2="16.891" layer="21"/>
<rectangle x1="18.07718125" y1="16.80641875" x2="20.27681875" y2="16.891" layer="21"/>
<rectangle x1="24.42718125" y1="16.80641875" x2="39.15918125" y2="16.891" layer="21"/>
<rectangle x1="0.635" y1="16.891" x2="1.82118125" y2="16.97558125" layer="21"/>
<rectangle x1="4.78281875" y1="16.891" x2="5.88518125" y2="16.97558125" layer="21"/>
<rectangle x1="10.287" y1="16.891" x2="15.19681875" y2="16.97558125" layer="21"/>
<rectangle x1="18.07718125" y1="16.891" x2="20.36318125" y2="16.97558125" layer="21"/>
<rectangle x1="24.34081875" y1="16.891" x2="39.15918125" y2="16.97558125" layer="21"/>
<rectangle x1="0.635" y1="16.97558125" x2="1.82118125" y2="17.06041875" layer="21"/>
<rectangle x1="4.78281875" y1="16.97558125" x2="5.88518125" y2="17.06041875" layer="21"/>
<rectangle x1="10.20318125" y1="16.97558125" x2="15.02918125" y2="17.06041875" layer="21"/>
<rectangle x1="18.07718125" y1="16.97558125" x2="20.53081875" y2="17.06041875" layer="21"/>
<rectangle x1="24.17318125" y1="16.97558125" x2="39.07281875" y2="17.06041875" layer="21"/>
<rectangle x1="0.71881875" y1="17.06041875" x2="1.905" y2="17.145" layer="21"/>
<rectangle x1="4.78281875" y1="17.06041875" x2="5.88518125" y2="17.145" layer="21"/>
<rectangle x1="10.287" y1="17.06041875" x2="14.94281875" y2="17.145" layer="21"/>
<rectangle x1="18.07718125" y1="17.06041875" x2="20.61718125" y2="17.145" layer="21"/>
<rectangle x1="24.08681875" y1="17.06041875" x2="39.07281875" y2="17.145" layer="21"/>
<rectangle x1="0.71881875" y1="17.145" x2="1.905" y2="17.22958125" layer="21"/>
<rectangle x1="4.86918125" y1="17.145" x2="5.88518125" y2="17.22958125" layer="21"/>
<rectangle x1="10.287" y1="17.145" x2="14.77518125" y2="17.22958125" layer="21"/>
<rectangle x1="18.07718125" y1="17.145" x2="20.78481875" y2="17.22958125" layer="21"/>
<rectangle x1="23.91918125" y1="17.145" x2="39.07281875" y2="17.22958125" layer="21"/>
<rectangle x1="0.80518125" y1="17.22958125" x2="1.98881875" y2="17.31441875" layer="21"/>
<rectangle x1="4.86918125" y1="17.22958125" x2="5.88518125" y2="17.31441875" layer="21"/>
<rectangle x1="10.287" y1="17.22958125" x2="14.605" y2="17.31441875" layer="21"/>
<rectangle x1="18.07718125" y1="17.22958125" x2="20.955" y2="17.31441875" layer="21"/>
<rectangle x1="23.749" y1="17.22958125" x2="38.989" y2="17.31441875" layer="21"/>
<rectangle x1="0.80518125" y1="17.31441875" x2="1.98881875" y2="17.399" layer="21"/>
<rectangle x1="4.86918125" y1="17.31441875" x2="5.79881875" y2="17.399" layer="21"/>
<rectangle x1="10.37081875" y1="17.31441875" x2="14.43481875" y2="17.399" layer="21"/>
<rectangle x1="18.161" y1="17.31441875" x2="21.12518125" y2="17.399" layer="21"/>
<rectangle x1="23.57881875" y1="17.31441875" x2="38.989" y2="17.399" layer="21"/>
<rectangle x1="0.80518125" y1="17.399" x2="2.07518125" y2="17.48358125" layer="21"/>
<rectangle x1="4.953" y1="17.399" x2="5.715" y2="17.48358125" layer="21"/>
<rectangle x1="10.37081875" y1="17.399" x2="14.18081875" y2="17.48358125" layer="21"/>
<rectangle x1="18.161" y1="17.399" x2="21.37918125" y2="17.48358125" layer="21"/>
<rectangle x1="23.32481875" y1="17.399" x2="38.90518125" y2="17.48358125" layer="21"/>
<rectangle x1="0.889" y1="17.48358125" x2="2.07518125" y2="17.56841875" layer="21"/>
<rectangle x1="5.12318125" y1="17.48358125" x2="5.63118125" y2="17.56841875" layer="21"/>
<rectangle x1="10.541" y1="17.48358125" x2="13.843" y2="17.56841875" layer="21"/>
<rectangle x1="18.161" y1="17.48358125" x2="21.717" y2="17.56841875" layer="21"/>
<rectangle x1="22.987" y1="17.48358125" x2="38.90518125" y2="17.56841875" layer="21"/>
<rectangle x1="0.97281875" y1="17.56841875" x2="2.159" y2="17.653" layer="21"/>
<rectangle x1="18.161" y1="17.56841875" x2="38.81881875" y2="17.653" layer="21"/>
<rectangle x1="0.97281875" y1="17.653" x2="2.159" y2="17.73758125" layer="21"/>
<rectangle x1="18.161" y1="17.653" x2="38.81881875" y2="17.73758125" layer="21"/>
<rectangle x1="0.97281875" y1="17.73758125" x2="2.24281875" y2="17.82241875" layer="21"/>
<rectangle x1="18.24481875" y1="17.73758125" x2="38.81881875" y2="17.82241875" layer="21"/>
<rectangle x1="1.05918125" y1="17.82241875" x2="2.24281875" y2="17.907" layer="21"/>
<rectangle x1="18.24481875" y1="17.82241875" x2="38.735" y2="17.907" layer="21"/>
<rectangle x1="1.05918125" y1="17.907" x2="2.32918125" y2="17.99158125" layer="21"/>
<rectangle x1="18.24481875" y1="17.907" x2="38.735" y2="17.99158125" layer="21"/>
<rectangle x1="1.143" y1="17.99158125" x2="2.413" y2="18.07641875" layer="21"/>
<rectangle x1="18.24481875" y1="17.99158125" x2="38.65118125" y2="18.07641875" layer="21"/>
<rectangle x1="1.143" y1="18.07641875" x2="2.413" y2="18.161" layer="21"/>
<rectangle x1="18.24481875" y1="18.07641875" x2="38.56481875" y2="18.161" layer="21"/>
<rectangle x1="1.22681875" y1="18.161" x2="2.49681875" y2="18.24558125" layer="21"/>
<rectangle x1="18.33118125" y1="18.161" x2="38.56481875" y2="18.24558125" layer="21"/>
<rectangle x1="1.22681875" y1="18.24558125" x2="2.58318125" y2="18.33041875" layer="21"/>
<rectangle x1="18.33118125" y1="18.24558125" x2="38.481" y2="18.33041875" layer="21"/>
<rectangle x1="1.31318125" y1="18.33041875" x2="2.667" y2="18.415" layer="21"/>
<rectangle x1="18.33118125" y1="18.33041875" x2="38.481" y2="18.415" layer="21"/>
<rectangle x1="1.397" y1="18.415" x2="2.667" y2="18.49958125" layer="21"/>
<rectangle x1="18.33118125" y1="18.415" x2="38.39718125" y2="18.49958125" layer="21"/>
<rectangle x1="1.397" y1="18.49958125" x2="2.75081875" y2="18.58441875" layer="21"/>
<rectangle x1="18.415" y1="18.49958125" x2="38.31081875" y2="18.58441875" layer="21"/>
<rectangle x1="1.48081875" y1="18.58441875" x2="2.83718125" y2="18.669" layer="21"/>
<rectangle x1="18.415" y1="18.58441875" x2="38.31081875" y2="18.669" layer="21"/>
<rectangle x1="1.56718125" y1="18.669" x2="2.921" y2="18.75358125" layer="21"/>
<rectangle x1="18.415" y1="18.669" x2="38.227" y2="18.75358125" layer="21"/>
<rectangle x1="1.56718125" y1="18.75358125" x2="3.00481875" y2="18.83841875" layer="21"/>
<rectangle x1="18.49881875" y1="18.75358125" x2="38.227" y2="18.83841875" layer="21"/>
<rectangle x1="1.651" y1="18.83841875" x2="3.00481875" y2="18.923" layer="21"/>
<rectangle x1="18.49881875" y1="18.83841875" x2="38.14318125" y2="18.923" layer="21"/>
<rectangle x1="1.73481875" y1="18.923" x2="3.09118125" y2="19.00758125" layer="21"/>
<rectangle x1="18.49881875" y1="18.923" x2="38.05681875" y2="19.00758125" layer="21"/>
<rectangle x1="1.73481875" y1="19.00758125" x2="3.175" y2="19.09241875" layer="21"/>
<rectangle x1="18.49881875" y1="19.00758125" x2="37.973" y2="19.09241875" layer="21"/>
<rectangle x1="1.82118125" y1="19.09241875" x2="3.25881875" y2="19.177" layer="21"/>
<rectangle x1="18.58518125" y1="19.09241875" x2="37.973" y2="19.177" layer="21"/>
<rectangle x1="1.905" y1="19.177" x2="3.34518125" y2="19.26158125" layer="21"/>
<rectangle x1="18.58518125" y1="19.177" x2="37.88918125" y2="19.26158125" layer="21"/>
<rectangle x1="1.98881875" y1="19.26158125" x2="3.429" y2="19.34641875" layer="21"/>
<rectangle x1="18.669" y1="19.26158125" x2="37.80281875" y2="19.34641875" layer="21"/>
<rectangle x1="2.07518125" y1="19.34641875" x2="3.51281875" y2="19.431" layer="21"/>
<rectangle x1="18.669" y1="19.34641875" x2="37.719" y2="19.431" layer="21"/>
<rectangle x1="2.07518125" y1="19.431" x2="3.683" y2="19.51558125" layer="21"/>
<rectangle x1="18.669" y1="19.431" x2="37.63518125" y2="19.51558125" layer="21"/>
<rectangle x1="2.159" y1="19.51558125" x2="3.76681875" y2="19.60041875" layer="21"/>
<rectangle x1="18.669" y1="19.51558125" x2="37.54881875" y2="19.60041875" layer="21"/>
<rectangle x1="2.24281875" y1="19.60041875" x2="3.85318125" y2="19.685" layer="21"/>
<rectangle x1="18.75281875" y1="19.60041875" x2="37.465" y2="19.685" layer="21"/>
<rectangle x1="2.32918125" y1="19.685" x2="3.937" y2="19.76958125" layer="21"/>
<rectangle x1="18.75281875" y1="19.685" x2="37.465" y2="19.76958125" layer="21"/>
<rectangle x1="2.413" y1="19.76958125" x2="4.10718125" y2="19.85441875" layer="21"/>
<rectangle x1="18.83918125" y1="19.76958125" x2="37.38118125" y2="19.85441875" layer="21"/>
<rectangle x1="2.49681875" y1="19.85441875" x2="4.191" y2="19.939" layer="21"/>
<rectangle x1="18.83918125" y1="19.85441875" x2="37.29481875" y2="19.939" layer="21"/>
<rectangle x1="2.58318125" y1="19.939" x2="4.27481875" y2="20.02358125" layer="21"/>
<rectangle x1="18.923" y1="19.939" x2="37.211" y2="20.02358125" layer="21"/>
<rectangle x1="2.667" y1="20.02358125" x2="4.445" y2="20.10841875" layer="21"/>
<rectangle x1="18.923" y1="20.02358125" x2="37.12718125" y2="20.10841875" layer="21"/>
<rectangle x1="2.75081875" y1="20.10841875" x2="4.52881875" y2="20.193" layer="21"/>
<rectangle x1="19.00681875" y1="20.10841875" x2="36.957" y2="20.193" layer="21"/>
<rectangle x1="2.83718125" y1="20.193" x2="4.699" y2="20.27758125" layer="21"/>
<rectangle x1="19.00681875" y1="20.193" x2="36.87318125" y2="20.27758125" layer="21"/>
<rectangle x1="3.00481875" y1="20.27758125" x2="4.86918125" y2="20.36241875" layer="21"/>
<rectangle x1="19.09318125" y1="20.27758125" x2="36.78681875" y2="20.36241875" layer="21"/>
<rectangle x1="3.09118125" y1="20.36241875" x2="5.03681875" y2="20.447" layer="21"/>
<rectangle x1="19.09318125" y1="20.36241875" x2="36.703" y2="20.447" layer="21"/>
<rectangle x1="3.175" y1="20.447" x2="5.207" y2="20.53158125" layer="21"/>
<rectangle x1="19.177" y1="20.447" x2="36.61918125" y2="20.53158125" layer="21"/>
<rectangle x1="3.25881875" y1="20.53158125" x2="5.37718125" y2="20.61641875" layer="21"/>
<rectangle x1="19.177" y1="20.53158125" x2="36.53281875" y2="20.61641875" layer="21"/>
<rectangle x1="3.429" y1="20.61641875" x2="5.54481875" y2="20.701" layer="21"/>
<rectangle x1="19.26081875" y1="20.61641875" x2="36.36518125" y2="20.701" layer="21"/>
<rectangle x1="3.51281875" y1="20.701" x2="5.79881875" y2="20.78558125" layer="21"/>
<rectangle x1="19.26081875" y1="20.701" x2="36.27881875" y2="20.78558125" layer="21"/>
<rectangle x1="3.683" y1="20.78558125" x2="6.05281875" y2="20.87041875" layer="21"/>
<rectangle x1="19.34718125" y1="20.78558125" x2="36.195" y2="20.87041875" layer="21"/>
<rectangle x1="3.76681875" y1="20.87041875" x2="6.30681875" y2="20.955" layer="21"/>
<rectangle x1="19.34718125" y1="20.87041875" x2="36.02481875" y2="20.955" layer="21"/>
<rectangle x1="3.85318125" y1="20.955" x2="6.731" y2="21.03958125" layer="21"/>
<rectangle x1="19.431" y1="20.955" x2="35.85718125" y2="21.03958125" layer="21"/>
<rectangle x1="4.02081875" y1="21.03958125" x2="7.239" y2="21.12441875" layer="21"/>
<rectangle x1="19.51481875" y1="21.03958125" x2="35.77081875" y2="21.12441875" layer="21"/>
<rectangle x1="4.191" y1="21.12441875" x2="35.60318125" y2="21.209" layer="21"/>
<rectangle x1="4.27481875" y1="21.209" x2="35.433" y2="21.29358125" layer="21"/>
<rectangle x1="4.445" y1="21.29358125" x2="35.26281875" y2="21.37841875" layer="21"/>
<rectangle x1="4.61518125" y1="21.37841875" x2="35.179" y2="21.463" layer="21"/>
<rectangle x1="4.78281875" y1="21.463" x2="34.925" y2="21.54758125" layer="21"/>
<rectangle x1="4.953" y1="21.54758125" x2="34.75481875" y2="21.63241875" layer="21"/>
<rectangle x1="5.207" y1="21.63241875" x2="34.58718125" y2="21.717" layer="21"/>
<rectangle x1="5.461" y1="21.717" x2="34.33318125" y2="21.80158125" layer="21"/>
<rectangle x1="5.715" y1="21.80158125" x2="34.07918125" y2="21.88641875" layer="21"/>
<rectangle x1="5.969" y1="21.88641875" x2="33.82518125" y2="21.971" layer="21"/>
<rectangle x1="6.30681875" y1="21.971" x2="33.48481875" y2="22.05558125" layer="21"/>
<rectangle x1="6.731" y1="22.05558125" x2="32.97681875" y2="22.14041875" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="UDO-LOGO-10MM" urn="urn:adsk.eagle:package:6649258/2" type="box">
<packageinstances>
<packageinstance name="UDO-LOGO-10MM"/>
</packageinstances>
</package3d>
<package3d name="UDO-LOGO-12MM" urn="urn:adsk.eagle:package:6649257/3" type="box">
<packageinstances>
<packageinstance name="UDO-LOGO-12MM"/>
</packageinstances>
</package3d>
<package3d name="UDO-LOGO-15MM" urn="urn:adsk.eagle:package:6649256/2" type="box">
<packageinstances>
<packageinstance name="UDO-LOGO-15MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="UDO-LOGO-20MM">
<rectangle x1="3.00481875" y1="0.29641875" x2="3.429" y2="0.381" layer="94"/>
<rectangle x1="14.351" y1="0.29641875" x2="14.68881875" y2="0.381" layer="94"/>
<rectangle x1="2.75081875" y1="0.381" x2="3.683" y2="0.46558125" layer="94"/>
<rectangle x1="4.445" y1="0.381" x2="4.52881875" y2="0.46558125" layer="94"/>
<rectangle x1="5.54481875" y1="0.381" x2="5.715" y2="0.46558125" layer="94"/>
<rectangle x1="5.969" y1="0.381" x2="6.05281875" y2="0.46558125" layer="94"/>
<rectangle x1="6.985" y1="0.381" x2="7.15518125" y2="0.46558125" layer="94"/>
<rectangle x1="7.57681875" y1="0.381" x2="8.42518125" y2="0.46558125" layer="94"/>
<rectangle x1="9.94918125" y1="0.381" x2="10.20318125" y2="0.46558125" layer="94"/>
<rectangle x1="11.21918125" y1="0.381" x2="11.38681875" y2="0.46558125" layer="94"/>
<rectangle x1="12.40281875" y1="0.381" x2="12.74318125" y2="0.46558125" layer="94"/>
<rectangle x1="14.097" y1="0.381" x2="14.94281875" y2="0.46558125" layer="94"/>
<rectangle x1="15.70481875" y1="0.381" x2="15.79118125" y2="0.46558125" layer="94"/>
<rectangle x1="16.80718125" y1="0.381" x2="16.891" y2="0.46558125" layer="94"/>
<rectangle x1="17.653" y1="0.381" x2="17.907" y2="0.46558125" layer="94"/>
<rectangle x1="2.667" y1="0.46558125" x2="3.85318125" y2="0.55041875" layer="94"/>
<rectangle x1="4.36118125" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="94"/>
<rectangle x1="5.54481875" y1="0.46558125" x2="5.715" y2="0.55041875" layer="94"/>
<rectangle x1="5.88518125" y1="0.46558125" x2="6.13918125" y2="0.55041875" layer="94"/>
<rectangle x1="6.731" y1="0.46558125" x2="7.239" y2="0.55041875" layer="94"/>
<rectangle x1="7.493" y1="0.46558125" x2="8.67918125" y2="0.55041875" layer="94"/>
<rectangle x1="9.69518125" y1="0.46558125" x2="10.37081875" y2="0.55041875" layer="94"/>
<rectangle x1="11.049" y1="0.46558125" x2="11.64081875" y2="0.55041875" layer="94"/>
<rectangle x1="12.23518125" y1="0.46558125" x2="12.91081875" y2="0.55041875" layer="94"/>
<rectangle x1="13.92681875" y1="0.46558125" x2="15.113" y2="0.55041875" layer="94"/>
<rectangle x1="15.621" y1="0.46558125" x2="15.875" y2="0.55041875" layer="94"/>
<rectangle x1="16.72081875" y1="0.46558125" x2="16.97481875" y2="0.55041875" layer="94"/>
<rectangle x1="17.399" y1="0.46558125" x2="18.07718125" y2="0.55041875" layer="94"/>
<rectangle x1="2.58318125" y1="0.55041875" x2="3.00481875" y2="0.635" layer="94"/>
<rectangle x1="3.51281875" y1="0.55041875" x2="3.937" y2="0.635" layer="94"/>
<rectangle x1="4.36118125" y1="0.55041875" x2="4.61518125" y2="0.635" layer="94"/>
<rectangle x1="5.54481875" y1="0.55041875" x2="5.715" y2="0.635" layer="94"/>
<rectangle x1="5.88518125" y1="0.55041875" x2="6.13918125" y2="0.635" layer="94"/>
<rectangle x1="6.64718125" y1="0.55041875" x2="7.239" y2="0.635" layer="94"/>
<rectangle x1="7.493" y1="0.55041875" x2="8.84681875" y2="0.635" layer="94"/>
<rectangle x1="9.60881875" y1="0.55041875" x2="10.45718125" y2="0.635" layer="94"/>
<rectangle x1="10.96518125" y1="0.55041875" x2="11.72718125" y2="0.635" layer="94"/>
<rectangle x1="12.14881875" y1="0.55041875" x2="12.99718125" y2="0.635" layer="94"/>
<rectangle x1="13.843" y1="0.55041875" x2="14.26718125" y2="0.635" layer="94"/>
<rectangle x1="14.77518125" y1="0.55041875" x2="15.19681875" y2="0.635" layer="94"/>
<rectangle x1="15.621" y1="0.55041875" x2="15.875" y2="0.635" layer="94"/>
<rectangle x1="16.72081875" y1="0.55041875" x2="16.97481875" y2="0.635" layer="94"/>
<rectangle x1="17.31518125" y1="0.55041875" x2="18.24481875" y2="0.635" layer="94"/>
<rectangle x1="2.49681875" y1="0.635" x2="2.83718125" y2="0.71958125" layer="94"/>
<rectangle x1="3.683" y1="0.635" x2="4.02081875" y2="0.71958125" layer="94"/>
<rectangle x1="4.36118125" y1="0.635" x2="4.61518125" y2="0.71958125" layer="94"/>
<rectangle x1="5.54481875" y1="0.635" x2="5.715" y2="0.71958125" layer="94"/>
<rectangle x1="5.88518125" y1="0.635" x2="6.13918125" y2="0.71958125" layer="94"/>
<rectangle x1="6.56081875" y1="0.635" x2="6.90118125" y2="0.71958125" layer="94"/>
<rectangle x1="7.493" y1="0.635" x2="7.747" y2="0.71958125" layer="94"/>
<rectangle x1="8.509" y1="0.635" x2="8.93318125" y2="0.71958125" layer="94"/>
<rectangle x1="9.525" y1="0.635" x2="9.86281875" y2="0.71958125" layer="94"/>
<rectangle x1="10.20318125" y1="0.635" x2="10.541" y2="0.71958125" layer="94"/>
<rectangle x1="10.87881875" y1="0.635" x2="11.13281875" y2="0.71958125" layer="94"/>
<rectangle x1="11.47318125" y1="0.635" x2="11.811" y2="0.71958125" layer="94"/>
<rectangle x1="12.065" y1="0.635" x2="12.40281875" y2="0.71958125" layer="94"/>
<rectangle x1="12.74318125" y1="0.635" x2="13.081" y2="0.71958125" layer="94"/>
<rectangle x1="13.75918125" y1="0.635" x2="14.097" y2="0.71958125" layer="94"/>
<rectangle x1="14.94281875" y1="0.635" x2="15.28318125" y2="0.71958125" layer="94"/>
<rectangle x1="15.621" y1="0.635" x2="15.875" y2="0.71958125" layer="94"/>
<rectangle x1="16.72081875" y1="0.635" x2="16.97481875" y2="0.71958125" layer="94"/>
<rectangle x1="17.22881875" y1="0.635" x2="17.56918125" y2="0.71958125" layer="94"/>
<rectangle x1="17.907" y1="0.635" x2="18.33118125" y2="0.71958125" layer="94"/>
<rectangle x1="2.413" y1="0.71958125" x2="2.667" y2="0.80441875" layer="94"/>
<rectangle x1="3.76681875" y1="0.71958125" x2="4.02081875" y2="0.80441875" layer="94"/>
<rectangle x1="4.36118125" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="94"/>
<rectangle x1="5.54481875" y1="0.71958125" x2="5.715" y2="0.80441875" layer="94"/>
<rectangle x1="5.88518125" y1="0.71958125" x2="6.13918125" y2="0.80441875" layer="94"/>
<rectangle x1="6.56081875" y1="0.71958125" x2="6.81481875" y2="0.80441875" layer="94"/>
<rectangle x1="7.493" y1="0.71958125" x2="7.747" y2="0.80441875" layer="94"/>
<rectangle x1="8.67918125" y1="0.71958125" x2="9.017" y2="0.80441875" layer="94"/>
<rectangle x1="9.44118125" y1="0.71958125" x2="9.69518125" y2="0.80441875" layer="94"/>
<rectangle x1="10.37081875" y1="0.71958125" x2="10.62481875" y2="0.80441875" layer="94"/>
<rectangle x1="10.87881875" y1="0.71958125" x2="11.049" y2="0.80441875" layer="94"/>
<rectangle x1="11.557" y1="0.71958125" x2="11.811" y2="0.80441875" layer="94"/>
<rectangle x1="11.98118125" y1="0.71958125" x2="12.23518125" y2="0.80441875" layer="94"/>
<rectangle x1="12.91081875" y1="0.71958125" x2="13.16481875" y2="0.80441875" layer="94"/>
<rectangle x1="13.67281875" y1="0.71958125" x2="14.01318125" y2="0.80441875" layer="94"/>
<rectangle x1="15.02918125" y1="0.71958125" x2="15.367" y2="0.80441875" layer="94"/>
<rectangle x1="15.621" y1="0.71958125" x2="15.875" y2="0.80441875" layer="94"/>
<rectangle x1="16.72081875" y1="0.71958125" x2="16.97481875" y2="0.80441875" layer="94"/>
<rectangle x1="17.145" y1="0.71958125" x2="17.399" y2="0.80441875" layer="94"/>
<rectangle x1="18.07718125" y1="0.71958125" x2="18.33118125" y2="0.80441875" layer="94"/>
<rectangle x1="2.32918125" y1="0.80441875" x2="2.58318125" y2="0.889" layer="94"/>
<rectangle x1="3.85318125" y1="0.80441875" x2="4.10718125" y2="0.889" layer="94"/>
<rectangle x1="4.36118125" y1="0.80441875" x2="4.61518125" y2="0.889" layer="94"/>
<rectangle x1="5.54481875" y1="0.80441875" x2="5.715" y2="0.889" layer="94"/>
<rectangle x1="5.88518125" y1="0.80441875" x2="6.13918125" y2="0.889" layer="94"/>
<rectangle x1="6.477" y1="0.80441875" x2="6.731" y2="0.889" layer="94"/>
<rectangle x1="7.493" y1="0.80441875" x2="7.747" y2="0.889" layer="94"/>
<rectangle x1="8.763" y1="0.80441875" x2="9.10081875" y2="0.889" layer="94"/>
<rectangle x1="9.44118125" y1="0.80441875" x2="9.69518125" y2="0.889" layer="94"/>
<rectangle x1="10.45718125" y1="0.80441875" x2="10.71118125" y2="0.889" layer="94"/>
<rectangle x1="11.557" y1="0.80441875" x2="11.811" y2="0.889" layer="94"/>
<rectangle x1="11.98118125" y1="0.80441875" x2="12.14881875" y2="0.889" layer="94"/>
<rectangle x1="12.99718125" y1="0.80441875" x2="13.16481875" y2="0.889" layer="94"/>
<rectangle x1="13.67281875" y1="0.80441875" x2="13.92681875" y2="0.889" layer="94"/>
<rectangle x1="15.113" y1="0.80441875" x2="15.367" y2="0.889" layer="94"/>
<rectangle x1="15.621" y1="0.80441875" x2="15.875" y2="0.889" layer="94"/>
<rectangle x1="16.72081875" y1="0.80441875" x2="16.97481875" y2="0.889" layer="94"/>
<rectangle x1="17.145" y1="0.80441875" x2="17.399" y2="0.889" layer="94"/>
<rectangle x1="18.161" y1="0.80441875" x2="18.33118125" y2="0.889" layer="94"/>
<rectangle x1="2.32918125" y1="0.889" x2="2.58318125" y2="0.97358125" layer="94"/>
<rectangle x1="3.937" y1="0.889" x2="4.191" y2="0.97358125" layer="94"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.61518125" y2="0.97358125" layer="94"/>
<rectangle x1="5.54481875" y1="0.889" x2="5.715" y2="0.97358125" layer="94"/>
<rectangle x1="5.88518125" y1="0.889" x2="6.13918125" y2="0.97358125" layer="94"/>
<rectangle x1="6.477" y1="0.889" x2="6.731" y2="0.97358125" layer="94"/>
<rectangle x1="7.493" y1="0.889" x2="7.747" y2="0.97358125" layer="94"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.10081875" y2="0.97358125" layer="94"/>
<rectangle x1="9.35481875" y1="0.889" x2="9.60881875" y2="0.97358125" layer="94"/>
<rectangle x1="10.45718125" y1="0.889" x2="10.71118125" y2="0.97358125" layer="94"/>
<rectangle x1="11.47318125" y1="0.889" x2="11.72718125" y2="0.97358125" layer="94"/>
<rectangle x1="11.89481875" y1="0.889" x2="12.14881875" y2="0.97358125" layer="94"/>
<rectangle x1="13.589" y1="0.889" x2="13.843" y2="0.97358125" layer="94"/>
<rectangle x1="15.19681875" y1="0.889" x2="15.45081875" y2="0.97358125" layer="94"/>
<rectangle x1="15.621" y1="0.889" x2="15.875" y2="0.97358125" layer="94"/>
<rectangle x1="16.72081875" y1="0.889" x2="16.97481875" y2="0.97358125" layer="94"/>
<rectangle x1="17.06118125" y1="0.889" x2="17.31518125" y2="0.97358125" layer="94"/>
<rectangle x1="2.32918125" y1="0.97358125" x2="2.49681875" y2="1.05841875" layer="94"/>
<rectangle x1="3.937" y1="0.97358125" x2="4.191" y2="1.05841875" layer="94"/>
<rectangle x1="4.36118125" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="94"/>
<rectangle x1="5.54481875" y1="0.97358125" x2="5.715" y2="1.05841875" layer="94"/>
<rectangle x1="5.88518125" y1="0.97358125" x2="6.13918125" y2="1.05841875" layer="94"/>
<rectangle x1="6.477" y1="0.97358125" x2="6.64718125" y2="1.05841875" layer="94"/>
<rectangle x1="7.493" y1="0.97358125" x2="7.747" y2="1.05841875" layer="94"/>
<rectangle x1="8.93318125" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="94"/>
<rectangle x1="9.35481875" y1="0.97358125" x2="9.60881875" y2="1.05841875" layer="94"/>
<rectangle x1="10.45718125" y1="0.97358125" x2="10.71118125" y2="1.05841875" layer="94"/>
<rectangle x1="11.21918125" y1="0.97358125" x2="11.72718125" y2="1.05841875" layer="94"/>
<rectangle x1="11.89481875" y1="0.97358125" x2="13.25118125" y2="1.05841875" layer="94"/>
<rectangle x1="13.589" y1="0.97358125" x2="13.843" y2="1.05841875" layer="94"/>
<rectangle x1="15.19681875" y1="0.97358125" x2="15.45081875" y2="1.05841875" layer="94"/>
<rectangle x1="15.621" y1="0.97358125" x2="15.875" y2="1.05841875" layer="94"/>
<rectangle x1="16.72081875" y1="0.97358125" x2="16.97481875" y2="1.05841875" layer="94"/>
<rectangle x1="17.06118125" y1="0.97358125" x2="18.415" y2="1.05841875" layer="94"/>
<rectangle x1="2.24281875" y1="1.05841875" x2="2.49681875" y2="1.143" layer="94"/>
<rectangle x1="3.937" y1="1.05841875" x2="4.191" y2="1.143" layer="94"/>
<rectangle x1="4.36118125" y1="1.05841875" x2="4.61518125" y2="1.143" layer="94"/>
<rectangle x1="5.54481875" y1="1.05841875" x2="5.715" y2="1.143" layer="94"/>
<rectangle x1="5.88518125" y1="1.05841875" x2="6.13918125" y2="1.143" layer="94"/>
<rectangle x1="6.477" y1="1.05841875" x2="6.64718125" y2="1.143" layer="94"/>
<rectangle x1="7.493" y1="1.05841875" x2="7.747" y2="1.143" layer="94"/>
<rectangle x1="8.93318125" y1="1.05841875" x2="9.18718125" y2="1.143" layer="94"/>
<rectangle x1="9.35481875" y1="1.05841875" x2="9.60881875" y2="1.143" layer="94"/>
<rectangle x1="10.45718125" y1="1.05841875" x2="10.71118125" y2="1.143" layer="94"/>
<rectangle x1="11.049" y1="1.05841875" x2="11.557" y2="1.143" layer="94"/>
<rectangle x1="11.89481875" y1="1.05841875" x2="13.25118125" y2="1.143" layer="94"/>
<rectangle x1="13.589" y1="1.05841875" x2="13.75918125" y2="1.143" layer="94"/>
<rectangle x1="15.28318125" y1="1.05841875" x2="15.53718125" y2="1.143" layer="94"/>
<rectangle x1="15.621" y1="1.05841875" x2="15.875" y2="1.143" layer="94"/>
<rectangle x1="16.72081875" y1="1.05841875" x2="16.97481875" y2="1.143" layer="94"/>
<rectangle x1="17.06118125" y1="1.05841875" x2="18.415" y2="1.143" layer="94"/>
<rectangle x1="2.24281875" y1="1.143" x2="2.49681875" y2="1.22758125" layer="94"/>
<rectangle x1="4.02081875" y1="1.143" x2="4.191" y2="1.22758125" layer="94"/>
<rectangle x1="4.36118125" y1="1.143" x2="4.61518125" y2="1.22758125" layer="94"/>
<rectangle x1="5.54481875" y1="1.143" x2="5.715" y2="1.22758125" layer="94"/>
<rectangle x1="5.88518125" y1="1.143" x2="6.13918125" y2="1.22758125" layer="94"/>
<rectangle x1="6.477" y1="1.143" x2="6.64718125" y2="1.22758125" layer="94"/>
<rectangle x1="7.493" y1="1.143" x2="7.747" y2="1.22758125" layer="94"/>
<rectangle x1="9.017" y1="1.143" x2="9.271" y2="1.22758125" layer="94"/>
<rectangle x1="9.35481875" y1="1.143" x2="9.60881875" y2="1.22758125" layer="94"/>
<rectangle x1="10.45718125" y1="1.143" x2="10.71118125" y2="1.22758125" layer="94"/>
<rectangle x1="10.87881875" y1="1.143" x2="11.38681875" y2="1.22758125" layer="94"/>
<rectangle x1="11.89481875" y1="1.143" x2="13.25118125" y2="1.22758125" layer="94"/>
<rectangle x1="13.50518125" y1="1.143" x2="13.75918125" y2="1.22758125" layer="94"/>
<rectangle x1="15.28318125" y1="1.143" x2="15.53718125" y2="1.22758125" layer="94"/>
<rectangle x1="15.621" y1="1.143" x2="15.875" y2="1.22758125" layer="94"/>
<rectangle x1="16.72081875" y1="1.143" x2="16.97481875" y2="1.22758125" layer="94"/>
<rectangle x1="17.06118125" y1="1.143" x2="18.415" y2="1.22758125" layer="94"/>
<rectangle x1="2.24281875" y1="1.22758125" x2="2.49681875" y2="1.31241875" layer="94"/>
<rectangle x1="4.02081875" y1="1.22758125" x2="4.191" y2="1.31241875" layer="94"/>
<rectangle x1="4.36118125" y1="1.22758125" x2="4.61518125" y2="1.31241875" layer="94"/>
<rectangle x1="5.461" y1="1.22758125" x2="5.715" y2="1.31241875" layer="94"/>
<rectangle x1="5.88518125" y1="1.22758125" x2="6.13918125" y2="1.31241875" layer="94"/>
<rectangle x1="6.477" y1="1.22758125" x2="6.64718125" y2="1.31241875" layer="94"/>
<rectangle x1="7.493" y1="1.22758125" x2="7.747" y2="1.31241875" layer="94"/>
<rectangle x1="9.017" y1="1.22758125" x2="9.271" y2="1.31241875" layer="94"/>
<rectangle x1="9.35481875" y1="1.22758125" x2="9.60881875" y2="1.31241875" layer="94"/>
<rectangle x1="10.45718125" y1="1.22758125" x2="10.71118125" y2="1.31241875" layer="94"/>
<rectangle x1="10.795" y1="1.22758125" x2="11.21918125" y2="1.31241875" layer="94"/>
<rectangle x1="11.89481875" y1="1.22758125" x2="12.14881875" y2="1.31241875" layer="94"/>
<rectangle x1="12.99718125" y1="1.22758125" x2="13.25118125" y2="1.31241875" layer="94"/>
<rectangle x1="13.50518125" y1="1.22758125" x2="13.75918125" y2="1.31241875" layer="94"/>
<rectangle x1="15.28318125" y1="1.22758125" x2="15.53718125" y2="1.31241875" layer="94"/>
<rectangle x1="15.621" y1="1.22758125" x2="15.875" y2="1.31241875" layer="94"/>
<rectangle x1="16.72081875" y1="1.22758125" x2="16.97481875" y2="1.31241875" layer="94"/>
<rectangle x1="17.06118125" y1="1.22758125" x2="17.31518125" y2="1.31241875" layer="94"/>
<rectangle x1="18.161" y1="1.22758125" x2="18.415" y2="1.31241875" layer="94"/>
<rectangle x1="2.24281875" y1="1.31241875" x2="2.49681875" y2="1.397" layer="94"/>
<rectangle x1="4.02081875" y1="1.31241875" x2="4.191" y2="1.397" layer="94"/>
<rectangle x1="4.36118125" y1="1.31241875" x2="4.699" y2="1.397" layer="94"/>
<rectangle x1="5.461" y1="1.31241875" x2="5.715" y2="1.397" layer="94"/>
<rectangle x1="5.88518125" y1="1.31241875" x2="6.13918125" y2="1.397" layer="94"/>
<rectangle x1="6.477" y1="1.31241875" x2="6.64718125" y2="1.397" layer="94"/>
<rectangle x1="7.493" y1="1.31241875" x2="7.747" y2="1.397" layer="94"/>
<rectangle x1="9.017" y1="1.31241875" x2="9.271" y2="1.397" layer="94"/>
<rectangle x1="9.44118125" y1="1.31241875" x2="9.69518125" y2="1.397" layer="94"/>
<rectangle x1="10.45718125" y1="1.31241875" x2="10.62481875" y2="1.397" layer="94"/>
<rectangle x1="10.795" y1="1.31241875" x2="11.049" y2="1.397" layer="94"/>
<rectangle x1="11.64081875" y1="1.31241875" x2="11.72718125" y2="1.397" layer="94"/>
<rectangle x1="11.98118125" y1="1.31241875" x2="12.23518125" y2="1.397" layer="94"/>
<rectangle x1="12.99718125" y1="1.31241875" x2="13.16481875" y2="1.397" layer="94"/>
<rectangle x1="13.50518125" y1="1.31241875" x2="13.75918125" y2="1.397" layer="94"/>
<rectangle x1="15.28318125" y1="1.31241875" x2="15.53718125" y2="1.397" layer="94"/>
<rectangle x1="15.621" y1="1.31241875" x2="15.875" y2="1.397" layer="94"/>
<rectangle x1="16.637" y1="1.31241875" x2="16.891" y2="1.397" layer="94"/>
<rectangle x1="17.145" y1="1.31241875" x2="17.399" y2="1.397" layer="94"/>
<rectangle x1="18.161" y1="1.31241875" x2="18.415" y2="1.397" layer="94"/>
<rectangle x1="2.24281875" y1="1.397" x2="2.49681875" y2="1.48158125" layer="94"/>
<rectangle x1="4.02081875" y1="1.397" x2="4.191" y2="1.48158125" layer="94"/>
<rectangle x1="4.36118125" y1="1.397" x2="4.78281875" y2="1.48158125" layer="94"/>
<rectangle x1="5.37718125" y1="1.397" x2="5.63118125" y2="1.48158125" layer="94"/>
<rectangle x1="5.88518125" y1="1.397" x2="6.13918125" y2="1.48158125" layer="94"/>
<rectangle x1="6.477" y1="1.397" x2="6.64718125" y2="1.48158125" layer="94"/>
<rectangle x1="7.493" y1="1.397" x2="7.747" y2="1.48158125" layer="94"/>
<rectangle x1="9.017" y1="1.397" x2="9.271" y2="1.48158125" layer="94"/>
<rectangle x1="9.44118125" y1="1.397" x2="9.69518125" y2="1.48158125" layer="94"/>
<rectangle x1="10.37081875" y1="1.397" x2="10.62481875" y2="1.48158125" layer="94"/>
<rectangle x1="10.795" y1="1.397" x2="11.049" y2="1.48158125" layer="94"/>
<rectangle x1="11.557" y1="1.397" x2="11.811" y2="1.48158125" layer="94"/>
<rectangle x1="11.98118125" y1="1.397" x2="12.319" y2="1.48158125" layer="94"/>
<rectangle x1="12.91081875" y1="1.397" x2="13.16481875" y2="1.48158125" layer="94"/>
<rectangle x1="13.50518125" y1="1.397" x2="13.75918125" y2="1.48158125" layer="94"/>
<rectangle x1="15.28318125" y1="1.397" x2="15.53718125" y2="1.48158125" layer="94"/>
<rectangle x1="15.621" y1="1.397" x2="15.95881875" y2="1.48158125" layer="94"/>
<rectangle x1="16.637" y1="1.397" x2="16.891" y2="1.48158125" layer="94"/>
<rectangle x1="17.145" y1="1.397" x2="17.48281875" y2="1.48158125" layer="94"/>
<rectangle x1="18.07718125" y1="1.397" x2="18.33118125" y2="1.48158125" layer="94"/>
<rectangle x1="2.24281875" y1="1.48158125" x2="2.49681875" y2="1.56641875" layer="94"/>
<rectangle x1="4.02081875" y1="1.48158125" x2="4.191" y2="1.56641875" layer="94"/>
<rectangle x1="4.36118125" y1="1.48158125" x2="4.953" y2="1.56641875" layer="94"/>
<rectangle x1="5.207" y1="1.48158125" x2="5.63118125" y2="1.56641875" layer="94"/>
<rectangle x1="5.88518125" y1="1.48158125" x2="6.13918125" y2="1.56641875" layer="94"/>
<rectangle x1="6.477" y1="1.48158125" x2="6.64718125" y2="1.56641875" layer="94"/>
<rectangle x1="7.493" y1="1.48158125" x2="7.747" y2="1.56641875" layer="94"/>
<rectangle x1="9.017" y1="1.48158125" x2="9.271" y2="1.56641875" layer="94"/>
<rectangle x1="9.525" y1="1.48158125" x2="9.86281875" y2="1.56641875" layer="94"/>
<rectangle x1="10.20318125" y1="1.48158125" x2="10.541" y2="1.56641875" layer="94"/>
<rectangle x1="10.87881875" y1="1.48158125" x2="11.21918125" y2="1.56641875" layer="94"/>
<rectangle x1="11.47318125" y1="1.48158125" x2="11.72718125" y2="1.56641875" layer="94"/>
<rectangle x1="12.065" y1="1.48158125" x2="12.40281875" y2="1.56641875" layer="94"/>
<rectangle x1="12.74318125" y1="1.48158125" x2="13.081" y2="1.56641875" layer="94"/>
<rectangle x1="13.589" y1="1.48158125" x2="13.75918125" y2="1.56641875" layer="94"/>
<rectangle x1="15.28318125" y1="1.48158125" x2="15.53718125" y2="1.56641875" layer="94"/>
<rectangle x1="15.621" y1="1.48158125" x2="16.129" y2="1.56641875" layer="94"/>
<rectangle x1="16.46681875" y1="1.48158125" x2="16.80718125" y2="1.56641875" layer="94"/>
<rectangle x1="17.22881875" y1="1.48158125" x2="17.653" y2="1.56641875" layer="94"/>
<rectangle x1="17.907" y1="1.48158125" x2="18.24481875" y2="1.56641875" layer="94"/>
<rectangle x1="2.24281875" y1="1.56641875" x2="2.49681875" y2="1.651" layer="94"/>
<rectangle x1="4.02081875" y1="1.56641875" x2="4.191" y2="1.651" layer="94"/>
<rectangle x1="4.36118125" y1="1.56641875" x2="5.54481875" y2="1.651" layer="94"/>
<rectangle x1="5.88518125" y1="1.56641875" x2="6.13918125" y2="1.651" layer="94"/>
<rectangle x1="6.223" y1="1.56641875" x2="7.239" y2="1.651" layer="94"/>
<rectangle x1="7.493" y1="1.56641875" x2="7.747" y2="1.651" layer="94"/>
<rectangle x1="9.017" y1="1.56641875" x2="9.271" y2="1.651" layer="94"/>
<rectangle x1="9.60881875" y1="1.56641875" x2="10.45718125" y2="1.651" layer="94"/>
<rectangle x1="10.87881875" y1="1.56641875" x2="11.72718125" y2="1.651" layer="94"/>
<rectangle x1="12.14881875" y1="1.56641875" x2="12.99718125" y2="1.651" layer="94"/>
<rectangle x1="13.589" y1="1.56641875" x2="13.843" y2="1.651" layer="94"/>
<rectangle x1="15.19681875" y1="1.56641875" x2="15.45081875" y2="1.651" layer="94"/>
<rectangle x1="15.621" y1="1.56641875" x2="16.72081875" y2="1.651" layer="94"/>
<rectangle x1="17.31518125" y1="1.56641875" x2="18.161" y2="1.651" layer="94"/>
<rectangle x1="2.24281875" y1="1.651" x2="2.49681875" y2="1.73558125" layer="94"/>
<rectangle x1="4.02081875" y1="1.651" x2="4.191" y2="1.73558125" layer="94"/>
<rectangle x1="4.36118125" y1="1.651" x2="4.61518125" y2="1.73558125" layer="94"/>
<rectangle x1="4.78281875" y1="1.651" x2="5.37718125" y2="1.73558125" layer="94"/>
<rectangle x1="5.88518125" y1="1.651" x2="6.13918125" y2="1.73558125" layer="94"/>
<rectangle x1="6.223" y1="1.651" x2="7.239" y2="1.73558125" layer="94"/>
<rectangle x1="7.493" y1="1.651" x2="7.747" y2="1.73558125" layer="94"/>
<rectangle x1="9.017" y1="1.651" x2="9.18718125" y2="1.73558125" layer="94"/>
<rectangle x1="9.69518125" y1="1.651" x2="10.37081875" y2="1.73558125" layer="94"/>
<rectangle x1="11.049" y1="1.651" x2="11.557" y2="1.73558125" layer="94"/>
<rectangle x1="12.23518125" y1="1.651" x2="12.91081875" y2="1.73558125" layer="94"/>
<rectangle x1="13.589" y1="1.651" x2="13.843" y2="1.73558125" layer="94"/>
<rectangle x1="15.19681875" y1="1.651" x2="15.45081875" y2="1.73558125" layer="94"/>
<rectangle x1="15.621" y1="1.651" x2="15.79118125" y2="1.73558125" layer="94"/>
<rectangle x1="15.95881875" y1="1.651" x2="16.637" y2="1.73558125" layer="94"/>
<rectangle x1="17.399" y1="1.651" x2="18.07718125" y2="1.73558125" layer="94"/>
<rectangle x1="2.24281875" y1="1.73558125" x2="2.49681875" y2="1.82041875" layer="94"/>
<rectangle x1="4.02081875" y1="1.73558125" x2="4.191" y2="1.82041875" layer="94"/>
<rectangle x1="4.445" y1="1.73558125" x2="4.52881875" y2="1.82041875" layer="94"/>
<rectangle x1="4.953" y1="1.73558125" x2="5.12318125" y2="1.82041875" layer="94"/>
<rectangle x1="5.969" y1="1.73558125" x2="6.05281875" y2="1.82041875" layer="94"/>
<rectangle x1="6.30681875" y1="1.73558125" x2="7.15518125" y2="1.82041875" layer="94"/>
<rectangle x1="7.493" y1="1.73558125" x2="7.747" y2="1.82041875" layer="94"/>
<rectangle x1="8.93318125" y1="1.73558125" x2="9.18718125" y2="1.82041875" layer="94"/>
<rectangle x1="9.94918125" y1="1.73558125" x2="10.11681875" y2="1.82041875" layer="94"/>
<rectangle x1="11.21918125" y1="1.73558125" x2="11.38681875" y2="1.82041875" layer="94"/>
<rectangle x1="12.48918125" y1="1.73558125" x2="12.65681875" y2="1.82041875" layer="94"/>
<rectangle x1="13.67281875" y1="1.73558125" x2="13.92681875" y2="1.82041875" layer="94"/>
<rectangle x1="15.113" y1="1.73558125" x2="15.367" y2="1.82041875" layer="94"/>
<rectangle x1="16.21281875" y1="1.73558125" x2="16.383" y2="1.82041875" layer="94"/>
<rectangle x1="17.653" y1="1.73558125" x2="17.82318125" y2="1.82041875" layer="94"/>
<rectangle x1="2.24281875" y1="1.82041875" x2="2.49681875" y2="1.905" layer="94"/>
<rectangle x1="4.02081875" y1="1.82041875" x2="4.191" y2="1.905" layer="94"/>
<rectangle x1="5.88518125" y1="1.82041875" x2="6.13918125" y2="1.905" layer="94"/>
<rectangle x1="6.477" y1="1.82041875" x2="6.64718125" y2="1.905" layer="94"/>
<rectangle x1="7.493" y1="1.82041875" x2="7.747" y2="1.905" layer="94"/>
<rectangle x1="8.84681875" y1="1.82041875" x2="9.18718125" y2="1.905" layer="94"/>
<rectangle x1="13.67281875" y1="1.82041875" x2="14.01318125" y2="1.905" layer="94"/>
<rectangle x1="15.02918125" y1="1.82041875" x2="15.367" y2="1.905" layer="94"/>
<rectangle x1="2.24281875" y1="1.905" x2="2.49681875" y2="1.98958125" layer="94"/>
<rectangle x1="4.02081875" y1="1.905" x2="4.191" y2="1.98958125" layer="94"/>
<rectangle x1="5.88518125" y1="1.905" x2="6.13918125" y2="1.98958125" layer="94"/>
<rectangle x1="6.477" y1="1.905" x2="6.64718125" y2="1.98958125" layer="94"/>
<rectangle x1="7.493" y1="1.905" x2="7.747" y2="1.98958125" layer="94"/>
<rectangle x1="8.84681875" y1="1.905" x2="9.10081875" y2="1.98958125" layer="94"/>
<rectangle x1="13.75918125" y1="1.905" x2="14.097" y2="1.98958125" layer="94"/>
<rectangle x1="14.94281875" y1="1.905" x2="15.28318125" y2="1.98958125" layer="94"/>
<rectangle x1="2.24281875" y1="1.98958125" x2="2.49681875" y2="2.07441875" layer="94"/>
<rectangle x1="4.02081875" y1="1.98958125" x2="4.27481875" y2="2.07441875" layer="94"/>
<rectangle x1="5.88518125" y1="1.98958125" x2="6.05281875" y2="2.07441875" layer="94"/>
<rectangle x1="6.477" y1="1.98958125" x2="6.64718125" y2="2.07441875" layer="94"/>
<rectangle x1="7.493" y1="1.98958125" x2="7.747" y2="2.07441875" layer="94"/>
<rectangle x1="8.67918125" y1="1.98958125" x2="9.017" y2="2.07441875" layer="94"/>
<rectangle x1="13.843" y1="1.98958125" x2="14.26718125" y2="2.07441875" layer="94"/>
<rectangle x1="14.77518125" y1="1.98958125" x2="15.19681875" y2="2.07441875" layer="94"/>
<rectangle x1="2.24281875" y1="2.07441875" x2="2.49681875" y2="2.159" layer="94"/>
<rectangle x1="4.02081875" y1="2.07441875" x2="4.27481875" y2="2.159" layer="94"/>
<rectangle x1="6.477" y1="2.07441875" x2="6.64718125" y2="2.159" layer="94"/>
<rectangle x1="7.493" y1="2.07441875" x2="7.747" y2="2.159" layer="94"/>
<rectangle x1="8.59281875" y1="2.07441875" x2="8.93318125" y2="2.159" layer="94"/>
<rectangle x1="13.92681875" y1="2.07441875" x2="15.113" y2="2.159" layer="94"/>
<rectangle x1="2.24281875" y1="2.159" x2="2.49681875" y2="2.24358125" layer="94"/>
<rectangle x1="4.02081875" y1="2.159" x2="4.191" y2="2.24358125" layer="94"/>
<rectangle x1="6.477" y1="2.159" x2="6.64718125" y2="2.24358125" layer="94"/>
<rectangle x1="7.493" y1="2.159" x2="8.84681875" y2="2.24358125" layer="94"/>
<rectangle x1="14.097" y1="2.159" x2="14.94281875" y2="2.24358125" layer="94"/>
<rectangle x1="2.24281875" y1="2.24358125" x2="2.413" y2="2.32841875" layer="94"/>
<rectangle x1="4.02081875" y1="2.24358125" x2="4.191" y2="2.32841875" layer="94"/>
<rectangle x1="6.477" y1="2.24358125" x2="6.64718125" y2="2.32841875" layer="94"/>
<rectangle x1="7.493" y1="2.24358125" x2="8.763" y2="2.32841875" layer="94"/>
<rectangle x1="14.26718125" y1="2.24358125" x2="14.77518125" y2="2.32841875" layer="94"/>
<rectangle x1="7.493" y1="2.32841875" x2="8.509" y2="2.413" layer="94"/>
<rectangle x1="3.34518125" y1="3.25958125" x2="16.80718125" y2="3.34441875" layer="94"/>
<rectangle x1="3.00481875" y1="3.34441875" x2="17.145" y2="3.429" layer="94"/>
<rectangle x1="2.667" y1="3.429" x2="17.399" y2="3.51358125" layer="94"/>
<rectangle x1="2.49681875" y1="3.51358125" x2="17.56918125" y2="3.59841875" layer="94"/>
<rectangle x1="2.32918125" y1="3.59841875" x2="17.82318125" y2="3.683" layer="94"/>
<rectangle x1="2.159" y1="3.683" x2="17.99081875" y2="3.76758125" layer="94"/>
<rectangle x1="1.98881875" y1="3.76758125" x2="18.07718125" y2="3.85241875" layer="94"/>
<rectangle x1="1.905" y1="3.85241875" x2="3.51281875" y2="3.937" layer="94"/>
<rectangle x1="9.779" y1="3.85241875" x2="18.24481875" y2="3.937" layer="94"/>
<rectangle x1="1.73481875" y1="3.937" x2="3.175" y2="4.02158125" layer="94"/>
<rectangle x1="9.779" y1="3.937" x2="18.33118125" y2="4.02158125" layer="94"/>
<rectangle x1="1.651" y1="4.02158125" x2="2.83718125" y2="4.10641875" layer="94"/>
<rectangle x1="9.69518125" y1="4.02158125" x2="18.49881875" y2="4.10641875" layer="94"/>
<rectangle x1="1.56718125" y1="4.10641875" x2="2.667" y2="4.191" layer="94"/>
<rectangle x1="9.60881875" y1="4.10641875" x2="18.58518125" y2="4.191" layer="94"/>
<rectangle x1="1.397" y1="4.191" x2="2.49681875" y2="4.27558125" layer="94"/>
<rectangle x1="9.60881875" y1="4.191" x2="18.669" y2="4.27558125" layer="94"/>
<rectangle x1="1.31318125" y1="4.27558125" x2="2.32918125" y2="4.36041875" layer="94"/>
<rectangle x1="9.525" y1="4.27558125" x2="18.75281875" y2="4.36041875" layer="94"/>
<rectangle x1="1.22681875" y1="4.36041875" x2="2.159" y2="4.445" layer="94"/>
<rectangle x1="9.525" y1="4.36041875" x2="18.83918125" y2="4.445" layer="94"/>
<rectangle x1="1.143" y1="4.445" x2="2.07518125" y2="4.52958125" layer="94"/>
<rectangle x1="9.44118125" y1="4.445" x2="18.923" y2="4.52958125" layer="94"/>
<rectangle x1="1.05918125" y1="4.52958125" x2="1.98881875" y2="4.61441875" layer="94"/>
<rectangle x1="9.44118125" y1="4.52958125" x2="19.00681875" y2="4.61441875" layer="94"/>
<rectangle x1="0.97281875" y1="4.61441875" x2="1.905" y2="4.699" layer="94"/>
<rectangle x1="9.44118125" y1="4.61441875" x2="19.09318125" y2="4.699" layer="94"/>
<rectangle x1="0.97281875" y1="4.699" x2="1.73481875" y2="4.78358125" layer="94"/>
<rectangle x1="9.35481875" y1="4.699" x2="19.177" y2="4.78358125" layer="94"/>
<rectangle x1="0.889" y1="4.78358125" x2="1.651" y2="4.86841875" layer="94"/>
<rectangle x1="9.35481875" y1="4.78358125" x2="19.26081875" y2="4.86841875" layer="94"/>
<rectangle x1="0.80518125" y1="4.86841875" x2="1.56718125" y2="4.953" layer="94"/>
<rectangle x1="9.35481875" y1="4.86841875" x2="19.26081875" y2="4.953" layer="94"/>
<rectangle x1="0.71881875" y1="4.953" x2="1.48081875" y2="5.03758125" layer="94"/>
<rectangle x1="9.271" y1="4.953" x2="19.34718125" y2="5.03758125" layer="94"/>
<rectangle x1="0.71881875" y1="5.03758125" x2="1.48081875" y2="5.12241875" layer="94"/>
<rectangle x1="9.271" y1="5.03758125" x2="19.431" y2="5.12241875" layer="94"/>
<rectangle x1="0.635" y1="5.12241875" x2="1.397" y2="5.207" layer="94"/>
<rectangle x1="9.271" y1="5.12241875" x2="19.431" y2="5.207" layer="94"/>
<rectangle x1="0.55118125" y1="5.207" x2="1.31318125" y2="5.29158125" layer="94"/>
<rectangle x1="9.271" y1="5.207" x2="19.51481875" y2="5.29158125" layer="94"/>
<rectangle x1="0.55118125" y1="5.29158125" x2="1.22681875" y2="5.37641875" layer="94"/>
<rectangle x1="9.18718125" y1="5.29158125" x2="19.51481875" y2="5.37641875" layer="94"/>
<rectangle x1="0.46481875" y1="5.37641875" x2="1.22681875" y2="5.461" layer="94"/>
<rectangle x1="9.18718125" y1="5.37641875" x2="19.60118125" y2="5.461" layer="94"/>
<rectangle x1="0.46481875" y1="5.461" x2="1.143" y2="5.54558125" layer="94"/>
<rectangle x1="9.18718125" y1="5.461" x2="19.685" y2="5.54558125" layer="94"/>
<rectangle x1="0.381" y1="5.54558125" x2="1.05918125" y2="5.63041875" layer="94"/>
<rectangle x1="3.85318125" y1="5.54558125" x2="4.27481875" y2="5.63041875" layer="94"/>
<rectangle x1="5.88518125" y1="5.54558125" x2="6.81481875" y2="5.63041875" layer="94"/>
<rectangle x1="9.18718125" y1="5.54558125" x2="19.685" y2="5.63041875" layer="94"/>
<rectangle x1="0.381" y1="5.63041875" x2="1.05918125" y2="5.715" layer="94"/>
<rectangle x1="3.51281875" y1="5.63041875" x2="4.61518125" y2="5.715" layer="94"/>
<rectangle x1="5.79881875" y1="5.63041875" x2="7.239" y2="5.715" layer="94"/>
<rectangle x1="9.10081875" y1="5.63041875" x2="10.96518125" y2="5.715" layer="94"/>
<rectangle x1="11.64081875" y1="5.63041875" x2="19.685" y2="5.715" layer="94"/>
<rectangle x1="0.381" y1="5.715" x2="0.97281875" y2="5.79958125" layer="94"/>
<rectangle x1="3.34518125" y1="5.715" x2="4.78281875" y2="5.79958125" layer="94"/>
<rectangle x1="5.715" y1="5.715" x2="7.40918125" y2="5.79958125" layer="94"/>
<rectangle x1="9.10081875" y1="5.715" x2="10.71118125" y2="5.79958125" layer="94"/>
<rectangle x1="11.89481875" y1="5.715" x2="13.335" y2="5.79958125" layer="94"/>
<rectangle x1="13.67281875" y1="5.715" x2="15.113" y2="5.79958125" layer="94"/>
<rectangle x1="15.367" y1="5.715" x2="16.383" y2="5.79958125" layer="94"/>
<rectangle x1="17.22881875" y1="5.715" x2="19.76881875" y2="5.79958125" layer="94"/>
<rectangle x1="0.29718125" y1="5.79958125" x2="0.97281875" y2="5.88441875" layer="94"/>
<rectangle x1="3.175" y1="5.79958125" x2="4.953" y2="5.88441875" layer="94"/>
<rectangle x1="5.715" y1="5.79958125" x2="7.57681875" y2="5.88441875" layer="94"/>
<rectangle x1="9.10081875" y1="5.79958125" x2="10.541" y2="5.88441875" layer="94"/>
<rectangle x1="12.065" y1="5.79958125" x2="13.335" y2="5.88441875" layer="94"/>
<rectangle x1="13.67281875" y1="5.79958125" x2="15.113" y2="5.88441875" layer="94"/>
<rectangle x1="15.45081875" y1="5.79958125" x2="16.21281875" y2="5.88441875" layer="94"/>
<rectangle x1="17.48281875" y1="5.79958125" x2="19.76881875" y2="5.88441875" layer="94"/>
<rectangle x1="0.29718125" y1="5.88441875" x2="0.889" y2="5.969" layer="94"/>
<rectangle x1="3.09118125" y1="5.88441875" x2="5.12318125" y2="5.969" layer="94"/>
<rectangle x1="5.715" y1="5.88441875" x2="7.66318125" y2="5.969" layer="94"/>
<rectangle x1="9.10081875" y1="5.88441875" x2="10.37081875" y2="5.969" layer="94"/>
<rectangle x1="12.23518125" y1="5.88441875" x2="13.335" y2="5.969" layer="94"/>
<rectangle x1="13.67281875" y1="5.88441875" x2="15.113" y2="5.969" layer="94"/>
<rectangle x1="15.45081875" y1="5.88441875" x2="16.129" y2="5.969" layer="94"/>
<rectangle x1="17.56918125" y1="5.88441875" x2="19.85518125" y2="5.969" layer="94"/>
<rectangle x1="0.21081875" y1="5.969" x2="0.889" y2="6.05358125" layer="94"/>
<rectangle x1="2.921" y1="5.969" x2="5.207" y2="6.05358125" layer="94"/>
<rectangle x1="5.715" y1="5.969" x2="7.747" y2="6.05358125" layer="94"/>
<rectangle x1="9.10081875" y1="5.969" x2="10.287" y2="6.05358125" layer="94"/>
<rectangle x1="12.319" y1="5.969" x2="13.335" y2="6.05358125" layer="94"/>
<rectangle x1="13.67281875" y1="5.969" x2="15.113" y2="6.05358125" layer="94"/>
<rectangle x1="15.45081875" y1="5.969" x2="16.04518125" y2="6.05358125" layer="94"/>
<rectangle x1="17.653" y1="5.969" x2="19.85518125" y2="6.05358125" layer="94"/>
<rectangle x1="0.21081875" y1="6.05358125" x2="0.889" y2="6.13841875" layer="94"/>
<rectangle x1="2.83718125" y1="6.05358125" x2="5.29081875" y2="6.13841875" layer="94"/>
<rectangle x1="5.79881875" y1="6.05358125" x2="7.83081875" y2="6.13841875" layer="94"/>
<rectangle x1="9.10081875" y1="6.05358125" x2="10.11681875" y2="6.13841875" layer="94"/>
<rectangle x1="12.40281875" y1="6.05358125" x2="13.335" y2="6.13841875" layer="94"/>
<rectangle x1="13.67281875" y1="6.05358125" x2="15.113" y2="6.13841875" layer="94"/>
<rectangle x1="15.45081875" y1="6.05358125" x2="15.95881875" y2="6.13841875" layer="94"/>
<rectangle x1="16.55318125" y1="6.05358125" x2="17.06118125" y2="6.13841875" layer="94"/>
<rectangle x1="17.73681875" y1="6.05358125" x2="19.85518125" y2="6.13841875" layer="94"/>
<rectangle x1="0.21081875" y1="6.13841875" x2="0.80518125" y2="6.223" layer="94"/>
<rectangle x1="2.75081875" y1="6.13841875" x2="5.37718125" y2="6.223" layer="94"/>
<rectangle x1="5.88518125" y1="6.13841875" x2="7.91718125" y2="6.223" layer="94"/>
<rectangle x1="9.017" y1="6.13841875" x2="10.11681875" y2="6.223" layer="94"/>
<rectangle x1="10.96518125" y1="6.13841875" x2="11.64081875" y2="6.223" layer="94"/>
<rectangle x1="12.48918125" y1="6.13841875" x2="13.335" y2="6.223" layer="94"/>
<rectangle x1="13.67281875" y1="6.13841875" x2="15.02918125" y2="6.223" layer="94"/>
<rectangle x1="15.45081875" y1="6.13841875" x2="15.875" y2="6.223" layer="94"/>
<rectangle x1="16.46681875" y1="6.13841875" x2="17.22881875" y2="6.223" layer="94"/>
<rectangle x1="17.73681875" y1="6.13841875" x2="19.939" y2="6.223" layer="94"/>
<rectangle x1="0.21081875" y1="6.223" x2="0.80518125" y2="6.30758125" layer="94"/>
<rectangle x1="2.75081875" y1="6.223" x2="3.683" y2="6.30758125" layer="94"/>
<rectangle x1="4.445" y1="6.223" x2="5.37718125" y2="6.30758125" layer="94"/>
<rectangle x1="7.06881875" y1="6.223" x2="8.001" y2="6.30758125" layer="94"/>
<rectangle x1="9.017" y1="6.223" x2="10.033" y2="6.30758125" layer="94"/>
<rectangle x1="10.795" y1="6.223" x2="11.811" y2="6.30758125" layer="94"/>
<rectangle x1="12.573" y1="6.223" x2="13.335" y2="6.30758125" layer="94"/>
<rectangle x1="13.67281875" y1="6.223" x2="15.02918125" y2="6.30758125" layer="94"/>
<rectangle x1="15.45081875" y1="6.223" x2="15.875" y2="6.30758125" layer="94"/>
<rectangle x1="16.29918125" y1="6.223" x2="17.399" y2="6.30758125" layer="94"/>
<rectangle x1="17.73681875" y1="6.223" x2="19.939" y2="6.30758125" layer="94"/>
<rectangle x1="0.127" y1="6.30758125" x2="0.80518125" y2="6.39241875" layer="94"/>
<rectangle x1="2.667" y1="6.30758125" x2="3.51281875" y2="6.39241875" layer="94"/>
<rectangle x1="4.61518125" y1="6.30758125" x2="5.461" y2="6.39241875" layer="94"/>
<rectangle x1="7.239" y1="6.30758125" x2="8.08481875" y2="6.39241875" layer="94"/>
<rectangle x1="9.017" y1="6.30758125" x2="9.94918125" y2="6.39241875" layer="94"/>
<rectangle x1="10.62481875" y1="6.30758125" x2="11.98118125" y2="6.39241875" layer="94"/>
<rectangle x1="12.65681875" y1="6.30758125" x2="13.335" y2="6.39241875" layer="94"/>
<rectangle x1="13.67281875" y1="6.30758125" x2="15.113" y2="6.39241875" layer="94"/>
<rectangle x1="15.45081875" y1="6.30758125" x2="15.79118125" y2="6.39241875" layer="94"/>
<rectangle x1="16.21281875" y1="6.30758125" x2="17.48281875" y2="6.39241875" layer="94"/>
<rectangle x1="17.653" y1="6.30758125" x2="19.939" y2="6.39241875" layer="94"/>
<rectangle x1="0.127" y1="6.39241875" x2="0.71881875" y2="6.477" layer="94"/>
<rectangle x1="2.58318125" y1="6.39241875" x2="3.34518125" y2="6.477" layer="94"/>
<rectangle x1="4.78281875" y1="6.39241875" x2="5.54481875" y2="6.477" layer="94"/>
<rectangle x1="7.32281875" y1="6.39241875" x2="8.17118125" y2="6.477" layer="94"/>
<rectangle x1="9.017" y1="6.39241875" x2="9.86281875" y2="6.477" layer="94"/>
<rectangle x1="10.541" y1="6.39241875" x2="12.065" y2="6.477" layer="94"/>
<rectangle x1="12.65681875" y1="6.39241875" x2="13.335" y2="6.477" layer="94"/>
<rectangle x1="13.67281875" y1="6.39241875" x2="15.113" y2="6.477" layer="94"/>
<rectangle x1="15.45081875" y1="6.39241875" x2="15.79118125" y2="6.477" layer="94"/>
<rectangle x1="16.21281875" y1="6.39241875" x2="19.939" y2="6.477" layer="94"/>
<rectangle x1="0.127" y1="6.477" x2="0.71881875" y2="6.56158125" layer="94"/>
<rectangle x1="2.58318125" y1="6.477" x2="3.25881875" y2="6.56158125" layer="94"/>
<rectangle x1="4.86918125" y1="6.477" x2="5.54481875" y2="6.56158125" layer="94"/>
<rectangle x1="7.493" y1="6.477" x2="8.17118125" y2="6.56158125" layer="94"/>
<rectangle x1="9.017" y1="6.477" x2="9.86281875" y2="6.56158125" layer="94"/>
<rectangle x1="10.45718125" y1="6.477" x2="12.14881875" y2="6.56158125" layer="94"/>
<rectangle x1="12.74318125" y1="6.477" x2="13.335" y2="6.56158125" layer="94"/>
<rectangle x1="13.67281875" y1="6.477" x2="15.113" y2="6.56158125" layer="94"/>
<rectangle x1="15.45081875" y1="6.477" x2="15.79118125" y2="6.56158125" layer="94"/>
<rectangle x1="16.21281875" y1="6.477" x2="19.939" y2="6.56158125" layer="94"/>
<rectangle x1="0.127" y1="6.56158125" x2="0.71881875" y2="6.64641875" layer="94"/>
<rectangle x1="2.49681875" y1="6.56158125" x2="3.175" y2="6.64641875" layer="94"/>
<rectangle x1="4.953" y1="6.56158125" x2="5.63118125" y2="6.64641875" layer="94"/>
<rectangle x1="7.493" y1="6.56158125" x2="8.255" y2="6.64641875" layer="94"/>
<rectangle x1="9.017" y1="6.56158125" x2="9.779" y2="6.64641875" layer="94"/>
<rectangle x1="10.37081875" y1="6.56158125" x2="12.23518125" y2="6.64641875" layer="94"/>
<rectangle x1="12.74318125" y1="6.56158125" x2="13.335" y2="6.64641875" layer="94"/>
<rectangle x1="13.67281875" y1="6.56158125" x2="15.02918125" y2="6.64641875" layer="94"/>
<rectangle x1="15.45081875" y1="6.56158125" x2="15.79118125" y2="6.64641875" layer="94"/>
<rectangle x1="17.82318125" y1="6.56158125" x2="19.939" y2="6.64641875" layer="94"/>
<rectangle x1="0.127" y1="6.64641875" x2="0.71881875" y2="6.731" layer="94"/>
<rectangle x1="2.49681875" y1="6.64641875" x2="3.175" y2="6.731" layer="94"/>
<rectangle x1="4.953" y1="6.64641875" x2="5.63118125" y2="6.731" layer="94"/>
<rectangle x1="7.57681875" y1="6.64641875" x2="8.255" y2="6.731" layer="94"/>
<rectangle x1="9.017" y1="6.64641875" x2="9.779" y2="6.731" layer="94"/>
<rectangle x1="10.287" y1="6.64641875" x2="12.319" y2="6.731" layer="94"/>
<rectangle x1="12.827" y1="6.64641875" x2="13.335" y2="6.731" layer="94"/>
<rectangle x1="13.67281875" y1="6.64641875" x2="15.02918125" y2="6.731" layer="94"/>
<rectangle x1="15.45081875" y1="6.64641875" x2="15.70481875" y2="6.731" layer="94"/>
<rectangle x1="17.907" y1="6.64641875" x2="19.939" y2="6.731" layer="94"/>
<rectangle x1="0.04318125" y1="6.731" x2="0.71881875" y2="6.81558125" layer="94"/>
<rectangle x1="2.49681875" y1="6.731" x2="3.09118125" y2="6.81558125" layer="94"/>
<rectangle x1="5.03681875" y1="6.731" x2="5.715" y2="6.81558125" layer="94"/>
<rectangle x1="7.66318125" y1="6.731" x2="8.255" y2="6.81558125" layer="94"/>
<rectangle x1="9.017" y1="6.731" x2="9.779" y2="6.81558125" layer="94"/>
<rectangle x1="10.287" y1="6.731" x2="12.319" y2="6.81558125" layer="94"/>
<rectangle x1="12.827" y1="6.731" x2="13.335" y2="6.81558125" layer="94"/>
<rectangle x1="13.67281875" y1="6.731" x2="15.113" y2="6.81558125" layer="94"/>
<rectangle x1="15.45081875" y1="6.731" x2="15.70481875" y2="6.81558125" layer="94"/>
<rectangle x1="17.907" y1="6.731" x2="19.939" y2="6.81558125" layer="94"/>
<rectangle x1="0.04318125" y1="6.81558125" x2="0.71881875" y2="6.90041875" layer="94"/>
<rectangle x1="2.413" y1="6.81558125" x2="3.09118125" y2="6.90041875" layer="94"/>
<rectangle x1="5.03681875" y1="6.81558125" x2="5.715" y2="6.90041875" layer="94"/>
<rectangle x1="7.66318125" y1="6.81558125" x2="8.33881875" y2="6.90041875" layer="94"/>
<rectangle x1="9.017" y1="6.81558125" x2="9.69518125" y2="6.90041875" layer="94"/>
<rectangle x1="10.20318125" y1="6.81558125" x2="12.319" y2="6.90041875" layer="94"/>
<rectangle x1="12.827" y1="6.81558125" x2="13.335" y2="6.90041875" layer="94"/>
<rectangle x1="13.67281875" y1="6.81558125" x2="15.02918125" y2="6.90041875" layer="94"/>
<rectangle x1="15.45081875" y1="6.81558125" x2="15.70481875" y2="6.90041875" layer="94"/>
<rectangle x1="17.907" y1="6.81558125" x2="19.939" y2="6.90041875" layer="94"/>
<rectangle x1="0.04318125" y1="6.90041875" x2="0.635" y2="6.985" layer="94"/>
<rectangle x1="2.413" y1="6.90041875" x2="3.00481875" y2="6.985" layer="94"/>
<rectangle x1="5.12318125" y1="6.90041875" x2="5.715" y2="6.985" layer="94"/>
<rectangle x1="7.66318125" y1="6.90041875" x2="8.33881875" y2="6.985" layer="94"/>
<rectangle x1="9.017" y1="6.90041875" x2="9.69518125" y2="6.985" layer="94"/>
<rectangle x1="10.20318125" y1="6.90041875" x2="12.40281875" y2="6.985" layer="94"/>
<rectangle x1="12.91081875" y1="6.90041875" x2="13.335" y2="6.985" layer="94"/>
<rectangle x1="13.67281875" y1="6.90041875" x2="15.02918125" y2="6.985" layer="94"/>
<rectangle x1="15.45081875" y1="6.90041875" x2="15.79118125" y2="6.985" layer="94"/>
<rectangle x1="16.21281875" y1="6.90041875" x2="17.399" y2="6.985" layer="94"/>
<rectangle x1="17.907" y1="6.90041875" x2="19.939" y2="6.985" layer="94"/>
<rectangle x1="0.04318125" y1="6.985" x2="0.635" y2="7.06958125" layer="94"/>
<rectangle x1="2.413" y1="6.985" x2="3.00481875" y2="7.06958125" layer="94"/>
<rectangle x1="5.12318125" y1="6.985" x2="5.715" y2="7.06958125" layer="94"/>
<rectangle x1="7.747" y1="6.985" x2="8.33881875" y2="7.06958125" layer="94"/>
<rectangle x1="9.017" y1="6.985" x2="9.69518125" y2="7.06958125" layer="94"/>
<rectangle x1="10.20318125" y1="6.985" x2="12.40281875" y2="7.06958125" layer="94"/>
<rectangle x1="12.91081875" y1="6.985" x2="13.335" y2="7.06958125" layer="94"/>
<rectangle x1="13.75918125" y1="6.985" x2="15.02918125" y2="7.06958125" layer="94"/>
<rectangle x1="15.45081875" y1="6.985" x2="15.79118125" y2="7.06958125" layer="94"/>
<rectangle x1="16.21281875" y1="6.985" x2="17.48281875" y2="7.06958125" layer="94"/>
<rectangle x1="17.82318125" y1="6.985" x2="19.939" y2="7.06958125" layer="94"/>
<rectangle x1="0.04318125" y1="7.06958125" x2="0.635" y2="7.15441875" layer="94"/>
<rectangle x1="2.413" y1="7.06958125" x2="3.00481875" y2="7.15441875" layer="94"/>
<rectangle x1="5.12318125" y1="7.06958125" x2="5.715" y2="7.15441875" layer="94"/>
<rectangle x1="7.747" y1="7.06958125" x2="8.33881875" y2="7.15441875" layer="94"/>
<rectangle x1="9.017" y1="7.06958125" x2="9.69518125" y2="7.15441875" layer="94"/>
<rectangle x1="10.20318125" y1="7.06958125" x2="12.40281875" y2="7.15441875" layer="94"/>
<rectangle x1="12.91081875" y1="7.06958125" x2="13.335" y2="7.15441875" layer="94"/>
<rectangle x1="13.75918125" y1="7.06958125" x2="14.94281875" y2="7.15441875" layer="94"/>
<rectangle x1="15.367" y1="7.06958125" x2="15.79118125" y2="7.15441875" layer="94"/>
<rectangle x1="16.21281875" y1="7.06958125" x2="17.399" y2="7.15441875" layer="94"/>
<rectangle x1="17.82318125" y1="7.06958125" x2="19.939" y2="7.15441875" layer="94"/>
<rectangle x1="0.04318125" y1="7.15441875" x2="0.635" y2="7.239" layer="94"/>
<rectangle x1="2.413" y1="7.15441875" x2="3.00481875" y2="7.239" layer="94"/>
<rectangle x1="5.12318125" y1="7.15441875" x2="5.715" y2="7.239" layer="94"/>
<rectangle x1="7.747" y1="7.15441875" x2="8.33881875" y2="7.239" layer="94"/>
<rectangle x1="9.017" y1="7.15441875" x2="9.69518125" y2="7.239" layer="94"/>
<rectangle x1="10.20318125" y1="7.15441875" x2="12.40281875" y2="7.239" layer="94"/>
<rectangle x1="12.91081875" y1="7.15441875" x2="13.335" y2="7.239" layer="94"/>
<rectangle x1="13.843" y1="7.15441875" x2="14.94281875" y2="7.239" layer="94"/>
<rectangle x1="15.367" y1="7.15441875" x2="15.875" y2="7.239" layer="94"/>
<rectangle x1="16.29918125" y1="7.15441875" x2="17.31518125" y2="7.239" layer="94"/>
<rectangle x1="17.82318125" y1="7.15441875" x2="19.939" y2="7.239" layer="94"/>
<rectangle x1="0.04318125" y1="7.239" x2="0.635" y2="7.32358125" layer="94"/>
<rectangle x1="2.413" y1="7.239" x2="3.00481875" y2="7.32358125" layer="94"/>
<rectangle x1="5.12318125" y1="7.239" x2="5.715" y2="7.32358125" layer="94"/>
<rectangle x1="7.747" y1="7.239" x2="8.33881875" y2="7.32358125" layer="94"/>
<rectangle x1="9.017" y1="7.239" x2="9.69518125" y2="7.32358125" layer="94"/>
<rectangle x1="10.20318125" y1="7.239" x2="12.40281875" y2="7.32358125" layer="94"/>
<rectangle x1="12.91081875" y1="7.239" x2="13.335" y2="7.32358125" layer="94"/>
<rectangle x1="13.92681875" y1="7.239" x2="14.859" y2="7.32358125" layer="94"/>
<rectangle x1="15.28318125" y1="7.239" x2="15.875" y2="7.32358125" layer="94"/>
<rectangle x1="16.383" y1="7.239" x2="17.22881875" y2="7.32358125" layer="94"/>
<rectangle x1="17.73681875" y1="7.239" x2="19.939" y2="7.32358125" layer="94"/>
<rectangle x1="0.04318125" y1="7.32358125" x2="0.635" y2="7.40841875" layer="94"/>
<rectangle x1="2.413" y1="7.32358125" x2="3.00481875" y2="7.40841875" layer="94"/>
<rectangle x1="5.12318125" y1="7.32358125" x2="5.715" y2="7.40841875" layer="94"/>
<rectangle x1="7.747" y1="7.32358125" x2="8.33881875" y2="7.40841875" layer="94"/>
<rectangle x1="9.017" y1="7.32358125" x2="9.69518125" y2="7.40841875" layer="94"/>
<rectangle x1="10.20318125" y1="7.32358125" x2="12.40281875" y2="7.40841875" layer="94"/>
<rectangle x1="12.91081875" y1="7.32358125" x2="13.335" y2="7.40841875" layer="94"/>
<rectangle x1="14.01318125" y1="7.32358125" x2="14.68881875" y2="7.40841875" layer="94"/>
<rectangle x1="15.28318125" y1="7.32358125" x2="15.95881875" y2="7.40841875" layer="94"/>
<rectangle x1="16.46681875" y1="7.32358125" x2="17.145" y2="7.40841875" layer="94"/>
<rectangle x1="17.73681875" y1="7.32358125" x2="19.939" y2="7.40841875" layer="94"/>
<rectangle x1="0.04318125" y1="7.40841875" x2="0.635" y2="7.493" layer="94"/>
<rectangle x1="2.413" y1="7.40841875" x2="3.00481875" y2="7.493" layer="94"/>
<rectangle x1="5.12318125" y1="7.40841875" x2="5.715" y2="7.493" layer="94"/>
<rectangle x1="7.747" y1="7.40841875" x2="8.33881875" y2="7.493" layer="94"/>
<rectangle x1="9.017" y1="7.40841875" x2="9.69518125" y2="7.493" layer="94"/>
<rectangle x1="10.20318125" y1="7.40841875" x2="12.40281875" y2="7.493" layer="94"/>
<rectangle x1="12.91081875" y1="7.40841875" x2="13.335" y2="7.493" layer="94"/>
<rectangle x1="14.26718125" y1="7.40841875" x2="14.43481875" y2="7.493" layer="94"/>
<rectangle x1="15.19681875" y1="7.40841875" x2="15.95881875" y2="7.493" layer="94"/>
<rectangle x1="16.72081875" y1="7.40841875" x2="16.891" y2="7.493" layer="94"/>
<rectangle x1="17.653" y1="7.40841875" x2="19.939" y2="7.493" layer="94"/>
<rectangle x1="0.04318125" y1="7.493" x2="0.635" y2="7.57758125" layer="94"/>
<rectangle x1="2.413" y1="7.493" x2="3.00481875" y2="7.57758125" layer="94"/>
<rectangle x1="5.12318125" y1="7.493" x2="5.715" y2="7.57758125" layer="94"/>
<rectangle x1="7.66318125" y1="7.493" x2="8.33881875" y2="7.57758125" layer="94"/>
<rectangle x1="9.017" y1="7.493" x2="9.69518125" y2="7.57758125" layer="94"/>
<rectangle x1="10.20318125" y1="7.493" x2="12.40281875" y2="7.57758125" layer="94"/>
<rectangle x1="12.91081875" y1="7.493" x2="13.335" y2="7.57758125" layer="94"/>
<rectangle x1="15.113" y1="7.493" x2="16.04518125" y2="7.57758125" layer="94"/>
<rectangle x1="17.56918125" y1="7.493" x2="19.939" y2="7.57758125" layer="94"/>
<rectangle x1="0.04318125" y1="7.57758125" x2="0.71881875" y2="7.66241875" layer="94"/>
<rectangle x1="2.413" y1="7.57758125" x2="3.00481875" y2="7.66241875" layer="94"/>
<rectangle x1="5.12318125" y1="7.57758125" x2="5.715" y2="7.66241875" layer="94"/>
<rectangle x1="7.66318125" y1="7.57758125" x2="8.33881875" y2="7.66241875" layer="94"/>
<rectangle x1="9.017" y1="7.57758125" x2="9.69518125" y2="7.66241875" layer="94"/>
<rectangle x1="10.20318125" y1="7.57758125" x2="12.40281875" y2="7.66241875" layer="94"/>
<rectangle x1="12.91081875" y1="7.57758125" x2="13.335" y2="7.66241875" layer="94"/>
<rectangle x1="15.02918125" y1="7.57758125" x2="16.21281875" y2="7.66241875" layer="94"/>
<rectangle x1="17.48281875" y1="7.57758125" x2="19.939" y2="7.66241875" layer="94"/>
<rectangle x1="0.04318125" y1="7.66241875" x2="0.71881875" y2="7.747" layer="94"/>
<rectangle x1="2.413" y1="7.66241875" x2="3.00481875" y2="7.747" layer="94"/>
<rectangle x1="5.12318125" y1="7.66241875" x2="5.715" y2="7.747" layer="94"/>
<rectangle x1="7.66318125" y1="7.66241875" x2="8.255" y2="7.747" layer="94"/>
<rectangle x1="9.017" y1="7.66241875" x2="9.779" y2="7.747" layer="94"/>
<rectangle x1="10.287" y1="7.66241875" x2="12.319" y2="7.747" layer="94"/>
<rectangle x1="12.827" y1="7.66241875" x2="13.335" y2="7.747" layer="94"/>
<rectangle x1="13.67281875" y1="7.66241875" x2="13.843" y2="7.747" layer="94"/>
<rectangle x1="14.859" y1="7.66241875" x2="16.29918125" y2="7.747" layer="94"/>
<rectangle x1="17.31518125" y1="7.66241875" x2="19.939" y2="7.747" layer="94"/>
<rectangle x1="0.127" y1="7.747" x2="0.71881875" y2="7.83158125" layer="94"/>
<rectangle x1="2.413" y1="7.747" x2="3.00481875" y2="7.83158125" layer="94"/>
<rectangle x1="5.12318125" y1="7.747" x2="5.715" y2="7.83158125" layer="94"/>
<rectangle x1="7.57681875" y1="7.747" x2="8.255" y2="7.83158125" layer="94"/>
<rectangle x1="9.017" y1="7.747" x2="9.779" y2="7.83158125" layer="94"/>
<rectangle x1="10.287" y1="7.747" x2="12.319" y2="7.83158125" layer="94"/>
<rectangle x1="12.827" y1="7.747" x2="13.335" y2="7.83158125" layer="94"/>
<rectangle x1="13.589" y1="7.747" x2="14.097" y2="7.83158125" layer="94"/>
<rectangle x1="14.68881875" y1="7.747" x2="16.55318125" y2="7.83158125" layer="94"/>
<rectangle x1="17.06118125" y1="7.747" x2="19.939" y2="7.83158125" layer="94"/>
<rectangle x1="0.127" y1="7.83158125" x2="0.71881875" y2="7.91641875" layer="94"/>
<rectangle x1="2.413" y1="7.83158125" x2="3.00481875" y2="7.91641875" layer="94"/>
<rectangle x1="5.12318125" y1="7.83158125" x2="5.715" y2="7.91641875" layer="94"/>
<rectangle x1="7.57681875" y1="7.83158125" x2="8.255" y2="7.91641875" layer="94"/>
<rectangle x1="9.017" y1="7.83158125" x2="9.779" y2="7.91641875" layer="94"/>
<rectangle x1="10.37081875" y1="7.83158125" x2="12.23518125" y2="7.91641875" layer="94"/>
<rectangle x1="12.827" y1="7.83158125" x2="19.939" y2="7.91641875" layer="94"/>
<rectangle x1="0.127" y1="7.91641875" x2="0.71881875" y2="8.001" layer="94"/>
<rectangle x1="2.413" y1="7.91641875" x2="3.00481875" y2="8.001" layer="94"/>
<rectangle x1="5.12318125" y1="7.91641875" x2="5.715" y2="8.001" layer="94"/>
<rectangle x1="7.493" y1="7.91641875" x2="8.17118125" y2="8.001" layer="94"/>
<rectangle x1="9.017" y1="7.91641875" x2="9.86281875" y2="8.001" layer="94"/>
<rectangle x1="10.45718125" y1="7.91641875" x2="12.14881875" y2="8.001" layer="94"/>
<rectangle x1="12.74318125" y1="7.91641875" x2="19.939" y2="8.001" layer="94"/>
<rectangle x1="0.127" y1="8.001" x2="0.71881875" y2="8.08558125" layer="94"/>
<rectangle x1="2.413" y1="8.001" x2="3.00481875" y2="8.08558125" layer="94"/>
<rectangle x1="5.12318125" y1="8.001" x2="5.715" y2="8.08558125" layer="94"/>
<rectangle x1="7.40918125" y1="8.001" x2="8.17118125" y2="8.08558125" layer="94"/>
<rectangle x1="9.017" y1="8.001" x2="9.86281875" y2="8.08558125" layer="94"/>
<rectangle x1="10.45718125" y1="8.001" x2="12.065" y2="8.08558125" layer="94"/>
<rectangle x1="12.74318125" y1="8.001" x2="19.939" y2="8.08558125" layer="94"/>
<rectangle x1="0.127" y1="8.08558125" x2="0.80518125" y2="8.17041875" layer="94"/>
<rectangle x1="2.413" y1="8.08558125" x2="3.00481875" y2="8.17041875" layer="94"/>
<rectangle x1="5.12318125" y1="8.08558125" x2="5.715" y2="8.17041875" layer="94"/>
<rectangle x1="7.32281875" y1="8.08558125" x2="8.08481875" y2="8.17041875" layer="94"/>
<rectangle x1="9.017" y1="8.08558125" x2="9.94918125" y2="8.17041875" layer="94"/>
<rectangle x1="10.62481875" y1="8.08558125" x2="11.98118125" y2="8.17041875" layer="94"/>
<rectangle x1="12.65681875" y1="8.08558125" x2="19.939" y2="8.17041875" layer="94"/>
<rectangle x1="0.127" y1="8.17041875" x2="0.80518125" y2="8.255" layer="94"/>
<rectangle x1="2.413" y1="8.17041875" x2="3.00481875" y2="8.255" layer="94"/>
<rectangle x1="5.12318125" y1="8.17041875" x2="5.715" y2="8.255" layer="94"/>
<rectangle x1="7.15518125" y1="8.17041875" x2="8.001" y2="8.255" layer="94"/>
<rectangle x1="9.017" y1="8.17041875" x2="9.94918125" y2="8.255" layer="94"/>
<rectangle x1="10.71118125" y1="8.17041875" x2="11.89481875" y2="8.255" layer="94"/>
<rectangle x1="12.573" y1="8.17041875" x2="19.939" y2="8.255" layer="94"/>
<rectangle x1="0.21081875" y1="8.255" x2="0.80518125" y2="8.33958125" layer="94"/>
<rectangle x1="2.413" y1="8.255" x2="3.00481875" y2="8.33958125" layer="94"/>
<rectangle x1="5.12318125" y1="8.255" x2="5.715" y2="8.33958125" layer="94"/>
<rectangle x1="6.90118125" y1="8.255" x2="8.001" y2="8.33958125" layer="94"/>
<rectangle x1="9.017" y1="8.255" x2="10.033" y2="8.33958125" layer="94"/>
<rectangle x1="10.87881875" y1="8.255" x2="11.72718125" y2="8.33958125" layer="94"/>
<rectangle x1="12.573" y1="8.255" x2="19.939" y2="8.33958125" layer="94"/>
<rectangle x1="0.21081875" y1="8.33958125" x2="0.889" y2="8.42441875" layer="94"/>
<rectangle x1="2.413" y1="8.33958125" x2="3.00481875" y2="8.42441875" layer="94"/>
<rectangle x1="5.12318125" y1="8.33958125" x2="7.91718125" y2="8.42441875" layer="94"/>
<rectangle x1="9.10081875" y1="8.33958125" x2="10.11681875" y2="8.42441875" layer="94"/>
<rectangle x1="11.21918125" y1="8.33958125" x2="11.38681875" y2="8.42441875" layer="94"/>
<rectangle x1="12.48918125" y1="8.33958125" x2="19.85518125" y2="8.42441875" layer="94"/>
<rectangle x1="0.21081875" y1="8.42441875" x2="0.889" y2="8.509" layer="94"/>
<rectangle x1="2.413" y1="8.42441875" x2="3.00481875" y2="8.509" layer="94"/>
<rectangle x1="5.12318125" y1="8.42441875" x2="7.83081875" y2="8.509" layer="94"/>
<rectangle x1="9.10081875" y1="8.42441875" x2="10.20318125" y2="8.509" layer="94"/>
<rectangle x1="12.40281875" y1="8.42441875" x2="19.85518125" y2="8.509" layer="94"/>
<rectangle x1="0.29718125" y1="8.509" x2="0.889" y2="8.59358125" layer="94"/>
<rectangle x1="2.413" y1="8.509" x2="3.00481875" y2="8.59358125" layer="94"/>
<rectangle x1="5.12318125" y1="8.509" x2="7.747" y2="8.59358125" layer="94"/>
<rectangle x1="9.10081875" y1="8.509" x2="10.287" y2="8.59358125" layer="94"/>
<rectangle x1="12.23518125" y1="8.509" x2="19.85518125" y2="8.59358125" layer="94"/>
<rectangle x1="0.29718125" y1="8.59358125" x2="0.97281875" y2="8.67841875" layer="94"/>
<rectangle x1="2.413" y1="8.59358125" x2="3.00481875" y2="8.67841875" layer="94"/>
<rectangle x1="5.12318125" y1="8.59358125" x2="7.57681875" y2="8.67841875" layer="94"/>
<rectangle x1="9.10081875" y1="8.59358125" x2="10.45718125" y2="8.67841875" layer="94"/>
<rectangle x1="12.14881875" y1="8.59358125" x2="19.76881875" y2="8.67841875" layer="94"/>
<rectangle x1="0.29718125" y1="8.67841875" x2="0.97281875" y2="8.763" layer="94"/>
<rectangle x1="2.413" y1="8.67841875" x2="3.00481875" y2="8.763" layer="94"/>
<rectangle x1="5.12318125" y1="8.67841875" x2="7.493" y2="8.763" layer="94"/>
<rectangle x1="9.10081875" y1="8.67841875" x2="10.62481875" y2="8.763" layer="94"/>
<rectangle x1="11.98118125" y1="8.67841875" x2="19.76881875" y2="8.763" layer="94"/>
<rectangle x1="0.381" y1="8.763" x2="1.05918125" y2="8.84758125" layer="94"/>
<rectangle x1="2.413" y1="8.763" x2="2.921" y2="8.84758125" layer="94"/>
<rectangle x1="5.207" y1="8.763" x2="7.32281875" y2="8.84758125" layer="94"/>
<rectangle x1="9.10081875" y1="8.763" x2="10.795" y2="8.84758125" layer="94"/>
<rectangle x1="11.72718125" y1="8.763" x2="19.76881875" y2="8.84758125" layer="94"/>
<rectangle x1="0.381" y1="8.84758125" x2="1.05918125" y2="8.93241875" layer="94"/>
<rectangle x1="2.58318125" y1="8.84758125" x2="2.83718125" y2="8.93241875" layer="94"/>
<rectangle x1="5.29081875" y1="8.84758125" x2="7.06881875" y2="8.93241875" layer="94"/>
<rectangle x1="9.18718125" y1="8.84758125" x2="19.685" y2="8.93241875" layer="94"/>
<rectangle x1="0.46481875" y1="8.93241875" x2="1.143" y2="9.017" layer="94"/>
<rectangle x1="9.18718125" y1="8.93241875" x2="19.685" y2="9.017" layer="94"/>
<rectangle x1="0.46481875" y1="9.017" x2="1.143" y2="9.10158125" layer="94"/>
<rectangle x1="9.18718125" y1="9.017" x2="19.60118125" y2="9.10158125" layer="94"/>
<rectangle x1="0.55118125" y1="9.10158125" x2="1.22681875" y2="9.18641875" layer="94"/>
<rectangle x1="9.18718125" y1="9.10158125" x2="19.60118125" y2="9.18641875" layer="94"/>
<rectangle x1="0.55118125" y1="9.18641875" x2="1.31318125" y2="9.271" layer="94"/>
<rectangle x1="9.18718125" y1="9.18641875" x2="19.51481875" y2="9.271" layer="94"/>
<rectangle x1="0.635" y1="9.271" x2="1.397" y2="9.35558125" layer="94"/>
<rectangle x1="9.271" y1="9.271" x2="19.51481875" y2="9.35558125" layer="94"/>
<rectangle x1="0.71881875" y1="9.35558125" x2="1.397" y2="9.44041875" layer="94"/>
<rectangle x1="9.271" y1="9.35558125" x2="19.431" y2="9.44041875" layer="94"/>
<rectangle x1="0.71881875" y1="9.44041875" x2="1.48081875" y2="9.525" layer="94"/>
<rectangle x1="9.271" y1="9.44041875" x2="19.34718125" y2="9.525" layer="94"/>
<rectangle x1="0.80518125" y1="9.525" x2="1.56718125" y2="9.60958125" layer="94"/>
<rectangle x1="9.35481875" y1="9.525" x2="19.34718125" y2="9.60958125" layer="94"/>
<rectangle x1="0.889" y1="9.60958125" x2="1.651" y2="9.69441875" layer="94"/>
<rectangle x1="9.35481875" y1="9.60958125" x2="19.26081875" y2="9.69441875" layer="94"/>
<rectangle x1="0.889" y1="9.69441875" x2="1.73481875" y2="9.779" layer="94"/>
<rectangle x1="9.35481875" y1="9.69441875" x2="19.177" y2="9.779" layer="94"/>
<rectangle x1="0.97281875" y1="9.779" x2="1.82118125" y2="9.86358125" layer="94"/>
<rectangle x1="9.44118125" y1="9.779" x2="19.09318125" y2="9.86358125" layer="94"/>
<rectangle x1="1.05918125" y1="9.86358125" x2="1.905" y2="9.94841875" layer="94"/>
<rectangle x1="9.44118125" y1="9.86358125" x2="19.00681875" y2="9.94841875" layer="94"/>
<rectangle x1="1.143" y1="9.94841875" x2="2.07518125" y2="10.033" layer="94"/>
<rectangle x1="9.44118125" y1="9.94841875" x2="18.923" y2="10.033" layer="94"/>
<rectangle x1="1.22681875" y1="10.033" x2="2.159" y2="10.11758125" layer="94"/>
<rectangle x1="9.525" y1="10.033" x2="18.923" y2="10.11758125" layer="94"/>
<rectangle x1="1.31318125" y1="10.11758125" x2="2.24281875" y2="10.20241875" layer="94"/>
<rectangle x1="9.525" y1="10.11758125" x2="18.75281875" y2="10.20241875" layer="94"/>
<rectangle x1="1.397" y1="10.20241875" x2="2.413" y2="10.287" layer="94"/>
<rectangle x1="9.60881875" y1="10.20241875" x2="18.669" y2="10.287" layer="94"/>
<rectangle x1="1.48081875" y1="10.287" x2="2.58318125" y2="10.37158125" layer="94"/>
<rectangle x1="9.60881875" y1="10.287" x2="18.58518125" y2="10.37158125" layer="94"/>
<rectangle x1="1.56718125" y1="10.37158125" x2="2.75081875" y2="10.45641875" layer="94"/>
<rectangle x1="9.69518125" y1="10.37158125" x2="18.49881875" y2="10.45641875" layer="94"/>
<rectangle x1="1.73481875" y1="10.45641875" x2="3.00481875" y2="10.541" layer="94"/>
<rectangle x1="9.69518125" y1="10.45641875" x2="18.415" y2="10.541" layer="94"/>
<rectangle x1="1.82118125" y1="10.541" x2="3.34518125" y2="10.62558125" layer="94"/>
<rectangle x1="9.779" y1="10.541" x2="18.24481875" y2="10.62558125" layer="94"/>
<rectangle x1="1.98881875" y1="10.62558125" x2="9.69518125" y2="10.71041875" layer="94"/>
<rectangle x1="9.779" y1="10.62558125" x2="18.161" y2="10.71041875" layer="94"/>
<rectangle x1="2.07518125" y1="10.71041875" x2="17.99081875" y2="10.795" layer="94"/>
<rectangle x1="2.24281875" y1="10.795" x2="17.82318125" y2="10.87958125" layer="94"/>
<rectangle x1="2.413" y1="10.87958125" x2="17.653" y2="10.96441875" layer="94"/>
<rectangle x1="2.58318125" y1="10.96441875" x2="17.48281875" y2="11.049" layer="94"/>
<rectangle x1="2.83718125" y1="11.049" x2="17.22881875" y2="11.13358125" layer="94"/>
<rectangle x1="3.175" y1="11.13358125" x2="16.891" y2="11.21841875" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="UDO-LOGO-" prefix="LOGO" uservalue="yes">
<gates>
<gate name="LOGO" symbol="UDO-LOGO-20MM" x="0" y="0"/>
</gates>
<devices>
<device name="10MM" package="UDO-LOGO-10MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6649258/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="12MM" package="UDO-LOGO-12MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6649257/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="15MM" package="UDO-LOGO-15MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6649256/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="20MM" package="UDO-LOGO-20MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="30MM" package="UDO-LOGO-30MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="40MM" package="UDO-LOGO-40MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-wago-mcs">
<packages>
<package name="P-733-342" urn="urn:adsk.eagle:footprint:9475022/1">
<wire x1="3.7" y1="14.75" x2="3.7" y2="12.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="12.25" x2="3.7" y2="10.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="9.75" x2="3.7" y2="7.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="7.25" x2="3.7" y2="5.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="4.75" x2="3.7" y2="2.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="2.25" x2="3.7" y2="0.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-0.25" x2="3.7" y2="-2.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-2.75" x2="3.7" y2="-4.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-5.25" x2="3.7" y2="-7.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-7.75" x2="3.7" y2="-9.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-10.25" x2="3.7" y2="-12.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-12.75" x2="3.7" y2="-14.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="14.75" x2="-3.7" y2="12.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="12.25" x2="-3.7" y2="10.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="9.75" x2="-3.7" y2="7.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="7.25" x2="-3.7" y2="5.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="4.75" x2="-3.7" y2="2.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="2.25" x2="-3.7" y2="0.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-0.25" x2="-3.7" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-2.75" x2="-3.7" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-5.25" x2="-3.7" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-7.75" x2="-3.7" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-10.25" x2="-3.7" y2="-12.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-12.75" x2="-3.7" y2="-14.75" width="0.01" layer="51"/>
<wire x1="-2.1" y1="-15.1" x2="-2.1" y2="15.1" width="0.01" layer="51"/>
<wire x1="3.5" y1="15.1" x2="3.5" y2="13.7" width="0.01" layer="51"/>
<wire x1="3.5" y1="13.7" x2="1.9" y2="13.7" width="0.01" layer="51"/>
<wire x1="1.9" y1="-15.1" x2="1.9" y2="-13.8" width="0.01" layer="51"/>
<wire x1="1.9" y1="-13.8" x2="1.9" y2="-12.4" width="0.01" layer="51"/>
<wire x1="1.9" y1="-12.4" x2="1.9" y2="15.1" width="0.01" layer="51"/>
<wire x1="1.9" y1="-12.4" x2="3.5" y2="-12.4" width="0.01" layer="51"/>
<wire x1="3.5" y1="-12.4" x2="3.5" y2="-13.8" width="0.01" layer="51"/>
<wire x1="3.5" y1="-13.8" x2="1.9" y2="-13.8" width="0.01" layer="51"/>
<wire x1="0.75" y1="15.1" x2="3.5" y2="15.1" width="0.01" layer="51"/>
<wire x1="-2.1" y1="15.1" x2="-0.75" y2="15.1" width="0.01" layer="51"/>
<wire x1="-0.75" y1="15.1" x2="-0.75" y2="15.4" width="0.01" layer="51"/>
<wire x1="-0.75" y1="15.4" x2="0.75" y2="15.4" width="0.01" layer="51"/>
<wire x1="0.75" y1="15.4" x2="0.75" y2="15.1" width="0.01" layer="51"/>
<wire x1="-0.75" y1="-15.1" x2="-2.1" y2="-15.1" width="0.01" layer="51"/>
<wire x1="1.9" y1="-15.1" x2="0.75" y2="-15.1" width="0.01" layer="51"/>
<wire x1="-0.75" y1="-15.4" x2="-0.75" y2="-15.1" width="0.01" layer="51"/>
<wire x1="0.75" y1="-15.1" x2="0.75" y2="-15.4" width="0.01" layer="51"/>
<wire x1="0.75" y1="-15.4" x2="-0.75" y2="-15.4" width="0.01" layer="51"/>
<wire x1="-1.1" y1="12.75" x2="-1.1" y2="14.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="12.75" x2="-1.1" y2="12.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="14.75" x2="1.1" y2="12.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="14.75" x2="1.1" y2="14.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-14.75" x2="-1.1" y2="-12.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="-14.75" x2="-1.1" y2="-14.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="-12.75" x2="1.1" y2="-14.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-12.75" x2="1.1" y2="-12.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-12.25" x2="-1.1" y2="-10.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="-12.25" x2="-1.1" y2="-12.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="-10.25" x2="1.1" y2="-12.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-10.25" x2="1.1" y2="-10.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-9.75" x2="-1.1" y2="-7.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="-9.75" x2="-1.1" y2="-9.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="-7.75" x2="1.1" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-7.75" x2="1.1" y2="-7.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-7.25" x2="-1.1" y2="-5.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="-7.25" x2="-1.1" y2="-7.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="-5.25" x2="1.1" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-5.25" x2="1.1" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-4.75" x2="-1.1" y2="-2.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="-4.75" x2="-1.1" y2="-4.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="-2.75" x2="1.1" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-2.75" x2="1.1" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-2.25" x2="-1.1" y2="-0.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="-2.25" x2="-1.1" y2="-2.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="-0.25" x2="1.1" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-0.25" x2="1.1" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="0.25" x2="-1.1" y2="2.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="0.25" x2="-1.1" y2="0.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="2.25" x2="1.1" y2="0.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="2.25" x2="1.1" y2="2.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="2.75" x2="-1.1" y2="4.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="2.75" x2="-1.1" y2="2.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="4.75" x2="1.1" y2="2.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="4.75" x2="1.1" y2="4.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="5.25" x2="-1.1" y2="7.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="5.25" x2="-1.1" y2="5.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="7.25" x2="1.1" y2="5.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="7.25" x2="1.1" y2="7.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="7.75" x2="-1.1" y2="9.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="7.75" x2="-1.1" y2="7.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="9.75" x2="1.1" y2="7.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="9.75" x2="1.1" y2="9.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="10.25" x2="-1.1" y2="12.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="10.25" x2="-1.1" y2="10.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="12.25" x2="1.1" y2="10.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="12.25" x2="1.1" y2="12.25" width="0.01" layer="51"/>
<wire x1="-0.4" y1="13.35" x2="-0.4" y2="14.15" width="0.01" layer="51"/>
<wire x1="0.4" y1="13.35" x2="-0.4" y2="13.35" width="0.01" layer="51"/>
<wire x1="-0.4" y1="14.15" x2="0.4" y2="14.15" width="0.01" layer="51"/>
<wire x1="0.4" y1="14.15" x2="0.4" y2="13.35" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-14.15" x2="-0.4" y2="-13.35" width="0.01" layer="51"/>
<wire x1="0.4" y1="-14.15" x2="-0.4" y2="-14.15" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-13.35" x2="0.4" y2="-13.35" width="0.01" layer="51"/>
<wire x1="0.4" y1="-13.35" x2="0.4" y2="-14.15" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-11.65" x2="-0.4" y2="-10.85" width="0.01" layer="51"/>
<wire x1="0.4" y1="-11.65" x2="-0.4" y2="-11.65" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-10.85" x2="0.4" y2="-10.85" width="0.01" layer="51"/>
<wire x1="0.4" y1="-10.85" x2="0.4" y2="-11.65" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-9.15" x2="-0.4" y2="-8.35" width="0.01" layer="51"/>
<wire x1="0.4" y1="-9.15" x2="-0.4" y2="-9.15" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-8.35" x2="0.4" y2="-8.35" width="0.01" layer="51"/>
<wire x1="0.4" y1="-8.35" x2="0.4" y2="-9.15" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-6.65" x2="-0.4" y2="-5.85" width="0.01" layer="51"/>
<wire x1="0.4" y1="-6.65" x2="-0.4" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-5.85" x2="0.4" y2="-5.85" width="0.01" layer="51"/>
<wire x1="0.4" y1="-5.85" x2="0.4" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-4.15" x2="-0.4" y2="-3.35" width="0.01" layer="51"/>
<wire x1="0.4" y1="-4.15" x2="-0.4" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-3.35" x2="0.4" y2="-3.35" width="0.01" layer="51"/>
<wire x1="0.4" y1="-3.35" x2="0.4" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-1.65" x2="-0.4" y2="-0.85" width="0.01" layer="51"/>
<wire x1="0.4" y1="-1.65" x2="-0.4" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-0.85" x2="0.4" y2="-0.85" width="0.01" layer="51"/>
<wire x1="0.4" y1="-0.85" x2="0.4" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-0.4" y1="0.85" x2="-0.4" y2="1.65" width="0.01" layer="51"/>
<wire x1="0.4" y1="0.85" x2="-0.4" y2="0.85" width="0.01" layer="51"/>
<wire x1="-0.4" y1="1.65" x2="0.4" y2="1.65" width="0.01" layer="51"/>
<wire x1="0.4" y1="1.65" x2="0.4" y2="0.85" width="0.01" layer="51"/>
<wire x1="-0.4" y1="3.35" x2="-0.4" y2="4.15" width="0.01" layer="51"/>
<wire x1="0.4" y1="3.35" x2="-0.4" y2="3.35" width="0.01" layer="51"/>
<wire x1="-0.4" y1="4.15" x2="0.4" y2="4.15" width="0.01" layer="51"/>
<wire x1="0.4" y1="4.15" x2="0.4" y2="3.35" width="0.01" layer="51"/>
<wire x1="-0.4" y1="5.85" x2="-0.4" y2="6.65" width="0.01" layer="51"/>
<wire x1="0.4" y1="5.85" x2="-0.4" y2="5.85" width="0.01" layer="51"/>
<wire x1="-0.4" y1="6.65" x2="0.4" y2="6.65" width="0.01" layer="51"/>
<wire x1="0.4" y1="6.65" x2="0.4" y2="5.85" width="0.01" layer="51"/>
<wire x1="-0.4" y1="8.35" x2="-0.4" y2="9.15" width="0.01" layer="51"/>
<wire x1="0.4" y1="8.35" x2="-0.4" y2="8.35" width="0.01" layer="51"/>
<wire x1="-0.4" y1="9.15" x2="0.4" y2="9.15" width="0.01" layer="51"/>
<wire x1="0.4" y1="9.15" x2="0.4" y2="8.35" width="0.01" layer="51"/>
<wire x1="-0.4" y1="10.85" x2="-0.4" y2="11.65" width="0.01" layer="51"/>
<wire x1="0.4" y1="10.85" x2="-0.4" y2="10.85" width="0.01" layer="51"/>
<wire x1="-0.4" y1="11.65" x2="0.4" y2="11.65" width="0.01" layer="51"/>
<wire x1="0.4" y1="11.65" x2="0.4" y2="10.85" width="0.01" layer="51"/>
<wire x1="3.1" y1="13.3" x2="3.1" y2="14.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="13.3" x2="3.1" y2="13.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="12.75" x2="3.6" y2="13.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="14.75" x2="3.7" y2="14.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="14.75" x2="3.9" y2="14.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="14.2" x2="3.6" y2="14.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="14.2" x2="3.6" y2="14.2" width="0.01" layer="51"/>
<wire x1="3.9" y1="12.75" x2="3.7" y2="12.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="12.75" x2="3.6" y2="12.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="-14.2" x2="3.1" y2="-13.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="-14.2" x2="3.1" y2="-14.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="-14.75" x2="3.6" y2="-14.2" width="0.01" layer="51"/>
<wire x1="3.9" y1="-14.75" x2="3.7" y2="-14.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-14.75" x2="3.6" y2="-14.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="-12.75" x2="3.7" y2="-12.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-12.75" x2="3.9" y2="-12.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="-13.3" x2="3.6" y2="-12.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="-13.3" x2="3.6" y2="-13.3" width="0.01" layer="51"/>
<wire x1="3.1" y1="10.8" x2="3.1" y2="11.7" width="0.01" layer="51"/>
<wire x1="3.6" y1="10.8" x2="3.1" y2="10.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="10.25" x2="3.6" y2="10.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="12.25" x2="3.7" y2="12.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="12.25" x2="3.9" y2="12.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="11.7" x2="3.6" y2="12.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="11.7" x2="3.6" y2="11.7" width="0.01" layer="51"/>
<wire x1="3.9" y1="10.25" x2="3.7" y2="10.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="10.25" x2="3.6" y2="10.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="-11.7" x2="3.1" y2="-10.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="-11.7" x2="3.1" y2="-11.7" width="0.01" layer="51"/>
<wire x1="3.6" y1="-12.25" x2="3.6" y2="-11.7" width="0.01" layer="51"/>
<wire x1="3.9" y1="-12.25" x2="3.7" y2="-12.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-12.25" x2="3.6" y2="-12.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="-10.25" x2="3.7" y2="-10.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-10.25" x2="3.9" y2="-10.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="-10.8" x2="3.6" y2="-10.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="-10.8" x2="3.6" y2="-10.8" width="0.01" layer="51"/>
<wire x1="3.1" y1="-9.2" x2="3.1" y2="-8.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="-9.2" x2="3.1" y2="-9.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="-9.75" x2="3.6" y2="-9.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="-7.75" x2="3.7" y2="-7.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-7.75" x2="3.9" y2="-7.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="-8.3" x2="3.6" y2="-7.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="-8.3" x2="3.6" y2="-8.3" width="0.01" layer="51"/>
<wire x1="3.9" y1="-9.75" x2="3.7" y2="-9.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-9.75" x2="3.6" y2="-9.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="-6.7" x2="3.1" y2="-5.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="-6.7" x2="3.1" y2="-6.7" width="0.01" layer="51"/>
<wire x1="3.6" y1="-7.25" x2="3.6" y2="-6.7" width="0.01" layer="51"/>
<wire x1="3.9" y1="-7.25" x2="3.7" y2="-7.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-7.25" x2="3.6" y2="-7.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="-5.25" x2="3.7" y2="-5.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-5.25" x2="3.9" y2="-5.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="-5.8" x2="3.6" y2="-5.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="-5.8" x2="3.6" y2="-5.8" width="0.01" layer="51"/>
<wire x1="3.1" y1="-4.2" x2="3.1" y2="-3.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="-4.2" x2="3.1" y2="-4.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="-4.75" x2="3.6" y2="-4.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="-2.75" x2="3.7" y2="-2.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-2.75" x2="3.9" y2="-2.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="-3.3" x2="3.6" y2="-2.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="-3.3" x2="3.6" y2="-3.3" width="0.01" layer="51"/>
<wire x1="3.9" y1="-4.75" x2="3.7" y2="-4.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-4.75" x2="3.6" y2="-4.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="-1.7" x2="3.1" y2="-0.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="-1.7" x2="3.1" y2="-1.7" width="0.01" layer="51"/>
<wire x1="3.6" y1="-2.25" x2="3.6" y2="-1.7" width="0.01" layer="51"/>
<wire x1="3.9" y1="-2.25" x2="3.7" y2="-2.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-2.25" x2="3.6" y2="-2.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="-0.25" x2="3.7" y2="-0.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-0.25" x2="3.9" y2="-0.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="-0.8" x2="3.6" y2="-0.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="-0.8" x2="3.6" y2="-0.8" width="0.01" layer="51"/>
<wire x1="3.1" y1="0.8" x2="3.1" y2="1.7" width="0.01" layer="51"/>
<wire x1="3.6" y1="0.8" x2="3.1" y2="0.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="0.25" x2="3.6" y2="0.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="2.25" x2="3.7" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="2.25" x2="3.9" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="1.7" x2="3.6" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="1.7" x2="3.6" y2="1.7" width="0.01" layer="51"/>
<wire x1="3.9" y1="0.25" x2="3.7" y2="0.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="0.25" x2="3.6" y2="0.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="3.3" x2="3.1" y2="4.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="3.3" x2="3.1" y2="3.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="2.75" x2="3.6" y2="3.3" width="0.01" layer="51"/>
<wire x1="3.9" y1="2.75" x2="3.7" y2="2.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="2.75" x2="3.6" y2="2.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="4.75" x2="3.7" y2="4.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="4.75" x2="3.9" y2="4.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="4.2" x2="3.6" y2="4.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="4.2" x2="3.6" y2="4.2" width="0.01" layer="51"/>
<wire x1="3.1" y1="5.8" x2="3.1" y2="6.7" width="0.01" layer="51"/>
<wire x1="3.6" y1="5.8" x2="3.1" y2="5.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="5.25" x2="3.6" y2="5.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="7.25" x2="3.7" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="7.25" x2="3.9" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="6.7" x2="3.6" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="6.7" x2="3.6" y2="6.7" width="0.01" layer="51"/>
<wire x1="3.9" y1="5.25" x2="3.7" y2="5.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="5.25" x2="3.6" y2="5.25" width="0.01" layer="51"/>
<wire x1="3.9" y1="16.25" x2="3.9" y2="-16.25" width="0.2" layer="21"/>
<wire x1="3.1" y1="8.3" x2="3.1" y2="9.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="8.3" x2="3.1" y2="8.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="7.75" x2="3.6" y2="8.3" width="0.01" layer="51"/>
<wire x1="3.9" y1="7.75" x2="3.7" y2="7.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="7.75" x2="3.6" y2="7.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="9.75" x2="3.7" y2="9.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="9.75" x2="3.9" y2="9.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="9.2" x2="3.6" y2="9.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="9.2" x2="3.6" y2="9.2" width="0.01" layer="51"/>
<wire x1="-2.7" y1="14.75" x2="-2.7" y2="12.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="14.75" x2="-3.7" y2="14.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="14.75" x2="-2.7" y2="14.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="12.75" x2="-3.7" y2="12.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="12.75" x2="-3.9" y2="12.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-14.75" x2="-3.7" y2="-14.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-14.75" x2="-3.9" y2="-14.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-12.75" x2="-2.7" y2="-14.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="-12.75" x2="-3.7" y2="-12.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-12.75" x2="-2.7" y2="-12.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-12.25" x2="-3.7" y2="-12.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-12.25" x2="-3.9" y2="-12.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-10.25" x2="-2.7" y2="-12.25" width="0.01" layer="51"/>
<wire x1="-3.9" y1="-10.25" x2="-3.7" y2="-10.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-10.25" x2="-2.7" y2="-10.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-9.75" x2="-3.7" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-9.75" x2="-3.9" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-7.75" x2="-2.7" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="-7.75" x2="-3.7" y2="-7.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-7.75" x2="-2.7" y2="-7.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-7.25" x2="-3.7" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-7.25" x2="-3.9" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-5.25" x2="-2.7" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-3.9" y1="-5.25" x2="-3.7" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-5.25" x2="-2.7" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-4.75" x2="-3.7" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-4.75" x2="-3.9" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-2.75" x2="-2.7" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="-2.75" x2="-3.7" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-2.75" x2="-2.7" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-2.25" x2="-3.7" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-2.25" x2="-3.9" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-0.25" x2="-2.7" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-3.9" y1="-0.25" x2="-3.7" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-0.25" x2="-2.7" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="0.25" x2="-3.7" y2="0.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="0.25" x2="-3.9" y2="0.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="2.25" x2="-2.7" y2="0.25" width="0.01" layer="51"/>
<wire x1="-3.9" y1="2.25" x2="-3.7" y2="2.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="2.25" x2="-2.7" y2="2.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="2.75" x2="-3.7" y2="2.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="2.75" x2="-3.9" y2="2.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="4.75" x2="-2.7" y2="2.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="4.75" x2="-3.7" y2="4.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="4.75" x2="-2.7" y2="4.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="5.25" x2="-3.7" y2="5.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="5.25" x2="-3.9" y2="5.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="7.25" x2="-2.7" y2="5.25" width="0.01" layer="51"/>
<wire x1="-3.9" y1="7.25" x2="-3.7" y2="7.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="7.25" x2="-2.7" y2="7.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="7.75" x2="-3.7" y2="7.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="7.75" x2="-3.9" y2="7.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="9.75" x2="-2.7" y2="7.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="9.75" x2="-3.7" y2="9.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="9.75" x2="-2.7" y2="9.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="16.25" x2="-3.9" y2="-16.25" width="0.2" layer="21"/>
<wire x1="-2.7" y1="10.25" x2="-3.7" y2="10.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="10.25" x2="-3.9" y2="10.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="12.25" x2="-2.7" y2="10.25" width="0.01" layer="51"/>
<wire x1="-3.9" y1="12.25" x2="-3.7" y2="12.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="12.25" x2="-2.7" y2="12.25" width="0.01" layer="51"/>
<wire x1="-0.75" y1="15.4" x2="-0.75" y2="15.75" width="0.01" layer="51"/>
<wire x1="0.75" y1="15.75" x2="0.75" y2="15.4" width="0.01" layer="51"/>
<wire x1="-2.5" y1="15.75" x2="-2.5" y2="16.25" width="0.01" layer="51"/>
<wire x1="-0.75" y1="15.75" x2="-2.5" y2="15.75" width="0.01" layer="51"/>
<wire x1="2.5" y1="15.75" x2="0.75" y2="15.75" width="0.01" layer="51"/>
<wire x1="2.5" y1="16.25" x2="2.5" y2="15.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-14.75" x2="3.7" y2="-16.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-12.25" x2="3.7" y2="-12.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-9.75" x2="3.7" y2="-10.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-7.25" x2="3.7" y2="-7.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-4.75" x2="3.7" y2="-5.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-2.25" x2="3.7" y2="-2.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="0.25" x2="3.7" y2="-0.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="2.75" x2="3.7" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="5.25" x2="3.7" y2="4.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="7.75" x2="3.7" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="10.25" x2="3.7" y2="9.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="12.75" x2="3.7" y2="12.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="16.25" x2="3.7" y2="14.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="16.25" x2="3.9" y2="16.25" width="0.2" layer="21"/>
<wire x1="-3.7" y1="16.25" x2="-3.7" y2="14.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="12.75" x2="-3.7" y2="12.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="10.25" x2="-3.7" y2="9.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="7.75" x2="-3.7" y2="7.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="5.25" x2="-3.7" y2="4.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="2.75" x2="-3.7" y2="2.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="0.25" x2="-3.7" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-2.25" x2="-3.7" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-4.75" x2="-3.7" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-7.25" x2="-3.7" y2="-7.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-9.75" x2="-3.7" y2="-10.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-12.25" x2="-3.7" y2="-12.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-14.75" x2="-3.7" y2="-16.25" width="0.01" layer="51"/>
<wire x1="3.9" y1="-16.25" x2="-3.9" y2="-16.25" width="0.2" layer="21"/>
<wire x1="-0.75" y1="-15.4" x2="-0.75" y2="-15.75" width="0.01" layer="51"/>
<wire x1="0.75" y1="-15.75" x2="0.75" y2="-15.4" width="0.01" layer="51"/>
<wire x1="2.5" y1="-16.25" x2="2.5" y2="-15.75" width="0.01" layer="51"/>
<wire x1="2.5" y1="-15.75" x2="0.75" y2="-15.75" width="0.01" layer="51"/>
<wire x1="-0.75" y1="-15.75" x2="-2.5" y2="-15.75" width="0.01" layer="51"/>
<wire x1="-2.5" y1="-15.75" x2="-2.5" y2="-16.25" width="0.01" layer="51"/>
<pad name="L12" x="0" y="13.75" drill="1.1" shape="long"/>
<pad name="L11" x="0" y="11.25" drill="1.1" shape="long"/>
<pad name="L10" x="0" y="8.75" drill="1.1" shape="long"/>
<pad name="L9" x="0" y="6.25" drill="1.1" shape="long"/>
<pad name="L8" x="0" y="3.75" drill="1.1" shape="long"/>
<pad name="L7" x="0" y="1.25" drill="1.1" shape="long"/>
<pad name="L6" x="0" y="-1.25" drill="1.1" shape="long"/>
<pad name="L5" x="0" y="-3.75" drill="1.1" shape="long"/>
<pad name="L4" x="0" y="-6.25" drill="1.1" shape="long"/>
<pad name="L3" x="0" y="-8.75" drill="1.1" shape="long"/>
<pad name="L2" x="0" y="-11.25" drill="1.1" shape="long"/>
<pad name="L1" x="0" y="-13.75" drill="1.1" shape="long"/>
<text x="-3.4" y="18.02" size="2" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.73" y="-15.46" size="2" layer="51" font="vector" ratio="10" rot="R90">1</text>
<text x="-3.4" y="-18.92" size="2" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="P-733-338" urn="urn:adsk.eagle:footprint:10511810/1">
<wire x1="3.7" y1="9.75" x2="3.7" y2="7.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="7.25" x2="3.7" y2="5.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="4.75" x2="3.7" y2="2.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="2.25" x2="3.7" y2="0.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-0.25" x2="3.7" y2="-2.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-2.75" x2="3.7" y2="-4.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-5.25" x2="3.7" y2="-7.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-7.75" x2="3.7" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="9.75" x2="-3.7" y2="7.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="7.25" x2="-3.7" y2="5.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="4.75" x2="-3.7" y2="2.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="2.25" x2="-3.7" y2="0.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-0.25" x2="-3.7" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-2.75" x2="-3.7" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-5.25" x2="-3.7" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-7.75" x2="-3.7" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-2.1" y1="-10.1" x2="-2.1" y2="10.1" width="0.01" layer="51"/>
<wire x1="3.5" y1="10.1" x2="3.5" y2="8.7" width="0.01" layer="51"/>
<wire x1="3.5" y1="8.7" x2="1.9" y2="8.7" width="0.01" layer="51"/>
<wire x1="1.9" y1="-10.1" x2="1.9" y2="-8.8" width="0.01" layer="51"/>
<wire x1="1.9" y1="-8.8" x2="1.9" y2="-7.4" width="0.01" layer="51"/>
<wire x1="1.9" y1="-7.4" x2="1.9" y2="10.1" width="0.01" layer="51"/>
<wire x1="1.9" y1="-7.4" x2="3.5" y2="-7.4" width="0.01" layer="51"/>
<wire x1="3.5" y1="-7.4" x2="3.5" y2="-8.8" width="0.01" layer="51"/>
<wire x1="3.5" y1="-8.8" x2="1.9" y2="-8.8" width="0.01" layer="51"/>
<wire x1="0.75" y1="10.1" x2="3.5" y2="10.1" width="0.01" layer="51"/>
<wire x1="-2.1" y1="10.1" x2="-0.75" y2="10.1" width="0.01" layer="51"/>
<wire x1="-0.75" y1="10.1" x2="-0.75" y2="10.4" width="0.01" layer="51"/>
<wire x1="-0.75" y1="10.4" x2="0.75" y2="10.4" width="0.01" layer="51"/>
<wire x1="0.75" y1="10.4" x2="0.75" y2="10.1" width="0.01" layer="51"/>
<wire x1="-0.75" y1="-10.1" x2="-2.1" y2="-10.1" width="0.01" layer="51"/>
<wire x1="1.9" y1="-10.1" x2="0.75" y2="-10.1" width="0.01" layer="51"/>
<wire x1="-0.75" y1="-10.4" x2="-0.75" y2="-10.1" width="0.01" layer="51"/>
<wire x1="0.75" y1="-10.1" x2="0.75" y2="-10.4" width="0.01" layer="51"/>
<wire x1="0.75" y1="-10.4" x2="-0.75" y2="-10.4" width="0.01" layer="51"/>
<wire x1="-1.1" y1="7.75" x2="-1.1" y2="9.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="7.75" x2="-1.1" y2="7.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="9.75" x2="1.1" y2="7.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="9.75" x2="1.1" y2="9.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-9.75" x2="-1.1" y2="-7.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="-9.75" x2="-1.1" y2="-9.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="-7.75" x2="1.1" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-7.75" x2="1.1" y2="-7.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-7.25" x2="-1.1" y2="-5.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="-7.25" x2="-1.1" y2="-7.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="-5.25" x2="1.1" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-5.25" x2="1.1" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-4.75" x2="-1.1" y2="-2.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="-4.75" x2="-1.1" y2="-4.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="-2.75" x2="1.1" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-2.75" x2="1.1" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-2.25" x2="-1.1" y2="-0.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="-2.25" x2="-1.1" y2="-2.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="-0.25" x2="1.1" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="-0.25" x2="1.1" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="0.25" x2="-1.1" y2="2.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="0.25" x2="-1.1" y2="0.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="2.25" x2="1.1" y2="0.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="2.25" x2="1.1" y2="2.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="2.75" x2="-1.1" y2="4.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="2.75" x2="-1.1" y2="2.75" width="0.01" layer="51"/>
<wire x1="1.1" y1="4.75" x2="1.1" y2="2.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="4.75" x2="1.1" y2="4.75" width="0.01" layer="51"/>
<wire x1="-1.1" y1="5.25" x2="-1.1" y2="7.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="5.25" x2="-1.1" y2="5.25" width="0.01" layer="51"/>
<wire x1="1.1" y1="7.25" x2="1.1" y2="5.25" width="0.01" layer="51"/>
<wire x1="-1.1" y1="7.25" x2="1.1" y2="7.25" width="0.01" layer="51"/>
<wire x1="-0.4" y1="8.35" x2="-0.4" y2="9.15" width="0.01" layer="51"/>
<wire x1="0.4" y1="8.35" x2="-0.4" y2="8.35" width="0.01" layer="51"/>
<wire x1="-0.4" y1="9.15" x2="0.4" y2="9.15" width="0.01" layer="51"/>
<wire x1="0.4" y1="9.15" x2="0.4" y2="8.35" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-9.15" x2="-0.4" y2="-8.35" width="0.01" layer="51"/>
<wire x1="0.4" y1="-9.15" x2="-0.4" y2="-9.15" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-8.35" x2="0.4" y2="-8.35" width="0.01" layer="51"/>
<wire x1="0.4" y1="-8.35" x2="0.4" y2="-9.15" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-6.65" x2="-0.4" y2="-5.85" width="0.01" layer="51"/>
<wire x1="0.4" y1="-6.65" x2="-0.4" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-5.85" x2="0.4" y2="-5.85" width="0.01" layer="51"/>
<wire x1="0.4" y1="-5.85" x2="0.4" y2="-6.65" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-4.15" x2="-0.4" y2="-3.35" width="0.01" layer="51"/>
<wire x1="0.4" y1="-4.15" x2="-0.4" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-3.35" x2="0.4" y2="-3.35" width="0.01" layer="51"/>
<wire x1="0.4" y1="-3.35" x2="0.4" y2="-4.15" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-1.65" x2="-0.4" y2="-0.85" width="0.01" layer="51"/>
<wire x1="0.4" y1="-1.65" x2="-0.4" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-0.4" y1="-0.85" x2="0.4" y2="-0.85" width="0.01" layer="51"/>
<wire x1="0.4" y1="-0.85" x2="0.4" y2="-1.65" width="0.01" layer="51"/>
<wire x1="-0.4" y1="0.85" x2="-0.4" y2="1.65" width="0.01" layer="51"/>
<wire x1="0.4" y1="0.85" x2="-0.4" y2="0.85" width="0.01" layer="51"/>
<wire x1="-0.4" y1="1.65" x2="0.4" y2="1.65" width="0.01" layer="51"/>
<wire x1="0.4" y1="1.65" x2="0.4" y2="0.85" width="0.01" layer="51"/>
<wire x1="-0.4" y1="3.35" x2="-0.4" y2="4.15" width="0.01" layer="51"/>
<wire x1="0.4" y1="3.35" x2="-0.4" y2="3.35" width="0.01" layer="51"/>
<wire x1="-0.4" y1="4.15" x2="0.4" y2="4.15" width="0.01" layer="51"/>
<wire x1="0.4" y1="4.15" x2="0.4" y2="3.35" width="0.01" layer="51"/>
<wire x1="-0.4" y1="5.85" x2="-0.4" y2="6.65" width="0.01" layer="51"/>
<wire x1="0.4" y1="5.85" x2="-0.4" y2="5.85" width="0.01" layer="51"/>
<wire x1="-0.4" y1="6.65" x2="0.4" y2="6.65" width="0.01" layer="51"/>
<wire x1="0.4" y1="6.65" x2="0.4" y2="5.85" width="0.01" layer="51"/>
<wire x1="3.1" y1="8.3" x2="3.1" y2="9.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="8.3" x2="3.1" y2="8.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="7.75" x2="3.6" y2="8.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="9.75" x2="3.7" y2="9.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="9.75" x2="3.9" y2="9.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="9.2" x2="3.6" y2="9.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="9.2" x2="3.6" y2="9.2" width="0.01" layer="51"/>
<wire x1="3.9" y1="7.75" x2="3.7" y2="7.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="7.75" x2="3.6" y2="7.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="-9.2" x2="3.1" y2="-8.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="-9.2" x2="3.1" y2="-9.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="-9.75" x2="3.6" y2="-9.2" width="0.01" layer="51"/>
<wire x1="3.9" y1="-9.75" x2="3.7" y2="-9.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-9.75" x2="3.6" y2="-9.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="-7.75" x2="3.7" y2="-7.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-7.75" x2="3.9" y2="-7.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="-8.3" x2="3.6" y2="-7.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="-8.3" x2="3.6" y2="-8.3" width="0.01" layer="51"/>
<wire x1="3.1" y1="5.8" x2="3.1" y2="6.7" width="0.01" layer="51"/>
<wire x1="3.6" y1="5.8" x2="3.1" y2="5.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="5.25" x2="3.6" y2="5.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="7.25" x2="3.7" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="7.25" x2="3.9" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="6.7" x2="3.6" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="6.7" x2="3.6" y2="6.7" width="0.01" layer="51"/>
<wire x1="3.9" y1="5.25" x2="3.7" y2="5.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="5.25" x2="3.6" y2="5.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="-6.7" x2="3.1" y2="-5.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="-6.7" x2="3.1" y2="-6.7" width="0.01" layer="51"/>
<wire x1="3.6" y1="-7.25" x2="3.6" y2="-6.7" width="0.01" layer="51"/>
<wire x1="3.9" y1="-7.25" x2="3.7" y2="-7.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-7.25" x2="3.6" y2="-7.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="-5.25" x2="3.7" y2="-5.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-5.25" x2="3.9" y2="-5.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="-5.8" x2="3.6" y2="-5.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="-5.8" x2="3.6" y2="-5.8" width="0.01" layer="51"/>
<wire x1="3.1" y1="-4.2" x2="3.1" y2="-3.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="-4.2" x2="3.1" y2="-4.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="-4.75" x2="3.6" y2="-4.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="-2.75" x2="3.7" y2="-2.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-2.75" x2="3.9" y2="-2.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="-3.3" x2="3.6" y2="-2.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="-3.3" x2="3.6" y2="-3.3" width="0.01" layer="51"/>
<wire x1="3.9" y1="-4.75" x2="3.7" y2="-4.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-4.75" x2="3.6" y2="-4.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="-1.7" x2="3.1" y2="-0.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="-1.7" x2="3.1" y2="-1.7" width="0.01" layer="51"/>
<wire x1="3.6" y1="-2.25" x2="3.6" y2="-1.7" width="0.01" layer="51"/>
<wire x1="3.9" y1="-2.25" x2="3.7" y2="-2.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-2.25" x2="3.6" y2="-2.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="-0.25" x2="3.7" y2="-0.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-0.25" x2="3.9" y2="-0.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="-0.8" x2="3.6" y2="-0.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="-0.8" x2="3.6" y2="-0.8" width="0.01" layer="51"/>
<wire x1="3.1" y1="0.8" x2="3.1" y2="1.7" width="0.01" layer="51"/>
<wire x1="3.6" y1="0.8" x2="3.1" y2="0.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="0.25" x2="3.6" y2="0.8" width="0.01" layer="51"/>
<wire x1="3.6" y1="2.25" x2="3.7" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="2.25" x2="3.9" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.6" y1="1.7" x2="3.6" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.1" y1="1.7" x2="3.6" y2="1.7" width="0.01" layer="51"/>
<wire x1="3.9" y1="0.25" x2="3.7" y2="0.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="0.25" x2="3.6" y2="0.25" width="0.01" layer="51"/>
<wire x1="3.9" y1="11.25" x2="3.9" y2="-11.25" width="0.2" layer="21"/>
<wire x1="3.1" y1="3.3" x2="3.1" y2="4.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="3.3" x2="3.1" y2="3.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="2.75" x2="3.6" y2="3.3" width="0.01" layer="51"/>
<wire x1="3.9" y1="2.75" x2="3.7" y2="2.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="2.75" x2="3.6" y2="2.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="4.75" x2="3.7" y2="4.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="4.75" x2="3.9" y2="4.75" width="0.01" layer="51"/>
<wire x1="3.6" y1="4.2" x2="3.6" y2="4.75" width="0.01" layer="51"/>
<wire x1="3.1" y1="4.2" x2="3.6" y2="4.2" width="0.01" layer="51"/>
<wire x1="-2.7" y1="9.75" x2="-2.7" y2="7.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="9.75" x2="-3.7" y2="9.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="9.75" x2="-2.7" y2="9.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="7.75" x2="-3.7" y2="7.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="7.75" x2="-3.9" y2="7.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-9.75" x2="-3.7" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-9.75" x2="-3.9" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-7.75" x2="-2.7" y2="-9.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="-7.75" x2="-3.7" y2="-7.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-7.75" x2="-2.7" y2="-7.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-7.25" x2="-3.7" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-7.25" x2="-3.9" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-5.25" x2="-2.7" y2="-7.25" width="0.01" layer="51"/>
<wire x1="-3.9" y1="-5.25" x2="-3.7" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-5.25" x2="-2.7" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-4.75" x2="-3.7" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-4.75" x2="-3.9" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-2.75" x2="-2.7" y2="-4.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="-2.75" x2="-3.7" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-2.75" x2="-2.7" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-2.25" x2="-3.7" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-2.25" x2="-3.9" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="-0.25" x2="-2.7" y2="-2.25" width="0.01" layer="51"/>
<wire x1="-3.9" y1="-0.25" x2="-3.7" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-0.25" x2="-2.7" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="0.25" x2="-3.7" y2="0.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="0.25" x2="-3.9" y2="0.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="2.25" x2="-2.7" y2="0.25" width="0.01" layer="51"/>
<wire x1="-3.9" y1="2.25" x2="-3.7" y2="2.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="2.25" x2="-2.7" y2="2.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="2.75" x2="-3.7" y2="2.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="2.75" x2="-3.9" y2="2.75" width="0.01" layer="51"/>
<wire x1="-2.7" y1="4.75" x2="-2.7" y2="2.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="4.75" x2="-3.7" y2="4.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="4.75" x2="-2.7" y2="4.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="11.25" x2="-3.9" y2="-11.25" width="0.2" layer="21"/>
<wire x1="-2.7" y1="5.25" x2="-3.7" y2="5.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="5.25" x2="-3.9" y2="5.25" width="0.01" layer="51"/>
<wire x1="-2.7" y1="7.25" x2="-2.7" y2="5.25" width="0.01" layer="51"/>
<wire x1="-3.9" y1="7.25" x2="-3.7" y2="7.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="7.25" x2="-2.7" y2="7.25" width="0.01" layer="51"/>
<wire x1="-0.75" y1="10.4" x2="-0.75" y2="10.75" width="0.01" layer="51"/>
<wire x1="0.75" y1="10.75" x2="0.75" y2="10.4" width="0.01" layer="51"/>
<wire x1="-2.5" y1="10.75" x2="-2.5" y2="11.25" width="0.01" layer="51"/>
<wire x1="-0.75" y1="10.75" x2="-2.5" y2="10.75" width="0.01" layer="51"/>
<wire x1="2.5" y1="10.75" x2="0.75" y2="10.75" width="0.01" layer="51"/>
<wire x1="2.5" y1="11.25" x2="2.5" y2="10.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-9.75" x2="3.7" y2="-11.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-7.25" x2="3.7" y2="-7.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="-4.75" x2="3.7" y2="-5.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="-2.25" x2="3.7" y2="-2.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="0.25" x2="3.7" y2="-0.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="2.75" x2="3.7" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="5.25" x2="3.7" y2="4.75" width="0.01" layer="51"/>
<wire x1="3.7" y1="7.75" x2="3.7" y2="7.25" width="0.01" layer="51"/>
<wire x1="3.7" y1="11.25" x2="3.7" y2="9.75" width="0.01" layer="51"/>
<wire x1="-3.9" y1="11.25" x2="3.9" y2="11.25" width="0.2" layer="21"/>
<wire x1="-3.7" y1="11.25" x2="-3.7" y2="9.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="7.75" x2="-3.7" y2="7.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="5.25" x2="-3.7" y2="4.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="2.75" x2="-3.7" y2="2.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="0.25" x2="-3.7" y2="-0.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-2.25" x2="-3.7" y2="-2.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-4.75" x2="-3.7" y2="-5.25" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-7.25" x2="-3.7" y2="-7.75" width="0.01" layer="51"/>
<wire x1="-3.7" y1="-9.75" x2="-3.7" y2="-11.25" width="0.01" layer="51"/>
<wire x1="3.9" y1="-11.25" x2="-3.9" y2="-11.25" width="0.2" layer="21"/>
<wire x1="-0.75" y1="-10.4" x2="-0.75" y2="-10.75" width="0.01" layer="51"/>
<wire x1="0.75" y1="-10.75" x2="0.75" y2="-10.4" width="0.01" layer="51"/>
<wire x1="2.5" y1="-11.25" x2="2.5" y2="-10.75" width="0.01" layer="51"/>
<wire x1="2.5" y1="-10.75" x2="0.75" y2="-10.75" width="0.01" layer="51"/>
<wire x1="-0.75" y1="-10.75" x2="-2.5" y2="-10.75" width="0.01" layer="51"/>
<wire x1="-2.5" y1="-10.75" x2="-2.5" y2="-11.25" width="0.01" layer="51"/>
<pad name="L8" x="0" y="8.75" drill="1.1" shape="long"/>
<pad name="L7" x="0" y="6.25" drill="1.1" shape="long"/>
<pad name="L6" x="0" y="3.75" drill="1.1" shape="long"/>
<pad name="L5" x="0" y="1.25" drill="1.1" shape="long"/>
<pad name="L4" x="0" y="-1.25" drill="1.1" shape="long"/>
<pad name="L3" x="0" y="-3.75" drill="1.1" shape="long"/>
<pad name="L2" x="0" y="-6.25" drill="1.1" shape="long"/>
<pad name="L1" x="0" y="-8.75" drill="1.1" shape="long"/>
<text x="-3.4" y="13.02" size="2" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.73" y="-10.3" size="2" layer="51" font="vector" ratio="10" rot="R90">1</text>
<text x="-3.4" y="-14.22" size="2" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="P-721-134/001-000" urn="urn:adsk.eagle:footprint:11036716/1">
<wire x1="1.8" y1="11.6" x2="1.8" y2="-11.6" width="0.2" layer="21"/>
<wire x1="2.7" y1="11.6" x2="2.1" y2="11.6" width="0.2" layer="21"/>
<wire x1="2.1" y1="11.6" x2="2.0301" y2="11.6" width="0.2" layer="21"/>
<wire x1="2.0301" y1="11.6" x2="-5.7" y2="11.6" width="0.2" layer="21"/>
<wire x1="2.7" y1="11.6" x2="2.7" y2="7.2" width="0.2" layer="21"/>
<wire x1="-4.1" y1="5.3" x2="-4.1" y2="4.7" width="0.01" layer="51"/>
<wire x1="-4.1" y1="5.3" x2="-4.3679" y2="5.3" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="5.3" x2="-4.5" y2="5.3" width="0.01" layer="51"/>
<wire x1="-4.1" y1="-9.8" x2="-4.1" y2="-10.72" width="0.01" layer="51"/>
<wire x1="-4.5" y1="-5.3" x2="-4.3679" y2="-5.3" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="-5.3" x2="-4.1" y2="-5.3" width="0.01" layer="51"/>
<wire x1="-4.1" y1="-4.7" x2="-4.1" y2="-5.3" width="0.01" layer="51"/>
<wire x1="-4.1" y1="-4.7" x2="-4.3679" y2="-4.7" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="-4.7" x2="-4.5" y2="-4.7" width="0.01" layer="51"/>
<wire x1="-4.5" y1="-0.3" x2="-4.3679" y2="-0.3" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="-0.3" x2="-4.1" y2="-0.3" width="0.01" layer="51"/>
<wire x1="-4.1" y1="0.3" x2="-4.1" y2="-0.3" width="0.01" layer="51"/>
<wire x1="-4.5" y1="4.7" x2="-4.3679" y2="4.7" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="4.7" x2="-4.1" y2="4.7" width="0.01" layer="51"/>
<wire x1="-4.1" y1="0.3" x2="-4.3679" y2="0.3" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="0.3" x2="-4.5" y2="0.3" width="0.01" layer="51"/>
<wire x1="-4.1" y1="10.72" x2="-4.1" y2="9.85" width="0.01" layer="51"/>
<wire x1="-4.5" y1="9.85" x2="-4.3679" y2="9.85" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="9.85" x2="-4.1" y2="9.85" width="0.01" layer="51"/>
<wire x1="-4.1" y1="-9.8" x2="-4.3679" y2="-9.8" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="-9.8" x2="-4.5" y2="-9.8" width="0.01" layer="51"/>
<wire x1="-1.4" y1="7" x2="-2.4" y2="7" width="0.01" layer="51"/>
<wire x1="-1.4" y1="8" x2="-1.4" y2="7" width="0.01" layer="51"/>
<wire x1="-2.4" y1="8" x2="-1.4" y2="8" width="0.01" layer="51"/>
<wire x1="-2.4" y1="7" x2="-2.4" y2="8" width="0.01" layer="51"/>
<wire x1="-1.4" y1="-8" x2="-2.4" y2="-8" width="0.01" layer="51"/>
<wire x1="-1.4" y1="-7" x2="-1.4" y2="-8" width="0.01" layer="51"/>
<wire x1="-2.4" y1="-7" x2="-1.4" y2="-7" width="0.01" layer="51"/>
<wire x1="-2.4" y1="-8" x2="-2.4" y2="-7" width="0.01" layer="51"/>
<wire x1="-1.4" y1="-3" x2="-2.4" y2="-3" width="0.01" layer="51"/>
<wire x1="-1.4" y1="-2" x2="-1.4" y2="-3" width="0.01" layer="51"/>
<wire x1="-2.4" y1="-2" x2="-1.4" y2="-2" width="0.01" layer="51"/>
<wire x1="-2.4" y1="-3" x2="-2.4" y2="-2" width="0.01" layer="51"/>
<wire x1="-1.4" y1="2" x2="-2.4" y2="2" width="0.01" layer="51"/>
<wire x1="-1.4" y1="3" x2="-1.4" y2="2" width="0.01" layer="51"/>
<wire x1="-2.4" y1="3" x2="-1.4" y2="3" width="0.01" layer="51"/>
<wire x1="-2.4" y1="2" x2="-2.4" y2="3" width="0.01" layer="51"/>
<wire x1="1" y1="10.72" x2="1" y2="6.4" width="0.01" layer="51"/>
<wire x1="2.7" y1="3.8" x2="2.7" y2="-2.8" width="0.2" layer="21"/>
<wire x1="2.7" y1="-6.2" x2="2.7" y2="-11.6" width="0.2" layer="21"/>
<wire x1="-5.7" y1="-11.6" x2="2.0301" y2="-11.6" width="0.2" layer="21"/>
<wire x1="2.0301" y1="-11.6" x2="2.1" y2="-11.6" width="0.2" layer="21"/>
<wire x1="2.1" y1="-11.6" x2="2.7" y2="-11.6" width="0.2" layer="21"/>
<wire x1="2.0301" y1="-6.2" x2="2.0301" y2="-11.6" width="0.2" layer="21"/>
<wire x1="-5.7" y1="11.6" x2="-5.7" y2="-11.6" width="0.2" layer="21"/>
<wire x1="2.0301" y1="11.6" x2="2.0301" y2="7.2" width="0.2" layer="21"/>
<wire x1="5.3" y1="7.2" x2="2.1" y2="7.2" width="0.2" layer="21"/>
<wire x1="2.1" y1="7.2" x2="2.0301" y2="7.2" width="0.2" layer="21"/>
<wire x1="5.7" y1="6.8" x2="5.3" y2="7.2" width="0.2" layer="21" curve="90"/>
<wire x1="5.7" y1="4.2" x2="5.7" y2="4.6" width="0.2" layer="21"/>
<wire x1="5.7" y1="4.6" x2="5.7" y2="6.4" width="0.2" layer="21"/>
<wire x1="5.7" y1="6.4" x2="5.7" y2="6.8" width="0.2" layer="21"/>
<wire x1="5.3" y1="3.8" x2="5.7" y2="4.2" width="0.2" layer="21" curve="90"/>
<wire x1="2.0301" y1="3.8" x2="2.1" y2="3.8" width="0.2" layer="21"/>
<wire x1="2.1" y1="3.8" x2="5.3" y2="3.8" width="0.2" layer="21"/>
<wire x1="2.0301" y1="3.8" x2="2.0301" y2="-2.8" width="0.2" layer="21"/>
<wire x1="1.2679" y1="10.72" x2="1.2679" y2="6.4" width="0.01" layer="51"/>
<wire x1="-1.4" y1="10.72" x2="-1.4" y2="10.02" width="0.01" layer="51"/>
<wire x1="-2.4" y1="10.02" x2="-1.4" y2="10.02" width="0.01" layer="51"/>
<wire x1="-2.4" y1="10.72" x2="-2.4" y2="10.02" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="10.72" x2="-2.4" y2="10.72" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="9.85" x2="-4.3679" y2="10.72" width="0.01" layer="51"/>
<wire x1="-4.5" y1="8.9967" x2="-4.5" y2="9.85" width="0.01" layer="51"/>
<wire x1="-4.5" y1="8.9967" x2="-4.5" y2="6.0033" width="0.01" layer="51" curve="59.8541"/>
<wire x1="-4.5" y1="5.3" x2="-4.5" y2="6.0033" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="4.7" x2="-4.3679" y2="5.3" width="0.01" layer="51"/>
<wire x1="-4.5" y1="3.9967" x2="-4.5" y2="4.7" width="0.01" layer="51"/>
<wire x1="-4.5" y1="3.9967" x2="-4.5" y2="1.0033" width="0.01" layer="51" curve="59.8541"/>
<wire x1="-4.5" y1="0.3" x2="-4.5" y2="1.0033" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="-0.3" x2="-4.3679" y2="0.3" width="0.01" layer="51"/>
<wire x1="-4.5" y1="-1.0033" x2="-4.5" y2="-0.3" width="0.01" layer="51"/>
<wire x1="-4.5" y1="-1.0033" x2="-4.5" y2="-3.9967" width="0.01" layer="51" curve="59.8541"/>
<wire x1="-4.5" y1="-4.7" x2="-4.5" y2="-3.9967" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="-5.3" x2="-4.3679" y2="-4.7" width="0.01" layer="51"/>
<wire x1="-4.5" y1="-6.0033" x2="-4.5" y2="-5.3" width="0.01" layer="51"/>
<wire x1="-4.5" y1="-6.0033" x2="-4.5" y2="-8.9967" width="0.01" layer="51" curve="59.8541"/>
<wire x1="-4.5" y1="-9.8" x2="-4.5" y2="-8.9967" width="0.01" layer="51"/>
<wire x1="-4.3679" y1="-10.72" x2="-4.3679" y2="-9.8" width="0.01" layer="51"/>
<wire x1="1.2679" y1="-5.4" x2="1.2679" y2="-10.72" width="0.01" layer="51"/>
<wire x1="5.7" y1="-3.2" x2="5.3" y2="-2.8" width="0.2" layer="21" curve="90"/>
<wire x1="5.7" y1="-5.8" x2="5.7" y2="-5.4" width="0.2" layer="21"/>
<wire x1="5.7" y1="-5.4" x2="5.7" y2="-3.6" width="0.2" layer="21"/>
<wire x1="5.7" y1="-3.6" x2="5.7" y2="-3.2" width="0.2" layer="21"/>
<wire x1="5.3" y1="-6.2" x2="5.7" y2="-5.8" width="0.2" layer="21" curve="90"/>
<wire x1="2.0301" y1="-6.2" x2="2.1" y2="-6.2" width="0.2" layer="21"/>
<wire x1="2.1" y1="-6.2" x2="5.3" y2="-6.2" width="0.2" layer="21"/>
<wire x1="5.3" y1="-2.8" x2="2.1" y2="-2.8" width="0.2" layer="21"/>
<wire x1="2.1" y1="-2.8" x2="2.0301" y2="-2.8" width="0.2" layer="21"/>
<wire x1="1.2679" y1="4.6" x2="1.2679" y2="-3.6" width="0.01" layer="51"/>
<wire x1="1" y1="-5.4" x2="1" y2="-10.72" width="0.01" layer="51"/>
<wire x1="1" y1="4.6" x2="1" y2="-3.6" width="0.01" layer="51"/>
<wire x1="2.1" y1="3.8" x2="2.1" y2="-2.8" width="0.2" layer="21"/>
<wire x1="1.8422" y1="6.4" x2="1.8422" y2="4.6" width="0.2" layer="21"/>
<wire x1="2.1" y1="11.6" x2="2.1" y2="7.2" width="0.2" layer="21"/>
<wire x1="2.1" y1="-6.2" x2="2.1" y2="-11.6" width="0.2" layer="21"/>
<wire x1="1.8422" y1="-3.6" x2="1.8422" y2="-5.4" width="0.2" layer="21"/>
<wire x1="5" y1="-5.4" x2="1" y2="-5.4" width="0.01" layer="51"/>
<wire x1="5" y1="6.4" x2="5" y2="4.6" width="0.2" layer="21"/>
<wire x1="2.0301" y1="7.2" x2="1.9125" y2="7.2" width="0.2" layer="21"/>
<wire x1="1.9125" y1="7.2" x2="1.9125" y2="6.4" width="0.2" layer="21"/>
<wire x1="1.9125" y1="4.6" x2="1.9125" y2="3.8" width="0.2" layer="21"/>
<wire x1="1.9125" y1="3.8" x2="2.0301" y2="3.8" width="0.2" layer="21"/>
<wire x1="5" y1="-3.6" x2="5" y2="-5.4" width="0.2" layer="21"/>
<wire x1="2.0301" y1="-2.8" x2="1.9125" y2="-2.8" width="0.2" layer="21"/>
<wire x1="1.9125" y1="-2.8" x2="1.9125" y2="-3.6" width="0.2" layer="21"/>
<wire x1="1.9125" y1="-5.4" x2="1.9125" y2="-6.2" width="0.2" layer="21"/>
<wire x1="1.9125" y1="-6.2" x2="2.0301" y2="-6.2" width="0.2" layer="21"/>
<wire x1="5" y1="-5.4" x2="5.7" y2="-5.4" width="0.2" layer="21"/>
<wire x1="1" y1="-3.6" x2="5" y2="-3.6" width="0.01" layer="51"/>
<wire x1="5.7" y1="-3.6" x2="5" y2="-3.6" width="0.2" layer="21"/>
<wire x1="1.2679" y1="-10.72" x2="-4.3679" y2="-10.72" width="0.01" layer="51"/>
<wire x1="-1.4" y1="10.72" x2="1.2679" y2="10.72" width="0.01" layer="51"/>
<wire x1="1" y1="6.4" x2="5" y2="6.4" width="0.01" layer="51"/>
<wire x1="5.7" y1="6.4" x2="5" y2="6.4" width="0.2" layer="21"/>
<wire x1="5" y1="4.6" x2="5.7" y2="4.6" width="0.2" layer="21"/>
<wire x1="5" y1="4.6" x2="1" y2="4.6" width="0.01" layer="51"/>
<wire x1="-3.5585" y1="7.5" x2="-0.2415" y2="7.5" width="0.01" layer="51"/>
<wire x1="-1.9" y1="5.8415" x2="-1.9" y2="9.1585" width="0.01" layer="51"/>
<wire x1="-3.5585" y1="-7.5" x2="-0.2415" y2="-7.5" width="0.01" layer="51"/>
<wire x1="-1.9" y1="-9.1585" x2="-1.9" y2="-5.8415" width="0.01" layer="51"/>
<wire x1="-3.5585" y1="-2.5" x2="-0.2415" y2="-2.5" width="0.01" layer="51"/>
<wire x1="-1.9" y1="-4.1585" x2="-1.9" y2="-0.8415" width="0.01" layer="51"/>
<wire x1="-3.5585" y1="2.5" x2="-0.2415" y2="2.5" width="0.01" layer="51"/>
<wire x1="-1.9" y1="0.8415" x2="-1.9" y2="4.1585" width="0.01" layer="51"/>
<circle x="-1.9" y="7.5" radius="1.4" width="0.01" layer="51"/>
<circle x="-1.9" y="-7.5" radius="1.4" width="0.01" layer="51"/>
<circle x="-1.9" y="-2.5" radius="1.4" width="0.01" layer="51"/>
<circle x="-1.9" y="2.5" radius="1.4" width="0.01" layer="51"/>
<pad name="L4" x="-1.9" y="7.5" drill="1.4" shape="long"/>
<pad name="L3" x="-1.9" y="2.5" drill="1.4" shape="long"/>
<pad name="L2" x="-1.9" y="-2.5" drill="1.4" shape="long"/>
<pad name="L1" x="-1.9" y="-7.5" drill="1.4" shape="long"/>
<text x="-5.2" y="12.5" size="2" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-8.41" y="-7.34" size="2" layer="51" font="vector" ratio="10" rot="R270">1</text>
<text x="-5.2" y="-14.4" size="2" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="P-733-342" urn="urn:adsk.eagle:package:9475023/2" type="model">
<packageinstances>
<packageinstance name="P-733-342"/>
</packageinstances>
</package3d>
<package3d name="P-733-338" urn="urn:adsk.eagle:package:10511812/2" type="model">
<packageinstances>
<packageinstance name="P-733-338"/>
</packageinstances>
</package3d>
<package3d name="P-721-134/001-000" urn="urn:adsk.eagle:package:11036717/2" type="model">
<packageinstances>
<packageinstance name="P-721-134/001-000"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="S-12-POL-S-1">
<wire x1="-6.35" y1="30.48" x2="3.81" y2="30.48" width="0.254" layer="97"/>
<wire x1="3.81" y1="30.48" x2="3.81" y2="25.4" width="0.254" layer="97"/>
<wire x1="3.81" y1="25.4" x2="3.81" y2="20.32" width="0.254" layer="97"/>
<wire x1="3.81" y1="20.32" x2="3.81" y2="15.24" width="0.254" layer="97"/>
<wire x1="3.81" y1="15.24" x2="3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="10.16" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="-10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="-15.24" width="0.254" layer="97"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="-20.32" width="0.254" layer="97"/>
<wire x1="3.81" y1="-20.32" x2="3.81" y2="-25.4" width="0.254" layer="97"/>
<wire x1="3.81" y1="-25.4" x2="3.81" y2="-30.48" width="0.254" layer="97"/>
<wire x1="3.81" y1="-30.48" x2="-6.35" y2="-30.48" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-30.48" x2="-6.35" y2="-25.4" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-25.4" x2="-6.35" y2="-20.32" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-20.32" x2="-6.35" y2="-15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-15.24" x2="-6.35" y2="-10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-10.16" x2="-6.35" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-5.08" x2="-6.35" y2="0" width="0.254" layer="97"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="15.24" x2="-6.35" y2="20.32" width="0.254" layer="97"/>
<wire x1="-6.35" y1="20.32" x2="-6.35" y2="25.4" width="0.254" layer="97"/>
<wire x1="-6.35" y1="25.4" x2="-6.35" y2="30.48" width="0.254" layer="97"/>
<wire x1="-6.35" y1="25.4" x2="3.81" y2="25.4" width="0.254" layer="97"/>
<wire x1="-6.35" y1="20.32" x2="3.81" y2="20.32" width="0.254" layer="97"/>
<wire x1="-6.35" y1="15.24" x2="3.81" y2="15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="10.16" x2="3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="5.08" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="0" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-5.08" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-10.16" x2="3.81" y2="-10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-15.24" x2="3.81" y2="-15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-20.32" x2="3.81" y2="-20.32" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-25.4" x2="3.81" y2="-25.4" width="0.254" layer="97"/>
<text x="-6.35" y="32.2" size="2" layer="95" font="vector" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-36.02" size="2" layer="96" font="vector" ratio="10">&gt;VALUE</text>
<pin name="P1" x="-2.54" y="-27.94" visible="pad" length="short" function="dot"/>
<pin name="P2" x="-2.54" y="-22.86" visible="pad" length="short" function="dot"/>
<pin name="P3" x="-2.54" y="-17.78" visible="pad" length="short" function="dot"/>
<pin name="P4" x="-2.54" y="-12.7" visible="pad" length="short" function="dot"/>
<pin name="P5" x="-2.54" y="-7.62" visible="pad" length="short" function="dot"/>
<pin name="P6" x="-2.54" y="-2.54" visible="pad" length="short" function="dot"/>
<pin name="P7" x="-2.54" y="2.54" visible="pad" length="short" function="dot"/>
<pin name="P8" x="-2.54" y="7.62" visible="pad" length="short" function="dot"/>
<pin name="P9" x="-2.54" y="12.7" visible="pad" length="short" function="dot"/>
<pin name="P10" x="-2.54" y="17.78" visible="pad" length="short" function="dot"/>
<pin name="P11" x="-2.54" y="22.86" visible="pad" length="short" function="dot"/>
<pin name="P12" x="-2.54" y="27.94" visible="pad" length="short" function="dot"/>
</symbol>
<symbol name="S-8-POL-S-2">
<wire x1="-6.35" y1="20.32" x2="3.81" y2="20.32" width="0.254" layer="97"/>
<wire x1="3.81" y1="20.32" x2="3.81" y2="15.24" width="0.254" layer="97"/>
<wire x1="3.81" y1="15.24" x2="3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="10.16" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="-10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="-15.24" width="0.254" layer="97"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="-20.32" width="0.254" layer="97"/>
<wire x1="3.81" y1="-20.32" x2="-6.35" y2="-20.32" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-20.32" x2="-6.35" y2="-15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-15.24" x2="-6.35" y2="-10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-10.16" x2="-6.35" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-5.08" x2="-6.35" y2="0" width="0.254" layer="97"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="15.24" x2="-6.35" y2="20.32" width="0.254" layer="97"/>
<wire x1="-6.35" y1="15.24" x2="3.81" y2="15.24" width="0.254" layer="97"/>
<wire x1="-6.35" y1="10.16" x2="3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="5.08" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="0" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-5.08" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-10.16" x2="3.81" y2="-10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-15.24" x2="3.81" y2="-15.24" width="0.254" layer="97"/>
<text x="-6.35" y="22.68" size="2" layer="95" font="vector" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-25.46" size="2" layer="96" font="vector" ratio="10">&gt;VALUE</text>
<pin name="P2" x="-2.54" y="-12.7" visible="pad" length="short" function="dot"/>
<pin name="P3" x="-2.54" y="-7.62" visible="pad" length="short" function="dot"/>
<pin name="P4" x="-2.54" y="-2.54" visible="pad" length="short" function="dot"/>
<pin name="P5" x="-2.54" y="2.54" visible="pad" length="short" function="dot"/>
<pin name="P6" x="-2.54" y="7.62" visible="pad" length="short" function="dot"/>
<pin name="P7" x="-2.54" y="12.7" visible="pad" length="short" function="dot"/>
<pin name="P8" x="-2.54" y="17.78" visible="pad" length="short" function="dot"/>
<pin name="P1" x="-2.54" y="-17.78" visible="pad" length="short" function="dot"/>
</symbol>
<symbol name="S-4-POL-S-1">
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="0" width="0.254" layer="97"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-5.08" x2="-6.35" y2="-10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-10.16" x2="3.81" y2="-10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="3.81" y1="0" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="10.16" x2="-6.35" y2="10.16" width="0.254" layer="97"/>
<wire x1="-6.35" y1="5.08" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="-6.35" y1="0" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="-6.35" y1="-5.08" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<text x="-6.15" y="12.14" size="2" layer="95" font="vector" ratio="10">&gt;NAME</text>
<text x="-6.15" y="-13.16" size="2" layer="96" font="vector" ratio="10">&gt;VALUE</text>
<pin name="P1" x="-2.54" y="-7.62" visible="pad" length="short" function="dot"/>
<pin name="P2" x="-2.54" y="-2.54" visible="pad" length="short" function="dot"/>
<pin name="P3" x="-2.54" y="2.54" visible="pad" length="short" function="dot"/>
<pin name="P4" x="-2.54" y="7.62" visible="pad" length="short" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="733-342" prefix="CN">
<description>&lt;b&gt;Serie 733,  Stiftleiste (für Leiterplatten) Einlötstift gerade 0,8 x 0,8 mm / Series 733,  Male header (for PCBs) Straight solder pin 0.8 x 0.8 mm&lt;/b&gt;&lt;br&gt;&lt;br&gt;Polzahl / Pole No.: 12 &lt;br&gt;Rastermaß / Pitch: 2.5 - 2.5  mm&lt;br&gt;Bemessungsspannung / Rated Voltage: 160 V&lt;br&gt;Nennstrom / Nominal Current: 6 A&lt;br&gt;Lötstiftlänge: / Solder Pin Length: 4.6 mm&lt;br&gt;Farbe / Color: lichtgrau / light gray&lt;br&gt;&lt;br&gt; Stand: 01.01.2014. Die jeweils aktuellen Daten zu diesem Artikel finden Sie in unserem Produktkatalog, den Sie unter &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt; einsehen können. &lt;br&gt; As of: 01/01/2014. Please find the newest data for this article in our product catalogue, which can be viewed under &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt;.&lt;br&gt;</description>
<gates>
<gate name="CN" symbol="S-12-POL-S-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P-733-342">
<connects>
<connect gate="CN" pin="P1" pad="L1"/>
<connect gate="CN" pin="P10" pad="L10"/>
<connect gate="CN" pin="P11" pad="L11"/>
<connect gate="CN" pin="P12" pad="L12"/>
<connect gate="CN" pin="P2" pad="L2"/>
<connect gate="CN" pin="P3" pad="L3"/>
<connect gate="CN" pin="P4" pad="L4"/>
<connect gate="CN" pin="P5" pad="L5"/>
<connect gate="CN" pin="P6" pad="L6"/>
<connect gate="CN" pin="P7" pad="L7"/>
<connect gate="CN" pin="P8" pad="L8"/>
<connect gate="CN" pin="P9" pad="L9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9475023/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Series 733, Male header (for PCBs) Straight solder pin 0.8 x 0.8 mm"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-342"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="733-338" prefix="CN">
<description>&lt;b&gt;Serie 733,  Stiftleiste (für Leiterplatten) Einlötstift gerade 0,8 x 0,8 mm / Series 733,  Male header (for PCBs) Straight solder pin 0.8 x 0.8 mm&lt;/b&gt;&lt;br&gt;&lt;br&gt;Polzahl / Pole No.: 8 &lt;br&gt;Rastermaß / Pitch: 2.5 - 2.5  mm&lt;br&gt;Bemessungsspannung / Rated Voltage: 160 V&lt;br&gt;Nennstrom / Nominal Current: 6 A&lt;br&gt;Lötstiftlänge: / Solder Pin Length: 4.6 mm&lt;br&gt;Farbe / Color: lichtgrau / light gray&lt;br&gt;&lt;br&gt; Stand: 01.01.2014. Die jeweils aktuellen Daten zu diesem Artikel finden Sie in unserem Produktkatalog, den Sie unter &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt; einsehen können. &lt;br&gt; As of: 01/01/2014. Please find the newest data for this article in our product catalogue, which can be viewed under &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt;.&lt;br&gt;</description>
<gates>
<gate name="CN" symbol="S-8-POL-S-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P-733-338">
<connects>
<connect gate="CN" pin="P1" pad="L1"/>
<connect gate="CN" pin="P2" pad="L2"/>
<connect gate="CN" pin="P3" pad="L3"/>
<connect gate="CN" pin="P4" pad="L4"/>
<connect gate="CN" pin="P5" pad="L5"/>
<connect gate="CN" pin="P6" pad="L6"/>
<connect gate="CN" pin="P7" pad="L7"/>
<connect gate="CN" pin="P8" pad="L8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10511812/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Series 733, Male header (for PCBs) Straight solder pin 0.8 x 0.8 mm "/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-338"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="721-134/001-000" prefix="CN">
<description>&lt;b&gt;Serie 721,  Stiftleiste mit geraden Einlötstiften / Series 721,  Male connector with straight solder pins&lt;/b&gt;&lt;br&gt;&lt;br&gt;Polzahl / Pole No.: 4 &lt;br&gt;Rastermaß / Pitch: 5 - 5  mm&lt;br&gt;Bemessungsspannung / Rated Voltage: 320 V&lt;br&gt;Nennstrom / Nominal Current: 12 A&lt;br&gt;Lötstiftlänge: / Solder Pin Length: 5 mm&lt;br&gt;Farbe / Color: lichtgrau / light gray&lt;br&gt;&lt;br&gt; Stand: 01.01.2014. Die jeweils aktuellen Daten zu diesem Artikel finden Sie in unserem Produktkatalog, den Sie unter &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt; einsehen können. &lt;br&gt; As of: 01/01/2014. Please find the newest data for this article in our product catalogue, which can be viewed under &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt;.&lt;br&gt;</description>
<gates>
<gate name="CN" symbol="S-4-POL-S-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P-721-134/001-000">
<connects>
<connect gate="CN" pin="P1" pad="L1"/>
<connect gate="CN" pin="P2" pad="L2"/>
<connect gate="CN" pin="P3" pad="L3"/>
<connect gate="CN" pin="P4" pad="L4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11036717/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Series 721, Male connector with straight solder pins"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="721-134/001-000"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-dummy">
<packages>
<package name="EXTRA-BOM-LINE">
<text x="0" y="0" size="1.778" layer="48">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="48">&gt;VALUE</text>
<circle x="-2.54" y="0" radius="2.300065625" width="0.127" layer="48"/>
<wire x1="-4.064" y1="0" x2="-1.016" y2="0" width="0.127" layer="48"/>
<wire x1="-2.54" y1="1.524" x2="-2.54" y2="-1.27" width="0.127" layer="48"/>
</package>
</packages>
<symbols>
<symbol name="BOM">
<text x="2.54" y="2.54" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="2.54" y="0" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<circle x="0" y="0" radius="2.0478125" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="-2.54" size="1.778" layer="97" align="center-left">&gt;DESCRIPTION</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BOM" prefix="X" uservalue="yes">
<description>Dummy symbol for inserting extra BOM lines.</description>
<gates>
<gate name="X" symbol="BOM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EXTRA-BOM-LINE">
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DOCFIELD" urn="urn:adsk.eagle:symbol:13864/1" library_version="1">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
<symbol name="DINA3_P" urn="urn:adsk.eagle:symbol:13865/1" library_version="1">
<frame x1="0" y1="0" x2="264.16" y2="388.62" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA3_P" urn="urn:adsk.eagle:component:13922/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, portrait</description>
<gates>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="always"/>
<gate name="G$1" symbol="DINA3_P" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-idc">
<packages>
<package name="3M_20" urn="urn:adsk.eagle:footprint:6230384/1" locally_modified="yes">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<wire x1="22.225" y1="-4.2418" x2="22.225" y2="4.3" width="0.3048" layer="21"/>
<wire x1="-22.225" y1="4.3" x2="-22.225" y2="-4.2418" width="0.3048" layer="21"/>
<wire x1="-22.225" y1="-4.3" x2="-2.54" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-4.3" x2="-2.54" y2="-3.048" width="0.3048" layer="21"/>
<wire x1="2.54" y1="-3.048" x2="2.54" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="2.54" y1="-4.3" x2="21.971" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="22.225" y1="4.3" x2="-22.225" y2="4.3" width="0.3048" layer="21"/>
<wire x1="15.24" y1="-3" x2="2.54" y2="-3" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-3" x2="-15.24" y2="-3" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="-3" x2="-15.24" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="15.113" y1="3" x2="-15.24" y2="3" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="1.27" x2="-15.24" y2="3" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="-1.27" x2="-22.098" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="1.27" x2="-22.098" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-4.318" x2="2.54" y2="-4.318" width="0.3048" layer="21"/>
<wire x1="15.24" y1="-3" x2="15.24" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="15.24" y1="1.27" x2="15.24" y2="3" width="0.3048" layer="21"/>
<wire x1="15.24" y1="-1.27" x2="22.098" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="15.24" y1="1.27" x2="22.098" y2="1.27" width="0.3048" layer="21"/>
<pad name="1" x="-11.43" y="-1.27" drill="1" shape="square"/>
<pad name="2" x="-11.43" y="1.27" drill="1" shape="square"/>
<pad name="3" x="-8.89" y="-1.27" drill="1" shape="square"/>
<pad name="4" x="-8.89" y="1.27" drill="1" shape="square"/>
<pad name="5" x="-6.35" y="-1.27" drill="1" shape="square"/>
<pad name="6" x="-6.35" y="1.27" drill="1" shape="square"/>
<pad name="8" x="-3.81" y="1.27" drill="1" shape="square"/>
<pad name="9" x="-1.27" y="-1.27" drill="1" shape="square"/>
<pad name="10" x="-1.27" y="1.27" drill="1" shape="square"/>
<pad name="11" x="1.27" y="-1.27" drill="1" shape="square"/>
<pad name="12" x="1.27" y="1.27" drill="1" shape="square"/>
<pad name="13" x="3.81" y="-1.27" drill="1" shape="square"/>
<pad name="14" x="3.81" y="1.27" drill="1" shape="square"/>
<pad name="15" x="6.35" y="-1.27" drill="1" shape="square"/>
<pad name="16" x="6.35" y="1.27" drill="1" shape="square"/>
<pad name="17" x="8.89" y="-1.27" drill="1" shape="square"/>
<pad name="18" x="8.89" y="1.27" drill="1" shape="square"/>
<pad name="19" x="11.43" y="-1.27" drill="1" shape="square"/>
<pad name="20" x="11.43" y="1.27" drill="1" shape="square"/>
<pad name="7" x="-3.81" y="-1.27" drill="1" shape="square"/>
<text x="-8.89" y="-7.62" size="2.54" layer="25">&gt;NAME</text>
<text x="8.89" y="-7.62" size="2.54" layer="27">&gt;VALUE</text>
</package>
<package name="3M_20L" urn="urn:adsk.eagle:footprint:6230383/1" locally_modified="yes">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<wire x1="-22.225" y1="-6.0198" x2="-19.685" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="22.225" y1="-6.0198" x2="22.225" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-22.225" y1="2.54" x2="-22.225" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-19.685" y1="-6.0198" x2="-19.685" y2="-2.032" width="0.3048" layer="21"/>
<wire x1="-19.685" y1="-6.0198" x2="-15.0114" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-19.685" y1="-2.032" x2="-17.3482" y2="-0.4572" width="0.3048" layer="21"/>
<wire x1="-17.3482" y1="-0.4572" x2="-15.0114" y2="-2.032" width="0.3048" layer="21"/>
<wire x1="-15.0114" y1="-2.032" x2="-15.0114" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-15.0114" y1="-6.0198" x2="15.0114" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="15.0114" y1="-6.0198" x2="15.0114" y2="-2.0574" width="0.3048" layer="21"/>
<wire x1="15.0114" y1="-2.0574" x2="17.3482" y2="-0.4572" width="0.3048" layer="21"/>
<wire x1="17.3482" y1="-0.4572" x2="19.685" y2="-2.0574" width="0.3048" layer="21"/>
<wire x1="19.685" y1="-2.0574" x2="19.685" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="15.0114" y1="-6.0198" x2="19.685" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="19.685" y1="-6.0198" x2="22.225" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-22.225" y1="2.54" x2="-19.3548" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="22.225" y1="2.54" x2="19.3548" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="10.9982" x2="-19.3548" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="8.89" x2="-15.24" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="6.6802" width="0.3048" layer="21"/>
<wire x1="-3.81" y1="7.874" x2="-2.54" y2="6.604" width="0.3048" layer="21" curve="-90"/>
<wire x1="-15.24" y1="8.89" x2="-14.224" y2="7.874" width="0.3048" layer="21" curve="90"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="6.6802" width="0.3048" layer="21"/>
<wire x1="2.54" y1="6.604" x2="3.81" y2="7.874" width="0.3048" layer="21" curve="-90"/>
<wire x1="-15.24" y1="1.27" x2="-15.24" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="5.08" x2="-15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.27" x2="-12.7" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.27" x2="-7.62" y2="1.27" width="0.1524" layer="21"/>
<wire x1="15.24" y1="10.9982" x2="19.3548" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-14.224" y1="7.874" x2="14.224" y2="7.874" width="0.3048" layer="21"/>
<wire x1="15.24" y1="8.89" x2="15.24" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="14.224" y1="7.874" x2="15.24" y2="8.89" width="0.3048" layer="21" curve="90"/>
<wire x1="0" y1="1.27" x2="2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.27" x2="15.24" y2="3.81" width="0.1524" layer="21"/>
<wire x1="15.24" y1="5.08" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.27" x2="12.7" y2="1.27" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.27" x2="7.62" y2="1.27" width="0.1524" layer="21"/>
<circle x="-18.0848" y="3.81" radius="0.9398" width="0.1524" layer="21"/>
<circle x="18.0848" y="3.81" radius="0.9398" width="0.1524" layer="21"/>
<pad name="1" x="-11.43" y="-5.08" drill="1" shape="square"/>
<pad name="2" x="-11.43" y="-2.54" drill="1" shape="square"/>
<pad name="3" x="-8.89" y="-5.08" drill="1" shape="square"/>
<pad name="4" x="-8.89" y="-2.54" drill="1" shape="square"/>
<pad name="5" x="-6.35" y="-5.08" drill="1" shape="square"/>
<pad name="6" x="-6.35" y="-2.54" drill="1" shape="square"/>
<pad name="8" x="-3.81" y="-2.54" drill="1" shape="square"/>
<pad name="9" x="-1.27" y="-5.08" drill="1" shape="square"/>
<pad name="10" x="-1.27" y="-2.54" drill="1" shape="square"/>
<pad name="11" x="1.27" y="-5.08" drill="1" shape="square"/>
<pad name="12" x="1.27" y="-2.54" drill="1" shape="square"/>
<pad name="13" x="3.81" y="-5.08" drill="1" shape="square"/>
<pad name="14" x="3.81" y="-2.54" drill="1" shape="square"/>
<pad name="15" x="6.35" y="-5.08" drill="1" shape="square"/>
<pad name="16" x="6.35" y="-2.54" drill="1" shape="square"/>
<pad name="17" x="8.89" y="-5.08" drill="1" shape="square"/>
<pad name="18" x="8.89" y="-2.54" drill="1" shape="square"/>
<pad name="19" x="11.43" y="-5.08" drill="1" shape="square"/>
<pad name="20" x="11.43" y="-2.54" drill="1" shape="square"/>
<pad name="7" x="-3.81" y="-5.08" drill="1" shape="square"/>
<text x="-21.59" y="-10.16" size="2.54" layer="25">&gt;NAME</text>
<text x="7.62" y="-10.16" size="2.54" layer="27">&gt;VALUE</text>
<polygon width="0.3048" layer="21">
<vertex x="-12.7" y="5.35"/>
<vertex x="-10.16" y="5.35"/>
<vertex x="-11.43" y="2.81"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="3M_20" urn="urn:adsk.eagle:package:6230386/2" locally_modified="yes" type="model">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="3M_20"/>
</packageinstances>
</package3d>
<package3d name="3M_20L" urn="urn:adsk.eagle:package:6230385/2" locally_modified="yes" type="model">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="3M_20L"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="20P">
<wire x1="3.81" y1="-15.24" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-1.27" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-1.27" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-12.7" x2="-1.27" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-1.27" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="10.16" x2="-1.27" y2="10.16" width="0.6096" layer="94"/>
<text x="-3.81" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="13" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="15" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="17" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="19" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="12" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="14" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="16" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="18" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="20" x="-7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AWP-20" prefix="CN">
<gates>
<gate name="CN" symbol="20P" x="0" y="0"/>
</gates>
<devices>
<device name="P" package="3M_20">
<connects>
<connect gate="CN" pin="1" pad="1"/>
<connect gate="CN" pin="10" pad="10"/>
<connect gate="CN" pin="11" pad="11"/>
<connect gate="CN" pin="12" pad="12"/>
<connect gate="CN" pin="13" pad="13"/>
<connect gate="CN" pin="14" pad="14"/>
<connect gate="CN" pin="15" pad="15"/>
<connect gate="CN" pin="16" pad="16"/>
<connect gate="CN" pin="17" pad="17"/>
<connect gate="CN" pin="18" pad="18"/>
<connect gate="CN" pin="19" pad="19"/>
<connect gate="CN" pin="2" pad="2"/>
<connect gate="CN" pin="20" pad="20"/>
<connect gate="CN" pin="3" pad="3"/>
<connect gate="CN" pin="4" pad="4"/>
<connect gate="CN" pin="5" pad="5"/>
<connect gate="CN" pin="6" pad="6"/>
<connect gate="CN" pin="7" pad="7"/>
<connect gate="CN" pin="8" pad="8"/>
<connect gate="CN" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6230386/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="IDC socket, 2x10, straight, with latch" constant="no"/>
<attribute name="MF" value="CONNFLY" constant="no"/>
<attribute name="MPN" value="DS1011-20SBSIB7" constant="no"/>
</technology>
</technologies>
</device>
<device name="K" package="3M_20L">
<connects>
<connect gate="CN" pin="1" pad="1"/>
<connect gate="CN" pin="10" pad="10"/>
<connect gate="CN" pin="11" pad="11"/>
<connect gate="CN" pin="12" pad="12"/>
<connect gate="CN" pin="13" pad="13"/>
<connect gate="CN" pin="14" pad="14"/>
<connect gate="CN" pin="15" pad="15"/>
<connect gate="CN" pin="16" pad="16"/>
<connect gate="CN" pin="17" pad="17"/>
<connect gate="CN" pin="18" pad="18"/>
<connect gate="CN" pin="19" pad="19"/>
<connect gate="CN" pin="2" pad="2"/>
<connect gate="CN" pin="20" pad="20"/>
<connect gate="CN" pin="3" pad="3"/>
<connect gate="CN" pin="4" pad="4"/>
<connect gate="CN" pin="5" pad="5"/>
<connect gate="CN" pin="6" pad="6"/>
<connect gate="CN" pin="7" pad="7"/>
<connect gate="CN" pin="8" pad="8"/>
<connect gate="CN" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6230385/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="IDC socket, 2x10, right angle, with latch" constant="no"/>
<attribute name="MF" value="CONNFLY" constant="no"/>
<attribute name="MPN" value="DS1011-20RBSIB7" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="blebox-dummy">
<packages>
<package name="EXTRA-BOM-LINE">
<text x="0" y="0" size="1.778" layer="48">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="48">&gt;VALUE</text>
<circle x="-2.54" y="0" radius="2.300065625" width="0.127" layer="48"/>
<wire x1="-4.064" y1="0" x2="-1.016" y2="0" width="0.127" layer="48"/>
<wire x1="-2.54" y1="1.524" x2="-2.54" y2="-1.27" width="0.127" layer="48"/>
</package>
</packages>
<symbols>
<symbol name="BOM">
<text x="2.54" y="2.54" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="2.54" y="0" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<circle x="0" y="0" radius="2.0478125" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="-2.54" size="1.778" layer="97" align="center-left">&gt;DESCRIPTION</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BOM" prefix="X" uservalue="yes">
<description>Dummy symbol for inserting extra BOM lines.</description>
<gates>
<gate name="X" symbol="BOM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EXTRA-BOM-LINE">
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-wago-terminal-blocks">
<packages>
<package name="P-2060-452/998-404" urn="urn:adsk.eagle:footprint:8647138/1" locally_modified="yes">
<wire x1="6.6127" y1="3.125" x2="5.125" y2="3.125" width="0.01" layer="51"/>
<wire x1="5.125" y1="0.875" x2="6.6127" y2="0.875" width="0.01" layer="51"/>
<wire x1="6.6127" y1="-0.875" x2="5.125" y2="-0.875" width="0.01" layer="51"/>
<wire x1="5.125" y1="-3.125" x2="6.6127" y2="-3.125" width="0.01" layer="51"/>
<wire x1="5.525" y1="1.3" x2="5.525" y2="0.3757" width="0.01" layer="51"/>
<wire x1="5.525" y1="0.3757" x2="5.525" y2="-0.3757" width="0.01" layer="51"/>
<wire x1="5.525" y1="-0.3757" x2="5.525" y2="-1.3" width="0.01" layer="51"/>
<wire x1="5.525" y1="3.95" x2="5.525" y2="2.7" width="0.01" layer="51"/>
<wire x1="7.1072" y1="0.3757" x2="5.525" y2="0.3757" width="0.01" layer="51"/>
<wire x1="7.1072" y1="-0.3757" x2="5.525" y2="-0.3757" width="0.01" layer="51"/>
<wire x1="5.525" y1="-2.7" x2="5.525" y2="-3.95" width="0.01" layer="51"/>
<wire x1="5.125" y1="0.875" x2="5.125" y2="3.125" width="0.01" layer="51"/>
<wire x1="5.125" y1="-3.125" x2="5.125" y2="-0.875" width="0.01" layer="51"/>
<wire x1="1.025" y1="3.3" x2="0.9405" y2="3.4633" width="0.01" layer="51" curve="54.7314"/>
<wire x1="0.9405" y1="3.4633" x2="0.825" y2="3.5" width="0.01" layer="51" curve="35.2729"/>
<wire x1="0.825" y1="0.5" x2="0.9405" y2="0.5367" width="0.01" layer="51" curve="35.2729"/>
<wire x1="0.9405" y1="0.5367" x2="1.025" y2="0.7" width="0.01" layer="51" curve="54.7314"/>
<wire x1="1.025" y1="-0.7" x2="0.9405" y2="-0.5367" width="0.01" layer="51" curve="54.7314"/>
<wire x1="0.9405" y1="-0.5367" x2="0.825" y2="-0.5" width="0.01" layer="51" curve="35.2729"/>
<wire x1="0.825" y1="-3.5" x2="0.9405" y2="-3.4633" width="0.01" layer="51" curve="35.2729"/>
<wire x1="0.9405" y1="-3.4633" x2="1.025" y2="-3.3" width="0.01" layer="51" curve="54.7314"/>
<wire x1="0.1834" y1="-3.1" x2="0.1834" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-0.1111" y1="-3.1" x2="-0.1111" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-1.7589" y1="-3.1" x2="-1.7589" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-2.2097" y1="-3.1" x2="-2.2097" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-2.3528" y1="-3.1" x2="-2.3528" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-2.3816" y1="-3.1" x2="-2.3816" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-2.5918" y1="-3.1" x2="-2.5918" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-3.5904" y1="-3.1" x2="-3.5904" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-4.775" y1="-0.9" x2="-4.775" y2="-3.1" width="0.01" layer="51"/>
<wire x1="0.1834" y1="0.9" x2="0.1834" y2="3.1" width="0.01" layer="51"/>
<wire x1="-0.1111" y1="0.9" x2="-0.1111" y2="3.1" width="0.01" layer="51"/>
<wire x1="-1.7589" y1="0.9" x2="-1.7589" y2="3.1" width="0.01" layer="51"/>
<wire x1="-2.2097" y1="0.9" x2="-2.2097" y2="3.1" width="0.01" layer="51"/>
<wire x1="-2.3528" y1="0.9" x2="-2.3528" y2="3.1" width="0.01" layer="51"/>
<wire x1="-2.3816" y1="0.9" x2="-2.3816" y2="3.1" width="0.01" layer="51"/>
<wire x1="-2.5918" y1="0.9" x2="-2.5918" y2="3.1" width="0.01" layer="51"/>
<wire x1="-3.5904" y1="0.9" x2="-3.5904" y2="3.1" width="0.01" layer="51"/>
<wire x1="-4.775" y1="3.1" x2="-4.775" y2="0.9" width="0.01" layer="51"/>
<wire x1="-5.575" y1="-1.3" x2="7.125" y2="-1.3" width="0.01" layer="51"/>
<wire x1="7.125" y1="-2.7" x2="-5.575" y2="-2.7" width="0.01" layer="51"/>
<wire x1="2.2083" y1="-2.6" x2="3.5744" y2="-2.6" width="0.01" layer="51"/>
<wire x1="3.5744" y1="-2.6" x2="3.6786" y2="-2.6" width="0.01" layer="51"/>
<wire x1="3.6786" y1="-2.6" x2="6.975" y2="-2.6" width="0.01" layer="51"/>
<wire x1="6.975" y1="-2.6" x2="7.125" y2="-2.6" width="0.01" layer="51"/>
<wire x1="6.975" y1="-2.6" x2="6.975" y2="-1.4" width="0.01" layer="51"/>
<wire x1="3.6786" y1="-2.6" x2="3.6786" y2="-1.4" width="0.01" layer="51"/>
<wire x1="3.5744" y1="-2.6" x2="3.5744" y2="-1.4" width="0.01" layer="51"/>
<wire x1="2.2083" y1="-1.4" x2="2.2083" y2="-2.6" width="0.01" layer="51"/>
<wire x1="-3.7141" y1="-2.6" x2="-3.7141" y2="-1.4" width="0.01" layer="51"/>
<wire x1="-3.7469" y1="-2.6" x2="-3.7469" y2="-1.4" width="0.01" layer="51"/>
<wire x1="-5.825" y1="-2.6" x2="-5.825" y2="-1.4" width="0.2" layer="51"/>
<wire x1="-4.275" y1="-2.6" x2="-4.275" y2="-1.4" width="0.01" layer="51"/>
<wire x1="7.125" y1="-1.4" x2="2.2083" y2="-1.4" width="0.01" layer="51"/>
<wire x1="-5.575" y1="-1.4" x2="-3.7141" y2="-1.4" width="0.01" layer="51"/>
<wire x1="-5.575" y1="-2.6" x2="-3.7141" y2="-2.6" width="0.01" layer="51"/>
<wire x1="-5.575" y1="2.7" x2="7.125" y2="2.7" width="0.01" layer="51"/>
<wire x1="7.125" y1="1.3" x2="-5.575" y2="1.3" width="0.01" layer="51"/>
<wire x1="2.2083" y1="1.4" x2="3.5744" y2="1.4" width="0.01" layer="51"/>
<wire x1="3.5744" y1="1.4" x2="3.6786" y2="1.4" width="0.01" layer="51"/>
<wire x1="3.6786" y1="1.4" x2="6.975" y2="1.4" width="0.01" layer="51"/>
<wire x1="6.975" y1="1.4" x2="7.125" y2="1.4" width="0.01" layer="51"/>
<wire x1="6.975" y1="1.4" x2="6.975" y2="2.6" width="0.01" layer="51"/>
<wire x1="3.6786" y1="1.4" x2="3.6786" y2="2.6" width="0.01" layer="51"/>
<wire x1="3.5744" y1="1.4" x2="3.5744" y2="2.6" width="0.01" layer="51"/>
<wire x1="2.2083" y1="2.6" x2="2.2083" y2="1.4" width="0.01" layer="51"/>
<wire x1="-3.7141" y1="1.4" x2="-3.7141" y2="2.6" width="0.01" layer="51"/>
<wire x1="-3.7469" y1="1.4" x2="-3.7469" y2="2.6" width="0.01" layer="51"/>
<wire x1="-5.825" y1="1.4" x2="-5.825" y2="2.6" width="0.2" layer="51"/>
<wire x1="-4.275" y1="1.4" x2="-4.275" y2="2.6" width="0.01" layer="51"/>
<wire x1="7.125" y1="2.6" x2="2.2083" y2="2.6" width="0.01" layer="51"/>
<wire x1="-5.575" y1="2.6" x2="-3.7141" y2="2.6" width="0.01" layer="51"/>
<wire x1="-5.575" y1="1.4" x2="-3.7141" y2="1.4" width="0.01" layer="51"/>
<wire x1="5.6745" y1="3.95" x2="5.6745" y2="-3.95" width="0.01" layer="51"/>
<wire x1="0.7673" y1="3.95" x2="0.7673" y2="3.5" width="0.01" layer="51"/>
<wire x1="0.6518" y1="3.95" x2="0.6518" y2="3.5" width="0.01" layer="51"/>
<wire x1="0.2188" y1="3.95" x2="0.2188" y2="3.5" width="0.01" layer="51"/>
<wire x1="7.0245" y1="3.7447" x2="7.0245" y2="2.87" width="0.01" layer="51"/>
<wire x1="7.0245" y1="1.13" x2="7.0245" y2="-1.13" width="0.2" layer="51"/>
<wire x1="7.125" y1="3.65" x2="7.125" y2="0.5339" width="0.2" layer="51"/>
<wire x1="7.0245" y1="-2.87" x2="7.0245" y2="-3.7447" width="0.01" layer="51"/>
<wire x1="0.7673" y1="-3.5" x2="0.7673" y2="-3.95" width="0.01" layer="51"/>
<wire x1="0.9405" y1="3.95" x2="0.9405" y2="3.4633" width="0.01" layer="51"/>
<wire x1="5.025" y1="3.95" x2="5.025" y2="-3.95" width="0.01" layer="51"/>
<wire x1="-5.575" y1="-3.95" x2="6.8004" y2="-3.95" width="0.2" layer="21"/>
<wire x1="0.9405" y1="-3.4633" x2="0.9405" y2="-3.95" width="0.01" layer="51"/>
<wire x1="1.025" y1="3.3" x2="0.9405" y2="3.4633" width="0.01" layer="51" curve="54.7314"/>
<wire x1="0.9405" y1="0.5367" x2="1.025" y2="0.7" width="0.01" layer="51" curve="54.7314"/>
<wire x1="1.025" y1="3.3" x2="1.025" y2="0.7" width="0.01" layer="51"/>
<wire x1="0.9405" y1="0.5367" x2="0.9405" y2="-0.5367" width="0.01" layer="51"/>
<wire x1="0.7673" y1="0.5" x2="0.7673" y2="-0.5" width="0.01" layer="51"/>
<wire x1="0.6518" y1="-3.5" x2="0.6518" y2="-3.95" width="0.01" layer="51"/>
<wire x1="0.2188" y1="-3.5" x2="0.2188" y2="-3.95" width="0.01" layer="51"/>
<wire x1="0.6518" y1="0.5" x2="0.6518" y2="-0.5" width="0.01" layer="51"/>
<wire x1="0.2188" y1="0.5" x2="0.2188" y2="-0.5" width="0.01" layer="51"/>
<wire x1="0.9405" y1="-3.4633" x2="1.025" y2="-3.3" width="0.01" layer="51" curve="54.7314"/>
<wire x1="1.025" y1="-0.7" x2="1.025" y2="-3.3" width="0.01" layer="51"/>
<wire x1="1.025" y1="-0.7" x2="0.9405" y2="-0.5367" width="0.01" layer="51" curve="54.7314"/>
<wire x1="6.8004" y1="3.95" x2="-5.575" y2="3.95" width="0.2" layer="21"/>
<wire x1="-3.575" y1="3.95" x2="-3.575" y2="3.5" width="0.01" layer="51"/>
<wire x1="0.625" y1="3.5" x2="0.625" y2="0.5" width="0.01" layer="51"/>
<wire x1="-3.575" y1="-3.5" x2="-3.575" y2="-3.95" width="0.01" layer="51"/>
<wire x1="-3.575" y1="0.5" x2="-3.575" y2="-0.5" width="0.01" layer="51"/>
<wire x1="0.625" y1="-0.5" x2="0.625" y2="-3.5" width="0.01" layer="51"/>
<wire x1="-5.575" y1="3.5" x2="-4.775" y2="3.5" width="0.01" layer="51"/>
<wire x1="-4.775" y1="3.5" x2="-3.5904" y2="3.5" width="0.01" layer="51"/>
<wire x1="-3.5904" y1="3.5" x2="-0.875" y2="3.5" width="0.01" layer="51"/>
<wire x1="-0.875" y1="3.5" x2="0.825" y2="3.5" width="0.01" layer="51"/>
<wire x1="-3.5904" y1="3.5" x2="-3.5904" y2="3.25" width="0.01" layer="51"/>
<wire x1="-0.875" y1="3.5" x2="-0.875" y2="3.25" width="0.01" layer="51"/>
<wire x1="-4.775" y1="3.25" x2="-0.875" y2="3.25" width="0.01" layer="51"/>
<wire x1="-4.775" y1="3.5" x2="-4.775" y2="3.25" width="0.01" layer="51"/>
<wire x1="-5.575" y1="-0.5" x2="-4.775" y2="-0.5" width="0.01" layer="51"/>
<wire x1="-4.775" y1="-0.5" x2="-3.5904" y2="-0.5" width="0.01" layer="51"/>
<wire x1="-3.5904" y1="-0.5" x2="-0.875" y2="-0.5" width="0.01" layer="51"/>
<wire x1="-0.875" y1="-0.5" x2="0.825" y2="-0.5" width="0.01" layer="51"/>
<wire x1="-3.5904" y1="-0.5" x2="-3.5904" y2="-0.75" width="0.01" layer="51"/>
<wire x1="-0.875" y1="-0.5" x2="-0.875" y2="-0.75" width="0.01" layer="51"/>
<wire x1="-4.775" y1="-0.75" x2="-0.875" y2="-0.75" width="0.01" layer="51"/>
<wire x1="-4.775" y1="-0.5" x2="-4.775" y2="-0.75" width="0.01" layer="51"/>
<wire x1="0.825" y1="0.5" x2="-0.875" y2="0.5" width="0.01" layer="51"/>
<wire x1="-0.875" y1="0.5" x2="-3.5904" y2="0.5" width="0.01" layer="51"/>
<wire x1="-3.5904" y1="0.5" x2="-4.775" y2="0.5" width="0.01" layer="51"/>
<wire x1="-4.775" y1="0.5" x2="-5.575" y2="0.5" width="0.01" layer="51"/>
<wire x1="-3.5904" y1="0.5" x2="-3.5904" y2="0.75" width="0.01" layer="51"/>
<wire x1="-4.775" y1="0.5" x2="-4.775" y2="0.75" width="0.01" layer="51"/>
<wire x1="-0.875" y1="0.75" x2="-4.775" y2="0.75" width="0.01" layer="51"/>
<wire x1="-0.875" y1="0.5" x2="-0.875" y2="0.75" width="0.01" layer="51"/>
<wire x1="0.825" y1="-3.5" x2="-0.875" y2="-3.5" width="0.01" layer="51"/>
<wire x1="-0.875" y1="-3.5" x2="-3.5904" y2="-3.5" width="0.01" layer="51"/>
<wire x1="-3.5904" y1="-3.5" x2="-4.775" y2="-3.5" width="0.01" layer="51"/>
<wire x1="-4.775" y1="-3.5" x2="-5.575" y2="-3.5" width="0.01" layer="51"/>
<wire x1="-3.5904" y1="-3.5" x2="-3.5904" y2="-3.25" width="0.01" layer="51"/>
<wire x1="-4.775" y1="-3.5" x2="-4.775" y2="-3.25" width="0.01" layer="51"/>
<wire x1="-0.875" y1="-3.25" x2="-4.775" y2="-3.25" width="0.01" layer="51"/>
<wire x1="-0.875" y1="-3.5" x2="-0.875" y2="-3.25" width="0.01" layer="51"/>
<wire x1="-5.475" y1="-0.9" x2="-5.475" y2="-3.1" width="0.2" layer="51"/>
<wire x1="-5.275" y1="-3.1" x2="-5.275" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-3.6337" y1="-3.1" x2="-3.6337" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-3.599" y1="-3.1" x2="-3.599" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-0.873" y1="-3.1" x2="-0.873" y2="-2.4701" width="0.01" layer="51"/>
<wire x1="-0.3737" y1="-3.1" x2="-0.3737" y2="-2.4686" width="0.01" layer="51"/>
<wire x1="-0.2959" y1="-3.1" x2="-0.2959" y2="-2.634" width="0.01" layer="51"/>
<wire x1="0.2124" y1="-3.1" x2="0.2124" y2="-0.9" width="0.01" layer="51"/>
<wire x1="0.4331" y1="-3.1" x2="0.4226" y2="-3.1" width="0.01" layer="51"/>
<wire x1="0.4226" y1="-3.1" x2="-0.9924" y2="-3.1" width="0.01" layer="51"/>
<wire x1="-0.9924" y1="-3.1" x2="-1.5401" y2="-3.1" width="0.01" layer="51"/>
<wire x1="-1.5401" y1="-3.1" x2="-5.475" y2="-3.1" width="0.01" layer="51"/>
<wire x1="0.4226" y1="-3.1" x2="0.4226" y2="-0.9" width="0.01" layer="51"/>
<wire x1="0.4331" y1="-3.1" x2="0.4331" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-0.2959" y1="-1.366" x2="-0.2959" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-0.3737" y1="-1.5314" x2="-0.3737" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-0.873" y1="-1.5299" x2="-0.873" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-0.9924" y1="-3.1" x2="-0.9924" y2="-2.6472" width="0.01" layer="51"/>
<wire x1="-1.5401" y1="-3.1" x2="-1.5401" y2="-0.9" width="0.01" layer="51"/>
<wire x1="0.4331" y1="-0.9" x2="-0.9924" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-0.9924" y1="-0.9" x2="-5.475" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-0.9924" y1="-1.3528" x2="-0.9924" y2="-0.9" width="0.01" layer="51"/>
<wire x1="-5.575" y1="3.95" x2="-5.575" y2="2.6" width="0.2" layer="51"/>
<wire x1="-5.575" y1="2.6" x2="-5.575" y2="1.4" width="0.2" layer="51"/>
<wire x1="-5.575" y1="1.4" x2="-5.575" y2="-1.4" width="0.2" layer="51"/>
<wire x1="-5.575" y1="-1.4" x2="-5.575" y2="-2.6" width="0.2" layer="51"/>
<wire x1="-5.575" y1="-2.6" x2="-5.575" y2="-3.95" width="0.2" layer="51"/>
<wire x1="-5.475" y1="3.1" x2="-5.475" y2="0.9" width="0.2" layer="51"/>
<wire x1="-5.275" y1="0.9" x2="-5.275" y2="3.1" width="0.01" layer="51"/>
<wire x1="-3.6337" y1="0.9" x2="-3.6337" y2="3.1" width="0.01" layer="51"/>
<wire x1="-3.599" y1="0.9" x2="-3.599" y2="3.1" width="0.01" layer="51"/>
<wire x1="-0.873" y1="0.9" x2="-0.873" y2="1.5299" width="0.01" layer="51"/>
<wire x1="-0.3737" y1="0.9" x2="-0.3737" y2="1.5314" width="0.01" layer="51"/>
<wire x1="-0.2959" y1="0.9" x2="-0.2959" y2="1.366" width="0.01" layer="51"/>
<wire x1="0.2124" y1="0.9" x2="0.2124" y2="3.1" width="0.01" layer="51"/>
<wire x1="0.4331" y1="0.9" x2="0.4226" y2="0.9" width="0.01" layer="51"/>
<wire x1="0.4226" y1="0.9" x2="-0.9924" y2="0.9" width="0.01" layer="51"/>
<wire x1="-0.9924" y1="0.9" x2="-1.5401" y2="0.9" width="0.01" layer="51"/>
<wire x1="-1.5401" y1="0.9" x2="-5.475" y2="0.9" width="0.01" layer="51"/>
<wire x1="0.4226" y1="0.9" x2="0.4226" y2="3.1" width="0.01" layer="51"/>
<wire x1="0.4331" y1="0.9" x2="0.4331" y2="3.1" width="0.01" layer="51"/>
<wire x1="-0.2959" y1="2.634" x2="-0.2959" y2="3.1" width="0.01" layer="51"/>
<wire x1="-0.3737" y1="2.4686" x2="-0.3737" y2="3.1" width="0.01" layer="51"/>
<wire x1="-0.873" y1="2.4701" x2="-0.873" y2="3.1" width="0.01" layer="51"/>
<wire x1="-0.9924" y1="0.9" x2="-0.9924" y2="1.3528" width="0.01" layer="51"/>
<wire x1="-1.5401" y1="0.9" x2="-1.5401" y2="3.1" width="0.01" layer="51"/>
<wire x1="0.4331" y1="3.1" x2="-0.9924" y2="3.1" width="0.01" layer="51"/>
<wire x1="-0.9924" y1="3.1" x2="-5.475" y2="3.1" width="0.01" layer="51"/>
<wire x1="-0.9924" y1="2.6472" x2="-0.9924" y2="3.1" width="0.01" layer="51"/>
<wire x1="-5.975" y1="-2.6" x2="-5.975" y2="-1.4" width="0.2" layer="51"/>
<wire x1="-5.575" y1="-1.4" x2="-5.975" y2="-1.4" width="0.2" layer="51"/>
<wire x1="-5.575" y1="-2.6" x2="-5.975" y2="-2.6" width="0.2" layer="51"/>
<wire x1="-5.975" y1="1.4" x2="-5.975" y2="2.6" width="0.2" layer="51"/>
<wire x1="-5.575" y1="2.6" x2="-5.975" y2="2.6" width="0.2" layer="51"/>
<wire x1="-5.575" y1="1.4" x2="-5.975" y2="1.4" width="0.2" layer="51"/>
<wire x1="6.825" y1="3.95" x2="7.125" y2="3.65" width="0.2" layer="51" curve="-90"/>
<wire x1="6.825" y1="-3.95" x2="7.125" y2="-3.65" width="0.2" layer="51" curve="90"/>
<wire x1="7.125" y1="-0.5339" x2="7.125" y2="-3.65" width="0.2" layer="51"/>
<smd name="L1.2" x="-4.375" y="2" dx="3.9" dy="2" layer="1"/>
<smd name="L1.1" x="4.325" y="2" dx="6.4" dy="2" layer="1"/>
<smd name="L2.2" x="-4.375" y="-2" dx="3.9" dy="2" layer="1"/>
<smd name="L2.1" x="4.325" y="-2" dx="6.4" dy="2" layer="1"/>
<text x="-4.975" y="4.85" size="2" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.975" y="-6.75" size="2" layer="27" ratio="10">&gt;VALUE</text>
<text x="10.575" y="2" size="2" layer="51" font="vector" ratio="10" rot="R90">1</text>
</package>
</packages>
<packages3d>
<package3d name="P-2060-452/998-404" urn="urn:adsk.eagle:package:8647144/3" locally_modified="yes" type="model">
<packageinstances>
<packageinstance name="P-2060-452/998-404"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="S-2-POL-S">
<wire x1="-3.81" y1="-1.27" x2="3.81" y2="-1.27" width="0.2" layer="97"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="1.27" width="0.2" layer="97"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="3.81" width="0.2" layer="97"/>
<wire x1="3.81" y1="3.81" x2="-3.81" y2="3.81" width="0.2" layer="97"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="1.27" width="0.2" layer="97"/>
<text x="5.08" y="2.54" size="1.778" layer="95" font="vector" ratio="10" rot="MR180" align="center-left">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96" font="vector" ratio="10" rot="MR180" align="center-left">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pin" length="short" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pin" length="short" function="dot"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.2" layer="97"/>
<wire x1="3.81" y1="1.27" x2="-3.81" y2="1.27" width="0.2" layer="97"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2060-452/998-404" prefix="CN">
<description>&lt;b&gt;Serie 2060,  SMD-Leiterplattenklemme mit Betätigungsdrückern im Gurt 2-polig / Series 2060,  SMD terminal block with push-buttons in tape-and-reel packing 2-pole&lt;/b&gt;&lt;br&gt;&lt;br&gt;Polzahl / Pole No.: 2 &lt;br&gt;Rastermaß / Pitch: 4  mm&lt;br&gt;Bemessungsspannung / Rated Voltage: 160 V&lt;br&gt;Nennstrom / Nominal Current: 9 A&lt;br&gt;Leiterquerschnitt / Conductor Size: 0.2 - 0.75 mm2&lt;br&gt;Anschlusstechnik / Connection Technology: CAGE CLAMP&lt;sup&gt;®&lt;/sup&gt; S &lt;br&gt;Leitereinführung (zur Platine) / Conductor entry angle (to PCB): 0 °&lt;br&gt;Farbe / Color: weiß / white&lt;br&gt;&lt;br&gt; Stand: 01.01.2014. Die jeweils aktuellen Daten zu diesem Artikel finden Sie in unserem Produktkatalog, den Sie unter &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt; einsehen können. &lt;br&gt; As of: 01/01/2014. Please find the newest data for this article in our product catalogue, which can be viewed under &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt;.&lt;br&gt;</description>
<gates>
<gate name="CN" symbol="S-2-POL-S" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P-2060-452/998-404">
<connects>
<connect gate="CN" pin="1" pad="L1.1 L1.2"/>
<connect gate="CN" pin="2" pad="L2.1 L2.2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8647144/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Series 2060, SMD terminal block with push-buttons in tape-and-reel packing 2-pole"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="2060-452/998-404"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.3048" drill="0.3">
</class>
</classes>
<parts>
<part name="LOGO101" library="udo-logo" deviceset="UDO-LOGO-" device="10MM" package3d_urn="urn:adsk.eagle:package:6649258/2"/>
<part name="FRAME201" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA3_P" device=""/>
<part name="FRAME101" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA3_P" device=""/>
<part name="CN103" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN115" library="udo-wago-mcs" deviceset="733-342" device="" package3d_urn="urn:adsk.eagle:package:9475023/2"/>
<part name="X115" library="udo-dummy" deviceset="BOM" device="" value="733-112">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 12-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-112"/>
</part>
<part name="CN116" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X116" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN305" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN301" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X305" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X301" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="FRAME301" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA3_P" device=""/>
<part name="CN306" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN302" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X306" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X302" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN307" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN303" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X307" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X303" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN308" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN304" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X308" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X304" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN313" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN309" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X313" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X309" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN314" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN310" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X314" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X310" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN315" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN311" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X315" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X311" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN316" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN312" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X316" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X312" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN319" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN317" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X319" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X317" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN320" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN318" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X320" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X318" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="FRAME401" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA3_P" device=""/>
<part name="X403" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X401" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X404" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X402" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN403" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN401" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN402" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN404" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN408" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN405" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X408" library="blebox-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X405" library="blebox-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN409" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN406" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X409" library="blebox-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X406" library="blebox-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN410" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN407" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X410" library="blebox-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X407" library="blebox-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X101" library="udo-dummy" deviceset="BOM" device="" value="721-104/026-000">
<attribute name="DESCRIPTION" value="MCS-MIDI socket, 4-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="721-104/026-000"/>
</part>
<part name="CN104" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN105" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN106" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="CN201" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN202" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN203" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN204" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN205" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN206" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN207" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN208" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN209" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN210" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN211" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN212" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN213" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN214" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="CN111" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X111" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN107" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X107" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN112" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X112" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN108" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X108" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN113" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X113" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN109" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X109" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN114" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X114" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN110" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X110" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN117" library="udo-wago-mcs" deviceset="733-338" device="" package3d_urn="urn:adsk.eagle:package:10511812/2"/>
<part name="X117" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X103" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X104" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X105" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="X106" library="udo-dummy" deviceset="BOM" device="" value="733-108">
<attribute name="DESCRIPTION" value="MCS-MICRO socket, 8-way"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="733-108"/>
</part>
<part name="CN118" library="udo-wago-terminal-blocks" deviceset="2060-452/998-404" device="" package3d_urn="urn:adsk.eagle:package:8647144/3"/>
<part name="X201" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X202" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X203" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X204" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X205" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X206" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X207" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X208" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X209" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X210" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X211" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X212" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X213" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="X214" library="blebox-dummy" deviceset="BOM" device="" value="DS1016-20MASIBB">
<attribute name="DESCRIPTION" value="IDC 2x10 female connector, with strain relief, generic"/>
<attribute name="MF" value="CONNFLY"/>
<attribute name="MPN" value="DS1016-20MASIBB"/>
</part>
<part name="CN1" library="udo-wago-mcs" deviceset="721-134/001-000" device="" package3d_urn="urn:adsk.eagle:package:11036717/2"/>
</parts>
<sheets>
<sheet>
<description>MASTER</description>
<plain>
<wire x1="73.66" y1="73.66" x2="73.66" y2="147.32" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="73.66" x2="20.32" y2="147.32" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="147.32" x2="73.66" y2="147.32" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="73.66" x2="73.66" y2="73.66" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="147.32" x2="20.32" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="154.94" x2="73.66" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<wire x1="73.66" y1="147.32" x2="73.66" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<text x="22.86" y="149.86" size="2.54" layer="94">Fuse module I/O</text>
<wire x1="129.54" y1="73.66" x2="129.54" y2="147.32" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="73.66" x2="76.2" y2="147.32" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="147.32" x2="129.54" y2="147.32" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="73.66" x2="129.54" y2="73.66" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="147.32" x2="76.2" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="154.94" x2="129.54" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<wire x1="129.54" y1="147.32" x2="129.54" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<text x="78.74" y="149.86" size="2.54" layer="94">Redundancy module I/O</text>
<wire x1="20.32" y1="279.4" x2="20.32" y2="332.74" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="332.74" x2="73.66" y2="332.74" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="279.4" x2="73.66" y2="279.4" width="0.254" layer="94" style="shortdash"/>
<wire x1="73.66" y1="279.4" x2="73.66" y2="332.74" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="279.4" x2="76.2" y2="332.74" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="332.74" x2="129.54" y2="332.74" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="279.4" x2="129.54" y2="279.4" width="0.254" layer="94" style="shortdash"/>
<wire x1="129.54" y1="279.4" x2="129.54" y2="332.74" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="279.4" x2="132.08" y2="332.74" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="332.74" x2="185.42" y2="332.74" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="279.4" x2="185.42" y2="279.4" width="0.254" layer="94" style="shortdash"/>
<wire x1="185.42" y1="279.4" x2="185.42" y2="332.74" width="0.254" layer="94" style="shortdash"/>
<wire x1="187.96" y1="279.4" x2="187.96" y2="332.74" width="0.254" layer="94" style="shortdash"/>
<wire x1="187.96" y1="332.74" x2="241.3" y2="332.74" width="0.254" layer="94" style="shortdash"/>
<wire x1="187.96" y1="279.4" x2="241.3" y2="279.4" width="0.254" layer="94" style="shortdash"/>
<wire x1="241.3" y1="279.4" x2="241.3" y2="332.74" width="0.254" layer="94" style="shortdash"/>
<wire x1="73.66" y1="172.72" x2="73.66" y2="276.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="172.72" x2="20.32" y2="276.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="172.72" x2="73.66" y2="172.72" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="276.86" x2="73.66" y2="276.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="129.54" y1="172.72" x2="129.54" y2="276.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="172.72" x2="76.2" y2="276.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="172.72" x2="129.54" y2="172.72" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="276.86" x2="129.54" y2="276.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="185.42" y1="172.72" x2="185.42" y2="276.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="172.72" x2="132.08" y2="276.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="172.72" x2="185.42" y2="172.72" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="276.86" x2="185.42" y2="276.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="241.3" y1="172.72" x2="241.3" y2="276.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="187.96" y1="172.72" x2="187.96" y2="276.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="187.96" y1="172.72" x2="241.3" y2="172.72" width="0.254" layer="94" style="shortdash"/>
<wire x1="187.96" y1="276.86" x2="241.3" y2="276.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="185.42" y1="73.66" x2="185.42" y2="147.32" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="73.66" x2="132.08" y2="147.32" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="147.32" x2="185.42" y2="147.32" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="73.66" x2="185.42" y2="73.66" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="147.32" x2="132.08" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="154.94" x2="185.42" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<wire x1="185.42" y1="147.32" x2="185.42" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<text x="134.62" y="149.86" size="2.54" layer="94">Extra +24V outputs</text>
</plain>
<instances>
<instance part="FRAME101" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME101" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="CN103" gate="CN" x="25.4" y="309.88" smashed="yes" rot="MR0">
<attribute name="NAME" x="22.86" y="287.02" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="22.86" y="284.48" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN115" gate="CN" x="25.4" y="114.3" smashed="yes" rot="MR0">
<attribute name="NAME" x="22.86" y="81.28" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="22.86" y="78.74" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X115" gate="X" x="38.1" y="78.74" smashed="yes">
<attribute name="NAME" x="40.64" y="81.28" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="40.64" y="78.74" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="40.64" y="76.2" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="38.1" y="78.74" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="38.1" y="78.74" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN116" gate="CN" x="81.28" y="104.14" smashed="yes" rot="MR0">
<attribute name="NAME" x="78.74" y="81.28" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="78.74" y="78.74" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X116" gate="X" x="93.98" y="78.74" smashed="yes">
<attribute name="NAME" x="96.52" y="81.28" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="96.52" y="78.74" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="96.52" y="76.2" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="93.98" y="78.74" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="93.98" y="78.74" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X101" gate="X" x="58.42" y="363.22" smashed="yes">
<attribute name="NAME" x="60.96" y="365.76" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="60.96" y="363.22" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="60.96" y="360.68" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="58.42" y="363.22" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="58.42" y="363.22" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN104" gate="CN" x="81.28" y="309.88" smashed="yes" rot="MR0">
<attribute name="NAME" x="78.74" y="287.02" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="78.74" y="284.48" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN105" gate="CN" x="137.16" y="309.88" smashed="yes" rot="MR0">
<attribute name="NAME" x="134.62" y="287.02" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="134.62" y="284.48" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN106" gate="CN" x="193.04" y="309.88" smashed="yes" rot="MR0">
<attribute name="NAME" x="190.5" y="287.02" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="190.5" y="284.48" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="LOGO101" gate="LOGO" x="243.078" y="23.368" smashed="yes"/>
<instance part="CN111" gate="CN" x="25.4" y="203.2" smashed="yes" rot="MR0">
<attribute name="NAME" x="22.86" y="180.34" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="22.86" y="177.8" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X111" gate="X" x="38.1" y="177.8" smashed="yes">
<attribute name="NAME" x="40.64" y="180.34" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="40.64" y="177.8" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="40.64" y="175.26" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="38.1" y="177.8" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="38.1" y="177.8" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN107" gate="CN" x="25.4" y="254" smashed="yes" rot="MR0">
<attribute name="NAME" x="22.86" y="231.14" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="22.86" y="228.6" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X107" gate="X" x="38.1" y="228.6" smashed="yes">
<attribute name="NAME" x="40.64" y="231.14" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="40.64" y="228.6" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="40.64" y="226.06" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="38.1" y="228.6" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="38.1" y="228.6" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN112" gate="CN" x="81.28" y="203.2" smashed="yes" rot="MR0">
<attribute name="NAME" x="78.74" y="180.34" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="78.74" y="177.8" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X112" gate="X" x="93.98" y="177.8" smashed="yes">
<attribute name="NAME" x="96.52" y="180.34" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="96.52" y="177.8" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="96.52" y="175.26" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="93.98" y="177.8" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="93.98" y="177.8" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN108" gate="CN" x="81.28" y="254" smashed="yes" rot="MR0">
<attribute name="NAME" x="78.74" y="231.14" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="78.74" y="228.6" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X108" gate="X" x="93.98" y="228.6" smashed="yes">
<attribute name="NAME" x="96.52" y="231.14" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="96.52" y="228.6" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="96.52" y="226.06" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="93.98" y="228.6" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="93.98" y="228.6" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN113" gate="CN" x="137.16" y="203.2" smashed="yes" rot="MR0">
<attribute name="NAME" x="134.62" y="180.34" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="134.62" y="177.8" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X113" gate="X" x="149.86" y="177.8" smashed="yes">
<attribute name="NAME" x="152.4" y="180.34" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="152.4" y="177.8" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="152.4" y="175.26" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="149.86" y="177.8" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="149.86" y="177.8" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN109" gate="CN" x="137.16" y="254" smashed="yes" rot="MR0">
<attribute name="NAME" x="134.62" y="231.14" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="134.62" y="228.6" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X109" gate="X" x="149.86" y="228.6" smashed="yes">
<attribute name="NAME" x="152.4" y="231.14" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="152.4" y="228.6" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="152.4" y="226.06" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="149.86" y="228.6" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="149.86" y="228.6" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN114" gate="CN" x="193.04" y="203.2" smashed="yes" rot="MR0">
<attribute name="NAME" x="190.5" y="180.34" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="190.5" y="177.8" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X114" gate="X" x="205.74" y="177.8" smashed="yes">
<attribute name="NAME" x="208.28" y="180.34" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="208.28" y="177.8" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="208.28" y="175.26" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="205.74" y="177.8" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="205.74" y="177.8" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN110" gate="CN" x="193.04" y="254" smashed="yes" rot="MR0">
<attribute name="NAME" x="190.5" y="231.14" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="190.5" y="228.6" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X110" gate="X" x="205.74" y="228.6" smashed="yes">
<attribute name="NAME" x="208.28" y="231.14" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="208.28" y="228.6" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="208.28" y="226.06" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="205.74" y="228.6" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="205.74" y="228.6" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN117" gate="CN" x="137.16" y="104.14" smashed="yes" rot="MR0">
<attribute name="NAME" x="134.62" y="81.28" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="134.62" y="78.74" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X117" gate="X" x="149.86" y="78.74" smashed="yes">
<attribute name="NAME" x="152.4" y="81.28" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="152.4" y="78.74" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="152.4" y="76.2" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="149.86" y="78.74" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="149.86" y="78.74" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X103" gate="X" x="38.1" y="284.48" smashed="yes">
<attribute name="NAME" x="40.64" y="287.02" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="40.64" y="284.48" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="40.64" y="281.94" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="38.1" y="284.48" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="38.1" y="284.48" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X104" gate="X" x="93.98" y="284.48" smashed="yes">
<attribute name="NAME" x="96.52" y="287.02" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="96.52" y="284.48" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="96.52" y="281.94" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="93.98" y="284.48" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="93.98" y="284.48" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X105" gate="X" x="149.86" y="284.48" smashed="yes">
<attribute name="NAME" x="152.4" y="287.02" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="152.4" y="284.48" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="152.4" y="281.94" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="149.86" y="284.48" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="149.86" y="284.48" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X106" gate="X" x="205.74" y="284.48" smashed="yes">
<attribute name="NAME" x="208.28" y="287.02" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="208.28" y="284.48" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="208.28" y="281.94" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="205.74" y="284.48" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="205.74" y="284.48" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN118" gate="CN" x="58.42" y="40.64" smashed="yes" rot="MR180">
<attribute name="NAME" x="63.5" y="38.1" size="1.778" layer="95" font="vector" ratio="10" align="center-left"/>
<attribute name="VALUE" x="63.5" y="40.64" size="1.778" layer="96" font="vector" ratio="10" align="center-left"/>
</instance>
<instance part="CN1" gate="CN" x="33.02" y="363.22" smashed="yes" rot="MR0">
<attribute name="NAME" x="39.17" y="375.36" size="2" layer="95" font="vector" ratio="10" rot="MR0"/>
<attribute name="VALUE" x="39.17" y="350.06" size="2" layer="96" font="vector" ratio="10" rot="MR0"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="35.56" y1="365.76" x2="43.18" y2="365.76" width="0.1524" layer="91"/>
<label x="43.18" y="365.76" size="1.27" layer="95" xref="yes"/>
<pinref part="CN1" gate="CN" pin="P3"/>
</segment>
<segment>
<wire x1="27.94" y1="185.42" x2="38.1" y2="185.42" width="0.1524" layer="91"/>
<label x="38.1" y="185.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN111" gate="CN" pin="P1"/>
</segment>
<segment>
<wire x1="27.94" y1="195.58" x2="38.1" y2="195.58" width="0.1524" layer="91"/>
<label x="38.1" y="195.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN111" gate="CN" pin="P3"/>
</segment>
<segment>
<wire x1="38.1" y1="205.74" x2="27.94" y2="205.74" width="0.1524" layer="91"/>
<label x="38.1" y="205.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN111" gate="CN" pin="P5"/>
</segment>
<segment>
<wire x1="38.1" y1="215.9" x2="27.94" y2="215.9" width="0.1524" layer="91"/>
<label x="38.1" y="215.9" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN111" gate="CN" pin="P7"/>
</segment>
<segment>
<wire x1="27.94" y1="236.22" x2="38.1" y2="236.22" width="0.1524" layer="91"/>
<label x="38.1" y="236.22" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN107" gate="CN" pin="P1"/>
</segment>
<segment>
<wire x1="27.94" y1="246.38" x2="38.1" y2="246.38" width="0.1524" layer="91"/>
<label x="38.1" y="246.38" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN107" gate="CN" pin="P3"/>
</segment>
<segment>
<wire x1="38.1" y1="256.54" x2="27.94" y2="256.54" width="0.1524" layer="91"/>
<label x="38.1" y="256.54" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN107" gate="CN" pin="P5"/>
</segment>
<segment>
<wire x1="38.1" y1="266.7" x2="27.94" y2="266.7" width="0.1524" layer="91"/>
<label x="38.1" y="266.7" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN107" gate="CN" pin="P7"/>
</segment>
<segment>
<wire x1="83.82" y1="185.42" x2="93.98" y2="185.42" width="0.1524" layer="91"/>
<label x="93.98" y="185.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN112" gate="CN" pin="P1"/>
</segment>
<segment>
<wire x1="83.82" y1="195.58" x2="93.98" y2="195.58" width="0.1524" layer="91"/>
<label x="93.98" y="195.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN112" gate="CN" pin="P3"/>
</segment>
<segment>
<wire x1="93.98" y1="205.74" x2="83.82" y2="205.74" width="0.1524" layer="91"/>
<label x="93.98" y="205.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN112" gate="CN" pin="P5"/>
</segment>
<segment>
<wire x1="93.98" y1="215.9" x2="83.82" y2="215.9" width="0.1524" layer="91"/>
<label x="93.98" y="215.9" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN112" gate="CN" pin="P7"/>
</segment>
<segment>
<wire x1="83.82" y1="236.22" x2="93.98" y2="236.22" width="0.1524" layer="91"/>
<label x="93.98" y="236.22" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN108" gate="CN" pin="P1"/>
</segment>
<segment>
<wire x1="83.82" y1="246.38" x2="93.98" y2="246.38" width="0.1524" layer="91"/>
<label x="93.98" y="246.38" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN108" gate="CN" pin="P3"/>
</segment>
<segment>
<wire x1="93.98" y1="256.54" x2="83.82" y2="256.54" width="0.1524" layer="91"/>
<label x="93.98" y="256.54" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN108" gate="CN" pin="P5"/>
</segment>
<segment>
<wire x1="93.98" y1="266.7" x2="83.82" y2="266.7" width="0.1524" layer="91"/>
<label x="93.98" y="266.7" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN108" gate="CN" pin="P7"/>
</segment>
<segment>
<wire x1="139.7" y1="185.42" x2="149.86" y2="185.42" width="0.1524" layer="91"/>
<label x="149.86" y="185.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN113" gate="CN" pin="P1"/>
</segment>
<segment>
<wire x1="139.7" y1="195.58" x2="149.86" y2="195.58" width="0.1524" layer="91"/>
<label x="149.86" y="195.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN113" gate="CN" pin="P3"/>
</segment>
<segment>
<wire x1="149.86" y1="205.74" x2="139.7" y2="205.74" width="0.1524" layer="91"/>
<label x="149.86" y="205.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN113" gate="CN" pin="P5"/>
</segment>
<segment>
<wire x1="149.86" y1="215.9" x2="139.7" y2="215.9" width="0.1524" layer="91"/>
<label x="149.86" y="215.9" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN113" gate="CN" pin="P7"/>
</segment>
<segment>
<wire x1="139.7" y1="236.22" x2="149.86" y2="236.22" width="0.1524" layer="91"/>
<label x="149.86" y="236.22" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN109" gate="CN" pin="P1"/>
</segment>
<segment>
<wire x1="139.7" y1="246.38" x2="149.86" y2="246.38" width="0.1524" layer="91"/>
<label x="149.86" y="246.38" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN109" gate="CN" pin="P3"/>
</segment>
<segment>
<wire x1="149.86" y1="256.54" x2="139.7" y2="256.54" width="0.1524" layer="91"/>
<label x="149.86" y="256.54" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN109" gate="CN" pin="P5"/>
</segment>
<segment>
<wire x1="149.86" y1="266.7" x2="139.7" y2="266.7" width="0.1524" layer="91"/>
<label x="149.86" y="266.7" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN109" gate="CN" pin="P7"/>
</segment>
<segment>
<wire x1="195.58" y1="185.42" x2="205.74" y2="185.42" width="0.1524" layer="91"/>
<label x="205.74" y="185.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN114" gate="CN" pin="P1"/>
</segment>
<segment>
<wire x1="195.58" y1="195.58" x2="205.74" y2="195.58" width="0.1524" layer="91"/>
<label x="205.74" y="195.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN114" gate="CN" pin="P3"/>
</segment>
<segment>
<wire x1="205.74" y1="205.74" x2="195.58" y2="205.74" width="0.1524" layer="91"/>
<label x="205.74" y="205.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN114" gate="CN" pin="P5"/>
</segment>
<segment>
<wire x1="205.74" y1="215.9" x2="195.58" y2="215.9" width="0.1524" layer="91"/>
<label x="205.74" y="215.9" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN114" gate="CN" pin="P7"/>
</segment>
<segment>
<wire x1="195.58" y1="236.22" x2="205.74" y2="236.22" width="0.1524" layer="91"/>
<label x="205.74" y="236.22" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN110" gate="CN" pin="P1"/>
</segment>
<segment>
<wire x1="195.58" y1="246.38" x2="205.74" y2="246.38" width="0.1524" layer="91"/>
<label x="205.74" y="246.38" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN110" gate="CN" pin="P3"/>
</segment>
<segment>
<wire x1="205.74" y1="256.54" x2="195.58" y2="256.54" width="0.1524" layer="91"/>
<label x="205.74" y="256.54" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN110" gate="CN" pin="P5"/>
</segment>
<segment>
<wire x1="205.74" y1="266.7" x2="195.58" y2="266.7" width="0.1524" layer="91"/>
<label x="205.74" y="266.7" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN110" gate="CN" pin="P7"/>
</segment>
<segment>
<wire x1="139.7" y1="86.36" x2="149.86" y2="86.36" width="0.1524" layer="91"/>
<label x="149.86" y="86.36" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN117" gate="CN" pin="P1"/>
</segment>
<segment>
<wire x1="139.7" y1="96.52" x2="149.86" y2="96.52" width="0.1524" layer="91"/>
<label x="149.86" y="96.52" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN117" gate="CN" pin="P3"/>
</segment>
<segment>
<wire x1="149.86" y1="106.68" x2="139.7" y2="106.68" width="0.1524" layer="91"/>
<label x="149.86" y="106.68" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN117" gate="CN" pin="P5"/>
</segment>
<segment>
<wire x1="149.86" y1="116.84" x2="139.7" y2="116.84" width="0.1524" layer="91"/>
<label x="149.86" y="116.84" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN117" gate="CN" pin="P7"/>
</segment>
<segment>
<pinref part="CN118" gate="CN" pin="2"/>
<wire x1="48.26" y1="40.64" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
<label x="48.26" y="40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="35.56" y1="370.84" x2="43.18" y2="370.84" width="0.1524" layer="91"/>
<label x="43.18" y="370.84" size="1.27" layer="95" xref="yes"/>
<pinref part="CN1" gate="CN" pin="P4"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="P2"/>
<wire x1="35.56" y1="360.68" x2="43.18" y2="360.68" width="0.1524" layer="91"/>
<label x="43.18" y="360.68" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="P1"/>
<wire x1="43.18" y1="355.6" x2="35.56" y2="355.6" width="0.1524" layer="91"/>
<label x="43.18" y="355.6" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_PSU1_S1" class="0">
<segment>
<pinref part="CN115" gate="CN" pin="P1"/>
<wire x1="27.94" y1="86.36" x2="38.1" y2="86.36" width="0.1524" layer="91"/>
<label x="38.1" y="86.36" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU1_S2" class="0">
<segment>
<pinref part="CN115" gate="CN" pin="P2"/>
<wire x1="27.94" y1="91.44" x2="38.1" y2="91.44" width="0.1524" layer="91"/>
<label x="38.1" y="91.44" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU1_S3" class="0">
<segment>
<pinref part="CN115" gate="CN" pin="P3"/>
<wire x1="27.94" y1="96.52" x2="38.1" y2="96.52" width="0.1524" layer="91"/>
<label x="38.1" y="96.52" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_PSU2_S1" class="0">
<segment>
<pinref part="CN115" gate="CN" pin="P4"/>
<wire x1="27.94" y1="101.6" x2="38.1" y2="101.6" width="0.1524" layer="91"/>
<label x="38.1" y="101.6" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_PSU3_S1" class="0">
<segment>
<pinref part="CN115" gate="CN" pin="P7"/>
<wire x1="38.1" y1="116.84" x2="27.94" y2="116.84" width="0.1524" layer="91"/>
<label x="38.1" y="116.84" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_PSU4_S1" class="0">
<segment>
<pinref part="CN115" gate="CN" pin="P10"/>
<wire x1="38.1" y1="132.08" x2="27.94" y2="132.08" width="0.1524" layer="91"/>
<label x="38.1" y="132.08" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU2_S2" class="0">
<segment>
<pinref part="CN115" gate="CN" pin="P5"/>
<wire x1="38.1" y1="106.68" x2="27.94" y2="106.68" width="0.1524" layer="91"/>
<label x="38.1" y="106.68" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU4_S2" class="0">
<segment>
<pinref part="CN115" gate="CN" pin="P11"/>
<wire x1="38.1" y1="137.16" x2="27.94" y2="137.16" width="0.1524" layer="91"/>
<label x="38.1" y="137.16" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU3_S2" class="0">
<segment>
<pinref part="CN115" gate="CN" pin="P8"/>
<wire x1="38.1" y1="121.92" x2="27.94" y2="121.92" width="0.1524" layer="91"/>
<label x="38.1" y="121.92" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU2_S3" class="0">
<segment>
<pinref part="CN115" gate="CN" pin="P6"/>
<wire x1="38.1" y1="111.76" x2="27.94" y2="111.76" width="0.1524" layer="91"/>
<label x="38.1" y="111.76" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU3_S3" class="0">
<segment>
<pinref part="CN115" gate="CN" pin="P9"/>
<wire x1="38.1" y1="127" x2="27.94" y2="127" width="0.1524" layer="91"/>
<label x="38.1" y="127" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU4_S3" class="0">
<segment>
<pinref part="CN115" gate="CN" pin="P12"/>
<wire x1="38.1" y1="142.24" x2="27.94" y2="142.24" width="0.1524" layer="91"/>
<label x="38.1" y="142.24" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU1_OK" class="0">
<segment>
<wire x1="83.82" y1="91.44" x2="93.98" y2="91.44" width="0.1524" layer="91"/>
<label x="93.98" y="91.44" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN116" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_PSU2_OK" class="0">
<segment>
<wire x1="83.82" y1="101.6" x2="93.98" y2="101.6" width="0.1524" layer="91"/>
<label x="93.98" y="101.6" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN116" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_PSU3_OK" class="0">
<segment>
<wire x1="93.98" y1="111.76" x2="83.82" y2="111.76" width="0.1524" layer="91"/>
<label x="93.98" y="111.76" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN116" gate="CN" pin="P6"/>
</segment>
</net>
<net name="+24V_PSU1_CH1" class="0">
<segment>
<wire x1="27.94" y1="190.5" x2="38.1" y2="190.5" width="0.1524" layer="91"/>
<label x="38.1" y="190.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN111" gate="CN" pin="P2"/>
</segment>
<segment>
<wire x1="27.94" y1="327.66" x2="35.56" y2="327.66" width="0.1524" layer="91"/>
<label x="35.56" y="327.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN103" gate="CN" pin="P8"/>
</segment>
<segment>
<wire x1="83.82" y1="86.36" x2="93.98" y2="86.36" width="0.1524" layer="91"/>
<label x="93.98" y="86.36" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN116" gate="CN" pin="P1"/>
</segment>
<segment>
<wire x1="83.82" y1="96.52" x2="93.98" y2="96.52" width="0.1524" layer="91"/>
<label x="93.98" y="96.52" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN116" gate="CN" pin="P3"/>
</segment>
<segment>
<wire x1="93.98" y1="106.68" x2="83.82" y2="106.68" width="0.1524" layer="91"/>
<label x="93.98" y="106.68" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN116" gate="CN" pin="P5"/>
</segment>
<segment>
<wire x1="93.98" y1="116.84" x2="83.82" y2="116.84" width="0.1524" layer="91"/>
<label x="93.98" y="116.84" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN116" gate="CN" pin="P7"/>
</segment>
<segment>
<wire x1="139.7" y1="91.44" x2="149.86" y2="91.44" width="0.1524" layer="91"/>
<label x="149.86" y="91.44" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN117" gate="CN" pin="P2"/>
</segment>
<segment>
<wire x1="139.7" y1="101.6" x2="149.86" y2="101.6" width="0.1524" layer="91"/>
<label x="149.86" y="101.6" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN117" gate="CN" pin="P4"/>
</segment>
<segment>
<wire x1="149.86" y1="111.76" x2="139.7" y2="111.76" width="0.1524" layer="91"/>
<label x="149.86" y="111.76" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN117" gate="CN" pin="P6"/>
</segment>
<segment>
<wire x1="149.86" y1="121.92" x2="139.7" y2="121.92" width="0.1524" layer="91"/>
<label x="149.86" y="121.92" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN117" gate="CN" pin="P8"/>
</segment>
<segment>
<pinref part="CN118" gate="CN" pin="1"/>
<wire x1="55.88" y1="38.1" x2="48.26" y2="38.1" width="0.1524" layer="91"/>
<label x="48.26" y="38.1" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+24V_PSU1_CH2" class="0">
<segment>
<wire x1="27.94" y1="200.66" x2="38.1" y2="200.66" width="0.1524" layer="91"/>
<label x="38.1" y="200.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN111" gate="CN" pin="P4"/>
</segment>
<segment>
<wire x1="27.94" y1="322.58" x2="35.56" y2="322.58" width="0.1524" layer="91"/>
<label x="35.56" y="322.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN103" gate="CN" pin="P7"/>
</segment>
</net>
<net name="+24V_PSU1_CH3" class="0">
<segment>
<wire x1="38.1" y1="210.82" x2="27.94" y2="210.82" width="0.1524" layer="91"/>
<label x="38.1" y="210.82" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN111" gate="CN" pin="P6"/>
</segment>
<segment>
<wire x1="27.94" y1="317.5" x2="35.56" y2="317.5" width="0.1524" layer="91"/>
<label x="35.56" y="317.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN103" gate="CN" pin="P6"/>
</segment>
</net>
<net name="+24V_PSU1_CH4" class="0">
<segment>
<wire x1="38.1" y1="220.98" x2="27.94" y2="220.98" width="0.1524" layer="91"/>
<label x="38.1" y="220.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN111" gate="CN" pin="P8"/>
</segment>
<segment>
<wire x1="27.94" y1="312.42" x2="35.56" y2="312.42" width="0.1524" layer="91"/>
<label x="35.56" y="312.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN103" gate="CN" pin="P5"/>
</segment>
</net>
<net name="+24V_PSU1_CH5" class="0">
<segment>
<wire x1="27.94" y1="241.3" x2="38.1" y2="241.3" width="0.1524" layer="91"/>
<label x="38.1" y="241.3" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN107" gate="CN" pin="P2"/>
</segment>
<segment>
<wire x1="27.94" y1="307.34" x2="35.56" y2="307.34" width="0.1524" layer="91"/>
<label x="35.56" y="307.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN103" gate="CN" pin="P4"/>
</segment>
</net>
<net name="+24V_PSU1_CH6" class="0">
<segment>
<wire x1="27.94" y1="251.46" x2="38.1" y2="251.46" width="0.1524" layer="91"/>
<label x="38.1" y="251.46" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN107" gate="CN" pin="P4"/>
</segment>
<segment>
<wire x1="27.94" y1="302.26" x2="35.56" y2="302.26" width="0.1524" layer="91"/>
<label x="35.56" y="302.26" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN103" gate="CN" pin="P3"/>
</segment>
</net>
<net name="+24V_PSU1_CH7" class="0">
<segment>
<wire x1="38.1" y1="261.62" x2="27.94" y2="261.62" width="0.1524" layer="91"/>
<label x="38.1" y="261.62" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN107" gate="CN" pin="P6"/>
</segment>
<segment>
<wire x1="27.94" y1="297.18" x2="35.56" y2="297.18" width="0.1524" layer="91"/>
<label x="35.56" y="297.18" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN103" gate="CN" pin="P2"/>
</segment>
</net>
<net name="+24V_PSU1_CH8" class="0">
<segment>
<wire x1="38.1" y1="271.78" x2="27.94" y2="271.78" width="0.1524" layer="91"/>
<label x="38.1" y="271.78" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN107" gate="CN" pin="P8"/>
</segment>
<segment>
<wire x1="27.94" y1="292.1" x2="35.56" y2="292.1" width="0.1524" layer="91"/>
<label x="35.56" y="292.1" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN103" gate="CN" pin="P1"/>
</segment>
</net>
<net name="+24V_PSU2_CH1" class="0">
<segment>
<wire x1="83.82" y1="190.5" x2="93.98" y2="190.5" width="0.1524" layer="91"/>
<label x="93.98" y="190.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN112" gate="CN" pin="P2"/>
</segment>
<segment>
<wire x1="83.82" y1="327.66" x2="91.44" y2="327.66" width="0.1524" layer="91"/>
<label x="91.44" y="327.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN104" gate="CN" pin="P8"/>
</segment>
</net>
<net name="+24V_PSU2_CH2" class="0">
<segment>
<wire x1="83.82" y1="200.66" x2="93.98" y2="200.66" width="0.1524" layer="91"/>
<label x="93.98" y="200.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN112" gate="CN" pin="P4"/>
</segment>
<segment>
<wire x1="83.82" y1="322.58" x2="91.44" y2="322.58" width="0.1524" layer="91"/>
<label x="91.44" y="322.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN104" gate="CN" pin="P7"/>
</segment>
</net>
<net name="+24V_PSU2_CH3" class="0">
<segment>
<wire x1="93.98" y1="210.82" x2="83.82" y2="210.82" width="0.1524" layer="91"/>
<label x="93.98" y="210.82" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN112" gate="CN" pin="P6"/>
</segment>
<segment>
<wire x1="83.82" y1="317.5" x2="91.44" y2="317.5" width="0.1524" layer="91"/>
<label x="91.44" y="317.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN104" gate="CN" pin="P6"/>
</segment>
</net>
<net name="+24V_PSU2_CH4" class="0">
<segment>
<wire x1="93.98" y1="220.98" x2="83.82" y2="220.98" width="0.1524" layer="91"/>
<label x="93.98" y="220.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN112" gate="CN" pin="P8"/>
</segment>
<segment>
<wire x1="83.82" y1="312.42" x2="91.44" y2="312.42" width="0.1524" layer="91"/>
<label x="91.44" y="312.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN104" gate="CN" pin="P5"/>
</segment>
</net>
<net name="+24V_PSU2_CH5" class="0">
<segment>
<wire x1="83.82" y1="241.3" x2="93.98" y2="241.3" width="0.1524" layer="91"/>
<label x="93.98" y="241.3" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN108" gate="CN" pin="P2"/>
</segment>
<segment>
<wire x1="83.82" y1="307.34" x2="91.44" y2="307.34" width="0.1524" layer="91"/>
<label x="91.44" y="307.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN104" gate="CN" pin="P4"/>
</segment>
</net>
<net name="+24V_PSU2_CH6" class="0">
<segment>
<wire x1="83.82" y1="251.46" x2="93.98" y2="251.46" width="0.1524" layer="91"/>
<label x="93.98" y="251.46" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN108" gate="CN" pin="P4"/>
</segment>
<segment>
<wire x1="83.82" y1="302.26" x2="91.44" y2="302.26" width="0.1524" layer="91"/>
<label x="91.44" y="302.26" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN104" gate="CN" pin="P3"/>
</segment>
</net>
<net name="+24V_PSU2_CH7" class="0">
<segment>
<wire x1="93.98" y1="261.62" x2="83.82" y2="261.62" width="0.1524" layer="91"/>
<label x="93.98" y="261.62" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN108" gate="CN" pin="P6"/>
</segment>
<segment>
<wire x1="83.82" y1="297.18" x2="91.44" y2="297.18" width="0.1524" layer="91"/>
<label x="91.44" y="297.18" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN104" gate="CN" pin="P2"/>
</segment>
</net>
<net name="+24V_PSU2_CH8" class="0">
<segment>
<wire x1="93.98" y1="271.78" x2="83.82" y2="271.78" width="0.1524" layer="91"/>
<label x="93.98" y="271.78" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN108" gate="CN" pin="P8"/>
</segment>
<segment>
<wire x1="83.82" y1="292.1" x2="91.44" y2="292.1" width="0.1524" layer="91"/>
<label x="91.44" y="292.1" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN104" gate="CN" pin="P1"/>
</segment>
</net>
<net name="+24V_PSU3_CH1" class="0">
<segment>
<wire x1="139.7" y1="190.5" x2="149.86" y2="190.5" width="0.1524" layer="91"/>
<label x="149.86" y="190.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN113" gate="CN" pin="P2"/>
</segment>
<segment>
<wire x1="139.7" y1="327.66" x2="147.32" y2="327.66" width="0.1524" layer="91"/>
<label x="147.32" y="327.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN105" gate="CN" pin="P8"/>
</segment>
</net>
<net name="+24V_PSU3_CH2" class="0">
<segment>
<wire x1="139.7" y1="200.66" x2="149.86" y2="200.66" width="0.1524" layer="91"/>
<label x="149.86" y="200.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN113" gate="CN" pin="P4"/>
</segment>
<segment>
<wire x1="139.7" y1="322.58" x2="147.32" y2="322.58" width="0.1524" layer="91"/>
<label x="147.32" y="322.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN105" gate="CN" pin="P7"/>
</segment>
</net>
<net name="+24V_PSU3_CH3" class="0">
<segment>
<wire x1="149.86" y1="210.82" x2="139.7" y2="210.82" width="0.1524" layer="91"/>
<label x="149.86" y="210.82" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN113" gate="CN" pin="P6"/>
</segment>
<segment>
<wire x1="139.7" y1="317.5" x2="147.32" y2="317.5" width="0.1524" layer="91"/>
<label x="147.32" y="317.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN105" gate="CN" pin="P6"/>
</segment>
</net>
<net name="+24V_PSU3_CH4" class="0">
<segment>
<wire x1="149.86" y1="220.98" x2="139.7" y2="220.98" width="0.1524" layer="91"/>
<label x="149.86" y="220.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN113" gate="CN" pin="P8"/>
</segment>
<segment>
<wire x1="139.7" y1="312.42" x2="147.32" y2="312.42" width="0.1524" layer="91"/>
<label x="147.32" y="312.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN105" gate="CN" pin="P5"/>
</segment>
</net>
<net name="+24V_PSU3_CH5" class="0">
<segment>
<wire x1="139.7" y1="241.3" x2="149.86" y2="241.3" width="0.1524" layer="91"/>
<label x="149.86" y="241.3" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN109" gate="CN" pin="P2"/>
</segment>
<segment>
<wire x1="139.7" y1="307.34" x2="147.32" y2="307.34" width="0.1524" layer="91"/>
<label x="147.32" y="307.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN105" gate="CN" pin="P4"/>
</segment>
</net>
<net name="+24V_PSU3_CH6" class="0">
<segment>
<wire x1="139.7" y1="251.46" x2="149.86" y2="251.46" width="0.1524" layer="91"/>
<label x="149.86" y="251.46" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN109" gate="CN" pin="P4"/>
</segment>
<segment>
<wire x1="139.7" y1="302.26" x2="147.32" y2="302.26" width="0.1524" layer="91"/>
<label x="147.32" y="302.26" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN105" gate="CN" pin="P3"/>
</segment>
</net>
<net name="+24V_PSU3_CH7" class="0">
<segment>
<wire x1="149.86" y1="261.62" x2="139.7" y2="261.62" width="0.1524" layer="91"/>
<label x="149.86" y="261.62" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN109" gate="CN" pin="P6"/>
</segment>
<segment>
<wire x1="139.7" y1="297.18" x2="147.32" y2="297.18" width="0.1524" layer="91"/>
<label x="147.32" y="297.18" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN105" gate="CN" pin="P2"/>
</segment>
</net>
<net name="+24V_PSU3_CH8" class="0">
<segment>
<wire x1="149.86" y1="271.78" x2="139.7" y2="271.78" width="0.1524" layer="91"/>
<label x="149.86" y="271.78" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN109" gate="CN" pin="P8"/>
</segment>
<segment>
<wire x1="139.7" y1="292.1" x2="147.32" y2="292.1" width="0.1524" layer="91"/>
<label x="147.32" y="292.1" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN105" gate="CN" pin="P1"/>
</segment>
</net>
<net name="+24V_PSU4_CH1" class="0">
<segment>
<wire x1="195.58" y1="190.5" x2="205.74" y2="190.5" width="0.1524" layer="91"/>
<label x="205.74" y="190.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN114" gate="CN" pin="P2"/>
</segment>
<segment>
<wire x1="195.58" y1="327.66" x2="203.2" y2="327.66" width="0.1524" layer="91"/>
<label x="203.2" y="327.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN106" gate="CN" pin="P8"/>
</segment>
</net>
<net name="+24V_PSU4_CH2" class="0">
<segment>
<wire x1="195.58" y1="200.66" x2="205.74" y2="200.66" width="0.1524" layer="91"/>
<label x="205.74" y="200.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN114" gate="CN" pin="P4"/>
</segment>
<segment>
<wire x1="195.58" y1="322.58" x2="203.2" y2="322.58" width="0.1524" layer="91"/>
<label x="203.2" y="322.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN106" gate="CN" pin="P7"/>
</segment>
</net>
<net name="+24V_PSU4_CH3" class="0">
<segment>
<wire x1="205.74" y1="210.82" x2="195.58" y2="210.82" width="0.1524" layer="91"/>
<label x="205.74" y="210.82" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN114" gate="CN" pin="P6"/>
</segment>
<segment>
<wire x1="195.58" y1="317.5" x2="203.2" y2="317.5" width="0.1524" layer="91"/>
<label x="203.2" y="317.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN106" gate="CN" pin="P6"/>
</segment>
</net>
<net name="+24V_PSU4_CH4" class="0">
<segment>
<wire x1="205.74" y1="220.98" x2="195.58" y2="220.98" width="0.1524" layer="91"/>
<label x="205.74" y="220.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN114" gate="CN" pin="P8"/>
</segment>
<segment>
<wire x1="195.58" y1="312.42" x2="203.2" y2="312.42" width="0.1524" layer="91"/>
<label x="203.2" y="312.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN106" gate="CN" pin="P5"/>
</segment>
</net>
<net name="+24V_PSU4_CH5" class="0">
<segment>
<wire x1="195.58" y1="241.3" x2="205.74" y2="241.3" width="0.1524" layer="91"/>
<label x="205.74" y="241.3" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN110" gate="CN" pin="P2"/>
</segment>
<segment>
<wire x1="195.58" y1="307.34" x2="203.2" y2="307.34" width="0.1524" layer="91"/>
<label x="203.2" y="307.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN106" gate="CN" pin="P4"/>
</segment>
</net>
<net name="+24V_PSU4_CH6" class="0">
<segment>
<wire x1="195.58" y1="251.46" x2="205.74" y2="251.46" width="0.1524" layer="91"/>
<label x="205.74" y="251.46" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN110" gate="CN" pin="P4"/>
</segment>
<segment>
<wire x1="195.58" y1="302.26" x2="203.2" y2="302.26" width="0.1524" layer="91"/>
<label x="203.2" y="302.26" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN106" gate="CN" pin="P3"/>
</segment>
</net>
<net name="+24V_PSU4_CH7" class="0">
<segment>
<wire x1="205.74" y1="261.62" x2="195.58" y2="261.62" width="0.1524" layer="91"/>
<label x="205.74" y="261.62" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN110" gate="CN" pin="P6"/>
</segment>
<segment>
<wire x1="195.58" y1="297.18" x2="203.2" y2="297.18" width="0.1524" layer="91"/>
<label x="203.2" y="297.18" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN106" gate="CN" pin="P2"/>
</segment>
</net>
<net name="+24V_PSU4_CH8" class="0">
<segment>
<wire x1="205.74" y1="271.78" x2="195.58" y2="271.78" width="0.1524" layer="91"/>
<label x="205.74" y="271.78" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN110" gate="CN" pin="P8"/>
</segment>
<segment>
<wire x1="195.58" y1="292.1" x2="203.2" y2="292.1" width="0.1524" layer="91"/>
<label x="203.2" y="292.1" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN106" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_IN_PSU4_OK" class="0">
<segment>
<wire x1="93.98" y1="121.92" x2="83.82" y2="121.92" width="0.1524" layer="91"/>
<label x="93.98" y="121.92" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN116" gate="CN" pin="P8"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>PLC</description>
<plain>
<text x="167.64" y="27.94" size="2.54" layer="94">PLC</text>
</plain>
<instances>
<instance part="FRAME201" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME201" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="CN201" gate="CN" x="58.42" y="365.76" smashed="yes">
<attribute name="VALUE" x="54.61" y="347.98" size="1.778" layer="96"/>
<attribute name="NAME" x="54.61" y="379.222" size="1.778" layer="95"/>
</instance>
<instance part="CN203" gate="CN" x="58.42" y="294.64" smashed="yes">
<attribute name="VALUE" x="54.61" y="276.86" size="1.778" layer="96"/>
<attribute name="NAME" x="54.61" y="308.102" size="1.778" layer="95"/>
</instance>
<instance part="CN205" gate="CN" x="58.42" y="218.44" smashed="yes">
<attribute name="VALUE" x="54.61" y="200.66" size="1.778" layer="96"/>
<attribute name="NAME" x="54.61" y="231.902" size="1.778" layer="95"/>
</instance>
<instance part="CN202" gate="CN" x="58.42" y="330.2" smashed="yes">
<attribute name="VALUE" x="54.61" y="312.42" size="1.778" layer="96"/>
<attribute name="NAME" x="54.61" y="343.662" size="1.778" layer="95"/>
</instance>
<instance part="CN204" gate="CN" x="58.42" y="256.54" smashed="yes">
<attribute name="VALUE" x="54.61" y="238.76" size="1.778" layer="96"/>
<attribute name="NAME" x="54.61" y="270.002" size="1.778" layer="95"/>
</instance>
<instance part="CN206" gate="CN" x="58.42" y="177.8" smashed="yes">
<attribute name="VALUE" x="54.61" y="160.02" size="1.778" layer="96"/>
<attribute name="NAME" x="54.61" y="191.262" size="1.778" layer="95"/>
</instance>
<instance part="CN208" gate="CN" x="58.42" y="99.06" smashed="yes">
<attribute name="VALUE" x="54.61" y="81.28" size="1.778" layer="96"/>
<attribute name="NAME" x="54.61" y="112.522" size="1.778" layer="95"/>
</instance>
<instance part="CN210" gate="CN" x="58.42" y="20.32" smashed="yes">
<attribute name="VALUE" x="54.61" y="2.54" size="1.778" layer="96"/>
<attribute name="NAME" x="54.61" y="33.782" size="1.778" layer="95"/>
</instance>
<instance part="CN207" gate="CN" x="58.42" y="139.7" smashed="yes">
<attribute name="VALUE" x="54.61" y="121.92" size="1.778" layer="96"/>
<attribute name="NAME" x="54.61" y="153.162" size="1.778" layer="95"/>
</instance>
<instance part="CN209" gate="CN" x="58.42" y="60.96" smashed="yes">
<attribute name="VALUE" x="54.61" y="43.18" size="1.778" layer="96"/>
<attribute name="NAME" x="54.61" y="74.422" size="1.778" layer="95"/>
</instance>
<instance part="CN211" gate="CN" x="190.5" y="363.22" smashed="yes">
<attribute name="VALUE" x="186.69" y="345.44" size="1.778" layer="96"/>
<attribute name="NAME" x="186.69" y="376.682" size="1.778" layer="95"/>
</instance>
<instance part="CN213" gate="CN" x="190.5" y="287.02" smashed="yes">
<attribute name="VALUE" x="186.69" y="269.24" size="1.778" layer="96"/>
<attribute name="NAME" x="186.69" y="300.482" size="1.778" layer="95"/>
</instance>
<instance part="CN212" gate="CN" x="190.5" y="325.12" smashed="yes">
<attribute name="VALUE" x="186.69" y="307.34" size="1.778" layer="96"/>
<attribute name="NAME" x="186.69" y="338.582" size="1.778" layer="95"/>
</instance>
<instance part="CN214" gate="CN" x="190.5" y="248.92" smashed="yes">
<attribute name="VALUE" x="186.69" y="231.14" size="1.778" layer="96"/>
<attribute name="NAME" x="186.69" y="262.382" size="1.778" layer="95"/>
</instance>
<instance part="X201" gate="X" x="139.7" y="220.98" smashed="yes">
<attribute name="NAME" x="142.24" y="223.52" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="220.98" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="218.44" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="220.98" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X202" gate="X" x="139.7" y="210.82" smashed="yes">
<attribute name="NAME" x="142.24" y="213.36" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="210.82" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="208.28" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="210.82" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="210.82" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X203" gate="X" x="139.7" y="200.66" smashed="yes">
<attribute name="NAME" x="142.24" y="203.2" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="200.66" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="198.12" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="200.66" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="200.66" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X204" gate="X" x="139.7" y="190.5" smashed="yes">
<attribute name="NAME" x="142.24" y="193.04" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="190.5" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="187.96" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="190.5" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="190.5" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X205" gate="X" x="139.7" y="180.34" smashed="yes">
<attribute name="NAME" x="142.24" y="182.88" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="180.34" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="177.8" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="180.34" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="180.34" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X206" gate="X" x="139.7" y="170.18" smashed="yes">
<attribute name="NAME" x="142.24" y="172.72" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="170.18" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="167.64" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="170.18" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="170.18" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X207" gate="X" x="139.7" y="160.02" smashed="yes">
<attribute name="NAME" x="142.24" y="162.56" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="160.02" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="157.48" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="160.02" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="160.02" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X208" gate="X" x="139.7" y="149.86" smashed="yes">
<attribute name="NAME" x="142.24" y="152.4" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="149.86" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="147.32" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="149.86" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="149.86" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X209" gate="X" x="139.7" y="139.7" smashed="yes">
<attribute name="NAME" x="142.24" y="142.24" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="139.7" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="137.16" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="139.7" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="139.7" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X210" gate="X" x="139.7" y="129.54" smashed="yes">
<attribute name="NAME" x="142.24" y="132.08" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="129.54" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="127" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="129.54" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="129.54" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X211" gate="X" x="139.7" y="119.38" smashed="yes">
<attribute name="NAME" x="142.24" y="121.92" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="119.38" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="116.84" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="119.38" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="119.38" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X212" gate="X" x="139.7" y="109.22" smashed="yes">
<attribute name="NAME" x="142.24" y="111.76" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="109.22" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="106.68" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="109.22" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="109.22" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X213" gate="X" x="139.7" y="99.06" smashed="yes">
<attribute name="NAME" x="142.24" y="101.6" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="99.06" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="96.52" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="99.06" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="99.06" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X214" gate="X" x="139.7" y="88.9" smashed="yes">
<attribute name="NAME" x="142.24" y="91.44" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="142.24" y="88.9" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="142.24" y="86.36" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="139.7" y="88.9" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="88.9" size="1.27" layer="96" display="off"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="SIG_OUT_SP2_LOCK" class="0">
<segment>
<wire x1="66.04" y1="25.4" x2="73.66" y2="25.4" width="0.1524" layer="91"/>
<label x="73.66" y="25.4" size="1.27" layer="95" xref="yes"/>
<pinref part="CN210" gate="CN" pin="15"/>
</segment>
</net>
<net name="+24V_PSU1_CH1" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="17"/>
<wire x1="66.04" y1="373.38" x2="68.58" y2="373.38" width="0.1524" layer="91"/>
<wire x1="68.58" y1="373.38" x2="68.58" y2="375.92" width="0.1524" layer="91"/>
<pinref part="CN201" gate="CN" pin="19"/>
<wire x1="68.58" y1="375.92" x2="66.04" y2="375.92" width="0.1524" layer="91"/>
<wire x1="68.58" y1="375.92" x2="73.66" y2="375.92" width="0.1524" layer="91"/>
<junction x="68.58" y="375.92"/>
<label x="73.66" y="375.92" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="66.04" y1="302.26" x2="68.58" y2="302.26" width="0.1524" layer="91"/>
<wire x1="68.58" y1="302.26" x2="68.58" y2="304.8" width="0.1524" layer="91"/>
<wire x1="68.58" y1="304.8" x2="66.04" y2="304.8" width="0.1524" layer="91"/>
<wire x1="68.58" y1="304.8" x2="73.66" y2="304.8" width="0.1524" layer="91"/>
<junction x="68.58" y="304.8"/>
<label x="73.66" y="304.8" size="1.27" layer="95" xref="yes"/>
<pinref part="CN203" gate="CN" pin="17"/>
<pinref part="CN203" gate="CN" pin="19"/>
</segment>
<segment>
<wire x1="66.04" y1="226.06" x2="68.58" y2="226.06" width="0.1524" layer="91"/>
<wire x1="68.58" y1="226.06" x2="68.58" y2="228.6" width="0.1524" layer="91"/>
<wire x1="68.58" y1="228.6" x2="66.04" y2="228.6" width="0.1524" layer="91"/>
<wire x1="68.58" y1="228.6" x2="73.66" y2="228.6" width="0.1524" layer="91"/>
<junction x="68.58" y="228.6"/>
<label x="73.66" y="228.6" size="1.27" layer="95" xref="yes"/>
<pinref part="CN205" gate="CN" pin="17"/>
<pinref part="CN205" gate="CN" pin="19"/>
</segment>
<segment>
<wire x1="66.04" y1="337.82" x2="68.58" y2="337.82" width="0.1524" layer="91"/>
<wire x1="68.58" y1="337.82" x2="68.58" y2="340.36" width="0.1524" layer="91"/>
<wire x1="68.58" y1="340.36" x2="66.04" y2="340.36" width="0.1524" layer="91"/>
<wire x1="68.58" y1="340.36" x2="73.66" y2="340.36" width="0.1524" layer="91"/>
<junction x="68.58" y="340.36"/>
<label x="73.66" y="340.36" size="1.27" layer="95" xref="yes"/>
<pinref part="CN202" gate="CN" pin="17"/>
<pinref part="CN202" gate="CN" pin="19"/>
</segment>
<segment>
<wire x1="66.04" y1="264.16" x2="68.58" y2="264.16" width="0.1524" layer="91"/>
<wire x1="68.58" y1="264.16" x2="68.58" y2="266.7" width="0.1524" layer="91"/>
<wire x1="68.58" y1="266.7" x2="66.04" y2="266.7" width="0.1524" layer="91"/>
<wire x1="68.58" y1="266.7" x2="73.66" y2="266.7" width="0.1524" layer="91"/>
<junction x="68.58" y="266.7"/>
<label x="73.66" y="266.7" size="1.27" layer="95" xref="yes"/>
<pinref part="CN204" gate="CN" pin="17"/>
<pinref part="CN204" gate="CN" pin="19"/>
</segment>
<segment>
<pinref part="CN206" gate="CN" pin="17"/>
<wire x1="66.04" y1="185.42" x2="68.58" y2="185.42" width="0.1524" layer="91"/>
<wire x1="68.58" y1="185.42" x2="68.58" y2="187.96" width="0.1524" layer="91"/>
<pinref part="CN206" gate="CN" pin="19"/>
<wire x1="68.58" y1="187.96" x2="66.04" y2="187.96" width="0.1524" layer="91"/>
<wire x1="68.58" y1="187.96" x2="73.66" y2="187.96" width="0.1524" layer="91"/>
<junction x="68.58" y="187.96"/>
<label x="73.66" y="187.96" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="66.04" y1="106.68" x2="68.58" y2="106.68" width="0.1524" layer="91"/>
<wire x1="68.58" y1="106.68" x2="68.58" y2="109.22" width="0.1524" layer="91"/>
<wire x1="68.58" y1="109.22" x2="66.04" y2="109.22" width="0.1524" layer="91"/>
<wire x1="68.58" y1="109.22" x2="73.66" y2="109.22" width="0.1524" layer="91"/>
<junction x="68.58" y="109.22"/>
<label x="73.66" y="109.22" size="1.27" layer="95" xref="yes"/>
<pinref part="CN208" gate="CN" pin="17"/>
<pinref part="CN208" gate="CN" pin="19"/>
</segment>
<segment>
<wire x1="66.04" y1="27.94" x2="68.58" y2="27.94" width="0.1524" layer="91"/>
<wire x1="68.58" y1="27.94" x2="68.58" y2="30.48" width="0.1524" layer="91"/>
<wire x1="68.58" y1="30.48" x2="66.04" y2="30.48" width="0.1524" layer="91"/>
<wire x1="68.58" y1="30.48" x2="73.66" y2="30.48" width="0.1524" layer="91"/>
<junction x="68.58" y="30.48"/>
<label x="73.66" y="30.48" size="1.27" layer="95" xref="yes"/>
<pinref part="CN210" gate="CN" pin="17"/>
<pinref part="CN210" gate="CN" pin="19"/>
</segment>
<segment>
<wire x1="66.04" y1="147.32" x2="68.58" y2="147.32" width="0.1524" layer="91"/>
<wire x1="68.58" y1="147.32" x2="68.58" y2="149.86" width="0.1524" layer="91"/>
<wire x1="68.58" y1="149.86" x2="66.04" y2="149.86" width="0.1524" layer="91"/>
<wire x1="68.58" y1="149.86" x2="73.66" y2="149.86" width="0.1524" layer="91"/>
<junction x="68.58" y="149.86"/>
<label x="73.66" y="149.86" size="1.27" layer="95" xref="yes"/>
<pinref part="CN207" gate="CN" pin="17"/>
<pinref part="CN207" gate="CN" pin="19"/>
</segment>
<segment>
<wire x1="66.04" y1="68.58" x2="68.58" y2="68.58" width="0.1524" layer="91"/>
<wire x1="68.58" y1="68.58" x2="68.58" y2="71.12" width="0.1524" layer="91"/>
<wire x1="68.58" y1="71.12" x2="66.04" y2="71.12" width="0.1524" layer="91"/>
<wire x1="68.58" y1="71.12" x2="73.66" y2="71.12" width="0.1524" layer="91"/>
<junction x="68.58" y="71.12"/>
<label x="73.66" y="71.12" size="1.27" layer="95" xref="yes"/>
<pinref part="CN209" gate="CN" pin="17"/>
<pinref part="CN209" gate="CN" pin="19"/>
</segment>
<segment>
<pinref part="CN211" gate="CN" pin="17"/>
<wire x1="198.12" y1="370.84" x2="200.66" y2="370.84" width="0.1524" layer="91"/>
<wire x1="200.66" y1="370.84" x2="200.66" y2="373.38" width="0.1524" layer="91"/>
<pinref part="CN211" gate="CN" pin="19"/>
<wire x1="200.66" y1="373.38" x2="198.12" y2="373.38" width="0.1524" layer="91"/>
<wire x1="200.66" y1="373.38" x2="205.74" y2="373.38" width="0.1524" layer="91"/>
<junction x="200.66" y="373.38"/>
<label x="205.74" y="373.38" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="198.12" y1="294.64" x2="200.66" y2="294.64" width="0.1524" layer="91"/>
<wire x1="200.66" y1="294.64" x2="200.66" y2="297.18" width="0.1524" layer="91"/>
<wire x1="200.66" y1="297.18" x2="198.12" y2="297.18" width="0.1524" layer="91"/>
<wire x1="200.66" y1="297.18" x2="205.74" y2="297.18" width="0.1524" layer="91"/>
<junction x="200.66" y="297.18"/>
<label x="205.74" y="297.18" size="1.27" layer="95" xref="yes"/>
<pinref part="CN213" gate="CN" pin="17"/>
<pinref part="CN213" gate="CN" pin="19"/>
</segment>
<segment>
<wire x1="198.12" y1="332.74" x2="200.66" y2="332.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="332.74" x2="200.66" y2="335.28" width="0.1524" layer="91"/>
<wire x1="200.66" y1="335.28" x2="198.12" y2="335.28" width="0.1524" layer="91"/>
<wire x1="200.66" y1="335.28" x2="205.74" y2="335.28" width="0.1524" layer="91"/>
<junction x="200.66" y="335.28"/>
<label x="205.74" y="335.28" size="1.27" layer="95" xref="yes"/>
<pinref part="CN212" gate="CN" pin="17"/>
<pinref part="CN212" gate="CN" pin="19"/>
</segment>
<segment>
<wire x1="198.12" y1="256.54" x2="200.66" y2="256.54" width="0.1524" layer="91"/>
<wire x1="200.66" y1="256.54" x2="200.66" y2="259.08" width="0.1524" layer="91"/>
<wire x1="200.66" y1="259.08" x2="198.12" y2="259.08" width="0.1524" layer="91"/>
<wire x1="200.66" y1="259.08" x2="205.74" y2="259.08" width="0.1524" layer="91"/>
<junction x="200.66" y="259.08"/>
<label x="205.74" y="259.08" size="1.27" layer="95" xref="yes"/>
<pinref part="CN214" gate="CN" pin="17"/>
<pinref part="CN214" gate="CN" pin="19"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="18"/>
<wire x1="50.8" y1="373.38" x2="48.26" y2="373.38" width="0.1524" layer="91"/>
<wire x1="48.26" y1="373.38" x2="48.26" y2="375.92" width="0.1524" layer="91"/>
<pinref part="CN201" gate="CN" pin="20"/>
<wire x1="48.26" y1="375.92" x2="50.8" y2="375.92" width="0.1524" layer="91"/>
<wire x1="48.26" y1="375.92" x2="43.18" y2="375.92" width="0.1524" layer="91"/>
<junction x="48.26" y="375.92"/>
<label x="43.18" y="375.92" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="50.8" y1="302.26" x2="48.26" y2="302.26" width="0.1524" layer="91"/>
<wire x1="48.26" y1="302.26" x2="48.26" y2="304.8" width="0.1524" layer="91"/>
<wire x1="48.26" y1="304.8" x2="50.8" y2="304.8" width="0.1524" layer="91"/>
<wire x1="48.26" y1="304.8" x2="43.18" y2="304.8" width="0.1524" layer="91"/>
<junction x="48.26" y="304.8"/>
<label x="43.18" y="304.8" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN203" gate="CN" pin="18"/>
<pinref part="CN203" gate="CN" pin="20"/>
</segment>
<segment>
<wire x1="50.8" y1="226.06" x2="48.26" y2="226.06" width="0.1524" layer="91"/>
<wire x1="48.26" y1="226.06" x2="48.26" y2="228.6" width="0.1524" layer="91"/>
<wire x1="48.26" y1="228.6" x2="50.8" y2="228.6" width="0.1524" layer="91"/>
<wire x1="48.26" y1="228.6" x2="43.18" y2="228.6" width="0.1524" layer="91"/>
<junction x="48.26" y="228.6"/>
<label x="43.18" y="228.6" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN205" gate="CN" pin="18"/>
<pinref part="CN205" gate="CN" pin="20"/>
</segment>
<segment>
<wire x1="50.8" y1="337.82" x2="48.26" y2="337.82" width="0.1524" layer="91"/>
<wire x1="48.26" y1="337.82" x2="48.26" y2="340.36" width="0.1524" layer="91"/>
<wire x1="48.26" y1="340.36" x2="50.8" y2="340.36" width="0.1524" layer="91"/>
<wire x1="48.26" y1="340.36" x2="43.18" y2="340.36" width="0.1524" layer="91"/>
<junction x="48.26" y="340.36"/>
<label x="43.18" y="340.36" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="18"/>
<pinref part="CN202" gate="CN" pin="20"/>
</segment>
<segment>
<wire x1="50.8" y1="264.16" x2="48.26" y2="264.16" width="0.1524" layer="91"/>
<wire x1="48.26" y1="264.16" x2="48.26" y2="266.7" width="0.1524" layer="91"/>
<wire x1="48.26" y1="266.7" x2="50.8" y2="266.7" width="0.1524" layer="91"/>
<wire x1="48.26" y1="266.7" x2="43.18" y2="266.7" width="0.1524" layer="91"/>
<junction x="48.26" y="266.7"/>
<label x="43.18" y="266.7" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="18"/>
<pinref part="CN204" gate="CN" pin="20"/>
</segment>
<segment>
<pinref part="CN206" gate="CN" pin="18"/>
<wire x1="50.8" y1="185.42" x2="48.26" y2="185.42" width="0.1524" layer="91"/>
<wire x1="48.26" y1="185.42" x2="48.26" y2="187.96" width="0.1524" layer="91"/>
<pinref part="CN206" gate="CN" pin="20"/>
<wire x1="48.26" y1="187.96" x2="50.8" y2="187.96" width="0.1524" layer="91"/>
<wire x1="48.26" y1="187.96" x2="43.18" y2="187.96" width="0.1524" layer="91"/>
<junction x="48.26" y="187.96"/>
<label x="43.18" y="187.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="50.8" y1="106.68" x2="48.26" y2="106.68" width="0.1524" layer="91"/>
<wire x1="48.26" y1="106.68" x2="48.26" y2="109.22" width="0.1524" layer="91"/>
<wire x1="48.26" y1="109.22" x2="50.8" y2="109.22" width="0.1524" layer="91"/>
<wire x1="48.26" y1="109.22" x2="43.18" y2="109.22" width="0.1524" layer="91"/>
<junction x="48.26" y="109.22"/>
<label x="43.18" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="18"/>
<pinref part="CN208" gate="CN" pin="20"/>
</segment>
<segment>
<wire x1="50.8" y1="27.94" x2="48.26" y2="27.94" width="0.1524" layer="91"/>
<wire x1="48.26" y1="27.94" x2="48.26" y2="30.48" width="0.1524" layer="91"/>
<wire x1="48.26" y1="30.48" x2="50.8" y2="30.48" width="0.1524" layer="91"/>
<wire x1="48.26" y1="30.48" x2="43.18" y2="30.48" width="0.1524" layer="91"/>
<junction x="48.26" y="30.48"/>
<label x="43.18" y="30.48" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN210" gate="CN" pin="18"/>
<pinref part="CN210" gate="CN" pin="20"/>
</segment>
<segment>
<wire x1="50.8" y1="147.32" x2="48.26" y2="147.32" width="0.1524" layer="91"/>
<wire x1="48.26" y1="147.32" x2="48.26" y2="149.86" width="0.1524" layer="91"/>
<wire x1="48.26" y1="149.86" x2="50.8" y2="149.86" width="0.1524" layer="91"/>
<wire x1="48.26" y1="149.86" x2="43.18" y2="149.86" width="0.1524" layer="91"/>
<junction x="48.26" y="149.86"/>
<label x="43.18" y="149.86" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN207" gate="CN" pin="18"/>
<pinref part="CN207" gate="CN" pin="20"/>
</segment>
<segment>
<wire x1="50.8" y1="68.58" x2="48.26" y2="68.58" width="0.1524" layer="91"/>
<wire x1="48.26" y1="68.58" x2="48.26" y2="71.12" width="0.1524" layer="91"/>
<wire x1="48.26" y1="71.12" x2="50.8" y2="71.12" width="0.1524" layer="91"/>
<wire x1="48.26" y1="71.12" x2="43.18" y2="71.12" width="0.1524" layer="91"/>
<junction x="48.26" y="71.12"/>
<label x="43.18" y="71.12" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN209" gate="CN" pin="18"/>
<pinref part="CN209" gate="CN" pin="20"/>
</segment>
<segment>
<pinref part="CN211" gate="CN" pin="18"/>
<wire x1="182.88" y1="370.84" x2="180.34" y2="370.84" width="0.1524" layer="91"/>
<wire x1="180.34" y1="370.84" x2="180.34" y2="373.38" width="0.1524" layer="91"/>
<pinref part="CN211" gate="CN" pin="20"/>
<wire x1="180.34" y1="373.38" x2="182.88" y2="373.38" width="0.1524" layer="91"/>
<wire x1="180.34" y1="373.38" x2="175.26" y2="373.38" width="0.1524" layer="91"/>
<junction x="180.34" y="373.38"/>
<label x="175.26" y="373.38" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="182.88" y1="294.64" x2="180.34" y2="294.64" width="0.1524" layer="91"/>
<wire x1="180.34" y1="294.64" x2="180.34" y2="297.18" width="0.1524" layer="91"/>
<wire x1="180.34" y1="297.18" x2="182.88" y2="297.18" width="0.1524" layer="91"/>
<wire x1="180.34" y1="297.18" x2="175.26" y2="297.18" width="0.1524" layer="91"/>
<junction x="180.34" y="297.18"/>
<label x="175.26" y="297.18" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN213" gate="CN" pin="18"/>
<pinref part="CN213" gate="CN" pin="20"/>
</segment>
<segment>
<wire x1="182.88" y1="332.74" x2="180.34" y2="332.74" width="0.1524" layer="91"/>
<wire x1="180.34" y1="332.74" x2="180.34" y2="335.28" width="0.1524" layer="91"/>
<wire x1="180.34" y1="335.28" x2="182.88" y2="335.28" width="0.1524" layer="91"/>
<wire x1="180.34" y1="335.28" x2="175.26" y2="335.28" width="0.1524" layer="91"/>
<junction x="180.34" y="335.28"/>
<label x="175.26" y="335.28" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="18"/>
<pinref part="CN212" gate="CN" pin="20"/>
</segment>
<segment>
<wire x1="182.88" y1="256.54" x2="180.34" y2="256.54" width="0.1524" layer="91"/>
<wire x1="180.34" y1="256.54" x2="180.34" y2="259.08" width="0.1524" layer="91"/>
<wire x1="180.34" y1="259.08" x2="182.88" y2="259.08" width="0.1524" layer="91"/>
<wire x1="180.34" y1="259.08" x2="175.26" y2="259.08" width="0.1524" layer="91"/>
<junction x="180.34" y="259.08"/>
<label x="175.26" y="259.08" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="18"/>
<pinref part="CN214" gate="CN" pin="20"/>
</segment>
</net>
<net name="SIG_IN_PD10_LEFT_UP" class="0">
<segment>
<wire x1="66.04" y1="66.04" x2="73.66" y2="66.04" width="0.1524" layer="91"/>
<label x="73.66" y="66.04" size="1.27" layer="95" xref="yes"/>
<pinref part="CN209" gate="CN" pin="15"/>
</segment>
</net>
<net name="SIG_IN_SP1_SHAKER_UP" class="0">
<segment>
<wire x1="182.88" y1="330.2" x2="175.26" y2="330.2" width="0.1524" layer="91"/>
<label x="175.26" y="330.2" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="16"/>
</segment>
</net>
<net name="SIG_IN_SP1_PORT_EXTENDED" class="0">
<segment>
<wire x1="175.26" y1="350.52" x2="182.88" y2="350.52" width="0.1524" layer="91"/>
<label x="175.26" y="350.52" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN211" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_IN_SP1_PORT_RETRACTED" class="0">
<segment>
<wire x1="182.88" y1="353.06" x2="175.26" y2="353.06" width="0.1524" layer="91"/>
<label x="175.26" y="353.06" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN211" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_IN_SP1_CHEST_CLOSED" class="0">
<segment>
<wire x1="182.88" y1="355.6" x2="175.26" y2="355.6" width="0.1524" layer="91"/>
<label x="175.26" y="355.6" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN211" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_IN_SP1_PRESSURE2" class="0">
<segment>
<wire x1="182.88" y1="368.3" x2="175.26" y2="368.3" width="0.1524" layer="91"/>
<label x="175.26" y="368.3" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN211" gate="CN" pin="16"/>
</segment>
</net>
<net name="SIG_IN_SP1_BOX1" class="0">
<segment>
<wire x1="182.88" y1="358.14" x2="175.26" y2="358.14" width="0.1524" layer="91"/>
<label x="175.26" y="358.14" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN211" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_IN_SP1_BOX2" class="0">
<segment>
<wire x1="182.88" y1="360.68" x2="175.26" y2="360.68" width="0.1524" layer="91"/>
<label x="175.26" y="360.68" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN211" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_IN_SP1_PRESSURE1" class="0">
<segment>
<wire x1="182.88" y1="365.76" x2="175.26" y2="365.76" width="0.1524" layer="91"/>
<label x="175.26" y="365.76" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN211" gate="CN" pin="14"/>
</segment>
</net>
<net name="SIG_IN_SP1_CURTAIN" class="0">
<segment>
<wire x1="182.88" y1="363.22" x2="175.26" y2="363.22" width="0.1524" layer="91"/>
<label x="175.26" y="363.22" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN211" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_IN_OB1_NORMAL_LEFT" class="0">
<segment>
<wire x1="182.88" y1="314.96" x2="175.26" y2="314.96" width="0.1524" layer="91"/>
<label x="175.26" y="314.96" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_IN_OB1_NORMAL_RIGHT" class="0">
<segment>
<wire x1="198.12" y1="312.42" x2="205.74" y2="312.42" width="0.1524" layer="91"/>
<label x="205.74" y="312.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_IN_OB1_ROTATED_LEFT" class="0">
<segment>
<wire x1="182.88" y1="312.42" x2="175.26" y2="312.42" width="0.1524" layer="91"/>
<label x="175.26" y="312.42" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_IN_OB1_ROTATED_RIGHT" class="0">
<segment>
<wire x1="198.12" y1="314.96" x2="205.74" y2="314.96" width="0.1524" layer="91"/>
<label x="205.74" y="314.96" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_OUT_OB1_VALVE_EXTEND" class="0">
<segment>
<wire x1="182.88" y1="274.32" x2="175.26" y2="274.32" width="0.1524" layer="91"/>
<label x="175.26" y="274.32" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN213" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_OUT_OB1_VALVE_RETRACT" class="0">
<segment>
<wire x1="182.88" y1="279.4" x2="175.26" y2="279.4" width="0.1524" layer="91"/>
<label x="175.26" y="279.4" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN213" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_IN_OB1_LOCKED" class="0">
<segment>
<wire x1="198.12" y1="317.5" x2="205.74" y2="317.5" width="0.1524" layer="91"/>
<label x="205.74" y="317.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_IN_OB1_UNLOCKED" class="0">
<segment>
<wire x1="198.12" y1="320.04" x2="205.74" y2="320.04" width="0.1524" layer="91"/>
<label x="205.74" y="320.04" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_IN_OB1_BLOCK_UP_LEFT" class="0">
<segment>
<wire x1="198.12" y1="322.58" x2="205.74" y2="322.58" width="0.1524" layer="91"/>
<label x="205.74" y="322.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_IN_OB1_BLOCK_DOWN_LEFT" class="0">
<segment>
<wire x1="198.12" y1="325.12" x2="205.74" y2="325.12" width="0.1524" layer="91"/>
<label x="205.74" y="325.12" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_IN_OB1_BLOCK_UP_RIGHT" class="0">
<segment>
<wire x1="198.12" y1="327.66" x2="205.74" y2="327.66" width="0.1524" layer="91"/>
<label x="205.74" y="327.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="13"/>
</segment>
</net>
<net name="SIG_IN_OB1_BLOCK_DOWN_RIGHT" class="0">
<segment>
<wire x1="198.12" y1="330.2" x2="205.74" y2="330.2" width="0.1524" layer="91"/>
<label x="205.74" y="330.2" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="15"/>
</segment>
</net>
<net name="SIG_OUT_OB1_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="182.88" y1="284.48" x2="175.26" y2="284.48" width="0.1524" layer="91"/>
<label x="175.26" y="284.48" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN213" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_OUT_OB1_VALVE_LOCK" class="0">
<segment>
<wire x1="182.88" y1="289.56" x2="175.26" y2="289.56" width="0.1524" layer="91"/>
<label x="175.26" y="289.56" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN213" gate="CN" pin="14"/>
</segment>
</net>
<net name="SIG_IN_SP2_BOX1" class="0">
<segment>
<wire x1="198.12" y1="358.14" x2="205.74" y2="358.14" width="0.1524" layer="91"/>
<label x="205.74" y="358.14" size="1.27" layer="95" xref="yes"/>
<pinref part="CN211" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_IN_SP2_BOX2" class="0">
<segment>
<wire x1="198.12" y1="360.68" x2="205.74" y2="360.68" width="0.1524" layer="91"/>
<label x="205.74" y="360.68" size="1.27" layer="95" xref="yes"/>
<pinref part="CN211" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_IN_SP2_CHEST_CLOSED" class="0">
<segment>
<wire x1="198.12" y1="355.6" x2="205.74" y2="355.6" width="0.1524" layer="91"/>
<label x="205.74" y="355.6" size="1.27" layer="95" xref="yes"/>
<pinref part="CN211" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_IN_SP2_CURTAIN" class="0">
<segment>
<wire x1="198.12" y1="363.22" x2="205.74" y2="363.22" width="0.1524" layer="91"/>
<label x="205.74" y="363.22" size="1.27" layer="95" xref="yes"/>
<pinref part="CN211" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_IN_SP2_PORT_EXTENDED" class="0">
<segment>
<wire x1="205.74" y1="350.52" x2="198.12" y2="350.52" width="0.1524" layer="91"/>
<label x="205.74" y="350.52" size="1.27" layer="95" xref="yes"/>
<pinref part="CN211" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_IN_SP2_PORT_RETRACTED" class="0">
<segment>
<wire x1="198.12" y1="353.06" x2="205.74" y2="353.06" width="0.1524" layer="91"/>
<label x="205.74" y="353.06" size="1.27" layer="95" xref="yes"/>
<pinref part="CN211" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_IN_SP2_PRESSURE1" class="0">
<segment>
<wire x1="198.12" y1="365.76" x2="205.74" y2="365.76" width="0.1524" layer="91"/>
<label x="205.74" y="365.76" size="1.27" layer="95" xref="yes"/>
<pinref part="CN211" gate="CN" pin="13"/>
</segment>
</net>
<net name="SIG_IN_SP2_PRESSURE2" class="0">
<segment>
<wire x1="198.12" y1="368.3" x2="205.74" y2="368.3" width="0.1524" layer="91"/>
<label x="205.74" y="368.3" size="1.27" layer="95" xref="yes"/>
<pinref part="CN211" gate="CN" pin="15"/>
</segment>
</net>
<net name="SIG_IN_SP2_SHAKER_UP" class="0">
<segment>
<wire x1="182.88" y1="317.5" x2="175.26" y2="317.5" width="0.1524" layer="91"/>
<label x="175.26" y="317.5" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_OUT_SP2_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="182.88" y1="292.1" x2="175.26" y2="292.1" width="0.1524" layer="91"/>
<label x="175.26" y="292.1" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN213" gate="CN" pin="16"/>
</segment>
</net>
<net name="SIG_OUT_SP2_VALVE11" class="0">
<segment>
<wire x1="182.88" y1="276.86" x2="175.26" y2="276.86" width="0.1524" layer="91"/>
<label x="175.26" y="276.86" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN213" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_OUT_SP2_VALVE12" class="0">
<segment>
<wire x1="182.88" y1="281.94" x2="175.26" y2="281.94" width="0.1524" layer="91"/>
<label x="175.26" y="281.94" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN213" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_OUT_SP2_VALVE21" class="0">
<segment>
<wire x1="182.88" y1="287.02" x2="175.26" y2="287.02" width="0.1524" layer="91"/>
<label x="175.26" y="287.02" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN213" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_IN_OB2_BLOCK_DOWN_LEFT" class="0">
<segment>
<wire x1="182.88" y1="248.92" x2="175.26" y2="248.92" width="0.1524" layer="91"/>
<label x="175.26" y="248.92" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_IN_OB2_BLOCK_DOWN_RIGHT" class="0">
<segment>
<wire x1="182.88" y1="254" x2="175.26" y2="254" width="0.1524" layer="91"/>
<label x="175.26" y="254" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="16"/>
</segment>
</net>
<net name="SIG_IN_OB2_BLOCK_UP_LEFT" class="0">
<segment>
<wire x1="182.88" y1="246.38" x2="175.26" y2="246.38" width="0.1524" layer="91"/>
<label x="175.26" y="246.38" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_IN_OB2_BLOCK_UP_RIGHT" class="0">
<segment>
<wire x1="182.88" y1="251.46" x2="175.26" y2="251.46" width="0.1524" layer="91"/>
<label x="175.26" y="251.46" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="14"/>
</segment>
</net>
<net name="SIG_IN_OB2_LOCKED" class="0">
<segment>
<wire x1="182.88" y1="241.3" x2="175.26" y2="241.3" width="0.1524" layer="91"/>
<label x="175.26" y="241.3" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_IN_OB2_NORMAL_LEFT" class="0">
<segment>
<wire x1="182.88" y1="320.04" x2="175.26" y2="320.04" width="0.1524" layer="91"/>
<label x="175.26" y="320.04" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_IN_OB2_NORMAL_RIGHT" class="0">
<segment>
<wire x1="182.88" y1="236.22" x2="175.26" y2="236.22" width="0.1524" layer="91"/>
<label x="175.26" y="236.22" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_IN_OB2_ROTATED_LEFT" class="0">
<segment>
<wire x1="182.88" y1="325.12" x2="175.26" y2="325.12" width="0.1524" layer="91"/>
<label x="175.26" y="325.12" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_IN_OB2_ROTATED_RIGHT" class="0">
<segment>
<wire x1="182.88" y1="238.76" x2="175.26" y2="238.76" width="0.1524" layer="91"/>
<label x="175.26" y="238.76" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_IN_OB2_UNLOCKED" class="0">
<segment>
<wire x1="182.88" y1="243.84" x2="175.26" y2="243.84" width="0.1524" layer="91"/>
<label x="175.26" y="243.84" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_OUT_OB2_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="198.12" y1="284.48" x2="205.74" y2="284.48" width="0.1524" layer="91"/>
<label x="205.74" y="284.48" size="1.27" layer="95" xref="yes"/>
<pinref part="CN213" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_OUT_OB2_VALVE_EXTEND" class="0">
<segment>
<wire x1="198.12" y1="274.32" x2="205.74" y2="274.32" width="0.1524" layer="91"/>
<label x="205.74" y="274.32" size="1.27" layer="95" xref="yes"/>
<pinref part="CN213" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_OUT_OB2_VALVE_LOCK" class="0">
<segment>
<wire x1="198.12" y1="289.56" x2="205.74" y2="289.56" width="0.1524" layer="91"/>
<label x="205.74" y="289.56" size="1.27" layer="95" xref="yes"/>
<pinref part="CN213" gate="CN" pin="13"/>
</segment>
</net>
<net name="SIG_OUT_OB2_VALVE_RETRACT" class="0">
<segment>
<wire x1="198.12" y1="279.4" x2="205.74" y2="279.4" width="0.1524" layer="91"/>
<label x="205.74" y="279.4" size="1.27" layer="95" xref="yes"/>
<pinref part="CN213" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_IN_OB3_BLOCK_DOWN_LEFT" class="0">
<segment>
<wire x1="198.12" y1="248.92" x2="205.74" y2="248.92" width="0.1524" layer="91"/>
<label x="205.74" y="248.92" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_IN_OB3_BLOCK_DOWN_RIGHT" class="0">
<segment>
<wire x1="198.12" y1="254" x2="205.74" y2="254" width="0.1524" layer="91"/>
<label x="205.74" y="254" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="15"/>
</segment>
</net>
<net name="SIG_IN_OB3_BLOCK_UP_LEFT" class="0">
<segment>
<wire x1="198.12" y1="246.38" x2="205.74" y2="246.38" width="0.1524" layer="91"/>
<label x="205.74" y="246.38" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_IN_OB3_BLOCK_UP_RIGHT" class="0">
<segment>
<wire x1="198.12" y1="251.46" x2="205.74" y2="251.46" width="0.1524" layer="91"/>
<label x="205.74" y="251.46" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="13"/>
</segment>
</net>
<net name="SIG_IN_OB3_LOCKED" class="0">
<segment>
<wire x1="198.12" y1="241.3" x2="205.74" y2="241.3" width="0.1524" layer="91"/>
<label x="205.74" y="241.3" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_IN_OB3_NORMAL_LEFT" class="0">
<segment>
<wire x1="182.88" y1="322.58" x2="175.26" y2="322.58" width="0.1524" layer="91"/>
<label x="175.26" y="322.58" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_IN_OB3_NORMAL_RIGHT" class="0">
<segment>
<wire x1="198.12" y1="236.22" x2="205.74" y2="236.22" width="0.1524" layer="91"/>
<label x="205.74" y="236.22" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_IN_OB3_ROTATED_LEFT" class="0">
<segment>
<wire x1="182.88" y1="327.66" x2="175.26" y2="327.66" width="0.1524" layer="91"/>
<label x="175.26" y="327.66" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN212" gate="CN" pin="14"/>
</segment>
</net>
<net name="SIG_IN_OB3_ROTATED_RIGHT" class="0">
<segment>
<wire x1="198.12" y1="238.76" x2="205.74" y2="238.76" width="0.1524" layer="91"/>
<label x="205.74" y="238.76" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_IN_OB3_UNLOCKED" class="0">
<segment>
<wire x1="198.12" y1="243.84" x2="205.74" y2="243.84" width="0.1524" layer="91"/>
<label x="205.74" y="243.84" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN214" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_OUT_OB3_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="198.12" y1="287.02" x2="205.74" y2="287.02" width="0.1524" layer="91"/>
<label x="205.74" y="287.02" size="1.27" layer="95" xref="yes"/>
<pinref part="CN213" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_OUT_OB3_VALVE_EXTEND" class="0">
<segment>
<wire x1="198.12" y1="276.86" x2="205.74" y2="276.86" width="0.1524" layer="91"/>
<label x="205.74" y="276.86" size="1.27" layer="95" xref="yes"/>
<pinref part="CN213" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_OUT_OB3_VALVE_LOCK" class="0">
<segment>
<wire x1="198.12" y1="292.1" x2="205.74" y2="292.1" width="0.1524" layer="91"/>
<label x="205.74" y="292.1" size="1.27" layer="95" xref="yes"/>
<pinref part="CN213" gate="CN" pin="15"/>
</segment>
</net>
<net name="SIG_OUT_OB3_VALVE_RETRACT" class="0">
<segment>
<wire x1="198.12" y1="281.94" x2="205.74" y2="281.94" width="0.1524" layer="91"/>
<label x="205.74" y="281.94" size="1.27" layer="95" xref="yes"/>
<pinref part="CN213" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_IN_PSU1_OK" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="1"/>
<wire x1="66.04" y1="353.06" x2="73.66" y2="353.06" width="0.1524" layer="91"/>
<label x="73.66" y="353.06" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU3_OK" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="3"/>
<wire x1="66.04" y1="355.6" x2="73.66" y2="355.6" width="0.1524" layer="91"/>
<label x="73.66" y="355.6" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU2_OK" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="2"/>
<wire x1="50.8" y1="353.06" x2="43.18" y2="353.06" width="0.1524" layer="91"/>
<label x="43.18" y="353.06" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU4_OK" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="4"/>
<wire x1="50.8" y1="355.6" x2="43.18" y2="355.6" width="0.1524" layer="91"/>
<label x="43.18" y="355.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU1_S2" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="5"/>
<wire x1="66.04" y1="358.14" x2="73.66" y2="358.14" width="0.1524" layer="91"/>
<label x="73.66" y="358.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU2_S2" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="7"/>
<wire x1="66.04" y1="360.68" x2="73.66" y2="360.68" width="0.1524" layer="91"/>
<label x="73.66" y="360.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU3_S2" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="9"/>
<wire x1="66.04" y1="363.22" x2="73.66" y2="363.22" width="0.1524" layer="91"/>
<label x="73.66" y="363.22" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU4_S2" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="11"/>
<wire x1="66.04" y1="365.76" x2="73.66" y2="365.76" width="0.1524" layer="91"/>
<label x="73.66" y="365.76" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU1_S3" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="6"/>
<wire x1="50.8" y1="358.14" x2="43.18" y2="358.14" width="0.1524" layer="91"/>
<label x="43.18" y="358.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU2_S3" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="8"/>
<wire x1="50.8" y1="360.68" x2="43.18" y2="360.68" width="0.1524" layer="91"/>
<label x="43.18" y="360.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU3_S3" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="10"/>
<wire x1="50.8" y1="363.22" x2="43.18" y2="363.22" width="0.1524" layer="91"/>
<label x="43.18" y="363.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_PSU4_S3" class="0">
<segment>
<pinref part="CN201" gate="CN" pin="12"/>
<wire x1="50.8" y1="365.76" x2="43.18" y2="365.76" width="0.1524" layer="91"/>
<label x="43.18" y="365.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_PD1_BLUE" class="0">
<segment>
<wire x1="50.8" y1="335.28" x2="43.18" y2="335.28" width="0.1524" layer="91"/>
<label x="43.18" y="335.28" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="16"/>
</segment>
</net>
<net name="SIG_OUT_PD1_RED" class="0">
<segment>
<wire x1="50.8" y1="332.74" x2="43.18" y2="332.74" width="0.1524" layer="91"/>
<label x="43.18" y="332.74" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="14"/>
</segment>
</net>
<net name="SIG_OUT_PD1_GREEN" class="0">
<segment>
<wire x1="50.8" y1="330.2" x2="43.18" y2="330.2" width="0.1524" layer="91"/>
<label x="43.18" y="330.2" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_OUT_PD1_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="50.8" y1="327.66" x2="43.18" y2="327.66" width="0.1524" layer="91"/>
<label x="43.18" y="327.66" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_OUT_PD1_VALVE_DOWN" class="0">
<segment>
<wire x1="50.8" y1="325.12" x2="43.18" y2="325.12" width="0.1524" layer="91"/>
<label x="43.18" y="325.12" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_OUT_PD1_VALVE_UP" class="0">
<segment>
<wire x1="50.8" y1="322.58" x2="43.18" y2="322.58" width="0.1524" layer="91"/>
<label x="43.18" y="322.58" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_OUT_PD1_UNLOCK" class="0">
<segment>
<wire x1="50.8" y1="320.04" x2="43.18" y2="320.04" width="0.1524" layer="91"/>
<label x="43.18" y="320.04" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_OUT_PSU1_S1" class="0">
<segment>
<wire x1="50.8" y1="317.5" x2="43.18" y2="317.5" width="0.1524" layer="91"/>
<label x="43.18" y="317.5" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_OUT_PD2_BLUE" class="0">
<segment>
<wire x1="66.04" y1="335.28" x2="73.66" y2="335.28" width="0.1524" layer="91"/>
<label x="73.66" y="335.28" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="15"/>
</segment>
</net>
<net name="SIG_OUT_PD2_GREEN" class="0">
<segment>
<wire x1="66.04" y1="330.2" x2="73.66" y2="330.2" width="0.1524" layer="91"/>
<label x="73.66" y="330.2" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_OUT_PD2_RED" class="0">
<segment>
<wire x1="66.04" y1="332.74" x2="73.66" y2="332.74" width="0.1524" layer="91"/>
<label x="73.66" y="332.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="13"/>
</segment>
</net>
<net name="SIG_OUT_PD2_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="66.04" y1="327.66" x2="73.66" y2="327.66" width="0.1524" layer="91"/>
<label x="73.66" y="327.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_OUT_PD2_UNLOCK" class="0">
<segment>
<wire x1="66.04" y1="320.04" x2="73.66" y2="320.04" width="0.1524" layer="91"/>
<label x="73.66" y="320.04" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_OUT_PD2_VALVE_DOWN" class="0">
<segment>
<wire x1="66.04" y1="325.12" x2="73.66" y2="325.12" width="0.1524" layer="91"/>
<label x="73.66" y="325.12" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_OUT_PD2_VALVE_UP" class="0">
<segment>
<wire x1="66.04" y1="322.58" x2="73.66" y2="322.58" width="0.1524" layer="91"/>
<label x="73.66" y="322.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_OUT_PSU2_S1" class="0">
<segment>
<wire x1="66.04" y1="317.5" x2="73.66" y2="317.5" width="0.1524" layer="91"/>
<label x="73.66" y="317.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN202" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_OUT_PSU3_S1" class="0">
<segment>
<wire x1="50.8" y1="243.84" x2="43.18" y2="243.84" width="0.1524" layer="91"/>
<label x="43.18" y="243.84" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_OUT_PSU4_S1" class="0">
<segment>
<wire x1="66.04" y1="243.84" x2="73.66" y2="243.84" width="0.1524" layer="91"/>
<label x="73.66" y="243.84" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_OUT_PD3_BLUE" class="0">
<segment>
<wire x1="50.8" y1="261.62" x2="43.18" y2="261.62" width="0.1524" layer="91"/>
<label x="43.18" y="261.62" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="16"/>
</segment>
</net>
<net name="SIG_OUT_PD3_GREEN" class="0">
<segment>
<wire x1="50.8" y1="256.54" x2="43.18" y2="256.54" width="0.1524" layer="91"/>
<label x="43.18" y="256.54" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_OUT_PD3_RED" class="0">
<segment>
<wire x1="50.8" y1="259.08" x2="43.18" y2="259.08" width="0.1524" layer="91"/>
<label x="43.18" y="259.08" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="14"/>
</segment>
</net>
<net name="SIG_OUT_PD3_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="50.8" y1="254" x2="43.18" y2="254" width="0.1524" layer="91"/>
<label x="43.18" y="254" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_OUT_PD3_UNLOCK" class="0">
<segment>
<wire x1="50.8" y1="246.38" x2="43.18" y2="246.38" width="0.1524" layer="91"/>
<label x="43.18" y="246.38" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_OUT_PD3_VALVE_DOWN" class="0">
<segment>
<wire x1="50.8" y1="251.46" x2="43.18" y2="251.46" width="0.1524" layer="91"/>
<label x="43.18" y="251.46" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_OUT_PD3_VALVE_UP" class="0">
<segment>
<wire x1="50.8" y1="248.92" x2="43.18" y2="248.92" width="0.1524" layer="91"/>
<label x="43.18" y="248.92" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_OUT_PD4_BLUE" class="0">
<segment>
<wire x1="66.04" y1="261.62" x2="73.66" y2="261.62" width="0.1524" layer="91"/>
<label x="73.66" y="261.62" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="15"/>
</segment>
</net>
<net name="SIG_OUT_PD4_GREEN" class="0">
<segment>
<wire x1="66.04" y1="256.54" x2="73.66" y2="256.54" width="0.1524" layer="91"/>
<label x="73.66" y="256.54" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_OUT_PD4_RED" class="0">
<segment>
<wire x1="66.04" y1="259.08" x2="73.66" y2="259.08" width="0.1524" layer="91"/>
<label x="73.66" y="259.08" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="13"/>
</segment>
</net>
<net name="SIG_OUT_PD4_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="66.04" y1="254" x2="73.66" y2="254" width="0.1524" layer="91"/>
<label x="73.66" y="254" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_OUT_PD4_UNLOCK" class="0">
<segment>
<wire x1="66.04" y1="246.38" x2="73.66" y2="246.38" width="0.1524" layer="91"/>
<label x="73.66" y="246.38" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_OUT_PD4_VALVE_DOWN" class="0">
<segment>
<wire x1="66.04" y1="251.46" x2="73.66" y2="251.46" width="0.1524" layer="91"/>
<label x="73.66" y="251.46" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_OUT_PD4_VALVE_UP" class="0">
<segment>
<wire x1="66.04" y1="248.92" x2="73.66" y2="248.92" width="0.1524" layer="91"/>
<label x="73.66" y="248.92" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN204" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_IN_PD1_LEFT_UP" class="0">
<segment>
<wire x1="50.8" y1="294.64" x2="43.18" y2="294.64" width="0.1524" layer="91"/>
<label x="43.18" y="294.64" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN203" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_IN_PD2_LEFT_UP" class="0">
<segment>
<wire x1="50.8" y1="292.1" x2="43.18" y2="292.1" width="0.1524" layer="91"/>
<label x="43.18" y="292.1" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN203" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_IN_PD1_LEFT_DOWN" class="0">
<segment>
<wire x1="50.8" y1="289.56" x2="43.18" y2="289.56" width="0.1524" layer="91"/>
<label x="43.18" y="289.56" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN203" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_IN_PD2_LEFT_DOWN" class="0">
<segment>
<wire x1="50.8" y1="287.02" x2="43.18" y2="287.02" width="0.1524" layer="91"/>
<label x="43.18" y="287.02" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN203" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_IN_PD1_RIGHT_UP" class="0">
<segment>
<wire x1="50.8" y1="284.48" x2="43.18" y2="284.48" width="0.1524" layer="91"/>
<label x="43.18" y="284.48" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN203" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_IN_PD2_RIGHT_UP" class="0">
<segment>
<wire x1="50.8" y1="281.94" x2="43.18" y2="281.94" width="0.1524" layer="91"/>
<label x="43.18" y="281.94" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN203" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_IN_PD1_RIGHT_DOWN" class="0">
<segment>
<wire x1="66.04" y1="281.94" x2="73.66" y2="281.94" width="0.1524" layer="91"/>
<label x="73.66" y="281.94" size="1.27" layer="95" xref="yes"/>
<pinref part="CN203" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_IN_PD2_RIGHT_DOWN" class="0">
<segment>
<wire x1="66.04" y1="284.48" x2="73.66" y2="284.48" width="0.1524" layer="91"/>
<label x="73.66" y="284.48" size="1.27" layer="95" xref="yes"/>
<pinref part="CN203" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_IN_PD1_CURTAIN" class="0">
<segment>
<wire x1="66.04" y1="287.02" x2="73.66" y2="287.02" width="0.1524" layer="91"/>
<label x="73.66" y="287.02" size="1.27" layer="95" xref="yes"/>
<pinref part="CN203" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_IN_PD2_CURTAIN" class="0">
<segment>
<wire x1="66.04" y1="289.56" x2="73.66" y2="289.56" width="0.1524" layer="91"/>
<label x="73.66" y="289.56" size="1.27" layer="95" xref="yes"/>
<pinref part="CN203" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_IN_PD1_OPEN" class="0">
<segment>
<wire x1="66.04" y1="292.1" x2="73.66" y2="292.1" width="0.1524" layer="91"/>
<label x="73.66" y="292.1" size="1.27" layer="95" xref="yes"/>
<pinref part="CN203" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_IN_PD2_OPEN" class="0">
<segment>
<wire x1="66.04" y1="294.64" x2="73.66" y2="294.64" width="0.1524" layer="91"/>
<label x="73.66" y="294.64" size="1.27" layer="95" xref="yes"/>
<pinref part="CN203" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_OUT_PD6_BLUE" class="0">
<segment>
<wire x1="50.8" y1="182.88" x2="43.18" y2="182.88" width="0.1524" layer="91"/>
<label x="43.18" y="182.88" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="16"/>
</segment>
</net>
<net name="SIG_OUT_PD6_GREEN" class="0">
<segment>
<wire x1="50.8" y1="177.8" x2="43.18" y2="177.8" width="0.1524" layer="91"/>
<label x="43.18" y="177.8" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_OUT_PD6_RED" class="0">
<segment>
<wire x1="50.8" y1="180.34" x2="43.18" y2="180.34" width="0.1524" layer="91"/>
<label x="43.18" y="180.34" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="14"/>
</segment>
</net>
<net name="SIG_OUT_PD6_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="50.8" y1="175.26" x2="43.18" y2="175.26" width="0.1524" layer="91"/>
<label x="43.18" y="175.26" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_OUT_PD6_UNLOCK" class="0">
<segment>
<wire x1="50.8" y1="167.64" x2="43.18" y2="167.64" width="0.1524" layer="91"/>
<label x="43.18" y="167.64" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_OUT_PD6_VALVE_DOWN" class="0">
<segment>
<wire x1="50.8" y1="172.72" x2="43.18" y2="172.72" width="0.1524" layer="91"/>
<label x="43.18" y="172.72" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_OUT_PD6_VALVE_UP" class="0">
<segment>
<wire x1="50.8" y1="170.18" x2="43.18" y2="170.18" width="0.1524" layer="91"/>
<label x="43.18" y="170.18" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_OUT_PD7_BLUE" class="0">
<segment>
<wire x1="66.04" y1="182.88" x2="73.66" y2="182.88" width="0.1524" layer="91"/>
<label x="73.66" y="182.88" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="15"/>
</segment>
</net>
<net name="SIG_OUT_PD7_GREEN" class="0">
<segment>
<wire x1="66.04" y1="177.8" x2="73.66" y2="177.8" width="0.1524" layer="91"/>
<label x="73.66" y="177.8" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_OUT_PD7_RED" class="0">
<segment>
<wire x1="66.04" y1="180.34" x2="73.66" y2="180.34" width="0.1524" layer="91"/>
<label x="73.66" y="180.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="13"/>
</segment>
</net>
<net name="SIG_OUT_PD7_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="66.04" y1="175.26" x2="73.66" y2="175.26" width="0.1524" layer="91"/>
<label x="73.66" y="175.26" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_OUT_PD7_UNLOCK" class="0">
<segment>
<wire x1="66.04" y1="167.64" x2="73.66" y2="167.64" width="0.1524" layer="91"/>
<label x="73.66" y="167.64" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_OUT_PD7_VALVE_DOWN" class="0">
<segment>
<wire x1="66.04" y1="172.72" x2="73.66" y2="172.72" width="0.1524" layer="91"/>
<label x="73.66" y="172.72" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_OUT_PD7_VALVE_UP" class="0">
<segment>
<wire x1="66.04" y1="170.18" x2="73.66" y2="170.18" width="0.1524" layer="91"/>
<label x="73.66" y="170.18" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_OUT_PD5_BLUE" class="0">
<segment>
<wire x1="50.8" y1="165.1" x2="43.18" y2="165.1" width="0.1524" layer="91"/>
<label x="43.18" y="165.1" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_OUT_PD5_RED" class="0">
<segment>
<wire x1="66.04" y1="165.1" x2="73.66" y2="165.1" width="0.1524" layer="91"/>
<label x="73.66" y="165.1" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN206" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_IN_PD3_LEFT_UP" class="0">
<segment>
<wire x1="50.8" y1="223.52" x2="43.18" y2="223.52" width="0.1524" layer="91"/>
<label x="43.18" y="223.52" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN205" gate="CN" pin="16"/>
</segment>
</net>
<net name="SIG_IN_PD4_LEFT_UP" class="0">
<segment>
<wire x1="50.8" y1="220.98" x2="43.18" y2="220.98" width="0.1524" layer="91"/>
<label x="43.18" y="220.98" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN205" gate="CN" pin="14"/>
</segment>
</net>
<net name="SIG_IN_PD3_LEFT_DOWN" class="0">
<segment>
<wire x1="50.8" y1="218.44" x2="43.18" y2="218.44" width="0.1524" layer="91"/>
<label x="43.18" y="218.44" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN205" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_IN_PD4_LEFT_DOWN" class="0">
<segment>
<wire x1="50.8" y1="215.9" x2="43.18" y2="215.9" width="0.1524" layer="91"/>
<label x="43.18" y="215.9" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN205" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_IN_PD3_RIGHT_UP" class="0">
<segment>
<wire x1="50.8" y1="213.36" x2="43.18" y2="213.36" width="0.1524" layer="91"/>
<label x="43.18" y="213.36" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN205" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_IN_PD4_RIGHT_UP" class="0">
<segment>
<wire x1="50.8" y1="210.82" x2="43.18" y2="210.82" width="0.1524" layer="91"/>
<label x="43.18" y="210.82" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN205" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_IN_PD3_RIGHT_DOWN" class="0">
<segment>
<wire x1="50.8" y1="208.28" x2="43.18" y2="208.28" width="0.1524" layer="91"/>
<label x="43.18" y="208.28" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN205" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_IN_PD4_RIGHT_DOWN" class="0">
<segment>
<wire x1="50.8" y1="205.74" x2="43.18" y2="205.74" width="0.1524" layer="91"/>
<label x="43.18" y="205.74" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN205" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_IN_PD3_CURTAIN" class="0">
<segment>
<wire x1="66.04" y1="205.74" x2="73.66" y2="205.74" width="0.1524" layer="91"/>
<label x="73.66" y="205.74" size="1.27" layer="95" xref="yes"/>
<pinref part="CN205" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_IN_PD4_CURTAIN" class="0">
<segment>
<wire x1="66.04" y1="208.28" x2="73.66" y2="208.28" width="0.1524" layer="91"/>
<label x="73.66" y="208.28" size="1.27" layer="95" xref="yes"/>
<pinref part="CN205" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_IN_PD3_OPEN" class="0">
<segment>
<wire x1="66.04" y1="210.82" x2="73.66" y2="210.82" width="0.1524" layer="91"/>
<label x="73.66" y="210.82" size="1.27" layer="95" xref="yes"/>
<pinref part="CN205" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_IN_PD4_OPEN" class="0">
<segment>
<wire x1="66.04" y1="213.36" x2="73.66" y2="213.36" width="0.1524" layer="91"/>
<label x="73.66" y="213.36" size="1.27" layer="95" xref="yes"/>
<pinref part="CN205" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_IN_PD5_RIGHT_DOWN" class="0">
<segment>
<wire x1="66.04" y1="215.9" x2="73.66" y2="215.9" width="0.1524" layer="91"/>
<label x="73.66" y="215.9" size="1.27" layer="95" xref="yes"/>
<pinref part="CN205" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_IN_PD5_RIGHT_UP" class="0">
<segment>
<wire x1="66.04" y1="218.44" x2="73.66" y2="218.44" width="0.1524" layer="91"/>
<label x="73.66" y="218.44" size="1.27" layer="95" xref="yes"/>
<pinref part="CN205" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_IN_PD5_LEFT_DOWN" class="0">
<segment>
<wire x1="66.04" y1="220.98" x2="73.66" y2="220.98" width="0.1524" layer="91"/>
<label x="73.66" y="220.98" size="1.27" layer="95" xref="yes"/>
<pinref part="CN205" gate="CN" pin="13"/>
</segment>
</net>
<net name="SIG_IN_PD5_LEFT_UP" class="0">
<segment>
<wire x1="66.04" y1="223.52" x2="73.66" y2="223.52" width="0.1524" layer="91"/>
<label x="73.66" y="223.52" size="1.27" layer="95" xref="yes"/>
<pinref part="CN205" gate="CN" pin="15"/>
</segment>
</net>
<net name="SIG_IN_PD5_CURTAIN" class="0">
<segment>
<wire x1="50.8" y1="144.78" x2="43.18" y2="144.78" width="0.1524" layer="91"/>
<label x="43.18" y="144.78" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN207" gate="CN" pin="16"/>
</segment>
</net>
<net name="SIG_IN_PD5_OPEN" class="0">
<segment>
<wire x1="50.8" y1="142.24" x2="43.18" y2="142.24" width="0.1524" layer="91"/>
<label x="43.18" y="142.24" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN207" gate="CN" pin="14"/>
</segment>
</net>
<net name="SIG_IN_PD6_LEFT_UP" class="0">
<segment>
<wire x1="50.8" y1="139.7" x2="43.18" y2="139.7" width="0.1524" layer="91"/>
<label x="43.18" y="139.7" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN207" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_IN_PD7_LEFT_UP" class="0">
<segment>
<wire x1="50.8" y1="137.16" x2="43.18" y2="137.16" width="0.1524" layer="91"/>
<label x="43.18" y="137.16" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN207" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_IN_PD6_LEFT_DOWN" class="0">
<segment>
<wire x1="50.8" y1="134.62" x2="43.18" y2="134.62" width="0.1524" layer="91"/>
<label x="43.18" y="134.62" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN207" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_IN_PD7_LEFT_DOWN" class="0">
<segment>
<wire x1="50.8" y1="132.08" x2="43.18" y2="132.08" width="0.1524" layer="91"/>
<label x="43.18" y="132.08" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN207" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_IN_PD6_RIGHT_UP" class="0">
<segment>
<wire x1="50.8" y1="129.54" x2="43.18" y2="129.54" width="0.1524" layer="91"/>
<label x="43.18" y="129.54" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN207" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_IN_PD7_RIGHT_UP" class="0">
<segment>
<wire x1="50.8" y1="127" x2="43.18" y2="127" width="0.1524" layer="91"/>
<label x="43.18" y="127" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN207" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_IN_PD6_RIGHT_DOWN" class="0">
<segment>
<wire x1="66.04" y1="127" x2="73.66" y2="127" width="0.1524" layer="91"/>
<label x="73.66" y="127" size="1.27" layer="95" xref="yes"/>
<pinref part="CN207" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_IN_PD7_RIGHT_DOWN" class="0">
<segment>
<wire x1="66.04" y1="129.54" x2="73.66" y2="129.54" width="0.1524" layer="91"/>
<label x="73.66" y="129.54" size="1.27" layer="95" xref="yes"/>
<pinref part="CN207" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_IN_PD6_CURTAIN" class="0">
<segment>
<wire x1="66.04" y1="132.08" x2="73.66" y2="132.08" width="0.1524" layer="91"/>
<label x="73.66" y="132.08" size="1.27" layer="95" xref="yes"/>
<pinref part="CN207" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_IN_PD7_CURTAIN" class="0">
<segment>
<wire x1="66.04" y1="134.62" x2="73.66" y2="134.62" width="0.1524" layer="91"/>
<label x="73.66" y="134.62" size="1.27" layer="95" xref="yes"/>
<pinref part="CN207" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_IN_PD6_OPEN" class="0">
<segment>
<wire x1="66.04" y1="137.16" x2="73.66" y2="137.16" width="0.1524" layer="91"/>
<label x="73.66" y="137.16" size="1.27" layer="95" xref="yes"/>
<pinref part="CN207" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_IN_PD7_OPEN" class="0">
<segment>
<wire x1="66.04" y1="139.7" x2="73.66" y2="139.7" width="0.1524" layer="91"/>
<label x="73.66" y="139.7" size="1.27" layer="95" xref="yes"/>
<pinref part="CN207" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_IN_PD8_LEFT_DOWN" class="0">
<segment>
<wire x1="66.04" y1="142.24" x2="73.66" y2="142.24" width="0.1524" layer="91"/>
<label x="73.66" y="142.24" size="1.27" layer="95" xref="yes"/>
<pinref part="CN207" gate="CN" pin="13"/>
</segment>
</net>
<net name="SIG_IN_PD8_LEFT_UP" class="0">
<segment>
<wire x1="66.04" y1="144.78" x2="73.66" y2="144.78" width="0.1524" layer="91"/>
<label x="73.66" y="144.78" size="1.27" layer="95" xref="yes"/>
<pinref part="CN207" gate="CN" pin="15"/>
</segment>
</net>
<net name="SIG_OUT_PD8_BLUE" class="0">
<segment>
<wire x1="50.8" y1="104.14" x2="43.18" y2="104.14" width="0.1524" layer="91"/>
<label x="43.18" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="16"/>
</segment>
</net>
<net name="SIG_OUT_PD8_GREEN" class="0">
<segment>
<wire x1="50.8" y1="99.06" x2="43.18" y2="99.06" width="0.1524" layer="91"/>
<label x="43.18" y="99.06" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_OUT_PD8_RED" class="0">
<segment>
<wire x1="50.8" y1="101.6" x2="43.18" y2="101.6" width="0.1524" layer="91"/>
<label x="43.18" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="14"/>
</segment>
</net>
<net name="SIG_OUT_PD8_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="50.8" y1="96.52" x2="43.18" y2="96.52" width="0.1524" layer="91"/>
<label x="43.18" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_OUT_PD8_UNLOCK" class="0">
<segment>
<wire x1="50.8" y1="88.9" x2="43.18" y2="88.9" width="0.1524" layer="91"/>
<label x="43.18" y="88.9" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_OUT_PD8_VALVE_DOWN" class="0">
<segment>
<wire x1="50.8" y1="93.98" x2="43.18" y2="93.98" width="0.1524" layer="91"/>
<label x="43.18" y="93.98" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_OUT_PD8_VALVE_UP" class="0">
<segment>
<wire x1="50.8" y1="91.44" x2="43.18" y2="91.44" width="0.1524" layer="91"/>
<label x="43.18" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_OUT_PD9_BLUE" class="0">
<segment>
<wire x1="66.04" y1="104.14" x2="73.66" y2="104.14" width="0.1524" layer="91"/>
<label x="73.66" y="104.14" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="15"/>
</segment>
</net>
<net name="SIG_OUT_PD9_GREEN" class="0">
<segment>
<wire x1="66.04" y1="99.06" x2="73.66" y2="99.06" width="0.1524" layer="91"/>
<label x="73.66" y="99.06" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_OUT_PD9_RED" class="0">
<segment>
<wire x1="66.04" y1="101.6" x2="73.66" y2="101.6" width="0.1524" layer="91"/>
<label x="73.66" y="101.6" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="13"/>
</segment>
</net>
<net name="SIG_OUT_PD9_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="66.04" y1="96.52" x2="73.66" y2="96.52" width="0.1524" layer="91"/>
<label x="73.66" y="96.52" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_OUT_PD9_UNLOCK" class="0">
<segment>
<wire x1="66.04" y1="88.9" x2="73.66" y2="88.9" width="0.1524" layer="91"/>
<label x="73.66" y="88.9" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_OUT_PD9_VALVE_DOWN" class="0">
<segment>
<wire x1="66.04" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<label x="73.66" y="93.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_OUT_PD9_VALVE_UP" class="0">
<segment>
<wire x1="66.04" y1="91.44" x2="73.66" y2="91.44" width="0.1524" layer="91"/>
<label x="73.66" y="91.44" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_OUT_PD5_GREEN" class="0">
<segment>
<wire x1="50.8" y1="86.36" x2="43.18" y2="86.36" width="0.1524" layer="91"/>
<label x="43.18" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_OUT_PD5_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="66.04" y1="86.36" x2="73.66" y2="86.36" width="0.1524" layer="91"/>
<label x="73.66" y="86.36" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN208" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_IN_PD8_RIGHT_UP" class="0">
<segment>
<wire x1="50.8" y1="66.04" x2="43.18" y2="66.04" width="0.1524" layer="91"/>
<label x="43.18" y="66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN209" gate="CN" pin="16"/>
</segment>
</net>
<net name="SIG_IN_PD8_RIGHT_DOWN" class="0">
<segment>
<wire x1="50.8" y1="63.5" x2="43.18" y2="63.5" width="0.1524" layer="91"/>
<label x="43.18" y="63.5" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN209" gate="CN" pin="14"/>
</segment>
</net>
<net name="SIG_IN_PD8_CURTAIN" class="0">
<segment>
<wire x1="50.8" y1="60.96" x2="43.18" y2="60.96" width="0.1524" layer="91"/>
<label x="43.18" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN209" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_IN_PD8_OPEN" class="0">
<segment>
<wire x1="50.8" y1="58.42" x2="43.18" y2="58.42" width="0.1524" layer="91"/>
<label x="43.18" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN209" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_IN_PD9_LEFT_UP" class="0">
<segment>
<wire x1="50.8" y1="55.88" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
<label x="43.18" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN209" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_IN_PD9_LEFT_DOWN" class="0">
<segment>
<wire x1="50.8" y1="53.34" x2="43.18" y2="53.34" width="0.1524" layer="91"/>
<label x="43.18" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN209" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_IN_PD9_RIGHT_UP" class="0">
<segment>
<wire x1="50.8" y1="50.8" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<label x="43.18" y="50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN209" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_IN_PD9_RIGHT_DOWN" class="0">
<segment>
<wire x1="50.8" y1="48.26" x2="43.18" y2="48.26" width="0.1524" layer="91"/>
<label x="43.18" y="48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN209" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_IN_PD9_CURTAIN" class="0">
<segment>
<wire x1="66.04" y1="48.26" x2="73.66" y2="48.26" width="0.1524" layer="91"/>
<label x="73.66" y="48.26" size="1.27" layer="95" xref="yes"/>
<pinref part="CN209" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_IN_PD9_OPEN" class="0">
<segment>
<wire x1="66.04" y1="50.8" x2="73.66" y2="50.8" width="0.1524" layer="91"/>
<label x="73.66" y="50.8" size="1.27" layer="95" xref="yes"/>
<pinref part="CN209" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_IN_PD10_OPEN" class="0">
<segment>
<wire x1="66.04" y1="53.34" x2="73.66" y2="53.34" width="0.1524" layer="91"/>
<label x="73.66" y="53.34" size="1.27" layer="95" xref="yes"/>
<pinref part="CN209" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_IN_PD10_CURTAIN" class="0">
<segment>
<wire x1="66.04" y1="55.88" x2="73.66" y2="55.88" width="0.1524" layer="91"/>
<label x="73.66" y="55.88" size="1.27" layer="95" xref="yes"/>
<pinref part="CN209" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_IN_PD10_RIGHT_DOWN" class="0">
<segment>
<wire x1="66.04" y1="58.42" x2="73.66" y2="58.42" width="0.1524" layer="91"/>
<label x="73.66" y="58.42" size="1.27" layer="95" xref="yes"/>
<pinref part="CN209" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_IN_PD10_RIGHT_UP" class="0">
<segment>
<wire x1="66.04" y1="60.96" x2="73.66" y2="60.96" width="0.1524" layer="91"/>
<label x="73.66" y="60.96" size="1.27" layer="95" xref="yes"/>
<pinref part="CN209" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_IN_PD10_LEFT_DOWN" class="0">
<segment>
<wire x1="66.04" y1="63.5" x2="73.66" y2="63.5" width="0.1524" layer="91"/>
<label x="73.66" y="63.5" size="1.27" layer="95" xref="yes"/>
<pinref part="CN209" gate="CN" pin="13"/>
</segment>
</net>
<net name="SIG_OUT_PD10_BLUE" class="0">
<segment>
<wire x1="50.8" y1="25.4" x2="43.18" y2="25.4" width="0.1524" layer="91"/>
<label x="43.18" y="25.4" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN210" gate="CN" pin="16"/>
</segment>
</net>
<net name="SIG_OUT_PD10_RED" class="0">
<segment>
<wire x1="50.8" y1="22.86" x2="43.18" y2="22.86" width="0.1524" layer="91"/>
<label x="43.18" y="22.86" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN210" gate="CN" pin="14"/>
</segment>
</net>
<net name="SIG_OUT_PD10_GREEN" class="0">
<segment>
<wire x1="50.8" y1="20.32" x2="43.18" y2="20.32" width="0.1524" layer="91"/>
<label x="43.18" y="20.32" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN210" gate="CN" pin="12"/>
</segment>
</net>
<net name="SIG_OUT_PD10_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="50.8" y1="17.78" x2="43.18" y2="17.78" width="0.1524" layer="91"/>
<label x="43.18" y="17.78" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN210" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_OUT_PD10_VALVE_DOWN" class="0">
<segment>
<wire x1="50.8" y1="15.24" x2="43.18" y2="15.24" width="0.1524" layer="91"/>
<label x="43.18" y="15.24" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN210" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_OUT_PD10_VALVE_UP" class="0">
<segment>
<wire x1="50.8" y1="12.7" x2="43.18" y2="12.7" width="0.1524" layer="91"/>
<label x="43.18" y="12.7" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN210" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_OUT_PD10_UNLOCK" class="0">
<segment>
<wire x1="50.8" y1="10.16" x2="43.18" y2="10.16" width="0.1524" layer="91"/>
<label x="43.18" y="10.16" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN210" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_OUT_PD5_VALVE_DOWN" class="0">
<segment>
<wire x1="50.8" y1="7.62" x2="43.18" y2="7.62" width="0.1524" layer="91"/>
<label x="43.18" y="7.62" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN210" gate="CN" pin="2"/>
</segment>
</net>
<net name="SIG_OUT_PD5_VALVE_UP" class="0">
<segment>
<wire x1="66.04" y1="7.62" x2="73.66" y2="7.62" width="0.1524" layer="91"/>
<label x="73.66" y="7.62" size="1.27" layer="95" xref="yes"/>
<pinref part="CN210" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_OUT_PD5_UNLOCK" class="0">
<segment>
<wire x1="66.04" y1="10.16" x2="73.66" y2="10.16" width="0.1524" layer="91"/>
<label x="73.66" y="10.16" size="1.27" layer="95" xref="yes"/>
<pinref part="CN210" gate="CN" pin="3"/>
</segment>
</net>
<net name="SIG_OUT_SP1_VALVE11" class="0">
<segment>
<wire x1="66.04" y1="12.7" x2="73.66" y2="12.7" width="0.1524" layer="91"/>
<label x="73.66" y="12.7" size="1.27" layer="95" xref="yes"/>
<pinref part="CN210" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_OUT_SP1_VALVE12" class="0">
<segment>
<wire x1="66.04" y1="15.24" x2="73.66" y2="15.24" width="0.1524" layer="91"/>
<label x="73.66" y="15.24" size="1.27" layer="95" xref="yes"/>
<pinref part="CN210" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_OUT_SP1_VALVE21" class="0">
<segment>
<wire x1="66.04" y1="17.78" x2="73.66" y2="17.78" width="0.1524" layer="91"/>
<label x="73.66" y="17.78" size="1.27" layer="95" xref="yes"/>
<pinref part="CN210" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_OUT_SP1_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="66.04" y1="20.32" x2="73.66" y2="20.32" width="0.1524" layer="91"/>
<label x="73.66" y="20.32" size="1.27" layer="95" xref="yes"/>
<pinref part="CN210" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_OUT_SP1_LOCK" class="0">
<segment>
<wire x1="66.04" y1="22.86" x2="73.66" y2="22.86" width="0.1524" layer="91"/>
<label x="73.66" y="22.86" size="1.27" layer="95" xref="yes"/>
<pinref part="CN210" gate="CN" pin="13"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>UD-C27</description>
<plain>
<wire x1="20.32" y1="264.16" x2="73.66" y2="264.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="73.66" y1="264.16" x2="73.66" y2="368.3" width="0.254" layer="94" style="shortdash"/>
<wire x1="73.66" y1="368.3" x2="20.32" y2="368.3" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="368.3" x2="20.32" y2="264.16" width="0.254" layer="94" style="shortdash"/>
<text x="167.64" y="27.94" size="2.54" layer="94">UD-C27 connectors</text>
<wire x1="76.2" y1="264.16" x2="129.54" y2="264.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="129.54" y1="264.16" x2="129.54" y2="368.3" width="0.254" layer="94" style="shortdash"/>
<wire x1="129.54" y1="368.3" x2="76.2" y2="368.3" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="368.3" x2="76.2" y2="264.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="264.16" x2="185.42" y2="264.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="185.42" y1="264.16" x2="185.42" y2="368.3" width="0.254" layer="94" style="shortdash"/>
<wire x1="185.42" y1="368.3" x2="132.08" y2="368.3" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="368.3" x2="132.08" y2="264.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="187.96" y1="264.16" x2="241.3" y2="264.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="241.3" y1="264.16" x2="241.3" y2="368.3" width="0.254" layer="94" style="shortdash"/>
<wire x1="241.3" y1="368.3" x2="187.96" y2="368.3" width="0.254" layer="94" style="shortdash"/>
<wire x1="187.96" y1="368.3" x2="187.96" y2="264.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="157.48" x2="73.66" y2="157.48" width="0.254" layer="94" style="shortdash"/>
<wire x1="73.66" y1="157.48" x2="73.66" y2="261.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="73.66" y1="261.62" x2="20.32" y2="261.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="261.62" x2="20.32" y2="157.48" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="157.48" x2="129.54" y2="157.48" width="0.254" layer="94" style="shortdash"/>
<wire x1="129.54" y1="157.48" x2="129.54" y2="261.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="129.54" y1="261.62" x2="76.2" y2="261.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="261.62" x2="76.2" y2="157.48" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="157.48" x2="185.42" y2="157.48" width="0.254" layer="94" style="shortdash"/>
<wire x1="185.42" y1="157.48" x2="185.42" y2="261.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="185.42" y1="261.62" x2="132.08" y2="261.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="261.62" x2="132.08" y2="157.48" width="0.254" layer="94" style="shortdash"/>
<wire x1="187.96" y1="157.48" x2="241.3" y2="157.48" width="0.254" layer="94" style="shortdash"/>
<wire x1="241.3" y1="157.48" x2="241.3" y2="261.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="241.3" y1="261.62" x2="187.96" y2="261.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="187.96" y1="261.62" x2="187.96" y2="157.48" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="50.8" x2="73.66" y2="50.8" width="0.254" layer="94" style="shortdash"/>
<wire x1="73.66" y1="50.8" x2="73.66" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<wire x1="73.66" y1="154.94" x2="20.32" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<wire x1="20.32" y1="154.94" x2="20.32" y2="50.8" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="50.8" x2="129.54" y2="50.8" width="0.254" layer="94" style="shortdash"/>
<wire x1="129.54" y1="50.8" x2="129.54" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<wire x1="129.54" y1="154.94" x2="76.2" y2="154.94" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="154.94" x2="76.2" y2="50.8" width="0.254" layer="94" style="shortdash"/>
</plain>
<instances>
<instance part="CN305" gate="CN" x="25.4" y="294.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="22.86" y="271.78" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="22.86" y="269.24" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN301" gate="CN" x="25.4" y="345.44" smashed="yes" rot="MR0">
<attribute name="NAME" x="22.86" y="322.58" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="22.86" y="320.04" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X305" gate="X" x="38.1" y="269.24" smashed="yes">
<attribute name="NAME" x="40.64" y="271.78" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="40.64" y="269.24" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="40.64" y="266.7" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="38.1" y="269.24" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="38.1" y="269.24" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X301" gate="X" x="38.1" y="320.04" smashed="yes">
<attribute name="NAME" x="40.64" y="322.58" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="40.64" y="320.04" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="40.64" y="317.5" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="38.1" y="320.04" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="38.1" y="320.04" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="FRAME301" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="FRAME301" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="CN306" gate="CN" x="81.28" y="294.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="78.74" y="271.78" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="78.74" y="269.24" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN302" gate="CN" x="81.28" y="345.44" smashed="yes" rot="MR0">
<attribute name="NAME" x="78.74" y="322.58" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="78.74" y="320.04" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X306" gate="X" x="93.98" y="269.24" smashed="yes">
<attribute name="NAME" x="96.52" y="271.78" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="96.52" y="269.24" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="96.52" y="266.7" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="93.98" y="269.24" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="93.98" y="269.24" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X302" gate="X" x="93.98" y="320.04" smashed="yes">
<attribute name="NAME" x="96.52" y="322.58" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="96.52" y="320.04" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="96.52" y="317.5" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="93.98" y="320.04" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="93.98" y="320.04" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN307" gate="CN" x="137.16" y="294.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="134.62" y="271.78" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="134.62" y="269.24" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN303" gate="CN" x="137.16" y="345.44" smashed="yes" rot="MR0">
<attribute name="NAME" x="134.62" y="322.58" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="134.62" y="320.04" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X307" gate="X" x="149.86" y="269.24" smashed="yes">
<attribute name="NAME" x="152.4" y="271.78" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="152.4" y="269.24" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="152.4" y="266.7" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="149.86" y="269.24" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="149.86" y="269.24" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X303" gate="X" x="149.86" y="320.04" smashed="yes">
<attribute name="NAME" x="152.4" y="322.58" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="152.4" y="320.04" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="152.4" y="317.5" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="149.86" y="320.04" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="149.86" y="320.04" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN308" gate="CN" x="193.04" y="294.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="190.5" y="271.78" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="190.5" y="269.24" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN304" gate="CN" x="193.04" y="345.44" smashed="yes" rot="MR0">
<attribute name="NAME" x="190.5" y="322.58" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="190.5" y="320.04" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X308" gate="X" x="205.74" y="269.24" smashed="yes">
<attribute name="NAME" x="208.28" y="271.78" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="208.28" y="269.24" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="208.28" y="266.7" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="205.74" y="269.24" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="205.74" y="269.24" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X304" gate="X" x="205.74" y="320.04" smashed="yes">
<attribute name="NAME" x="208.28" y="322.58" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="208.28" y="320.04" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="208.28" y="317.5" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="205.74" y="320.04" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="205.74" y="320.04" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN313" gate="CN" x="25.4" y="187.96" smashed="yes" rot="MR0">
<attribute name="NAME" x="22.86" y="165.1" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="22.86" y="162.56" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN309" gate="CN" x="25.4" y="238.76" smashed="yes" rot="MR0">
<attribute name="NAME" x="22.86" y="215.9" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="22.86" y="213.36" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X313" gate="X" x="38.1" y="162.56" smashed="yes">
<attribute name="NAME" x="40.64" y="165.1" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="40.64" y="162.56" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="40.64" y="160.02" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="38.1" y="162.56" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="38.1" y="162.56" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X309" gate="X" x="38.1" y="213.36" smashed="yes">
<attribute name="NAME" x="40.64" y="215.9" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="40.64" y="213.36" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="40.64" y="210.82" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="38.1" y="213.36" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="38.1" y="213.36" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN314" gate="CN" x="81.28" y="187.96" smashed="yes" rot="MR0">
<attribute name="NAME" x="78.74" y="165.1" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="78.74" y="162.56" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN310" gate="CN" x="81.28" y="238.76" smashed="yes" rot="MR0">
<attribute name="NAME" x="78.74" y="215.9" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="78.74" y="213.36" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X314" gate="X" x="93.98" y="162.56" smashed="yes">
<attribute name="NAME" x="96.52" y="165.1" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="96.52" y="162.56" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="96.52" y="160.02" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="93.98" y="162.56" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="93.98" y="162.56" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X310" gate="X" x="93.98" y="213.36" smashed="yes">
<attribute name="NAME" x="96.52" y="215.9" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="96.52" y="213.36" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="96.52" y="210.82" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="93.98" y="213.36" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="93.98" y="213.36" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN315" gate="CN" x="137.16" y="187.96" smashed="yes" rot="MR0">
<attribute name="NAME" x="134.62" y="165.1" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="134.62" y="162.56" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN311" gate="CN" x="137.16" y="238.76" smashed="yes" rot="MR0">
<attribute name="NAME" x="134.62" y="215.9" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="134.62" y="213.36" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X315" gate="X" x="149.86" y="162.56" smashed="yes">
<attribute name="NAME" x="152.4" y="165.1" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="152.4" y="162.56" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="152.4" y="160.02" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="149.86" y="162.56" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="149.86" y="162.56" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X311" gate="X" x="149.86" y="213.36" smashed="yes">
<attribute name="NAME" x="152.4" y="215.9" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="152.4" y="213.36" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="152.4" y="210.82" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="149.86" y="213.36" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="149.86" y="213.36" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN316" gate="CN" x="193.04" y="187.96" smashed="yes" rot="MR0">
<attribute name="NAME" x="190.5" y="165.1" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="190.5" y="162.56" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN312" gate="CN" x="193.04" y="238.76" smashed="yes" rot="MR0">
<attribute name="NAME" x="190.5" y="215.9" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="190.5" y="213.36" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X316" gate="X" x="205.74" y="162.56" smashed="yes">
<attribute name="NAME" x="208.28" y="165.1" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="208.28" y="162.56" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="208.28" y="160.02" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="205.74" y="162.56" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="205.74" y="162.56" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X312" gate="X" x="205.74" y="213.36" smashed="yes">
<attribute name="NAME" x="208.28" y="215.9" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="208.28" y="213.36" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="208.28" y="210.82" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="205.74" y="213.36" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="205.74" y="213.36" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN319" gate="CN" x="25.4" y="81.28" smashed="yes" rot="MR0">
<attribute name="NAME" x="22.86" y="58.42" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="22.86" y="55.88" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN317" gate="CN" x="25.4" y="132.08" smashed="yes" rot="MR0">
<attribute name="NAME" x="22.86" y="109.22" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="22.86" y="106.68" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X319" gate="X" x="38.1" y="55.88" smashed="yes">
<attribute name="NAME" x="40.64" y="58.42" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="40.64" y="55.88" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="40.64" y="53.34" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="38.1" y="55.88" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="38.1" y="55.88" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X317" gate="X" x="38.1" y="106.68" smashed="yes">
<attribute name="NAME" x="40.64" y="109.22" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="40.64" y="106.68" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="40.64" y="104.14" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="38.1" y="106.68" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="38.1" y="106.68" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="CN320" gate="CN" x="81.28" y="81.28" smashed="yes" rot="MR0">
<attribute name="NAME" x="78.74" y="58.42" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="78.74" y="55.88" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN318" gate="CN" x="81.28" y="132.08" smashed="yes" rot="MR0">
<attribute name="NAME" x="78.74" y="109.22" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="78.74" y="106.68" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X320" gate="X" x="93.98" y="55.88" smashed="yes">
<attribute name="NAME" x="96.52" y="58.42" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="96.52" y="55.88" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="96.52" y="53.34" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="93.98" y="55.88" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="93.98" y="55.88" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
<instance part="X318" gate="X" x="93.98" y="106.68" smashed="yes">
<attribute name="NAME" x="96.52" y="109.22" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="96.52" y="106.68" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="96.52" y="104.14" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="93.98" y="106.68" size="1.778" layer="96" align="top-left" display="off"/>
<attribute name="MPN" x="93.98" y="106.68" size="1.778" layer="96" align="top-left" display="off"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="SIG_IN_PD1_LEFT_UP" class="0">
<segment>
<wire x1="27.94" y1="312.42" x2="33.02" y2="312.42" width="0.1524" layer="91"/>
<label x="33.02" y="312.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN305" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_PD1_LEFT_DOWN" class="0">
<segment>
<wire x1="27.94" y1="307.34" x2="33.02" y2="307.34" width="0.1524" layer="91"/>
<label x="33.02" y="307.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN305" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_PD1_RIGHT_UP" class="0">
<segment>
<wire x1="27.94" y1="302.26" x2="33.02" y2="302.26" width="0.1524" layer="91"/>
<label x="33.02" y="302.26" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN305" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_IN_PD1_RIGHT_DOWN" class="0">
<segment>
<wire x1="27.94" y1="297.18" x2="33.02" y2="297.18" width="0.1524" layer="91"/>
<label x="33.02" y="297.18" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN305" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_OUT_PD1_UNLOCK" class="0">
<segment>
<wire x1="27.94" y1="327.66" x2="33.02" y2="327.66" width="0.1524" layer="91"/>
<label x="33.02" y="327.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN301" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_IN_PD1_OPEN" class="0">
<segment>
<wire x1="27.94" y1="287.02" x2="33.02" y2="287.02" width="0.1524" layer="91"/>
<label x="33.02" y="287.02" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN305" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_PD1_BLUE" class="0">
<segment>
<wire x1="27.94" y1="358.14" x2="33.02" y2="358.14" width="0.1524" layer="91"/>
<label x="33.02" y="358.14" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN301" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_OUT_PD1_RED" class="0">
<segment>
<wire x1="27.94" y1="353.06" x2="33.02" y2="353.06" width="0.1524" layer="91"/>
<label x="33.02" y="353.06" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN301" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD1_GREEN" class="0">
<segment>
<wire x1="27.94" y1="347.98" x2="33.02" y2="347.98" width="0.1524" layer="91"/>
<label x="33.02" y="347.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN301" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_PD1_CURTAIN" class="0">
<segment>
<wire x1="27.94" y1="292.1" x2="33.02" y2="292.1" width="0.1524" layer="91"/>
<label x="33.02" y="292.1" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN305" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_PD1_VALVE_UP" class="0">
<segment>
<wire x1="27.94" y1="332.74" x2="33.02" y2="332.74" width="0.1524" layer="91"/>
<label x="33.02" y="332.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN301" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_OUT_PD1_VALVE_DOWN" class="0">
<segment>
<wire x1="27.94" y1="337.82" x2="33.02" y2="337.82" width="0.1524" layer="91"/>
<label x="33.02" y="337.82" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN301" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_PD1_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="27.94" y1="342.9" x2="33.02" y2="342.9" width="0.1524" layer="91"/>
<label x="33.02" y="342.9" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN301" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_PD2_CURTAIN" class="0">
<segment>
<wire x1="83.82" y1="292.1" x2="88.9" y2="292.1" width="0.1524" layer="91"/>
<label x="88.9" y="292.1" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN306" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_PD2_LEFT_DOWN" class="0">
<segment>
<wire x1="83.82" y1="307.34" x2="88.9" y2="307.34" width="0.1524" layer="91"/>
<label x="88.9" y="307.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN306" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_PD2_LEFT_UP" class="0">
<segment>
<wire x1="83.82" y1="312.42" x2="88.9" y2="312.42" width="0.1524" layer="91"/>
<label x="88.9" y="312.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN306" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_PD2_OPEN" class="0">
<segment>
<wire x1="83.82" y1="287.02" x2="88.9" y2="287.02" width="0.1524" layer="91"/>
<label x="88.9" y="287.02" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN306" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_PD2_RIGHT_DOWN" class="0">
<segment>
<wire x1="83.82" y1="297.18" x2="88.9" y2="297.18" width="0.1524" layer="91"/>
<label x="88.9" y="297.18" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN306" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_PD2_RIGHT_UP" class="0">
<segment>
<wire x1="83.82" y1="302.26" x2="88.9" y2="302.26" width="0.1524" layer="91"/>
<label x="88.9" y="302.26" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN306" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD2_BLUE" class="0">
<segment>
<wire x1="83.82" y1="358.14" x2="88.9" y2="358.14" width="0.1524" layer="91"/>
<label x="88.9" y="358.14" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN302" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_OUT_PD2_GREEN" class="0">
<segment>
<wire x1="83.82" y1="347.98" x2="88.9" y2="347.98" width="0.1524" layer="91"/>
<label x="88.9" y="347.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN302" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_OUT_PD2_RED" class="0">
<segment>
<wire x1="83.82" y1="353.06" x2="88.9" y2="353.06" width="0.1524" layer="91"/>
<label x="88.9" y="353.06" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN302" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD2_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="83.82" y1="342.9" x2="88.9" y2="342.9" width="0.1524" layer="91"/>
<label x="88.9" y="342.9" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN302" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_PD2_UNLOCK" class="0">
<segment>
<wire x1="83.82" y1="327.66" x2="88.9" y2="327.66" width="0.1524" layer="91"/>
<label x="88.9" y="327.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN302" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_OUT_PD2_VALVE_DOWN" class="0">
<segment>
<wire x1="83.82" y1="337.82" x2="88.9" y2="337.82" width="0.1524" layer="91"/>
<label x="88.9" y="337.82" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN302" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_PD2_VALVE_UP" class="0">
<segment>
<wire x1="83.82" y1="332.74" x2="88.9" y2="332.74" width="0.1524" layer="91"/>
<label x="88.9" y="332.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN302" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_PD3_CURTAIN" class="0">
<segment>
<wire x1="139.7" y1="292.1" x2="144.78" y2="292.1" width="0.1524" layer="91"/>
<label x="144.78" y="292.1" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN307" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_PD3_LEFT_DOWN" class="0">
<segment>
<wire x1="139.7" y1="307.34" x2="144.78" y2="307.34" width="0.1524" layer="91"/>
<label x="144.78" y="307.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN307" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_PD3_LEFT_UP" class="0">
<segment>
<wire x1="139.7" y1="312.42" x2="144.78" y2="312.42" width="0.1524" layer="91"/>
<label x="144.78" y="312.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN307" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_PD3_OPEN" class="0">
<segment>
<wire x1="139.7" y1="287.02" x2="144.78" y2="287.02" width="0.1524" layer="91"/>
<label x="144.78" y="287.02" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN307" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_PD3_RIGHT_DOWN" class="0">
<segment>
<wire x1="139.7" y1="297.18" x2="144.78" y2="297.18" width="0.1524" layer="91"/>
<label x="144.78" y="297.18" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN307" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_PD3_RIGHT_UP" class="0">
<segment>
<wire x1="139.7" y1="302.26" x2="144.78" y2="302.26" width="0.1524" layer="91"/>
<label x="144.78" y="302.26" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN307" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD3_BLUE" class="0">
<segment>
<wire x1="139.7" y1="358.14" x2="144.78" y2="358.14" width="0.1524" layer="91"/>
<label x="144.78" y="358.14" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN303" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_OUT_PD3_GREEN" class="0">
<segment>
<wire x1="139.7" y1="347.98" x2="144.78" y2="347.98" width="0.1524" layer="91"/>
<label x="144.78" y="347.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN303" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_OUT_PD3_RED" class="0">
<segment>
<wire x1="139.7" y1="353.06" x2="144.78" y2="353.06" width="0.1524" layer="91"/>
<label x="144.78" y="353.06" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN303" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD3_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="139.7" y1="342.9" x2="144.78" y2="342.9" width="0.1524" layer="91"/>
<label x="144.78" y="342.9" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN303" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_PD3_UNLOCK" class="0">
<segment>
<wire x1="139.7" y1="327.66" x2="144.78" y2="327.66" width="0.1524" layer="91"/>
<label x="144.78" y="327.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN303" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_OUT_PD3_VALVE_DOWN" class="0">
<segment>
<wire x1="139.7" y1="337.82" x2="144.78" y2="337.82" width="0.1524" layer="91"/>
<label x="144.78" y="337.82" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN303" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_PD3_VALVE_UP" class="0">
<segment>
<wire x1="139.7" y1="332.74" x2="144.78" y2="332.74" width="0.1524" layer="91"/>
<label x="144.78" y="332.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN303" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_PD4_CURTAIN" class="0">
<segment>
<wire x1="195.58" y1="292.1" x2="200.66" y2="292.1" width="0.1524" layer="91"/>
<label x="200.66" y="292.1" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN308" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_PD4_LEFT_DOWN" class="0">
<segment>
<wire x1="195.58" y1="307.34" x2="200.66" y2="307.34" width="0.1524" layer="91"/>
<label x="200.66" y="307.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN308" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_PD4_LEFT_UP" class="0">
<segment>
<wire x1="195.58" y1="312.42" x2="200.66" y2="312.42" width="0.1524" layer="91"/>
<label x="200.66" y="312.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN308" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_PD4_OPEN" class="0">
<segment>
<wire x1="195.58" y1="287.02" x2="200.66" y2="287.02" width="0.1524" layer="91"/>
<label x="200.66" y="287.02" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN308" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_PD4_RIGHT_DOWN" class="0">
<segment>
<wire x1="195.58" y1="297.18" x2="200.66" y2="297.18" width="0.1524" layer="91"/>
<label x="200.66" y="297.18" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN308" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_PD4_RIGHT_UP" class="0">
<segment>
<wire x1="195.58" y1="302.26" x2="200.66" y2="302.26" width="0.1524" layer="91"/>
<label x="200.66" y="302.26" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN308" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD4_BLUE" class="0">
<segment>
<wire x1="195.58" y1="358.14" x2="200.66" y2="358.14" width="0.1524" layer="91"/>
<label x="200.66" y="358.14" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN304" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_OUT_PD4_GREEN" class="0">
<segment>
<wire x1="195.58" y1="347.98" x2="200.66" y2="347.98" width="0.1524" layer="91"/>
<label x="200.66" y="347.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN304" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_OUT_PD4_RED" class="0">
<segment>
<wire x1="195.58" y1="353.06" x2="200.66" y2="353.06" width="0.1524" layer="91"/>
<label x="200.66" y="353.06" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN304" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD4_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="195.58" y1="342.9" x2="200.66" y2="342.9" width="0.1524" layer="91"/>
<label x="200.66" y="342.9" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN304" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_PD4_UNLOCK" class="0">
<segment>
<wire x1="195.58" y1="327.66" x2="200.66" y2="327.66" width="0.1524" layer="91"/>
<label x="200.66" y="327.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN304" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_OUT_PD4_VALVE_DOWN" class="0">
<segment>
<wire x1="195.58" y1="337.82" x2="200.66" y2="337.82" width="0.1524" layer="91"/>
<label x="200.66" y="337.82" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN304" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_PD4_VALVE_UP" class="0">
<segment>
<wire x1="195.58" y1="332.74" x2="200.66" y2="332.74" width="0.1524" layer="91"/>
<label x="200.66" y="332.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN304" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_PD5_CURTAIN" class="0">
<segment>
<wire x1="27.94" y1="185.42" x2="33.02" y2="185.42" width="0.1524" layer="91"/>
<label x="33.02" y="185.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN313" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_PD5_LEFT_DOWN" class="0">
<segment>
<wire x1="27.94" y1="200.66" x2="33.02" y2="200.66" width="0.1524" layer="91"/>
<label x="33.02" y="200.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN313" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_PD5_LEFT_UP" class="0">
<segment>
<wire x1="27.94" y1="205.74" x2="33.02" y2="205.74" width="0.1524" layer="91"/>
<label x="33.02" y="205.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN313" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_PD5_OPEN" class="0">
<segment>
<wire x1="27.94" y1="180.34" x2="33.02" y2="180.34" width="0.1524" layer="91"/>
<label x="33.02" y="180.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN313" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_PD5_RIGHT_DOWN" class="0">
<segment>
<wire x1="27.94" y1="190.5" x2="33.02" y2="190.5" width="0.1524" layer="91"/>
<label x="33.02" y="190.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN313" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_PD5_RIGHT_UP" class="0">
<segment>
<wire x1="27.94" y1="195.58" x2="33.02" y2="195.58" width="0.1524" layer="91"/>
<label x="33.02" y="195.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN313" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD5_BLUE" class="0">
<segment>
<wire x1="27.94" y1="251.46" x2="33.02" y2="251.46" width="0.1524" layer="91"/>
<label x="33.02" y="251.46" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN309" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_OUT_PD5_GREEN" class="0">
<segment>
<wire x1="27.94" y1="241.3" x2="33.02" y2="241.3" width="0.1524" layer="91"/>
<label x="33.02" y="241.3" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN309" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_OUT_PD5_RED" class="0">
<segment>
<wire x1="27.94" y1="246.38" x2="33.02" y2="246.38" width="0.1524" layer="91"/>
<label x="33.02" y="246.38" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN309" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD5_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="27.94" y1="236.22" x2="33.02" y2="236.22" width="0.1524" layer="91"/>
<label x="33.02" y="236.22" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN309" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_PD5_UNLOCK" class="0">
<segment>
<wire x1="27.94" y1="220.98" x2="33.02" y2="220.98" width="0.1524" layer="91"/>
<label x="33.02" y="220.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN309" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_OUT_PD5_VALVE_DOWN" class="0">
<segment>
<wire x1="27.94" y1="231.14" x2="33.02" y2="231.14" width="0.1524" layer="91"/>
<label x="33.02" y="231.14" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN309" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_PD5_VALVE_UP" class="0">
<segment>
<wire x1="27.94" y1="226.06" x2="33.02" y2="226.06" width="0.1524" layer="91"/>
<label x="33.02" y="226.06" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN309" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_PD6_CURTAIN" class="0">
<segment>
<wire x1="83.82" y1="185.42" x2="88.9" y2="185.42" width="0.1524" layer="91"/>
<label x="88.9" y="185.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN314" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_PD6_LEFT_DOWN" class="0">
<segment>
<wire x1="83.82" y1="200.66" x2="88.9" y2="200.66" width="0.1524" layer="91"/>
<label x="88.9" y="200.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN314" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_PD6_LEFT_UP" class="0">
<segment>
<wire x1="83.82" y1="205.74" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<label x="88.9" y="205.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN314" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_PD6_OPEN" class="0">
<segment>
<wire x1="83.82" y1="180.34" x2="88.9" y2="180.34" width="0.1524" layer="91"/>
<label x="88.9" y="180.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN314" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_PD6_RIGHT_DOWN" class="0">
<segment>
<wire x1="83.82" y1="190.5" x2="88.9" y2="190.5" width="0.1524" layer="91"/>
<label x="88.9" y="190.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN314" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_PD6_RIGHT_UP" class="0">
<segment>
<wire x1="83.82" y1="195.58" x2="88.9" y2="195.58" width="0.1524" layer="91"/>
<label x="88.9" y="195.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN314" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD6_BLUE" class="0">
<segment>
<wire x1="83.82" y1="251.46" x2="88.9" y2="251.46" width="0.1524" layer="91"/>
<label x="88.9" y="251.46" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN310" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_OUT_PD6_GREEN" class="0">
<segment>
<wire x1="83.82" y1="241.3" x2="88.9" y2="241.3" width="0.1524" layer="91"/>
<label x="88.9" y="241.3" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN310" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_OUT_PD6_RED" class="0">
<segment>
<wire x1="83.82" y1="246.38" x2="88.9" y2="246.38" width="0.1524" layer="91"/>
<label x="88.9" y="246.38" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN310" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD6_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="83.82" y1="236.22" x2="88.9" y2="236.22" width="0.1524" layer="91"/>
<label x="88.9" y="236.22" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN310" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_PD6_UNLOCK" class="0">
<segment>
<wire x1="83.82" y1="220.98" x2="88.9" y2="220.98" width="0.1524" layer="91"/>
<label x="88.9" y="220.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN310" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_OUT_PD6_VALVE_DOWN" class="0">
<segment>
<wire x1="83.82" y1="231.14" x2="88.9" y2="231.14" width="0.1524" layer="91"/>
<label x="88.9" y="231.14" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN310" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_PD6_VALVE_UP" class="0">
<segment>
<wire x1="83.82" y1="226.06" x2="88.9" y2="226.06" width="0.1524" layer="91"/>
<label x="88.9" y="226.06" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN310" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_PD7_CURTAIN" class="0">
<segment>
<wire x1="139.7" y1="185.42" x2="144.78" y2="185.42" width="0.1524" layer="91"/>
<label x="144.78" y="185.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN315" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_PD7_LEFT_DOWN" class="0">
<segment>
<wire x1="139.7" y1="200.66" x2="144.78" y2="200.66" width="0.1524" layer="91"/>
<label x="144.78" y="200.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN315" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_PD7_LEFT_UP" class="0">
<segment>
<wire x1="139.7" y1="205.74" x2="144.78" y2="205.74" width="0.1524" layer="91"/>
<label x="144.78" y="205.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN315" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_PD7_OPEN" class="0">
<segment>
<wire x1="139.7" y1="180.34" x2="144.78" y2="180.34" width="0.1524" layer="91"/>
<label x="144.78" y="180.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN315" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_PD7_RIGHT_DOWN" class="0">
<segment>
<wire x1="139.7" y1="190.5" x2="144.78" y2="190.5" width="0.1524" layer="91"/>
<label x="144.78" y="190.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN315" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_PD7_RIGHT_UP" class="0">
<segment>
<wire x1="139.7" y1="195.58" x2="144.78" y2="195.58" width="0.1524" layer="91"/>
<label x="144.78" y="195.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN315" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD7_BLUE" class="0">
<segment>
<wire x1="139.7" y1="251.46" x2="144.78" y2="251.46" width="0.1524" layer="91"/>
<label x="144.78" y="251.46" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN311" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_OUT_PD7_GREEN" class="0">
<segment>
<wire x1="139.7" y1="241.3" x2="144.78" y2="241.3" width="0.1524" layer="91"/>
<label x="144.78" y="241.3" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN311" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_OUT_PD7_RED" class="0">
<segment>
<wire x1="139.7" y1="246.38" x2="144.78" y2="246.38" width="0.1524" layer="91"/>
<label x="144.78" y="246.38" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN311" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD7_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="139.7" y1="236.22" x2="144.78" y2="236.22" width="0.1524" layer="91"/>
<label x="144.78" y="236.22" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN311" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_PD7_UNLOCK" class="0">
<segment>
<wire x1="139.7" y1="220.98" x2="144.78" y2="220.98" width="0.1524" layer="91"/>
<label x="144.78" y="220.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN311" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_OUT_PD7_VALVE_DOWN" class="0">
<segment>
<wire x1="139.7" y1="231.14" x2="144.78" y2="231.14" width="0.1524" layer="91"/>
<label x="144.78" y="231.14" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN311" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_PD7_VALVE_UP" class="0">
<segment>
<wire x1="139.7" y1="226.06" x2="144.78" y2="226.06" width="0.1524" layer="91"/>
<label x="144.78" y="226.06" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN311" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_PD8_CURTAIN" class="0">
<segment>
<wire x1="195.58" y1="185.42" x2="200.66" y2="185.42" width="0.1524" layer="91"/>
<label x="200.66" y="185.42" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN316" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_PD8_LEFT_DOWN" class="0">
<segment>
<wire x1="195.58" y1="200.66" x2="200.66" y2="200.66" width="0.1524" layer="91"/>
<label x="200.66" y="200.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN316" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_PD8_LEFT_UP" class="0">
<segment>
<wire x1="195.58" y1="205.74" x2="200.66" y2="205.74" width="0.1524" layer="91"/>
<label x="200.66" y="205.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN316" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_PD8_OPEN" class="0">
<segment>
<wire x1="195.58" y1="180.34" x2="200.66" y2="180.34" width="0.1524" layer="91"/>
<label x="200.66" y="180.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN316" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_PD8_RIGHT_DOWN" class="0">
<segment>
<wire x1="195.58" y1="190.5" x2="200.66" y2="190.5" width="0.1524" layer="91"/>
<label x="200.66" y="190.5" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN316" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_PD8_RIGHT_UP" class="0">
<segment>
<wire x1="195.58" y1="195.58" x2="200.66" y2="195.58" width="0.1524" layer="91"/>
<label x="200.66" y="195.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN316" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD8_BLUE" class="0">
<segment>
<wire x1="195.58" y1="251.46" x2="200.66" y2="251.46" width="0.1524" layer="91"/>
<label x="200.66" y="251.46" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN312" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_OUT_PD8_GREEN" class="0">
<segment>
<wire x1="195.58" y1="241.3" x2="200.66" y2="241.3" width="0.1524" layer="91"/>
<label x="200.66" y="241.3" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN312" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_OUT_PD8_RED" class="0">
<segment>
<wire x1="195.58" y1="246.38" x2="200.66" y2="246.38" width="0.1524" layer="91"/>
<label x="200.66" y="246.38" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN312" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD8_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="195.58" y1="236.22" x2="200.66" y2="236.22" width="0.1524" layer="91"/>
<label x="200.66" y="236.22" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN312" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_PD8_UNLOCK" class="0">
<segment>
<wire x1="195.58" y1="220.98" x2="200.66" y2="220.98" width="0.1524" layer="91"/>
<label x="200.66" y="220.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN312" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_OUT_PD8_VALVE_DOWN" class="0">
<segment>
<wire x1="195.58" y1="231.14" x2="200.66" y2="231.14" width="0.1524" layer="91"/>
<label x="200.66" y="231.14" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN312" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_PD8_VALVE_UP" class="0">
<segment>
<wire x1="195.58" y1="226.06" x2="200.66" y2="226.06" width="0.1524" layer="91"/>
<label x="200.66" y="226.06" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN312" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_PD9_CURTAIN" class="0">
<segment>
<wire x1="27.94" y1="78.74" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<label x="33.02" y="78.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN319" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_PD9_LEFT_DOWN" class="0">
<segment>
<wire x1="27.94" y1="93.98" x2="33.02" y2="93.98" width="0.1524" layer="91"/>
<label x="33.02" y="93.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN319" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_PD9_LEFT_UP" class="0">
<segment>
<wire x1="27.94" y1="99.06" x2="33.02" y2="99.06" width="0.1524" layer="91"/>
<label x="33.02" y="99.06" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN319" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_PD9_OPEN" class="0">
<segment>
<wire x1="27.94" y1="73.66" x2="33.02" y2="73.66" width="0.1524" layer="91"/>
<label x="33.02" y="73.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN319" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_PD9_RIGHT_DOWN" class="0">
<segment>
<wire x1="27.94" y1="83.82" x2="33.02" y2="83.82" width="0.1524" layer="91"/>
<label x="33.02" y="83.82" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN319" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_PD9_RIGHT_UP" class="0">
<segment>
<wire x1="27.94" y1="88.9" x2="33.02" y2="88.9" width="0.1524" layer="91"/>
<label x="33.02" y="88.9" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN319" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD9_BLUE" class="0">
<segment>
<wire x1="27.94" y1="144.78" x2="33.02" y2="144.78" width="0.1524" layer="91"/>
<label x="33.02" y="144.78" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN317" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_OUT_PD9_GREEN" class="0">
<segment>
<wire x1="27.94" y1="134.62" x2="33.02" y2="134.62" width="0.1524" layer="91"/>
<label x="33.02" y="134.62" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN317" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_OUT_PD9_RED" class="0">
<segment>
<wire x1="27.94" y1="139.7" x2="33.02" y2="139.7" width="0.1524" layer="91"/>
<label x="33.02" y="139.7" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN317" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD9_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="27.94" y1="129.54" x2="33.02" y2="129.54" width="0.1524" layer="91"/>
<label x="33.02" y="129.54" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN317" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_PD9_UNLOCK" class="0">
<segment>
<wire x1="27.94" y1="114.3" x2="33.02" y2="114.3" width="0.1524" layer="91"/>
<label x="33.02" y="114.3" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN317" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_OUT_PD9_VALVE_DOWN" class="0">
<segment>
<wire x1="27.94" y1="124.46" x2="33.02" y2="124.46" width="0.1524" layer="91"/>
<label x="33.02" y="124.46" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN317" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_PD9_VALVE_UP" class="0">
<segment>
<wire x1="27.94" y1="119.38" x2="33.02" y2="119.38" width="0.1524" layer="91"/>
<label x="33.02" y="119.38" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN317" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_PD10_CURTAIN" class="0">
<segment>
<wire x1="83.82" y1="78.74" x2="88.9" y2="78.74" width="0.1524" layer="91"/>
<label x="88.9" y="78.74" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN320" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_PD10_LEFT_DOWN" class="0">
<segment>
<wire x1="83.82" y1="93.98" x2="88.9" y2="93.98" width="0.1524" layer="91"/>
<label x="88.9" y="93.98" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN320" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_PD10_LEFT_UP" class="0">
<segment>
<wire x1="83.82" y1="99.06" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<label x="88.9" y="99.06" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN320" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_PD10_OPEN" class="0">
<segment>
<wire x1="83.82" y1="73.66" x2="88.9" y2="73.66" width="0.1524" layer="91"/>
<label x="88.9" y="73.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN320" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_PD10_RIGHT_DOWN" class="0">
<segment>
<wire x1="83.82" y1="83.82" x2="88.9" y2="83.82" width="0.1524" layer="91"/>
<label x="88.9" y="83.82" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN320" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_PD10_RIGHT_UP" class="0">
<segment>
<wire x1="83.82" y1="88.9" x2="88.9" y2="88.9" width="0.1524" layer="91"/>
<label x="88.9" y="88.9" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN320" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD10_BLUE" class="0">
<segment>
<wire x1="83.82" y1="144.78" x2="88.9" y2="144.78" width="0.1524" layer="91"/>
<label x="88.9" y="144.78" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN318" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_OUT_PD10_GREEN" class="0">
<segment>
<wire x1="83.82" y1="134.62" x2="88.9" y2="134.62" width="0.1524" layer="91"/>
<label x="88.9" y="134.62" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN318" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_OUT_PD10_RED" class="0">
<segment>
<wire x1="83.82" y1="139.7" x2="88.9" y2="139.7" width="0.1524" layer="91"/>
<label x="88.9" y="139.7" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN318" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_PD10_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="83.82" y1="129.54" x2="88.9" y2="129.54" width="0.1524" layer="91"/>
<label x="88.9" y="129.54" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN318" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_PD10_UNLOCK" class="0">
<segment>
<wire x1="83.82" y1="114.3" x2="88.9" y2="114.3" width="0.1524" layer="91"/>
<label x="88.9" y="114.3" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN318" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_OUT_PD10_VALVE_DOWN" class="0">
<segment>
<wire x1="83.82" y1="124.46" x2="88.9" y2="124.46" width="0.1524" layer="91"/>
<label x="88.9" y="124.46" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN318" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_PD10_VALVE_UP" class="0">
<segment>
<wire x1="83.82" y1="119.38" x2="88.9" y2="119.38" width="0.1524" layer="91"/>
<label x="88.9" y="119.38" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN318" gate="CN" pin="P2"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>UD-C26, UD-C24</description>
<plain>
<text x="167.64" y="27.94" size="2.54" layer="94">UD-C26 &amp; UD-C24 connectors</text>
<wire x1="22.86" y1="368.3" x2="22.86" y2="264.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="368.3" x2="22.86" y2="368.3" width="0.254" layer="94" style="shortdash"/>
<wire x1="22.86" y1="264.16" x2="76.2" y2="264.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="264.16" x2="76.2" y2="368.3" width="0.254" layer="94" style="shortdash"/>
<wire x1="78.74" y1="368.3" x2="78.74" y2="264.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="368.3" x2="78.74" y2="368.3" width="0.254" layer="94" style="shortdash"/>
<wire x1="78.74" y1="264.16" x2="132.08" y2="264.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="264.16" x2="132.08" y2="368.3" width="0.254" layer="94" style="shortdash"/>
<wire x1="22.86" y1="248.92" x2="76.2" y2="248.92" width="0.254" layer="94" style="shortdash"/>
<wire x1="22.86" y1="248.92" x2="22.86" y2="144.78" width="0.254" layer="94" style="shortdash"/>
<wire x1="76.2" y1="248.92" x2="76.2" y2="144.78" width="0.254" layer="94" style="shortdash"/>
<wire x1="22.86" y1="144.78" x2="76.2" y2="144.78" width="0.254" layer="94" style="shortdash"/>
<wire x1="78.74" y1="248.92" x2="132.08" y2="248.92" width="0.254" layer="94" style="shortdash"/>
<wire x1="78.74" y1="248.92" x2="78.74" y2="144.78" width="0.254" layer="94" style="shortdash"/>
<wire x1="132.08" y1="248.92" x2="132.08" y2="144.78" width="0.254" layer="94" style="shortdash"/>
<wire x1="78.74" y1="144.78" x2="132.08" y2="144.78" width="0.254" layer="94" style="shortdash"/>
<wire x1="134.62" y1="248.92" x2="187.96" y2="248.92" width="0.254" layer="94" style="shortdash"/>
<wire x1="134.62" y1="248.92" x2="134.62" y2="144.78" width="0.254" layer="94" style="shortdash"/>
<wire x1="187.96" y1="248.92" x2="187.96" y2="144.78" width="0.254" layer="94" style="shortdash"/>
<wire x1="134.62" y1="144.78" x2="187.96" y2="144.78" width="0.254" layer="94" style="shortdash"/>
</plain>
<instances>
<instance part="FRAME401" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME401" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="X403" gate="X" x="40.64" y="269.24" smashed="yes">
<attribute name="NAME" x="43.18" y="271.78" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="43.18" y="269.24" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="43.18" y="266.7" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="40.64" y="269.24" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="40.64" y="269.24" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X401" gate="X" x="40.64" y="320.04" smashed="yes">
<attribute name="NAME" x="43.18" y="322.58" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="43.18" y="320.04" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="43.18" y="317.5" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="40.64" y="320.04" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="40.64" y="320.04" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X404" gate="X" x="96.52" y="269.24" smashed="yes">
<attribute name="NAME" x="99.06" y="271.78" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="99.06" y="269.24" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="99.06" y="266.7" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="96.52" y="269.24" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="96.52" y="269.24" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X402" gate="X" x="96.52" y="320.04" smashed="yes">
<attribute name="NAME" x="99.06" y="322.58" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="99.06" y="320.04" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="99.06" y="317.5" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="96.52" y="320.04" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="96.52" y="320.04" size="1.27" layer="96" display="off"/>
</instance>
<instance part="CN403" gate="CN" x="27.94" y="294.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="25.4" y="271.78" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="25.4" y="269.24" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN401" gate="CN" x="27.94" y="345.44" smashed="yes" rot="MR0">
<attribute name="NAME" x="25.4" y="322.58" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="25.4" y="320.04" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN402" gate="CN" x="83.82" y="345.44" smashed="yes" rot="MR0">
<attribute name="NAME" x="81.28" y="322.58" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="81.28" y="320.04" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN404" gate="CN" x="83.82" y="294.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="81.28" y="271.78" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="81.28" y="269.24" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN408" gate="CN" x="27.94" y="175.26" smashed="yes" rot="MR0">
<attribute name="NAME" x="25.4" y="152.4" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="25.4" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN405" gate="CN" x="27.94" y="226.06" smashed="yes" rot="MR0">
<attribute name="NAME" x="25.4" y="203.2" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="25.4" y="200.66" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X408" gate="X" x="40.64" y="149.86" smashed="yes">
<attribute name="NAME" x="43.18" y="152.4" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="43.18" y="149.86" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="43.18" y="147.32" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="40.64" y="149.86" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="40.64" y="149.86" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X405" gate="X" x="40.64" y="200.66" smashed="yes">
<attribute name="NAME" x="43.18" y="203.2" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="43.18" y="200.66" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="43.18" y="198.12" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="40.64" y="200.66" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="40.64" y="200.66" size="1.27" layer="96" display="off"/>
</instance>
<instance part="CN409" gate="CN" x="83.82" y="175.26" smashed="yes" rot="MR0">
<attribute name="NAME" x="81.28" y="152.4" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="81.28" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN406" gate="CN" x="83.82" y="226.06" smashed="yes" rot="MR0">
<attribute name="NAME" x="81.28" y="203.2" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="81.28" y="200.66" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X409" gate="X" x="96.52" y="149.86" smashed="yes">
<attribute name="NAME" x="99.06" y="152.4" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="99.06" y="149.86" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="99.06" y="147.32" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="96.52" y="149.86" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="96.52" y="149.86" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X406" gate="X" x="96.52" y="200.66" smashed="yes">
<attribute name="NAME" x="99.06" y="203.2" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="99.06" y="200.66" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="99.06" y="198.12" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="96.52" y="200.66" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="96.52" y="200.66" size="1.27" layer="96" display="off"/>
</instance>
<instance part="CN410" gate="CN" x="139.7" y="175.26" smashed="yes" rot="MR0">
<attribute name="NAME" x="137.16" y="152.4" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="137.16" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="CN407" gate="CN" x="139.7" y="226.06" smashed="yes" rot="MR0">
<attribute name="NAME" x="137.16" y="203.2" size="2" layer="95" font="vector" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="137.16" y="200.66" size="2" layer="96" font="vector" ratio="10" rot="MR180"/>
</instance>
<instance part="X410" gate="X" x="152.4" y="149.86" smashed="yes">
<attribute name="NAME" x="154.94" y="152.4" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="154.94" y="149.86" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="154.94" y="147.32" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="152.4" y="149.86" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="152.4" y="149.86" size="1.27" layer="96" display="off"/>
</instance>
<instance part="X407" gate="X" x="152.4" y="200.66" smashed="yes">
<attribute name="NAME" x="154.94" y="203.2" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="154.94" y="200.66" size="1.778" layer="96" align="center-left"/>
<attribute name="DESCRIPTION" x="154.94" y="198.12" size="1.778" layer="97" align="center-left"/>
<attribute name="MF" x="152.4" y="200.66" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="152.4" y="200.66" size="1.27" layer="96" display="off"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="SIG_IN_SP1_SHAKER_UP" class="0">
<segment>
<wire x1="30.48" y1="312.42" x2="38.1" y2="312.42" width="0.1524" layer="91"/>
<label x="38.1" y="312.42" size="1.27" layer="95" xref="yes"/>
<pinref part="CN403" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_SP1_PORT_EXTENDED" class="0">
<segment>
<wire x1="38.1" y1="327.66" x2="30.48" y2="327.66" width="0.1524" layer="91"/>
<label x="38.1" y="327.66" size="1.27" layer="95" xref="yes"/>
<pinref part="CN401" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_IN_SP1_PORT_RETRACTED" class="0">
<segment>
<wire x1="30.48" y1="332.74" x2="38.1" y2="332.74" width="0.1524" layer="91"/>
<label x="38.1" y="332.74" size="1.27" layer="95" xref="yes"/>
<pinref part="CN401" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_SP1_CHEST_CLOSED" class="0">
<segment>
<wire x1="30.48" y1="337.82" x2="38.1" y2="337.82" width="0.1524" layer="91"/>
<label x="38.1" y="337.82" size="1.27" layer="95" xref="yes"/>
<pinref part="CN401" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_SP1_PRESSURE2" class="0">
<segment>
<wire x1="30.48" y1="363.22" x2="38.1" y2="363.22" width="0.1524" layer="91"/>
<label x="38.1" y="363.22" size="1.27" layer="95" xref="yes"/>
<pinref part="CN401" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_SP1_BOX1" class="0">
<segment>
<wire x1="30.48" y1="342.9" x2="38.1" y2="342.9" width="0.1524" layer="91"/>
<label x="38.1" y="342.9" size="1.27" layer="95" xref="yes"/>
<pinref part="CN401" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_SP1_BOX2" class="0">
<segment>
<wire x1="30.48" y1="347.98" x2="38.1" y2="347.98" width="0.1524" layer="91"/>
<label x="38.1" y="347.98" size="1.27" layer="95" xref="yes"/>
<pinref part="CN401" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_SP1_PRESSURE1" class="0">
<segment>
<wire x1="30.48" y1="358.14" x2="38.1" y2="358.14" width="0.1524" layer="91"/>
<label x="38.1" y="358.14" size="1.27" layer="95" xref="yes"/>
<pinref part="CN401" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_SP1_CURTAIN" class="0">
<segment>
<wire x1="30.48" y1="353.06" x2="38.1" y2="353.06" width="0.1524" layer="91"/>
<label x="38.1" y="353.06" size="1.27" layer="95" xref="yes"/>
<pinref part="CN401" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_SP1_VALVE11" class="0">
<segment>
<wire x1="30.48" y1="287.02" x2="38.1" y2="287.02" width="0.1524" layer="91"/>
<label x="38.1" y="287.02" size="1.27" layer="95" xref="yes"/>
<pinref part="CN403" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_SP1_VALVE12" class="0">
<segment>
<wire x1="30.48" y1="292.1" x2="38.1" y2="292.1" width="0.1524" layer="91"/>
<label x="38.1" y="292.1" size="1.27" layer="95" xref="yes"/>
<pinref part="CN403" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_SP1_VALVE21" class="0">
<segment>
<wire x1="30.48" y1="297.18" x2="38.1" y2="297.18" width="0.1524" layer="91"/>
<label x="38.1" y="297.18" size="1.27" layer="95" xref="yes"/>
<pinref part="CN403" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_OUT_SP1_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="30.48" y1="302.26" x2="38.1" y2="302.26" width="0.1524" layer="91"/>
<label x="38.1" y="302.26" size="1.27" layer="95" xref="yes"/>
<pinref part="CN403" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_SP1_LOCK" class="0">
<segment>
<wire x1="30.48" y1="307.34" x2="38.1" y2="307.34" width="0.1524" layer="91"/>
<label x="38.1" y="307.34" size="1.27" layer="95" xref="yes"/>
<pinref part="CN403" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_OB1_NORMAL_LEFT" class="0">
<segment>
<pinref part="CN408" gate="CN" pin="P7"/>
<wire x1="30.48" y1="187.96" x2="38.1" y2="187.96" width="0.1524" layer="91"/>
<label x="38.1" y="187.96" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_OB1_NORMAL_RIGHT" class="0">
<segment>
<wire x1="30.48" y1="208.28" x2="38.1" y2="208.28" width="0.1524" layer="91"/>
<label x="38.1" y="208.28" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN405" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_IN_OB1_ROTATED_LEFT" class="0">
<segment>
<pinref part="CN408" gate="CN" pin="P8"/>
<wire x1="30.48" y1="193.04" x2="38.1" y2="193.04" width="0.1524" layer="91"/>
<label x="38.1" y="193.04" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_OB1_ROTATED_RIGHT" class="0">
<segment>
<wire x1="30.48" y1="213.36" x2="38.1" y2="213.36" width="0.1524" layer="91"/>
<label x="38.1" y="213.36" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN405" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_OUT_OB1_VALVE_EXTEND" class="0">
<segment>
<pinref part="CN408" gate="CN" pin="P3"/>
<wire x1="30.48" y1="167.64" x2="38.1" y2="167.64" width="0.1524" layer="91"/>
<label x="38.1" y="167.64" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_OB1_VALVE_RETRACT" class="0">
<segment>
<pinref part="CN408" gate="CN" pin="P4"/>
<wire x1="30.48" y1="172.72" x2="38.1" y2="172.72" width="0.1524" layer="91"/>
<label x="38.1" y="172.72" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_OB1_LOCKED" class="0">
<segment>
<wire x1="30.48" y1="218.44" x2="38.1" y2="218.44" width="0.1524" layer="91"/>
<label x="38.1" y="218.44" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN405" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_OB1_UNLOCKED" class="0">
<segment>
<wire x1="30.48" y1="223.52" x2="38.1" y2="223.52" width="0.1524" layer="91"/>
<label x="38.1" y="223.52" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN405" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_OB1_BLOCK_UP_LEFT" class="0">
<segment>
<wire x1="30.48" y1="228.6" x2="38.1" y2="228.6" width="0.1524" layer="91"/>
<label x="38.1" y="228.6" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN405" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_OB1_BLOCK_DOWN_LEFT" class="0">
<segment>
<wire x1="30.48" y1="233.68" x2="38.1" y2="233.68" width="0.1524" layer="91"/>
<label x="38.1" y="233.68" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN405" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_IN_OB1_BLOCK_UP_RIGHT" class="0">
<segment>
<wire x1="30.48" y1="238.76" x2="38.1" y2="238.76" width="0.1524" layer="91"/>
<label x="38.1" y="238.76" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN405" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_OB1_BLOCK_DOWN_RIGHT" class="0">
<segment>
<wire x1="30.48" y1="243.84" x2="38.1" y2="243.84" width="0.1524" layer="91"/>
<label x="38.1" y="243.84" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN405" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_OUT_OB1_SECONDARY_RELAYS" class="0">
<segment>
<pinref part="CN408" gate="CN" pin="P5"/>
<wire x1="30.48" y1="177.8" x2="38.1" y2="177.8" width="0.1524" layer="91"/>
<label x="38.1" y="177.8" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_OB1_VALVE_LOCK" class="0">
<segment>
<pinref part="CN408" gate="CN" pin="P6"/>
<wire x1="30.48" y1="182.88" x2="38.1" y2="182.88" width="0.1524" layer="91"/>
<label x="38.1" y="182.88" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_SP2_BOX1" class="0">
<segment>
<wire x1="86.36" y1="342.9" x2="93.98" y2="342.9" width="0.1524" layer="91"/>
<label x="93.98" y="342.9" size="1.27" layer="95" xref="yes"/>
<pinref part="CN402" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_IN_SP2_BOX2" class="0">
<segment>
<wire x1="86.36" y1="347.98" x2="93.98" y2="347.98" width="0.1524" layer="91"/>
<label x="93.98" y="347.98" size="1.27" layer="95" xref="yes"/>
<pinref part="CN402" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_SP2_CHEST_CLOSED" class="0">
<segment>
<wire x1="86.36" y1="337.82" x2="93.98" y2="337.82" width="0.1524" layer="91"/>
<label x="93.98" y="337.82" size="1.27" layer="95" xref="yes"/>
<pinref part="CN402" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_SP2_CURTAIN" class="0">
<segment>
<wire x1="86.36" y1="353.06" x2="93.98" y2="353.06" width="0.1524" layer="91"/>
<label x="93.98" y="353.06" size="1.27" layer="95" xref="yes"/>
<pinref part="CN402" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_IN_SP2_PORT_EXTENDED" class="0">
<segment>
<wire x1="93.98" y1="327.66" x2="86.36" y2="327.66" width="0.1524" layer="91"/>
<label x="93.98" y="327.66" size="1.27" layer="95" xref="yes"/>
<pinref part="CN402" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_IN_SP2_PORT_RETRACTED" class="0">
<segment>
<wire x1="86.36" y1="332.74" x2="93.98" y2="332.74" width="0.1524" layer="91"/>
<label x="93.98" y="332.74" size="1.27" layer="95" xref="yes"/>
<pinref part="CN402" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_SP2_PRESSURE1" class="0">
<segment>
<wire x1="86.36" y1="358.14" x2="93.98" y2="358.14" width="0.1524" layer="91"/>
<label x="93.98" y="358.14" size="1.27" layer="95" xref="yes"/>
<pinref part="CN402" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_SP2_PRESSURE2" class="0">
<segment>
<wire x1="86.36" y1="363.22" x2="93.98" y2="363.22" width="0.1524" layer="91"/>
<label x="93.98" y="363.22" size="1.27" layer="95" xref="yes"/>
<pinref part="CN402" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_SP2_SHAKER_UP" class="0">
<segment>
<wire x1="86.36" y1="312.42" x2="93.98" y2="312.42" width="0.1524" layer="91"/>
<label x="93.98" y="312.42" size="1.27" layer="95" xref="yes"/>
<pinref part="CN404" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_OUT_SP2_LOCK" class="0">
<segment>
<wire x1="86.36" y1="307.34" x2="93.98" y2="307.34" width="0.1524" layer="91"/>
<label x="93.98" y="307.34" size="1.27" layer="95" xref="yes"/>
<pinref part="CN404" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_OUT_SP2_SECONDARY_RELAYS" class="0">
<segment>
<wire x1="86.36" y1="302.26" x2="93.98" y2="302.26" width="0.1524" layer="91"/>
<label x="93.98" y="302.26" size="1.27" layer="95" xref="yes"/>
<pinref part="CN404" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_OUT_SP2_VALVE11" class="0">
<segment>
<wire x1="86.36" y1="287.02" x2="93.98" y2="287.02" width="0.1524" layer="91"/>
<label x="93.98" y="287.02" size="1.27" layer="95" xref="yes"/>
<pinref part="CN404" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_OUT_SP2_VALVE12" class="0">
<segment>
<wire x1="86.36" y1="292.1" x2="93.98" y2="292.1" width="0.1524" layer="91"/>
<label x="93.98" y="292.1" size="1.27" layer="95" xref="yes"/>
<pinref part="CN404" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_SP2_VALVE21" class="0">
<segment>
<wire x1="86.36" y1="297.18" x2="93.98" y2="297.18" width="0.1524" layer="91"/>
<label x="93.98" y="297.18" size="1.27" layer="95" xref="yes"/>
<pinref part="CN404" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_OB2_BLOCK_DOWN_LEFT" class="0">
<segment>
<wire x1="86.36" y1="233.68" x2="93.98" y2="233.68" width="0.1524" layer="91"/>
<label x="93.98" y="233.68" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN406" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_IN_OB2_BLOCK_DOWN_RIGHT" class="0">
<segment>
<wire x1="86.36" y1="243.84" x2="93.98" y2="243.84" width="0.1524" layer="91"/>
<label x="93.98" y="243.84" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN406" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_OB2_BLOCK_UP_LEFT" class="0">
<segment>
<wire x1="86.36" y1="228.6" x2="93.98" y2="228.6" width="0.1524" layer="91"/>
<label x="93.98" y="228.6" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN406" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_OB2_BLOCK_UP_RIGHT" class="0">
<segment>
<wire x1="86.36" y1="238.76" x2="93.98" y2="238.76" width="0.1524" layer="91"/>
<label x="93.98" y="238.76" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN406" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_OB2_LOCKED" class="0">
<segment>
<wire x1="86.36" y1="218.44" x2="93.98" y2="218.44" width="0.1524" layer="91"/>
<label x="93.98" y="218.44" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN406" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_OB2_NORMAL_LEFT" class="0">
<segment>
<pinref part="CN409" gate="CN" pin="P7"/>
<wire x1="86.36" y1="187.96" x2="93.98" y2="187.96" width="0.1524" layer="91"/>
<label x="93.98" y="187.96" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_OB2_NORMAL_RIGHT" class="0">
<segment>
<wire x1="86.36" y1="208.28" x2="93.98" y2="208.28" width="0.1524" layer="91"/>
<label x="93.98" y="208.28" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN406" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_IN_OB2_ROTATED_LEFT" class="0">
<segment>
<pinref part="CN409" gate="CN" pin="P8"/>
<wire x1="86.36" y1="193.04" x2="93.98" y2="193.04" width="0.1524" layer="91"/>
<label x="93.98" y="193.04" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_OB2_ROTATED_RIGHT" class="0">
<segment>
<wire x1="86.36" y1="213.36" x2="93.98" y2="213.36" width="0.1524" layer="91"/>
<label x="93.98" y="213.36" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN406" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_OB2_UNLOCKED" class="0">
<segment>
<wire x1="86.36" y1="223.52" x2="93.98" y2="223.52" width="0.1524" layer="91"/>
<label x="93.98" y="223.52" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN406" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_OB2_SECONDARY_RELAYS" class="0">
<segment>
<pinref part="CN409" gate="CN" pin="P5"/>
<wire x1="86.36" y1="177.8" x2="93.98" y2="177.8" width="0.1524" layer="91"/>
<label x="93.98" y="177.8" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_OB2_VALVE_EXTEND" class="0">
<segment>
<pinref part="CN409" gate="CN" pin="P3"/>
<wire x1="86.36" y1="167.64" x2="93.98" y2="167.64" width="0.1524" layer="91"/>
<label x="93.98" y="167.64" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_OB2_VALVE_LOCK" class="0">
<segment>
<pinref part="CN409" gate="CN" pin="P6"/>
<wire x1="86.36" y1="182.88" x2="93.98" y2="182.88" width="0.1524" layer="91"/>
<label x="93.98" y="182.88" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_OB2_VALVE_RETRACT" class="0">
<segment>
<pinref part="CN409" gate="CN" pin="P4"/>
<wire x1="86.36" y1="172.72" x2="93.98" y2="172.72" width="0.1524" layer="91"/>
<label x="93.98" y="172.72" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_OB3_BLOCK_DOWN_LEFT" class="0">
<segment>
<wire x1="142.24" y1="233.68" x2="149.86" y2="233.68" width="0.1524" layer="91"/>
<label x="149.86" y="233.68" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN407" gate="CN" pin="P6"/>
</segment>
</net>
<net name="SIG_IN_OB3_BLOCK_DOWN_RIGHT" class="0">
<segment>
<wire x1="142.24" y1="243.84" x2="149.86" y2="243.84" width="0.1524" layer="91"/>
<label x="149.86" y="243.84" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN407" gate="CN" pin="P8"/>
</segment>
</net>
<net name="SIG_IN_OB3_BLOCK_UP_LEFT" class="0">
<segment>
<wire x1="142.24" y1="228.6" x2="149.86" y2="228.6" width="0.1524" layer="91"/>
<label x="149.86" y="228.6" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN407" gate="CN" pin="P5"/>
</segment>
</net>
<net name="SIG_IN_OB3_BLOCK_UP_RIGHT" class="0">
<segment>
<wire x1="142.24" y1="238.76" x2="149.86" y2="238.76" width="0.1524" layer="91"/>
<label x="149.86" y="238.76" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN407" gate="CN" pin="P7"/>
</segment>
</net>
<net name="SIG_IN_OB3_LOCKED" class="0">
<segment>
<wire x1="142.24" y1="218.44" x2="149.86" y2="218.44" width="0.1524" layer="91"/>
<label x="149.86" y="218.44" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN407" gate="CN" pin="P3"/>
</segment>
</net>
<net name="SIG_IN_OB3_NORMAL_LEFT" class="0">
<segment>
<pinref part="CN410" gate="CN" pin="P7"/>
<wire x1="142.24" y1="187.96" x2="149.86" y2="187.96" width="0.1524" layer="91"/>
<label x="149.86" y="187.96" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_OB3_NORMAL_RIGHT" class="0">
<segment>
<wire x1="142.24" y1="208.28" x2="149.86" y2="208.28" width="0.1524" layer="91"/>
<label x="149.86" y="208.28" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN407" gate="CN" pin="P1"/>
</segment>
</net>
<net name="SIG_IN_OB3_ROTATED_LEFT" class="0">
<segment>
<pinref part="CN410" gate="CN" pin="P8"/>
<wire x1="142.24" y1="193.04" x2="149.86" y2="193.04" width="0.1524" layer="91"/>
<label x="149.86" y="193.04" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_IN_OB3_ROTATED_RIGHT" class="0">
<segment>
<wire x1="142.24" y1="213.36" x2="149.86" y2="213.36" width="0.1524" layer="91"/>
<label x="149.86" y="213.36" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN407" gate="CN" pin="P2"/>
</segment>
</net>
<net name="SIG_IN_OB3_UNLOCKED" class="0">
<segment>
<wire x1="142.24" y1="223.52" x2="149.86" y2="223.52" width="0.1524" layer="91"/>
<label x="149.86" y="223.52" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN407" gate="CN" pin="P4"/>
</segment>
</net>
<net name="SIG_OUT_OB3_SECONDARY_RELAYS" class="0">
<segment>
<pinref part="CN410" gate="CN" pin="P5"/>
<wire x1="142.24" y1="177.8" x2="149.86" y2="177.8" width="0.1524" layer="91"/>
<label x="149.86" y="177.8" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_OB3_VALVE_EXTEND" class="0">
<segment>
<pinref part="CN410" gate="CN" pin="P3"/>
<wire x1="142.24" y1="167.64" x2="149.86" y2="167.64" width="0.1524" layer="91"/>
<label x="149.86" y="167.64" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_OB3_VALVE_LOCK" class="0">
<segment>
<pinref part="CN410" gate="CN" pin="P6"/>
<wire x1="142.24" y1="182.88" x2="149.86" y2="182.88" width="0.1524" layer="91"/>
<label x="149.86" y="182.88" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SIG_OUT_OB3_VALVE_RETRACT" class="0">
<segment>
<pinref part="CN410" gate="CN" pin="P4"/>
<wire x1="142.24" y1="172.72" x2="149.86" y2="172.72" width="0.1524" layer="91"/>
<label x="149.86" y="172.72" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="113,1,252.794,28.7232,LOGO101,,,,,"/>
<approved hash="113,2,131.976,194.206,FRAME201,,,,,"/>
<approved hash="113,1,131.976,194.206,FRAME101,,,,,"/>
<approved hash="113,3,213.131,17.5514,FRAME401,,,,,"/>
<approved hash="113,2,406.296,194.206,FRAME501,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
