<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.1">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Warning" color="59" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DINA4_L" urn="urn:adsk.eagle:symbol:13867/1" library_version="1">
<frame x1="0" y1="0" x2="264.16" y2="180.34" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD" urn="urn:adsk.eagle:symbol:13864/1" library_version="1">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" urn="urn:adsk.eagle:component:13919/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-logo">
<packages>
<package name="UDO-LOGO-10MM" urn="urn:adsk.eagle:footprint:6649251/1">
<rectangle x1="1.397" y1="0.127" x2="1.82118125" y2="0.21158125" layer="21"/>
<rectangle x1="3.85318125" y1="0.127" x2="4.191" y2="0.21158125" layer="21"/>
<rectangle x1="4.953" y1="0.127" x2="5.03681875" y2="0.21158125" layer="21"/>
<rectangle x1="5.63118125" y1="0.127" x2="5.715" y2="0.21158125" layer="21"/>
<rectangle x1="6.223" y1="0.127" x2="6.30681875" y2="0.21158125" layer="21"/>
<rectangle x1="7.06881875" y1="0.127" x2="7.40918125" y2="0.21158125" layer="21"/>
<rectangle x1="8.84681875" y1="0.127" x2="8.93318125" y2="0.21158125" layer="21"/>
<rectangle x1="1.22681875" y1="0.21158125" x2="1.905" y2="0.29641875" layer="21"/>
<rectangle x1="2.159" y1="0.21158125" x2="2.32918125" y2="0.29641875" layer="21"/>
<rectangle x1="2.75081875" y1="0.21158125" x2="2.83718125" y2="0.29641875" layer="21"/>
<rectangle x1="2.921" y1="0.21158125" x2="3.09118125" y2="0.29641875" layer="21"/>
<rectangle x1="3.34518125" y1="0.21158125" x2="3.59918125" y2="0.29641875" layer="21"/>
<rectangle x1="3.683" y1="0.21158125" x2="4.36118125" y2="0.29641875" layer="21"/>
<rectangle x1="4.78281875" y1="0.21158125" x2="5.207" y2="0.29641875" layer="21"/>
<rectangle x1="5.461" y1="0.21158125" x2="5.79881875" y2="0.29641875" layer="21"/>
<rectangle x1="6.05281875" y1="0.21158125" x2="6.477" y2="0.29641875" layer="21"/>
<rectangle x1="6.90118125" y1="0.21158125" x2="7.57681875" y2="0.29641875" layer="21"/>
<rectangle x1="7.747" y1="0.21158125" x2="7.91718125" y2="0.29641875" layer="21"/>
<rectangle x1="8.33881875" y1="0.21158125" x2="8.509" y2="0.29641875" layer="21"/>
<rectangle x1="8.67918125" y1="0.21158125" x2="9.10081875" y2="0.29641875" layer="21"/>
<rectangle x1="1.22681875" y1="0.29641875" x2="1.397" y2="0.381" layer="21"/>
<rectangle x1="1.82118125" y1="0.29641875" x2="1.98881875" y2="0.381" layer="21"/>
<rectangle x1="2.159" y1="0.29641875" x2="2.32918125" y2="0.381" layer="21"/>
<rectangle x1="2.75081875" y1="0.29641875" x2="2.83718125" y2="0.381" layer="21"/>
<rectangle x1="2.921" y1="0.29641875" x2="3.09118125" y2="0.381" layer="21"/>
<rectangle x1="3.25881875" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="3.683" y1="0.29641875" x2="3.85318125" y2="0.381" layer="21"/>
<rectangle x1="4.27481875" y1="0.29641875" x2="4.445" y2="0.381" layer="21"/>
<rectangle x1="4.699" y1="0.29641875" x2="4.86918125" y2="0.381" layer="21"/>
<rectangle x1="5.12318125" y1="0.29641875" x2="5.29081875" y2="0.381" layer="21"/>
<rectangle x1="5.37718125" y1="0.29641875" x2="5.54481875" y2="0.381" layer="21"/>
<rectangle x1="5.715" y1="0.29641875" x2="5.88518125" y2="0.381" layer="21"/>
<rectangle x1="5.969" y1="0.29641875" x2="6.13918125" y2="0.381" layer="21"/>
<rectangle x1="6.39318125" y1="0.29641875" x2="6.56081875" y2="0.381" layer="21"/>
<rectangle x1="6.81481875" y1="0.29641875" x2="6.985" y2="0.381" layer="21"/>
<rectangle x1="7.493" y1="0.29641875" x2="7.66318125" y2="0.381" layer="21"/>
<rectangle x1="7.747" y1="0.29641875" x2="7.91718125" y2="0.381" layer="21"/>
<rectangle x1="8.33881875" y1="0.29641875" x2="8.509" y2="0.381" layer="21"/>
<rectangle x1="8.59281875" y1="0.29641875" x2="8.763" y2="0.381" layer="21"/>
<rectangle x1="8.93318125" y1="0.29641875" x2="9.18718125" y2="0.381" layer="21"/>
<rectangle x1="1.143" y1="0.381" x2="1.31318125" y2="0.46558125" layer="21"/>
<rectangle x1="1.905" y1="0.381" x2="2.07518125" y2="0.46558125" layer="21"/>
<rectangle x1="2.159" y1="0.381" x2="2.32918125" y2="0.46558125" layer="21"/>
<rectangle x1="2.75081875" y1="0.381" x2="2.83718125" y2="0.46558125" layer="21"/>
<rectangle x1="2.921" y1="0.381" x2="3.09118125" y2="0.46558125" layer="21"/>
<rectangle x1="3.175" y1="0.381" x2="3.34518125" y2="0.46558125" layer="21"/>
<rectangle x1="3.683" y1="0.381" x2="3.85318125" y2="0.46558125" layer="21"/>
<rectangle x1="4.36118125" y1="0.381" x2="4.52881875" y2="0.46558125" layer="21"/>
<rectangle x1="4.699" y1="0.381" x2="4.78281875" y2="0.46558125" layer="21"/>
<rectangle x1="5.207" y1="0.381" x2="5.37718125" y2="0.46558125" layer="21"/>
<rectangle x1="5.715" y1="0.381" x2="5.88518125" y2="0.46558125" layer="21"/>
<rectangle x1="5.969" y1="0.381" x2="6.05281875" y2="0.46558125" layer="21"/>
<rectangle x1="6.477" y1="0.381" x2="6.56081875" y2="0.46558125" layer="21"/>
<rectangle x1="6.81481875" y1="0.381" x2="6.90118125" y2="0.46558125" layer="21"/>
<rectangle x1="7.57681875" y1="0.381" x2="7.66318125" y2="0.46558125" layer="21"/>
<rectangle x1="7.747" y1="0.381" x2="7.91718125" y2="0.46558125" layer="21"/>
<rectangle x1="8.33881875" y1="0.381" x2="8.67918125" y2="0.46558125" layer="21"/>
<rectangle x1="9.017" y1="0.381" x2="9.18718125" y2="0.46558125" layer="21"/>
<rectangle x1="1.143" y1="0.46558125" x2="1.22681875" y2="0.55041875" layer="21"/>
<rectangle x1="1.905" y1="0.46558125" x2="2.07518125" y2="0.55041875" layer="21"/>
<rectangle x1="2.159" y1="0.46558125" x2="2.32918125" y2="0.55041875" layer="21"/>
<rectangle x1="2.75081875" y1="0.46558125" x2="2.83718125" y2="0.55041875" layer="21"/>
<rectangle x1="2.921" y1="0.46558125" x2="3.09118125" y2="0.55041875" layer="21"/>
<rectangle x1="3.175" y1="0.46558125" x2="3.34518125" y2="0.55041875" layer="21"/>
<rectangle x1="3.683" y1="0.46558125" x2="3.85318125" y2="0.55041875" layer="21"/>
<rectangle x1="4.445" y1="0.46558125" x2="4.78281875" y2="0.55041875" layer="21"/>
<rectangle x1="5.207" y1="0.46558125" x2="5.37718125" y2="0.55041875" layer="21"/>
<rectangle x1="5.54481875" y1="0.46558125" x2="5.79881875" y2="0.55041875" layer="21"/>
<rectangle x1="5.88518125" y1="0.46558125" x2="6.64718125" y2="0.55041875" layer="21"/>
<rectangle x1="6.731" y1="0.46558125" x2="6.90118125" y2="0.55041875" layer="21"/>
<rectangle x1="7.57681875" y1="0.46558125" x2="7.91718125" y2="0.55041875" layer="21"/>
<rectangle x1="8.33881875" y1="0.46558125" x2="9.18718125" y2="0.55041875" layer="21"/>
<rectangle x1="1.05918125" y1="0.55041875" x2="1.22681875" y2="0.635" layer="21"/>
<rectangle x1="1.98881875" y1="0.55041875" x2="2.07518125" y2="0.635" layer="21"/>
<rectangle x1="2.159" y1="0.55041875" x2="2.32918125" y2="0.635" layer="21"/>
<rectangle x1="2.75081875" y1="0.55041875" x2="2.83718125" y2="0.635" layer="21"/>
<rectangle x1="2.921" y1="0.55041875" x2="3.09118125" y2="0.635" layer="21"/>
<rectangle x1="3.175" y1="0.55041875" x2="3.34518125" y2="0.635" layer="21"/>
<rectangle x1="3.683" y1="0.55041875" x2="3.85318125" y2="0.635" layer="21"/>
<rectangle x1="4.445" y1="0.55041875" x2="4.78281875" y2="0.635" layer="21"/>
<rectangle x1="5.207" y1="0.55041875" x2="5.715" y2="0.635" layer="21"/>
<rectangle x1="5.88518125" y1="0.55041875" x2="6.05281875" y2="0.635" layer="21"/>
<rectangle x1="6.13918125" y1="0.55041875" x2="6.39318125" y2="0.635" layer="21"/>
<rectangle x1="6.477" y1="0.55041875" x2="6.64718125" y2="0.635" layer="21"/>
<rectangle x1="6.731" y1="0.55041875" x2="6.90118125" y2="0.635" layer="21"/>
<rectangle x1="7.57681875" y1="0.55041875" x2="7.91718125" y2="0.635" layer="21"/>
<rectangle x1="8.33881875" y1="0.55041875" x2="8.67918125" y2="0.635" layer="21"/>
<rectangle x1="8.763" y1="0.55041875" x2="9.18718125" y2="0.635" layer="21"/>
<rectangle x1="1.05918125" y1="0.635" x2="1.22681875" y2="0.71958125" layer="21"/>
<rectangle x1="1.98881875" y1="0.635" x2="2.07518125" y2="0.71958125" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.32918125" y2="0.71958125" layer="21"/>
<rectangle x1="2.667" y1="0.635" x2="2.83718125" y2="0.71958125" layer="21"/>
<rectangle x1="2.921" y1="0.635" x2="3.09118125" y2="0.71958125" layer="21"/>
<rectangle x1="3.175" y1="0.635" x2="3.34518125" y2="0.71958125" layer="21"/>
<rectangle x1="3.683" y1="0.635" x2="3.85318125" y2="0.71958125" layer="21"/>
<rectangle x1="4.445" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="4.699" y1="0.635" x2="4.86918125" y2="0.71958125" layer="21"/>
<rectangle x1="5.12318125" y1="0.635" x2="5.29081875" y2="0.71958125" layer="21"/>
<rectangle x1="5.37718125" y1="0.635" x2="5.54481875" y2="0.71958125" layer="21"/>
<rectangle x1="5.715" y1="0.635" x2="5.88518125" y2="0.71958125" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.13918125" y2="0.71958125" layer="21"/>
<rectangle x1="6.39318125" y1="0.635" x2="6.56081875" y2="0.71958125" layer="21"/>
<rectangle x1="6.731" y1="0.635" x2="6.90118125" y2="0.71958125" layer="21"/>
<rectangle x1="7.57681875" y1="0.635" x2="7.747" y2="0.71958125" layer="21"/>
<rectangle x1="7.83081875" y1="0.635" x2="8.001" y2="0.71958125" layer="21"/>
<rectangle x1="8.255" y1="0.635" x2="8.42518125" y2="0.71958125" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="8.67918125" y2="0.71958125" layer="21"/>
<rectangle x1="9.017" y1="0.635" x2="9.18718125" y2="0.71958125" layer="21"/>
<rectangle x1="1.05918125" y1="0.71958125" x2="1.22681875" y2="0.80441875" layer="21"/>
<rectangle x1="1.98881875" y1="0.71958125" x2="2.07518125" y2="0.80441875" layer="21"/>
<rectangle x1="2.159" y1="0.71958125" x2="2.75081875" y2="0.80441875" layer="21"/>
<rectangle x1="2.921" y1="0.71958125" x2="3.59918125" y2="0.80441875" layer="21"/>
<rectangle x1="3.683" y1="0.71958125" x2="3.85318125" y2="0.80441875" layer="21"/>
<rectangle x1="4.445" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="4.78281875" y1="0.71958125" x2="5.29081875" y2="0.80441875" layer="21"/>
<rectangle x1="5.37718125" y1="0.71958125" x2="5.88518125" y2="0.80441875" layer="21"/>
<rectangle x1="6.05281875" y1="0.71958125" x2="6.56081875" y2="0.80441875" layer="21"/>
<rectangle x1="6.731" y1="0.71958125" x2="6.90118125" y2="0.80441875" layer="21"/>
<rectangle x1="7.57681875" y1="0.71958125" x2="8.33881875" y2="0.80441875" layer="21"/>
<rectangle x1="8.59281875" y1="0.71958125" x2="9.10081875" y2="0.80441875" layer="21"/>
<rectangle x1="1.05918125" y1="0.80441875" x2="1.22681875" y2="0.889" layer="21"/>
<rectangle x1="1.98881875" y1="0.80441875" x2="2.07518125" y2="0.889" layer="21"/>
<rectangle x1="2.159" y1="0.80441875" x2="2.24281875" y2="0.889" layer="21"/>
<rectangle x1="2.413" y1="0.80441875" x2="2.667" y2="0.889" layer="21"/>
<rectangle x1="2.921" y1="0.80441875" x2="3.00481875" y2="0.889" layer="21"/>
<rectangle x1="3.09118125" y1="0.80441875" x2="3.59918125" y2="0.889" layer="21"/>
<rectangle x1="3.683" y1="0.80441875" x2="3.85318125" y2="0.889" layer="21"/>
<rectangle x1="4.445" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="4.86918125" y1="0.80441875" x2="5.12318125" y2="0.889" layer="21"/>
<rectangle x1="5.54481875" y1="0.80441875" x2="5.715" y2="0.889" layer="21"/>
<rectangle x1="6.13918125" y1="0.80441875" x2="6.39318125" y2="0.889" layer="21"/>
<rectangle x1="6.81481875" y1="0.80441875" x2="6.90118125" y2="0.889" layer="21"/>
<rectangle x1="7.57681875" y1="0.80441875" x2="7.66318125" y2="0.889" layer="21"/>
<rectangle x1="8.001" y1="0.80441875" x2="8.255" y2="0.889" layer="21"/>
<rectangle x1="8.67918125" y1="0.80441875" x2="9.017" y2="0.889" layer="21"/>
<rectangle x1="1.05918125" y1="0.889" x2="1.22681875" y2="0.97358125" layer="21"/>
<rectangle x1="1.98881875" y1="0.889" x2="2.07518125" y2="0.97358125" layer="21"/>
<rectangle x1="2.921" y1="0.889" x2="3.09118125" y2="0.97358125" layer="21"/>
<rectangle x1="3.175" y1="0.889" x2="3.34518125" y2="0.97358125" layer="21"/>
<rectangle x1="3.683" y1="0.889" x2="3.85318125" y2="0.97358125" layer="21"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.52881875" y2="0.97358125" layer="21"/>
<rectangle x1="6.81481875" y1="0.889" x2="6.985" y2="0.97358125" layer="21"/>
<rectangle x1="7.493" y1="0.889" x2="7.66318125" y2="0.97358125" layer="21"/>
<rectangle x1="1.05918125" y1="0.97358125" x2="1.22681875" y2="1.05841875" layer="21"/>
<rectangle x1="1.98881875" y1="0.97358125" x2="2.07518125" y2="1.05841875" layer="21"/>
<rectangle x1="2.921" y1="0.97358125" x2="3.00481875" y2="1.05841875" layer="21"/>
<rectangle x1="3.175" y1="0.97358125" x2="3.34518125" y2="1.05841875" layer="21"/>
<rectangle x1="3.683" y1="0.97358125" x2="3.85318125" y2="1.05841875" layer="21"/>
<rectangle x1="4.27481875" y1="0.97358125" x2="4.445" y2="1.05841875" layer="21"/>
<rectangle x1="6.90118125" y1="0.97358125" x2="7.57681875" y2="1.05841875" layer="21"/>
<rectangle x1="1.05918125" y1="1.05841875" x2="1.22681875" y2="1.143" layer="21"/>
<rectangle x1="1.98881875" y1="1.05841875" x2="2.07518125" y2="1.143" layer="21"/>
<rectangle x1="3.175" y1="1.05841875" x2="3.34518125" y2="1.143" layer="21"/>
<rectangle x1="3.683" y1="1.05841875" x2="4.36118125" y2="1.143" layer="21"/>
<rectangle x1="7.06881875" y1="1.05841875" x2="7.40918125" y2="1.143" layer="21"/>
<rectangle x1="3.76681875" y1="1.143" x2="4.191" y2="1.22758125" layer="21"/>
<rectangle x1="1.82118125" y1="1.56641875" x2="8.255" y2="1.651" layer="21"/>
<rectangle x1="1.397" y1="1.651" x2="8.59281875" y2="1.73558125" layer="21"/>
<rectangle x1="1.143" y1="1.73558125" x2="8.84681875" y2="1.82041875" layer="21"/>
<rectangle x1="1.05918125" y1="1.82041875" x2="9.017" y2="1.905" layer="21"/>
<rectangle x1="0.889" y1="1.905" x2="1.651" y2="1.98958125" layer="21"/>
<rectangle x1="4.86918125" y1="1.905" x2="9.10081875" y2="1.98958125" layer="21"/>
<rectangle x1="0.80518125" y1="1.98958125" x2="1.397" y2="2.07441875" layer="21"/>
<rectangle x1="4.78281875" y1="1.98958125" x2="9.271" y2="2.07441875" layer="21"/>
<rectangle x1="0.635" y1="2.07441875" x2="1.22681875" y2="2.159" layer="21"/>
<rectangle x1="4.78281875" y1="2.07441875" x2="9.35481875" y2="2.159" layer="21"/>
<rectangle x1="0.55118125" y1="2.159" x2="1.05918125" y2="2.24358125" layer="21"/>
<rectangle x1="4.699" y1="2.159" x2="9.44118125" y2="2.24358125" layer="21"/>
<rectangle x1="0.46481875" y1="2.24358125" x2="0.97281875" y2="2.32841875" layer="21"/>
<rectangle x1="4.699" y1="2.24358125" x2="9.525" y2="2.32841875" layer="21"/>
<rectangle x1="0.46481875" y1="2.32841875" x2="0.80518125" y2="2.413" layer="21"/>
<rectangle x1="4.699" y1="2.32841875" x2="9.60881875" y2="2.413" layer="21"/>
<rectangle x1="0.381" y1="2.413" x2="0.71881875" y2="2.49758125" layer="21"/>
<rectangle x1="4.61518125" y1="2.413" x2="9.60881875" y2="2.49758125" layer="21"/>
<rectangle x1="0.29718125" y1="2.49758125" x2="0.71881875" y2="2.58241875" layer="21"/>
<rectangle x1="4.61518125" y1="2.49758125" x2="9.69518125" y2="2.58241875" layer="21"/>
<rectangle x1="0.29718125" y1="2.58241875" x2="0.635" y2="2.667" layer="21"/>
<rectangle x1="4.61518125" y1="2.58241875" x2="9.779" y2="2.667" layer="21"/>
<rectangle x1="0.21081875" y1="2.667" x2="0.55118125" y2="2.75158125" layer="21"/>
<rectangle x1="4.52881875" y1="2.667" x2="9.779" y2="2.75158125" layer="21"/>
<rectangle x1="0.21081875" y1="2.75158125" x2="0.55118125" y2="2.83641875" layer="21"/>
<rectangle x1="1.82118125" y1="2.75158125" x2="2.24281875" y2="2.83641875" layer="21"/>
<rectangle x1="2.921" y1="2.75158125" x2="3.51281875" y2="2.83641875" layer="21"/>
<rectangle x1="4.52881875" y1="2.75158125" x2="9.86281875" y2="2.83641875" layer="21"/>
<rectangle x1="0.127" y1="2.83641875" x2="0.46481875" y2="2.921" layer="21"/>
<rectangle x1="1.56718125" y1="2.83641875" x2="2.413" y2="2.921" layer="21"/>
<rectangle x1="2.83718125" y1="2.83641875" x2="3.683" y2="2.921" layer="21"/>
<rectangle x1="4.52881875" y1="2.83641875" x2="5.29081875" y2="2.921" layer="21"/>
<rectangle x1="5.969" y1="2.83641875" x2="6.64718125" y2="2.921" layer="21"/>
<rectangle x1="6.81481875" y1="2.83641875" x2="7.493" y2="2.921" layer="21"/>
<rectangle x1="7.66318125" y1="2.83641875" x2="8.17118125" y2="2.921" layer="21"/>
<rectangle x1="8.67918125" y1="2.83641875" x2="9.86281875" y2="2.921" layer="21"/>
<rectangle x1="0.127" y1="2.921" x2="0.46481875" y2="3.00558125" layer="21"/>
<rectangle x1="1.48081875" y1="2.921" x2="2.58318125" y2="3.00558125" layer="21"/>
<rectangle x1="2.83718125" y1="2.921" x2="3.85318125" y2="3.00558125" layer="21"/>
<rectangle x1="4.52881875" y1="2.921" x2="5.12318125" y2="3.00558125" layer="21"/>
<rectangle x1="6.13918125" y1="2.921" x2="6.64718125" y2="3.00558125" layer="21"/>
<rectangle x1="6.81481875" y1="2.921" x2="7.493" y2="3.00558125" layer="21"/>
<rectangle x1="7.747" y1="2.921" x2="8.001" y2="3.00558125" layer="21"/>
<rectangle x1="8.763" y1="2.921" x2="9.86281875" y2="3.00558125" layer="21"/>
<rectangle x1="0.127" y1="3.00558125" x2="0.381" y2="3.09041875" layer="21"/>
<rectangle x1="1.397" y1="3.00558125" x2="2.667" y2="3.09041875" layer="21"/>
<rectangle x1="2.921" y1="3.00558125" x2="3.937" y2="3.09041875" layer="21"/>
<rectangle x1="4.52881875" y1="3.00558125" x2="5.03681875" y2="3.09041875" layer="21"/>
<rectangle x1="5.54481875" y1="3.00558125" x2="5.715" y2="3.09041875" layer="21"/>
<rectangle x1="6.223" y1="3.00558125" x2="6.64718125" y2="3.09041875" layer="21"/>
<rectangle x1="6.81481875" y1="3.00558125" x2="7.493" y2="3.09041875" layer="21"/>
<rectangle x1="7.747" y1="3.00558125" x2="7.91718125" y2="3.09041875" layer="21"/>
<rectangle x1="8.255" y1="3.00558125" x2="8.59281875" y2="3.09041875" layer="21"/>
<rectangle x1="8.84681875" y1="3.00558125" x2="9.94918125" y2="3.09041875" layer="21"/>
<rectangle x1="0.04318125" y1="3.09041875" x2="0.381" y2="3.175" layer="21"/>
<rectangle x1="1.31318125" y1="3.09041875" x2="1.73481875" y2="3.175" layer="21"/>
<rectangle x1="2.24281875" y1="3.09041875" x2="2.667" y2="3.175" layer="21"/>
<rectangle x1="3.59918125" y1="3.09041875" x2="4.02081875" y2="3.175" layer="21"/>
<rectangle x1="4.52881875" y1="3.09041875" x2="4.953" y2="3.175" layer="21"/>
<rectangle x1="5.29081875" y1="3.09041875" x2="5.969" y2="3.175" layer="21"/>
<rectangle x1="6.30681875" y1="3.09041875" x2="6.64718125" y2="3.175" layer="21"/>
<rectangle x1="6.81481875" y1="3.09041875" x2="7.493" y2="3.175" layer="21"/>
<rectangle x1="7.747" y1="3.09041875" x2="7.91718125" y2="3.175" layer="21"/>
<rectangle x1="8.08481875" y1="3.09041875" x2="8.67918125" y2="3.175" layer="21"/>
<rectangle x1="8.84681875" y1="3.09041875" x2="9.94918125" y2="3.175" layer="21"/>
<rectangle x1="0.04318125" y1="3.175" x2="0.381" y2="3.25958125" layer="21"/>
<rectangle x1="1.31318125" y1="3.175" x2="1.651" y2="3.25958125" layer="21"/>
<rectangle x1="2.413" y1="3.175" x2="2.75081875" y2="3.25958125" layer="21"/>
<rectangle x1="3.683" y1="3.175" x2="4.02081875" y2="3.25958125" layer="21"/>
<rectangle x1="4.52881875" y1="3.175" x2="4.953" y2="3.25958125" layer="21"/>
<rectangle x1="5.207" y1="3.175" x2="6.05281875" y2="3.25958125" layer="21"/>
<rectangle x1="6.30681875" y1="3.175" x2="6.64718125" y2="3.25958125" layer="21"/>
<rectangle x1="6.81481875" y1="3.175" x2="7.493" y2="3.25958125" layer="21"/>
<rectangle x1="7.66318125" y1="3.175" x2="7.83081875" y2="3.25958125" layer="21"/>
<rectangle x1="8.08481875" y1="3.175" x2="9.94918125" y2="3.25958125" layer="21"/>
<rectangle x1="0.04318125" y1="3.25958125" x2="0.29718125" y2="3.34441875" layer="21"/>
<rectangle x1="1.22681875" y1="3.25958125" x2="1.56718125" y2="3.34441875" layer="21"/>
<rectangle x1="2.49681875" y1="3.25958125" x2="2.75081875" y2="3.34441875" layer="21"/>
<rectangle x1="3.76681875" y1="3.25958125" x2="4.10718125" y2="3.34441875" layer="21"/>
<rectangle x1="4.445" y1="3.25958125" x2="4.86918125" y2="3.34441875" layer="21"/>
<rectangle x1="5.12318125" y1="3.25958125" x2="6.13918125" y2="3.34441875" layer="21"/>
<rectangle x1="6.39318125" y1="3.25958125" x2="6.64718125" y2="3.34441875" layer="21"/>
<rectangle x1="6.81481875" y1="3.25958125" x2="7.493" y2="3.34441875" layer="21"/>
<rectangle x1="7.66318125" y1="3.25958125" x2="7.83081875" y2="3.34441875" layer="21"/>
<rectangle x1="8.93318125" y1="3.25958125" x2="9.94918125" y2="3.34441875" layer="21"/>
<rectangle x1="0.04318125" y1="3.34441875" x2="0.29718125" y2="3.429" layer="21"/>
<rectangle x1="1.22681875" y1="3.34441875" x2="1.48081875" y2="3.429" layer="21"/>
<rectangle x1="2.49681875" y1="3.34441875" x2="2.83718125" y2="3.429" layer="21"/>
<rectangle x1="3.76681875" y1="3.34441875" x2="4.10718125" y2="3.429" layer="21"/>
<rectangle x1="4.445" y1="3.34441875" x2="4.86918125" y2="3.429" layer="21"/>
<rectangle x1="5.12318125" y1="3.34441875" x2="6.13918125" y2="3.429" layer="21"/>
<rectangle x1="6.39318125" y1="3.34441875" x2="6.64718125" y2="3.429" layer="21"/>
<rectangle x1="6.81481875" y1="3.34441875" x2="7.493" y2="3.429" layer="21"/>
<rectangle x1="7.66318125" y1="3.34441875" x2="7.83081875" y2="3.429" layer="21"/>
<rectangle x1="8.93318125" y1="3.34441875" x2="9.94918125" y2="3.429" layer="21"/>
<rectangle x1="0.04318125" y1="3.429" x2="0.29718125" y2="3.51358125" layer="21"/>
<rectangle x1="1.143" y1="3.429" x2="1.48081875" y2="3.51358125" layer="21"/>
<rectangle x1="2.49681875" y1="3.429" x2="2.83718125" y2="3.51358125" layer="21"/>
<rectangle x1="3.85318125" y1="3.429" x2="4.10718125" y2="3.51358125" layer="21"/>
<rectangle x1="4.445" y1="3.429" x2="4.78281875" y2="3.51358125" layer="21"/>
<rectangle x1="5.03681875" y1="3.429" x2="6.13918125" y2="3.51358125" layer="21"/>
<rectangle x1="6.39318125" y1="3.429" x2="6.64718125" y2="3.51358125" layer="21"/>
<rectangle x1="6.81481875" y1="3.429" x2="7.493" y2="3.51358125" layer="21"/>
<rectangle x1="7.66318125" y1="3.429" x2="7.83081875" y2="3.51358125" layer="21"/>
<rectangle x1="8.08481875" y1="3.429" x2="8.67918125" y2="3.51358125" layer="21"/>
<rectangle x1="8.93318125" y1="3.429" x2="9.94918125" y2="3.51358125" layer="21"/>
<rectangle x1="0.04318125" y1="3.51358125" x2="0.29718125" y2="3.59841875" layer="21"/>
<rectangle x1="1.143" y1="3.51358125" x2="1.48081875" y2="3.59841875" layer="21"/>
<rectangle x1="2.58318125" y1="3.51358125" x2="2.83718125" y2="3.59841875" layer="21"/>
<rectangle x1="3.85318125" y1="3.51358125" x2="4.191" y2="3.59841875" layer="21"/>
<rectangle x1="4.445" y1="3.51358125" x2="4.78281875" y2="3.59841875" layer="21"/>
<rectangle x1="5.03681875" y1="3.51358125" x2="6.223" y2="3.59841875" layer="21"/>
<rectangle x1="6.477" y1="3.51358125" x2="6.64718125" y2="3.59841875" layer="21"/>
<rectangle x1="6.90118125" y1="3.51358125" x2="7.40918125" y2="3.59841875" layer="21"/>
<rectangle x1="7.66318125" y1="3.51358125" x2="7.91718125" y2="3.59841875" layer="21"/>
<rectangle x1="8.08481875" y1="3.51358125" x2="8.67918125" y2="3.59841875" layer="21"/>
<rectangle x1="8.84681875" y1="3.51358125" x2="9.94918125" y2="3.59841875" layer="21"/>
<rectangle x1="0.04318125" y1="3.59841875" x2="0.29718125" y2="3.683" layer="21"/>
<rectangle x1="1.143" y1="3.59841875" x2="1.48081875" y2="3.683" layer="21"/>
<rectangle x1="2.58318125" y1="3.59841875" x2="2.83718125" y2="3.683" layer="21"/>
<rectangle x1="3.85318125" y1="3.59841875" x2="4.191" y2="3.683" layer="21"/>
<rectangle x1="4.445" y1="3.59841875" x2="4.78281875" y2="3.683" layer="21"/>
<rectangle x1="5.03681875" y1="3.59841875" x2="6.223" y2="3.683" layer="21"/>
<rectangle x1="6.477" y1="3.59841875" x2="6.64718125" y2="3.683" layer="21"/>
<rectangle x1="6.985" y1="3.59841875" x2="7.32281875" y2="3.683" layer="21"/>
<rectangle x1="7.66318125" y1="3.59841875" x2="7.91718125" y2="3.683" layer="21"/>
<rectangle x1="8.17118125" y1="3.59841875" x2="8.59281875" y2="3.683" layer="21"/>
<rectangle x1="8.84681875" y1="3.59841875" x2="9.94918125" y2="3.683" layer="21"/>
<rectangle x1="0.04318125" y1="3.683" x2="0.29718125" y2="3.76758125" layer="21"/>
<rectangle x1="1.143" y1="3.683" x2="1.48081875" y2="3.76758125" layer="21"/>
<rectangle x1="2.58318125" y1="3.683" x2="2.83718125" y2="3.76758125" layer="21"/>
<rectangle x1="3.85318125" y1="3.683" x2="4.10718125" y2="3.76758125" layer="21"/>
<rectangle x1="4.445" y1="3.683" x2="4.78281875" y2="3.76758125" layer="21"/>
<rectangle x1="5.03681875" y1="3.683" x2="6.13918125" y2="3.76758125" layer="21"/>
<rectangle x1="6.39318125" y1="3.683" x2="6.64718125" y2="3.76758125" layer="21"/>
<rectangle x1="7.57681875" y1="3.683" x2="8.001" y2="3.76758125" layer="21"/>
<rectangle x1="8.763" y1="3.683" x2="9.94918125" y2="3.76758125" layer="21"/>
<rectangle x1="0.04318125" y1="3.76758125" x2="0.29718125" y2="3.85241875" layer="21"/>
<rectangle x1="1.143" y1="3.76758125" x2="1.48081875" y2="3.85241875" layer="21"/>
<rectangle x1="2.58318125" y1="3.76758125" x2="2.83718125" y2="3.85241875" layer="21"/>
<rectangle x1="3.85318125" y1="3.76758125" x2="4.10718125" y2="3.85241875" layer="21"/>
<rectangle x1="4.445" y1="3.76758125" x2="4.86918125" y2="3.85241875" layer="21"/>
<rectangle x1="5.12318125" y1="3.76758125" x2="6.13918125" y2="3.85241875" layer="21"/>
<rectangle x1="6.39318125" y1="3.76758125" x2="6.64718125" y2="3.85241875" layer="21"/>
<rectangle x1="6.81481875" y1="3.76758125" x2="6.90118125" y2="3.85241875" layer="21"/>
<rectangle x1="7.40918125" y1="3.76758125" x2="8.08481875" y2="3.85241875" layer="21"/>
<rectangle x1="8.67918125" y1="3.76758125" x2="9.94918125" y2="3.85241875" layer="21"/>
<rectangle x1="0.04318125" y1="3.85241875" x2="0.29718125" y2="3.937" layer="21"/>
<rectangle x1="1.143" y1="3.85241875" x2="1.48081875" y2="3.937" layer="21"/>
<rectangle x1="2.58318125" y1="3.85241875" x2="2.83718125" y2="3.937" layer="21"/>
<rectangle x1="3.76681875" y1="3.85241875" x2="4.10718125" y2="3.937" layer="21"/>
<rectangle x1="4.445" y1="3.85241875" x2="4.86918125" y2="3.937" layer="21"/>
<rectangle x1="5.12318125" y1="3.85241875" x2="6.13918125" y2="3.937" layer="21"/>
<rectangle x1="6.39318125" y1="3.85241875" x2="7.06881875" y2="3.937" layer="21"/>
<rectangle x1="7.239" y1="3.85241875" x2="8.33881875" y2="3.937" layer="21"/>
<rectangle x1="8.42518125" y1="3.85241875" x2="9.94918125" y2="3.937" layer="21"/>
<rectangle x1="0.04318125" y1="3.937" x2="0.381" y2="4.02158125" layer="21"/>
<rectangle x1="1.143" y1="3.937" x2="1.48081875" y2="4.02158125" layer="21"/>
<rectangle x1="2.58318125" y1="3.937" x2="2.83718125" y2="4.02158125" layer="21"/>
<rectangle x1="3.683" y1="3.937" x2="4.02081875" y2="4.02158125" layer="21"/>
<rectangle x1="4.52881875" y1="3.937" x2="4.86918125" y2="4.02158125" layer="21"/>
<rectangle x1="5.207" y1="3.937" x2="6.05281875" y2="4.02158125" layer="21"/>
<rectangle x1="6.30681875" y1="3.937" x2="9.94918125" y2="4.02158125" layer="21"/>
<rectangle x1="0.04318125" y1="4.02158125" x2="0.381" y2="4.10641875" layer="21"/>
<rectangle x1="1.143" y1="4.02158125" x2="1.48081875" y2="4.10641875" layer="21"/>
<rectangle x1="2.58318125" y1="4.02158125" x2="2.83718125" y2="4.10641875" layer="21"/>
<rectangle x1="3.59918125" y1="4.02158125" x2="4.02081875" y2="4.10641875" layer="21"/>
<rectangle x1="4.52881875" y1="4.02158125" x2="4.953" y2="4.10641875" layer="21"/>
<rectangle x1="5.29081875" y1="4.02158125" x2="5.969" y2="4.10641875" layer="21"/>
<rectangle x1="6.30681875" y1="4.02158125" x2="9.94918125" y2="4.10641875" layer="21"/>
<rectangle x1="0.04318125" y1="4.10641875" x2="0.381" y2="4.191" layer="21"/>
<rectangle x1="1.143" y1="4.10641875" x2="1.48081875" y2="4.191" layer="21"/>
<rectangle x1="2.58318125" y1="4.10641875" x2="3.937" y2="4.191" layer="21"/>
<rectangle x1="4.52881875" y1="4.10641875" x2="5.03681875" y2="4.191" layer="21"/>
<rectangle x1="5.461" y1="4.10641875" x2="5.79881875" y2="4.191" layer="21"/>
<rectangle x1="6.223" y1="4.10641875" x2="9.94918125" y2="4.191" layer="21"/>
<rectangle x1="0.127" y1="4.191" x2="0.46481875" y2="4.27558125" layer="21"/>
<rectangle x1="1.143" y1="4.191" x2="1.48081875" y2="4.27558125" layer="21"/>
<rectangle x1="2.58318125" y1="4.191" x2="3.85318125" y2="4.27558125" layer="21"/>
<rectangle x1="4.52881875" y1="4.191" x2="5.12318125" y2="4.27558125" layer="21"/>
<rectangle x1="6.13918125" y1="4.191" x2="9.86281875" y2="4.27558125" layer="21"/>
<rectangle x1="0.127" y1="4.27558125" x2="0.46481875" y2="4.36041875" layer="21"/>
<rectangle x1="1.143" y1="4.27558125" x2="1.48081875" y2="4.36041875" layer="21"/>
<rectangle x1="2.58318125" y1="4.27558125" x2="3.76681875" y2="4.36041875" layer="21"/>
<rectangle x1="4.52881875" y1="4.27558125" x2="5.207" y2="4.36041875" layer="21"/>
<rectangle x1="5.969" y1="4.27558125" x2="9.86281875" y2="4.36041875" layer="21"/>
<rectangle x1="0.21081875" y1="4.36041875" x2="0.46481875" y2="4.445" layer="21"/>
<rectangle x1="1.22681875" y1="4.36041875" x2="1.397" y2="4.445" layer="21"/>
<rectangle x1="2.58318125" y1="4.36041875" x2="3.59918125" y2="4.445" layer="21"/>
<rectangle x1="4.52881875" y1="4.36041875" x2="5.461" y2="4.445" layer="21"/>
<rectangle x1="5.79881875" y1="4.36041875" x2="9.86281875" y2="4.445" layer="21"/>
<rectangle x1="0.21081875" y1="4.445" x2="0.55118125" y2="4.52958125" layer="21"/>
<rectangle x1="4.52881875" y1="4.445" x2="9.779" y2="4.52958125" layer="21"/>
<rectangle x1="0.29718125" y1="4.52958125" x2="0.635" y2="4.61441875" layer="21"/>
<rectangle x1="4.61518125" y1="4.52958125" x2="9.779" y2="4.61441875" layer="21"/>
<rectangle x1="0.29718125" y1="4.61441875" x2="0.635" y2="4.699" layer="21"/>
<rectangle x1="4.61518125" y1="4.61441875" x2="9.69518125" y2="4.699" layer="21"/>
<rectangle x1="0.381" y1="4.699" x2="0.71881875" y2="4.78358125" layer="21"/>
<rectangle x1="4.61518125" y1="4.699" x2="9.60881875" y2="4.78358125" layer="21"/>
<rectangle x1="0.46481875" y1="4.78358125" x2="0.80518125" y2="4.86841875" layer="21"/>
<rectangle x1="4.699" y1="4.78358125" x2="9.60881875" y2="4.86841875" layer="21"/>
<rectangle x1="0.46481875" y1="4.86841875" x2="0.889" y2="4.953" layer="21"/>
<rectangle x1="4.699" y1="4.86841875" x2="9.525" y2="4.953" layer="21"/>
<rectangle x1="0.55118125" y1="4.953" x2="1.05918125" y2="5.03758125" layer="21"/>
<rectangle x1="4.699" y1="4.953" x2="9.44118125" y2="5.03758125" layer="21"/>
<rectangle x1="0.635" y1="5.03758125" x2="1.143" y2="5.12241875" layer="21"/>
<rectangle x1="4.78281875" y1="5.03758125" x2="9.35481875" y2="5.12241875" layer="21"/>
<rectangle x1="0.71881875" y1="5.12241875" x2="1.31318125" y2="5.207" layer="21"/>
<rectangle x1="4.78281875" y1="5.12241875" x2="9.271" y2="5.207" layer="21"/>
<rectangle x1="0.889" y1="5.207" x2="1.56718125" y2="5.29158125" layer="21"/>
<rectangle x1="4.86918125" y1="5.207" x2="9.18718125" y2="5.29158125" layer="21"/>
<rectangle x1="0.97281875" y1="5.29158125" x2="9.017" y2="5.37641875" layer="21"/>
<rectangle x1="1.143" y1="5.37641875" x2="8.84681875" y2="5.461" layer="21"/>
<rectangle x1="1.31318125" y1="5.461" x2="8.67918125" y2="5.54558125" layer="21"/>
<rectangle x1="1.651" y1="5.54558125" x2="8.33881875" y2="5.63041875" layer="21"/>
</package>
<package name="UDO-LOGO-12MM" urn="urn:adsk.eagle:footprint:6649250/1">
<rectangle x1="1.651" y1="0.21158125" x2="2.24281875" y2="0.29641875" layer="21"/>
<rectangle x1="2.667" y1="0.21158125" x2="2.75081875" y2="0.29641875" layer="21"/>
<rectangle x1="3.34518125" y1="0.21158125" x2="3.429" y2="0.29641875" layer="21"/>
<rectangle x1="4.10718125" y1="0.21158125" x2="4.27481875" y2="0.29641875" layer="21"/>
<rectangle x1="4.52881875" y1="0.21158125" x2="5.12318125" y2="0.29641875" layer="21"/>
<rectangle x1="5.88518125" y1="0.21158125" x2="6.13918125" y2="0.29641875" layer="21"/>
<rectangle x1="6.64718125" y1="0.21158125" x2="6.90118125" y2="0.29641875" layer="21"/>
<rectangle x1="7.40918125" y1="0.21158125" x2="7.66318125" y2="0.29641875" layer="21"/>
<rectangle x1="8.42518125" y1="0.21158125" x2="9.017" y2="0.29641875" layer="21"/>
<rectangle x1="10.541" y1="0.21158125" x2="10.795" y2="0.29641875" layer="21"/>
<rectangle x1="1.56718125" y1="0.29641875" x2="1.73481875" y2="0.381" layer="21"/>
<rectangle x1="2.07518125" y1="0.29641875" x2="2.32918125" y2="0.381" layer="21"/>
<rectangle x1="2.58318125" y1="0.29641875" x2="2.75081875" y2="0.381" layer="21"/>
<rectangle x1="3.34518125" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="3.51281875" y1="0.29641875" x2="3.683" y2="0.381" layer="21"/>
<rectangle x1="4.02081875" y1="0.29641875" x2="4.27481875" y2="0.381" layer="21"/>
<rectangle x1="4.52881875" y1="0.29641875" x2="5.29081875" y2="0.381" layer="21"/>
<rectangle x1="5.79881875" y1="0.29641875" x2="6.30681875" y2="0.381" layer="21"/>
<rectangle x1="6.56081875" y1="0.29641875" x2="6.985" y2="0.381" layer="21"/>
<rectangle x1="7.32281875" y1="0.29641875" x2="7.83081875" y2="0.381" layer="21"/>
<rectangle x1="8.33881875" y1="0.29641875" x2="8.509" y2="0.381" layer="21"/>
<rectangle x1="8.93318125" y1="0.29641875" x2="9.10081875" y2="0.381" layer="21"/>
<rectangle x1="9.35481875" y1="0.29641875" x2="9.525" y2="0.381" layer="21"/>
<rectangle x1="10.033" y1="0.29641875" x2="10.20318125" y2="0.381" layer="21"/>
<rectangle x1="10.37081875" y1="0.29641875" x2="10.96518125" y2="0.381" layer="21"/>
<rectangle x1="1.48081875" y1="0.381" x2="1.651" y2="0.46558125" layer="21"/>
<rectangle x1="2.24281875" y1="0.381" x2="2.413" y2="0.46558125" layer="21"/>
<rectangle x1="2.58318125" y1="0.381" x2="2.75081875" y2="0.46558125" layer="21"/>
<rectangle x1="3.34518125" y1="0.381" x2="3.429" y2="0.46558125" layer="21"/>
<rectangle x1="3.51281875" y1="0.381" x2="3.683" y2="0.46558125" layer="21"/>
<rectangle x1="3.937" y1="0.381" x2="4.10718125" y2="0.46558125" layer="21"/>
<rectangle x1="4.52881875" y1="0.381" x2="4.61518125" y2="0.46558125" layer="21"/>
<rectangle x1="5.207" y1="0.381" x2="5.37718125" y2="0.46558125" layer="21"/>
<rectangle x1="5.715" y1="0.381" x2="5.88518125" y2="0.46558125" layer="21"/>
<rectangle x1="6.223" y1="0.381" x2="6.39318125" y2="0.46558125" layer="21"/>
<rectangle x1="6.56081875" y1="0.381" x2="6.64718125" y2="0.46558125" layer="21"/>
<rectangle x1="6.90118125" y1="0.381" x2="7.06881875" y2="0.46558125" layer="21"/>
<rectangle x1="7.239" y1="0.381" x2="7.40918125" y2="0.46558125" layer="21"/>
<rectangle x1="7.747" y1="0.381" x2="7.91718125" y2="0.46558125" layer="21"/>
<rectangle x1="8.255" y1="0.381" x2="8.42518125" y2="0.46558125" layer="21"/>
<rectangle x1="9.017" y1="0.381" x2="9.18718125" y2="0.46558125" layer="21"/>
<rectangle x1="9.35481875" y1="0.381" x2="9.525" y2="0.46558125" layer="21"/>
<rectangle x1="10.033" y1="0.381" x2="10.20318125" y2="0.46558125" layer="21"/>
<rectangle x1="10.37081875" y1="0.381" x2="10.45718125" y2="0.46558125" layer="21"/>
<rectangle x1="10.87881875" y1="0.381" x2="11.049" y2="0.46558125" layer="21"/>
<rectangle x1="1.397" y1="0.46558125" x2="1.56718125" y2="0.55041875" layer="21"/>
<rectangle x1="2.32918125" y1="0.46558125" x2="2.49681875" y2="0.55041875" layer="21"/>
<rectangle x1="2.58318125" y1="0.46558125" x2="2.75081875" y2="0.55041875" layer="21"/>
<rectangle x1="3.34518125" y1="0.46558125" x2="3.429" y2="0.55041875" layer="21"/>
<rectangle x1="3.51281875" y1="0.46558125" x2="3.683" y2="0.55041875" layer="21"/>
<rectangle x1="3.85318125" y1="0.46558125" x2="4.02081875" y2="0.55041875" layer="21"/>
<rectangle x1="4.52881875" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="21"/>
<rectangle x1="5.29081875" y1="0.46558125" x2="5.461" y2="0.55041875" layer="21"/>
<rectangle x1="5.63118125" y1="0.46558125" x2="5.79881875" y2="0.55041875" layer="21"/>
<rectangle x1="6.30681875" y1="0.46558125" x2="6.39318125" y2="0.55041875" layer="21"/>
<rectangle x1="6.90118125" y1="0.46558125" x2="7.06881875" y2="0.55041875" layer="21"/>
<rectangle x1="7.15518125" y1="0.46558125" x2="7.32281875" y2="0.55041875" layer="21"/>
<rectangle x1="7.83081875" y1="0.46558125" x2="7.91718125" y2="0.55041875" layer="21"/>
<rectangle x1="8.17118125" y1="0.46558125" x2="8.33881875" y2="0.55041875" layer="21"/>
<rectangle x1="9.10081875" y1="0.46558125" x2="9.271" y2="0.55041875" layer="21"/>
<rectangle x1="9.35481875" y1="0.46558125" x2="9.525" y2="0.55041875" layer="21"/>
<rectangle x1="10.033" y1="0.46558125" x2="10.20318125" y2="0.55041875" layer="21"/>
<rectangle x1="10.287" y1="0.46558125" x2="10.45718125" y2="0.55041875" layer="21"/>
<rectangle x1="1.397" y1="0.55041875" x2="1.48081875" y2="0.635" layer="21"/>
<rectangle x1="2.32918125" y1="0.55041875" x2="2.49681875" y2="0.635" layer="21"/>
<rectangle x1="2.667" y1="0.55041875" x2="2.75081875" y2="0.635" layer="21"/>
<rectangle x1="3.34518125" y1="0.55041875" x2="3.429" y2="0.635" layer="21"/>
<rectangle x1="3.51281875" y1="0.55041875" x2="3.683" y2="0.635" layer="21"/>
<rectangle x1="3.85318125" y1="0.55041875" x2="4.02081875" y2="0.635" layer="21"/>
<rectangle x1="4.52881875" y1="0.55041875" x2="4.61518125" y2="0.635" layer="21"/>
<rectangle x1="5.37718125" y1="0.55041875" x2="5.461" y2="0.635" layer="21"/>
<rectangle x1="5.63118125" y1="0.55041875" x2="5.715" y2="0.635" layer="21"/>
<rectangle x1="6.30681875" y1="0.55041875" x2="6.39318125" y2="0.635" layer="21"/>
<rectangle x1="6.731" y1="0.55041875" x2="6.985" y2="0.635" layer="21"/>
<rectangle x1="7.15518125" y1="0.55041875" x2="7.91718125" y2="0.635" layer="21"/>
<rectangle x1="8.17118125" y1="0.55041875" x2="8.255" y2="0.635" layer="21"/>
<rectangle x1="9.18718125" y1="0.55041875" x2="9.271" y2="0.635" layer="21"/>
<rectangle x1="9.35481875" y1="0.55041875" x2="9.525" y2="0.635" layer="21"/>
<rectangle x1="10.033" y1="0.55041875" x2="10.20318125" y2="0.635" layer="21"/>
<rectangle x1="10.287" y1="0.55041875" x2="10.96518125" y2="0.635" layer="21"/>
<rectangle x1="1.31318125" y1="0.635" x2="1.48081875" y2="0.71958125" layer="21"/>
<rectangle x1="2.413" y1="0.635" x2="2.49681875" y2="0.71958125" layer="21"/>
<rectangle x1="2.667" y1="0.635" x2="2.75081875" y2="0.71958125" layer="21"/>
<rectangle x1="3.25881875" y1="0.635" x2="3.429" y2="0.71958125" layer="21"/>
<rectangle x1="3.51281875" y1="0.635" x2="3.683" y2="0.71958125" layer="21"/>
<rectangle x1="3.85318125" y1="0.635" x2="4.02081875" y2="0.71958125" layer="21"/>
<rectangle x1="4.52881875" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="5.37718125" y1="0.635" x2="5.54481875" y2="0.71958125" layer="21"/>
<rectangle x1="5.63118125" y1="0.635" x2="5.715" y2="0.71958125" layer="21"/>
<rectangle x1="6.30681875" y1="0.635" x2="6.39318125" y2="0.71958125" layer="21"/>
<rectangle x1="6.56081875" y1="0.635" x2="6.90118125" y2="0.71958125" layer="21"/>
<rectangle x1="7.15518125" y1="0.635" x2="7.91718125" y2="0.71958125" layer="21"/>
<rectangle x1="8.17118125" y1="0.635" x2="8.255" y2="0.71958125" layer="21"/>
<rectangle x1="9.18718125" y1="0.635" x2="9.271" y2="0.71958125" layer="21"/>
<rectangle x1="9.35481875" y1="0.635" x2="9.525" y2="0.71958125" layer="21"/>
<rectangle x1="10.033" y1="0.635" x2="10.20318125" y2="0.71958125" layer="21"/>
<rectangle x1="10.287" y1="0.635" x2="11.049" y2="0.71958125" layer="21"/>
<rectangle x1="1.31318125" y1="0.71958125" x2="1.48081875" y2="0.80441875" layer="21"/>
<rectangle x1="2.413" y1="0.71958125" x2="2.49681875" y2="0.80441875" layer="21"/>
<rectangle x1="2.667" y1="0.71958125" x2="2.75081875" y2="0.80441875" layer="21"/>
<rectangle x1="3.25881875" y1="0.71958125" x2="3.429" y2="0.80441875" layer="21"/>
<rectangle x1="3.51281875" y1="0.71958125" x2="3.683" y2="0.80441875" layer="21"/>
<rectangle x1="3.85318125" y1="0.71958125" x2="4.02081875" y2="0.80441875" layer="21"/>
<rectangle x1="4.52881875" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="5.37718125" y1="0.71958125" x2="5.54481875" y2="0.80441875" layer="21"/>
<rectangle x1="5.63118125" y1="0.71958125" x2="5.79881875" y2="0.80441875" layer="21"/>
<rectangle x1="6.30681875" y1="0.71958125" x2="6.39318125" y2="0.80441875" layer="21"/>
<rectangle x1="6.477" y1="0.71958125" x2="6.64718125" y2="0.80441875" layer="21"/>
<rectangle x1="7.15518125" y1="0.71958125" x2="7.32281875" y2="0.80441875" layer="21"/>
<rectangle x1="7.83081875" y1="0.71958125" x2="7.91718125" y2="0.80441875" layer="21"/>
<rectangle x1="8.08481875" y1="0.71958125" x2="8.255" y2="0.80441875" layer="21"/>
<rectangle x1="9.18718125" y1="0.71958125" x2="9.271" y2="0.80441875" layer="21"/>
<rectangle x1="9.44118125" y1="0.71958125" x2="9.525" y2="0.80441875" layer="21"/>
<rectangle x1="10.033" y1="0.71958125" x2="10.20318125" y2="0.80441875" layer="21"/>
<rectangle x1="10.287" y1="0.71958125" x2="10.37081875" y2="0.80441875" layer="21"/>
<rectangle x1="10.87881875" y1="0.71958125" x2="11.049" y2="0.80441875" layer="21"/>
<rectangle x1="1.31318125" y1="0.80441875" x2="1.48081875" y2="0.889" layer="21"/>
<rectangle x1="2.413" y1="0.80441875" x2="2.49681875" y2="0.889" layer="21"/>
<rectangle x1="2.667" y1="0.80441875" x2="2.83718125" y2="0.889" layer="21"/>
<rectangle x1="3.175" y1="0.80441875" x2="3.34518125" y2="0.889" layer="21"/>
<rectangle x1="3.51281875" y1="0.80441875" x2="3.683" y2="0.889" layer="21"/>
<rectangle x1="3.85318125" y1="0.80441875" x2="4.02081875" y2="0.889" layer="21"/>
<rectangle x1="4.52881875" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="5.37718125" y1="0.80441875" x2="5.54481875" y2="0.889" layer="21"/>
<rectangle x1="5.715" y1="0.80441875" x2="5.79881875" y2="0.889" layer="21"/>
<rectangle x1="6.223" y1="0.80441875" x2="6.39318125" y2="0.889" layer="21"/>
<rectangle x1="6.477" y1="0.80441875" x2="6.64718125" y2="0.889" layer="21"/>
<rectangle x1="6.90118125" y1="0.80441875" x2="7.06881875" y2="0.889" layer="21"/>
<rectangle x1="7.239" y1="0.80441875" x2="7.40918125" y2="0.889" layer="21"/>
<rectangle x1="7.747" y1="0.80441875" x2="7.91718125" y2="0.889" layer="21"/>
<rectangle x1="8.17118125" y1="0.80441875" x2="8.255" y2="0.889" layer="21"/>
<rectangle x1="9.18718125" y1="0.80441875" x2="9.271" y2="0.889" layer="21"/>
<rectangle x1="9.44118125" y1="0.80441875" x2="9.60881875" y2="0.889" layer="21"/>
<rectangle x1="9.94918125" y1="0.80441875" x2="10.11681875" y2="0.889" layer="21"/>
<rectangle x1="10.287" y1="0.80441875" x2="10.45718125" y2="0.889" layer="21"/>
<rectangle x1="10.87881875" y1="0.80441875" x2="11.049" y2="0.889" layer="21"/>
<rectangle x1="1.31318125" y1="0.889" x2="1.48081875" y2="0.97358125" layer="21"/>
<rectangle x1="2.413" y1="0.889" x2="2.49681875" y2="0.97358125" layer="21"/>
<rectangle x1="2.667" y1="0.889" x2="3.25881875" y2="0.97358125" layer="21"/>
<rectangle x1="3.51281875" y1="0.889" x2="3.683" y2="0.97358125" layer="21"/>
<rectangle x1="3.76681875" y1="0.889" x2="4.27481875" y2="0.97358125" layer="21"/>
<rectangle x1="4.52881875" y1="0.889" x2="4.61518125" y2="0.97358125" layer="21"/>
<rectangle x1="5.37718125" y1="0.889" x2="5.54481875" y2="0.97358125" layer="21"/>
<rectangle x1="5.715" y1="0.889" x2="6.30681875" y2="0.97358125" layer="21"/>
<rectangle x1="6.56081875" y1="0.889" x2="6.985" y2="0.97358125" layer="21"/>
<rectangle x1="7.32281875" y1="0.889" x2="7.83081875" y2="0.97358125" layer="21"/>
<rectangle x1="8.17118125" y1="0.889" x2="8.255" y2="0.97358125" layer="21"/>
<rectangle x1="9.18718125" y1="0.889" x2="9.271" y2="0.97358125" layer="21"/>
<rectangle x1="9.35481875" y1="0.889" x2="10.033" y2="0.97358125" layer="21"/>
<rectangle x1="10.37081875" y1="0.889" x2="10.96518125" y2="0.97358125" layer="21"/>
<rectangle x1="1.31318125" y1="0.97358125" x2="1.48081875" y2="1.05841875" layer="21"/>
<rectangle x1="2.413" y1="0.97358125" x2="2.49681875" y2="1.05841875" layer="21"/>
<rectangle x1="2.667" y1="0.97358125" x2="2.75081875" y2="1.05841875" layer="21"/>
<rectangle x1="2.921" y1="0.97358125" x2="3.175" y2="1.05841875" layer="21"/>
<rectangle x1="3.76681875" y1="0.97358125" x2="4.27481875" y2="1.05841875" layer="21"/>
<rectangle x1="4.52881875" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="21"/>
<rectangle x1="5.37718125" y1="0.97358125" x2="5.54481875" y2="1.05841875" layer="21"/>
<rectangle x1="5.88518125" y1="0.97358125" x2="6.13918125" y2="1.05841875" layer="21"/>
<rectangle x1="6.64718125" y1="0.97358125" x2="6.90118125" y2="1.05841875" layer="21"/>
<rectangle x1="7.40918125" y1="0.97358125" x2="7.66318125" y2="1.05841875" layer="21"/>
<rectangle x1="8.17118125" y1="0.97358125" x2="8.33881875" y2="1.05841875" layer="21"/>
<rectangle x1="9.10081875" y1="0.97358125" x2="9.271" y2="1.05841875" layer="21"/>
<rectangle x1="9.60881875" y1="0.97358125" x2="9.94918125" y2="1.05841875" layer="21"/>
<rectangle x1="10.541" y1="0.97358125" x2="10.795" y2="1.05841875" layer="21"/>
<rectangle x1="1.31318125" y1="1.05841875" x2="1.48081875" y2="1.143" layer="21"/>
<rectangle x1="2.413" y1="1.05841875" x2="2.49681875" y2="1.143" layer="21"/>
<rectangle x1="3.51281875" y1="1.05841875" x2="3.683" y2="1.143" layer="21"/>
<rectangle x1="3.85318125" y1="1.05841875" x2="4.02081875" y2="1.143" layer="21"/>
<rectangle x1="4.52881875" y1="1.05841875" x2="4.61518125" y2="1.143" layer="21"/>
<rectangle x1="5.29081875" y1="1.05841875" x2="5.461" y2="1.143" layer="21"/>
<rectangle x1="8.255" y1="1.05841875" x2="8.42518125" y2="1.143" layer="21"/>
<rectangle x1="9.017" y1="1.05841875" x2="9.18718125" y2="1.143" layer="21"/>
<rectangle x1="1.31318125" y1="1.143" x2="1.48081875" y2="1.22758125" layer="21"/>
<rectangle x1="2.413" y1="1.143" x2="2.49681875" y2="1.22758125" layer="21"/>
<rectangle x1="3.51281875" y1="1.143" x2="3.683" y2="1.22758125" layer="21"/>
<rectangle x1="3.85318125" y1="1.143" x2="4.02081875" y2="1.22758125" layer="21"/>
<rectangle x1="4.52881875" y1="1.143" x2="4.61518125" y2="1.22758125" layer="21"/>
<rectangle x1="5.29081875" y1="1.143" x2="5.461" y2="1.22758125" layer="21"/>
<rectangle x1="8.33881875" y1="1.143" x2="8.509" y2="1.22758125" layer="21"/>
<rectangle x1="8.93318125" y1="1.143" x2="9.10081875" y2="1.22758125" layer="21"/>
<rectangle x1="1.31318125" y1="1.22758125" x2="1.48081875" y2="1.31241875" layer="21"/>
<rectangle x1="2.413" y1="1.22758125" x2="2.49681875" y2="1.31241875" layer="21"/>
<rectangle x1="3.85318125" y1="1.22758125" x2="4.02081875" y2="1.31241875" layer="21"/>
<rectangle x1="4.52881875" y1="1.22758125" x2="4.61518125" y2="1.31241875" layer="21"/>
<rectangle x1="5.12318125" y1="1.22758125" x2="5.37718125" y2="1.31241875" layer="21"/>
<rectangle x1="8.42518125" y1="1.22758125" x2="9.017" y2="1.31241875" layer="21"/>
<rectangle x1="4.52881875" y1="1.31241875" x2="5.207" y2="1.397" layer="21"/>
<rectangle x1="8.59281875" y1="1.31241875" x2="8.84681875" y2="1.397" layer="21"/>
<rectangle x1="2.24281875" y1="1.905" x2="9.779" y2="1.98958125" layer="21"/>
<rectangle x1="1.82118125" y1="1.98958125" x2="10.287" y2="2.07441875" layer="21"/>
<rectangle x1="1.56718125" y1="2.07441875" x2="10.541" y2="2.159" layer="21"/>
<rectangle x1="1.31318125" y1="2.159" x2="10.71118125" y2="2.24358125" layer="21"/>
<rectangle x1="1.22681875" y1="2.24358125" x2="2.07518125" y2="2.32841875" layer="21"/>
<rectangle x1="5.88518125" y1="2.24358125" x2="10.87881875" y2="2.32841875" layer="21"/>
<rectangle x1="1.05918125" y1="2.32841875" x2="1.73481875" y2="2.413" layer="21"/>
<rectangle x1="5.88518125" y1="2.32841875" x2="10.96518125" y2="2.413" layer="21"/>
<rectangle x1="0.97281875" y1="2.413" x2="1.56718125" y2="2.49758125" layer="21"/>
<rectangle x1="5.79881875" y1="2.413" x2="11.13281875" y2="2.49758125" layer="21"/>
<rectangle x1="0.889" y1="2.49758125" x2="1.397" y2="2.58241875" layer="21"/>
<rectangle x1="5.79881875" y1="2.49758125" x2="11.21918125" y2="2.58241875" layer="21"/>
<rectangle x1="0.71881875" y1="2.58241875" x2="1.22681875" y2="2.667" layer="21"/>
<rectangle x1="5.715" y1="2.58241875" x2="11.303" y2="2.667" layer="21"/>
<rectangle x1="0.635" y1="2.667" x2="1.143" y2="2.75158125" layer="21"/>
<rectangle x1="5.715" y1="2.667" x2="11.38681875" y2="2.75158125" layer="21"/>
<rectangle x1="0.635" y1="2.75158125" x2="1.05918125" y2="2.83641875" layer="21"/>
<rectangle x1="5.63118125" y1="2.75158125" x2="11.47318125" y2="2.83641875" layer="21"/>
<rectangle x1="0.55118125" y1="2.83641875" x2="0.97281875" y2="2.921" layer="21"/>
<rectangle x1="5.63118125" y1="2.83641875" x2="11.557" y2="2.921" layer="21"/>
<rectangle x1="0.46481875" y1="2.921" x2="0.889" y2="3.00558125" layer="21"/>
<rectangle x1="5.63118125" y1="2.921" x2="11.557" y2="3.00558125" layer="21"/>
<rectangle x1="0.381" y1="3.00558125" x2="0.80518125" y2="3.09041875" layer="21"/>
<rectangle x1="5.54481875" y1="3.00558125" x2="11.64081875" y2="3.09041875" layer="21"/>
<rectangle x1="0.381" y1="3.09041875" x2="0.71881875" y2="3.175" layer="21"/>
<rectangle x1="5.54481875" y1="3.09041875" x2="11.72718125" y2="3.175" layer="21"/>
<rectangle x1="0.29718125" y1="3.175" x2="0.71881875" y2="3.25958125" layer="21"/>
<rectangle x1="5.54481875" y1="3.175" x2="11.72718125" y2="3.25958125" layer="21"/>
<rectangle x1="0.29718125" y1="3.25958125" x2="0.635" y2="3.34441875" layer="21"/>
<rectangle x1="5.54481875" y1="3.25958125" x2="11.811" y2="3.34441875" layer="21"/>
<rectangle x1="0.21081875" y1="3.34441875" x2="0.55118125" y2="3.429" layer="21"/>
<rectangle x1="2.159" y1="3.34441875" x2="2.75081875" y2="3.429" layer="21"/>
<rectangle x1="3.51281875" y1="3.34441875" x2="4.27481875" y2="3.429" layer="21"/>
<rectangle x1="5.54481875" y1="3.34441875" x2="6.477" y2="3.429" layer="21"/>
<rectangle x1="7.06881875" y1="3.34441875" x2="8.001" y2="3.429" layer="21"/>
<rectangle x1="8.17118125" y1="3.34441875" x2="9.10081875" y2="3.429" layer="21"/>
<rectangle x1="9.271" y1="3.34441875" x2="9.86281875" y2="3.429" layer="21"/>
<rectangle x1="10.287" y1="3.34441875" x2="11.811" y2="3.429" layer="21"/>
<rectangle x1="0.21081875" y1="3.429" x2="0.55118125" y2="3.51358125" layer="21"/>
<rectangle x1="1.98881875" y1="3.429" x2="2.921" y2="3.51358125" layer="21"/>
<rectangle x1="3.429" y1="3.429" x2="4.445" y2="3.51358125" layer="21"/>
<rectangle x1="5.461" y1="3.429" x2="6.30681875" y2="3.51358125" layer="21"/>
<rectangle x1="7.32281875" y1="3.429" x2="8.001" y2="3.51358125" layer="21"/>
<rectangle x1="8.255" y1="3.429" x2="9.017" y2="3.51358125" layer="21"/>
<rectangle x1="9.271" y1="3.429" x2="9.69518125" y2="3.51358125" layer="21"/>
<rectangle x1="10.541" y1="3.429" x2="11.89481875" y2="3.51358125" layer="21"/>
<rectangle x1="0.127" y1="3.51358125" x2="0.55118125" y2="3.59841875" layer="21"/>
<rectangle x1="1.82118125" y1="3.51358125" x2="3.00481875" y2="3.59841875" layer="21"/>
<rectangle x1="3.429" y1="3.51358125" x2="4.61518125" y2="3.59841875" layer="21"/>
<rectangle x1="5.461" y1="3.51358125" x2="6.13918125" y2="3.59841875" layer="21"/>
<rectangle x1="7.40918125" y1="3.51358125" x2="7.91718125" y2="3.59841875" layer="21"/>
<rectangle x1="8.255" y1="3.51358125" x2="9.017" y2="3.59841875" layer="21"/>
<rectangle x1="9.271" y1="3.51358125" x2="9.60881875" y2="3.59841875" layer="21"/>
<rectangle x1="10.62481875" y1="3.51358125" x2="11.89481875" y2="3.59841875" layer="21"/>
<rectangle x1="0.127" y1="3.59841875" x2="0.46481875" y2="3.683" layer="21"/>
<rectangle x1="1.73481875" y1="3.59841875" x2="3.175" y2="3.683" layer="21"/>
<rectangle x1="3.51281875" y1="3.59841875" x2="4.699" y2="3.683" layer="21"/>
<rectangle x1="5.461" y1="3.59841875" x2="6.05281875" y2="3.683" layer="21"/>
<rectangle x1="7.493" y1="3.59841875" x2="7.91718125" y2="3.683" layer="21"/>
<rectangle x1="8.255" y1="3.59841875" x2="9.017" y2="3.683" layer="21"/>
<rectangle x1="9.271" y1="3.59841875" x2="9.525" y2="3.683" layer="21"/>
<rectangle x1="10.033" y1="3.59841875" x2="10.20318125" y2="3.683" layer="21"/>
<rectangle x1="10.71118125" y1="3.59841875" x2="11.89481875" y2="3.683" layer="21"/>
<rectangle x1="0.127" y1="3.683" x2="0.46481875" y2="3.76758125" layer="21"/>
<rectangle x1="1.651" y1="3.683" x2="2.159" y2="3.76758125" layer="21"/>
<rectangle x1="2.75081875" y1="3.683" x2="3.175" y2="3.76758125" layer="21"/>
<rectangle x1="4.27481875" y1="3.683" x2="4.78281875" y2="3.76758125" layer="21"/>
<rectangle x1="5.461" y1="3.683" x2="5.969" y2="3.76758125" layer="21"/>
<rectangle x1="6.477" y1="3.683" x2="7.06881875" y2="3.76758125" layer="21"/>
<rectangle x1="7.57681875" y1="3.683" x2="7.91718125" y2="3.76758125" layer="21"/>
<rectangle x1="8.255" y1="3.683" x2="9.017" y2="3.76758125" layer="21"/>
<rectangle x1="9.271" y1="3.683" x2="9.525" y2="3.76758125" layer="21"/>
<rectangle x1="9.86281875" y1="3.683" x2="10.37081875" y2="3.76758125" layer="21"/>
<rectangle x1="10.71118125" y1="3.683" x2="11.98118125" y2="3.76758125" layer="21"/>
<rectangle x1="0.127" y1="3.76758125" x2="0.381" y2="3.85241875" layer="21"/>
<rectangle x1="1.56718125" y1="3.76758125" x2="1.98881875" y2="3.85241875" layer="21"/>
<rectangle x1="2.83718125" y1="3.76758125" x2="3.25881875" y2="3.85241875" layer="21"/>
<rectangle x1="4.445" y1="3.76758125" x2="4.86918125" y2="3.85241875" layer="21"/>
<rectangle x1="5.461" y1="3.76758125" x2="5.969" y2="3.85241875" layer="21"/>
<rectangle x1="6.39318125" y1="3.76758125" x2="7.15518125" y2="3.85241875" layer="21"/>
<rectangle x1="7.66318125" y1="3.76758125" x2="7.91718125" y2="3.85241875" layer="21"/>
<rectangle x1="8.255" y1="3.76758125" x2="9.017" y2="3.85241875" layer="21"/>
<rectangle x1="9.271" y1="3.76758125" x2="9.44118125" y2="3.85241875" layer="21"/>
<rectangle x1="9.779" y1="3.76758125" x2="10.541" y2="3.85241875" layer="21"/>
<rectangle x1="10.62481875" y1="3.76758125" x2="11.98118125" y2="3.85241875" layer="21"/>
<rectangle x1="0.04318125" y1="3.85241875" x2="0.381" y2="3.937" layer="21"/>
<rectangle x1="1.56718125" y1="3.85241875" x2="1.905" y2="3.937" layer="21"/>
<rectangle x1="2.921" y1="3.85241875" x2="3.34518125" y2="3.937" layer="21"/>
<rectangle x1="4.52881875" y1="3.85241875" x2="4.86918125" y2="3.937" layer="21"/>
<rectangle x1="5.461" y1="3.85241875" x2="5.88518125" y2="3.937" layer="21"/>
<rectangle x1="6.30681875" y1="3.85241875" x2="7.239" y2="3.937" layer="21"/>
<rectangle x1="7.66318125" y1="3.85241875" x2="7.91718125" y2="3.937" layer="21"/>
<rectangle x1="8.255" y1="3.85241875" x2="9.017" y2="3.937" layer="21"/>
<rectangle x1="9.271" y1="3.85241875" x2="9.44118125" y2="3.937" layer="21"/>
<rectangle x1="9.779" y1="3.85241875" x2="9.86281875" y2="3.937" layer="21"/>
<rectangle x1="10.45718125" y1="3.85241875" x2="11.98118125" y2="3.937" layer="21"/>
<rectangle x1="0.04318125" y1="3.937" x2="0.381" y2="4.02158125" layer="21"/>
<rectangle x1="1.48081875" y1="3.937" x2="1.905" y2="4.02158125" layer="21"/>
<rectangle x1="3.00481875" y1="3.937" x2="3.34518125" y2="4.02158125" layer="21"/>
<rectangle x1="4.52881875" y1="3.937" x2="4.953" y2="4.02158125" layer="21"/>
<rectangle x1="5.461" y1="3.937" x2="5.88518125" y2="4.02158125" layer="21"/>
<rectangle x1="6.223" y1="3.937" x2="7.32281875" y2="4.02158125" layer="21"/>
<rectangle x1="7.747" y1="3.937" x2="7.91718125" y2="4.02158125" layer="21"/>
<rectangle x1="8.255" y1="3.937" x2="9.017" y2="4.02158125" layer="21"/>
<rectangle x1="9.271" y1="3.937" x2="9.44118125" y2="4.02158125" layer="21"/>
<rectangle x1="10.795" y1="3.937" x2="11.98118125" y2="4.02158125" layer="21"/>
<rectangle x1="0.04318125" y1="4.02158125" x2="0.381" y2="4.10641875" layer="21"/>
<rectangle x1="1.48081875" y1="4.02158125" x2="1.82118125" y2="4.10641875" layer="21"/>
<rectangle x1="3.00481875" y1="4.02158125" x2="3.34518125" y2="4.10641875" layer="21"/>
<rectangle x1="4.61518125" y1="4.02158125" x2="4.953" y2="4.10641875" layer="21"/>
<rectangle x1="5.461" y1="4.02158125" x2="5.79881875" y2="4.10641875" layer="21"/>
<rectangle x1="6.13918125" y1="4.02158125" x2="7.40918125" y2="4.10641875" layer="21"/>
<rectangle x1="7.747" y1="4.02158125" x2="7.91718125" y2="4.10641875" layer="21"/>
<rectangle x1="8.255" y1="4.02158125" x2="9.017" y2="4.10641875" layer="21"/>
<rectangle x1="9.271" y1="4.02158125" x2="9.44118125" y2="4.10641875" layer="21"/>
<rectangle x1="10.795" y1="4.02158125" x2="11.98118125" y2="4.10641875" layer="21"/>
<rectangle x1="0.04318125" y1="4.10641875" x2="0.381" y2="4.191" layer="21"/>
<rectangle x1="1.48081875" y1="4.10641875" x2="1.82118125" y2="4.191" layer="21"/>
<rectangle x1="3.09118125" y1="4.10641875" x2="3.429" y2="4.191" layer="21"/>
<rectangle x1="4.61518125" y1="4.10641875" x2="4.953" y2="4.191" layer="21"/>
<rectangle x1="5.37718125" y1="4.10641875" x2="5.79881875" y2="4.191" layer="21"/>
<rectangle x1="6.13918125" y1="4.10641875" x2="7.40918125" y2="4.191" layer="21"/>
<rectangle x1="7.747" y1="4.10641875" x2="7.91718125" y2="4.191" layer="21"/>
<rectangle x1="8.255" y1="4.10641875" x2="9.017" y2="4.191" layer="21"/>
<rectangle x1="9.271" y1="4.10641875" x2="9.44118125" y2="4.191" layer="21"/>
<rectangle x1="10.795" y1="4.10641875" x2="11.98118125" y2="4.191" layer="21"/>
<rectangle x1="0.04318125" y1="4.191" x2="0.381" y2="4.27558125" layer="21"/>
<rectangle x1="1.48081875" y1="4.191" x2="1.73481875" y2="4.27558125" layer="21"/>
<rectangle x1="3.09118125" y1="4.191" x2="3.429" y2="4.27558125" layer="21"/>
<rectangle x1="4.699" y1="4.191" x2="4.953" y2="4.27558125" layer="21"/>
<rectangle x1="5.37718125" y1="4.191" x2="5.79881875" y2="4.27558125" layer="21"/>
<rectangle x1="6.13918125" y1="4.191" x2="7.40918125" y2="4.27558125" layer="21"/>
<rectangle x1="7.747" y1="4.191" x2="7.91718125" y2="4.27558125" layer="21"/>
<rectangle x1="8.255" y1="4.191" x2="8.93318125" y2="4.27558125" layer="21"/>
<rectangle x1="9.271" y1="4.191" x2="9.44118125" y2="4.27558125" layer="21"/>
<rectangle x1="9.779" y1="4.191" x2="10.45718125" y2="4.27558125" layer="21"/>
<rectangle x1="10.71118125" y1="4.191" x2="11.98118125" y2="4.27558125" layer="21"/>
<rectangle x1="0.04318125" y1="4.27558125" x2="0.381" y2="4.36041875" layer="21"/>
<rectangle x1="1.48081875" y1="4.27558125" x2="1.73481875" y2="4.36041875" layer="21"/>
<rectangle x1="3.09118125" y1="4.27558125" x2="3.429" y2="4.36041875" layer="21"/>
<rectangle x1="4.699" y1="4.27558125" x2="4.953" y2="4.36041875" layer="21"/>
<rectangle x1="5.37718125" y1="4.27558125" x2="5.79881875" y2="4.36041875" layer="21"/>
<rectangle x1="6.13918125" y1="4.27558125" x2="7.40918125" y2="4.36041875" layer="21"/>
<rectangle x1="7.747" y1="4.27558125" x2="7.91718125" y2="4.36041875" layer="21"/>
<rectangle x1="8.33881875" y1="4.27558125" x2="8.93318125" y2="4.36041875" layer="21"/>
<rectangle x1="9.271" y1="4.27558125" x2="9.525" y2="4.36041875" layer="21"/>
<rectangle x1="9.86281875" y1="4.27558125" x2="10.37081875" y2="4.36041875" layer="21"/>
<rectangle x1="10.71118125" y1="4.27558125" x2="11.98118125" y2="4.36041875" layer="21"/>
<rectangle x1="0.04318125" y1="4.36041875" x2="0.381" y2="4.445" layer="21"/>
<rectangle x1="1.48081875" y1="4.36041875" x2="1.73481875" y2="4.445" layer="21"/>
<rectangle x1="3.09118125" y1="4.36041875" x2="3.429" y2="4.445" layer="21"/>
<rectangle x1="4.699" y1="4.36041875" x2="4.953" y2="4.445" layer="21"/>
<rectangle x1="5.37718125" y1="4.36041875" x2="5.79881875" y2="4.445" layer="21"/>
<rectangle x1="6.13918125" y1="4.36041875" x2="7.40918125" y2="4.445" layer="21"/>
<rectangle x1="7.747" y1="4.36041875" x2="7.91718125" y2="4.445" layer="21"/>
<rectangle x1="8.509" y1="4.36041875" x2="8.763" y2="4.445" layer="21"/>
<rectangle x1="9.18718125" y1="4.36041875" x2="9.525" y2="4.445" layer="21"/>
<rectangle x1="9.94918125" y1="4.36041875" x2="10.20318125" y2="4.445" layer="21"/>
<rectangle x1="10.62481875" y1="4.36041875" x2="11.98118125" y2="4.445" layer="21"/>
<rectangle x1="0.04318125" y1="4.445" x2="0.381" y2="4.52958125" layer="21"/>
<rectangle x1="1.48081875" y1="4.445" x2="1.73481875" y2="4.52958125" layer="21"/>
<rectangle x1="3.09118125" y1="4.445" x2="3.429" y2="4.52958125" layer="21"/>
<rectangle x1="4.61518125" y1="4.445" x2="4.953" y2="4.52958125" layer="21"/>
<rectangle x1="5.37718125" y1="4.445" x2="5.79881875" y2="4.52958125" layer="21"/>
<rectangle x1="6.13918125" y1="4.445" x2="7.40918125" y2="4.52958125" layer="21"/>
<rectangle x1="7.747" y1="4.445" x2="7.91718125" y2="4.52958125" layer="21"/>
<rectangle x1="9.10081875" y1="4.445" x2="9.60881875" y2="4.52958125" layer="21"/>
<rectangle x1="10.541" y1="4.445" x2="11.98118125" y2="4.52958125" layer="21"/>
<rectangle x1="0.04318125" y1="4.52958125" x2="0.381" y2="4.61441875" layer="21"/>
<rectangle x1="1.48081875" y1="4.52958125" x2="1.73481875" y2="4.61441875" layer="21"/>
<rectangle x1="3.09118125" y1="4.52958125" x2="3.429" y2="4.61441875" layer="21"/>
<rectangle x1="4.61518125" y1="4.52958125" x2="4.953" y2="4.61441875" layer="21"/>
<rectangle x1="5.461" y1="4.52958125" x2="5.79881875" y2="4.61441875" layer="21"/>
<rectangle x1="6.13918125" y1="4.52958125" x2="7.40918125" y2="4.61441875" layer="21"/>
<rectangle x1="7.747" y1="4.52958125" x2="8.001" y2="4.61441875" layer="21"/>
<rectangle x1="9.017" y1="4.52958125" x2="9.69518125" y2="4.61441875" layer="21"/>
<rectangle x1="10.45718125" y1="4.52958125" x2="11.98118125" y2="4.61441875" layer="21"/>
<rectangle x1="0.04318125" y1="4.61441875" x2="0.381" y2="4.699" layer="21"/>
<rectangle x1="1.48081875" y1="4.61441875" x2="1.73481875" y2="4.699" layer="21"/>
<rectangle x1="3.09118125" y1="4.61441875" x2="3.429" y2="4.699" layer="21"/>
<rectangle x1="4.52881875" y1="4.61441875" x2="4.953" y2="4.699" layer="21"/>
<rectangle x1="5.461" y1="4.61441875" x2="5.79881875" y2="4.699" layer="21"/>
<rectangle x1="6.223" y1="4.61441875" x2="7.32281875" y2="4.699" layer="21"/>
<rectangle x1="7.747" y1="4.61441875" x2="8.001" y2="4.699" layer="21"/>
<rectangle x1="8.17118125" y1="4.61441875" x2="8.42518125" y2="4.699" layer="21"/>
<rectangle x1="8.84681875" y1="4.61441875" x2="9.86281875" y2="4.699" layer="21"/>
<rectangle x1="10.287" y1="4.61441875" x2="11.98118125" y2="4.699" layer="21"/>
<rectangle x1="0.04318125" y1="4.699" x2="0.381" y2="4.78358125" layer="21"/>
<rectangle x1="1.48081875" y1="4.699" x2="1.73481875" y2="4.78358125" layer="21"/>
<rectangle x1="3.09118125" y1="4.699" x2="3.429" y2="4.78358125" layer="21"/>
<rectangle x1="4.52881875" y1="4.699" x2="4.86918125" y2="4.78358125" layer="21"/>
<rectangle x1="5.461" y1="4.699" x2="5.88518125" y2="4.78358125" layer="21"/>
<rectangle x1="6.30681875" y1="4.699" x2="7.32281875" y2="4.78358125" layer="21"/>
<rectangle x1="7.66318125" y1="4.699" x2="11.98118125" y2="4.78358125" layer="21"/>
<rectangle x1="0.127" y1="4.78358125" x2="0.381" y2="4.86841875" layer="21"/>
<rectangle x1="1.48081875" y1="4.78358125" x2="1.73481875" y2="4.86841875" layer="21"/>
<rectangle x1="3.09118125" y1="4.78358125" x2="3.429" y2="4.86841875" layer="21"/>
<rectangle x1="4.445" y1="4.78358125" x2="4.86918125" y2="4.86841875" layer="21"/>
<rectangle x1="5.461" y1="4.78358125" x2="5.88518125" y2="4.86841875" layer="21"/>
<rectangle x1="6.39318125" y1="4.78358125" x2="7.239" y2="4.86841875" layer="21"/>
<rectangle x1="7.66318125" y1="4.78358125" x2="11.98118125" y2="4.86841875" layer="21"/>
<rectangle x1="0.127" y1="4.86841875" x2="0.46481875" y2="4.953" layer="21"/>
<rectangle x1="1.48081875" y1="4.86841875" x2="1.73481875" y2="4.953" layer="21"/>
<rectangle x1="3.09118125" y1="4.86841875" x2="3.429" y2="4.953" layer="21"/>
<rectangle x1="4.27481875" y1="4.86841875" x2="4.78281875" y2="4.953" layer="21"/>
<rectangle x1="5.461" y1="4.86841875" x2="5.969" y2="4.953" layer="21"/>
<rectangle x1="6.477" y1="4.86841875" x2="7.06881875" y2="4.953" layer="21"/>
<rectangle x1="7.57681875" y1="4.86841875" x2="11.98118125" y2="4.953" layer="21"/>
<rectangle x1="0.127" y1="4.953" x2="0.46481875" y2="5.03758125" layer="21"/>
<rectangle x1="1.48081875" y1="4.953" x2="1.73481875" y2="5.03758125" layer="21"/>
<rectangle x1="3.09118125" y1="4.953" x2="4.699" y2="5.03758125" layer="21"/>
<rectangle x1="5.461" y1="4.953" x2="6.05281875" y2="5.03758125" layer="21"/>
<rectangle x1="7.493" y1="4.953" x2="11.89481875" y2="5.03758125" layer="21"/>
<rectangle x1="0.127" y1="5.03758125" x2="0.46481875" y2="5.12241875" layer="21"/>
<rectangle x1="1.48081875" y1="5.03758125" x2="1.73481875" y2="5.12241875" layer="21"/>
<rectangle x1="3.09118125" y1="5.03758125" x2="4.61518125" y2="5.12241875" layer="21"/>
<rectangle x1="5.461" y1="5.03758125" x2="6.13918125" y2="5.12241875" layer="21"/>
<rectangle x1="7.40918125" y1="5.03758125" x2="11.89481875" y2="5.12241875" layer="21"/>
<rectangle x1="0.21081875" y1="5.12241875" x2="0.55118125" y2="5.207" layer="21"/>
<rectangle x1="1.48081875" y1="5.12241875" x2="1.73481875" y2="5.207" layer="21"/>
<rectangle x1="3.09118125" y1="5.12241875" x2="4.52881875" y2="5.207" layer="21"/>
<rectangle x1="5.461" y1="5.12241875" x2="6.223" y2="5.207" layer="21"/>
<rectangle x1="7.32281875" y1="5.12241875" x2="11.89481875" y2="5.207" layer="21"/>
<rectangle x1="0.21081875" y1="5.207" x2="0.55118125" y2="5.29158125" layer="21"/>
<rectangle x1="1.48081875" y1="5.207" x2="1.73481875" y2="5.29158125" layer="21"/>
<rectangle x1="3.175" y1="5.207" x2="4.36118125" y2="5.29158125" layer="21"/>
<rectangle x1="5.461" y1="5.207" x2="6.39318125" y2="5.29158125" layer="21"/>
<rectangle x1="7.15518125" y1="5.207" x2="11.811" y2="5.29158125" layer="21"/>
<rectangle x1="0.29718125" y1="5.29158125" x2="0.635" y2="5.37641875" layer="21"/>
<rectangle x1="5.54481875" y1="5.29158125" x2="11.811" y2="5.37641875" layer="21"/>
<rectangle x1="0.29718125" y1="5.37641875" x2="0.71881875" y2="5.461" layer="21"/>
<rectangle x1="5.54481875" y1="5.37641875" x2="11.72718125" y2="5.461" layer="21"/>
<rectangle x1="0.381" y1="5.461" x2="0.71881875" y2="5.54558125" layer="21"/>
<rectangle x1="5.54481875" y1="5.461" x2="11.72718125" y2="5.54558125" layer="21"/>
<rectangle x1="0.381" y1="5.54558125" x2="0.80518125" y2="5.63041875" layer="21"/>
<rectangle x1="5.54481875" y1="5.54558125" x2="11.64081875" y2="5.63041875" layer="21"/>
<rectangle x1="0.46481875" y1="5.63041875" x2="0.889" y2="5.715" layer="21"/>
<rectangle x1="5.63118125" y1="5.63041875" x2="11.64081875" y2="5.715" layer="21"/>
<rectangle x1="0.55118125" y1="5.715" x2="0.97281875" y2="5.79958125" layer="21"/>
<rectangle x1="5.63118125" y1="5.715" x2="11.557" y2="5.79958125" layer="21"/>
<rectangle x1="0.55118125" y1="5.79958125" x2="1.05918125" y2="5.88441875" layer="21"/>
<rectangle x1="5.63118125" y1="5.79958125" x2="11.47318125" y2="5.88441875" layer="21"/>
<rectangle x1="0.635" y1="5.88441875" x2="1.143" y2="5.969" layer="21"/>
<rectangle x1="5.715" y1="5.88441875" x2="11.38681875" y2="5.969" layer="21"/>
<rectangle x1="0.71881875" y1="5.969" x2="1.22681875" y2="6.05358125" layer="21"/>
<rectangle x1="5.715" y1="5.969" x2="11.303" y2="6.05358125" layer="21"/>
<rectangle x1="0.80518125" y1="6.05358125" x2="1.397" y2="6.13841875" layer="21"/>
<rectangle x1="5.79881875" y1="6.05358125" x2="11.21918125" y2="6.13841875" layer="21"/>
<rectangle x1="0.889" y1="6.13841875" x2="1.56718125" y2="6.223" layer="21"/>
<rectangle x1="5.79881875" y1="6.13841875" x2="11.13281875" y2="6.223" layer="21"/>
<rectangle x1="1.05918125" y1="6.223" x2="1.73481875" y2="6.30758125" layer="21"/>
<rectangle x1="5.88518125" y1="6.223" x2="11.049" y2="6.30758125" layer="21"/>
<rectangle x1="1.143" y1="6.30758125" x2="1.98881875" y2="6.39241875" layer="21"/>
<rectangle x1="5.88518125" y1="6.30758125" x2="10.87881875" y2="6.39241875" layer="21"/>
<rectangle x1="1.31318125" y1="6.39241875" x2="10.71118125" y2="6.477" layer="21"/>
<rectangle x1="1.48081875" y1="6.477" x2="10.541" y2="6.56158125" layer="21"/>
<rectangle x1="1.73481875" y1="6.56158125" x2="10.37081875" y2="6.64641875" layer="21"/>
</package>
<package name="UDO-LOGO-15MM" urn="urn:adsk.eagle:footprint:6649249/1">
<rectangle x1="2.24281875" y1="0.21158125" x2="2.58318125" y2="0.29641875" layer="21"/>
<rectangle x1="10.71118125" y1="0.21158125" x2="11.049" y2="0.29641875" layer="21"/>
<rectangle x1="1.98881875" y1="0.29641875" x2="2.83718125" y2="0.381" layer="21"/>
<rectangle x1="3.25881875" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="4.10718125" y1="0.29641875" x2="4.27481875" y2="0.381" layer="21"/>
<rectangle x1="4.445" y1="0.29641875" x2="4.52881875" y2="0.381" layer="21"/>
<rectangle x1="5.12318125" y1="0.29641875" x2="5.37718125" y2="0.381" layer="21"/>
<rectangle x1="5.63118125" y1="0.29641875" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="7.32281875" y1="0.29641875" x2="7.747" y2="0.381" layer="21"/>
<rectangle x1="8.33881875" y1="0.29641875" x2="8.67918125" y2="0.381" layer="21"/>
<rectangle x1="9.18718125" y1="0.29641875" x2="9.60881875" y2="0.381" layer="21"/>
<rectangle x1="10.45718125" y1="0.29641875" x2="11.303" y2="0.381" layer="21"/>
<rectangle x1="11.72718125" y1="0.29641875" x2="11.89481875" y2="0.381" layer="21"/>
<rectangle x1="12.573" y1="0.29641875" x2="12.74318125" y2="0.381" layer="21"/>
<rectangle x1="13.081" y1="0.29641875" x2="13.50518125" y2="0.381" layer="21"/>
<rectangle x1="1.905" y1="0.381" x2="2.24281875" y2="0.46558125" layer="21"/>
<rectangle x1="2.58318125" y1="0.381" x2="2.921" y2="0.46558125" layer="21"/>
<rectangle x1="3.25881875" y1="0.381" x2="3.429" y2="0.46558125" layer="21"/>
<rectangle x1="4.10718125" y1="0.381" x2="4.27481875" y2="0.46558125" layer="21"/>
<rectangle x1="4.445" y1="0.381" x2="4.61518125" y2="0.46558125" layer="21"/>
<rectangle x1="5.03681875" y1="0.381" x2="5.37718125" y2="0.46558125" layer="21"/>
<rectangle x1="5.63118125" y1="0.381" x2="6.56081875" y2="0.46558125" layer="21"/>
<rectangle x1="7.15518125" y1="0.381" x2="7.83081875" y2="0.46558125" layer="21"/>
<rectangle x1="8.17118125" y1="0.381" x2="8.763" y2="0.46558125" layer="21"/>
<rectangle x1="9.10081875" y1="0.381" x2="9.779" y2="0.46558125" layer="21"/>
<rectangle x1="10.37081875" y1="0.381" x2="10.71118125" y2="0.46558125" layer="21"/>
<rectangle x1="11.049" y1="0.381" x2="11.38681875" y2="0.46558125" layer="21"/>
<rectangle x1="11.72718125" y1="0.381" x2="11.89481875" y2="0.46558125" layer="21"/>
<rectangle x1="12.573" y1="0.381" x2="12.74318125" y2="0.46558125" layer="21"/>
<rectangle x1="12.99718125" y1="0.381" x2="13.67281875" y2="0.46558125" layer="21"/>
<rectangle x1="1.82118125" y1="0.46558125" x2="2.07518125" y2="0.55041875" layer="21"/>
<rectangle x1="2.75081875" y1="0.46558125" x2="3.00481875" y2="0.55041875" layer="21"/>
<rectangle x1="3.25881875" y1="0.46558125" x2="3.429" y2="0.55041875" layer="21"/>
<rectangle x1="4.10718125" y1="0.46558125" x2="4.27481875" y2="0.55041875" layer="21"/>
<rectangle x1="4.445" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="21"/>
<rectangle x1="4.953" y1="0.46558125" x2="5.12318125" y2="0.55041875" layer="21"/>
<rectangle x1="5.63118125" y1="0.46558125" x2="5.79881875" y2="0.55041875" layer="21"/>
<rectangle x1="6.39318125" y1="0.46558125" x2="6.731" y2="0.55041875" layer="21"/>
<rectangle x1="7.06881875" y1="0.46558125" x2="7.32281875" y2="0.55041875" layer="21"/>
<rectangle x1="7.66318125" y1="0.46558125" x2="7.91718125" y2="0.55041875" layer="21"/>
<rectangle x1="8.17118125" y1="0.46558125" x2="8.33881875" y2="0.55041875" layer="21"/>
<rectangle x1="8.59281875" y1="0.46558125" x2="8.84681875" y2="0.55041875" layer="21"/>
<rectangle x1="9.017" y1="0.46558125" x2="9.271" y2="0.55041875" layer="21"/>
<rectangle x1="9.60881875" y1="0.46558125" x2="9.86281875" y2="0.55041875" layer="21"/>
<rectangle x1="10.287" y1="0.46558125" x2="10.541" y2="0.55041875" layer="21"/>
<rectangle x1="11.21918125" y1="0.46558125" x2="11.47318125" y2="0.55041875" layer="21"/>
<rectangle x1="11.72718125" y1="0.46558125" x2="11.89481875" y2="0.55041875" layer="21"/>
<rectangle x1="12.573" y1="0.46558125" x2="12.74318125" y2="0.55041875" layer="21"/>
<rectangle x1="12.91081875" y1="0.46558125" x2="13.16481875" y2="0.55041875" layer="21"/>
<rectangle x1="13.50518125" y1="0.46558125" x2="13.75918125" y2="0.55041875" layer="21"/>
<rectangle x1="1.73481875" y1="0.55041875" x2="1.98881875" y2="0.635" layer="21"/>
<rectangle x1="2.83718125" y1="0.55041875" x2="3.09118125" y2="0.635" layer="21"/>
<rectangle x1="3.25881875" y1="0.55041875" x2="3.429" y2="0.635" layer="21"/>
<rectangle x1="4.10718125" y1="0.55041875" x2="4.27481875" y2="0.635" layer="21"/>
<rectangle x1="4.445" y1="0.55041875" x2="4.61518125" y2="0.635" layer="21"/>
<rectangle x1="4.86918125" y1="0.55041875" x2="5.03681875" y2="0.635" layer="21"/>
<rectangle x1="5.63118125" y1="0.55041875" x2="5.79881875" y2="0.635" layer="21"/>
<rectangle x1="6.56081875" y1="0.55041875" x2="6.81481875" y2="0.635" layer="21"/>
<rectangle x1="7.06881875" y1="0.55041875" x2="7.239" y2="0.635" layer="21"/>
<rectangle x1="7.747" y1="0.55041875" x2="8.001" y2="0.635" layer="21"/>
<rectangle x1="8.17118125" y1="0.55041875" x2="8.255" y2="0.635" layer="21"/>
<rectangle x1="8.67918125" y1="0.55041875" x2="8.84681875" y2="0.635" layer="21"/>
<rectangle x1="8.93318125" y1="0.55041875" x2="9.18718125" y2="0.635" layer="21"/>
<rectangle x1="9.69518125" y1="0.55041875" x2="9.86281875" y2="0.635" layer="21"/>
<rectangle x1="10.20318125" y1="0.55041875" x2="10.45718125" y2="0.635" layer="21"/>
<rectangle x1="11.303" y1="0.55041875" x2="11.557" y2="0.635" layer="21"/>
<rectangle x1="11.72718125" y1="0.55041875" x2="11.89481875" y2="0.635" layer="21"/>
<rectangle x1="12.573" y1="0.55041875" x2="12.74318125" y2="0.635" layer="21"/>
<rectangle x1="12.827" y1="0.55041875" x2="13.081" y2="0.635" layer="21"/>
<rectangle x1="13.589" y1="0.55041875" x2="13.75918125" y2="0.635" layer="21"/>
<rectangle x1="1.73481875" y1="0.635" x2="1.905" y2="0.71958125" layer="21"/>
<rectangle x1="2.921" y1="0.635" x2="3.09118125" y2="0.71958125" layer="21"/>
<rectangle x1="3.25881875" y1="0.635" x2="3.429" y2="0.71958125" layer="21"/>
<rectangle x1="4.10718125" y1="0.635" x2="4.27481875" y2="0.71958125" layer="21"/>
<rectangle x1="4.445" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="4.86918125" y1="0.635" x2="5.03681875" y2="0.71958125" layer="21"/>
<rectangle x1="5.63118125" y1="0.635" x2="5.79881875" y2="0.71958125" layer="21"/>
<rectangle x1="6.64718125" y1="0.635" x2="6.81481875" y2="0.71958125" layer="21"/>
<rectangle x1="6.985" y1="0.635" x2="7.239" y2="0.71958125" layer="21"/>
<rectangle x1="7.83081875" y1="0.635" x2="8.001" y2="0.71958125" layer="21"/>
<rectangle x1="8.59281875" y1="0.635" x2="8.84681875" y2="0.71958125" layer="21"/>
<rectangle x1="8.93318125" y1="0.635" x2="9.10081875" y2="0.71958125" layer="21"/>
<rectangle x1="10.20318125" y1="0.635" x2="10.37081875" y2="0.71958125" layer="21"/>
<rectangle x1="11.38681875" y1="0.635" x2="11.557" y2="0.71958125" layer="21"/>
<rectangle x1="11.72718125" y1="0.635" x2="11.89481875" y2="0.71958125" layer="21"/>
<rectangle x1="12.573" y1="0.635" x2="12.74318125" y2="0.71958125" layer="21"/>
<rectangle x1="12.827" y1="0.635" x2="12.99718125" y2="0.71958125" layer="21"/>
<rectangle x1="1.73481875" y1="0.71958125" x2="1.905" y2="0.80441875" layer="21"/>
<rectangle x1="2.921" y1="0.71958125" x2="3.175" y2="0.80441875" layer="21"/>
<rectangle x1="3.25881875" y1="0.71958125" x2="3.429" y2="0.80441875" layer="21"/>
<rectangle x1="4.10718125" y1="0.71958125" x2="4.27481875" y2="0.80441875" layer="21"/>
<rectangle x1="4.445" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="4.78281875" y1="0.71958125" x2="4.953" y2="0.80441875" layer="21"/>
<rectangle x1="5.63118125" y1="0.71958125" x2="5.79881875" y2="0.80441875" layer="21"/>
<rectangle x1="6.731" y1="0.71958125" x2="6.90118125" y2="0.80441875" layer="21"/>
<rectangle x1="6.985" y1="0.71958125" x2="7.15518125" y2="0.80441875" layer="21"/>
<rectangle x1="7.83081875" y1="0.71958125" x2="8.001" y2="0.80441875" layer="21"/>
<rectangle x1="8.33881875" y1="0.71958125" x2="8.763" y2="0.80441875" layer="21"/>
<rectangle x1="8.93318125" y1="0.71958125" x2="9.94918125" y2="0.80441875" layer="21"/>
<rectangle x1="10.20318125" y1="0.71958125" x2="10.37081875" y2="0.80441875" layer="21"/>
<rectangle x1="11.38681875" y1="0.71958125" x2="11.557" y2="0.80441875" layer="21"/>
<rectangle x1="11.72718125" y1="0.71958125" x2="11.89481875" y2="0.80441875" layer="21"/>
<rectangle x1="12.573" y1="0.71958125" x2="12.74318125" y2="0.80441875" layer="21"/>
<rectangle x1="12.827" y1="0.71958125" x2="13.843" y2="0.80441875" layer="21"/>
<rectangle x1="1.651" y1="0.80441875" x2="1.82118125" y2="0.889" layer="21"/>
<rectangle x1="3.00481875" y1="0.80441875" x2="3.175" y2="0.889" layer="21"/>
<rectangle x1="3.25881875" y1="0.80441875" x2="3.429" y2="0.889" layer="21"/>
<rectangle x1="4.10718125" y1="0.80441875" x2="4.27481875" y2="0.889" layer="21"/>
<rectangle x1="4.445" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="4.78281875" y1="0.80441875" x2="4.953" y2="0.889" layer="21"/>
<rectangle x1="5.63118125" y1="0.80441875" x2="5.79881875" y2="0.889" layer="21"/>
<rectangle x1="6.731" y1="0.80441875" x2="6.90118125" y2="0.889" layer="21"/>
<rectangle x1="6.985" y1="0.80441875" x2="7.15518125" y2="0.889" layer="21"/>
<rectangle x1="7.83081875" y1="0.80441875" x2="8.001" y2="0.889" layer="21"/>
<rectangle x1="8.17118125" y1="0.80441875" x2="8.59281875" y2="0.889" layer="21"/>
<rectangle x1="8.93318125" y1="0.80441875" x2="9.94918125" y2="0.889" layer="21"/>
<rectangle x1="10.11681875" y1="0.80441875" x2="10.287" y2="0.889" layer="21"/>
<rectangle x1="11.47318125" y1="0.80441875" x2="11.64081875" y2="0.889" layer="21"/>
<rectangle x1="11.72718125" y1="0.80441875" x2="11.89481875" y2="0.889" layer="21"/>
<rectangle x1="12.573" y1="0.80441875" x2="12.74318125" y2="0.889" layer="21"/>
<rectangle x1="12.827" y1="0.80441875" x2="13.843" y2="0.889" layer="21"/>
<rectangle x1="1.651" y1="0.889" x2="1.82118125" y2="0.97358125" layer="21"/>
<rectangle x1="3.00481875" y1="0.889" x2="3.175" y2="0.97358125" layer="21"/>
<rectangle x1="3.25881875" y1="0.889" x2="3.51281875" y2="0.97358125" layer="21"/>
<rectangle x1="4.10718125" y1="0.889" x2="4.27481875" y2="0.97358125" layer="21"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.61518125" y2="0.97358125" layer="21"/>
<rectangle x1="4.78281875" y1="0.889" x2="4.953" y2="0.97358125" layer="21"/>
<rectangle x1="5.63118125" y1="0.889" x2="5.79881875" y2="0.97358125" layer="21"/>
<rectangle x1="6.731" y1="0.889" x2="6.90118125" y2="0.97358125" layer="21"/>
<rectangle x1="6.985" y1="0.889" x2="7.239" y2="0.97358125" layer="21"/>
<rectangle x1="7.83081875" y1="0.889" x2="8.001" y2="0.97358125" layer="21"/>
<rectangle x1="8.08481875" y1="0.889" x2="8.42518125" y2="0.97358125" layer="21"/>
<rectangle x1="8.93318125" y1="0.889" x2="9.10081875" y2="0.97358125" layer="21"/>
<rectangle x1="9.779" y1="0.889" x2="9.94918125" y2="0.97358125" layer="21"/>
<rectangle x1="10.11681875" y1="0.889" x2="10.287" y2="0.97358125" layer="21"/>
<rectangle x1="11.47318125" y1="0.889" x2="11.64081875" y2="0.97358125" layer="21"/>
<rectangle x1="11.72718125" y1="0.889" x2="11.89481875" y2="0.97358125" layer="21"/>
<rectangle x1="12.48918125" y1="0.889" x2="12.74318125" y2="0.97358125" layer="21"/>
<rectangle x1="12.827" y1="0.889" x2="12.99718125" y2="0.97358125" layer="21"/>
<rectangle x1="13.589" y1="0.889" x2="13.843" y2="0.97358125" layer="21"/>
<rectangle x1="1.651" y1="0.97358125" x2="1.82118125" y2="1.05841875" layer="21"/>
<rectangle x1="3.00481875" y1="0.97358125" x2="3.175" y2="1.05841875" layer="21"/>
<rectangle x1="3.25881875" y1="0.97358125" x2="3.51281875" y2="1.05841875" layer="21"/>
<rectangle x1="4.02081875" y1="0.97358125" x2="4.27481875" y2="1.05841875" layer="21"/>
<rectangle x1="4.36118125" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="21"/>
<rectangle x1="4.78281875" y1="0.97358125" x2="4.953" y2="1.05841875" layer="21"/>
<rectangle x1="5.63118125" y1="0.97358125" x2="5.79881875" y2="1.05841875" layer="21"/>
<rectangle x1="6.731" y1="0.97358125" x2="6.90118125" y2="1.05841875" layer="21"/>
<rectangle x1="7.06881875" y1="0.97358125" x2="7.239" y2="1.05841875" layer="21"/>
<rectangle x1="7.747" y1="0.97358125" x2="8.001" y2="1.05841875" layer="21"/>
<rectangle x1="8.08481875" y1="0.97358125" x2="8.255" y2="1.05841875" layer="21"/>
<rectangle x1="8.67918125" y1="0.97358125" x2="8.84681875" y2="1.05841875" layer="21"/>
<rectangle x1="8.93318125" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="21"/>
<rectangle x1="9.69518125" y1="0.97358125" x2="9.86281875" y2="1.05841875" layer="21"/>
<rectangle x1="10.11681875" y1="0.97358125" x2="10.287" y2="1.05841875" layer="21"/>
<rectangle x1="11.47318125" y1="0.97358125" x2="11.64081875" y2="1.05841875" layer="21"/>
<rectangle x1="11.72718125" y1="0.97358125" x2="11.98118125" y2="1.05841875" layer="21"/>
<rectangle x1="12.48918125" y1="0.97358125" x2="12.65681875" y2="1.05841875" layer="21"/>
<rectangle x1="12.827" y1="0.97358125" x2="12.99718125" y2="1.05841875" layer="21"/>
<rectangle x1="13.589" y1="0.97358125" x2="13.75918125" y2="1.05841875" layer="21"/>
<rectangle x1="1.651" y1="1.05841875" x2="1.82118125" y2="1.143" layer="21"/>
<rectangle x1="3.00481875" y1="1.05841875" x2="3.175" y2="1.143" layer="21"/>
<rectangle x1="3.25881875" y1="1.05841875" x2="3.59918125" y2="1.143" layer="21"/>
<rectangle x1="3.937" y1="1.05841875" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="4.36118125" y1="1.05841875" x2="4.61518125" y2="1.143" layer="21"/>
<rectangle x1="4.78281875" y1="1.05841875" x2="4.953" y2="1.143" layer="21"/>
<rectangle x1="5.63118125" y1="1.05841875" x2="5.79881875" y2="1.143" layer="21"/>
<rectangle x1="6.731" y1="1.05841875" x2="6.90118125" y2="1.143" layer="21"/>
<rectangle x1="7.06881875" y1="1.05841875" x2="7.32281875" y2="1.143" layer="21"/>
<rectangle x1="7.66318125" y1="1.05841875" x2="7.91718125" y2="1.143" layer="21"/>
<rectangle x1="8.08481875" y1="1.05841875" x2="8.33881875" y2="1.143" layer="21"/>
<rectangle x1="8.59281875" y1="1.05841875" x2="8.84681875" y2="1.143" layer="21"/>
<rectangle x1="9.017" y1="1.05841875" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="9.60881875" y1="1.05841875" x2="9.86281875" y2="1.143" layer="21"/>
<rectangle x1="10.11681875" y1="1.05841875" x2="10.287" y2="1.143" layer="21"/>
<rectangle x1="11.47318125" y1="1.05841875" x2="11.64081875" y2="1.143" layer="21"/>
<rectangle x1="11.72718125" y1="1.05841875" x2="12.065" y2="1.143" layer="21"/>
<rectangle x1="12.40281875" y1="1.05841875" x2="12.65681875" y2="1.143" layer="21"/>
<rectangle x1="12.91081875" y1="1.05841875" x2="13.16481875" y2="1.143" layer="21"/>
<rectangle x1="13.50518125" y1="1.05841875" x2="13.75918125" y2="1.143" layer="21"/>
<rectangle x1="1.651" y1="1.143" x2="1.82118125" y2="1.22758125" layer="21"/>
<rectangle x1="3.00481875" y1="1.143" x2="3.175" y2="1.22758125" layer="21"/>
<rectangle x1="3.25881875" y1="1.143" x2="4.10718125" y2="1.22758125" layer="21"/>
<rectangle x1="4.36118125" y1="1.143" x2="4.61518125" y2="1.22758125" layer="21"/>
<rectangle x1="4.699" y1="1.143" x2="5.37718125" y2="1.22758125" layer="21"/>
<rectangle x1="5.63118125" y1="1.143" x2="5.79881875" y2="1.22758125" layer="21"/>
<rectangle x1="6.731" y1="1.143" x2="6.90118125" y2="1.22758125" layer="21"/>
<rectangle x1="7.15518125" y1="1.143" x2="7.83081875" y2="1.22758125" layer="21"/>
<rectangle x1="8.17118125" y1="1.143" x2="8.763" y2="1.22758125" layer="21"/>
<rectangle x1="9.10081875" y1="1.143" x2="9.779" y2="1.22758125" layer="21"/>
<rectangle x1="10.20318125" y1="1.143" x2="10.37081875" y2="1.22758125" layer="21"/>
<rectangle x1="11.38681875" y1="1.143" x2="11.557" y2="1.22758125" layer="21"/>
<rectangle x1="11.72718125" y1="1.143" x2="12.573" y2="1.22758125" layer="21"/>
<rectangle x1="12.99718125" y1="1.143" x2="13.67281875" y2="1.22758125" layer="21"/>
<rectangle x1="1.651" y1="1.22758125" x2="1.82118125" y2="1.31241875" layer="21"/>
<rectangle x1="3.00481875" y1="1.22758125" x2="3.175" y2="1.31241875" layer="21"/>
<rectangle x1="3.25881875" y1="1.22758125" x2="3.429" y2="1.31241875" layer="21"/>
<rectangle x1="3.59918125" y1="1.22758125" x2="4.02081875" y2="1.31241875" layer="21"/>
<rectangle x1="4.445" y1="1.22758125" x2="4.52881875" y2="1.31241875" layer="21"/>
<rectangle x1="4.699" y1="1.22758125" x2="5.37718125" y2="1.31241875" layer="21"/>
<rectangle x1="5.63118125" y1="1.22758125" x2="5.79881875" y2="1.31241875" layer="21"/>
<rectangle x1="6.731" y1="1.22758125" x2="6.90118125" y2="1.31241875" layer="21"/>
<rectangle x1="7.32281875" y1="1.22758125" x2="7.747" y2="1.31241875" layer="21"/>
<rectangle x1="8.255" y1="1.22758125" x2="8.67918125" y2="1.31241875" layer="21"/>
<rectangle x1="9.18718125" y1="1.22758125" x2="9.60881875" y2="1.31241875" layer="21"/>
<rectangle x1="10.20318125" y1="1.22758125" x2="10.37081875" y2="1.31241875" layer="21"/>
<rectangle x1="11.38681875" y1="1.22758125" x2="11.557" y2="1.31241875" layer="21"/>
<rectangle x1="11.72718125" y1="1.22758125" x2="11.89481875" y2="1.31241875" layer="21"/>
<rectangle x1="11.98118125" y1="1.22758125" x2="12.40281875" y2="1.31241875" layer="21"/>
<rectangle x1="13.081" y1="1.22758125" x2="13.50518125" y2="1.31241875" layer="21"/>
<rectangle x1="1.651" y1="1.31241875" x2="1.82118125" y2="1.397" layer="21"/>
<rectangle x1="3.00481875" y1="1.31241875" x2="3.175" y2="1.397" layer="21"/>
<rectangle x1="4.445" y1="1.31241875" x2="4.52881875" y2="1.397" layer="21"/>
<rectangle x1="4.78281875" y1="1.31241875" x2="4.953" y2="1.397" layer="21"/>
<rectangle x1="5.63118125" y1="1.31241875" x2="5.79881875" y2="1.397" layer="21"/>
<rectangle x1="6.64718125" y1="1.31241875" x2="6.90118125" y2="1.397" layer="21"/>
<rectangle x1="10.20318125" y1="1.31241875" x2="10.45718125" y2="1.397" layer="21"/>
<rectangle x1="11.303" y1="1.31241875" x2="11.557" y2="1.397" layer="21"/>
<rectangle x1="1.651" y1="1.397" x2="1.82118125" y2="1.48158125" layer="21"/>
<rectangle x1="3.00481875" y1="1.397" x2="3.175" y2="1.48158125" layer="21"/>
<rectangle x1="4.36118125" y1="1.397" x2="4.61518125" y2="1.48158125" layer="21"/>
<rectangle x1="4.78281875" y1="1.397" x2="4.953" y2="1.48158125" layer="21"/>
<rectangle x1="5.63118125" y1="1.397" x2="5.79881875" y2="1.48158125" layer="21"/>
<rectangle x1="6.56081875" y1="1.397" x2="6.81481875" y2="1.48158125" layer="21"/>
<rectangle x1="10.287" y1="1.397" x2="10.541" y2="1.48158125" layer="21"/>
<rectangle x1="11.21918125" y1="1.397" x2="11.47318125" y2="1.48158125" layer="21"/>
<rectangle x1="1.651" y1="1.48158125" x2="1.82118125" y2="1.56641875" layer="21"/>
<rectangle x1="3.00481875" y1="1.48158125" x2="3.175" y2="1.56641875" layer="21"/>
<rectangle x1="4.445" y1="1.48158125" x2="4.52881875" y2="1.56641875" layer="21"/>
<rectangle x1="4.78281875" y1="1.48158125" x2="4.953" y2="1.56641875" layer="21"/>
<rectangle x1="5.63118125" y1="1.48158125" x2="5.79881875" y2="1.56641875" layer="21"/>
<rectangle x1="6.477" y1="1.48158125" x2="6.731" y2="1.56641875" layer="21"/>
<rectangle x1="10.37081875" y1="1.48158125" x2="10.71118125" y2="1.56641875" layer="21"/>
<rectangle x1="11.049" y1="1.48158125" x2="11.38681875" y2="1.56641875" layer="21"/>
<rectangle x1="1.651" y1="1.56641875" x2="1.82118125" y2="1.651" layer="21"/>
<rectangle x1="3.00481875" y1="1.56641875" x2="3.175" y2="1.651" layer="21"/>
<rectangle x1="4.78281875" y1="1.56641875" x2="4.953" y2="1.651" layer="21"/>
<rectangle x1="5.63118125" y1="1.56641875" x2="5.79881875" y2="1.651" layer="21"/>
<rectangle x1="5.88518125" y1="1.56641875" x2="6.64718125" y2="1.651" layer="21"/>
<rectangle x1="10.541" y1="1.56641875" x2="11.21918125" y2="1.651" layer="21"/>
<rectangle x1="1.651" y1="1.651" x2="1.82118125" y2="1.73558125" layer="21"/>
<rectangle x1="3.00481875" y1="1.651" x2="3.175" y2="1.73558125" layer="21"/>
<rectangle x1="4.86918125" y1="1.651" x2="4.953" y2="1.73558125" layer="21"/>
<rectangle x1="5.63118125" y1="1.651" x2="6.56081875" y2="1.73558125" layer="21"/>
<rectangle x1="10.71118125" y1="1.651" x2="11.049" y2="1.73558125" layer="21"/>
<rectangle x1="5.715" y1="1.73558125" x2="6.30681875" y2="1.82041875" layer="21"/>
<rectangle x1="2.49681875" y1="2.413" x2="12.573" y2="2.49758125" layer="21"/>
<rectangle x1="2.159" y1="2.49758125" x2="12.91081875" y2="2.58241875" layer="21"/>
<rectangle x1="1.905" y1="2.58241875" x2="13.16481875" y2="2.667" layer="21"/>
<rectangle x1="1.73481875" y1="2.667" x2="13.335" y2="2.75158125" layer="21"/>
<rectangle x1="1.56718125" y1="2.75158125" x2="13.50518125" y2="2.83641875" layer="21"/>
<rectangle x1="1.397" y1="2.83641875" x2="2.667" y2="2.921" layer="21"/>
<rectangle x1="7.32281875" y1="2.83641875" x2="13.589" y2="2.921" layer="21"/>
<rectangle x1="1.31318125" y1="2.921" x2="2.32918125" y2="3.00558125" layer="21"/>
<rectangle x1="7.32281875" y1="2.921" x2="13.75918125" y2="3.00558125" layer="21"/>
<rectangle x1="1.22681875" y1="3.00558125" x2="2.07518125" y2="3.09041875" layer="21"/>
<rectangle x1="7.239" y1="3.00558125" x2="13.843" y2="3.09041875" layer="21"/>
<rectangle x1="1.05918125" y1="3.09041875" x2="1.905" y2="3.175" layer="21"/>
<rectangle x1="7.15518125" y1="3.09041875" x2="13.92681875" y2="3.175" layer="21"/>
<rectangle x1="0.97281875" y1="3.175" x2="1.73481875" y2="3.25958125" layer="21"/>
<rectangle x1="7.15518125" y1="3.175" x2="14.097" y2="3.25958125" layer="21"/>
<rectangle x1="0.889" y1="3.25958125" x2="1.56718125" y2="3.34441875" layer="21"/>
<rectangle x1="7.15518125" y1="3.25958125" x2="14.18081875" y2="3.34441875" layer="21"/>
<rectangle x1="0.80518125" y1="3.34441875" x2="1.48081875" y2="3.429" layer="21"/>
<rectangle x1="7.06881875" y1="3.34441875" x2="14.26718125" y2="3.429" layer="21"/>
<rectangle x1="0.71881875" y1="3.429" x2="1.397" y2="3.51358125" layer="21"/>
<rectangle x1="7.06881875" y1="3.429" x2="14.26718125" y2="3.51358125" layer="21"/>
<rectangle x1="0.71881875" y1="3.51358125" x2="1.31318125" y2="3.59841875" layer="21"/>
<rectangle x1="6.985" y1="3.51358125" x2="14.351" y2="3.59841875" layer="21"/>
<rectangle x1="0.635" y1="3.59841875" x2="1.22681875" y2="3.683" layer="21"/>
<rectangle x1="6.985" y1="3.59841875" x2="14.43481875" y2="3.683" layer="21"/>
<rectangle x1="0.55118125" y1="3.683" x2="1.143" y2="3.76758125" layer="21"/>
<rectangle x1="6.985" y1="3.683" x2="14.52118125" y2="3.76758125" layer="21"/>
<rectangle x1="0.46481875" y1="3.76758125" x2="1.05918125" y2="3.85241875" layer="21"/>
<rectangle x1="6.90118125" y1="3.76758125" x2="14.52118125" y2="3.85241875" layer="21"/>
<rectangle x1="0.46481875" y1="3.85241875" x2="0.97281875" y2="3.937" layer="21"/>
<rectangle x1="6.90118125" y1="3.85241875" x2="14.605" y2="3.937" layer="21"/>
<rectangle x1="0.381" y1="3.937" x2="0.889" y2="4.02158125" layer="21"/>
<rectangle x1="6.90118125" y1="3.937" x2="14.68881875" y2="4.02158125" layer="21"/>
<rectangle x1="0.381" y1="4.02158125" x2="0.889" y2="4.10641875" layer="21"/>
<rectangle x1="6.90118125" y1="4.02158125" x2="14.68881875" y2="4.10641875" layer="21"/>
<rectangle x1="0.29718125" y1="4.10641875" x2="0.80518125" y2="4.191" layer="21"/>
<rectangle x1="6.81481875" y1="4.10641875" x2="14.77518125" y2="4.191" layer="21"/>
<rectangle x1="0.29718125" y1="4.191" x2="0.80518125" y2="4.27558125" layer="21"/>
<rectangle x1="2.58318125" y1="4.191" x2="3.51281875" y2="4.27558125" layer="21"/>
<rectangle x1="4.27481875" y1="4.191" x2="5.461" y2="4.27558125" layer="21"/>
<rectangle x1="6.81481875" y1="4.191" x2="8.17118125" y2="4.27558125" layer="21"/>
<rectangle x1="8.763" y1="4.191" x2="10.033" y2="4.27558125" layer="21"/>
<rectangle x1="10.11681875" y1="4.191" x2="11.38681875" y2="4.27558125" layer="21"/>
<rectangle x1="11.47318125" y1="4.191" x2="12.48918125" y2="4.27558125" layer="21"/>
<rectangle x1="12.65681875" y1="4.191" x2="14.77518125" y2="4.27558125" layer="21"/>
<rectangle x1="0.21081875" y1="4.27558125" x2="0.71881875" y2="4.36041875" layer="21"/>
<rectangle x1="2.413" y1="4.27558125" x2="3.683" y2="4.36041875" layer="21"/>
<rectangle x1="4.27481875" y1="4.27558125" x2="5.63118125" y2="4.36041875" layer="21"/>
<rectangle x1="6.81481875" y1="4.27558125" x2="7.91718125" y2="4.36041875" layer="21"/>
<rectangle x1="9.017" y1="4.27558125" x2="9.94918125" y2="4.36041875" layer="21"/>
<rectangle x1="10.20318125" y1="4.27558125" x2="11.303" y2="4.36041875" layer="21"/>
<rectangle x1="11.557" y1="4.27558125" x2="12.23518125" y2="4.36041875" layer="21"/>
<rectangle x1="12.99718125" y1="4.27558125" x2="14.859" y2="4.36041875" layer="21"/>
<rectangle x1="0.21081875" y1="4.36041875" x2="0.71881875" y2="4.445" layer="21"/>
<rectangle x1="2.32918125" y1="4.36041875" x2="3.76681875" y2="4.445" layer="21"/>
<rectangle x1="4.27481875" y1="4.36041875" x2="5.715" y2="4.445" layer="21"/>
<rectangle x1="6.81481875" y1="4.36041875" x2="7.747" y2="4.445" layer="21"/>
<rectangle x1="9.18718125" y1="4.36041875" x2="9.94918125" y2="4.445" layer="21"/>
<rectangle x1="10.287" y1="4.36041875" x2="11.303" y2="4.445" layer="21"/>
<rectangle x1="11.557" y1="4.36041875" x2="12.065" y2="4.445" layer="21"/>
<rectangle x1="13.16481875" y1="4.36041875" x2="14.859" y2="4.445" layer="21"/>
<rectangle x1="0.21081875" y1="4.445" x2="0.635" y2="4.52958125" layer="21"/>
<rectangle x1="2.159" y1="4.445" x2="3.85318125" y2="4.52958125" layer="21"/>
<rectangle x1="4.27481875" y1="4.445" x2="5.79881875" y2="4.52958125" layer="21"/>
<rectangle x1="6.81481875" y1="4.445" x2="7.66318125" y2="4.52958125" layer="21"/>
<rectangle x1="9.271" y1="4.445" x2="9.94918125" y2="4.52958125" layer="21"/>
<rectangle x1="10.287" y1="4.445" x2="11.303" y2="4.52958125" layer="21"/>
<rectangle x1="11.557" y1="4.445" x2="11.98118125" y2="4.52958125" layer="21"/>
<rectangle x1="13.25118125" y1="4.445" x2="14.859" y2="4.52958125" layer="21"/>
<rectangle x1="0.127" y1="4.52958125" x2="0.635" y2="4.61441875" layer="21"/>
<rectangle x1="2.07518125" y1="4.52958125" x2="3.937" y2="4.61441875" layer="21"/>
<rectangle x1="4.36118125" y1="4.52958125" x2="5.88518125" y2="4.61441875" layer="21"/>
<rectangle x1="6.81481875" y1="4.52958125" x2="7.57681875" y2="4.61441875" layer="21"/>
<rectangle x1="8.33881875" y1="4.52958125" x2="8.59281875" y2="4.61441875" layer="21"/>
<rectangle x1="9.35481875" y1="4.52958125" x2="9.94918125" y2="4.61441875" layer="21"/>
<rectangle x1="10.287" y1="4.52958125" x2="11.303" y2="4.61441875" layer="21"/>
<rectangle x1="11.557" y1="4.52958125" x2="11.89481875" y2="4.61441875" layer="21"/>
<rectangle x1="12.40281875" y1="4.52958125" x2="12.91081875" y2="4.61441875" layer="21"/>
<rectangle x1="13.335" y1="4.52958125" x2="14.94281875" y2="4.61441875" layer="21"/>
<rectangle x1="0.127" y1="4.61441875" x2="0.635" y2="4.699" layer="21"/>
<rectangle x1="2.07518125" y1="4.61441875" x2="2.75081875" y2="4.699" layer="21"/>
<rectangle x1="3.34518125" y1="4.61441875" x2="4.02081875" y2="4.699" layer="21"/>
<rectangle x1="5.29081875" y1="4.61441875" x2="5.969" y2="4.699" layer="21"/>
<rectangle x1="6.81481875" y1="4.61441875" x2="7.493" y2="4.699" layer="21"/>
<rectangle x1="8.08481875" y1="4.61441875" x2="8.84681875" y2="4.699" layer="21"/>
<rectangle x1="9.44118125" y1="4.61441875" x2="9.94918125" y2="4.699" layer="21"/>
<rectangle x1="10.287" y1="4.61441875" x2="11.303" y2="4.699" layer="21"/>
<rectangle x1="11.557" y1="4.61441875" x2="11.89481875" y2="4.699" layer="21"/>
<rectangle x1="12.23518125" y1="4.61441875" x2="12.99718125" y2="4.699" layer="21"/>
<rectangle x1="13.335" y1="4.61441875" x2="14.94281875" y2="4.699" layer="21"/>
<rectangle x1="0.127" y1="4.699" x2="0.55118125" y2="4.78358125" layer="21"/>
<rectangle x1="1.98881875" y1="4.699" x2="2.58318125" y2="4.78358125" layer="21"/>
<rectangle x1="3.51281875" y1="4.699" x2="4.10718125" y2="4.78358125" layer="21"/>
<rectangle x1="5.461" y1="4.699" x2="6.05281875" y2="4.78358125" layer="21"/>
<rectangle x1="6.731" y1="4.699" x2="7.40918125" y2="4.78358125" layer="21"/>
<rectangle x1="7.91718125" y1="4.699" x2="9.017" y2="4.78358125" layer="21"/>
<rectangle x1="9.44118125" y1="4.699" x2="9.94918125" y2="4.78358125" layer="21"/>
<rectangle x1="10.287" y1="4.699" x2="11.303" y2="4.78358125" layer="21"/>
<rectangle x1="11.557" y1="4.699" x2="11.811" y2="4.78358125" layer="21"/>
<rectangle x1="12.14881875" y1="4.699" x2="13.081" y2="4.78358125" layer="21"/>
<rectangle x1="13.25118125" y1="4.699" x2="14.94281875" y2="4.78358125" layer="21"/>
<rectangle x1="0.04318125" y1="4.78358125" x2="0.55118125" y2="4.86841875" layer="21"/>
<rectangle x1="1.905" y1="4.78358125" x2="2.49681875" y2="4.86841875" layer="21"/>
<rectangle x1="3.59918125" y1="4.78358125" x2="4.191" y2="4.86841875" layer="21"/>
<rectangle x1="5.54481875" y1="4.78358125" x2="6.13918125" y2="4.86841875" layer="21"/>
<rectangle x1="6.731" y1="4.78358125" x2="7.40918125" y2="4.86841875" layer="21"/>
<rectangle x1="7.83081875" y1="4.78358125" x2="9.10081875" y2="4.86841875" layer="21"/>
<rectangle x1="9.525" y1="4.78358125" x2="9.94918125" y2="4.86841875" layer="21"/>
<rectangle x1="10.287" y1="4.78358125" x2="11.303" y2="4.86841875" layer="21"/>
<rectangle x1="11.557" y1="4.78358125" x2="11.811" y2="4.86841875" layer="21"/>
<rectangle x1="12.14881875" y1="4.78358125" x2="14.94281875" y2="4.86841875" layer="21"/>
<rectangle x1="0.04318125" y1="4.86841875" x2="0.55118125" y2="4.953" layer="21"/>
<rectangle x1="1.905" y1="4.86841875" x2="2.413" y2="4.953" layer="21"/>
<rectangle x1="3.683" y1="4.86841875" x2="4.191" y2="4.953" layer="21"/>
<rectangle x1="5.63118125" y1="4.86841875" x2="6.13918125" y2="4.953" layer="21"/>
<rectangle x1="6.731" y1="4.86841875" x2="7.32281875" y2="4.953" layer="21"/>
<rectangle x1="7.747" y1="4.86841875" x2="9.10081875" y2="4.953" layer="21"/>
<rectangle x1="9.60881875" y1="4.86841875" x2="9.94918125" y2="4.953" layer="21"/>
<rectangle x1="10.287" y1="4.86841875" x2="11.303" y2="4.953" layer="21"/>
<rectangle x1="11.557" y1="4.86841875" x2="11.811" y2="4.953" layer="21"/>
<rectangle x1="13.335" y1="4.86841875" x2="14.94281875" y2="4.953" layer="21"/>
<rectangle x1="0.04318125" y1="4.953" x2="0.55118125" y2="5.03758125" layer="21"/>
<rectangle x1="1.82118125" y1="4.953" x2="2.32918125" y2="5.03758125" layer="21"/>
<rectangle x1="3.76681875" y1="4.953" x2="4.191" y2="5.03758125" layer="21"/>
<rectangle x1="5.715" y1="4.953" x2="6.13918125" y2="5.03758125" layer="21"/>
<rectangle x1="6.731" y1="4.953" x2="7.32281875" y2="5.03758125" layer="21"/>
<rectangle x1="7.747" y1="4.953" x2="9.18718125" y2="5.03758125" layer="21"/>
<rectangle x1="9.60881875" y1="4.953" x2="9.94918125" y2="5.03758125" layer="21"/>
<rectangle x1="10.287" y1="4.953" x2="11.303" y2="5.03758125" layer="21"/>
<rectangle x1="11.557" y1="4.953" x2="11.811" y2="5.03758125" layer="21"/>
<rectangle x1="13.41881875" y1="4.953" x2="14.94281875" y2="5.03758125" layer="21"/>
<rectangle x1="0.04318125" y1="5.03758125" x2="0.46481875" y2="5.12241875" layer="21"/>
<rectangle x1="1.82118125" y1="5.03758125" x2="2.32918125" y2="5.12241875" layer="21"/>
<rectangle x1="3.76681875" y1="5.03758125" x2="4.27481875" y2="5.12241875" layer="21"/>
<rectangle x1="5.715" y1="5.03758125" x2="6.223" y2="5.12241875" layer="21"/>
<rectangle x1="6.731" y1="5.03758125" x2="7.32281875" y2="5.12241875" layer="21"/>
<rectangle x1="7.66318125" y1="5.03758125" x2="9.271" y2="5.12241875" layer="21"/>
<rectangle x1="9.60881875" y1="5.03758125" x2="9.94918125" y2="5.12241875" layer="21"/>
<rectangle x1="10.287" y1="5.03758125" x2="11.303" y2="5.12241875" layer="21"/>
<rectangle x1="11.557" y1="5.03758125" x2="11.811" y2="5.12241875" layer="21"/>
<rectangle x1="13.41881875" y1="5.03758125" x2="14.94281875" y2="5.12241875" layer="21"/>
<rectangle x1="0.04318125" y1="5.12241875" x2="0.46481875" y2="5.207" layer="21"/>
<rectangle x1="1.82118125" y1="5.12241875" x2="2.24281875" y2="5.207" layer="21"/>
<rectangle x1="3.76681875" y1="5.12241875" x2="4.27481875" y2="5.207" layer="21"/>
<rectangle x1="5.79881875" y1="5.12241875" x2="6.223" y2="5.207" layer="21"/>
<rectangle x1="6.731" y1="5.12241875" x2="7.239" y2="5.207" layer="21"/>
<rectangle x1="7.66318125" y1="5.12241875" x2="9.271" y2="5.207" layer="21"/>
<rectangle x1="9.69518125" y1="5.12241875" x2="9.94918125" y2="5.207" layer="21"/>
<rectangle x1="10.287" y1="5.12241875" x2="11.303" y2="5.207" layer="21"/>
<rectangle x1="11.557" y1="5.12241875" x2="11.811" y2="5.207" layer="21"/>
<rectangle x1="13.41881875" y1="5.12241875" x2="14.94281875" y2="5.207" layer="21"/>
<rectangle x1="0.04318125" y1="5.207" x2="0.46481875" y2="5.29158125" layer="21"/>
<rectangle x1="1.82118125" y1="5.207" x2="2.24281875" y2="5.29158125" layer="21"/>
<rectangle x1="3.85318125" y1="5.207" x2="4.27481875" y2="5.29158125" layer="21"/>
<rectangle x1="5.79881875" y1="5.207" x2="6.223" y2="5.29158125" layer="21"/>
<rectangle x1="6.731" y1="5.207" x2="7.239" y2="5.29158125" layer="21"/>
<rectangle x1="7.66318125" y1="5.207" x2="9.271" y2="5.29158125" layer="21"/>
<rectangle x1="9.69518125" y1="5.207" x2="9.94918125" y2="5.29158125" layer="21"/>
<rectangle x1="10.287" y1="5.207" x2="11.21918125" y2="5.29158125" layer="21"/>
<rectangle x1="11.557" y1="5.207" x2="11.811" y2="5.29158125" layer="21"/>
<rectangle x1="12.14881875" y1="5.207" x2="13.081" y2="5.29158125" layer="21"/>
<rectangle x1="13.335" y1="5.207" x2="14.94281875" y2="5.29158125" layer="21"/>
<rectangle x1="0.04318125" y1="5.29158125" x2="0.46481875" y2="5.37641875" layer="21"/>
<rectangle x1="1.82118125" y1="5.29158125" x2="2.24281875" y2="5.37641875" layer="21"/>
<rectangle x1="3.85318125" y1="5.29158125" x2="4.27481875" y2="5.37641875" layer="21"/>
<rectangle x1="5.79881875" y1="5.29158125" x2="6.223" y2="5.37641875" layer="21"/>
<rectangle x1="6.731" y1="5.29158125" x2="7.239" y2="5.37641875" layer="21"/>
<rectangle x1="7.57681875" y1="5.29158125" x2="9.271" y2="5.37641875" layer="21"/>
<rectangle x1="9.69518125" y1="5.29158125" x2="9.94918125" y2="5.37641875" layer="21"/>
<rectangle x1="10.37081875" y1="5.29158125" x2="11.21918125" y2="5.37641875" layer="21"/>
<rectangle x1="11.557" y1="5.29158125" x2="11.811" y2="5.37641875" layer="21"/>
<rectangle x1="12.14881875" y1="5.29158125" x2="12.99718125" y2="5.37641875" layer="21"/>
<rectangle x1="13.335" y1="5.29158125" x2="14.94281875" y2="5.37641875" layer="21"/>
<rectangle x1="0.04318125" y1="5.37641875" x2="0.46481875" y2="5.461" layer="21"/>
<rectangle x1="1.82118125" y1="5.37641875" x2="2.24281875" y2="5.461" layer="21"/>
<rectangle x1="3.85318125" y1="5.37641875" x2="4.27481875" y2="5.461" layer="21"/>
<rectangle x1="5.79881875" y1="5.37641875" x2="6.223" y2="5.461" layer="21"/>
<rectangle x1="6.731" y1="5.37641875" x2="7.239" y2="5.461" layer="21"/>
<rectangle x1="7.57681875" y1="5.37641875" x2="9.271" y2="5.461" layer="21"/>
<rectangle x1="9.69518125" y1="5.37641875" x2="9.94918125" y2="5.461" layer="21"/>
<rectangle x1="10.45718125" y1="5.37641875" x2="11.13281875" y2="5.461" layer="21"/>
<rectangle x1="11.47318125" y1="5.37641875" x2="11.89481875" y2="5.461" layer="21"/>
<rectangle x1="12.23518125" y1="5.37641875" x2="12.91081875" y2="5.461" layer="21"/>
<rectangle x1="13.335" y1="5.37641875" x2="14.94281875" y2="5.461" layer="21"/>
<rectangle x1="0.04318125" y1="5.461" x2="0.46481875" y2="5.54558125" layer="21"/>
<rectangle x1="1.82118125" y1="5.461" x2="2.24281875" y2="5.54558125" layer="21"/>
<rectangle x1="3.85318125" y1="5.461" x2="4.27481875" y2="5.54558125" layer="21"/>
<rectangle x1="5.79881875" y1="5.461" x2="6.223" y2="5.54558125" layer="21"/>
<rectangle x1="6.731" y1="5.461" x2="7.239" y2="5.54558125" layer="21"/>
<rectangle x1="7.57681875" y1="5.461" x2="9.271" y2="5.54558125" layer="21"/>
<rectangle x1="9.69518125" y1="5.461" x2="9.94918125" y2="5.54558125" layer="21"/>
<rectangle x1="10.541" y1="5.461" x2="10.96518125" y2="5.54558125" layer="21"/>
<rectangle x1="11.38681875" y1="5.461" x2="11.98118125" y2="5.54558125" layer="21"/>
<rectangle x1="12.40281875" y1="5.461" x2="12.827" y2="5.54558125" layer="21"/>
<rectangle x1="13.25118125" y1="5.461" x2="14.94281875" y2="5.54558125" layer="21"/>
<rectangle x1="0.04318125" y1="5.54558125" x2="0.46481875" y2="5.63041875" layer="21"/>
<rectangle x1="1.82118125" y1="5.54558125" x2="2.24281875" y2="5.63041875" layer="21"/>
<rectangle x1="3.85318125" y1="5.54558125" x2="4.27481875" y2="5.63041875" layer="21"/>
<rectangle x1="5.79881875" y1="5.54558125" x2="6.223" y2="5.63041875" layer="21"/>
<rectangle x1="6.731" y1="5.54558125" x2="7.239" y2="5.63041875" layer="21"/>
<rectangle x1="7.66318125" y1="5.54558125" x2="9.271" y2="5.63041875" layer="21"/>
<rectangle x1="9.69518125" y1="5.54558125" x2="9.94918125" y2="5.63041875" layer="21"/>
<rectangle x1="11.38681875" y1="5.54558125" x2="11.98118125" y2="5.63041875" layer="21"/>
<rectangle x1="13.16481875" y1="5.54558125" x2="14.94281875" y2="5.63041875" layer="21"/>
<rectangle x1="0.04318125" y1="5.63041875" x2="0.46481875" y2="5.715" layer="21"/>
<rectangle x1="1.82118125" y1="5.63041875" x2="2.24281875" y2="5.715" layer="21"/>
<rectangle x1="3.85318125" y1="5.63041875" x2="4.27481875" y2="5.715" layer="21"/>
<rectangle x1="5.715" y1="5.63041875" x2="6.223" y2="5.715" layer="21"/>
<rectangle x1="6.731" y1="5.63041875" x2="7.239" y2="5.715" layer="21"/>
<rectangle x1="7.66318125" y1="5.63041875" x2="9.271" y2="5.715" layer="21"/>
<rectangle x1="9.60881875" y1="5.63041875" x2="9.94918125" y2="5.715" layer="21"/>
<rectangle x1="11.21918125" y1="5.63041875" x2="12.14881875" y2="5.715" layer="21"/>
<rectangle x1="13.081" y1="5.63041875" x2="14.94281875" y2="5.715" layer="21"/>
<rectangle x1="0.04318125" y1="5.715" x2="0.55118125" y2="5.79958125" layer="21"/>
<rectangle x1="1.82118125" y1="5.715" x2="2.24281875" y2="5.79958125" layer="21"/>
<rectangle x1="3.85318125" y1="5.715" x2="4.27481875" y2="5.79958125" layer="21"/>
<rectangle x1="5.715" y1="5.715" x2="6.223" y2="5.79958125" layer="21"/>
<rectangle x1="6.731" y1="5.715" x2="7.32281875" y2="5.79958125" layer="21"/>
<rectangle x1="7.66318125" y1="5.715" x2="9.271" y2="5.79958125" layer="21"/>
<rectangle x1="9.60881875" y1="5.715" x2="9.94918125" y2="5.79958125" layer="21"/>
<rectangle x1="10.20318125" y1="5.715" x2="10.45718125" y2="5.79958125" layer="21"/>
<rectangle x1="11.13281875" y1="5.715" x2="12.23518125" y2="5.79958125" layer="21"/>
<rectangle x1="12.91081875" y1="5.715" x2="14.94281875" y2="5.79958125" layer="21"/>
<rectangle x1="0.04318125" y1="5.79958125" x2="0.55118125" y2="5.88441875" layer="21"/>
<rectangle x1="1.82118125" y1="5.79958125" x2="2.24281875" y2="5.88441875" layer="21"/>
<rectangle x1="3.85318125" y1="5.79958125" x2="4.27481875" y2="5.88441875" layer="21"/>
<rectangle x1="5.63118125" y1="5.79958125" x2="6.13918125" y2="5.88441875" layer="21"/>
<rectangle x1="6.731" y1="5.79958125" x2="7.32281875" y2="5.88441875" layer="21"/>
<rectangle x1="7.747" y1="5.79958125" x2="9.18718125" y2="5.88441875" layer="21"/>
<rectangle x1="9.60881875" y1="5.79958125" x2="14.94281875" y2="5.88441875" layer="21"/>
<rectangle x1="0.04318125" y1="5.88441875" x2="0.55118125" y2="5.969" layer="21"/>
<rectangle x1="1.82118125" y1="5.88441875" x2="2.24281875" y2="5.969" layer="21"/>
<rectangle x1="3.85318125" y1="5.88441875" x2="4.27481875" y2="5.969" layer="21"/>
<rectangle x1="5.63118125" y1="5.88441875" x2="6.13918125" y2="5.969" layer="21"/>
<rectangle x1="6.731" y1="5.88441875" x2="7.40918125" y2="5.969" layer="21"/>
<rectangle x1="7.83081875" y1="5.88441875" x2="9.10081875" y2="5.969" layer="21"/>
<rectangle x1="9.525" y1="5.88441875" x2="14.94281875" y2="5.969" layer="21"/>
<rectangle x1="0.127" y1="5.969" x2="0.55118125" y2="6.05358125" layer="21"/>
<rectangle x1="1.82118125" y1="5.969" x2="2.24281875" y2="6.05358125" layer="21"/>
<rectangle x1="3.85318125" y1="5.969" x2="4.27481875" y2="6.05358125" layer="21"/>
<rectangle x1="5.54481875" y1="5.969" x2="6.05281875" y2="6.05358125" layer="21"/>
<rectangle x1="6.731" y1="5.969" x2="7.40918125" y2="6.05358125" layer="21"/>
<rectangle x1="7.91718125" y1="5.969" x2="9.017" y2="6.05358125" layer="21"/>
<rectangle x1="9.525" y1="5.969" x2="14.94281875" y2="6.05358125" layer="21"/>
<rectangle x1="0.127" y1="6.05358125" x2="0.55118125" y2="6.13841875" layer="21"/>
<rectangle x1="1.82118125" y1="6.05358125" x2="2.24281875" y2="6.13841875" layer="21"/>
<rectangle x1="3.85318125" y1="6.05358125" x2="4.27481875" y2="6.13841875" layer="21"/>
<rectangle x1="5.37718125" y1="6.05358125" x2="6.05281875" y2="6.13841875" layer="21"/>
<rectangle x1="6.731" y1="6.05358125" x2="7.493" y2="6.13841875" layer="21"/>
<rectangle x1="8.001" y1="6.05358125" x2="8.93318125" y2="6.13841875" layer="21"/>
<rectangle x1="9.44118125" y1="6.05358125" x2="14.94281875" y2="6.13841875" layer="21"/>
<rectangle x1="0.127" y1="6.13841875" x2="0.635" y2="6.223" layer="21"/>
<rectangle x1="1.82118125" y1="6.13841875" x2="2.24281875" y2="6.223" layer="21"/>
<rectangle x1="3.85318125" y1="6.13841875" x2="4.27481875" y2="6.223" layer="21"/>
<rectangle x1="5.207" y1="6.13841875" x2="5.969" y2="6.223" layer="21"/>
<rectangle x1="6.81481875" y1="6.13841875" x2="7.493" y2="6.223" layer="21"/>
<rectangle x1="8.17118125" y1="6.13841875" x2="8.763" y2="6.223" layer="21"/>
<rectangle x1="9.35481875" y1="6.13841875" x2="14.94281875" y2="6.223" layer="21"/>
<rectangle x1="0.127" y1="6.223" x2="0.635" y2="6.30758125" layer="21"/>
<rectangle x1="1.82118125" y1="6.223" x2="2.24281875" y2="6.30758125" layer="21"/>
<rectangle x1="3.85318125" y1="6.223" x2="5.88518125" y2="6.30758125" layer="21"/>
<rectangle x1="6.81481875" y1="6.223" x2="7.57681875" y2="6.30758125" layer="21"/>
<rectangle x1="9.35481875" y1="6.223" x2="14.859" y2="6.30758125" layer="21"/>
<rectangle x1="0.21081875" y1="6.30758125" x2="0.635" y2="6.39241875" layer="21"/>
<rectangle x1="1.82118125" y1="6.30758125" x2="2.24281875" y2="6.39241875" layer="21"/>
<rectangle x1="3.85318125" y1="6.30758125" x2="5.79881875" y2="6.39241875" layer="21"/>
<rectangle x1="6.81481875" y1="6.30758125" x2="7.66318125" y2="6.39241875" layer="21"/>
<rectangle x1="9.18718125" y1="6.30758125" x2="14.859" y2="6.39241875" layer="21"/>
<rectangle x1="0.21081875" y1="6.39241875" x2="0.71881875" y2="6.477" layer="21"/>
<rectangle x1="1.82118125" y1="6.39241875" x2="2.24281875" y2="6.477" layer="21"/>
<rectangle x1="3.85318125" y1="6.39241875" x2="5.715" y2="6.477" layer="21"/>
<rectangle x1="6.81481875" y1="6.39241875" x2="7.83081875" y2="6.477" layer="21"/>
<rectangle x1="9.10081875" y1="6.39241875" x2="14.859" y2="6.477" layer="21"/>
<rectangle x1="0.21081875" y1="6.477" x2="0.71881875" y2="6.56158125" layer="21"/>
<rectangle x1="1.82118125" y1="6.477" x2="2.24281875" y2="6.56158125" layer="21"/>
<rectangle x1="3.85318125" y1="6.477" x2="5.54481875" y2="6.56158125" layer="21"/>
<rectangle x1="6.81481875" y1="6.477" x2="8.001" y2="6.56158125" layer="21"/>
<rectangle x1="8.93318125" y1="6.477" x2="14.77518125" y2="6.56158125" layer="21"/>
<rectangle x1="0.29718125" y1="6.56158125" x2="0.80518125" y2="6.64641875" layer="21"/>
<rectangle x1="1.905" y1="6.56158125" x2="2.159" y2="6.64641875" layer="21"/>
<rectangle x1="3.937" y1="6.56158125" x2="5.37718125" y2="6.64641875" layer="21"/>
<rectangle x1="6.81481875" y1="6.56158125" x2="8.33881875" y2="6.64641875" layer="21"/>
<rectangle x1="8.59281875" y1="6.56158125" x2="14.77518125" y2="6.64641875" layer="21"/>
<rectangle x1="0.29718125" y1="6.64641875" x2="0.80518125" y2="6.731" layer="21"/>
<rectangle x1="6.90118125" y1="6.64641875" x2="14.77518125" y2="6.731" layer="21"/>
<rectangle x1="0.381" y1="6.731" x2="0.889" y2="6.81558125" layer="21"/>
<rectangle x1="6.90118125" y1="6.731" x2="14.68881875" y2="6.81558125" layer="21"/>
<rectangle x1="0.381" y1="6.81558125" x2="0.97281875" y2="6.90041875" layer="21"/>
<rectangle x1="6.90118125" y1="6.81558125" x2="14.605" y2="6.90041875" layer="21"/>
<rectangle x1="0.46481875" y1="6.90041875" x2="0.97281875" y2="6.985" layer="21"/>
<rectangle x1="6.90118125" y1="6.90041875" x2="14.605" y2="6.985" layer="21"/>
<rectangle x1="0.55118125" y1="6.985" x2="1.05918125" y2="7.06958125" layer="21"/>
<rectangle x1="6.985" y1="6.985" x2="14.52118125" y2="7.06958125" layer="21"/>
<rectangle x1="0.55118125" y1="7.06958125" x2="1.143" y2="7.15441875" layer="21"/>
<rectangle x1="6.985" y1="7.06958125" x2="14.52118125" y2="7.15441875" layer="21"/>
<rectangle x1="0.635" y1="7.15441875" x2="1.22681875" y2="7.239" layer="21"/>
<rectangle x1="6.985" y1="7.15441875" x2="14.43481875" y2="7.239" layer="21"/>
<rectangle x1="0.71881875" y1="7.239" x2="1.31318125" y2="7.32358125" layer="21"/>
<rectangle x1="6.985" y1="7.239" x2="14.351" y2="7.32358125" layer="21"/>
<rectangle x1="0.80518125" y1="7.32358125" x2="1.397" y2="7.40841875" layer="21"/>
<rectangle x1="7.06881875" y1="7.32358125" x2="14.26718125" y2="7.40841875" layer="21"/>
<rectangle x1="0.889" y1="7.40841875" x2="1.48081875" y2="7.493" layer="21"/>
<rectangle x1="7.06881875" y1="7.40841875" x2="14.18081875" y2="7.493" layer="21"/>
<rectangle x1="0.889" y1="7.493" x2="1.651" y2="7.57758125" layer="21"/>
<rectangle x1="7.15518125" y1="7.493" x2="14.097" y2="7.57758125" layer="21"/>
<rectangle x1="1.05918125" y1="7.57758125" x2="1.73481875" y2="7.66241875" layer="21"/>
<rectangle x1="7.15518125" y1="7.57758125" x2="14.01318125" y2="7.66241875" layer="21"/>
<rectangle x1="1.143" y1="7.66241875" x2="1.905" y2="7.747" layer="21"/>
<rectangle x1="7.239" y1="7.66241875" x2="13.92681875" y2="7.747" layer="21"/>
<rectangle x1="1.22681875" y1="7.747" x2="2.159" y2="7.83158125" layer="21"/>
<rectangle x1="7.239" y1="7.747" x2="13.843" y2="7.83158125" layer="21"/>
<rectangle x1="1.31318125" y1="7.83158125" x2="2.413" y2="7.91641875" layer="21"/>
<rectangle x1="7.32281875" y1="7.83158125" x2="13.67281875" y2="7.91641875" layer="21"/>
<rectangle x1="1.48081875" y1="7.91641875" x2="7.239" y2="8.001" layer="21"/>
<rectangle x1="7.32281875" y1="7.91641875" x2="13.589" y2="8.001" layer="21"/>
<rectangle x1="1.651" y1="8.001" x2="13.41881875" y2="8.08558125" layer="21"/>
<rectangle x1="1.82118125" y1="8.08558125" x2="13.25118125" y2="8.17041875" layer="21"/>
<rectangle x1="1.98881875" y1="8.17041875" x2="13.081" y2="8.255" layer="21"/>
<rectangle x1="2.24281875" y1="8.255" x2="12.827" y2="8.33958125" layer="21"/>
</package>
<package name="UDO-LOGO-20MM">
<rectangle x1="3.00481875" y1="0.29641875" x2="3.429" y2="0.381" layer="21"/>
<rectangle x1="14.351" y1="0.29641875" x2="14.68881875" y2="0.381" layer="21"/>
<rectangle x1="2.75081875" y1="0.381" x2="3.683" y2="0.46558125" layer="21"/>
<rectangle x1="4.445" y1="0.381" x2="4.52881875" y2="0.46558125" layer="21"/>
<rectangle x1="5.54481875" y1="0.381" x2="5.715" y2="0.46558125" layer="21"/>
<rectangle x1="5.969" y1="0.381" x2="6.05281875" y2="0.46558125" layer="21"/>
<rectangle x1="6.985" y1="0.381" x2="7.15518125" y2="0.46558125" layer="21"/>
<rectangle x1="7.57681875" y1="0.381" x2="8.42518125" y2="0.46558125" layer="21"/>
<rectangle x1="9.94918125" y1="0.381" x2="10.20318125" y2="0.46558125" layer="21"/>
<rectangle x1="11.21918125" y1="0.381" x2="11.38681875" y2="0.46558125" layer="21"/>
<rectangle x1="12.40281875" y1="0.381" x2="12.74318125" y2="0.46558125" layer="21"/>
<rectangle x1="14.097" y1="0.381" x2="14.94281875" y2="0.46558125" layer="21"/>
<rectangle x1="15.70481875" y1="0.381" x2="15.79118125" y2="0.46558125" layer="21"/>
<rectangle x1="16.80718125" y1="0.381" x2="16.891" y2="0.46558125" layer="21"/>
<rectangle x1="17.653" y1="0.381" x2="17.907" y2="0.46558125" layer="21"/>
<rectangle x1="2.667" y1="0.46558125" x2="3.85318125" y2="0.55041875" layer="21"/>
<rectangle x1="4.36118125" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="21"/>
<rectangle x1="5.54481875" y1="0.46558125" x2="5.715" y2="0.55041875" layer="21"/>
<rectangle x1="5.88518125" y1="0.46558125" x2="6.13918125" y2="0.55041875" layer="21"/>
<rectangle x1="6.731" y1="0.46558125" x2="7.239" y2="0.55041875" layer="21"/>
<rectangle x1="7.493" y1="0.46558125" x2="8.67918125" y2="0.55041875" layer="21"/>
<rectangle x1="9.69518125" y1="0.46558125" x2="10.37081875" y2="0.55041875" layer="21"/>
<rectangle x1="11.049" y1="0.46558125" x2="11.64081875" y2="0.55041875" layer="21"/>
<rectangle x1="12.23518125" y1="0.46558125" x2="12.91081875" y2="0.55041875" layer="21"/>
<rectangle x1="13.92681875" y1="0.46558125" x2="15.113" y2="0.55041875" layer="21"/>
<rectangle x1="15.621" y1="0.46558125" x2="15.875" y2="0.55041875" layer="21"/>
<rectangle x1="16.72081875" y1="0.46558125" x2="16.97481875" y2="0.55041875" layer="21"/>
<rectangle x1="17.399" y1="0.46558125" x2="18.07718125" y2="0.55041875" layer="21"/>
<rectangle x1="2.58318125" y1="0.55041875" x2="3.00481875" y2="0.635" layer="21"/>
<rectangle x1="3.51281875" y1="0.55041875" x2="3.937" y2="0.635" layer="21"/>
<rectangle x1="4.36118125" y1="0.55041875" x2="4.61518125" y2="0.635" layer="21"/>
<rectangle x1="5.54481875" y1="0.55041875" x2="5.715" y2="0.635" layer="21"/>
<rectangle x1="5.88518125" y1="0.55041875" x2="6.13918125" y2="0.635" layer="21"/>
<rectangle x1="6.64718125" y1="0.55041875" x2="7.239" y2="0.635" layer="21"/>
<rectangle x1="7.493" y1="0.55041875" x2="8.84681875" y2="0.635" layer="21"/>
<rectangle x1="9.60881875" y1="0.55041875" x2="10.45718125" y2="0.635" layer="21"/>
<rectangle x1="10.96518125" y1="0.55041875" x2="11.72718125" y2="0.635" layer="21"/>
<rectangle x1="12.14881875" y1="0.55041875" x2="12.99718125" y2="0.635" layer="21"/>
<rectangle x1="13.843" y1="0.55041875" x2="14.26718125" y2="0.635" layer="21"/>
<rectangle x1="14.77518125" y1="0.55041875" x2="15.19681875" y2="0.635" layer="21"/>
<rectangle x1="15.621" y1="0.55041875" x2="15.875" y2="0.635" layer="21"/>
<rectangle x1="16.72081875" y1="0.55041875" x2="16.97481875" y2="0.635" layer="21"/>
<rectangle x1="17.31518125" y1="0.55041875" x2="18.24481875" y2="0.635" layer="21"/>
<rectangle x1="2.49681875" y1="0.635" x2="2.83718125" y2="0.71958125" layer="21"/>
<rectangle x1="3.683" y1="0.635" x2="4.02081875" y2="0.71958125" layer="21"/>
<rectangle x1="4.36118125" y1="0.635" x2="4.61518125" y2="0.71958125" layer="21"/>
<rectangle x1="5.54481875" y1="0.635" x2="5.715" y2="0.71958125" layer="21"/>
<rectangle x1="5.88518125" y1="0.635" x2="6.13918125" y2="0.71958125" layer="21"/>
<rectangle x1="6.56081875" y1="0.635" x2="6.90118125" y2="0.71958125" layer="21"/>
<rectangle x1="7.493" y1="0.635" x2="7.747" y2="0.71958125" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="8.93318125" y2="0.71958125" layer="21"/>
<rectangle x1="9.525" y1="0.635" x2="9.86281875" y2="0.71958125" layer="21"/>
<rectangle x1="10.20318125" y1="0.635" x2="10.541" y2="0.71958125" layer="21"/>
<rectangle x1="10.87881875" y1="0.635" x2="11.13281875" y2="0.71958125" layer="21"/>
<rectangle x1="11.47318125" y1="0.635" x2="11.811" y2="0.71958125" layer="21"/>
<rectangle x1="12.065" y1="0.635" x2="12.40281875" y2="0.71958125" layer="21"/>
<rectangle x1="12.74318125" y1="0.635" x2="13.081" y2="0.71958125" layer="21"/>
<rectangle x1="13.75918125" y1="0.635" x2="14.097" y2="0.71958125" layer="21"/>
<rectangle x1="14.94281875" y1="0.635" x2="15.28318125" y2="0.71958125" layer="21"/>
<rectangle x1="15.621" y1="0.635" x2="15.875" y2="0.71958125" layer="21"/>
<rectangle x1="16.72081875" y1="0.635" x2="16.97481875" y2="0.71958125" layer="21"/>
<rectangle x1="17.22881875" y1="0.635" x2="17.56918125" y2="0.71958125" layer="21"/>
<rectangle x1="17.907" y1="0.635" x2="18.33118125" y2="0.71958125" layer="21"/>
<rectangle x1="2.413" y1="0.71958125" x2="2.667" y2="0.80441875" layer="21"/>
<rectangle x1="3.76681875" y1="0.71958125" x2="4.02081875" y2="0.80441875" layer="21"/>
<rectangle x1="4.36118125" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="21"/>
<rectangle x1="5.54481875" y1="0.71958125" x2="5.715" y2="0.80441875" layer="21"/>
<rectangle x1="5.88518125" y1="0.71958125" x2="6.13918125" y2="0.80441875" layer="21"/>
<rectangle x1="6.56081875" y1="0.71958125" x2="6.81481875" y2="0.80441875" layer="21"/>
<rectangle x1="7.493" y1="0.71958125" x2="7.747" y2="0.80441875" layer="21"/>
<rectangle x1="8.67918125" y1="0.71958125" x2="9.017" y2="0.80441875" layer="21"/>
<rectangle x1="9.44118125" y1="0.71958125" x2="9.69518125" y2="0.80441875" layer="21"/>
<rectangle x1="10.37081875" y1="0.71958125" x2="10.62481875" y2="0.80441875" layer="21"/>
<rectangle x1="10.87881875" y1="0.71958125" x2="11.049" y2="0.80441875" layer="21"/>
<rectangle x1="11.557" y1="0.71958125" x2="11.811" y2="0.80441875" layer="21"/>
<rectangle x1="11.98118125" y1="0.71958125" x2="12.23518125" y2="0.80441875" layer="21"/>
<rectangle x1="12.91081875" y1="0.71958125" x2="13.16481875" y2="0.80441875" layer="21"/>
<rectangle x1="13.67281875" y1="0.71958125" x2="14.01318125" y2="0.80441875" layer="21"/>
<rectangle x1="15.02918125" y1="0.71958125" x2="15.367" y2="0.80441875" layer="21"/>
<rectangle x1="15.621" y1="0.71958125" x2="15.875" y2="0.80441875" layer="21"/>
<rectangle x1="16.72081875" y1="0.71958125" x2="16.97481875" y2="0.80441875" layer="21"/>
<rectangle x1="17.145" y1="0.71958125" x2="17.399" y2="0.80441875" layer="21"/>
<rectangle x1="18.07718125" y1="0.71958125" x2="18.33118125" y2="0.80441875" layer="21"/>
<rectangle x1="2.32918125" y1="0.80441875" x2="2.58318125" y2="0.889" layer="21"/>
<rectangle x1="3.85318125" y1="0.80441875" x2="4.10718125" y2="0.889" layer="21"/>
<rectangle x1="4.36118125" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="5.54481875" y1="0.80441875" x2="5.715" y2="0.889" layer="21"/>
<rectangle x1="5.88518125" y1="0.80441875" x2="6.13918125" y2="0.889" layer="21"/>
<rectangle x1="6.477" y1="0.80441875" x2="6.731" y2="0.889" layer="21"/>
<rectangle x1="7.493" y1="0.80441875" x2="7.747" y2="0.889" layer="21"/>
<rectangle x1="8.763" y1="0.80441875" x2="9.10081875" y2="0.889" layer="21"/>
<rectangle x1="9.44118125" y1="0.80441875" x2="9.69518125" y2="0.889" layer="21"/>
<rectangle x1="10.45718125" y1="0.80441875" x2="10.71118125" y2="0.889" layer="21"/>
<rectangle x1="11.557" y1="0.80441875" x2="11.811" y2="0.889" layer="21"/>
<rectangle x1="11.98118125" y1="0.80441875" x2="12.14881875" y2="0.889" layer="21"/>
<rectangle x1="12.99718125" y1="0.80441875" x2="13.16481875" y2="0.889" layer="21"/>
<rectangle x1="13.67281875" y1="0.80441875" x2="13.92681875" y2="0.889" layer="21"/>
<rectangle x1="15.113" y1="0.80441875" x2="15.367" y2="0.889" layer="21"/>
<rectangle x1="15.621" y1="0.80441875" x2="15.875" y2="0.889" layer="21"/>
<rectangle x1="16.72081875" y1="0.80441875" x2="16.97481875" y2="0.889" layer="21"/>
<rectangle x1="17.145" y1="0.80441875" x2="17.399" y2="0.889" layer="21"/>
<rectangle x1="18.161" y1="0.80441875" x2="18.33118125" y2="0.889" layer="21"/>
<rectangle x1="2.32918125" y1="0.889" x2="2.58318125" y2="0.97358125" layer="21"/>
<rectangle x1="3.937" y1="0.889" x2="4.191" y2="0.97358125" layer="21"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.61518125" y2="0.97358125" layer="21"/>
<rectangle x1="5.54481875" y1="0.889" x2="5.715" y2="0.97358125" layer="21"/>
<rectangle x1="5.88518125" y1="0.889" x2="6.13918125" y2="0.97358125" layer="21"/>
<rectangle x1="6.477" y1="0.889" x2="6.731" y2="0.97358125" layer="21"/>
<rectangle x1="7.493" y1="0.889" x2="7.747" y2="0.97358125" layer="21"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.10081875" y2="0.97358125" layer="21"/>
<rectangle x1="9.35481875" y1="0.889" x2="9.60881875" y2="0.97358125" layer="21"/>
<rectangle x1="10.45718125" y1="0.889" x2="10.71118125" y2="0.97358125" layer="21"/>
<rectangle x1="11.47318125" y1="0.889" x2="11.72718125" y2="0.97358125" layer="21"/>
<rectangle x1="11.89481875" y1="0.889" x2="12.14881875" y2="0.97358125" layer="21"/>
<rectangle x1="13.589" y1="0.889" x2="13.843" y2="0.97358125" layer="21"/>
<rectangle x1="15.19681875" y1="0.889" x2="15.45081875" y2="0.97358125" layer="21"/>
<rectangle x1="15.621" y1="0.889" x2="15.875" y2="0.97358125" layer="21"/>
<rectangle x1="16.72081875" y1="0.889" x2="16.97481875" y2="0.97358125" layer="21"/>
<rectangle x1="17.06118125" y1="0.889" x2="17.31518125" y2="0.97358125" layer="21"/>
<rectangle x1="2.32918125" y1="0.97358125" x2="2.49681875" y2="1.05841875" layer="21"/>
<rectangle x1="3.937" y1="0.97358125" x2="4.191" y2="1.05841875" layer="21"/>
<rectangle x1="4.36118125" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="21"/>
<rectangle x1="5.54481875" y1="0.97358125" x2="5.715" y2="1.05841875" layer="21"/>
<rectangle x1="5.88518125" y1="0.97358125" x2="6.13918125" y2="1.05841875" layer="21"/>
<rectangle x1="6.477" y1="0.97358125" x2="6.64718125" y2="1.05841875" layer="21"/>
<rectangle x1="7.493" y1="0.97358125" x2="7.747" y2="1.05841875" layer="21"/>
<rectangle x1="8.93318125" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="21"/>
<rectangle x1="9.35481875" y1="0.97358125" x2="9.60881875" y2="1.05841875" layer="21"/>
<rectangle x1="10.45718125" y1="0.97358125" x2="10.71118125" y2="1.05841875" layer="21"/>
<rectangle x1="11.21918125" y1="0.97358125" x2="11.72718125" y2="1.05841875" layer="21"/>
<rectangle x1="11.89481875" y1="0.97358125" x2="13.25118125" y2="1.05841875" layer="21"/>
<rectangle x1="13.589" y1="0.97358125" x2="13.843" y2="1.05841875" layer="21"/>
<rectangle x1="15.19681875" y1="0.97358125" x2="15.45081875" y2="1.05841875" layer="21"/>
<rectangle x1="15.621" y1="0.97358125" x2="15.875" y2="1.05841875" layer="21"/>
<rectangle x1="16.72081875" y1="0.97358125" x2="16.97481875" y2="1.05841875" layer="21"/>
<rectangle x1="17.06118125" y1="0.97358125" x2="18.415" y2="1.05841875" layer="21"/>
<rectangle x1="2.24281875" y1="1.05841875" x2="2.49681875" y2="1.143" layer="21"/>
<rectangle x1="3.937" y1="1.05841875" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="4.36118125" y1="1.05841875" x2="4.61518125" y2="1.143" layer="21"/>
<rectangle x1="5.54481875" y1="1.05841875" x2="5.715" y2="1.143" layer="21"/>
<rectangle x1="5.88518125" y1="1.05841875" x2="6.13918125" y2="1.143" layer="21"/>
<rectangle x1="6.477" y1="1.05841875" x2="6.64718125" y2="1.143" layer="21"/>
<rectangle x1="7.493" y1="1.05841875" x2="7.747" y2="1.143" layer="21"/>
<rectangle x1="8.93318125" y1="1.05841875" x2="9.18718125" y2="1.143" layer="21"/>
<rectangle x1="9.35481875" y1="1.05841875" x2="9.60881875" y2="1.143" layer="21"/>
<rectangle x1="10.45718125" y1="1.05841875" x2="10.71118125" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="1.05841875" x2="11.557" y2="1.143" layer="21"/>
<rectangle x1="11.89481875" y1="1.05841875" x2="13.25118125" y2="1.143" layer="21"/>
<rectangle x1="13.589" y1="1.05841875" x2="13.75918125" y2="1.143" layer="21"/>
<rectangle x1="15.28318125" y1="1.05841875" x2="15.53718125" y2="1.143" layer="21"/>
<rectangle x1="15.621" y1="1.05841875" x2="15.875" y2="1.143" layer="21"/>
<rectangle x1="16.72081875" y1="1.05841875" x2="16.97481875" y2="1.143" layer="21"/>
<rectangle x1="17.06118125" y1="1.05841875" x2="18.415" y2="1.143" layer="21"/>
<rectangle x1="2.24281875" y1="1.143" x2="2.49681875" y2="1.22758125" layer="21"/>
<rectangle x1="4.02081875" y1="1.143" x2="4.191" y2="1.22758125" layer="21"/>
<rectangle x1="4.36118125" y1="1.143" x2="4.61518125" y2="1.22758125" layer="21"/>
<rectangle x1="5.54481875" y1="1.143" x2="5.715" y2="1.22758125" layer="21"/>
<rectangle x1="5.88518125" y1="1.143" x2="6.13918125" y2="1.22758125" layer="21"/>
<rectangle x1="6.477" y1="1.143" x2="6.64718125" y2="1.22758125" layer="21"/>
<rectangle x1="7.493" y1="1.143" x2="7.747" y2="1.22758125" layer="21"/>
<rectangle x1="9.017" y1="1.143" x2="9.271" y2="1.22758125" layer="21"/>
<rectangle x1="9.35481875" y1="1.143" x2="9.60881875" y2="1.22758125" layer="21"/>
<rectangle x1="10.45718125" y1="1.143" x2="10.71118125" y2="1.22758125" layer="21"/>
<rectangle x1="10.87881875" y1="1.143" x2="11.38681875" y2="1.22758125" layer="21"/>
<rectangle x1="11.89481875" y1="1.143" x2="13.25118125" y2="1.22758125" layer="21"/>
<rectangle x1="13.50518125" y1="1.143" x2="13.75918125" y2="1.22758125" layer="21"/>
<rectangle x1="15.28318125" y1="1.143" x2="15.53718125" y2="1.22758125" layer="21"/>
<rectangle x1="15.621" y1="1.143" x2="15.875" y2="1.22758125" layer="21"/>
<rectangle x1="16.72081875" y1="1.143" x2="16.97481875" y2="1.22758125" layer="21"/>
<rectangle x1="17.06118125" y1="1.143" x2="18.415" y2="1.22758125" layer="21"/>
<rectangle x1="2.24281875" y1="1.22758125" x2="2.49681875" y2="1.31241875" layer="21"/>
<rectangle x1="4.02081875" y1="1.22758125" x2="4.191" y2="1.31241875" layer="21"/>
<rectangle x1="4.36118125" y1="1.22758125" x2="4.61518125" y2="1.31241875" layer="21"/>
<rectangle x1="5.461" y1="1.22758125" x2="5.715" y2="1.31241875" layer="21"/>
<rectangle x1="5.88518125" y1="1.22758125" x2="6.13918125" y2="1.31241875" layer="21"/>
<rectangle x1="6.477" y1="1.22758125" x2="6.64718125" y2="1.31241875" layer="21"/>
<rectangle x1="7.493" y1="1.22758125" x2="7.747" y2="1.31241875" layer="21"/>
<rectangle x1="9.017" y1="1.22758125" x2="9.271" y2="1.31241875" layer="21"/>
<rectangle x1="9.35481875" y1="1.22758125" x2="9.60881875" y2="1.31241875" layer="21"/>
<rectangle x1="10.45718125" y1="1.22758125" x2="10.71118125" y2="1.31241875" layer="21"/>
<rectangle x1="10.795" y1="1.22758125" x2="11.21918125" y2="1.31241875" layer="21"/>
<rectangle x1="11.89481875" y1="1.22758125" x2="12.14881875" y2="1.31241875" layer="21"/>
<rectangle x1="12.99718125" y1="1.22758125" x2="13.25118125" y2="1.31241875" layer="21"/>
<rectangle x1="13.50518125" y1="1.22758125" x2="13.75918125" y2="1.31241875" layer="21"/>
<rectangle x1="15.28318125" y1="1.22758125" x2="15.53718125" y2="1.31241875" layer="21"/>
<rectangle x1="15.621" y1="1.22758125" x2="15.875" y2="1.31241875" layer="21"/>
<rectangle x1="16.72081875" y1="1.22758125" x2="16.97481875" y2="1.31241875" layer="21"/>
<rectangle x1="17.06118125" y1="1.22758125" x2="17.31518125" y2="1.31241875" layer="21"/>
<rectangle x1="18.161" y1="1.22758125" x2="18.415" y2="1.31241875" layer="21"/>
<rectangle x1="2.24281875" y1="1.31241875" x2="2.49681875" y2="1.397" layer="21"/>
<rectangle x1="4.02081875" y1="1.31241875" x2="4.191" y2="1.397" layer="21"/>
<rectangle x1="4.36118125" y1="1.31241875" x2="4.699" y2="1.397" layer="21"/>
<rectangle x1="5.461" y1="1.31241875" x2="5.715" y2="1.397" layer="21"/>
<rectangle x1="5.88518125" y1="1.31241875" x2="6.13918125" y2="1.397" layer="21"/>
<rectangle x1="6.477" y1="1.31241875" x2="6.64718125" y2="1.397" layer="21"/>
<rectangle x1="7.493" y1="1.31241875" x2="7.747" y2="1.397" layer="21"/>
<rectangle x1="9.017" y1="1.31241875" x2="9.271" y2="1.397" layer="21"/>
<rectangle x1="9.44118125" y1="1.31241875" x2="9.69518125" y2="1.397" layer="21"/>
<rectangle x1="10.45718125" y1="1.31241875" x2="10.62481875" y2="1.397" layer="21"/>
<rectangle x1="10.795" y1="1.31241875" x2="11.049" y2="1.397" layer="21"/>
<rectangle x1="11.64081875" y1="1.31241875" x2="11.72718125" y2="1.397" layer="21"/>
<rectangle x1="11.98118125" y1="1.31241875" x2="12.23518125" y2="1.397" layer="21"/>
<rectangle x1="12.99718125" y1="1.31241875" x2="13.16481875" y2="1.397" layer="21"/>
<rectangle x1="13.50518125" y1="1.31241875" x2="13.75918125" y2="1.397" layer="21"/>
<rectangle x1="15.28318125" y1="1.31241875" x2="15.53718125" y2="1.397" layer="21"/>
<rectangle x1="15.621" y1="1.31241875" x2="15.875" y2="1.397" layer="21"/>
<rectangle x1="16.637" y1="1.31241875" x2="16.891" y2="1.397" layer="21"/>
<rectangle x1="17.145" y1="1.31241875" x2="17.399" y2="1.397" layer="21"/>
<rectangle x1="18.161" y1="1.31241875" x2="18.415" y2="1.397" layer="21"/>
<rectangle x1="2.24281875" y1="1.397" x2="2.49681875" y2="1.48158125" layer="21"/>
<rectangle x1="4.02081875" y1="1.397" x2="4.191" y2="1.48158125" layer="21"/>
<rectangle x1="4.36118125" y1="1.397" x2="4.78281875" y2="1.48158125" layer="21"/>
<rectangle x1="5.37718125" y1="1.397" x2="5.63118125" y2="1.48158125" layer="21"/>
<rectangle x1="5.88518125" y1="1.397" x2="6.13918125" y2="1.48158125" layer="21"/>
<rectangle x1="6.477" y1="1.397" x2="6.64718125" y2="1.48158125" layer="21"/>
<rectangle x1="7.493" y1="1.397" x2="7.747" y2="1.48158125" layer="21"/>
<rectangle x1="9.017" y1="1.397" x2="9.271" y2="1.48158125" layer="21"/>
<rectangle x1="9.44118125" y1="1.397" x2="9.69518125" y2="1.48158125" layer="21"/>
<rectangle x1="10.37081875" y1="1.397" x2="10.62481875" y2="1.48158125" layer="21"/>
<rectangle x1="10.795" y1="1.397" x2="11.049" y2="1.48158125" layer="21"/>
<rectangle x1="11.557" y1="1.397" x2="11.811" y2="1.48158125" layer="21"/>
<rectangle x1="11.98118125" y1="1.397" x2="12.319" y2="1.48158125" layer="21"/>
<rectangle x1="12.91081875" y1="1.397" x2="13.16481875" y2="1.48158125" layer="21"/>
<rectangle x1="13.50518125" y1="1.397" x2="13.75918125" y2="1.48158125" layer="21"/>
<rectangle x1="15.28318125" y1="1.397" x2="15.53718125" y2="1.48158125" layer="21"/>
<rectangle x1="15.621" y1="1.397" x2="15.95881875" y2="1.48158125" layer="21"/>
<rectangle x1="16.637" y1="1.397" x2="16.891" y2="1.48158125" layer="21"/>
<rectangle x1="17.145" y1="1.397" x2="17.48281875" y2="1.48158125" layer="21"/>
<rectangle x1="18.07718125" y1="1.397" x2="18.33118125" y2="1.48158125" layer="21"/>
<rectangle x1="2.24281875" y1="1.48158125" x2="2.49681875" y2="1.56641875" layer="21"/>
<rectangle x1="4.02081875" y1="1.48158125" x2="4.191" y2="1.56641875" layer="21"/>
<rectangle x1="4.36118125" y1="1.48158125" x2="4.953" y2="1.56641875" layer="21"/>
<rectangle x1="5.207" y1="1.48158125" x2="5.63118125" y2="1.56641875" layer="21"/>
<rectangle x1="5.88518125" y1="1.48158125" x2="6.13918125" y2="1.56641875" layer="21"/>
<rectangle x1="6.477" y1="1.48158125" x2="6.64718125" y2="1.56641875" layer="21"/>
<rectangle x1="7.493" y1="1.48158125" x2="7.747" y2="1.56641875" layer="21"/>
<rectangle x1="9.017" y1="1.48158125" x2="9.271" y2="1.56641875" layer="21"/>
<rectangle x1="9.525" y1="1.48158125" x2="9.86281875" y2="1.56641875" layer="21"/>
<rectangle x1="10.20318125" y1="1.48158125" x2="10.541" y2="1.56641875" layer="21"/>
<rectangle x1="10.87881875" y1="1.48158125" x2="11.21918125" y2="1.56641875" layer="21"/>
<rectangle x1="11.47318125" y1="1.48158125" x2="11.72718125" y2="1.56641875" layer="21"/>
<rectangle x1="12.065" y1="1.48158125" x2="12.40281875" y2="1.56641875" layer="21"/>
<rectangle x1="12.74318125" y1="1.48158125" x2="13.081" y2="1.56641875" layer="21"/>
<rectangle x1="13.589" y1="1.48158125" x2="13.75918125" y2="1.56641875" layer="21"/>
<rectangle x1="15.28318125" y1="1.48158125" x2="15.53718125" y2="1.56641875" layer="21"/>
<rectangle x1="15.621" y1="1.48158125" x2="16.129" y2="1.56641875" layer="21"/>
<rectangle x1="16.46681875" y1="1.48158125" x2="16.80718125" y2="1.56641875" layer="21"/>
<rectangle x1="17.22881875" y1="1.48158125" x2="17.653" y2="1.56641875" layer="21"/>
<rectangle x1="17.907" y1="1.48158125" x2="18.24481875" y2="1.56641875" layer="21"/>
<rectangle x1="2.24281875" y1="1.56641875" x2="2.49681875" y2="1.651" layer="21"/>
<rectangle x1="4.02081875" y1="1.56641875" x2="4.191" y2="1.651" layer="21"/>
<rectangle x1="4.36118125" y1="1.56641875" x2="5.54481875" y2="1.651" layer="21"/>
<rectangle x1="5.88518125" y1="1.56641875" x2="6.13918125" y2="1.651" layer="21"/>
<rectangle x1="6.223" y1="1.56641875" x2="7.239" y2="1.651" layer="21"/>
<rectangle x1="7.493" y1="1.56641875" x2="7.747" y2="1.651" layer="21"/>
<rectangle x1="9.017" y1="1.56641875" x2="9.271" y2="1.651" layer="21"/>
<rectangle x1="9.60881875" y1="1.56641875" x2="10.45718125" y2="1.651" layer="21"/>
<rectangle x1="10.87881875" y1="1.56641875" x2="11.72718125" y2="1.651" layer="21"/>
<rectangle x1="12.14881875" y1="1.56641875" x2="12.99718125" y2="1.651" layer="21"/>
<rectangle x1="13.589" y1="1.56641875" x2="13.843" y2="1.651" layer="21"/>
<rectangle x1="15.19681875" y1="1.56641875" x2="15.45081875" y2="1.651" layer="21"/>
<rectangle x1="15.621" y1="1.56641875" x2="16.72081875" y2="1.651" layer="21"/>
<rectangle x1="17.31518125" y1="1.56641875" x2="18.161" y2="1.651" layer="21"/>
<rectangle x1="2.24281875" y1="1.651" x2="2.49681875" y2="1.73558125" layer="21"/>
<rectangle x1="4.02081875" y1="1.651" x2="4.191" y2="1.73558125" layer="21"/>
<rectangle x1="4.36118125" y1="1.651" x2="4.61518125" y2="1.73558125" layer="21"/>
<rectangle x1="4.78281875" y1="1.651" x2="5.37718125" y2="1.73558125" layer="21"/>
<rectangle x1="5.88518125" y1="1.651" x2="6.13918125" y2="1.73558125" layer="21"/>
<rectangle x1="6.223" y1="1.651" x2="7.239" y2="1.73558125" layer="21"/>
<rectangle x1="7.493" y1="1.651" x2="7.747" y2="1.73558125" layer="21"/>
<rectangle x1="9.017" y1="1.651" x2="9.18718125" y2="1.73558125" layer="21"/>
<rectangle x1="9.69518125" y1="1.651" x2="10.37081875" y2="1.73558125" layer="21"/>
<rectangle x1="11.049" y1="1.651" x2="11.557" y2="1.73558125" layer="21"/>
<rectangle x1="12.23518125" y1="1.651" x2="12.91081875" y2="1.73558125" layer="21"/>
<rectangle x1="13.589" y1="1.651" x2="13.843" y2="1.73558125" layer="21"/>
<rectangle x1="15.19681875" y1="1.651" x2="15.45081875" y2="1.73558125" layer="21"/>
<rectangle x1="15.621" y1="1.651" x2="15.79118125" y2="1.73558125" layer="21"/>
<rectangle x1="15.95881875" y1="1.651" x2="16.637" y2="1.73558125" layer="21"/>
<rectangle x1="17.399" y1="1.651" x2="18.07718125" y2="1.73558125" layer="21"/>
<rectangle x1="2.24281875" y1="1.73558125" x2="2.49681875" y2="1.82041875" layer="21"/>
<rectangle x1="4.02081875" y1="1.73558125" x2="4.191" y2="1.82041875" layer="21"/>
<rectangle x1="4.445" y1="1.73558125" x2="4.52881875" y2="1.82041875" layer="21"/>
<rectangle x1="4.953" y1="1.73558125" x2="5.12318125" y2="1.82041875" layer="21"/>
<rectangle x1="5.969" y1="1.73558125" x2="6.05281875" y2="1.82041875" layer="21"/>
<rectangle x1="6.30681875" y1="1.73558125" x2="7.15518125" y2="1.82041875" layer="21"/>
<rectangle x1="7.493" y1="1.73558125" x2="7.747" y2="1.82041875" layer="21"/>
<rectangle x1="8.93318125" y1="1.73558125" x2="9.18718125" y2="1.82041875" layer="21"/>
<rectangle x1="9.94918125" y1="1.73558125" x2="10.11681875" y2="1.82041875" layer="21"/>
<rectangle x1="11.21918125" y1="1.73558125" x2="11.38681875" y2="1.82041875" layer="21"/>
<rectangle x1="12.48918125" y1="1.73558125" x2="12.65681875" y2="1.82041875" layer="21"/>
<rectangle x1="13.67281875" y1="1.73558125" x2="13.92681875" y2="1.82041875" layer="21"/>
<rectangle x1="15.113" y1="1.73558125" x2="15.367" y2="1.82041875" layer="21"/>
<rectangle x1="16.21281875" y1="1.73558125" x2="16.383" y2="1.82041875" layer="21"/>
<rectangle x1="17.653" y1="1.73558125" x2="17.82318125" y2="1.82041875" layer="21"/>
<rectangle x1="2.24281875" y1="1.82041875" x2="2.49681875" y2="1.905" layer="21"/>
<rectangle x1="4.02081875" y1="1.82041875" x2="4.191" y2="1.905" layer="21"/>
<rectangle x1="5.88518125" y1="1.82041875" x2="6.13918125" y2="1.905" layer="21"/>
<rectangle x1="6.477" y1="1.82041875" x2="6.64718125" y2="1.905" layer="21"/>
<rectangle x1="7.493" y1="1.82041875" x2="7.747" y2="1.905" layer="21"/>
<rectangle x1="8.84681875" y1="1.82041875" x2="9.18718125" y2="1.905" layer="21"/>
<rectangle x1="13.67281875" y1="1.82041875" x2="14.01318125" y2="1.905" layer="21"/>
<rectangle x1="15.02918125" y1="1.82041875" x2="15.367" y2="1.905" layer="21"/>
<rectangle x1="2.24281875" y1="1.905" x2="2.49681875" y2="1.98958125" layer="21"/>
<rectangle x1="4.02081875" y1="1.905" x2="4.191" y2="1.98958125" layer="21"/>
<rectangle x1="5.88518125" y1="1.905" x2="6.13918125" y2="1.98958125" layer="21"/>
<rectangle x1="6.477" y1="1.905" x2="6.64718125" y2="1.98958125" layer="21"/>
<rectangle x1="7.493" y1="1.905" x2="7.747" y2="1.98958125" layer="21"/>
<rectangle x1="8.84681875" y1="1.905" x2="9.10081875" y2="1.98958125" layer="21"/>
<rectangle x1="13.75918125" y1="1.905" x2="14.097" y2="1.98958125" layer="21"/>
<rectangle x1="14.94281875" y1="1.905" x2="15.28318125" y2="1.98958125" layer="21"/>
<rectangle x1="2.24281875" y1="1.98958125" x2="2.49681875" y2="2.07441875" layer="21"/>
<rectangle x1="4.02081875" y1="1.98958125" x2="4.27481875" y2="2.07441875" layer="21"/>
<rectangle x1="5.88518125" y1="1.98958125" x2="6.05281875" y2="2.07441875" layer="21"/>
<rectangle x1="6.477" y1="1.98958125" x2="6.64718125" y2="2.07441875" layer="21"/>
<rectangle x1="7.493" y1="1.98958125" x2="7.747" y2="2.07441875" layer="21"/>
<rectangle x1="8.67918125" y1="1.98958125" x2="9.017" y2="2.07441875" layer="21"/>
<rectangle x1="13.843" y1="1.98958125" x2="14.26718125" y2="2.07441875" layer="21"/>
<rectangle x1="14.77518125" y1="1.98958125" x2="15.19681875" y2="2.07441875" layer="21"/>
<rectangle x1="2.24281875" y1="2.07441875" x2="2.49681875" y2="2.159" layer="21"/>
<rectangle x1="4.02081875" y1="2.07441875" x2="4.27481875" y2="2.159" layer="21"/>
<rectangle x1="6.477" y1="2.07441875" x2="6.64718125" y2="2.159" layer="21"/>
<rectangle x1="7.493" y1="2.07441875" x2="7.747" y2="2.159" layer="21"/>
<rectangle x1="8.59281875" y1="2.07441875" x2="8.93318125" y2="2.159" layer="21"/>
<rectangle x1="13.92681875" y1="2.07441875" x2="15.113" y2="2.159" layer="21"/>
<rectangle x1="2.24281875" y1="2.159" x2="2.49681875" y2="2.24358125" layer="21"/>
<rectangle x1="4.02081875" y1="2.159" x2="4.191" y2="2.24358125" layer="21"/>
<rectangle x1="6.477" y1="2.159" x2="6.64718125" y2="2.24358125" layer="21"/>
<rectangle x1="7.493" y1="2.159" x2="8.84681875" y2="2.24358125" layer="21"/>
<rectangle x1="14.097" y1="2.159" x2="14.94281875" y2="2.24358125" layer="21"/>
<rectangle x1="2.24281875" y1="2.24358125" x2="2.413" y2="2.32841875" layer="21"/>
<rectangle x1="4.02081875" y1="2.24358125" x2="4.191" y2="2.32841875" layer="21"/>
<rectangle x1="6.477" y1="2.24358125" x2="6.64718125" y2="2.32841875" layer="21"/>
<rectangle x1="7.493" y1="2.24358125" x2="8.763" y2="2.32841875" layer="21"/>
<rectangle x1="14.26718125" y1="2.24358125" x2="14.77518125" y2="2.32841875" layer="21"/>
<rectangle x1="7.493" y1="2.32841875" x2="8.509" y2="2.413" layer="21"/>
<rectangle x1="3.34518125" y1="3.25958125" x2="16.80718125" y2="3.34441875" layer="21"/>
<rectangle x1="3.00481875" y1="3.34441875" x2="17.145" y2="3.429" layer="21"/>
<rectangle x1="2.667" y1="3.429" x2="17.399" y2="3.51358125" layer="21"/>
<rectangle x1="2.49681875" y1="3.51358125" x2="17.56918125" y2="3.59841875" layer="21"/>
<rectangle x1="2.32918125" y1="3.59841875" x2="17.82318125" y2="3.683" layer="21"/>
<rectangle x1="2.159" y1="3.683" x2="17.99081875" y2="3.76758125" layer="21"/>
<rectangle x1="1.98881875" y1="3.76758125" x2="18.07718125" y2="3.85241875" layer="21"/>
<rectangle x1="1.905" y1="3.85241875" x2="3.51281875" y2="3.937" layer="21"/>
<rectangle x1="9.779" y1="3.85241875" x2="18.24481875" y2="3.937" layer="21"/>
<rectangle x1="1.73481875" y1="3.937" x2="3.175" y2="4.02158125" layer="21"/>
<rectangle x1="9.779" y1="3.937" x2="18.33118125" y2="4.02158125" layer="21"/>
<rectangle x1="1.651" y1="4.02158125" x2="2.83718125" y2="4.10641875" layer="21"/>
<rectangle x1="9.69518125" y1="4.02158125" x2="18.49881875" y2="4.10641875" layer="21"/>
<rectangle x1="1.56718125" y1="4.10641875" x2="2.667" y2="4.191" layer="21"/>
<rectangle x1="9.60881875" y1="4.10641875" x2="18.58518125" y2="4.191" layer="21"/>
<rectangle x1="1.397" y1="4.191" x2="2.49681875" y2="4.27558125" layer="21"/>
<rectangle x1="9.60881875" y1="4.191" x2="18.669" y2="4.27558125" layer="21"/>
<rectangle x1="1.31318125" y1="4.27558125" x2="2.32918125" y2="4.36041875" layer="21"/>
<rectangle x1="9.525" y1="4.27558125" x2="18.75281875" y2="4.36041875" layer="21"/>
<rectangle x1="1.22681875" y1="4.36041875" x2="2.159" y2="4.445" layer="21"/>
<rectangle x1="9.525" y1="4.36041875" x2="18.83918125" y2="4.445" layer="21"/>
<rectangle x1="1.143" y1="4.445" x2="2.07518125" y2="4.52958125" layer="21"/>
<rectangle x1="9.44118125" y1="4.445" x2="18.923" y2="4.52958125" layer="21"/>
<rectangle x1="1.05918125" y1="4.52958125" x2="1.98881875" y2="4.61441875" layer="21"/>
<rectangle x1="9.44118125" y1="4.52958125" x2="19.00681875" y2="4.61441875" layer="21"/>
<rectangle x1="0.97281875" y1="4.61441875" x2="1.905" y2="4.699" layer="21"/>
<rectangle x1="9.44118125" y1="4.61441875" x2="19.09318125" y2="4.699" layer="21"/>
<rectangle x1="0.97281875" y1="4.699" x2="1.73481875" y2="4.78358125" layer="21"/>
<rectangle x1="9.35481875" y1="4.699" x2="19.177" y2="4.78358125" layer="21"/>
<rectangle x1="0.889" y1="4.78358125" x2="1.651" y2="4.86841875" layer="21"/>
<rectangle x1="9.35481875" y1="4.78358125" x2="19.26081875" y2="4.86841875" layer="21"/>
<rectangle x1="0.80518125" y1="4.86841875" x2="1.56718125" y2="4.953" layer="21"/>
<rectangle x1="9.35481875" y1="4.86841875" x2="19.26081875" y2="4.953" layer="21"/>
<rectangle x1="0.71881875" y1="4.953" x2="1.48081875" y2="5.03758125" layer="21"/>
<rectangle x1="9.271" y1="4.953" x2="19.34718125" y2="5.03758125" layer="21"/>
<rectangle x1="0.71881875" y1="5.03758125" x2="1.48081875" y2="5.12241875" layer="21"/>
<rectangle x1="9.271" y1="5.03758125" x2="19.431" y2="5.12241875" layer="21"/>
<rectangle x1="0.635" y1="5.12241875" x2="1.397" y2="5.207" layer="21"/>
<rectangle x1="9.271" y1="5.12241875" x2="19.431" y2="5.207" layer="21"/>
<rectangle x1="0.55118125" y1="5.207" x2="1.31318125" y2="5.29158125" layer="21"/>
<rectangle x1="9.271" y1="5.207" x2="19.51481875" y2="5.29158125" layer="21"/>
<rectangle x1="0.55118125" y1="5.29158125" x2="1.22681875" y2="5.37641875" layer="21"/>
<rectangle x1="9.18718125" y1="5.29158125" x2="19.51481875" y2="5.37641875" layer="21"/>
<rectangle x1="0.46481875" y1="5.37641875" x2="1.22681875" y2="5.461" layer="21"/>
<rectangle x1="9.18718125" y1="5.37641875" x2="19.60118125" y2="5.461" layer="21"/>
<rectangle x1="0.46481875" y1="5.461" x2="1.143" y2="5.54558125" layer="21"/>
<rectangle x1="9.18718125" y1="5.461" x2="19.685" y2="5.54558125" layer="21"/>
<rectangle x1="0.381" y1="5.54558125" x2="1.05918125" y2="5.63041875" layer="21"/>
<rectangle x1="3.85318125" y1="5.54558125" x2="4.27481875" y2="5.63041875" layer="21"/>
<rectangle x1="5.88518125" y1="5.54558125" x2="6.81481875" y2="5.63041875" layer="21"/>
<rectangle x1="9.18718125" y1="5.54558125" x2="19.685" y2="5.63041875" layer="21"/>
<rectangle x1="0.381" y1="5.63041875" x2="1.05918125" y2="5.715" layer="21"/>
<rectangle x1="3.51281875" y1="5.63041875" x2="4.61518125" y2="5.715" layer="21"/>
<rectangle x1="5.79881875" y1="5.63041875" x2="7.239" y2="5.715" layer="21"/>
<rectangle x1="9.10081875" y1="5.63041875" x2="10.96518125" y2="5.715" layer="21"/>
<rectangle x1="11.64081875" y1="5.63041875" x2="19.685" y2="5.715" layer="21"/>
<rectangle x1="0.381" y1="5.715" x2="0.97281875" y2="5.79958125" layer="21"/>
<rectangle x1="3.34518125" y1="5.715" x2="4.78281875" y2="5.79958125" layer="21"/>
<rectangle x1="5.715" y1="5.715" x2="7.40918125" y2="5.79958125" layer="21"/>
<rectangle x1="9.10081875" y1="5.715" x2="10.71118125" y2="5.79958125" layer="21"/>
<rectangle x1="11.89481875" y1="5.715" x2="13.335" y2="5.79958125" layer="21"/>
<rectangle x1="13.67281875" y1="5.715" x2="15.113" y2="5.79958125" layer="21"/>
<rectangle x1="15.367" y1="5.715" x2="16.383" y2="5.79958125" layer="21"/>
<rectangle x1="17.22881875" y1="5.715" x2="19.76881875" y2="5.79958125" layer="21"/>
<rectangle x1="0.29718125" y1="5.79958125" x2="0.97281875" y2="5.88441875" layer="21"/>
<rectangle x1="3.175" y1="5.79958125" x2="4.953" y2="5.88441875" layer="21"/>
<rectangle x1="5.715" y1="5.79958125" x2="7.57681875" y2="5.88441875" layer="21"/>
<rectangle x1="9.10081875" y1="5.79958125" x2="10.541" y2="5.88441875" layer="21"/>
<rectangle x1="12.065" y1="5.79958125" x2="13.335" y2="5.88441875" layer="21"/>
<rectangle x1="13.67281875" y1="5.79958125" x2="15.113" y2="5.88441875" layer="21"/>
<rectangle x1="15.45081875" y1="5.79958125" x2="16.21281875" y2="5.88441875" layer="21"/>
<rectangle x1="17.48281875" y1="5.79958125" x2="19.76881875" y2="5.88441875" layer="21"/>
<rectangle x1="0.29718125" y1="5.88441875" x2="0.889" y2="5.969" layer="21"/>
<rectangle x1="3.09118125" y1="5.88441875" x2="5.12318125" y2="5.969" layer="21"/>
<rectangle x1="5.715" y1="5.88441875" x2="7.66318125" y2="5.969" layer="21"/>
<rectangle x1="9.10081875" y1="5.88441875" x2="10.37081875" y2="5.969" layer="21"/>
<rectangle x1="12.23518125" y1="5.88441875" x2="13.335" y2="5.969" layer="21"/>
<rectangle x1="13.67281875" y1="5.88441875" x2="15.113" y2="5.969" layer="21"/>
<rectangle x1="15.45081875" y1="5.88441875" x2="16.129" y2="5.969" layer="21"/>
<rectangle x1="17.56918125" y1="5.88441875" x2="19.85518125" y2="5.969" layer="21"/>
<rectangle x1="0.21081875" y1="5.969" x2="0.889" y2="6.05358125" layer="21"/>
<rectangle x1="2.921" y1="5.969" x2="5.207" y2="6.05358125" layer="21"/>
<rectangle x1="5.715" y1="5.969" x2="7.747" y2="6.05358125" layer="21"/>
<rectangle x1="9.10081875" y1="5.969" x2="10.287" y2="6.05358125" layer="21"/>
<rectangle x1="12.319" y1="5.969" x2="13.335" y2="6.05358125" layer="21"/>
<rectangle x1="13.67281875" y1="5.969" x2="15.113" y2="6.05358125" layer="21"/>
<rectangle x1="15.45081875" y1="5.969" x2="16.04518125" y2="6.05358125" layer="21"/>
<rectangle x1="17.653" y1="5.969" x2="19.85518125" y2="6.05358125" layer="21"/>
<rectangle x1="0.21081875" y1="6.05358125" x2="0.889" y2="6.13841875" layer="21"/>
<rectangle x1="2.83718125" y1="6.05358125" x2="5.29081875" y2="6.13841875" layer="21"/>
<rectangle x1="5.79881875" y1="6.05358125" x2="7.83081875" y2="6.13841875" layer="21"/>
<rectangle x1="9.10081875" y1="6.05358125" x2="10.11681875" y2="6.13841875" layer="21"/>
<rectangle x1="12.40281875" y1="6.05358125" x2="13.335" y2="6.13841875" layer="21"/>
<rectangle x1="13.67281875" y1="6.05358125" x2="15.113" y2="6.13841875" layer="21"/>
<rectangle x1="15.45081875" y1="6.05358125" x2="15.95881875" y2="6.13841875" layer="21"/>
<rectangle x1="16.55318125" y1="6.05358125" x2="17.06118125" y2="6.13841875" layer="21"/>
<rectangle x1="17.73681875" y1="6.05358125" x2="19.85518125" y2="6.13841875" layer="21"/>
<rectangle x1="0.21081875" y1="6.13841875" x2="0.80518125" y2="6.223" layer="21"/>
<rectangle x1="2.75081875" y1="6.13841875" x2="5.37718125" y2="6.223" layer="21"/>
<rectangle x1="5.88518125" y1="6.13841875" x2="7.91718125" y2="6.223" layer="21"/>
<rectangle x1="9.017" y1="6.13841875" x2="10.11681875" y2="6.223" layer="21"/>
<rectangle x1="10.96518125" y1="6.13841875" x2="11.64081875" y2="6.223" layer="21"/>
<rectangle x1="12.48918125" y1="6.13841875" x2="13.335" y2="6.223" layer="21"/>
<rectangle x1="13.67281875" y1="6.13841875" x2="15.02918125" y2="6.223" layer="21"/>
<rectangle x1="15.45081875" y1="6.13841875" x2="15.875" y2="6.223" layer="21"/>
<rectangle x1="16.46681875" y1="6.13841875" x2="17.22881875" y2="6.223" layer="21"/>
<rectangle x1="17.73681875" y1="6.13841875" x2="19.939" y2="6.223" layer="21"/>
<rectangle x1="0.21081875" y1="6.223" x2="0.80518125" y2="6.30758125" layer="21"/>
<rectangle x1="2.75081875" y1="6.223" x2="3.683" y2="6.30758125" layer="21"/>
<rectangle x1="4.445" y1="6.223" x2="5.37718125" y2="6.30758125" layer="21"/>
<rectangle x1="7.06881875" y1="6.223" x2="8.001" y2="6.30758125" layer="21"/>
<rectangle x1="9.017" y1="6.223" x2="10.033" y2="6.30758125" layer="21"/>
<rectangle x1="10.795" y1="6.223" x2="11.811" y2="6.30758125" layer="21"/>
<rectangle x1="12.573" y1="6.223" x2="13.335" y2="6.30758125" layer="21"/>
<rectangle x1="13.67281875" y1="6.223" x2="15.02918125" y2="6.30758125" layer="21"/>
<rectangle x1="15.45081875" y1="6.223" x2="15.875" y2="6.30758125" layer="21"/>
<rectangle x1="16.29918125" y1="6.223" x2="17.399" y2="6.30758125" layer="21"/>
<rectangle x1="17.73681875" y1="6.223" x2="19.939" y2="6.30758125" layer="21"/>
<rectangle x1="0.127" y1="6.30758125" x2="0.80518125" y2="6.39241875" layer="21"/>
<rectangle x1="2.667" y1="6.30758125" x2="3.51281875" y2="6.39241875" layer="21"/>
<rectangle x1="4.61518125" y1="6.30758125" x2="5.461" y2="6.39241875" layer="21"/>
<rectangle x1="7.239" y1="6.30758125" x2="8.08481875" y2="6.39241875" layer="21"/>
<rectangle x1="9.017" y1="6.30758125" x2="9.94918125" y2="6.39241875" layer="21"/>
<rectangle x1="10.62481875" y1="6.30758125" x2="11.98118125" y2="6.39241875" layer="21"/>
<rectangle x1="12.65681875" y1="6.30758125" x2="13.335" y2="6.39241875" layer="21"/>
<rectangle x1="13.67281875" y1="6.30758125" x2="15.113" y2="6.39241875" layer="21"/>
<rectangle x1="15.45081875" y1="6.30758125" x2="15.79118125" y2="6.39241875" layer="21"/>
<rectangle x1="16.21281875" y1="6.30758125" x2="17.48281875" y2="6.39241875" layer="21"/>
<rectangle x1="17.653" y1="6.30758125" x2="19.939" y2="6.39241875" layer="21"/>
<rectangle x1="0.127" y1="6.39241875" x2="0.71881875" y2="6.477" layer="21"/>
<rectangle x1="2.58318125" y1="6.39241875" x2="3.34518125" y2="6.477" layer="21"/>
<rectangle x1="4.78281875" y1="6.39241875" x2="5.54481875" y2="6.477" layer="21"/>
<rectangle x1="7.32281875" y1="6.39241875" x2="8.17118125" y2="6.477" layer="21"/>
<rectangle x1="9.017" y1="6.39241875" x2="9.86281875" y2="6.477" layer="21"/>
<rectangle x1="10.541" y1="6.39241875" x2="12.065" y2="6.477" layer="21"/>
<rectangle x1="12.65681875" y1="6.39241875" x2="13.335" y2="6.477" layer="21"/>
<rectangle x1="13.67281875" y1="6.39241875" x2="15.113" y2="6.477" layer="21"/>
<rectangle x1="15.45081875" y1="6.39241875" x2="15.79118125" y2="6.477" layer="21"/>
<rectangle x1="16.21281875" y1="6.39241875" x2="19.939" y2="6.477" layer="21"/>
<rectangle x1="0.127" y1="6.477" x2="0.71881875" y2="6.56158125" layer="21"/>
<rectangle x1="2.58318125" y1="6.477" x2="3.25881875" y2="6.56158125" layer="21"/>
<rectangle x1="4.86918125" y1="6.477" x2="5.54481875" y2="6.56158125" layer="21"/>
<rectangle x1="7.493" y1="6.477" x2="8.17118125" y2="6.56158125" layer="21"/>
<rectangle x1="9.017" y1="6.477" x2="9.86281875" y2="6.56158125" layer="21"/>
<rectangle x1="10.45718125" y1="6.477" x2="12.14881875" y2="6.56158125" layer="21"/>
<rectangle x1="12.74318125" y1="6.477" x2="13.335" y2="6.56158125" layer="21"/>
<rectangle x1="13.67281875" y1="6.477" x2="15.113" y2="6.56158125" layer="21"/>
<rectangle x1="15.45081875" y1="6.477" x2="15.79118125" y2="6.56158125" layer="21"/>
<rectangle x1="16.21281875" y1="6.477" x2="19.939" y2="6.56158125" layer="21"/>
<rectangle x1="0.127" y1="6.56158125" x2="0.71881875" y2="6.64641875" layer="21"/>
<rectangle x1="2.49681875" y1="6.56158125" x2="3.175" y2="6.64641875" layer="21"/>
<rectangle x1="4.953" y1="6.56158125" x2="5.63118125" y2="6.64641875" layer="21"/>
<rectangle x1="7.493" y1="6.56158125" x2="8.255" y2="6.64641875" layer="21"/>
<rectangle x1="9.017" y1="6.56158125" x2="9.779" y2="6.64641875" layer="21"/>
<rectangle x1="10.37081875" y1="6.56158125" x2="12.23518125" y2="6.64641875" layer="21"/>
<rectangle x1="12.74318125" y1="6.56158125" x2="13.335" y2="6.64641875" layer="21"/>
<rectangle x1="13.67281875" y1="6.56158125" x2="15.02918125" y2="6.64641875" layer="21"/>
<rectangle x1="15.45081875" y1="6.56158125" x2="15.79118125" y2="6.64641875" layer="21"/>
<rectangle x1="17.82318125" y1="6.56158125" x2="19.939" y2="6.64641875" layer="21"/>
<rectangle x1="0.127" y1="6.64641875" x2="0.71881875" y2="6.731" layer="21"/>
<rectangle x1="2.49681875" y1="6.64641875" x2="3.175" y2="6.731" layer="21"/>
<rectangle x1="4.953" y1="6.64641875" x2="5.63118125" y2="6.731" layer="21"/>
<rectangle x1="7.57681875" y1="6.64641875" x2="8.255" y2="6.731" layer="21"/>
<rectangle x1="9.017" y1="6.64641875" x2="9.779" y2="6.731" layer="21"/>
<rectangle x1="10.287" y1="6.64641875" x2="12.319" y2="6.731" layer="21"/>
<rectangle x1="12.827" y1="6.64641875" x2="13.335" y2="6.731" layer="21"/>
<rectangle x1="13.67281875" y1="6.64641875" x2="15.02918125" y2="6.731" layer="21"/>
<rectangle x1="15.45081875" y1="6.64641875" x2="15.70481875" y2="6.731" layer="21"/>
<rectangle x1="17.907" y1="6.64641875" x2="19.939" y2="6.731" layer="21"/>
<rectangle x1="0.04318125" y1="6.731" x2="0.71881875" y2="6.81558125" layer="21"/>
<rectangle x1="2.49681875" y1="6.731" x2="3.09118125" y2="6.81558125" layer="21"/>
<rectangle x1="5.03681875" y1="6.731" x2="5.715" y2="6.81558125" layer="21"/>
<rectangle x1="7.66318125" y1="6.731" x2="8.255" y2="6.81558125" layer="21"/>
<rectangle x1="9.017" y1="6.731" x2="9.779" y2="6.81558125" layer="21"/>
<rectangle x1="10.287" y1="6.731" x2="12.319" y2="6.81558125" layer="21"/>
<rectangle x1="12.827" y1="6.731" x2="13.335" y2="6.81558125" layer="21"/>
<rectangle x1="13.67281875" y1="6.731" x2="15.113" y2="6.81558125" layer="21"/>
<rectangle x1="15.45081875" y1="6.731" x2="15.70481875" y2="6.81558125" layer="21"/>
<rectangle x1="17.907" y1="6.731" x2="19.939" y2="6.81558125" layer="21"/>
<rectangle x1="0.04318125" y1="6.81558125" x2="0.71881875" y2="6.90041875" layer="21"/>
<rectangle x1="2.413" y1="6.81558125" x2="3.09118125" y2="6.90041875" layer="21"/>
<rectangle x1="5.03681875" y1="6.81558125" x2="5.715" y2="6.90041875" layer="21"/>
<rectangle x1="7.66318125" y1="6.81558125" x2="8.33881875" y2="6.90041875" layer="21"/>
<rectangle x1="9.017" y1="6.81558125" x2="9.69518125" y2="6.90041875" layer="21"/>
<rectangle x1="10.20318125" y1="6.81558125" x2="12.319" y2="6.90041875" layer="21"/>
<rectangle x1="12.827" y1="6.81558125" x2="13.335" y2="6.90041875" layer="21"/>
<rectangle x1="13.67281875" y1="6.81558125" x2="15.02918125" y2="6.90041875" layer="21"/>
<rectangle x1="15.45081875" y1="6.81558125" x2="15.70481875" y2="6.90041875" layer="21"/>
<rectangle x1="17.907" y1="6.81558125" x2="19.939" y2="6.90041875" layer="21"/>
<rectangle x1="0.04318125" y1="6.90041875" x2="0.635" y2="6.985" layer="21"/>
<rectangle x1="2.413" y1="6.90041875" x2="3.00481875" y2="6.985" layer="21"/>
<rectangle x1="5.12318125" y1="6.90041875" x2="5.715" y2="6.985" layer="21"/>
<rectangle x1="7.66318125" y1="6.90041875" x2="8.33881875" y2="6.985" layer="21"/>
<rectangle x1="9.017" y1="6.90041875" x2="9.69518125" y2="6.985" layer="21"/>
<rectangle x1="10.20318125" y1="6.90041875" x2="12.40281875" y2="6.985" layer="21"/>
<rectangle x1="12.91081875" y1="6.90041875" x2="13.335" y2="6.985" layer="21"/>
<rectangle x1="13.67281875" y1="6.90041875" x2="15.02918125" y2="6.985" layer="21"/>
<rectangle x1="15.45081875" y1="6.90041875" x2="15.79118125" y2="6.985" layer="21"/>
<rectangle x1="16.21281875" y1="6.90041875" x2="17.399" y2="6.985" layer="21"/>
<rectangle x1="17.907" y1="6.90041875" x2="19.939" y2="6.985" layer="21"/>
<rectangle x1="0.04318125" y1="6.985" x2="0.635" y2="7.06958125" layer="21"/>
<rectangle x1="2.413" y1="6.985" x2="3.00481875" y2="7.06958125" layer="21"/>
<rectangle x1="5.12318125" y1="6.985" x2="5.715" y2="7.06958125" layer="21"/>
<rectangle x1="7.747" y1="6.985" x2="8.33881875" y2="7.06958125" layer="21"/>
<rectangle x1="9.017" y1="6.985" x2="9.69518125" y2="7.06958125" layer="21"/>
<rectangle x1="10.20318125" y1="6.985" x2="12.40281875" y2="7.06958125" layer="21"/>
<rectangle x1="12.91081875" y1="6.985" x2="13.335" y2="7.06958125" layer="21"/>
<rectangle x1="13.75918125" y1="6.985" x2="15.02918125" y2="7.06958125" layer="21"/>
<rectangle x1="15.45081875" y1="6.985" x2="15.79118125" y2="7.06958125" layer="21"/>
<rectangle x1="16.21281875" y1="6.985" x2="17.48281875" y2="7.06958125" layer="21"/>
<rectangle x1="17.82318125" y1="6.985" x2="19.939" y2="7.06958125" layer="21"/>
<rectangle x1="0.04318125" y1="7.06958125" x2="0.635" y2="7.15441875" layer="21"/>
<rectangle x1="2.413" y1="7.06958125" x2="3.00481875" y2="7.15441875" layer="21"/>
<rectangle x1="5.12318125" y1="7.06958125" x2="5.715" y2="7.15441875" layer="21"/>
<rectangle x1="7.747" y1="7.06958125" x2="8.33881875" y2="7.15441875" layer="21"/>
<rectangle x1="9.017" y1="7.06958125" x2="9.69518125" y2="7.15441875" layer="21"/>
<rectangle x1="10.20318125" y1="7.06958125" x2="12.40281875" y2="7.15441875" layer="21"/>
<rectangle x1="12.91081875" y1="7.06958125" x2="13.335" y2="7.15441875" layer="21"/>
<rectangle x1="13.75918125" y1="7.06958125" x2="14.94281875" y2="7.15441875" layer="21"/>
<rectangle x1="15.367" y1="7.06958125" x2="15.79118125" y2="7.15441875" layer="21"/>
<rectangle x1="16.21281875" y1="7.06958125" x2="17.399" y2="7.15441875" layer="21"/>
<rectangle x1="17.82318125" y1="7.06958125" x2="19.939" y2="7.15441875" layer="21"/>
<rectangle x1="0.04318125" y1="7.15441875" x2="0.635" y2="7.239" layer="21"/>
<rectangle x1="2.413" y1="7.15441875" x2="3.00481875" y2="7.239" layer="21"/>
<rectangle x1="5.12318125" y1="7.15441875" x2="5.715" y2="7.239" layer="21"/>
<rectangle x1="7.747" y1="7.15441875" x2="8.33881875" y2="7.239" layer="21"/>
<rectangle x1="9.017" y1="7.15441875" x2="9.69518125" y2="7.239" layer="21"/>
<rectangle x1="10.20318125" y1="7.15441875" x2="12.40281875" y2="7.239" layer="21"/>
<rectangle x1="12.91081875" y1="7.15441875" x2="13.335" y2="7.239" layer="21"/>
<rectangle x1="13.843" y1="7.15441875" x2="14.94281875" y2="7.239" layer="21"/>
<rectangle x1="15.367" y1="7.15441875" x2="15.875" y2="7.239" layer="21"/>
<rectangle x1="16.29918125" y1="7.15441875" x2="17.31518125" y2="7.239" layer="21"/>
<rectangle x1="17.82318125" y1="7.15441875" x2="19.939" y2="7.239" layer="21"/>
<rectangle x1="0.04318125" y1="7.239" x2="0.635" y2="7.32358125" layer="21"/>
<rectangle x1="2.413" y1="7.239" x2="3.00481875" y2="7.32358125" layer="21"/>
<rectangle x1="5.12318125" y1="7.239" x2="5.715" y2="7.32358125" layer="21"/>
<rectangle x1="7.747" y1="7.239" x2="8.33881875" y2="7.32358125" layer="21"/>
<rectangle x1="9.017" y1="7.239" x2="9.69518125" y2="7.32358125" layer="21"/>
<rectangle x1="10.20318125" y1="7.239" x2="12.40281875" y2="7.32358125" layer="21"/>
<rectangle x1="12.91081875" y1="7.239" x2="13.335" y2="7.32358125" layer="21"/>
<rectangle x1="13.92681875" y1="7.239" x2="14.859" y2="7.32358125" layer="21"/>
<rectangle x1="15.28318125" y1="7.239" x2="15.875" y2="7.32358125" layer="21"/>
<rectangle x1="16.383" y1="7.239" x2="17.22881875" y2="7.32358125" layer="21"/>
<rectangle x1="17.73681875" y1="7.239" x2="19.939" y2="7.32358125" layer="21"/>
<rectangle x1="0.04318125" y1="7.32358125" x2="0.635" y2="7.40841875" layer="21"/>
<rectangle x1="2.413" y1="7.32358125" x2="3.00481875" y2="7.40841875" layer="21"/>
<rectangle x1="5.12318125" y1="7.32358125" x2="5.715" y2="7.40841875" layer="21"/>
<rectangle x1="7.747" y1="7.32358125" x2="8.33881875" y2="7.40841875" layer="21"/>
<rectangle x1="9.017" y1="7.32358125" x2="9.69518125" y2="7.40841875" layer="21"/>
<rectangle x1="10.20318125" y1="7.32358125" x2="12.40281875" y2="7.40841875" layer="21"/>
<rectangle x1="12.91081875" y1="7.32358125" x2="13.335" y2="7.40841875" layer="21"/>
<rectangle x1="14.01318125" y1="7.32358125" x2="14.68881875" y2="7.40841875" layer="21"/>
<rectangle x1="15.28318125" y1="7.32358125" x2="15.95881875" y2="7.40841875" layer="21"/>
<rectangle x1="16.46681875" y1="7.32358125" x2="17.145" y2="7.40841875" layer="21"/>
<rectangle x1="17.73681875" y1="7.32358125" x2="19.939" y2="7.40841875" layer="21"/>
<rectangle x1="0.04318125" y1="7.40841875" x2="0.635" y2="7.493" layer="21"/>
<rectangle x1="2.413" y1="7.40841875" x2="3.00481875" y2="7.493" layer="21"/>
<rectangle x1="5.12318125" y1="7.40841875" x2="5.715" y2="7.493" layer="21"/>
<rectangle x1="7.747" y1="7.40841875" x2="8.33881875" y2="7.493" layer="21"/>
<rectangle x1="9.017" y1="7.40841875" x2="9.69518125" y2="7.493" layer="21"/>
<rectangle x1="10.20318125" y1="7.40841875" x2="12.40281875" y2="7.493" layer="21"/>
<rectangle x1="12.91081875" y1="7.40841875" x2="13.335" y2="7.493" layer="21"/>
<rectangle x1="14.26718125" y1="7.40841875" x2="14.43481875" y2="7.493" layer="21"/>
<rectangle x1="15.19681875" y1="7.40841875" x2="15.95881875" y2="7.493" layer="21"/>
<rectangle x1="16.72081875" y1="7.40841875" x2="16.891" y2="7.493" layer="21"/>
<rectangle x1="17.653" y1="7.40841875" x2="19.939" y2="7.493" layer="21"/>
<rectangle x1="0.04318125" y1="7.493" x2="0.635" y2="7.57758125" layer="21"/>
<rectangle x1="2.413" y1="7.493" x2="3.00481875" y2="7.57758125" layer="21"/>
<rectangle x1="5.12318125" y1="7.493" x2="5.715" y2="7.57758125" layer="21"/>
<rectangle x1="7.66318125" y1="7.493" x2="8.33881875" y2="7.57758125" layer="21"/>
<rectangle x1="9.017" y1="7.493" x2="9.69518125" y2="7.57758125" layer="21"/>
<rectangle x1="10.20318125" y1="7.493" x2="12.40281875" y2="7.57758125" layer="21"/>
<rectangle x1="12.91081875" y1="7.493" x2="13.335" y2="7.57758125" layer="21"/>
<rectangle x1="15.113" y1="7.493" x2="16.04518125" y2="7.57758125" layer="21"/>
<rectangle x1="17.56918125" y1="7.493" x2="19.939" y2="7.57758125" layer="21"/>
<rectangle x1="0.04318125" y1="7.57758125" x2="0.71881875" y2="7.66241875" layer="21"/>
<rectangle x1="2.413" y1="7.57758125" x2="3.00481875" y2="7.66241875" layer="21"/>
<rectangle x1="5.12318125" y1="7.57758125" x2="5.715" y2="7.66241875" layer="21"/>
<rectangle x1="7.66318125" y1="7.57758125" x2="8.33881875" y2="7.66241875" layer="21"/>
<rectangle x1="9.017" y1="7.57758125" x2="9.69518125" y2="7.66241875" layer="21"/>
<rectangle x1="10.20318125" y1="7.57758125" x2="12.40281875" y2="7.66241875" layer="21"/>
<rectangle x1="12.91081875" y1="7.57758125" x2="13.335" y2="7.66241875" layer="21"/>
<rectangle x1="15.02918125" y1="7.57758125" x2="16.21281875" y2="7.66241875" layer="21"/>
<rectangle x1="17.48281875" y1="7.57758125" x2="19.939" y2="7.66241875" layer="21"/>
<rectangle x1="0.04318125" y1="7.66241875" x2="0.71881875" y2="7.747" layer="21"/>
<rectangle x1="2.413" y1="7.66241875" x2="3.00481875" y2="7.747" layer="21"/>
<rectangle x1="5.12318125" y1="7.66241875" x2="5.715" y2="7.747" layer="21"/>
<rectangle x1="7.66318125" y1="7.66241875" x2="8.255" y2="7.747" layer="21"/>
<rectangle x1="9.017" y1="7.66241875" x2="9.779" y2="7.747" layer="21"/>
<rectangle x1="10.287" y1="7.66241875" x2="12.319" y2="7.747" layer="21"/>
<rectangle x1="12.827" y1="7.66241875" x2="13.335" y2="7.747" layer="21"/>
<rectangle x1="13.67281875" y1="7.66241875" x2="13.843" y2="7.747" layer="21"/>
<rectangle x1="14.859" y1="7.66241875" x2="16.29918125" y2="7.747" layer="21"/>
<rectangle x1="17.31518125" y1="7.66241875" x2="19.939" y2="7.747" layer="21"/>
<rectangle x1="0.127" y1="7.747" x2="0.71881875" y2="7.83158125" layer="21"/>
<rectangle x1="2.413" y1="7.747" x2="3.00481875" y2="7.83158125" layer="21"/>
<rectangle x1="5.12318125" y1="7.747" x2="5.715" y2="7.83158125" layer="21"/>
<rectangle x1="7.57681875" y1="7.747" x2="8.255" y2="7.83158125" layer="21"/>
<rectangle x1="9.017" y1="7.747" x2="9.779" y2="7.83158125" layer="21"/>
<rectangle x1="10.287" y1="7.747" x2="12.319" y2="7.83158125" layer="21"/>
<rectangle x1="12.827" y1="7.747" x2="13.335" y2="7.83158125" layer="21"/>
<rectangle x1="13.589" y1="7.747" x2="14.097" y2="7.83158125" layer="21"/>
<rectangle x1="14.68881875" y1="7.747" x2="16.55318125" y2="7.83158125" layer="21"/>
<rectangle x1="17.06118125" y1="7.747" x2="19.939" y2="7.83158125" layer="21"/>
<rectangle x1="0.127" y1="7.83158125" x2="0.71881875" y2="7.91641875" layer="21"/>
<rectangle x1="2.413" y1="7.83158125" x2="3.00481875" y2="7.91641875" layer="21"/>
<rectangle x1="5.12318125" y1="7.83158125" x2="5.715" y2="7.91641875" layer="21"/>
<rectangle x1="7.57681875" y1="7.83158125" x2="8.255" y2="7.91641875" layer="21"/>
<rectangle x1="9.017" y1="7.83158125" x2="9.779" y2="7.91641875" layer="21"/>
<rectangle x1="10.37081875" y1="7.83158125" x2="12.23518125" y2="7.91641875" layer="21"/>
<rectangle x1="12.827" y1="7.83158125" x2="19.939" y2="7.91641875" layer="21"/>
<rectangle x1="0.127" y1="7.91641875" x2="0.71881875" y2="8.001" layer="21"/>
<rectangle x1="2.413" y1="7.91641875" x2="3.00481875" y2="8.001" layer="21"/>
<rectangle x1="5.12318125" y1="7.91641875" x2="5.715" y2="8.001" layer="21"/>
<rectangle x1="7.493" y1="7.91641875" x2="8.17118125" y2="8.001" layer="21"/>
<rectangle x1="9.017" y1="7.91641875" x2="9.86281875" y2="8.001" layer="21"/>
<rectangle x1="10.45718125" y1="7.91641875" x2="12.14881875" y2="8.001" layer="21"/>
<rectangle x1="12.74318125" y1="7.91641875" x2="19.939" y2="8.001" layer="21"/>
<rectangle x1="0.127" y1="8.001" x2="0.71881875" y2="8.08558125" layer="21"/>
<rectangle x1="2.413" y1="8.001" x2="3.00481875" y2="8.08558125" layer="21"/>
<rectangle x1="5.12318125" y1="8.001" x2="5.715" y2="8.08558125" layer="21"/>
<rectangle x1="7.40918125" y1="8.001" x2="8.17118125" y2="8.08558125" layer="21"/>
<rectangle x1="9.017" y1="8.001" x2="9.86281875" y2="8.08558125" layer="21"/>
<rectangle x1="10.45718125" y1="8.001" x2="12.065" y2="8.08558125" layer="21"/>
<rectangle x1="12.74318125" y1="8.001" x2="19.939" y2="8.08558125" layer="21"/>
<rectangle x1="0.127" y1="8.08558125" x2="0.80518125" y2="8.17041875" layer="21"/>
<rectangle x1="2.413" y1="8.08558125" x2="3.00481875" y2="8.17041875" layer="21"/>
<rectangle x1="5.12318125" y1="8.08558125" x2="5.715" y2="8.17041875" layer="21"/>
<rectangle x1="7.32281875" y1="8.08558125" x2="8.08481875" y2="8.17041875" layer="21"/>
<rectangle x1="9.017" y1="8.08558125" x2="9.94918125" y2="8.17041875" layer="21"/>
<rectangle x1="10.62481875" y1="8.08558125" x2="11.98118125" y2="8.17041875" layer="21"/>
<rectangle x1="12.65681875" y1="8.08558125" x2="19.939" y2="8.17041875" layer="21"/>
<rectangle x1="0.127" y1="8.17041875" x2="0.80518125" y2="8.255" layer="21"/>
<rectangle x1="2.413" y1="8.17041875" x2="3.00481875" y2="8.255" layer="21"/>
<rectangle x1="5.12318125" y1="8.17041875" x2="5.715" y2="8.255" layer="21"/>
<rectangle x1="7.15518125" y1="8.17041875" x2="8.001" y2="8.255" layer="21"/>
<rectangle x1="9.017" y1="8.17041875" x2="9.94918125" y2="8.255" layer="21"/>
<rectangle x1="10.71118125" y1="8.17041875" x2="11.89481875" y2="8.255" layer="21"/>
<rectangle x1="12.573" y1="8.17041875" x2="19.939" y2="8.255" layer="21"/>
<rectangle x1="0.21081875" y1="8.255" x2="0.80518125" y2="8.33958125" layer="21"/>
<rectangle x1="2.413" y1="8.255" x2="3.00481875" y2="8.33958125" layer="21"/>
<rectangle x1="5.12318125" y1="8.255" x2="5.715" y2="8.33958125" layer="21"/>
<rectangle x1="6.90118125" y1="8.255" x2="8.001" y2="8.33958125" layer="21"/>
<rectangle x1="9.017" y1="8.255" x2="10.033" y2="8.33958125" layer="21"/>
<rectangle x1="10.87881875" y1="8.255" x2="11.72718125" y2="8.33958125" layer="21"/>
<rectangle x1="12.573" y1="8.255" x2="19.939" y2="8.33958125" layer="21"/>
<rectangle x1="0.21081875" y1="8.33958125" x2="0.889" y2="8.42441875" layer="21"/>
<rectangle x1="2.413" y1="8.33958125" x2="3.00481875" y2="8.42441875" layer="21"/>
<rectangle x1="5.12318125" y1="8.33958125" x2="7.91718125" y2="8.42441875" layer="21"/>
<rectangle x1="9.10081875" y1="8.33958125" x2="10.11681875" y2="8.42441875" layer="21"/>
<rectangle x1="11.21918125" y1="8.33958125" x2="11.38681875" y2="8.42441875" layer="21"/>
<rectangle x1="12.48918125" y1="8.33958125" x2="19.85518125" y2="8.42441875" layer="21"/>
<rectangle x1="0.21081875" y1="8.42441875" x2="0.889" y2="8.509" layer="21"/>
<rectangle x1="2.413" y1="8.42441875" x2="3.00481875" y2="8.509" layer="21"/>
<rectangle x1="5.12318125" y1="8.42441875" x2="7.83081875" y2="8.509" layer="21"/>
<rectangle x1="9.10081875" y1="8.42441875" x2="10.20318125" y2="8.509" layer="21"/>
<rectangle x1="12.40281875" y1="8.42441875" x2="19.85518125" y2="8.509" layer="21"/>
<rectangle x1="0.29718125" y1="8.509" x2="0.889" y2="8.59358125" layer="21"/>
<rectangle x1="2.413" y1="8.509" x2="3.00481875" y2="8.59358125" layer="21"/>
<rectangle x1="5.12318125" y1="8.509" x2="7.747" y2="8.59358125" layer="21"/>
<rectangle x1="9.10081875" y1="8.509" x2="10.287" y2="8.59358125" layer="21"/>
<rectangle x1="12.23518125" y1="8.509" x2="19.85518125" y2="8.59358125" layer="21"/>
<rectangle x1="0.29718125" y1="8.59358125" x2="0.97281875" y2="8.67841875" layer="21"/>
<rectangle x1="2.413" y1="8.59358125" x2="3.00481875" y2="8.67841875" layer="21"/>
<rectangle x1="5.12318125" y1="8.59358125" x2="7.57681875" y2="8.67841875" layer="21"/>
<rectangle x1="9.10081875" y1="8.59358125" x2="10.45718125" y2="8.67841875" layer="21"/>
<rectangle x1="12.14881875" y1="8.59358125" x2="19.76881875" y2="8.67841875" layer="21"/>
<rectangle x1="0.29718125" y1="8.67841875" x2="0.97281875" y2="8.763" layer="21"/>
<rectangle x1="2.413" y1="8.67841875" x2="3.00481875" y2="8.763" layer="21"/>
<rectangle x1="5.12318125" y1="8.67841875" x2="7.493" y2="8.763" layer="21"/>
<rectangle x1="9.10081875" y1="8.67841875" x2="10.62481875" y2="8.763" layer="21"/>
<rectangle x1="11.98118125" y1="8.67841875" x2="19.76881875" y2="8.763" layer="21"/>
<rectangle x1="0.381" y1="8.763" x2="1.05918125" y2="8.84758125" layer="21"/>
<rectangle x1="2.413" y1="8.763" x2="2.921" y2="8.84758125" layer="21"/>
<rectangle x1="5.207" y1="8.763" x2="7.32281875" y2="8.84758125" layer="21"/>
<rectangle x1="9.10081875" y1="8.763" x2="10.795" y2="8.84758125" layer="21"/>
<rectangle x1="11.72718125" y1="8.763" x2="19.76881875" y2="8.84758125" layer="21"/>
<rectangle x1="0.381" y1="8.84758125" x2="1.05918125" y2="8.93241875" layer="21"/>
<rectangle x1="2.58318125" y1="8.84758125" x2="2.83718125" y2="8.93241875" layer="21"/>
<rectangle x1="5.29081875" y1="8.84758125" x2="7.06881875" y2="8.93241875" layer="21"/>
<rectangle x1="9.18718125" y1="8.84758125" x2="19.685" y2="8.93241875" layer="21"/>
<rectangle x1="0.46481875" y1="8.93241875" x2="1.143" y2="9.017" layer="21"/>
<rectangle x1="9.18718125" y1="8.93241875" x2="19.685" y2="9.017" layer="21"/>
<rectangle x1="0.46481875" y1="9.017" x2="1.143" y2="9.10158125" layer="21"/>
<rectangle x1="9.18718125" y1="9.017" x2="19.60118125" y2="9.10158125" layer="21"/>
<rectangle x1="0.55118125" y1="9.10158125" x2="1.22681875" y2="9.18641875" layer="21"/>
<rectangle x1="9.18718125" y1="9.10158125" x2="19.60118125" y2="9.18641875" layer="21"/>
<rectangle x1="0.55118125" y1="9.18641875" x2="1.31318125" y2="9.271" layer="21"/>
<rectangle x1="9.18718125" y1="9.18641875" x2="19.51481875" y2="9.271" layer="21"/>
<rectangle x1="0.635" y1="9.271" x2="1.397" y2="9.35558125" layer="21"/>
<rectangle x1="9.271" y1="9.271" x2="19.51481875" y2="9.35558125" layer="21"/>
<rectangle x1="0.71881875" y1="9.35558125" x2="1.397" y2="9.44041875" layer="21"/>
<rectangle x1="9.271" y1="9.35558125" x2="19.431" y2="9.44041875" layer="21"/>
<rectangle x1="0.71881875" y1="9.44041875" x2="1.48081875" y2="9.525" layer="21"/>
<rectangle x1="9.271" y1="9.44041875" x2="19.34718125" y2="9.525" layer="21"/>
<rectangle x1="0.80518125" y1="9.525" x2="1.56718125" y2="9.60958125" layer="21"/>
<rectangle x1="9.35481875" y1="9.525" x2="19.34718125" y2="9.60958125" layer="21"/>
<rectangle x1="0.889" y1="9.60958125" x2="1.651" y2="9.69441875" layer="21"/>
<rectangle x1="9.35481875" y1="9.60958125" x2="19.26081875" y2="9.69441875" layer="21"/>
<rectangle x1="0.889" y1="9.69441875" x2="1.73481875" y2="9.779" layer="21"/>
<rectangle x1="9.35481875" y1="9.69441875" x2="19.177" y2="9.779" layer="21"/>
<rectangle x1="0.97281875" y1="9.779" x2="1.82118125" y2="9.86358125" layer="21"/>
<rectangle x1="9.44118125" y1="9.779" x2="19.09318125" y2="9.86358125" layer="21"/>
<rectangle x1="1.05918125" y1="9.86358125" x2="1.905" y2="9.94841875" layer="21"/>
<rectangle x1="9.44118125" y1="9.86358125" x2="19.00681875" y2="9.94841875" layer="21"/>
<rectangle x1="1.143" y1="9.94841875" x2="2.07518125" y2="10.033" layer="21"/>
<rectangle x1="9.44118125" y1="9.94841875" x2="18.923" y2="10.033" layer="21"/>
<rectangle x1="1.22681875" y1="10.033" x2="2.159" y2="10.11758125" layer="21"/>
<rectangle x1="9.525" y1="10.033" x2="18.923" y2="10.11758125" layer="21"/>
<rectangle x1="1.31318125" y1="10.11758125" x2="2.24281875" y2="10.20241875" layer="21"/>
<rectangle x1="9.525" y1="10.11758125" x2="18.75281875" y2="10.20241875" layer="21"/>
<rectangle x1="1.397" y1="10.20241875" x2="2.413" y2="10.287" layer="21"/>
<rectangle x1="9.60881875" y1="10.20241875" x2="18.669" y2="10.287" layer="21"/>
<rectangle x1="1.48081875" y1="10.287" x2="2.58318125" y2="10.37158125" layer="21"/>
<rectangle x1="9.60881875" y1="10.287" x2="18.58518125" y2="10.37158125" layer="21"/>
<rectangle x1="1.56718125" y1="10.37158125" x2="2.75081875" y2="10.45641875" layer="21"/>
<rectangle x1="9.69518125" y1="10.37158125" x2="18.49881875" y2="10.45641875" layer="21"/>
<rectangle x1="1.73481875" y1="10.45641875" x2="3.00481875" y2="10.541" layer="21"/>
<rectangle x1="9.69518125" y1="10.45641875" x2="18.415" y2="10.541" layer="21"/>
<rectangle x1="1.82118125" y1="10.541" x2="3.34518125" y2="10.62558125" layer="21"/>
<rectangle x1="9.779" y1="10.541" x2="18.24481875" y2="10.62558125" layer="21"/>
<rectangle x1="1.98881875" y1="10.62558125" x2="9.69518125" y2="10.71041875" layer="21"/>
<rectangle x1="9.779" y1="10.62558125" x2="18.161" y2="10.71041875" layer="21"/>
<rectangle x1="2.07518125" y1="10.71041875" x2="17.99081875" y2="10.795" layer="21"/>
<rectangle x1="2.24281875" y1="10.795" x2="17.82318125" y2="10.87958125" layer="21"/>
<rectangle x1="2.413" y1="10.87958125" x2="17.653" y2="10.96441875" layer="21"/>
<rectangle x1="2.58318125" y1="10.96441875" x2="17.48281875" y2="11.049" layer="21"/>
<rectangle x1="2.83718125" y1="11.049" x2="17.22881875" y2="11.13358125" layer="21"/>
<rectangle x1="3.175" y1="11.13358125" x2="16.891" y2="11.21841875" layer="21"/>
</package>
<package name="UDO-LOGO-30MM">
<rectangle x1="4.36118125" y1="0.55041875" x2="5.29081875" y2="0.635" layer="21"/>
<rectangle x1="21.29281875" y1="0.55041875" x2="22.30881875" y2="0.635" layer="21"/>
<rectangle x1="4.191" y1="0.635" x2="5.54481875" y2="0.71958125" layer="21"/>
<rectangle x1="6.731" y1="0.635" x2="6.81481875" y2="0.71958125" layer="21"/>
<rectangle x1="8.42518125" y1="0.635" x2="8.509" y2="0.71958125" layer="21"/>
<rectangle x1="8.93318125" y1="0.635" x2="9.10081875" y2="0.71958125" layer="21"/>
<rectangle x1="10.541" y1="0.635" x2="10.795" y2="0.71958125" layer="21"/>
<rectangle x1="11.38681875" y1="0.635" x2="12.65681875" y2="0.71958125" layer="21"/>
<rectangle x1="14.859" y1="0.635" x2="15.28318125" y2="0.71958125" layer="21"/>
<rectangle x1="16.80718125" y1="0.635" x2="17.145" y2="0.71958125" layer="21"/>
<rectangle x1="18.669" y1="0.635" x2="19.09318125" y2="0.71958125" layer="21"/>
<rectangle x1="21.12518125" y1="0.635" x2="22.479" y2="0.71958125" layer="21"/>
<rectangle x1="23.495" y1="0.635" x2="23.66518125" y2="0.71958125" layer="21"/>
<rectangle x1="25.273" y1="0.635" x2="25.35681875" y2="0.71958125" layer="21"/>
<rectangle x1="26.45918125" y1="0.635" x2="26.797" y2="0.71958125" layer="21"/>
<rectangle x1="4.10718125" y1="0.71958125" x2="5.715" y2="0.80441875" layer="21"/>
<rectangle x1="6.64718125" y1="0.71958125" x2="6.90118125" y2="0.80441875" layer="21"/>
<rectangle x1="8.33881875" y1="0.71958125" x2="8.59281875" y2="0.80441875" layer="21"/>
<rectangle x1="8.84681875" y1="0.71958125" x2="9.18718125" y2="0.80441875" layer="21"/>
<rectangle x1="10.287" y1="0.71958125" x2="10.87881875" y2="0.80441875" layer="21"/>
<rectangle x1="11.303" y1="0.71958125" x2="12.91081875" y2="0.80441875" layer="21"/>
<rectangle x1="14.605" y1="0.71958125" x2="15.53718125" y2="0.80441875" layer="21"/>
<rectangle x1="16.637" y1="0.71958125" x2="17.399" y2="0.80441875" layer="21"/>
<rectangle x1="18.415" y1="0.71958125" x2="19.34718125" y2="0.80441875" layer="21"/>
<rectangle x1="20.955" y1="0.71958125" x2="22.64918125" y2="0.80441875" layer="21"/>
<rectangle x1="23.495" y1="0.71958125" x2="23.749" y2="0.80441875" layer="21"/>
<rectangle x1="25.18918125" y1="0.71958125" x2="25.44318125" y2="0.80441875" layer="21"/>
<rectangle x1="26.20518125" y1="0.71958125" x2="27.13481875" y2="0.80441875" layer="21"/>
<rectangle x1="3.937" y1="0.80441875" x2="4.61518125" y2="0.889" layer="21"/>
<rectangle x1="5.12318125" y1="0.80441875" x2="5.79881875" y2="0.889" layer="21"/>
<rectangle x1="6.64718125" y1="0.80441875" x2="6.90118125" y2="0.889" layer="21"/>
<rectangle x1="8.33881875" y1="0.80441875" x2="8.59281875" y2="0.889" layer="21"/>
<rectangle x1="8.84681875" y1="0.80441875" x2="9.18718125" y2="0.889" layer="21"/>
<rectangle x1="10.11681875" y1="0.80441875" x2="10.87881875" y2="0.889" layer="21"/>
<rectangle x1="11.303" y1="0.80441875" x2="13.081" y2="0.889" layer="21"/>
<rectangle x1="14.52118125" y1="0.80441875" x2="15.621" y2="0.889" layer="21"/>
<rectangle x1="16.46681875" y1="0.80441875" x2="17.48281875" y2="0.889" layer="21"/>
<rectangle x1="18.33118125" y1="0.80441875" x2="19.431" y2="0.889" layer="21"/>
<rectangle x1="20.87118125" y1="0.80441875" x2="21.54681875" y2="0.889" layer="21"/>
<rectangle x1="22.05481875" y1="0.80441875" x2="22.733" y2="0.889" layer="21"/>
<rectangle x1="23.495" y1="0.80441875" x2="23.749" y2="0.889" layer="21"/>
<rectangle x1="25.18918125" y1="0.80441875" x2="25.44318125" y2="0.889" layer="21"/>
<rectangle x1="26.035" y1="0.80441875" x2="27.22118125" y2="0.889" layer="21"/>
<rectangle x1="3.85318125" y1="0.889" x2="4.36118125" y2="0.97358125" layer="21"/>
<rectangle x1="5.37718125" y1="0.889" x2="5.88518125" y2="0.97358125" layer="21"/>
<rectangle x1="6.64718125" y1="0.889" x2="6.90118125" y2="0.97358125" layer="21"/>
<rectangle x1="8.33881875" y1="0.889" x2="8.59281875" y2="0.97358125" layer="21"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.18718125" y2="0.97358125" layer="21"/>
<rectangle x1="10.033" y1="0.889" x2="10.795" y2="0.97358125" layer="21"/>
<rectangle x1="11.303" y1="0.889" x2="13.25118125" y2="0.97358125" layer="21"/>
<rectangle x1="14.351" y1="0.889" x2="15.79118125" y2="0.97358125" layer="21"/>
<rectangle x1="16.383" y1="0.889" x2="17.56918125" y2="0.97358125" layer="21"/>
<rectangle x1="18.24481875" y1="0.889" x2="19.60118125" y2="0.97358125" layer="21"/>
<rectangle x1="20.78481875" y1="0.889" x2="21.29281875" y2="0.97358125" layer="21"/>
<rectangle x1="22.30881875" y1="0.889" x2="22.81681875" y2="0.97358125" layer="21"/>
<rectangle x1="23.495" y1="0.889" x2="23.749" y2="0.97358125" layer="21"/>
<rectangle x1="25.18918125" y1="0.889" x2="25.44318125" y2="0.97358125" layer="21"/>
<rectangle x1="25.95118125" y1="0.889" x2="27.305" y2="0.97358125" layer="21"/>
<rectangle x1="3.76681875" y1="0.97358125" x2="4.191" y2="1.05841875" layer="21"/>
<rectangle x1="5.54481875" y1="0.97358125" x2="5.969" y2="1.05841875" layer="21"/>
<rectangle x1="6.64718125" y1="0.97358125" x2="6.90118125" y2="1.05841875" layer="21"/>
<rectangle x1="8.33881875" y1="0.97358125" x2="8.59281875" y2="1.05841875" layer="21"/>
<rectangle x1="8.84681875" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="21"/>
<rectangle x1="9.94918125" y1="0.97358125" x2="10.45718125" y2="1.05841875" layer="21"/>
<rectangle x1="11.303" y1="0.97358125" x2="11.557" y2="1.05841875" layer="21"/>
<rectangle x1="12.827" y1="0.97358125" x2="13.335" y2="1.05841875" layer="21"/>
<rectangle x1="14.351" y1="0.97358125" x2="14.859" y2="1.05841875" layer="21"/>
<rectangle x1="15.367" y1="0.97358125" x2="15.875" y2="1.05841875" layer="21"/>
<rectangle x1="16.383" y1="0.97358125" x2="16.80718125" y2="1.05841875" layer="21"/>
<rectangle x1="17.22881875" y1="0.97358125" x2="17.653" y2="1.05841875" layer="21"/>
<rectangle x1="18.161" y1="0.97358125" x2="18.58518125" y2="1.05841875" layer="21"/>
<rectangle x1="19.177" y1="0.97358125" x2="19.685" y2="1.05841875" layer="21"/>
<rectangle x1="20.701" y1="0.97358125" x2="21.12518125" y2="1.05841875" layer="21"/>
<rectangle x1="22.479" y1="0.97358125" x2="22.90318125" y2="1.05841875" layer="21"/>
<rectangle x1="23.495" y1="0.97358125" x2="23.749" y2="1.05841875" layer="21"/>
<rectangle x1="25.18918125" y1="0.97358125" x2="25.44318125" y2="1.05841875" layer="21"/>
<rectangle x1="25.86481875" y1="0.97358125" x2="26.37281875" y2="1.05841875" layer="21"/>
<rectangle x1="26.96718125" y1="0.97358125" x2="27.38881875" y2="1.05841875" layer="21"/>
<rectangle x1="3.683" y1="1.05841875" x2="4.10718125" y2="1.143" layer="21"/>
<rectangle x1="5.63118125" y1="1.05841875" x2="6.05281875" y2="1.143" layer="21"/>
<rectangle x1="6.64718125" y1="1.05841875" x2="6.90118125" y2="1.143" layer="21"/>
<rectangle x1="8.33881875" y1="1.05841875" x2="8.59281875" y2="1.143" layer="21"/>
<rectangle x1="8.84681875" y1="1.05841875" x2="9.18718125" y2="1.143" layer="21"/>
<rectangle x1="9.86281875" y1="1.05841875" x2="10.287" y2="1.143" layer="21"/>
<rectangle x1="11.303" y1="1.05841875" x2="11.557" y2="1.143" layer="21"/>
<rectangle x1="12.99718125" y1="1.05841875" x2="13.41881875" y2="1.143" layer="21"/>
<rectangle x1="14.26718125" y1="1.05841875" x2="14.68881875" y2="1.143" layer="21"/>
<rectangle x1="15.53718125" y1="1.05841875" x2="15.875" y2="1.143" layer="21"/>
<rectangle x1="16.29918125" y1="1.05841875" x2="16.637" y2="1.143" layer="21"/>
<rectangle x1="17.399" y1="1.05841875" x2="17.653" y2="1.143" layer="21"/>
<rectangle x1="18.07718125" y1="1.05841875" x2="18.49881875" y2="1.143" layer="21"/>
<rectangle x1="19.34718125" y1="1.05841875" x2="19.685" y2="1.143" layer="21"/>
<rectangle x1="20.61718125" y1="1.05841875" x2="21.03881875" y2="1.143" layer="21"/>
<rectangle x1="22.56281875" y1="1.05841875" x2="22.987" y2="1.143" layer="21"/>
<rectangle x1="23.495" y1="1.05841875" x2="23.749" y2="1.143" layer="21"/>
<rectangle x1="25.18918125" y1="1.05841875" x2="25.44318125" y2="1.143" layer="21"/>
<rectangle x1="25.86481875" y1="1.05841875" x2="26.20518125" y2="1.143" layer="21"/>
<rectangle x1="27.051" y1="1.05841875" x2="27.47518125" y2="1.143" layer="21"/>
<rectangle x1="3.683" y1="1.143" x2="4.02081875" y2="1.22758125" layer="21"/>
<rectangle x1="5.715" y1="1.143" x2="6.13918125" y2="1.22758125" layer="21"/>
<rectangle x1="6.64718125" y1="1.143" x2="6.90118125" y2="1.22758125" layer="21"/>
<rectangle x1="8.33881875" y1="1.143" x2="8.59281875" y2="1.22758125" layer="21"/>
<rectangle x1="8.84681875" y1="1.143" x2="9.18718125" y2="1.22758125" layer="21"/>
<rectangle x1="9.779" y1="1.143" x2="10.20318125" y2="1.22758125" layer="21"/>
<rectangle x1="11.303" y1="1.143" x2="11.557" y2="1.22758125" layer="21"/>
<rectangle x1="13.081" y1="1.143" x2="13.50518125" y2="1.22758125" layer="21"/>
<rectangle x1="14.18081875" y1="1.143" x2="14.52118125" y2="1.22758125" layer="21"/>
<rectangle x1="15.621" y1="1.143" x2="15.95881875" y2="1.22758125" layer="21"/>
<rectangle x1="16.383" y1="1.143" x2="16.637" y2="1.22758125" layer="21"/>
<rectangle x1="17.399" y1="1.143" x2="17.73681875" y2="1.22758125" layer="21"/>
<rectangle x1="17.99081875" y1="1.143" x2="18.33118125" y2="1.22758125" layer="21"/>
<rectangle x1="19.431" y1="1.143" x2="19.76881875" y2="1.22758125" layer="21"/>
<rectangle x1="20.61718125" y1="1.143" x2="20.955" y2="1.22758125" layer="21"/>
<rectangle x1="22.64918125" y1="1.143" x2="23.07081875" y2="1.22758125" layer="21"/>
<rectangle x1="23.495" y1="1.143" x2="23.749" y2="1.22758125" layer="21"/>
<rectangle x1="25.18918125" y1="1.143" x2="25.44318125" y2="1.22758125" layer="21"/>
<rectangle x1="25.781" y1="1.143" x2="26.11881875" y2="1.22758125" layer="21"/>
<rectangle x1="27.22118125" y1="1.143" x2="27.559" y2="1.22758125" layer="21"/>
<rectangle x1="3.59918125" y1="1.22758125" x2="3.937" y2="1.31241875" layer="21"/>
<rectangle x1="5.79881875" y1="1.22758125" x2="6.13918125" y2="1.31241875" layer="21"/>
<rectangle x1="6.64718125" y1="1.22758125" x2="6.90118125" y2="1.31241875" layer="21"/>
<rectangle x1="8.33881875" y1="1.22758125" x2="8.59281875" y2="1.31241875" layer="21"/>
<rectangle x1="8.84681875" y1="1.22758125" x2="9.18718125" y2="1.31241875" layer="21"/>
<rectangle x1="9.779" y1="1.22758125" x2="10.11681875" y2="1.31241875" layer="21"/>
<rectangle x1="11.303" y1="1.22758125" x2="11.557" y2="1.31241875" layer="21"/>
<rectangle x1="13.25118125" y1="1.22758125" x2="13.589" y2="1.31241875" layer="21"/>
<rectangle x1="14.18081875" y1="1.22758125" x2="14.52118125" y2="1.31241875" layer="21"/>
<rectangle x1="15.70481875" y1="1.22758125" x2="15.95881875" y2="1.31241875" layer="21"/>
<rectangle x1="17.399" y1="1.22758125" x2="17.653" y2="1.31241875" layer="21"/>
<rectangle x1="17.99081875" y1="1.22758125" x2="18.33118125" y2="1.31241875" layer="21"/>
<rectangle x1="19.51481875" y1="1.22758125" x2="19.76881875" y2="1.31241875" layer="21"/>
<rectangle x1="20.53081875" y1="1.22758125" x2="20.87118125" y2="1.31241875" layer="21"/>
<rectangle x1="22.733" y1="1.22758125" x2="23.07081875" y2="1.31241875" layer="21"/>
<rectangle x1="23.495" y1="1.22758125" x2="23.749" y2="1.31241875" layer="21"/>
<rectangle x1="25.18918125" y1="1.22758125" x2="25.44318125" y2="1.31241875" layer="21"/>
<rectangle x1="25.69718125" y1="1.22758125" x2="26.035" y2="1.31241875" layer="21"/>
<rectangle x1="27.22118125" y1="1.22758125" x2="27.559" y2="1.31241875" layer="21"/>
<rectangle x1="3.51281875" y1="1.31241875" x2="3.85318125" y2="1.397" layer="21"/>
<rectangle x1="5.88518125" y1="1.31241875" x2="6.223" y2="1.397" layer="21"/>
<rectangle x1="6.64718125" y1="1.31241875" x2="6.90118125" y2="1.397" layer="21"/>
<rectangle x1="8.33881875" y1="1.31241875" x2="8.59281875" y2="1.397" layer="21"/>
<rectangle x1="8.84681875" y1="1.31241875" x2="9.18718125" y2="1.397" layer="21"/>
<rectangle x1="9.779" y1="1.31241875" x2="10.033" y2="1.397" layer="21"/>
<rectangle x1="11.303" y1="1.31241875" x2="11.557" y2="1.397" layer="21"/>
<rectangle x1="13.335" y1="1.31241875" x2="13.67281875" y2="1.397" layer="21"/>
<rectangle x1="14.097" y1="1.31241875" x2="14.43481875" y2="1.397" layer="21"/>
<rectangle x1="15.70481875" y1="1.31241875" x2="16.04518125" y2="1.397" layer="21"/>
<rectangle x1="17.31518125" y1="1.31241875" x2="17.653" y2="1.397" layer="21"/>
<rectangle x1="17.907" y1="1.31241875" x2="18.24481875" y2="1.397" layer="21"/>
<rectangle x1="19.51481875" y1="1.31241875" x2="19.76881875" y2="1.397" layer="21"/>
<rectangle x1="20.447" y1="1.31241875" x2="20.78481875" y2="1.397" layer="21"/>
<rectangle x1="22.81681875" y1="1.31241875" x2="23.15718125" y2="1.397" layer="21"/>
<rectangle x1="23.495" y1="1.31241875" x2="23.749" y2="1.397" layer="21"/>
<rectangle x1="25.18918125" y1="1.31241875" x2="25.44318125" y2="1.397" layer="21"/>
<rectangle x1="25.69718125" y1="1.31241875" x2="26.035" y2="1.397" layer="21"/>
<rectangle x1="27.305" y1="1.31241875" x2="27.47518125" y2="1.397" layer="21"/>
<rectangle x1="3.51281875" y1="1.397" x2="3.85318125" y2="1.48158125" layer="21"/>
<rectangle x1="5.88518125" y1="1.397" x2="6.223" y2="1.48158125" layer="21"/>
<rectangle x1="6.64718125" y1="1.397" x2="6.90118125" y2="1.48158125" layer="21"/>
<rectangle x1="8.33881875" y1="1.397" x2="8.59281875" y2="1.48158125" layer="21"/>
<rectangle x1="8.84681875" y1="1.397" x2="9.18718125" y2="1.48158125" layer="21"/>
<rectangle x1="9.69518125" y1="1.397" x2="10.033" y2="1.48158125" layer="21"/>
<rectangle x1="11.303" y1="1.397" x2="11.557" y2="1.48158125" layer="21"/>
<rectangle x1="13.335" y1="1.397" x2="13.67281875" y2="1.48158125" layer="21"/>
<rectangle x1="14.097" y1="1.397" x2="14.43481875" y2="1.48158125" layer="21"/>
<rectangle x1="15.79118125" y1="1.397" x2="16.04518125" y2="1.48158125" layer="21"/>
<rectangle x1="17.22881875" y1="1.397" x2="17.653" y2="1.48158125" layer="21"/>
<rectangle x1="17.907" y1="1.397" x2="18.24481875" y2="1.48158125" layer="21"/>
<rectangle x1="20.447" y1="1.397" x2="20.78481875" y2="1.48158125" layer="21"/>
<rectangle x1="22.81681875" y1="1.397" x2="23.15718125" y2="1.48158125" layer="21"/>
<rectangle x1="23.495" y1="1.397" x2="23.749" y2="1.48158125" layer="21"/>
<rectangle x1="25.18918125" y1="1.397" x2="25.44318125" y2="1.48158125" layer="21"/>
<rectangle x1="25.69718125" y1="1.397" x2="25.95118125" y2="1.48158125" layer="21"/>
<rectangle x1="3.51281875" y1="1.48158125" x2="3.76681875" y2="1.56641875" layer="21"/>
<rectangle x1="5.969" y1="1.48158125" x2="6.30681875" y2="1.56641875" layer="21"/>
<rectangle x1="6.64718125" y1="1.48158125" x2="6.90118125" y2="1.56641875" layer="21"/>
<rectangle x1="8.33881875" y1="1.48158125" x2="8.59281875" y2="1.56641875" layer="21"/>
<rectangle x1="8.84681875" y1="1.48158125" x2="9.18718125" y2="1.56641875" layer="21"/>
<rectangle x1="9.69518125" y1="1.48158125" x2="10.033" y2="1.56641875" layer="21"/>
<rectangle x1="11.303" y1="1.48158125" x2="11.557" y2="1.56641875" layer="21"/>
<rectangle x1="13.41881875" y1="1.48158125" x2="13.75918125" y2="1.56641875" layer="21"/>
<rectangle x1="14.097" y1="1.48158125" x2="14.351" y2="1.56641875" layer="21"/>
<rectangle x1="15.79118125" y1="1.48158125" x2="16.04518125" y2="1.56641875" layer="21"/>
<rectangle x1="16.97481875" y1="1.48158125" x2="17.56918125" y2="1.56641875" layer="21"/>
<rectangle x1="17.907" y1="1.48158125" x2="19.76881875" y2="1.56641875" layer="21"/>
<rectangle x1="20.447" y1="1.48158125" x2="20.701" y2="1.56641875" layer="21"/>
<rectangle x1="22.90318125" y1="1.48158125" x2="23.241" y2="1.56641875" layer="21"/>
<rectangle x1="23.495" y1="1.48158125" x2="23.749" y2="1.56641875" layer="21"/>
<rectangle x1="25.18918125" y1="1.48158125" x2="25.44318125" y2="1.56641875" layer="21"/>
<rectangle x1="25.69718125" y1="1.48158125" x2="27.559" y2="1.56641875" layer="21"/>
<rectangle x1="3.429" y1="1.56641875" x2="3.76681875" y2="1.651" layer="21"/>
<rectangle x1="5.969" y1="1.56641875" x2="6.30681875" y2="1.651" layer="21"/>
<rectangle x1="6.64718125" y1="1.56641875" x2="6.90118125" y2="1.651" layer="21"/>
<rectangle x1="8.33881875" y1="1.56641875" x2="8.59281875" y2="1.651" layer="21"/>
<rectangle x1="8.84681875" y1="1.56641875" x2="9.18718125" y2="1.651" layer="21"/>
<rectangle x1="9.69518125" y1="1.56641875" x2="10.033" y2="1.651" layer="21"/>
<rectangle x1="11.303" y1="1.56641875" x2="11.557" y2="1.651" layer="21"/>
<rectangle x1="13.50518125" y1="1.56641875" x2="13.75918125" y2="1.651" layer="21"/>
<rectangle x1="14.097" y1="1.56641875" x2="14.351" y2="1.651" layer="21"/>
<rectangle x1="15.79118125" y1="1.56641875" x2="16.04518125" y2="1.651" layer="21"/>
<rectangle x1="16.72081875" y1="1.56641875" x2="17.48281875" y2="1.651" layer="21"/>
<rectangle x1="17.907" y1="1.56641875" x2="19.85518125" y2="1.651" layer="21"/>
<rectangle x1="20.36318125" y1="1.56641875" x2="20.701" y2="1.651" layer="21"/>
<rectangle x1="22.90318125" y1="1.56641875" x2="23.241" y2="1.651" layer="21"/>
<rectangle x1="23.495" y1="1.56641875" x2="23.749" y2="1.651" layer="21"/>
<rectangle x1="25.18918125" y1="1.56641875" x2="25.44318125" y2="1.651" layer="21"/>
<rectangle x1="25.61081875" y1="1.56641875" x2="27.64281875" y2="1.651" layer="21"/>
<rectangle x1="3.429" y1="1.651" x2="3.76681875" y2="1.73558125" layer="21"/>
<rectangle x1="5.969" y1="1.651" x2="6.30681875" y2="1.73558125" layer="21"/>
<rectangle x1="6.64718125" y1="1.651" x2="6.90118125" y2="1.73558125" layer="21"/>
<rectangle x1="8.33881875" y1="1.651" x2="8.59281875" y2="1.73558125" layer="21"/>
<rectangle x1="8.84681875" y1="1.651" x2="9.18718125" y2="1.73558125" layer="21"/>
<rectangle x1="9.69518125" y1="1.651" x2="10.033" y2="1.73558125" layer="21"/>
<rectangle x1="11.303" y1="1.651" x2="11.557" y2="1.73558125" layer="21"/>
<rectangle x1="13.50518125" y1="1.651" x2="13.843" y2="1.73558125" layer="21"/>
<rectangle x1="14.097" y1="1.651" x2="14.351" y2="1.73558125" layer="21"/>
<rectangle x1="15.79118125" y1="1.651" x2="16.04518125" y2="1.73558125" layer="21"/>
<rectangle x1="16.55318125" y1="1.651" x2="17.31518125" y2="1.73558125" layer="21"/>
<rectangle x1="17.907" y1="1.651" x2="19.939" y2="1.73558125" layer="21"/>
<rectangle x1="20.36318125" y1="1.651" x2="20.701" y2="1.73558125" layer="21"/>
<rectangle x1="22.90318125" y1="1.651" x2="23.241" y2="1.73558125" layer="21"/>
<rectangle x1="23.495" y1="1.651" x2="23.749" y2="1.73558125" layer="21"/>
<rectangle x1="25.18918125" y1="1.651" x2="25.44318125" y2="1.73558125" layer="21"/>
<rectangle x1="25.61081875" y1="1.651" x2="27.64281875" y2="1.73558125" layer="21"/>
<rectangle x1="3.429" y1="1.73558125" x2="3.683" y2="1.82041875" layer="21"/>
<rectangle x1="6.05281875" y1="1.73558125" x2="6.30681875" y2="1.82041875" layer="21"/>
<rectangle x1="6.64718125" y1="1.73558125" x2="6.90118125" y2="1.82041875" layer="21"/>
<rectangle x1="8.33881875" y1="1.73558125" x2="8.59281875" y2="1.82041875" layer="21"/>
<rectangle x1="8.84681875" y1="1.73558125" x2="9.18718125" y2="1.82041875" layer="21"/>
<rectangle x1="9.69518125" y1="1.73558125" x2="10.033" y2="1.82041875" layer="21"/>
<rectangle x1="11.303" y1="1.73558125" x2="11.557" y2="1.82041875" layer="21"/>
<rectangle x1="13.50518125" y1="1.73558125" x2="13.843" y2="1.82041875" layer="21"/>
<rectangle x1="14.097" y1="1.73558125" x2="14.351" y2="1.82041875" layer="21"/>
<rectangle x1="15.79118125" y1="1.73558125" x2="16.04518125" y2="1.82041875" layer="21"/>
<rectangle x1="16.383" y1="1.73558125" x2="17.145" y2="1.82041875" layer="21"/>
<rectangle x1="17.907" y1="1.73558125" x2="19.85518125" y2="1.82041875" layer="21"/>
<rectangle x1="20.36318125" y1="1.73558125" x2="20.61718125" y2="1.82041875" layer="21"/>
<rectangle x1="22.987" y1="1.73558125" x2="23.241" y2="1.82041875" layer="21"/>
<rectangle x1="23.495" y1="1.73558125" x2="23.749" y2="1.82041875" layer="21"/>
<rectangle x1="25.18918125" y1="1.73558125" x2="25.44318125" y2="1.82041875" layer="21"/>
<rectangle x1="25.69718125" y1="1.73558125" x2="27.64281875" y2="1.82041875" layer="21"/>
<rectangle x1="3.429" y1="1.82041875" x2="3.683" y2="1.905" layer="21"/>
<rectangle x1="6.05281875" y1="1.82041875" x2="6.30681875" y2="1.905" layer="21"/>
<rectangle x1="6.64718125" y1="1.82041875" x2="6.90118125" y2="1.905" layer="21"/>
<rectangle x1="8.33881875" y1="1.82041875" x2="8.59281875" y2="1.905" layer="21"/>
<rectangle x1="8.84681875" y1="1.82041875" x2="9.18718125" y2="1.905" layer="21"/>
<rectangle x1="9.69518125" y1="1.82041875" x2="10.033" y2="1.905" layer="21"/>
<rectangle x1="11.303" y1="1.82041875" x2="11.557" y2="1.905" layer="21"/>
<rectangle x1="13.589" y1="1.82041875" x2="13.843" y2="1.905" layer="21"/>
<rectangle x1="14.097" y1="1.82041875" x2="14.43481875" y2="1.905" layer="21"/>
<rectangle x1="15.79118125" y1="1.82041875" x2="16.04518125" y2="1.905" layer="21"/>
<rectangle x1="16.383" y1="1.82041875" x2="16.891" y2="1.905" layer="21"/>
<rectangle x1="17.907" y1="1.82041875" x2="18.24481875" y2="1.905" layer="21"/>
<rectangle x1="19.60118125" y1="1.82041875" x2="19.85518125" y2="1.905" layer="21"/>
<rectangle x1="20.36318125" y1="1.82041875" x2="20.61718125" y2="1.905" layer="21"/>
<rectangle x1="22.987" y1="1.82041875" x2="23.241" y2="1.905" layer="21"/>
<rectangle x1="23.495" y1="1.82041875" x2="23.749" y2="1.905" layer="21"/>
<rectangle x1="25.10281875" y1="1.82041875" x2="25.44318125" y2="1.905" layer="21"/>
<rectangle x1="25.69718125" y1="1.82041875" x2="25.95118125" y2="1.905" layer="21"/>
<rectangle x1="27.305" y1="1.82041875" x2="27.64281875" y2="1.905" layer="21"/>
<rectangle x1="3.429" y1="1.905" x2="3.683" y2="1.98958125" layer="21"/>
<rectangle x1="6.05281875" y1="1.905" x2="6.30681875" y2="1.98958125" layer="21"/>
<rectangle x1="6.64718125" y1="1.905" x2="6.985" y2="1.98958125" layer="21"/>
<rectangle x1="8.255" y1="1.905" x2="8.59281875" y2="1.98958125" layer="21"/>
<rectangle x1="8.84681875" y1="1.905" x2="9.18718125" y2="1.98958125" layer="21"/>
<rectangle x1="9.69518125" y1="1.905" x2="10.033" y2="1.98958125" layer="21"/>
<rectangle x1="11.303" y1="1.905" x2="11.557" y2="1.98958125" layer="21"/>
<rectangle x1="13.589" y1="1.905" x2="13.843" y2="1.98958125" layer="21"/>
<rectangle x1="14.097" y1="1.905" x2="14.43481875" y2="1.98958125" layer="21"/>
<rectangle x1="15.70481875" y1="1.905" x2="16.04518125" y2="1.98958125" layer="21"/>
<rectangle x1="16.29918125" y1="1.905" x2="16.72081875" y2="1.98958125" layer="21"/>
<rectangle x1="17.907" y1="1.905" x2="18.24481875" y2="1.98958125" layer="21"/>
<rectangle x1="19.51481875" y1="1.905" x2="19.85518125" y2="1.98958125" layer="21"/>
<rectangle x1="20.36318125" y1="1.905" x2="20.61718125" y2="1.98958125" layer="21"/>
<rectangle x1="22.987" y1="1.905" x2="23.241" y2="1.98958125" layer="21"/>
<rectangle x1="23.495" y1="1.905" x2="23.83281875" y2="1.98958125" layer="21"/>
<rectangle x1="25.10281875" y1="1.905" x2="25.44318125" y2="1.98958125" layer="21"/>
<rectangle x1="25.69718125" y1="1.905" x2="26.035" y2="1.98958125" layer="21"/>
<rectangle x1="27.305" y1="1.905" x2="27.64281875" y2="1.98958125" layer="21"/>
<rectangle x1="3.429" y1="1.98958125" x2="3.683" y2="2.07441875" layer="21"/>
<rectangle x1="6.05281875" y1="1.98958125" x2="6.30681875" y2="2.07441875" layer="21"/>
<rectangle x1="6.64718125" y1="1.98958125" x2="6.985" y2="2.07441875" layer="21"/>
<rectangle x1="8.255" y1="1.98958125" x2="8.509" y2="2.07441875" layer="21"/>
<rectangle x1="8.84681875" y1="1.98958125" x2="9.18718125" y2="2.07441875" layer="21"/>
<rectangle x1="9.69518125" y1="1.98958125" x2="10.033" y2="2.07441875" layer="21"/>
<rectangle x1="11.303" y1="1.98958125" x2="11.557" y2="2.07441875" layer="21"/>
<rectangle x1="13.589" y1="1.98958125" x2="13.843" y2="2.07441875" layer="21"/>
<rectangle x1="14.18081875" y1="1.98958125" x2="14.43481875" y2="2.07441875" layer="21"/>
<rectangle x1="15.70481875" y1="1.98958125" x2="16.04518125" y2="2.07441875" layer="21"/>
<rectangle x1="16.29918125" y1="1.98958125" x2="16.55318125" y2="2.07441875" layer="21"/>
<rectangle x1="17.99081875" y1="1.98958125" x2="18.24481875" y2="2.07441875" layer="21"/>
<rectangle x1="19.51481875" y1="1.98958125" x2="19.85518125" y2="2.07441875" layer="21"/>
<rectangle x1="20.36318125" y1="1.98958125" x2="20.61718125" y2="2.07441875" layer="21"/>
<rectangle x1="22.987" y1="1.98958125" x2="23.241" y2="2.07441875" layer="21"/>
<rectangle x1="23.495" y1="1.98958125" x2="23.83281875" y2="2.07441875" layer="21"/>
<rectangle x1="25.019" y1="1.98958125" x2="25.35681875" y2="2.07441875" layer="21"/>
<rectangle x1="25.69718125" y1="1.98958125" x2="26.035" y2="2.07441875" layer="21"/>
<rectangle x1="27.22118125" y1="1.98958125" x2="27.559" y2="2.07441875" layer="21"/>
<rectangle x1="3.429" y1="2.07441875" x2="3.683" y2="2.159" layer="21"/>
<rectangle x1="6.05281875" y1="2.07441875" x2="6.30681875" y2="2.159" layer="21"/>
<rectangle x1="6.64718125" y1="2.07441875" x2="7.06881875" y2="2.159" layer="21"/>
<rectangle x1="8.17118125" y1="2.07441875" x2="8.509" y2="2.159" layer="21"/>
<rectangle x1="8.84681875" y1="2.07441875" x2="9.18718125" y2="2.159" layer="21"/>
<rectangle x1="9.69518125" y1="2.07441875" x2="10.033" y2="2.159" layer="21"/>
<rectangle x1="11.303" y1="2.07441875" x2="11.557" y2="2.159" layer="21"/>
<rectangle x1="13.589" y1="2.07441875" x2="13.92681875" y2="2.159" layer="21"/>
<rectangle x1="14.18081875" y1="2.07441875" x2="14.52118125" y2="2.159" layer="21"/>
<rectangle x1="15.621" y1="2.07441875" x2="15.95881875" y2="2.159" layer="21"/>
<rectangle x1="16.29918125" y1="2.07441875" x2="16.55318125" y2="2.159" layer="21"/>
<rectangle x1="17.399" y1="2.07441875" x2="17.653" y2="2.159" layer="21"/>
<rectangle x1="17.99081875" y1="2.07441875" x2="18.33118125" y2="2.159" layer="21"/>
<rectangle x1="19.431" y1="2.07441875" x2="19.76881875" y2="2.159" layer="21"/>
<rectangle x1="20.36318125" y1="2.07441875" x2="20.61718125" y2="2.159" layer="21"/>
<rectangle x1="22.987" y1="2.07441875" x2="23.241" y2="2.159" layer="21"/>
<rectangle x1="23.495" y1="2.07441875" x2="23.91918125" y2="2.159" layer="21"/>
<rectangle x1="25.019" y1="2.07441875" x2="25.35681875" y2="2.159" layer="21"/>
<rectangle x1="25.781" y1="2.07441875" x2="26.11881875" y2="2.159" layer="21"/>
<rectangle x1="27.22118125" y1="2.07441875" x2="27.559" y2="2.159" layer="21"/>
<rectangle x1="3.429" y1="2.159" x2="3.683" y2="2.24358125" layer="21"/>
<rectangle x1="6.05281875" y1="2.159" x2="6.30681875" y2="2.24358125" layer="21"/>
<rectangle x1="6.64718125" y1="2.159" x2="7.15518125" y2="2.24358125" layer="21"/>
<rectangle x1="8.08481875" y1="2.159" x2="8.42518125" y2="2.24358125" layer="21"/>
<rectangle x1="8.84681875" y1="2.159" x2="9.18718125" y2="2.24358125" layer="21"/>
<rectangle x1="9.69518125" y1="2.159" x2="10.033" y2="2.24358125" layer="21"/>
<rectangle x1="11.303" y1="2.159" x2="11.557" y2="2.24358125" layer="21"/>
<rectangle x1="13.589" y1="2.159" x2="13.92681875" y2="2.24358125" layer="21"/>
<rectangle x1="14.26718125" y1="2.159" x2="14.605" y2="2.24358125" layer="21"/>
<rectangle x1="15.53718125" y1="2.159" x2="15.95881875" y2="2.24358125" layer="21"/>
<rectangle x1="16.29918125" y1="2.159" x2="16.637" y2="2.24358125" layer="21"/>
<rectangle x1="17.31518125" y1="2.159" x2="17.653" y2="2.24358125" layer="21"/>
<rectangle x1="18.07718125" y1="2.159" x2="18.415" y2="2.24358125" layer="21"/>
<rectangle x1="19.34718125" y1="2.159" x2="19.76881875" y2="2.24358125" layer="21"/>
<rectangle x1="20.36318125" y1="2.159" x2="20.61718125" y2="2.24358125" layer="21"/>
<rectangle x1="22.987" y1="2.159" x2="23.241" y2="2.24358125" layer="21"/>
<rectangle x1="23.495" y1="2.159" x2="24.003" y2="2.24358125" layer="21"/>
<rectangle x1="24.93518125" y1="2.159" x2="25.273" y2="2.24358125" layer="21"/>
<rectangle x1="25.781" y1="2.159" x2="26.20518125" y2="2.24358125" layer="21"/>
<rectangle x1="27.13481875" y1="2.159" x2="27.47518125" y2="2.24358125" layer="21"/>
<rectangle x1="3.429" y1="2.24358125" x2="3.683" y2="2.32841875" layer="21"/>
<rectangle x1="6.05281875" y1="2.24358125" x2="6.30681875" y2="2.32841875" layer="21"/>
<rectangle x1="6.64718125" y1="2.24358125" x2="7.32281875" y2="2.32841875" layer="21"/>
<rectangle x1="7.91718125" y1="2.24358125" x2="8.42518125" y2="2.32841875" layer="21"/>
<rectangle x1="8.84681875" y1="2.24358125" x2="9.18718125" y2="2.32841875" layer="21"/>
<rectangle x1="9.69518125" y1="2.24358125" x2="10.033" y2="2.32841875" layer="21"/>
<rectangle x1="11.303" y1="2.24358125" x2="11.557" y2="2.32841875" layer="21"/>
<rectangle x1="13.589" y1="2.24358125" x2="13.843" y2="2.32841875" layer="21"/>
<rectangle x1="14.26718125" y1="2.24358125" x2="14.77518125" y2="2.32841875" layer="21"/>
<rectangle x1="15.45081875" y1="2.24358125" x2="15.875" y2="2.32841875" layer="21"/>
<rectangle x1="16.29918125" y1="2.24358125" x2="16.72081875" y2="2.32841875" layer="21"/>
<rectangle x1="17.22881875" y1="2.24358125" x2="17.653" y2="2.32841875" layer="21"/>
<rectangle x1="18.07718125" y1="2.24358125" x2="18.58518125" y2="2.32841875" layer="21"/>
<rectangle x1="19.177" y1="2.24358125" x2="19.685" y2="2.32841875" layer="21"/>
<rectangle x1="20.36318125" y1="2.24358125" x2="20.701" y2="2.32841875" layer="21"/>
<rectangle x1="22.90318125" y1="2.24358125" x2="23.241" y2="2.32841875" layer="21"/>
<rectangle x1="23.495" y1="2.24358125" x2="24.17318125" y2="2.32841875" layer="21"/>
<rectangle x1="24.765" y1="2.24358125" x2="25.273" y2="2.32841875" layer="21"/>
<rectangle x1="25.86481875" y1="2.24358125" x2="26.289" y2="2.32841875" layer="21"/>
<rectangle x1="26.96718125" y1="2.24358125" x2="27.47518125" y2="2.32841875" layer="21"/>
<rectangle x1="3.429" y1="2.32841875" x2="3.683" y2="2.413" layer="21"/>
<rectangle x1="6.05281875" y1="2.32841875" x2="6.30681875" y2="2.413" layer="21"/>
<rectangle x1="6.64718125" y1="2.32841875" x2="7.493" y2="2.413" layer="21"/>
<rectangle x1="7.66318125" y1="2.32841875" x2="8.33881875" y2="2.413" layer="21"/>
<rectangle x1="8.84681875" y1="2.32841875" x2="9.18718125" y2="2.413" layer="21"/>
<rectangle x1="9.69518125" y1="2.32841875" x2="10.033" y2="2.413" layer="21"/>
<rectangle x1="11.303" y1="2.32841875" x2="11.557" y2="2.413" layer="21"/>
<rectangle x1="13.589" y1="2.32841875" x2="13.843" y2="2.413" layer="21"/>
<rectangle x1="14.351" y1="2.32841875" x2="15.02918125" y2="2.413" layer="21"/>
<rectangle x1="15.19681875" y1="2.32841875" x2="15.79118125" y2="2.413" layer="21"/>
<rectangle x1="16.383" y1="2.32841875" x2="16.891" y2="2.413" layer="21"/>
<rectangle x1="17.06118125" y1="2.32841875" x2="17.56918125" y2="2.413" layer="21"/>
<rectangle x1="18.161" y1="2.32841875" x2="18.83918125" y2="2.413" layer="21"/>
<rectangle x1="19.00681875" y1="2.32841875" x2="19.60118125" y2="2.413" layer="21"/>
<rectangle x1="20.36318125" y1="2.32841875" x2="20.701" y2="2.413" layer="21"/>
<rectangle x1="22.90318125" y1="2.32841875" x2="23.241" y2="2.413" layer="21"/>
<rectangle x1="23.495" y1="2.32841875" x2="24.34081875" y2="2.413" layer="21"/>
<rectangle x1="24.511" y1="2.32841875" x2="25.18918125" y2="2.413" layer="21"/>
<rectangle x1="25.95118125" y1="2.32841875" x2="26.543" y2="2.413" layer="21"/>
<rectangle x1="26.71318125" y1="2.32841875" x2="27.38881875" y2="2.413" layer="21"/>
<rectangle x1="3.429" y1="2.413" x2="3.683" y2="2.49758125" layer="21"/>
<rectangle x1="6.05281875" y1="2.413" x2="6.30681875" y2="2.49758125" layer="21"/>
<rectangle x1="6.64718125" y1="2.413" x2="6.90118125" y2="2.49758125" layer="21"/>
<rectangle x1="6.985" y1="2.413" x2="8.255" y2="2.49758125" layer="21"/>
<rectangle x1="8.84681875" y1="2.413" x2="9.18718125" y2="2.49758125" layer="21"/>
<rectangle x1="9.35481875" y1="2.413" x2="10.795" y2="2.49758125" layer="21"/>
<rectangle x1="11.303" y1="2.413" x2="11.557" y2="2.49758125" layer="21"/>
<rectangle x1="13.50518125" y1="2.413" x2="13.843" y2="2.49758125" layer="21"/>
<rectangle x1="14.43481875" y1="2.413" x2="15.70481875" y2="2.49758125" layer="21"/>
<rectangle x1="16.46681875" y1="2.413" x2="17.48281875" y2="2.49758125" layer="21"/>
<rectangle x1="18.24481875" y1="2.413" x2="19.51481875" y2="2.49758125" layer="21"/>
<rectangle x1="20.447" y1="2.413" x2="20.701" y2="2.49758125" layer="21"/>
<rectangle x1="22.90318125" y1="2.413" x2="23.241" y2="2.49758125" layer="21"/>
<rectangle x1="23.495" y1="2.413" x2="23.749" y2="2.49758125" layer="21"/>
<rectangle x1="23.83281875" y1="2.413" x2="25.10281875" y2="2.49758125" layer="21"/>
<rectangle x1="26.035" y1="2.413" x2="27.305" y2="2.49758125" layer="21"/>
<rectangle x1="3.429" y1="2.49758125" x2="3.683" y2="2.58241875" layer="21"/>
<rectangle x1="6.05281875" y1="2.49758125" x2="6.30681875" y2="2.58241875" layer="21"/>
<rectangle x1="6.64718125" y1="2.49758125" x2="6.90118125" y2="2.58241875" layer="21"/>
<rectangle x1="7.15518125" y1="2.49758125" x2="8.08481875" y2="2.58241875" layer="21"/>
<rectangle x1="8.84681875" y1="2.49758125" x2="9.18718125" y2="2.58241875" layer="21"/>
<rectangle x1="9.35481875" y1="2.49758125" x2="10.87881875" y2="2.58241875" layer="21"/>
<rectangle x1="11.303" y1="2.49758125" x2="11.557" y2="2.58241875" layer="21"/>
<rectangle x1="13.50518125" y1="2.49758125" x2="13.843" y2="2.58241875" layer="21"/>
<rectangle x1="14.605" y1="2.49758125" x2="15.53718125" y2="2.58241875" layer="21"/>
<rectangle x1="16.55318125" y1="2.49758125" x2="17.399" y2="2.58241875" layer="21"/>
<rectangle x1="18.415" y1="2.49758125" x2="19.34718125" y2="2.58241875" layer="21"/>
<rectangle x1="20.447" y1="2.49758125" x2="20.78481875" y2="2.58241875" layer="21"/>
<rectangle x1="22.81681875" y1="2.49758125" x2="23.15718125" y2="2.58241875" layer="21"/>
<rectangle x1="23.495" y1="2.49758125" x2="23.749" y2="2.58241875" layer="21"/>
<rectangle x1="24.003" y1="2.49758125" x2="24.93518125" y2="2.58241875" layer="21"/>
<rectangle x1="26.20518125" y1="2.49758125" x2="27.13481875" y2="2.58241875" layer="21"/>
<rectangle x1="3.429" y1="2.58241875" x2="3.683" y2="2.667" layer="21"/>
<rectangle x1="6.05281875" y1="2.58241875" x2="6.30681875" y2="2.667" layer="21"/>
<rectangle x1="6.64718125" y1="2.58241875" x2="6.81481875" y2="2.667" layer="21"/>
<rectangle x1="7.32281875" y1="2.58241875" x2="7.91718125" y2="2.667" layer="21"/>
<rectangle x1="8.93318125" y1="2.58241875" x2="9.10081875" y2="2.667" layer="21"/>
<rectangle x1="9.44118125" y1="2.58241875" x2="10.795" y2="2.667" layer="21"/>
<rectangle x1="11.303" y1="2.58241875" x2="11.557" y2="2.667" layer="21"/>
<rectangle x1="13.50518125" y1="2.58241875" x2="13.843" y2="2.667" layer="21"/>
<rectangle x1="14.77518125" y1="2.58241875" x2="15.367" y2="2.667" layer="21"/>
<rectangle x1="16.72081875" y1="2.58241875" x2="17.22881875" y2="2.667" layer="21"/>
<rectangle x1="18.58518125" y1="2.58241875" x2="19.177" y2="2.667" layer="21"/>
<rectangle x1="20.447" y1="2.58241875" x2="20.78481875" y2="2.667" layer="21"/>
<rectangle x1="22.81681875" y1="2.58241875" x2="23.15718125" y2="2.667" layer="21"/>
<rectangle x1="23.495" y1="2.58241875" x2="23.66518125" y2="2.667" layer="21"/>
<rectangle x1="24.17318125" y1="2.58241875" x2="24.765" y2="2.667" layer="21"/>
<rectangle x1="26.37281875" y1="2.58241875" x2="26.96718125" y2="2.667" layer="21"/>
<rectangle x1="3.429" y1="2.667" x2="3.683" y2="2.75158125" layer="21"/>
<rectangle x1="6.05281875" y1="2.667" x2="6.30681875" y2="2.75158125" layer="21"/>
<rectangle x1="9.69518125" y1="2.667" x2="10.033" y2="2.75158125" layer="21"/>
<rectangle x1="11.303" y1="2.667" x2="11.557" y2="2.75158125" layer="21"/>
<rectangle x1="13.41881875" y1="2.667" x2="13.75918125" y2="2.75158125" layer="21"/>
<rectangle x1="20.53081875" y1="2.667" x2="20.87118125" y2="2.75158125" layer="21"/>
<rectangle x1="22.733" y1="2.667" x2="23.07081875" y2="2.75158125" layer="21"/>
<rectangle x1="3.429" y1="2.75158125" x2="3.683" y2="2.83641875" layer="21"/>
<rectangle x1="6.05281875" y1="2.75158125" x2="6.30681875" y2="2.83641875" layer="21"/>
<rectangle x1="8.93318125" y1="2.75158125" x2="9.10081875" y2="2.83641875" layer="21"/>
<rectangle x1="9.69518125" y1="2.75158125" x2="10.033" y2="2.83641875" layer="21"/>
<rectangle x1="11.303" y1="2.75158125" x2="11.557" y2="2.83641875" layer="21"/>
<rectangle x1="13.41881875" y1="2.75158125" x2="13.75918125" y2="2.83641875" layer="21"/>
<rectangle x1="20.61718125" y1="2.75158125" x2="20.955" y2="2.83641875" layer="21"/>
<rectangle x1="22.64918125" y1="2.75158125" x2="23.07081875" y2="2.83641875" layer="21"/>
<rectangle x1="3.429" y1="2.83641875" x2="3.683" y2="2.921" layer="21"/>
<rectangle x1="6.05281875" y1="2.83641875" x2="6.30681875" y2="2.921" layer="21"/>
<rectangle x1="8.84681875" y1="2.83641875" x2="9.18718125" y2="2.921" layer="21"/>
<rectangle x1="9.69518125" y1="2.83641875" x2="10.033" y2="2.921" layer="21"/>
<rectangle x1="11.303" y1="2.83641875" x2="11.557" y2="2.921" layer="21"/>
<rectangle x1="13.335" y1="2.83641875" x2="13.67281875" y2="2.921" layer="21"/>
<rectangle x1="20.61718125" y1="2.83641875" x2="21.03881875" y2="2.921" layer="21"/>
<rectangle x1="22.56281875" y1="2.83641875" x2="22.987" y2="2.921" layer="21"/>
<rectangle x1="3.429" y1="2.921" x2="3.683" y2="3.00558125" layer="21"/>
<rectangle x1="6.05281875" y1="2.921" x2="6.30681875" y2="3.00558125" layer="21"/>
<rectangle x1="8.84681875" y1="2.921" x2="9.18718125" y2="3.00558125" layer="21"/>
<rectangle x1="9.69518125" y1="2.921" x2="10.033" y2="3.00558125" layer="21"/>
<rectangle x1="11.303" y1="2.921" x2="11.557" y2="3.00558125" layer="21"/>
<rectangle x1="13.25118125" y1="2.921" x2="13.589" y2="3.00558125" layer="21"/>
<rectangle x1="20.701" y1="2.921" x2="21.12518125" y2="3.00558125" layer="21"/>
<rectangle x1="22.479" y1="2.921" x2="22.90318125" y2="3.00558125" layer="21"/>
<rectangle x1="3.429" y1="3.00558125" x2="3.683" y2="3.09041875" layer="21"/>
<rectangle x1="6.05281875" y1="3.00558125" x2="6.30681875" y2="3.09041875" layer="21"/>
<rectangle x1="8.93318125" y1="3.00558125" x2="9.18718125" y2="3.09041875" layer="21"/>
<rectangle x1="9.69518125" y1="3.00558125" x2="10.033" y2="3.09041875" layer="21"/>
<rectangle x1="11.303" y1="3.00558125" x2="11.557" y2="3.09041875" layer="21"/>
<rectangle x1="13.16481875" y1="3.00558125" x2="13.589" y2="3.09041875" layer="21"/>
<rectangle x1="20.78481875" y1="3.00558125" x2="21.29281875" y2="3.09041875" layer="21"/>
<rectangle x1="22.30881875" y1="3.00558125" x2="22.81681875" y2="3.09041875" layer="21"/>
<rectangle x1="3.429" y1="3.09041875" x2="3.683" y2="3.175" layer="21"/>
<rectangle x1="6.05281875" y1="3.09041875" x2="6.39318125" y2="3.175" layer="21"/>
<rectangle x1="9.69518125" y1="3.09041875" x2="10.033" y2="3.175" layer="21"/>
<rectangle x1="11.303" y1="3.09041875" x2="11.557" y2="3.175" layer="21"/>
<rectangle x1="12.99718125" y1="3.09041875" x2="13.50518125" y2="3.175" layer="21"/>
<rectangle x1="20.87118125" y1="3.09041875" x2="21.54681875" y2="3.175" layer="21"/>
<rectangle x1="22.05481875" y1="3.09041875" x2="22.733" y2="3.175" layer="21"/>
<rectangle x1="3.429" y1="3.175" x2="3.683" y2="3.25958125" layer="21"/>
<rectangle x1="6.05281875" y1="3.175" x2="6.30681875" y2="3.25958125" layer="21"/>
<rectangle x1="9.69518125" y1="3.175" x2="10.033" y2="3.25958125" layer="21"/>
<rectangle x1="11.303" y1="3.175" x2="11.557" y2="3.25958125" layer="21"/>
<rectangle x1="12.827" y1="3.175" x2="13.41881875" y2="3.25958125" layer="21"/>
<rectangle x1="20.955" y1="3.175" x2="22.64918125" y2="3.25958125" layer="21"/>
<rectangle x1="3.429" y1="3.25958125" x2="3.683" y2="3.34441875" layer="21"/>
<rectangle x1="6.05281875" y1="3.25958125" x2="6.30681875" y2="3.34441875" layer="21"/>
<rectangle x1="9.69518125" y1="3.25958125" x2="10.033" y2="3.34441875" layer="21"/>
<rectangle x1="11.303" y1="3.25958125" x2="13.335" y2="3.34441875" layer="21"/>
<rectangle x1="21.12518125" y1="3.25958125" x2="22.479" y2="3.34441875" layer="21"/>
<rectangle x1="3.429" y1="3.34441875" x2="3.683" y2="3.429" layer="21"/>
<rectangle x1="6.05281875" y1="3.34441875" x2="6.30681875" y2="3.429" layer="21"/>
<rectangle x1="9.69518125" y1="3.34441875" x2="9.94918125" y2="3.429" layer="21"/>
<rectangle x1="11.303" y1="3.34441875" x2="13.16481875" y2="3.429" layer="21"/>
<rectangle x1="21.29281875" y1="3.34441875" x2="22.225" y2="3.429" layer="21"/>
<rectangle x1="11.303" y1="3.429" x2="12.99718125" y2="3.51358125" layer="21"/>
<rectangle x1="11.303" y1="3.51358125" x2="12.827" y2="3.59841875" layer="21"/>
<rectangle x1="5.63118125" y1="4.86841875" x2="24.59481875" y2="4.953" layer="21"/>
<rectangle x1="5.03681875" y1="4.953" x2="25.18918125" y2="5.03758125" layer="21"/>
<rectangle x1="4.699" y1="5.03758125" x2="25.527" y2="5.12241875" layer="21"/>
<rectangle x1="4.36118125" y1="5.12241875" x2="25.86481875" y2="5.207" layer="21"/>
<rectangle x1="4.10718125" y1="5.207" x2="26.11881875" y2="5.29158125" layer="21"/>
<rectangle x1="3.85318125" y1="5.29158125" x2="26.289" y2="5.37641875" layer="21"/>
<rectangle x1="3.683" y1="5.37641875" x2="26.543" y2="5.461" layer="21"/>
<rectangle x1="3.51281875" y1="5.461" x2="26.71318125" y2="5.54558125" layer="21"/>
<rectangle x1="3.34518125" y1="5.54558125" x2="26.88081875" y2="5.63041875" layer="21"/>
<rectangle x1="3.175" y1="5.63041875" x2="27.051" y2="5.715" layer="21"/>
<rectangle x1="3.00481875" y1="5.715" x2="27.13481875" y2="5.79958125" layer="21"/>
<rectangle x1="2.921" y1="5.79958125" x2="5.29081875" y2="5.88441875" layer="21"/>
<rectangle x1="14.77518125" y1="5.79958125" x2="27.305" y2="5.88441875" layer="21"/>
<rectangle x1="2.75081875" y1="5.88441875" x2="4.86918125" y2="5.969" layer="21"/>
<rectangle x1="14.68881875" y1="5.88441875" x2="27.38881875" y2="5.969" layer="21"/>
<rectangle x1="2.667" y1="5.969" x2="4.52881875" y2="6.05358125" layer="21"/>
<rectangle x1="14.605" y1="5.969" x2="27.559" y2="6.05358125" layer="21"/>
<rectangle x1="2.49681875" y1="6.05358125" x2="4.36118125" y2="6.13841875" layer="21"/>
<rectangle x1="14.605" y1="6.05358125" x2="27.64281875" y2="6.13841875" layer="21"/>
<rectangle x1="2.413" y1="6.13841875" x2="4.10718125" y2="6.223" layer="21"/>
<rectangle x1="14.52118125" y1="6.13841875" x2="27.72918125" y2="6.223" layer="21"/>
<rectangle x1="2.32918125" y1="6.223" x2="3.937" y2="6.30758125" layer="21"/>
<rectangle x1="14.52118125" y1="6.223" x2="27.89681875" y2="6.30758125" layer="21"/>
<rectangle x1="2.24281875" y1="6.30758125" x2="3.76681875" y2="6.39241875" layer="21"/>
<rectangle x1="14.43481875" y1="6.30758125" x2="27.98318125" y2="6.39241875" layer="21"/>
<rectangle x1="2.159" y1="6.39241875" x2="3.59918125" y2="6.477" layer="21"/>
<rectangle x1="14.43481875" y1="6.39241875" x2="28.067" y2="6.477" layer="21"/>
<rectangle x1="2.07518125" y1="6.477" x2="3.429" y2="6.56158125" layer="21"/>
<rectangle x1="14.351" y1="6.477" x2="28.15081875" y2="6.56158125" layer="21"/>
<rectangle x1="1.905" y1="6.56158125" x2="3.34518125" y2="6.64641875" layer="21"/>
<rectangle x1="14.351" y1="6.56158125" x2="28.23718125" y2="6.64641875" layer="21"/>
<rectangle x1="1.905" y1="6.64641875" x2="3.175" y2="6.731" layer="21"/>
<rectangle x1="14.26718125" y1="6.64641875" x2="28.321" y2="6.731" layer="21"/>
<rectangle x1="1.73481875" y1="6.731" x2="3.09118125" y2="6.81558125" layer="21"/>
<rectangle x1="14.26718125" y1="6.731" x2="28.40481875" y2="6.81558125" layer="21"/>
<rectangle x1="1.73481875" y1="6.81558125" x2="3.00481875" y2="6.90041875" layer="21"/>
<rectangle x1="14.18081875" y1="6.81558125" x2="28.49118125" y2="6.90041875" layer="21"/>
<rectangle x1="1.651" y1="6.90041875" x2="2.83718125" y2="6.985" layer="21"/>
<rectangle x1="14.18081875" y1="6.90041875" x2="28.575" y2="6.985" layer="21"/>
<rectangle x1="1.56718125" y1="6.985" x2="2.75081875" y2="7.06958125" layer="21"/>
<rectangle x1="14.18081875" y1="6.985" x2="28.65881875" y2="7.06958125" layer="21"/>
<rectangle x1="1.48081875" y1="7.06958125" x2="2.667" y2="7.15441875" layer="21"/>
<rectangle x1="14.097" y1="7.06958125" x2="28.74518125" y2="7.15441875" layer="21"/>
<rectangle x1="1.397" y1="7.15441875" x2="2.58318125" y2="7.239" layer="21"/>
<rectangle x1="14.097" y1="7.15441875" x2="28.74518125" y2="7.239" layer="21"/>
<rectangle x1="1.31318125" y1="7.239" x2="2.49681875" y2="7.32358125" layer="21"/>
<rectangle x1="14.097" y1="7.239" x2="28.829" y2="7.32358125" layer="21"/>
<rectangle x1="1.31318125" y1="7.32358125" x2="2.413" y2="7.40841875" layer="21"/>
<rectangle x1="14.01318125" y1="7.32358125" x2="28.91281875" y2="7.40841875" layer="21"/>
<rectangle x1="1.22681875" y1="7.40841875" x2="2.32918125" y2="7.493" layer="21"/>
<rectangle x1="14.01318125" y1="7.40841875" x2="28.99918125" y2="7.493" layer="21"/>
<rectangle x1="1.143" y1="7.493" x2="2.24281875" y2="7.57758125" layer="21"/>
<rectangle x1="14.01318125" y1="7.493" x2="28.99918125" y2="7.57758125" layer="21"/>
<rectangle x1="1.143" y1="7.57758125" x2="2.159" y2="7.66241875" layer="21"/>
<rectangle x1="13.92681875" y1="7.57758125" x2="29.083" y2="7.66241875" layer="21"/>
<rectangle x1="1.05918125" y1="7.66241875" x2="2.07518125" y2="7.747" layer="21"/>
<rectangle x1="13.92681875" y1="7.66241875" x2="29.16681875" y2="7.747" layer="21"/>
<rectangle x1="0.97281875" y1="7.747" x2="2.07518125" y2="7.83158125" layer="21"/>
<rectangle x1="13.92681875" y1="7.747" x2="29.16681875" y2="7.83158125" layer="21"/>
<rectangle x1="0.97281875" y1="7.83158125" x2="1.98881875" y2="7.91641875" layer="21"/>
<rectangle x1="13.92681875" y1="7.83158125" x2="29.25318125" y2="7.91641875" layer="21"/>
<rectangle x1="0.889" y1="7.91641875" x2="1.905" y2="8.001" layer="21"/>
<rectangle x1="13.843" y1="7.91641875" x2="29.337" y2="8.001" layer="21"/>
<rectangle x1="0.889" y1="8.001" x2="1.82118125" y2="8.08558125" layer="21"/>
<rectangle x1="13.843" y1="8.001" x2="29.337" y2="8.08558125" layer="21"/>
<rectangle x1="0.80518125" y1="8.08558125" x2="1.82118125" y2="8.17041875" layer="21"/>
<rectangle x1="13.843" y1="8.08558125" x2="29.42081875" y2="8.17041875" layer="21"/>
<rectangle x1="0.71881875" y1="8.17041875" x2="1.73481875" y2="8.255" layer="21"/>
<rectangle x1="13.843" y1="8.17041875" x2="29.42081875" y2="8.255" layer="21"/>
<rectangle x1="0.71881875" y1="8.255" x2="1.73481875" y2="8.33958125" layer="21"/>
<rectangle x1="13.75918125" y1="8.255" x2="29.50718125" y2="8.33958125" layer="21"/>
<rectangle x1="0.71881875" y1="8.33958125" x2="1.651" y2="8.42441875" layer="21"/>
<rectangle x1="13.75918125" y1="8.33958125" x2="29.50718125" y2="8.42441875" layer="21"/>
<rectangle x1="0.635" y1="8.42441875" x2="1.651" y2="8.509" layer="21"/>
<rectangle x1="5.63118125" y1="8.42441875" x2="6.64718125" y2="8.509" layer="21"/>
<rectangle x1="8.763" y1="8.42441875" x2="10.541" y2="8.509" layer="21"/>
<rectangle x1="13.75918125" y1="8.42441875" x2="16.80718125" y2="8.509" layer="21"/>
<rectangle x1="17.145" y1="8.42441875" x2="29.591" y2="8.509" layer="21"/>
<rectangle x1="0.635" y1="8.509" x2="1.56718125" y2="8.59358125" layer="21"/>
<rectangle x1="5.29081875" y1="8.509" x2="6.90118125" y2="8.59358125" layer="21"/>
<rectangle x1="8.67918125" y1="8.509" x2="10.87881875" y2="8.59358125" layer="21"/>
<rectangle x1="13.75918125" y1="8.509" x2="16.29918125" y2="8.59358125" layer="21"/>
<rectangle x1="17.653" y1="8.509" x2="20.10918125" y2="8.59358125" layer="21"/>
<rectangle x1="20.447" y1="8.509" x2="22.733" y2="8.59358125" layer="21"/>
<rectangle x1="23.07081875" y1="8.509" x2="24.84881875" y2="8.59358125" layer="21"/>
<rectangle x1="25.61081875" y1="8.509" x2="29.591" y2="8.59358125" layer="21"/>
<rectangle x1="0.55118125" y1="8.59358125" x2="1.48081875" y2="8.67841875" layer="21"/>
<rectangle x1="5.12318125" y1="8.59358125" x2="7.15518125" y2="8.67841875" layer="21"/>
<rectangle x1="8.59281875" y1="8.59358125" x2="11.049" y2="8.67841875" layer="21"/>
<rectangle x1="13.75918125" y1="8.59358125" x2="16.04518125" y2="8.67841875" layer="21"/>
<rectangle x1="17.907" y1="8.59358125" x2="20.02281875" y2="8.67841875" layer="21"/>
<rectangle x1="20.53081875" y1="8.59358125" x2="22.64918125" y2="8.67841875" layer="21"/>
<rectangle x1="23.15718125" y1="8.59358125" x2="24.59481875" y2="8.67841875" layer="21"/>
<rectangle x1="25.86481875" y1="8.59358125" x2="29.591" y2="8.67841875" layer="21"/>
<rectangle x1="0.55118125" y1="8.67841875" x2="1.48081875" y2="8.763" layer="21"/>
<rectangle x1="4.953" y1="8.67841875" x2="7.32281875" y2="8.763" layer="21"/>
<rectangle x1="8.59281875" y1="8.67841875" x2="11.21918125" y2="8.763" layer="21"/>
<rectangle x1="13.67281875" y1="8.67841875" x2="15.875" y2="8.763" layer="21"/>
<rectangle x1="18.07718125" y1="8.67841875" x2="19.939" y2="8.763" layer="21"/>
<rectangle x1="20.53081875" y1="8.67841875" x2="22.64918125" y2="8.763" layer="21"/>
<rectangle x1="23.15718125" y1="8.67841875" x2="24.42718125" y2="8.763" layer="21"/>
<rectangle x1="26.11881875" y1="8.67841875" x2="29.67481875" y2="8.763" layer="21"/>
<rectangle x1="0.46481875" y1="8.763" x2="1.397" y2="8.84758125" layer="21"/>
<rectangle x1="4.78281875" y1="8.763" x2="7.493" y2="8.84758125" layer="21"/>
<rectangle x1="8.59281875" y1="8.763" x2="11.38681875" y2="8.84758125" layer="21"/>
<rectangle x1="13.67281875" y1="8.763" x2="15.70481875" y2="8.84758125" layer="21"/>
<rectangle x1="18.24481875" y1="8.763" x2="19.939" y2="8.84758125" layer="21"/>
<rectangle x1="20.53081875" y1="8.763" x2="22.64918125" y2="8.84758125" layer="21"/>
<rectangle x1="23.241" y1="8.763" x2="24.34081875" y2="8.84758125" layer="21"/>
<rectangle x1="26.289" y1="8.763" x2="29.67481875" y2="8.84758125" layer="21"/>
<rectangle x1="0.46481875" y1="8.84758125" x2="1.397" y2="8.93241875" layer="21"/>
<rectangle x1="4.699" y1="8.84758125" x2="7.57681875" y2="8.93241875" layer="21"/>
<rectangle x1="8.59281875" y1="8.84758125" x2="11.47318125" y2="8.93241875" layer="21"/>
<rectangle x1="13.67281875" y1="8.84758125" x2="15.621" y2="8.93241875" layer="21"/>
<rectangle x1="18.33118125" y1="8.84758125" x2="19.939" y2="8.93241875" layer="21"/>
<rectangle x1="20.53081875" y1="8.84758125" x2="22.56281875" y2="8.93241875" layer="21"/>
<rectangle x1="23.241" y1="8.84758125" x2="24.17318125" y2="8.93241875" layer="21"/>
<rectangle x1="26.37281875" y1="8.84758125" x2="29.76118125" y2="8.93241875" layer="21"/>
<rectangle x1="0.46481875" y1="8.93241875" x2="1.397" y2="9.017" layer="21"/>
<rectangle x1="4.52881875" y1="8.93241875" x2="7.66318125" y2="9.017" layer="21"/>
<rectangle x1="8.59281875" y1="8.93241875" x2="11.557" y2="9.017" layer="21"/>
<rectangle x1="13.67281875" y1="8.93241875" x2="15.45081875" y2="9.017" layer="21"/>
<rectangle x1="18.49881875" y1="8.93241875" x2="19.939" y2="9.017" layer="21"/>
<rectangle x1="20.53081875" y1="8.93241875" x2="22.56281875" y2="9.017" layer="21"/>
<rectangle x1="23.241" y1="8.93241875" x2="24.08681875" y2="9.017" layer="21"/>
<rectangle x1="26.45918125" y1="8.93241875" x2="29.76118125" y2="9.017" layer="21"/>
<rectangle x1="0.381" y1="9.017" x2="1.31318125" y2="9.10158125" layer="21"/>
<rectangle x1="4.445" y1="9.017" x2="7.83081875" y2="9.10158125" layer="21"/>
<rectangle x1="8.59281875" y1="9.017" x2="11.72718125" y2="9.10158125" layer="21"/>
<rectangle x1="13.67281875" y1="9.017" x2="15.367" y2="9.10158125" layer="21"/>
<rectangle x1="18.58518125" y1="9.017" x2="19.939" y2="9.10158125" layer="21"/>
<rectangle x1="20.53081875" y1="9.017" x2="22.56281875" y2="9.10158125" layer="21"/>
<rectangle x1="23.241" y1="9.017" x2="24.003" y2="9.10158125" layer="21"/>
<rectangle x1="26.543" y1="9.017" x2="29.76118125" y2="9.10158125" layer="21"/>
<rectangle x1="0.381" y1="9.10158125" x2="1.31318125" y2="9.18641875" layer="21"/>
<rectangle x1="4.36118125" y1="9.10158125" x2="7.91718125" y2="9.18641875" layer="21"/>
<rectangle x1="8.67918125" y1="9.10158125" x2="11.811" y2="9.18641875" layer="21"/>
<rectangle x1="13.67281875" y1="9.10158125" x2="15.28318125" y2="9.18641875" layer="21"/>
<rectangle x1="18.669" y1="9.10158125" x2="19.939" y2="9.18641875" layer="21"/>
<rectangle x1="20.53081875" y1="9.10158125" x2="22.56281875" y2="9.18641875" layer="21"/>
<rectangle x1="23.241" y1="9.10158125" x2="23.91918125" y2="9.18641875" layer="21"/>
<rectangle x1="25.019" y1="9.10158125" x2="25.44318125" y2="9.18641875" layer="21"/>
<rectangle x1="26.62681875" y1="9.10158125" x2="29.845" y2="9.18641875" layer="21"/>
<rectangle x1="0.381" y1="9.18641875" x2="1.31318125" y2="9.271" layer="21"/>
<rectangle x1="4.27481875" y1="9.18641875" x2="8.001" y2="9.271" layer="21"/>
<rectangle x1="8.763" y1="9.18641875" x2="11.89481875" y2="9.271" layer="21"/>
<rectangle x1="13.67281875" y1="9.18641875" x2="15.19681875" y2="9.271" layer="21"/>
<rectangle x1="16.891" y1="9.18641875" x2="17.06118125" y2="9.271" layer="21"/>
<rectangle x1="18.75281875" y1="9.18641875" x2="19.939" y2="9.271" layer="21"/>
<rectangle x1="20.53081875" y1="9.18641875" x2="22.56281875" y2="9.271" layer="21"/>
<rectangle x1="23.241" y1="9.18641875" x2="23.91918125" y2="9.271" layer="21"/>
<rectangle x1="24.765" y1="9.18641875" x2="25.781" y2="9.271" layer="21"/>
<rectangle x1="26.71318125" y1="9.18641875" x2="29.845" y2="9.271" layer="21"/>
<rectangle x1="0.29718125" y1="9.271" x2="1.22681875" y2="9.35558125" layer="21"/>
<rectangle x1="4.191" y1="9.271" x2="5.79881875" y2="9.35558125" layer="21"/>
<rectangle x1="6.477" y1="9.271" x2="8.08481875" y2="9.35558125" layer="21"/>
<rectangle x1="10.287" y1="9.271" x2="11.89481875" y2="9.35558125" layer="21"/>
<rectangle x1="13.589" y1="9.271" x2="15.113" y2="9.35558125" layer="21"/>
<rectangle x1="16.46681875" y1="9.271" x2="17.48281875" y2="9.35558125" layer="21"/>
<rectangle x1="18.83918125" y1="9.271" x2="19.939" y2="9.35558125" layer="21"/>
<rectangle x1="20.53081875" y1="9.271" x2="22.56281875" y2="9.35558125" layer="21"/>
<rectangle x1="23.241" y1="9.271" x2="23.83281875" y2="9.35558125" layer="21"/>
<rectangle x1="24.68118125" y1="9.271" x2="25.95118125" y2="9.35558125" layer="21"/>
<rectangle x1="26.71318125" y1="9.271" x2="29.845" y2="9.35558125" layer="21"/>
<rectangle x1="0.29718125" y1="9.35558125" x2="1.22681875" y2="9.44041875" layer="21"/>
<rectangle x1="4.10718125" y1="9.35558125" x2="5.54481875" y2="9.44041875" layer="21"/>
<rectangle x1="6.731" y1="9.35558125" x2="8.08481875" y2="9.44041875" layer="21"/>
<rectangle x1="10.62481875" y1="9.35558125" x2="11.98118125" y2="9.44041875" layer="21"/>
<rectangle x1="13.589" y1="9.35558125" x2="15.02918125" y2="9.44041875" layer="21"/>
<rectangle x1="16.21281875" y1="9.35558125" x2="17.653" y2="9.44041875" layer="21"/>
<rectangle x1="18.923" y1="9.35558125" x2="19.939" y2="9.44041875" layer="21"/>
<rectangle x1="20.53081875" y1="9.35558125" x2="22.56281875" y2="9.44041875" layer="21"/>
<rectangle x1="23.241" y1="9.35558125" x2="23.749" y2="9.44041875" layer="21"/>
<rectangle x1="24.59481875" y1="9.35558125" x2="26.035" y2="9.44041875" layer="21"/>
<rectangle x1="26.71318125" y1="9.35558125" x2="29.845" y2="9.44041875" layer="21"/>
<rectangle x1="0.29718125" y1="9.44041875" x2="1.22681875" y2="9.525" layer="21"/>
<rectangle x1="4.10718125" y1="9.44041875" x2="5.37718125" y2="9.525" layer="21"/>
<rectangle x1="6.90118125" y1="9.44041875" x2="8.17118125" y2="9.525" layer="21"/>
<rectangle x1="10.795" y1="9.44041875" x2="12.065" y2="9.525" layer="21"/>
<rectangle x1="13.589" y1="9.44041875" x2="14.94281875" y2="9.525" layer="21"/>
<rectangle x1="16.129" y1="9.44041875" x2="17.82318125" y2="9.525" layer="21"/>
<rectangle x1="18.923" y1="9.44041875" x2="19.939" y2="9.525" layer="21"/>
<rectangle x1="20.53081875" y1="9.44041875" x2="22.56281875" y2="9.525" layer="21"/>
<rectangle x1="23.241" y1="9.44041875" x2="23.749" y2="9.525" layer="21"/>
<rectangle x1="24.511" y1="9.44041875" x2="26.11881875" y2="9.525" layer="21"/>
<rectangle x1="26.62681875" y1="9.44041875" x2="29.92881875" y2="9.525" layer="21"/>
<rectangle x1="0.29718125" y1="9.525" x2="1.143" y2="9.60958125" layer="21"/>
<rectangle x1="4.02081875" y1="9.525" x2="5.207" y2="9.60958125" layer="21"/>
<rectangle x1="7.06881875" y1="9.525" x2="8.255" y2="9.60958125" layer="21"/>
<rectangle x1="10.96518125" y1="9.525" x2="12.14881875" y2="9.60958125" layer="21"/>
<rectangle x1="13.589" y1="9.525" x2="14.94281875" y2="9.60958125" layer="21"/>
<rectangle x1="15.95881875" y1="9.525" x2="17.99081875" y2="9.60958125" layer="21"/>
<rectangle x1="19.00681875" y1="9.525" x2="19.939" y2="9.60958125" layer="21"/>
<rectangle x1="20.53081875" y1="9.525" x2="22.56281875" y2="9.60958125" layer="21"/>
<rectangle x1="23.241" y1="9.525" x2="23.749" y2="9.60958125" layer="21"/>
<rectangle x1="24.42718125" y1="9.525" x2="26.20518125" y2="9.60958125" layer="21"/>
<rectangle x1="26.543" y1="9.525" x2="29.92881875" y2="9.60958125" layer="21"/>
<rectangle x1="0.21081875" y1="9.60958125" x2="1.143" y2="9.69441875" layer="21"/>
<rectangle x1="3.937" y1="9.60958125" x2="5.12318125" y2="9.69441875" layer="21"/>
<rectangle x1="7.15518125" y1="9.60958125" x2="8.255" y2="9.69441875" layer="21"/>
<rectangle x1="11.049" y1="9.60958125" x2="12.14881875" y2="9.69441875" layer="21"/>
<rectangle x1="13.589" y1="9.60958125" x2="14.859" y2="9.69441875" layer="21"/>
<rectangle x1="15.875" y1="9.60958125" x2="18.07718125" y2="9.69441875" layer="21"/>
<rectangle x1="19.09318125" y1="9.60958125" x2="19.939" y2="9.69441875" layer="21"/>
<rectangle x1="20.53081875" y1="9.60958125" x2="22.56281875" y2="9.69441875" layer="21"/>
<rectangle x1="23.241" y1="9.60958125" x2="23.66518125" y2="9.69441875" layer="21"/>
<rectangle x1="24.34081875" y1="9.60958125" x2="29.92881875" y2="9.69441875" layer="21"/>
<rectangle x1="0.21081875" y1="9.69441875" x2="1.143" y2="9.779" layer="21"/>
<rectangle x1="3.937" y1="9.69441875" x2="4.953" y2="9.779" layer="21"/>
<rectangle x1="7.239" y1="9.69441875" x2="8.33881875" y2="9.779" layer="21"/>
<rectangle x1="11.13281875" y1="9.69441875" x2="12.23518125" y2="9.779" layer="21"/>
<rectangle x1="13.589" y1="9.69441875" x2="14.77518125" y2="9.779" layer="21"/>
<rectangle x1="15.79118125" y1="9.69441875" x2="18.161" y2="9.779" layer="21"/>
<rectangle x1="19.09318125" y1="9.69441875" x2="19.939" y2="9.779" layer="21"/>
<rectangle x1="20.53081875" y1="9.69441875" x2="22.56281875" y2="9.779" layer="21"/>
<rectangle x1="23.241" y1="9.69441875" x2="23.66518125" y2="9.779" layer="21"/>
<rectangle x1="24.257" y1="9.69441875" x2="29.92881875" y2="9.779" layer="21"/>
<rectangle x1="0.21081875" y1="9.779" x2="1.143" y2="9.86358125" layer="21"/>
<rectangle x1="3.85318125" y1="9.779" x2="4.86918125" y2="9.86358125" layer="21"/>
<rectangle x1="7.32281875" y1="9.779" x2="8.33881875" y2="9.86358125" layer="21"/>
<rectangle x1="11.21918125" y1="9.779" x2="12.23518125" y2="9.86358125" layer="21"/>
<rectangle x1="13.589" y1="9.779" x2="14.77518125" y2="9.86358125" layer="21"/>
<rectangle x1="15.70481875" y1="9.779" x2="18.24481875" y2="9.86358125" layer="21"/>
<rectangle x1="19.177" y1="9.779" x2="19.939" y2="9.86358125" layer="21"/>
<rectangle x1="20.53081875" y1="9.779" x2="22.56281875" y2="9.86358125" layer="21"/>
<rectangle x1="23.241" y1="9.779" x2="23.66518125" y2="9.86358125" layer="21"/>
<rectangle x1="24.34081875" y1="9.779" x2="29.92881875" y2="9.86358125" layer="21"/>
<rectangle x1="0.21081875" y1="9.86358125" x2="1.05918125" y2="9.94841875" layer="21"/>
<rectangle x1="3.85318125" y1="9.86358125" x2="4.86918125" y2="9.94841875" layer="21"/>
<rectangle x1="7.40918125" y1="9.86358125" x2="8.42518125" y2="9.94841875" layer="21"/>
<rectangle x1="11.303" y1="9.86358125" x2="12.319" y2="9.94841875" layer="21"/>
<rectangle x1="13.589" y1="9.86358125" x2="14.68881875" y2="9.94841875" layer="21"/>
<rectangle x1="15.621" y1="9.86358125" x2="18.33118125" y2="9.94841875" layer="21"/>
<rectangle x1="19.177" y1="9.86358125" x2="19.939" y2="9.94841875" layer="21"/>
<rectangle x1="20.53081875" y1="9.86358125" x2="22.56281875" y2="9.94841875" layer="21"/>
<rectangle x1="23.241" y1="9.86358125" x2="23.66518125" y2="9.94841875" layer="21"/>
<rectangle x1="26.71318125" y1="9.86358125" x2="29.92881875" y2="9.94841875" layer="21"/>
<rectangle x1="0.21081875" y1="9.94841875" x2="1.05918125" y2="10.033" layer="21"/>
<rectangle x1="3.76681875" y1="9.94841875" x2="4.78281875" y2="10.033" layer="21"/>
<rectangle x1="7.493" y1="9.94841875" x2="8.42518125" y2="10.033" layer="21"/>
<rectangle x1="11.38681875" y1="9.94841875" x2="12.319" y2="10.033" layer="21"/>
<rectangle x1="13.589" y1="9.94841875" x2="14.68881875" y2="10.033" layer="21"/>
<rectangle x1="15.53718125" y1="9.94841875" x2="18.33118125" y2="10.033" layer="21"/>
<rectangle x1="19.26081875" y1="9.94841875" x2="19.939" y2="10.033" layer="21"/>
<rectangle x1="20.53081875" y1="9.94841875" x2="22.56281875" y2="10.033" layer="21"/>
<rectangle x1="23.241" y1="9.94841875" x2="23.57881875" y2="10.033" layer="21"/>
<rectangle x1="26.797" y1="9.94841875" x2="29.92881875" y2="10.033" layer="21"/>
<rectangle x1="0.21081875" y1="10.033" x2="1.05918125" y2="10.11758125" layer="21"/>
<rectangle x1="3.76681875" y1="10.033" x2="4.699" y2="10.11758125" layer="21"/>
<rectangle x1="7.493" y1="10.033" x2="8.509" y2="10.11758125" layer="21"/>
<rectangle x1="11.38681875" y1="10.033" x2="12.40281875" y2="10.11758125" layer="21"/>
<rectangle x1="13.589" y1="10.033" x2="14.68881875" y2="10.11758125" layer="21"/>
<rectangle x1="15.53718125" y1="10.033" x2="18.415" y2="10.11758125" layer="21"/>
<rectangle x1="19.26081875" y1="10.033" x2="19.939" y2="10.11758125" layer="21"/>
<rectangle x1="20.53081875" y1="10.033" x2="22.56281875" y2="10.11758125" layer="21"/>
<rectangle x1="23.241" y1="10.033" x2="23.57881875" y2="10.11758125" layer="21"/>
<rectangle x1="26.88081875" y1="10.033" x2="29.92881875" y2="10.11758125" layer="21"/>
<rectangle x1="0.127" y1="10.11758125" x2="1.05918125" y2="10.20241875" layer="21"/>
<rectangle x1="3.76681875" y1="10.11758125" x2="4.699" y2="10.20241875" layer="21"/>
<rectangle x1="7.57681875" y1="10.11758125" x2="8.509" y2="10.20241875" layer="21"/>
<rectangle x1="11.47318125" y1="10.11758125" x2="12.40281875" y2="10.20241875" layer="21"/>
<rectangle x1="13.589" y1="10.11758125" x2="14.605" y2="10.20241875" layer="21"/>
<rectangle x1="15.45081875" y1="10.11758125" x2="18.49881875" y2="10.20241875" layer="21"/>
<rectangle x1="19.34718125" y1="10.11758125" x2="19.939" y2="10.20241875" layer="21"/>
<rectangle x1="20.53081875" y1="10.11758125" x2="22.56281875" y2="10.20241875" layer="21"/>
<rectangle x1="23.241" y1="10.11758125" x2="23.57881875" y2="10.20241875" layer="21"/>
<rectangle x1="26.88081875" y1="10.11758125" x2="29.92881875" y2="10.20241875" layer="21"/>
<rectangle x1="0.127" y1="10.20241875" x2="1.05918125" y2="10.287" layer="21"/>
<rectangle x1="3.683" y1="10.20241875" x2="4.61518125" y2="10.287" layer="21"/>
<rectangle x1="7.57681875" y1="10.20241875" x2="8.509" y2="10.287" layer="21"/>
<rectangle x1="11.557" y1="10.20241875" x2="12.40281875" y2="10.287" layer="21"/>
<rectangle x1="13.50518125" y1="10.20241875" x2="14.605" y2="10.287" layer="21"/>
<rectangle x1="15.45081875" y1="10.20241875" x2="18.49881875" y2="10.287" layer="21"/>
<rectangle x1="19.34718125" y1="10.20241875" x2="19.939" y2="10.287" layer="21"/>
<rectangle x1="20.53081875" y1="10.20241875" x2="22.56281875" y2="10.287" layer="21"/>
<rectangle x1="23.241" y1="10.20241875" x2="23.57881875" y2="10.287" layer="21"/>
<rectangle x1="26.88081875" y1="10.20241875" x2="29.92881875" y2="10.287" layer="21"/>
<rectangle x1="0.127" y1="10.287" x2="0.97281875" y2="10.37158125" layer="21"/>
<rectangle x1="3.683" y1="10.287" x2="4.61518125" y2="10.37158125" layer="21"/>
<rectangle x1="7.66318125" y1="10.287" x2="8.509" y2="10.37158125" layer="21"/>
<rectangle x1="11.557" y1="10.287" x2="12.48918125" y2="10.37158125" layer="21"/>
<rectangle x1="13.50518125" y1="10.287" x2="14.605" y2="10.37158125" layer="21"/>
<rectangle x1="15.367" y1="10.287" x2="18.49881875" y2="10.37158125" layer="21"/>
<rectangle x1="19.34718125" y1="10.287" x2="19.939" y2="10.37158125" layer="21"/>
<rectangle x1="20.53081875" y1="10.287" x2="22.56281875" y2="10.37158125" layer="21"/>
<rectangle x1="23.241" y1="10.287" x2="23.57881875" y2="10.37158125" layer="21"/>
<rectangle x1="26.88081875" y1="10.287" x2="29.92881875" y2="10.37158125" layer="21"/>
<rectangle x1="0.127" y1="10.37158125" x2="0.97281875" y2="10.45641875" layer="21"/>
<rectangle x1="3.683" y1="10.37158125" x2="4.61518125" y2="10.45641875" layer="21"/>
<rectangle x1="7.66318125" y1="10.37158125" x2="8.59281875" y2="10.45641875" layer="21"/>
<rectangle x1="11.557" y1="10.37158125" x2="12.48918125" y2="10.45641875" layer="21"/>
<rectangle x1="13.50518125" y1="10.37158125" x2="14.605" y2="10.45641875" layer="21"/>
<rectangle x1="15.367" y1="10.37158125" x2="18.58518125" y2="10.45641875" layer="21"/>
<rectangle x1="19.34718125" y1="10.37158125" x2="19.939" y2="10.45641875" layer="21"/>
<rectangle x1="20.61718125" y1="10.37158125" x2="22.56281875" y2="10.45641875" layer="21"/>
<rectangle x1="23.15718125" y1="10.37158125" x2="23.66518125" y2="10.45641875" layer="21"/>
<rectangle x1="26.88081875" y1="10.37158125" x2="29.92881875" y2="10.45641875" layer="21"/>
<rectangle x1="0.127" y1="10.45641875" x2="0.97281875" y2="10.541" layer="21"/>
<rectangle x1="3.683" y1="10.45641875" x2="4.52881875" y2="10.541" layer="21"/>
<rectangle x1="7.747" y1="10.45641875" x2="8.59281875" y2="10.541" layer="21"/>
<rectangle x1="11.64081875" y1="10.45641875" x2="12.48918125" y2="10.541" layer="21"/>
<rectangle x1="13.50518125" y1="10.45641875" x2="14.52118125" y2="10.541" layer="21"/>
<rectangle x1="15.367" y1="10.45641875" x2="18.58518125" y2="10.541" layer="21"/>
<rectangle x1="19.34718125" y1="10.45641875" x2="19.939" y2="10.541" layer="21"/>
<rectangle x1="20.61718125" y1="10.45641875" x2="22.56281875" y2="10.541" layer="21"/>
<rectangle x1="23.15718125" y1="10.45641875" x2="23.66518125" y2="10.541" layer="21"/>
<rectangle x1="24.34081875" y1="10.45641875" x2="26.20518125" y2="10.541" layer="21"/>
<rectangle x1="26.797" y1="10.45641875" x2="29.92881875" y2="10.541" layer="21"/>
<rectangle x1="0.127" y1="10.541" x2="0.97281875" y2="10.62558125" layer="21"/>
<rectangle x1="3.683" y1="10.541" x2="4.52881875" y2="10.62558125" layer="21"/>
<rectangle x1="7.747" y1="10.541" x2="8.59281875" y2="10.62558125" layer="21"/>
<rectangle x1="11.64081875" y1="10.541" x2="12.48918125" y2="10.62558125" layer="21"/>
<rectangle x1="13.50518125" y1="10.541" x2="14.52118125" y2="10.62558125" layer="21"/>
<rectangle x1="15.28318125" y1="10.541" x2="18.58518125" y2="10.62558125" layer="21"/>
<rectangle x1="19.431" y1="10.541" x2="19.939" y2="10.62558125" layer="21"/>
<rectangle x1="20.61718125" y1="10.541" x2="22.479" y2="10.62558125" layer="21"/>
<rectangle x1="23.15718125" y1="10.541" x2="23.66518125" y2="10.62558125" layer="21"/>
<rectangle x1="24.34081875" y1="10.541" x2="26.20518125" y2="10.62558125" layer="21"/>
<rectangle x1="26.797" y1="10.541" x2="29.92881875" y2="10.62558125" layer="21"/>
<rectangle x1="0.127" y1="10.62558125" x2="0.97281875" y2="10.71041875" layer="21"/>
<rectangle x1="3.59918125" y1="10.62558125" x2="4.52881875" y2="10.71041875" layer="21"/>
<rectangle x1="7.747" y1="10.62558125" x2="8.59281875" y2="10.71041875" layer="21"/>
<rectangle x1="11.64081875" y1="10.62558125" x2="12.48918125" y2="10.71041875" layer="21"/>
<rectangle x1="13.50518125" y1="10.62558125" x2="14.52118125" y2="10.71041875" layer="21"/>
<rectangle x1="15.28318125" y1="10.62558125" x2="18.669" y2="10.71041875" layer="21"/>
<rectangle x1="19.431" y1="10.62558125" x2="19.939" y2="10.71041875" layer="21"/>
<rectangle x1="20.701" y1="10.62558125" x2="22.479" y2="10.71041875" layer="21"/>
<rectangle x1="23.15718125" y1="10.62558125" x2="23.66518125" y2="10.71041875" layer="21"/>
<rectangle x1="24.34081875" y1="10.62558125" x2="26.11881875" y2="10.71041875" layer="21"/>
<rectangle x1="26.797" y1="10.62558125" x2="29.92881875" y2="10.71041875" layer="21"/>
<rectangle x1="0.127" y1="10.71041875" x2="0.97281875" y2="10.795" layer="21"/>
<rectangle x1="3.59918125" y1="10.71041875" x2="4.52881875" y2="10.795" layer="21"/>
<rectangle x1="7.747" y1="10.71041875" x2="8.59281875" y2="10.795" layer="21"/>
<rectangle x1="11.64081875" y1="10.71041875" x2="12.48918125" y2="10.795" layer="21"/>
<rectangle x1="13.50518125" y1="10.71041875" x2="14.52118125" y2="10.795" layer="21"/>
<rectangle x1="15.28318125" y1="10.71041875" x2="18.669" y2="10.795" layer="21"/>
<rectangle x1="19.431" y1="10.71041875" x2="19.939" y2="10.795" layer="21"/>
<rectangle x1="20.78481875" y1="10.71041875" x2="22.39518125" y2="10.795" layer="21"/>
<rectangle x1="23.07081875" y1="10.71041875" x2="23.749" y2="10.795" layer="21"/>
<rectangle x1="24.42718125" y1="10.71041875" x2="26.035" y2="10.795" layer="21"/>
<rectangle x1="26.797" y1="10.71041875" x2="29.92881875" y2="10.795" layer="21"/>
<rectangle x1="0.127" y1="10.795" x2="0.97281875" y2="10.87958125" layer="21"/>
<rectangle x1="3.59918125" y1="10.795" x2="4.52881875" y2="10.87958125" layer="21"/>
<rectangle x1="7.747" y1="10.795" x2="8.59281875" y2="10.87958125" layer="21"/>
<rectangle x1="11.64081875" y1="10.795" x2="12.48918125" y2="10.87958125" layer="21"/>
<rectangle x1="13.50518125" y1="10.795" x2="14.52118125" y2="10.87958125" layer="21"/>
<rectangle x1="15.28318125" y1="10.795" x2="18.669" y2="10.87958125" layer="21"/>
<rectangle x1="19.431" y1="10.795" x2="19.939" y2="10.87958125" layer="21"/>
<rectangle x1="20.78481875" y1="10.795" x2="22.30881875" y2="10.87958125" layer="21"/>
<rectangle x1="23.07081875" y1="10.795" x2="23.749" y2="10.87958125" layer="21"/>
<rectangle x1="24.511" y1="10.795" x2="26.035" y2="10.87958125" layer="21"/>
<rectangle x1="26.71318125" y1="10.795" x2="29.92881875" y2="10.87958125" layer="21"/>
<rectangle x1="0.127" y1="10.87958125" x2="0.97281875" y2="10.96441875" layer="21"/>
<rectangle x1="3.59918125" y1="10.87958125" x2="4.52881875" y2="10.96441875" layer="21"/>
<rectangle x1="7.747" y1="10.87958125" x2="8.59281875" y2="10.96441875" layer="21"/>
<rectangle x1="11.64081875" y1="10.87958125" x2="12.48918125" y2="10.96441875" layer="21"/>
<rectangle x1="13.50518125" y1="10.87958125" x2="14.52118125" y2="10.96441875" layer="21"/>
<rectangle x1="15.28318125" y1="10.87958125" x2="18.669" y2="10.96441875" layer="21"/>
<rectangle x1="19.431" y1="10.87958125" x2="19.939" y2="10.96441875" layer="21"/>
<rectangle x1="20.87118125" y1="10.87958125" x2="22.225" y2="10.96441875" layer="21"/>
<rectangle x1="22.987" y1="10.87958125" x2="23.83281875" y2="10.96441875" layer="21"/>
<rectangle x1="24.59481875" y1="10.87958125" x2="25.95118125" y2="10.96441875" layer="21"/>
<rectangle x1="26.71318125" y1="10.87958125" x2="29.92881875" y2="10.96441875" layer="21"/>
<rectangle x1="0.127" y1="10.96441875" x2="0.97281875" y2="11.049" layer="21"/>
<rectangle x1="3.59918125" y1="10.96441875" x2="4.52881875" y2="11.049" layer="21"/>
<rectangle x1="7.747" y1="10.96441875" x2="8.59281875" y2="11.049" layer="21"/>
<rectangle x1="11.64081875" y1="10.96441875" x2="12.48918125" y2="11.049" layer="21"/>
<rectangle x1="13.50518125" y1="10.96441875" x2="14.52118125" y2="11.049" layer="21"/>
<rectangle x1="15.28318125" y1="10.96441875" x2="18.669" y2="11.049" layer="21"/>
<rectangle x1="19.431" y1="10.96441875" x2="19.939" y2="11.049" layer="21"/>
<rectangle x1="21.03881875" y1="10.96441875" x2="22.14118125" y2="11.049" layer="21"/>
<rectangle x1="22.987" y1="10.96441875" x2="23.83281875" y2="11.049" layer="21"/>
<rectangle x1="24.68118125" y1="10.96441875" x2="25.781" y2="11.049" layer="21"/>
<rectangle x1="26.62681875" y1="10.96441875" x2="29.92881875" y2="11.049" layer="21"/>
<rectangle x1="0.127" y1="11.049" x2="0.97281875" y2="11.13358125" layer="21"/>
<rectangle x1="3.59918125" y1="11.049" x2="4.52881875" y2="11.13358125" layer="21"/>
<rectangle x1="7.747" y1="11.049" x2="8.59281875" y2="11.13358125" layer="21"/>
<rectangle x1="11.64081875" y1="11.049" x2="12.48918125" y2="11.13358125" layer="21"/>
<rectangle x1="13.50518125" y1="11.049" x2="14.52118125" y2="11.13358125" layer="21"/>
<rectangle x1="15.28318125" y1="11.049" x2="18.669" y2="11.13358125" layer="21"/>
<rectangle x1="19.431" y1="11.049" x2="19.939" y2="11.13358125" layer="21"/>
<rectangle x1="21.209" y1="11.049" x2="21.971" y2="11.13358125" layer="21"/>
<rectangle x1="22.90318125" y1="11.049" x2="23.91918125" y2="11.13358125" layer="21"/>
<rectangle x1="24.84881875" y1="11.049" x2="25.61081875" y2="11.13358125" layer="21"/>
<rectangle x1="26.543" y1="11.049" x2="29.92881875" y2="11.13358125" layer="21"/>
<rectangle x1="0.127" y1="11.13358125" x2="0.97281875" y2="11.21841875" layer="21"/>
<rectangle x1="3.59918125" y1="11.13358125" x2="4.52881875" y2="11.21841875" layer="21"/>
<rectangle x1="7.747" y1="11.13358125" x2="8.59281875" y2="11.21841875" layer="21"/>
<rectangle x1="11.64081875" y1="11.13358125" x2="12.48918125" y2="11.21841875" layer="21"/>
<rectangle x1="13.50518125" y1="11.13358125" x2="14.52118125" y2="11.21841875" layer="21"/>
<rectangle x1="15.28318125" y1="11.13358125" x2="18.58518125" y2="11.21841875" layer="21"/>
<rectangle x1="19.431" y1="11.13358125" x2="19.939" y2="11.21841875" layer="21"/>
<rectangle x1="21.463" y1="11.13358125" x2="21.63318125" y2="11.21841875" layer="21"/>
<rectangle x1="22.81681875" y1="11.13358125" x2="24.003" y2="11.21841875" layer="21"/>
<rectangle x1="25.18918125" y1="11.13358125" x2="25.273" y2="11.21841875" layer="21"/>
<rectangle x1="26.543" y1="11.13358125" x2="29.92881875" y2="11.21841875" layer="21"/>
<rectangle x1="0.127" y1="11.21841875" x2="0.97281875" y2="11.303" layer="21"/>
<rectangle x1="3.59918125" y1="11.21841875" x2="4.52881875" y2="11.303" layer="21"/>
<rectangle x1="7.747" y1="11.21841875" x2="8.59281875" y2="11.303" layer="21"/>
<rectangle x1="11.64081875" y1="11.21841875" x2="12.48918125" y2="11.303" layer="21"/>
<rectangle x1="13.50518125" y1="11.21841875" x2="14.52118125" y2="11.303" layer="21"/>
<rectangle x1="15.28318125" y1="11.21841875" x2="18.58518125" y2="11.303" layer="21"/>
<rectangle x1="19.34718125" y1="11.21841875" x2="19.939" y2="11.303" layer="21"/>
<rectangle x1="22.81681875" y1="11.21841875" x2="24.003" y2="11.303" layer="21"/>
<rectangle x1="26.45918125" y1="11.21841875" x2="29.92881875" y2="11.303" layer="21"/>
<rectangle x1="0.127" y1="11.303" x2="0.97281875" y2="11.38758125" layer="21"/>
<rectangle x1="3.59918125" y1="11.303" x2="4.52881875" y2="11.38758125" layer="21"/>
<rectangle x1="7.747" y1="11.303" x2="8.59281875" y2="11.38758125" layer="21"/>
<rectangle x1="11.557" y1="11.303" x2="12.48918125" y2="11.38758125" layer="21"/>
<rectangle x1="13.50518125" y1="11.303" x2="14.52118125" y2="11.38758125" layer="21"/>
<rectangle x1="15.367" y1="11.303" x2="18.58518125" y2="11.38758125" layer="21"/>
<rectangle x1="19.34718125" y1="11.303" x2="19.939" y2="11.38758125" layer="21"/>
<rectangle x1="22.64918125" y1="11.303" x2="24.08681875" y2="11.38758125" layer="21"/>
<rectangle x1="26.37281875" y1="11.303" x2="29.92881875" y2="11.38758125" layer="21"/>
<rectangle x1="0.127" y1="11.38758125" x2="0.97281875" y2="11.47241875" layer="21"/>
<rectangle x1="3.59918125" y1="11.38758125" x2="4.52881875" y2="11.47241875" layer="21"/>
<rectangle x1="7.747" y1="11.38758125" x2="8.59281875" y2="11.47241875" layer="21"/>
<rectangle x1="11.557" y1="11.38758125" x2="12.48918125" y2="11.47241875" layer="21"/>
<rectangle x1="13.50518125" y1="11.38758125" x2="14.605" y2="11.47241875" layer="21"/>
<rectangle x1="15.367" y1="11.38758125" x2="18.58518125" y2="11.47241875" layer="21"/>
<rectangle x1="19.34718125" y1="11.38758125" x2="19.939" y2="11.47241875" layer="21"/>
<rectangle x1="22.56281875" y1="11.38758125" x2="24.257" y2="11.47241875" layer="21"/>
<rectangle x1="26.289" y1="11.38758125" x2="29.92881875" y2="11.47241875" layer="21"/>
<rectangle x1="0.127" y1="11.47241875" x2="1.05918125" y2="11.557" layer="21"/>
<rectangle x1="3.59918125" y1="11.47241875" x2="4.52881875" y2="11.557" layer="21"/>
<rectangle x1="7.747" y1="11.47241875" x2="8.59281875" y2="11.557" layer="21"/>
<rectangle x1="11.557" y1="11.47241875" x2="12.40281875" y2="11.557" layer="21"/>
<rectangle x1="13.50518125" y1="11.47241875" x2="14.605" y2="11.557" layer="21"/>
<rectangle x1="15.45081875" y1="11.47241875" x2="18.49881875" y2="11.557" layer="21"/>
<rectangle x1="19.34718125" y1="11.47241875" x2="19.939" y2="11.557" layer="21"/>
<rectangle x1="20.53081875" y1="11.47241875" x2="20.701" y2="11.557" layer="21"/>
<rectangle x1="22.479" y1="11.47241875" x2="24.34081875" y2="11.557" layer="21"/>
<rectangle x1="26.11881875" y1="11.47241875" x2="29.92881875" y2="11.557" layer="21"/>
<rectangle x1="0.127" y1="11.557" x2="1.05918125" y2="11.64158125" layer="21"/>
<rectangle x1="3.59918125" y1="11.557" x2="4.52881875" y2="11.64158125" layer="21"/>
<rectangle x1="7.747" y1="11.557" x2="8.59281875" y2="11.64158125" layer="21"/>
<rectangle x1="11.47318125" y1="11.557" x2="12.40281875" y2="11.64158125" layer="21"/>
<rectangle x1="13.50518125" y1="11.557" x2="14.605" y2="11.64158125" layer="21"/>
<rectangle x1="15.45081875" y1="11.557" x2="18.49881875" y2="11.64158125" layer="21"/>
<rectangle x1="19.34718125" y1="11.557" x2="19.939" y2="11.64158125" layer="21"/>
<rectangle x1="20.53081875" y1="11.557" x2="20.78481875" y2="11.64158125" layer="21"/>
<rectangle x1="22.30881875" y1="11.557" x2="24.511" y2="11.64158125" layer="21"/>
<rectangle x1="26.035" y1="11.557" x2="29.92881875" y2="11.64158125" layer="21"/>
<rectangle x1="0.21081875" y1="11.64158125" x2="1.05918125" y2="11.72641875" layer="21"/>
<rectangle x1="3.59918125" y1="11.64158125" x2="4.52881875" y2="11.72641875" layer="21"/>
<rectangle x1="7.747" y1="11.64158125" x2="8.59281875" y2="11.72641875" layer="21"/>
<rectangle x1="11.47318125" y1="11.64158125" x2="12.40281875" y2="11.72641875" layer="21"/>
<rectangle x1="13.589" y1="11.64158125" x2="14.605" y2="11.72641875" layer="21"/>
<rectangle x1="15.45081875" y1="11.64158125" x2="18.415" y2="11.72641875" layer="21"/>
<rectangle x1="19.26081875" y1="11.64158125" x2="20.02281875" y2="11.72641875" layer="21"/>
<rectangle x1="20.447" y1="11.64158125" x2="21.03881875" y2="11.72641875" layer="21"/>
<rectangle x1="22.14118125" y1="11.64158125" x2="24.68118125" y2="11.72641875" layer="21"/>
<rectangle x1="25.781" y1="11.64158125" x2="29.92881875" y2="11.72641875" layer="21"/>
<rectangle x1="0.21081875" y1="11.72641875" x2="1.05918125" y2="11.811" layer="21"/>
<rectangle x1="3.59918125" y1="11.72641875" x2="4.52881875" y2="11.811" layer="21"/>
<rectangle x1="7.747" y1="11.72641875" x2="8.59281875" y2="11.811" layer="21"/>
<rectangle x1="11.38681875" y1="11.72641875" x2="12.40281875" y2="11.811" layer="21"/>
<rectangle x1="13.589" y1="11.72641875" x2="14.68881875" y2="11.811" layer="21"/>
<rectangle x1="15.53718125" y1="11.72641875" x2="18.415" y2="11.811" layer="21"/>
<rectangle x1="19.26081875" y1="11.72641875" x2="20.10918125" y2="11.811" layer="21"/>
<rectangle x1="20.36318125" y1="11.72641875" x2="21.29281875" y2="11.811" layer="21"/>
<rectangle x1="21.88718125" y1="11.72641875" x2="24.93518125" y2="11.811" layer="21"/>
<rectangle x1="25.527" y1="11.72641875" x2="29.92881875" y2="11.811" layer="21"/>
<rectangle x1="0.21081875" y1="11.811" x2="1.05918125" y2="11.89558125" layer="21"/>
<rectangle x1="3.59918125" y1="11.811" x2="4.52881875" y2="11.89558125" layer="21"/>
<rectangle x1="7.747" y1="11.811" x2="8.59281875" y2="11.89558125" layer="21"/>
<rectangle x1="11.38681875" y1="11.811" x2="12.319" y2="11.89558125" layer="21"/>
<rectangle x1="13.589" y1="11.811" x2="14.68881875" y2="11.89558125" layer="21"/>
<rectangle x1="15.621" y1="11.811" x2="18.33118125" y2="11.89558125" layer="21"/>
<rectangle x1="19.26081875" y1="11.811" x2="29.92881875" y2="11.89558125" layer="21"/>
<rectangle x1="0.21081875" y1="11.89558125" x2="1.05918125" y2="11.98041875" layer="21"/>
<rectangle x1="3.59918125" y1="11.89558125" x2="4.52881875" y2="11.98041875" layer="21"/>
<rectangle x1="7.747" y1="11.89558125" x2="8.59281875" y2="11.98041875" layer="21"/>
<rectangle x1="11.303" y1="11.89558125" x2="12.319" y2="11.98041875" layer="21"/>
<rectangle x1="13.589" y1="11.89558125" x2="14.77518125" y2="11.98041875" layer="21"/>
<rectangle x1="15.621" y1="11.89558125" x2="18.24481875" y2="11.98041875" layer="21"/>
<rectangle x1="19.177" y1="11.89558125" x2="29.92881875" y2="11.98041875" layer="21"/>
<rectangle x1="0.21081875" y1="11.98041875" x2="1.143" y2="12.065" layer="21"/>
<rectangle x1="3.59918125" y1="11.98041875" x2="4.52881875" y2="12.065" layer="21"/>
<rectangle x1="7.747" y1="11.98041875" x2="8.59281875" y2="12.065" layer="21"/>
<rectangle x1="11.21918125" y1="11.98041875" x2="12.23518125" y2="12.065" layer="21"/>
<rectangle x1="13.589" y1="11.98041875" x2="14.77518125" y2="12.065" layer="21"/>
<rectangle x1="15.70481875" y1="11.98041875" x2="18.161" y2="12.065" layer="21"/>
<rectangle x1="19.177" y1="11.98041875" x2="29.92881875" y2="12.065" layer="21"/>
<rectangle x1="0.21081875" y1="12.065" x2="1.143" y2="12.14958125" layer="21"/>
<rectangle x1="3.59918125" y1="12.065" x2="4.52881875" y2="12.14958125" layer="21"/>
<rectangle x1="7.747" y1="12.065" x2="8.59281875" y2="12.14958125" layer="21"/>
<rectangle x1="11.13281875" y1="12.065" x2="12.23518125" y2="12.14958125" layer="21"/>
<rectangle x1="13.589" y1="12.065" x2="14.859" y2="12.14958125" layer="21"/>
<rectangle x1="15.79118125" y1="12.065" x2="18.07718125" y2="12.14958125" layer="21"/>
<rectangle x1="19.09318125" y1="12.065" x2="29.92881875" y2="12.14958125" layer="21"/>
<rectangle x1="0.29718125" y1="12.14958125" x2="1.143" y2="12.23441875" layer="21"/>
<rectangle x1="3.59918125" y1="12.14958125" x2="4.52881875" y2="12.23441875" layer="21"/>
<rectangle x1="7.747" y1="12.14958125" x2="8.59281875" y2="12.23441875" layer="21"/>
<rectangle x1="11.049" y1="12.14958125" x2="12.14881875" y2="12.23441875" layer="21"/>
<rectangle x1="13.589" y1="12.14958125" x2="14.859" y2="12.23441875" layer="21"/>
<rectangle x1="15.875" y1="12.14958125" x2="17.99081875" y2="12.23441875" layer="21"/>
<rectangle x1="19.09318125" y1="12.14958125" x2="29.92881875" y2="12.23441875" layer="21"/>
<rectangle x1="0.29718125" y1="12.23441875" x2="1.143" y2="12.319" layer="21"/>
<rectangle x1="3.59918125" y1="12.23441875" x2="4.52881875" y2="12.319" layer="21"/>
<rectangle x1="7.747" y1="12.23441875" x2="8.59281875" y2="12.319" layer="21"/>
<rectangle x1="10.87881875" y1="12.23441875" x2="12.065" y2="12.319" layer="21"/>
<rectangle x1="13.589" y1="12.23441875" x2="14.94281875" y2="12.319" layer="21"/>
<rectangle x1="16.04518125" y1="12.23441875" x2="17.907" y2="12.319" layer="21"/>
<rectangle x1="19.00681875" y1="12.23441875" x2="29.92881875" y2="12.319" layer="21"/>
<rectangle x1="0.29718125" y1="12.319" x2="1.22681875" y2="12.40358125" layer="21"/>
<rectangle x1="3.59918125" y1="12.319" x2="4.52881875" y2="12.40358125" layer="21"/>
<rectangle x1="7.747" y1="12.319" x2="8.59281875" y2="12.40358125" layer="21"/>
<rectangle x1="10.71118125" y1="12.319" x2="12.065" y2="12.40358125" layer="21"/>
<rectangle x1="13.589" y1="12.319" x2="15.02918125" y2="12.40358125" layer="21"/>
<rectangle x1="16.129" y1="12.319" x2="17.82318125" y2="12.40358125" layer="21"/>
<rectangle x1="18.923" y1="12.319" x2="29.92881875" y2="12.40358125" layer="21"/>
<rectangle x1="0.29718125" y1="12.40358125" x2="1.22681875" y2="12.48841875" layer="21"/>
<rectangle x1="3.59918125" y1="12.40358125" x2="4.52881875" y2="12.48841875" layer="21"/>
<rectangle x1="7.747" y1="12.40358125" x2="8.59281875" y2="12.48841875" layer="21"/>
<rectangle x1="10.541" y1="12.40358125" x2="11.98118125" y2="12.48841875" layer="21"/>
<rectangle x1="13.589" y1="12.40358125" x2="15.02918125" y2="12.48841875" layer="21"/>
<rectangle x1="16.29918125" y1="12.40358125" x2="17.653" y2="12.48841875" layer="21"/>
<rectangle x1="18.83918125" y1="12.40358125" x2="29.845" y2="12.48841875" layer="21"/>
<rectangle x1="0.381" y1="12.48841875" x2="1.22681875" y2="12.573" layer="21"/>
<rectangle x1="3.59918125" y1="12.48841875" x2="4.52881875" y2="12.573" layer="21"/>
<rectangle x1="7.747" y1="12.48841875" x2="11.89481875" y2="12.573" layer="21"/>
<rectangle x1="13.67281875" y1="12.48841875" x2="15.113" y2="12.573" layer="21"/>
<rectangle x1="16.55318125" y1="12.48841875" x2="17.31518125" y2="12.573" layer="21"/>
<rectangle x1="18.83918125" y1="12.48841875" x2="29.845" y2="12.573" layer="21"/>
<rectangle x1="0.381" y1="12.573" x2="1.31318125" y2="12.65758125" layer="21"/>
<rectangle x1="3.59918125" y1="12.573" x2="4.52881875" y2="12.65758125" layer="21"/>
<rectangle x1="7.747" y1="12.573" x2="11.811" y2="12.65758125" layer="21"/>
<rectangle x1="13.67281875" y1="12.573" x2="15.19681875" y2="12.65758125" layer="21"/>
<rectangle x1="18.75281875" y1="12.573" x2="29.845" y2="12.65758125" layer="21"/>
<rectangle x1="0.381" y1="12.65758125" x2="1.31318125" y2="12.74241875" layer="21"/>
<rectangle x1="3.59918125" y1="12.65758125" x2="4.52881875" y2="12.74241875" layer="21"/>
<rectangle x1="7.747" y1="12.65758125" x2="11.72718125" y2="12.74241875" layer="21"/>
<rectangle x1="13.67281875" y1="12.65758125" x2="15.28318125" y2="12.74241875" layer="21"/>
<rectangle x1="18.669" y1="12.65758125" x2="29.76118125" y2="12.74241875" layer="21"/>
<rectangle x1="0.381" y1="12.74241875" x2="1.31318125" y2="12.827" layer="21"/>
<rectangle x1="3.59918125" y1="12.74241875" x2="4.52881875" y2="12.827" layer="21"/>
<rectangle x1="7.747" y1="12.74241875" x2="11.64081875" y2="12.827" layer="21"/>
<rectangle x1="13.67281875" y1="12.74241875" x2="15.367" y2="12.827" layer="21"/>
<rectangle x1="18.49881875" y1="12.74241875" x2="29.76118125" y2="12.827" layer="21"/>
<rectangle x1="0.46481875" y1="12.827" x2="1.397" y2="12.91158125" layer="21"/>
<rectangle x1="3.59918125" y1="12.827" x2="4.52881875" y2="12.91158125" layer="21"/>
<rectangle x1="7.747" y1="12.827" x2="11.557" y2="12.91158125" layer="21"/>
<rectangle x1="13.67281875" y1="12.827" x2="15.53718125" y2="12.91158125" layer="21"/>
<rectangle x1="18.415" y1="12.827" x2="29.76118125" y2="12.91158125" layer="21"/>
<rectangle x1="0.46481875" y1="12.91158125" x2="1.397" y2="12.99641875" layer="21"/>
<rectangle x1="3.59918125" y1="12.91158125" x2="4.52881875" y2="12.99641875" layer="21"/>
<rectangle x1="7.747" y1="12.91158125" x2="11.38681875" y2="12.99641875" layer="21"/>
<rectangle x1="13.67281875" y1="12.91158125" x2="15.621" y2="12.99641875" layer="21"/>
<rectangle x1="18.33118125" y1="12.91158125" x2="29.67481875" y2="12.99641875" layer="21"/>
<rectangle x1="0.55118125" y1="12.99641875" x2="1.48081875" y2="13.081" layer="21"/>
<rectangle x1="3.59918125" y1="12.99641875" x2="4.445" y2="13.081" layer="21"/>
<rectangle x1="7.747" y1="12.99641875" x2="11.303" y2="13.081" layer="21"/>
<rectangle x1="13.67281875" y1="12.99641875" x2="15.79118125" y2="13.081" layer="21"/>
<rectangle x1="18.161" y1="12.99641875" x2="29.67481875" y2="13.081" layer="21"/>
<rectangle x1="0.55118125" y1="13.081" x2="1.48081875" y2="13.16558125" layer="21"/>
<rectangle x1="3.683" y1="13.081" x2="4.445" y2="13.16558125" layer="21"/>
<rectangle x1="7.83081875" y1="13.081" x2="11.13281875" y2="13.16558125" layer="21"/>
<rectangle x1="13.75918125" y1="13.081" x2="15.95881875" y2="13.16558125" layer="21"/>
<rectangle x1="17.99081875" y1="13.081" x2="29.67481875" y2="13.16558125" layer="21"/>
<rectangle x1="0.55118125" y1="13.16558125" x2="1.56718125" y2="13.25041875" layer="21"/>
<rectangle x1="3.683" y1="13.16558125" x2="4.445" y2="13.25041875" layer="21"/>
<rectangle x1="7.83081875" y1="13.16558125" x2="10.96518125" y2="13.25041875" layer="21"/>
<rectangle x1="13.75918125" y1="13.16558125" x2="16.129" y2="13.25041875" layer="21"/>
<rectangle x1="17.82318125" y1="13.16558125" x2="29.591" y2="13.25041875" layer="21"/>
<rectangle x1="0.635" y1="13.25041875" x2="1.56718125" y2="13.335" layer="21"/>
<rectangle x1="3.76681875" y1="13.25041875" x2="4.36118125" y2="13.335" layer="21"/>
<rectangle x1="7.91718125" y1="13.25041875" x2="10.71118125" y2="13.335" layer="21"/>
<rectangle x1="13.75918125" y1="13.25041875" x2="16.46681875" y2="13.335" layer="21"/>
<rectangle x1="17.48281875" y1="13.25041875" x2="29.591" y2="13.335" layer="21"/>
<rectangle x1="0.635" y1="13.335" x2="1.651" y2="13.41958125" layer="21"/>
<rectangle x1="3.937" y1="13.335" x2="4.191" y2="13.41958125" layer="21"/>
<rectangle x1="8.08481875" y1="13.335" x2="10.37081875" y2="13.41958125" layer="21"/>
<rectangle x1="13.75918125" y1="13.335" x2="29.50718125" y2="13.41958125" layer="21"/>
<rectangle x1="0.71881875" y1="13.41958125" x2="1.651" y2="13.50441875" layer="21"/>
<rectangle x1="13.75918125" y1="13.41958125" x2="29.50718125" y2="13.50441875" layer="21"/>
<rectangle x1="0.71881875" y1="13.50441875" x2="1.73481875" y2="13.589" layer="21"/>
<rectangle x1="13.843" y1="13.50441875" x2="29.42081875" y2="13.589" layer="21"/>
<rectangle x1="0.80518125" y1="13.589" x2="1.82118125" y2="13.67358125" layer="21"/>
<rectangle x1="13.843" y1="13.589" x2="29.42081875" y2="13.67358125" layer="21"/>
<rectangle x1="0.80518125" y1="13.67358125" x2="1.82118125" y2="13.75841875" layer="21"/>
<rectangle x1="13.843" y1="13.67358125" x2="29.337" y2="13.75841875" layer="21"/>
<rectangle x1="0.889" y1="13.75841875" x2="1.905" y2="13.843" layer="21"/>
<rectangle x1="13.843" y1="13.75841875" x2="29.337" y2="13.843" layer="21"/>
<rectangle x1="0.889" y1="13.843" x2="1.905" y2="13.92758125" layer="21"/>
<rectangle x1="13.92681875" y1="13.843" x2="29.25318125" y2="13.92758125" layer="21"/>
<rectangle x1="0.97281875" y1="13.92758125" x2="1.98881875" y2="14.01241875" layer="21"/>
<rectangle x1="13.92681875" y1="13.92758125" x2="29.25318125" y2="14.01241875" layer="21"/>
<rectangle x1="1.05918125" y1="14.01241875" x2="2.07518125" y2="14.097" layer="21"/>
<rectangle x1="13.92681875" y1="14.01241875" x2="29.16681875" y2="14.097" layer="21"/>
<rectangle x1="1.05918125" y1="14.097" x2="2.159" y2="14.18158125" layer="21"/>
<rectangle x1="13.92681875" y1="14.097" x2="29.083" y2="14.18158125" layer="21"/>
<rectangle x1="1.143" y1="14.18158125" x2="2.24281875" y2="14.26641875" layer="21"/>
<rectangle x1="13.92681875" y1="14.18158125" x2="29.083" y2="14.26641875" layer="21"/>
<rectangle x1="1.143" y1="14.26641875" x2="2.24281875" y2="14.351" layer="21"/>
<rectangle x1="14.01318125" y1="14.26641875" x2="28.99918125" y2="14.351" layer="21"/>
<rectangle x1="1.22681875" y1="14.351" x2="2.32918125" y2="14.43558125" layer="21"/>
<rectangle x1="14.01318125" y1="14.351" x2="28.91281875" y2="14.43558125" layer="21"/>
<rectangle x1="1.31318125" y1="14.43558125" x2="2.413" y2="14.52041875" layer="21"/>
<rectangle x1="14.01318125" y1="14.43558125" x2="28.91281875" y2="14.52041875" layer="21"/>
<rectangle x1="1.397" y1="14.52041875" x2="2.49681875" y2="14.605" layer="21"/>
<rectangle x1="14.097" y1="14.52041875" x2="28.829" y2="14.605" layer="21"/>
<rectangle x1="1.48081875" y1="14.605" x2="2.58318125" y2="14.68958125" layer="21"/>
<rectangle x1="14.097" y1="14.605" x2="28.74518125" y2="14.68958125" layer="21"/>
<rectangle x1="1.48081875" y1="14.68958125" x2="2.667" y2="14.77441875" layer="21"/>
<rectangle x1="14.18081875" y1="14.68958125" x2="28.65881875" y2="14.77441875" layer="21"/>
<rectangle x1="1.56718125" y1="14.77441875" x2="2.83718125" y2="14.859" layer="21"/>
<rectangle x1="14.18081875" y1="14.77441875" x2="28.575" y2="14.859" layer="21"/>
<rectangle x1="1.651" y1="14.859" x2="2.921" y2="14.94358125" layer="21"/>
<rectangle x1="14.18081875" y1="14.859" x2="28.49118125" y2="14.94358125" layer="21"/>
<rectangle x1="1.73481875" y1="14.94358125" x2="3.00481875" y2="15.02841875" layer="21"/>
<rectangle x1="14.26718125" y1="14.94358125" x2="28.49118125" y2="15.02841875" layer="21"/>
<rectangle x1="1.82118125" y1="15.02841875" x2="3.09118125" y2="15.113" layer="21"/>
<rectangle x1="14.26718125" y1="15.02841875" x2="28.40481875" y2="15.113" layer="21"/>
<rectangle x1="1.905" y1="15.113" x2="3.25881875" y2="15.19758125" layer="21"/>
<rectangle x1="14.26718125" y1="15.113" x2="28.321" y2="15.19758125" layer="21"/>
<rectangle x1="1.98881875" y1="15.19758125" x2="3.34518125" y2="15.28241875" layer="21"/>
<rectangle x1="14.351" y1="15.19758125" x2="28.23718125" y2="15.28241875" layer="21"/>
<rectangle x1="2.07518125" y1="15.28241875" x2="3.51281875" y2="15.367" layer="21"/>
<rectangle x1="14.351" y1="15.28241875" x2="28.15081875" y2="15.367" layer="21"/>
<rectangle x1="2.159" y1="15.367" x2="3.683" y2="15.45158125" layer="21"/>
<rectangle x1="14.43481875" y1="15.367" x2="28.067" y2="15.45158125" layer="21"/>
<rectangle x1="2.24281875" y1="15.45158125" x2="3.76681875" y2="15.53641875" layer="21"/>
<rectangle x1="14.43481875" y1="15.45158125" x2="27.89681875" y2="15.53641875" layer="21"/>
<rectangle x1="2.32918125" y1="15.53641875" x2="4.02081875" y2="15.621" layer="21"/>
<rectangle x1="14.52118125" y1="15.53641875" x2="27.813" y2="15.621" layer="21"/>
<rectangle x1="2.49681875" y1="15.621" x2="4.191" y2="15.70558125" layer="21"/>
<rectangle x1="14.52118125" y1="15.621" x2="27.72918125" y2="15.70558125" layer="21"/>
<rectangle x1="2.58318125" y1="15.70558125" x2="4.445" y2="15.79041875" layer="21"/>
<rectangle x1="14.605" y1="15.70558125" x2="27.64281875" y2="15.79041875" layer="21"/>
<rectangle x1="2.75081875" y1="15.79041875" x2="4.61518125" y2="15.875" layer="21"/>
<rectangle x1="14.68881875" y1="15.79041875" x2="27.47518125" y2="15.875" layer="21"/>
<rectangle x1="2.83718125" y1="15.875" x2="5.03681875" y2="15.95958125" layer="21"/>
<rectangle x1="14.68881875" y1="15.875" x2="27.38881875" y2="15.95958125" layer="21"/>
<rectangle x1="3.00481875" y1="15.95958125" x2="5.461" y2="16.04441875" layer="21"/>
<rectangle x1="14.77518125" y1="15.95958125" x2="27.22118125" y2="16.04441875" layer="21"/>
<rectangle x1="3.09118125" y1="16.04441875" x2="27.13481875" y2="16.129" layer="21"/>
<rectangle x1="3.25881875" y1="16.129" x2="26.96718125" y2="16.21358125" layer="21"/>
<rectangle x1="3.429" y1="16.21358125" x2="26.797" y2="16.29841875" layer="21"/>
<rectangle x1="3.59918125" y1="16.29841875" x2="26.62681875" y2="16.383" layer="21"/>
<rectangle x1="3.76681875" y1="16.383" x2="26.45918125" y2="16.46758125" layer="21"/>
<rectangle x1="3.937" y1="16.46758125" x2="26.20518125" y2="16.55241875" layer="21"/>
<rectangle x1="4.191" y1="16.55241875" x2="26.035" y2="16.637" layer="21"/>
<rectangle x1="4.445" y1="16.637" x2="25.69718125" y2="16.72158125" layer="21"/>
<rectangle x1="4.78281875" y1="16.72158125" x2="25.35681875" y2="16.80641875" layer="21"/>
<rectangle x1="5.207" y1="16.80641875" x2="25.019" y2="16.891" layer="21"/>
</package>
<package name="UDO-LOGO-40MM">
<rectangle x1="5.969" y1="0.71958125" x2="6.81481875" y2="0.80441875" layer="21"/>
<rectangle x1="28.321" y1="0.71958125" x2="29.083" y2="0.80441875" layer="21"/>
<rectangle x1="5.715" y1="0.80441875" x2="7.06881875" y2="0.889" layer="21"/>
<rectangle x1="28.067" y1="0.80441875" x2="29.42081875" y2="0.889" layer="21"/>
<rectangle x1="5.54481875" y1="0.889" x2="7.32281875" y2="0.97358125" layer="21"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.017" y2="0.97358125" layer="21"/>
<rectangle x1="11.049" y1="0.889" x2="11.303" y2="0.97358125" layer="21"/>
<rectangle x1="11.811" y1="0.889" x2="11.98118125" y2="0.97358125" layer="21"/>
<rectangle x1="13.843" y1="0.889" x2="14.18081875" y2="0.97358125" layer="21"/>
<rectangle x1="14.94281875" y1="0.889" x2="16.72081875" y2="0.97358125" layer="21"/>
<rectangle x1="19.60118125" y1="0.889" x2="20.10918125" y2="0.97358125" layer="21"/>
<rectangle x1="22.14118125" y1="0.889" x2="22.56281875" y2="0.97358125" layer="21"/>
<rectangle x1="24.59481875" y1="0.889" x2="25.10281875" y2="0.97358125" layer="21"/>
<rectangle x1="27.813" y1="0.889" x2="29.591" y2="0.97358125" layer="21"/>
<rectangle x1="30.94481875" y1="0.889" x2="31.19881875" y2="0.97358125" layer="21"/>
<rectangle x1="33.23081875" y1="0.889" x2="33.401" y2="0.97358125" layer="21"/>
<rectangle x1="34.84118125" y1="0.889" x2="35.34918125" y2="0.97358125" layer="21"/>
<rectangle x1="5.37718125" y1="0.97358125" x2="7.493" y2="1.05841875" layer="21"/>
<rectangle x1="8.763" y1="0.97358125" x2="9.10081875" y2="1.05841875" layer="21"/>
<rectangle x1="10.96518125" y1="0.97358125" x2="11.303" y2="1.05841875" layer="21"/>
<rectangle x1="11.72718125" y1="0.97358125" x2="12.065" y2="1.05841875" layer="21"/>
<rectangle x1="13.589" y1="0.97358125" x2="14.26718125" y2="1.05841875" layer="21"/>
<rectangle x1="14.94281875" y1="0.97358125" x2="16.97481875" y2="1.05841875" layer="21"/>
<rectangle x1="19.34718125" y1="0.97358125" x2="20.36318125" y2="1.05841875" layer="21"/>
<rectangle x1="21.971" y1="0.97358125" x2="22.90318125" y2="1.05841875" layer="21"/>
<rectangle x1="24.34081875" y1="0.97358125" x2="25.44318125" y2="1.05841875" layer="21"/>
<rectangle x1="27.64281875" y1="0.97358125" x2="29.76118125" y2="1.05841875" layer="21"/>
<rectangle x1="30.94481875" y1="0.97358125" x2="31.28518125" y2="1.05841875" layer="21"/>
<rectangle x1="33.147" y1="0.97358125" x2="33.48481875" y2="1.05841875" layer="21"/>
<rectangle x1="34.58718125" y1="0.97358125" x2="35.60318125" y2="1.05841875" layer="21"/>
<rectangle x1="5.29081875" y1="1.05841875" x2="6.05281875" y2="1.143" layer="21"/>
<rectangle x1="6.81481875" y1="1.05841875" x2="7.57681875" y2="1.143" layer="21"/>
<rectangle x1="8.763" y1="1.05841875" x2="9.10081875" y2="1.143" layer="21"/>
<rectangle x1="10.96518125" y1="1.05841875" x2="11.303" y2="1.143" layer="21"/>
<rectangle x1="11.72718125" y1="1.05841875" x2="12.065" y2="1.143" layer="21"/>
<rectangle x1="13.41881875" y1="1.05841875" x2="14.26718125" y2="1.143" layer="21"/>
<rectangle x1="14.859" y1="1.05841875" x2="17.22881875" y2="1.143" layer="21"/>
<rectangle x1="19.177" y1="1.05841875" x2="20.53081875" y2="1.143" layer="21"/>
<rectangle x1="21.80081875" y1="1.05841875" x2="23.07081875" y2="1.143" layer="21"/>
<rectangle x1="24.17318125" y1="1.05841875" x2="25.61081875" y2="1.143" layer="21"/>
<rectangle x1="27.559" y1="1.05841875" x2="28.40481875" y2="1.143" layer="21"/>
<rectangle x1="29.083" y1="1.05841875" x2="29.845" y2="1.143" layer="21"/>
<rectangle x1="30.94481875" y1="1.05841875" x2="31.28518125" y2="1.143" layer="21"/>
<rectangle x1="33.147" y1="1.05841875" x2="33.48481875" y2="1.143" layer="21"/>
<rectangle x1="34.417" y1="1.05841875" x2="35.77081875" y2="1.143" layer="21"/>
<rectangle x1="5.207" y1="1.143" x2="5.79881875" y2="1.22758125" layer="21"/>
<rectangle x1="6.985" y1="1.143" x2="7.66318125" y2="1.22758125" layer="21"/>
<rectangle x1="8.763" y1="1.143" x2="9.10081875" y2="1.22758125" layer="21"/>
<rectangle x1="10.96518125" y1="1.143" x2="11.303" y2="1.22758125" layer="21"/>
<rectangle x1="11.72718125" y1="1.143" x2="12.065" y2="1.22758125" layer="21"/>
<rectangle x1="13.25118125" y1="1.143" x2="14.26718125" y2="1.22758125" layer="21"/>
<rectangle x1="14.859" y1="1.143" x2="17.31518125" y2="1.22758125" layer="21"/>
<rectangle x1="19.09318125" y1="1.143" x2="20.701" y2="1.22758125" layer="21"/>
<rectangle x1="21.717" y1="1.143" x2="23.07081875" y2="1.22758125" layer="21"/>
<rectangle x1="24.08681875" y1="1.143" x2="25.69718125" y2="1.22758125" layer="21"/>
<rectangle x1="27.47518125" y1="1.143" x2="28.067" y2="1.22758125" layer="21"/>
<rectangle x1="29.337" y1="1.143" x2="29.92881875" y2="1.22758125" layer="21"/>
<rectangle x1="30.94481875" y1="1.143" x2="31.28518125" y2="1.22758125" layer="21"/>
<rectangle x1="33.147" y1="1.143" x2="33.48481875" y2="1.22758125" layer="21"/>
<rectangle x1="34.33318125" y1="1.143" x2="35.941" y2="1.22758125" layer="21"/>
<rectangle x1="5.03681875" y1="1.22758125" x2="5.63118125" y2="1.31241875" layer="21"/>
<rectangle x1="7.239" y1="1.22758125" x2="7.747" y2="1.31241875" layer="21"/>
<rectangle x1="8.763" y1="1.22758125" x2="9.10081875" y2="1.31241875" layer="21"/>
<rectangle x1="10.96518125" y1="1.22758125" x2="11.303" y2="1.31241875" layer="21"/>
<rectangle x1="11.72718125" y1="1.22758125" x2="12.065" y2="1.31241875" layer="21"/>
<rectangle x1="13.16481875" y1="1.22758125" x2="13.843" y2="1.31241875" layer="21"/>
<rectangle x1="14.859" y1="1.22758125" x2="15.28318125" y2="1.31241875" layer="21"/>
<rectangle x1="16.637" y1="1.22758125" x2="17.48281875" y2="1.31241875" layer="21"/>
<rectangle x1="18.923" y1="1.22758125" x2="19.685" y2="1.31241875" layer="21"/>
<rectangle x1="20.02281875" y1="1.22758125" x2="20.78481875" y2="1.31241875" layer="21"/>
<rectangle x1="21.63318125" y1="1.22758125" x2="22.225" y2="1.31241875" layer="21"/>
<rectangle x1="22.56281875" y1="1.22758125" x2="23.15718125" y2="1.31241875" layer="21"/>
<rectangle x1="24.003" y1="1.22758125" x2="24.68118125" y2="1.31241875" layer="21"/>
<rectangle x1="25.10281875" y1="1.22758125" x2="25.781" y2="1.31241875" layer="21"/>
<rectangle x1="27.38881875" y1="1.22758125" x2="27.98318125" y2="1.31241875" layer="21"/>
<rectangle x1="29.50718125" y1="1.22758125" x2="30.01518125" y2="1.31241875" layer="21"/>
<rectangle x1="30.94481875" y1="1.22758125" x2="31.28518125" y2="1.31241875" layer="21"/>
<rectangle x1="33.147" y1="1.22758125" x2="33.48481875" y2="1.31241875" layer="21"/>
<rectangle x1="34.163" y1="1.22758125" x2="34.925" y2="1.31241875" layer="21"/>
<rectangle x1="35.26281875" y1="1.22758125" x2="36.02481875" y2="1.31241875" layer="21"/>
<rectangle x1="5.03681875" y1="1.31241875" x2="5.54481875" y2="1.397" layer="21"/>
<rectangle x1="7.32281875" y1="1.31241875" x2="7.83081875" y2="1.397" layer="21"/>
<rectangle x1="8.763" y1="1.31241875" x2="9.10081875" y2="1.397" layer="21"/>
<rectangle x1="10.96518125" y1="1.31241875" x2="11.303" y2="1.397" layer="21"/>
<rectangle x1="11.72718125" y1="1.31241875" x2="12.065" y2="1.397" layer="21"/>
<rectangle x1="13.081" y1="1.31241875" x2="13.67281875" y2="1.397" layer="21"/>
<rectangle x1="14.94281875" y1="1.31241875" x2="15.19681875" y2="1.397" layer="21"/>
<rectangle x1="16.891" y1="1.31241875" x2="17.56918125" y2="1.397" layer="21"/>
<rectangle x1="18.923" y1="1.31241875" x2="19.431" y2="1.397" layer="21"/>
<rectangle x1="20.27681875" y1="1.31241875" x2="20.87118125" y2="1.397" layer="21"/>
<rectangle x1="21.54681875" y1="1.31241875" x2="22.05481875" y2="1.397" layer="21"/>
<rectangle x1="22.733" y1="1.31241875" x2="23.241" y2="1.397" layer="21"/>
<rectangle x1="23.91918125" y1="1.31241875" x2="24.42718125" y2="1.397" layer="21"/>
<rectangle x1="25.273" y1="1.31241875" x2="25.86481875" y2="1.397" layer="21"/>
<rectangle x1="27.305" y1="1.31241875" x2="27.813" y2="1.397" layer="21"/>
<rectangle x1="29.591" y1="1.31241875" x2="30.099" y2="1.397" layer="21"/>
<rectangle x1="30.94481875" y1="1.31241875" x2="31.28518125" y2="1.397" layer="21"/>
<rectangle x1="33.147" y1="1.31241875" x2="33.48481875" y2="1.397" layer="21"/>
<rectangle x1="34.163" y1="1.31241875" x2="34.671" y2="1.397" layer="21"/>
<rectangle x1="35.51681875" y1="1.31241875" x2="36.11118125" y2="1.397" layer="21"/>
<rectangle x1="4.953" y1="1.397" x2="5.37718125" y2="1.48158125" layer="21"/>
<rectangle x1="7.40918125" y1="1.397" x2="7.91718125" y2="1.48158125" layer="21"/>
<rectangle x1="8.763" y1="1.397" x2="9.10081875" y2="1.48158125" layer="21"/>
<rectangle x1="10.96518125" y1="1.397" x2="11.303" y2="1.48158125" layer="21"/>
<rectangle x1="11.72718125" y1="1.397" x2="12.065" y2="1.48158125" layer="21"/>
<rectangle x1="13.081" y1="1.397" x2="13.50518125" y2="1.48158125" layer="21"/>
<rectangle x1="14.859" y1="1.397" x2="15.19681875" y2="1.48158125" layer="21"/>
<rectangle x1="17.06118125" y1="1.397" x2="17.653" y2="1.48158125" layer="21"/>
<rectangle x1="18.83918125" y1="1.397" x2="19.26081875" y2="1.48158125" layer="21"/>
<rectangle x1="20.447" y1="1.397" x2="20.87118125" y2="1.48158125" layer="21"/>
<rectangle x1="21.54681875" y1="1.397" x2="21.971" y2="1.48158125" layer="21"/>
<rectangle x1="22.90318125" y1="1.397" x2="23.241" y2="1.48158125" layer="21"/>
<rectangle x1="23.83281875" y1="1.397" x2="24.34081875" y2="1.48158125" layer="21"/>
<rectangle x1="25.44318125" y1="1.397" x2="25.95118125" y2="1.48158125" layer="21"/>
<rectangle x1="27.22118125" y1="1.397" x2="27.72918125" y2="1.48158125" layer="21"/>
<rectangle x1="29.76118125" y1="1.397" x2="30.18281875" y2="1.48158125" layer="21"/>
<rectangle x1="30.94481875" y1="1.397" x2="31.28518125" y2="1.48158125" layer="21"/>
<rectangle x1="33.147" y1="1.397" x2="33.48481875" y2="1.48158125" layer="21"/>
<rectangle x1="34.07918125" y1="1.397" x2="34.50081875" y2="1.48158125" layer="21"/>
<rectangle x1="35.687" y1="1.397" x2="36.11118125" y2="1.48158125" layer="21"/>
<rectangle x1="4.86918125" y1="1.48158125" x2="5.29081875" y2="1.56641875" layer="21"/>
<rectangle x1="7.57681875" y1="1.48158125" x2="8.001" y2="1.56641875" layer="21"/>
<rectangle x1="8.763" y1="1.48158125" x2="9.10081875" y2="1.56641875" layer="21"/>
<rectangle x1="10.96518125" y1="1.48158125" x2="11.303" y2="1.56641875" layer="21"/>
<rectangle x1="11.72718125" y1="1.48158125" x2="12.065" y2="1.56641875" layer="21"/>
<rectangle x1="12.99718125" y1="1.48158125" x2="13.41881875" y2="1.56641875" layer="21"/>
<rectangle x1="14.859" y1="1.48158125" x2="15.28318125" y2="1.56641875" layer="21"/>
<rectangle x1="17.22881875" y1="1.48158125" x2="17.73681875" y2="1.56641875" layer="21"/>
<rectangle x1="18.75281875" y1="1.48158125" x2="19.177" y2="1.56641875" layer="21"/>
<rectangle x1="20.53081875" y1="1.48158125" x2="20.955" y2="1.56641875" layer="21"/>
<rectangle x1="21.54681875" y1="1.48158125" x2="21.88718125" y2="1.56641875" layer="21"/>
<rectangle x1="22.90318125" y1="1.48158125" x2="23.241" y2="1.56641875" layer="21"/>
<rectangle x1="23.749" y1="1.48158125" x2="24.257" y2="1.56641875" layer="21"/>
<rectangle x1="25.527" y1="1.48158125" x2="25.95118125" y2="1.56641875" layer="21"/>
<rectangle x1="27.13481875" y1="1.48158125" x2="27.64281875" y2="1.56641875" layer="21"/>
<rectangle x1="29.845" y1="1.48158125" x2="30.26918125" y2="1.56641875" layer="21"/>
<rectangle x1="30.94481875" y1="1.48158125" x2="31.28518125" y2="1.56641875" layer="21"/>
<rectangle x1="33.147" y1="1.48158125" x2="33.48481875" y2="1.56641875" layer="21"/>
<rectangle x1="33.99281875" y1="1.48158125" x2="34.417" y2="1.56641875" layer="21"/>
<rectangle x1="35.77081875" y1="1.48158125" x2="36.195" y2="1.56641875" layer="21"/>
<rectangle x1="4.78281875" y1="1.56641875" x2="5.207" y2="1.651" layer="21"/>
<rectangle x1="7.57681875" y1="1.56641875" x2="8.001" y2="1.651" layer="21"/>
<rectangle x1="8.763" y1="1.56641875" x2="9.10081875" y2="1.651" layer="21"/>
<rectangle x1="10.96518125" y1="1.56641875" x2="11.303" y2="1.651" layer="21"/>
<rectangle x1="11.72718125" y1="1.56641875" x2="12.065" y2="1.651" layer="21"/>
<rectangle x1="12.91081875" y1="1.56641875" x2="13.335" y2="1.651" layer="21"/>
<rectangle x1="14.859" y1="1.56641875" x2="15.28318125" y2="1.651" layer="21"/>
<rectangle x1="17.31518125" y1="1.56641875" x2="17.82318125" y2="1.651" layer="21"/>
<rectangle x1="18.669" y1="1.56641875" x2="19.09318125" y2="1.651" layer="21"/>
<rectangle x1="20.61718125" y1="1.56641875" x2="21.03881875" y2="1.651" layer="21"/>
<rectangle x1="21.54681875" y1="1.56641875" x2="21.80081875" y2="1.651" layer="21"/>
<rectangle x1="22.987" y1="1.56641875" x2="23.241" y2="1.651" layer="21"/>
<rectangle x1="23.749" y1="1.56641875" x2="24.17318125" y2="1.651" layer="21"/>
<rectangle x1="25.61081875" y1="1.56641875" x2="26.035" y2="1.651" layer="21"/>
<rectangle x1="27.13481875" y1="1.56641875" x2="27.47518125" y2="1.651" layer="21"/>
<rectangle x1="29.92881875" y1="1.56641875" x2="30.353" y2="1.651" layer="21"/>
<rectangle x1="30.94481875" y1="1.56641875" x2="31.28518125" y2="1.651" layer="21"/>
<rectangle x1="33.147" y1="1.56641875" x2="33.48481875" y2="1.651" layer="21"/>
<rectangle x1="33.99281875" y1="1.56641875" x2="34.33318125" y2="1.651" layer="21"/>
<rectangle x1="35.85718125" y1="1.56641875" x2="36.27881875" y2="1.651" layer="21"/>
<rectangle x1="4.78281875" y1="1.651" x2="5.207" y2="1.73558125" layer="21"/>
<rectangle x1="7.66318125" y1="1.651" x2="8.08481875" y2="1.73558125" layer="21"/>
<rectangle x1="8.763" y1="1.651" x2="9.10081875" y2="1.73558125" layer="21"/>
<rectangle x1="10.96518125" y1="1.651" x2="11.303" y2="1.73558125" layer="21"/>
<rectangle x1="11.72718125" y1="1.651" x2="12.065" y2="1.73558125" layer="21"/>
<rectangle x1="12.91081875" y1="1.651" x2="13.25118125" y2="1.73558125" layer="21"/>
<rectangle x1="14.859" y1="1.651" x2="15.19681875" y2="1.73558125" layer="21"/>
<rectangle x1="17.48281875" y1="1.651" x2="17.907" y2="1.73558125" layer="21"/>
<rectangle x1="18.669" y1="1.651" x2="19.09318125" y2="1.73558125" layer="21"/>
<rectangle x1="20.701" y1="1.651" x2="21.03881875" y2="1.73558125" layer="21"/>
<rectangle x1="22.90318125" y1="1.651" x2="23.241" y2="1.73558125" layer="21"/>
<rectangle x1="23.66518125" y1="1.651" x2="24.08681875" y2="1.73558125" layer="21"/>
<rectangle x1="25.69718125" y1="1.651" x2="26.035" y2="1.73558125" layer="21"/>
<rectangle x1="27.051" y1="1.651" x2="27.47518125" y2="1.73558125" layer="21"/>
<rectangle x1="30.01518125" y1="1.651" x2="30.353" y2="1.73558125" layer="21"/>
<rectangle x1="30.94481875" y1="1.651" x2="31.28518125" y2="1.73558125" layer="21"/>
<rectangle x1="33.147" y1="1.651" x2="33.48481875" y2="1.73558125" layer="21"/>
<rectangle x1="33.909" y1="1.651" x2="34.33318125" y2="1.73558125" layer="21"/>
<rectangle x1="35.941" y1="1.651" x2="36.27881875" y2="1.73558125" layer="21"/>
<rectangle x1="4.699" y1="1.73558125" x2="5.12318125" y2="1.82041875" layer="21"/>
<rectangle x1="7.747" y1="1.73558125" x2="8.17118125" y2="1.82041875" layer="21"/>
<rectangle x1="8.763" y1="1.73558125" x2="9.10081875" y2="1.82041875" layer="21"/>
<rectangle x1="10.96518125" y1="1.73558125" x2="11.303" y2="1.82041875" layer="21"/>
<rectangle x1="11.72718125" y1="1.73558125" x2="12.065" y2="1.82041875" layer="21"/>
<rectangle x1="12.91081875" y1="1.73558125" x2="13.25118125" y2="1.82041875" layer="21"/>
<rectangle x1="14.859" y1="1.73558125" x2="15.28318125" y2="1.82041875" layer="21"/>
<rectangle x1="17.56918125" y1="1.73558125" x2="17.99081875" y2="1.82041875" layer="21"/>
<rectangle x1="18.669" y1="1.73558125" x2="19.00681875" y2="1.82041875" layer="21"/>
<rectangle x1="20.701" y1="1.73558125" x2="21.12518125" y2="1.82041875" layer="21"/>
<rectangle x1="22.90318125" y1="1.73558125" x2="23.241" y2="1.82041875" layer="21"/>
<rectangle x1="23.66518125" y1="1.73558125" x2="24.003" y2="1.82041875" layer="21"/>
<rectangle x1="25.781" y1="1.73558125" x2="25.95118125" y2="1.82041875" layer="21"/>
<rectangle x1="26.96718125" y1="1.73558125" x2="27.38881875" y2="1.82041875" layer="21"/>
<rectangle x1="30.01518125" y1="1.73558125" x2="30.43681875" y2="1.82041875" layer="21"/>
<rectangle x1="30.94481875" y1="1.73558125" x2="31.28518125" y2="1.82041875" layer="21"/>
<rectangle x1="33.147" y1="1.73558125" x2="33.48481875" y2="1.82041875" layer="21"/>
<rectangle x1="33.909" y1="1.73558125" x2="34.24681875" y2="1.82041875" layer="21"/>
<rectangle x1="35.941" y1="1.73558125" x2="36.195" y2="1.82041875" layer="21"/>
<rectangle x1="4.699" y1="1.82041875" x2="5.03681875" y2="1.905" layer="21"/>
<rectangle x1="7.83081875" y1="1.82041875" x2="8.17118125" y2="1.905" layer="21"/>
<rectangle x1="8.763" y1="1.82041875" x2="9.10081875" y2="1.905" layer="21"/>
<rectangle x1="10.96518125" y1="1.82041875" x2="11.303" y2="1.905" layer="21"/>
<rectangle x1="11.72718125" y1="1.82041875" x2="12.065" y2="1.905" layer="21"/>
<rectangle x1="12.827" y1="1.82041875" x2="13.16481875" y2="1.905" layer="21"/>
<rectangle x1="14.859" y1="1.82041875" x2="15.28318125" y2="1.905" layer="21"/>
<rectangle x1="17.56918125" y1="1.82041875" x2="17.99081875" y2="1.905" layer="21"/>
<rectangle x1="18.58518125" y1="1.82041875" x2="19.00681875" y2="1.905" layer="21"/>
<rectangle x1="20.78481875" y1="1.82041875" x2="21.12518125" y2="1.905" layer="21"/>
<rectangle x1="22.733" y1="1.82041875" x2="23.241" y2="1.905" layer="21"/>
<rectangle x1="23.66518125" y1="1.82041875" x2="24.003" y2="1.905" layer="21"/>
<rectangle x1="26.96718125" y1="1.82041875" x2="27.305" y2="1.905" layer="21"/>
<rectangle x1="30.099" y1="1.82041875" x2="30.43681875" y2="1.905" layer="21"/>
<rectangle x1="30.94481875" y1="1.82041875" x2="31.28518125" y2="1.905" layer="21"/>
<rectangle x1="33.147" y1="1.82041875" x2="33.48481875" y2="1.905" layer="21"/>
<rectangle x1="33.82518125" y1="1.82041875" x2="34.163" y2="1.905" layer="21"/>
<rectangle x1="4.61518125" y1="1.905" x2="5.03681875" y2="1.98958125" layer="21"/>
<rectangle x1="7.83081875" y1="1.905" x2="8.255" y2="1.98958125" layer="21"/>
<rectangle x1="8.763" y1="1.905" x2="9.10081875" y2="1.98958125" layer="21"/>
<rectangle x1="10.96518125" y1="1.905" x2="11.303" y2="1.98958125" layer="21"/>
<rectangle x1="11.72718125" y1="1.905" x2="12.065" y2="1.98958125" layer="21"/>
<rectangle x1="12.827" y1="1.905" x2="13.16481875" y2="1.98958125" layer="21"/>
<rectangle x1="14.859" y1="1.905" x2="15.28318125" y2="1.98958125" layer="21"/>
<rectangle x1="17.653" y1="1.905" x2="18.07718125" y2="1.98958125" layer="21"/>
<rectangle x1="18.58518125" y1="1.905" x2="18.923" y2="1.98958125" layer="21"/>
<rectangle x1="20.78481875" y1="1.905" x2="21.12518125" y2="1.98958125" layer="21"/>
<rectangle x1="22.64918125" y1="1.905" x2="23.15718125" y2="1.98958125" layer="21"/>
<rectangle x1="23.57881875" y1="1.905" x2="23.91918125" y2="1.98958125" layer="21"/>
<rectangle x1="26.96718125" y1="1.905" x2="27.305" y2="1.98958125" layer="21"/>
<rectangle x1="30.099" y1="1.905" x2="30.52318125" y2="1.98958125" layer="21"/>
<rectangle x1="30.94481875" y1="1.905" x2="31.28518125" y2="1.98958125" layer="21"/>
<rectangle x1="33.147" y1="1.905" x2="33.48481875" y2="1.98958125" layer="21"/>
<rectangle x1="33.82518125" y1="1.905" x2="34.163" y2="1.98958125" layer="21"/>
<rectangle x1="4.61518125" y1="1.98958125" x2="4.953" y2="2.07441875" layer="21"/>
<rectangle x1="7.91718125" y1="1.98958125" x2="8.255" y2="2.07441875" layer="21"/>
<rectangle x1="8.763" y1="1.98958125" x2="9.10081875" y2="2.07441875" layer="21"/>
<rectangle x1="10.96518125" y1="1.98958125" x2="11.303" y2="2.07441875" layer="21"/>
<rectangle x1="11.72718125" y1="1.98958125" x2="12.065" y2="2.07441875" layer="21"/>
<rectangle x1="12.827" y1="1.98958125" x2="13.16481875" y2="2.07441875" layer="21"/>
<rectangle x1="14.859" y1="1.98958125" x2="15.19681875" y2="2.07441875" layer="21"/>
<rectangle x1="17.73681875" y1="1.98958125" x2="18.07718125" y2="2.07441875" layer="21"/>
<rectangle x1="18.58518125" y1="1.98958125" x2="18.923" y2="2.07441875" layer="21"/>
<rectangle x1="20.78481875" y1="1.98958125" x2="21.12518125" y2="2.07441875" layer="21"/>
<rectangle x1="22.39518125" y1="1.98958125" x2="23.07081875" y2="2.07441875" layer="21"/>
<rectangle x1="23.57881875" y1="1.98958125" x2="25.86481875" y2="2.07441875" layer="21"/>
<rectangle x1="26.88081875" y1="1.98958125" x2="27.22118125" y2="2.07441875" layer="21"/>
<rectangle x1="30.18281875" y1="1.98958125" x2="30.52318125" y2="2.07441875" layer="21"/>
<rectangle x1="30.94481875" y1="1.98958125" x2="31.28518125" y2="2.07441875" layer="21"/>
<rectangle x1="33.147" y1="1.98958125" x2="33.48481875" y2="2.07441875" layer="21"/>
<rectangle x1="33.82518125" y1="1.98958125" x2="34.163" y2="2.07441875" layer="21"/>
<rectangle x1="34.33318125" y1="1.98958125" x2="35.941" y2="2.07441875" layer="21"/>
<rectangle x1="36.02481875" y1="1.98958125" x2="36.27881875" y2="2.07441875" layer="21"/>
<rectangle x1="4.61518125" y1="2.07441875" x2="4.953" y2="2.159" layer="21"/>
<rectangle x1="7.91718125" y1="2.07441875" x2="8.255" y2="2.159" layer="21"/>
<rectangle x1="8.763" y1="2.07441875" x2="9.10081875" y2="2.159" layer="21"/>
<rectangle x1="10.96518125" y1="2.07441875" x2="11.303" y2="2.159" layer="21"/>
<rectangle x1="11.72718125" y1="2.07441875" x2="12.065" y2="2.159" layer="21"/>
<rectangle x1="12.827" y1="2.07441875" x2="13.16481875" y2="2.159" layer="21"/>
<rectangle x1="14.859" y1="2.07441875" x2="15.19681875" y2="2.159" layer="21"/>
<rectangle x1="17.73681875" y1="2.07441875" x2="18.161" y2="2.159" layer="21"/>
<rectangle x1="18.58518125" y1="2.07441875" x2="18.923" y2="2.159" layer="21"/>
<rectangle x1="20.78481875" y1="2.07441875" x2="21.12518125" y2="2.159" layer="21"/>
<rectangle x1="22.14118125" y1="2.07441875" x2="22.987" y2="2.159" layer="21"/>
<rectangle x1="23.57881875" y1="2.07441875" x2="26.11881875" y2="2.159" layer="21"/>
<rectangle x1="26.88081875" y1="2.07441875" x2="27.22118125" y2="2.159" layer="21"/>
<rectangle x1="30.18281875" y1="2.07441875" x2="30.52318125" y2="2.159" layer="21"/>
<rectangle x1="30.94481875" y1="2.07441875" x2="31.28518125" y2="2.159" layer="21"/>
<rectangle x1="33.147" y1="2.07441875" x2="33.48481875" y2="2.159" layer="21"/>
<rectangle x1="33.82518125" y1="2.07441875" x2="36.36518125" y2="2.159" layer="21"/>
<rectangle x1="4.52881875" y1="2.159" x2="4.953" y2="2.24358125" layer="21"/>
<rectangle x1="7.91718125" y1="2.159" x2="8.255" y2="2.24358125" layer="21"/>
<rectangle x1="8.763" y1="2.159" x2="9.10081875" y2="2.24358125" layer="21"/>
<rectangle x1="10.96518125" y1="2.159" x2="11.303" y2="2.24358125" layer="21"/>
<rectangle x1="11.72718125" y1="2.159" x2="12.065" y2="2.24358125" layer="21"/>
<rectangle x1="12.827" y1="2.159" x2="13.16481875" y2="2.24358125" layer="21"/>
<rectangle x1="14.859" y1="2.159" x2="15.19681875" y2="2.24358125" layer="21"/>
<rectangle x1="17.82318125" y1="2.159" x2="18.161" y2="2.24358125" layer="21"/>
<rectangle x1="18.58518125" y1="2.159" x2="18.923" y2="2.24358125" layer="21"/>
<rectangle x1="20.78481875" y1="2.159" x2="21.12518125" y2="2.24358125" layer="21"/>
<rectangle x1="21.971" y1="2.159" x2="22.90318125" y2="2.24358125" layer="21"/>
<rectangle x1="23.57881875" y1="2.159" x2="26.20518125" y2="2.24358125" layer="21"/>
<rectangle x1="26.88081875" y1="2.159" x2="27.22118125" y2="2.24358125" layer="21"/>
<rectangle x1="30.18281875" y1="2.159" x2="30.607" y2="2.24358125" layer="21"/>
<rectangle x1="30.94481875" y1="2.159" x2="31.28518125" y2="2.24358125" layer="21"/>
<rectangle x1="33.147" y1="2.159" x2="33.48481875" y2="2.24358125" layer="21"/>
<rectangle x1="33.82518125" y1="2.159" x2="36.36518125" y2="2.24358125" layer="21"/>
<rectangle x1="4.52881875" y1="2.24358125" x2="4.86918125" y2="2.32841875" layer="21"/>
<rectangle x1="7.91718125" y1="2.24358125" x2="8.33881875" y2="2.32841875" layer="21"/>
<rectangle x1="8.763" y1="2.24358125" x2="9.10081875" y2="2.32841875" layer="21"/>
<rectangle x1="10.96518125" y1="2.24358125" x2="11.303" y2="2.32841875" layer="21"/>
<rectangle x1="11.72718125" y1="2.24358125" x2="12.065" y2="2.32841875" layer="21"/>
<rectangle x1="12.827" y1="2.24358125" x2="13.16481875" y2="2.32841875" layer="21"/>
<rectangle x1="14.859" y1="2.24358125" x2="15.19681875" y2="2.32841875" layer="21"/>
<rectangle x1="17.82318125" y1="2.24358125" x2="18.161" y2="2.32841875" layer="21"/>
<rectangle x1="18.58518125" y1="2.24358125" x2="18.923" y2="2.32841875" layer="21"/>
<rectangle x1="20.78481875" y1="2.24358125" x2="21.12518125" y2="2.32841875" layer="21"/>
<rectangle x1="21.717" y1="2.24358125" x2="22.64918125" y2="2.32841875" layer="21"/>
<rectangle x1="23.57881875" y1="2.24358125" x2="26.20518125" y2="2.32841875" layer="21"/>
<rectangle x1="26.797" y1="2.24358125" x2="27.22118125" y2="2.32841875" layer="21"/>
<rectangle x1="30.26918125" y1="2.24358125" x2="30.607" y2="2.32841875" layer="21"/>
<rectangle x1="30.94481875" y1="2.24358125" x2="31.28518125" y2="2.32841875" layer="21"/>
<rectangle x1="33.147" y1="2.24358125" x2="33.48481875" y2="2.32841875" layer="21"/>
<rectangle x1="33.82518125" y1="2.24358125" x2="36.36518125" y2="2.32841875" layer="21"/>
<rectangle x1="4.52881875" y1="2.32841875" x2="4.86918125" y2="2.413" layer="21"/>
<rectangle x1="8.001" y1="2.32841875" x2="8.33881875" y2="2.413" layer="21"/>
<rectangle x1="8.763" y1="2.32841875" x2="9.10081875" y2="2.413" layer="21"/>
<rectangle x1="10.96518125" y1="2.32841875" x2="11.303" y2="2.413" layer="21"/>
<rectangle x1="11.72718125" y1="2.32841875" x2="12.065" y2="2.413" layer="21"/>
<rectangle x1="12.827" y1="2.32841875" x2="13.16481875" y2="2.413" layer="21"/>
<rectangle x1="14.859" y1="2.32841875" x2="15.19681875" y2="2.413" layer="21"/>
<rectangle x1="17.82318125" y1="2.32841875" x2="18.24481875" y2="2.413" layer="21"/>
<rectangle x1="18.58518125" y1="2.32841875" x2="18.923" y2="2.413" layer="21"/>
<rectangle x1="20.78481875" y1="2.32841875" x2="21.12518125" y2="2.413" layer="21"/>
<rectangle x1="21.63318125" y1="2.32841875" x2="22.479" y2="2.413" layer="21"/>
<rectangle x1="23.57881875" y1="2.32841875" x2="24.257" y2="2.413" layer="21"/>
<rectangle x1="24.34081875" y1="2.32841875" x2="26.20518125" y2="2.413" layer="21"/>
<rectangle x1="26.797" y1="2.32841875" x2="27.13481875" y2="2.413" layer="21"/>
<rectangle x1="30.26918125" y1="2.32841875" x2="30.607" y2="2.413" layer="21"/>
<rectangle x1="30.94481875" y1="2.32841875" x2="31.28518125" y2="2.413" layer="21"/>
<rectangle x1="33.147" y1="2.32841875" x2="33.48481875" y2="2.413" layer="21"/>
<rectangle x1="33.82518125" y1="2.32841875" x2="35.179" y2="2.413" layer="21"/>
<rectangle x1="35.34918125" y1="2.32841875" x2="35.60318125" y2="2.413" layer="21"/>
<rectangle x1="35.77081875" y1="2.32841875" x2="35.941" y2="2.413" layer="21"/>
<rectangle x1="36.02481875" y1="2.32841875" x2="36.36518125" y2="2.413" layer="21"/>
<rectangle x1="4.52881875" y1="2.413" x2="4.86918125" y2="2.49758125" layer="21"/>
<rectangle x1="8.001" y1="2.413" x2="8.33881875" y2="2.49758125" layer="21"/>
<rectangle x1="8.763" y1="2.413" x2="9.10081875" y2="2.49758125" layer="21"/>
<rectangle x1="10.96518125" y1="2.413" x2="11.303" y2="2.49758125" layer="21"/>
<rectangle x1="11.72718125" y1="2.413" x2="12.065" y2="2.49758125" layer="21"/>
<rectangle x1="12.827" y1="2.413" x2="13.16481875" y2="2.49758125" layer="21"/>
<rectangle x1="14.859" y1="2.413" x2="15.19681875" y2="2.49758125" layer="21"/>
<rectangle x1="17.907" y1="2.413" x2="18.24481875" y2="2.49758125" layer="21"/>
<rectangle x1="18.58518125" y1="2.413" x2="18.923" y2="2.49758125" layer="21"/>
<rectangle x1="20.78481875" y1="2.413" x2="21.12518125" y2="2.49758125" layer="21"/>
<rectangle x1="21.54681875" y1="2.413" x2="22.225" y2="2.49758125" layer="21"/>
<rectangle x1="23.57881875" y1="2.413" x2="23.91918125" y2="2.49758125" layer="21"/>
<rectangle x1="25.781" y1="2.413" x2="26.11881875" y2="2.49758125" layer="21"/>
<rectangle x1="26.797" y1="2.413" x2="27.13481875" y2="2.49758125" layer="21"/>
<rectangle x1="30.26918125" y1="2.413" x2="30.607" y2="2.49758125" layer="21"/>
<rectangle x1="30.94481875" y1="2.413" x2="31.28518125" y2="2.49758125" layer="21"/>
<rectangle x1="33.147" y1="2.413" x2="33.48481875" y2="2.49758125" layer="21"/>
<rectangle x1="33.82518125" y1="2.413" x2="34.163" y2="2.49758125" layer="21"/>
<rectangle x1="36.02481875" y1="2.413" x2="36.36518125" y2="2.49758125" layer="21"/>
<rectangle x1="4.52881875" y1="2.49758125" x2="4.86918125" y2="2.58241875" layer="21"/>
<rectangle x1="8.001" y1="2.49758125" x2="8.33881875" y2="2.58241875" layer="21"/>
<rectangle x1="8.763" y1="2.49758125" x2="9.18718125" y2="2.58241875" layer="21"/>
<rectangle x1="10.96518125" y1="2.49758125" x2="11.303" y2="2.58241875" layer="21"/>
<rectangle x1="11.72718125" y1="2.49758125" x2="12.065" y2="2.58241875" layer="21"/>
<rectangle x1="12.827" y1="2.49758125" x2="13.16481875" y2="2.58241875" layer="21"/>
<rectangle x1="14.859" y1="2.49758125" x2="15.19681875" y2="2.58241875" layer="21"/>
<rectangle x1="17.907" y1="2.49758125" x2="18.24481875" y2="2.58241875" layer="21"/>
<rectangle x1="18.58518125" y1="2.49758125" x2="19.00681875" y2="2.58241875" layer="21"/>
<rectangle x1="20.78481875" y1="2.49758125" x2="21.12518125" y2="2.58241875" layer="21"/>
<rectangle x1="21.463" y1="2.49758125" x2="21.971" y2="2.58241875" layer="21"/>
<rectangle x1="23.66518125" y1="2.49758125" x2="24.003" y2="2.58241875" layer="21"/>
<rectangle x1="25.781" y1="2.49758125" x2="26.11881875" y2="2.58241875" layer="21"/>
<rectangle x1="26.797" y1="2.49758125" x2="27.13481875" y2="2.58241875" layer="21"/>
<rectangle x1="30.26918125" y1="2.49758125" x2="30.607" y2="2.58241875" layer="21"/>
<rectangle x1="30.94481875" y1="2.49758125" x2="31.369" y2="2.58241875" layer="21"/>
<rectangle x1="33.06318125" y1="2.49758125" x2="33.48481875" y2="2.58241875" layer="21"/>
<rectangle x1="33.82518125" y1="2.49758125" x2="34.24681875" y2="2.58241875" layer="21"/>
<rectangle x1="36.02481875" y1="2.49758125" x2="36.36518125" y2="2.58241875" layer="21"/>
<rectangle x1="4.52881875" y1="2.58241875" x2="4.86918125" y2="2.667" layer="21"/>
<rectangle x1="8.001" y1="2.58241875" x2="8.33881875" y2="2.667" layer="21"/>
<rectangle x1="8.763" y1="2.58241875" x2="9.18718125" y2="2.667" layer="21"/>
<rectangle x1="10.87881875" y1="2.58241875" x2="11.303" y2="2.667" layer="21"/>
<rectangle x1="11.72718125" y1="2.58241875" x2="12.065" y2="2.667" layer="21"/>
<rectangle x1="12.827" y1="2.58241875" x2="13.16481875" y2="2.667" layer="21"/>
<rectangle x1="14.859" y1="2.58241875" x2="15.19681875" y2="2.667" layer="21"/>
<rectangle x1="17.907" y1="2.58241875" x2="18.24481875" y2="2.667" layer="21"/>
<rectangle x1="18.669" y1="2.58241875" x2="19.00681875" y2="2.667" layer="21"/>
<rectangle x1="20.701" y1="2.58241875" x2="21.12518125" y2="2.667" layer="21"/>
<rectangle x1="21.463" y1="2.58241875" x2="21.88718125" y2="2.667" layer="21"/>
<rectangle x1="23.66518125" y1="2.58241875" x2="24.003" y2="2.667" layer="21"/>
<rectangle x1="25.69718125" y1="2.58241875" x2="26.11881875" y2="2.667" layer="21"/>
<rectangle x1="26.797" y1="2.58241875" x2="27.13481875" y2="2.667" layer="21"/>
<rectangle x1="30.26918125" y1="2.58241875" x2="30.607" y2="2.667" layer="21"/>
<rectangle x1="30.94481875" y1="2.58241875" x2="31.369" y2="2.667" layer="21"/>
<rectangle x1="33.06318125" y1="2.58241875" x2="33.401" y2="2.667" layer="21"/>
<rectangle x1="33.909" y1="2.58241875" x2="34.24681875" y2="2.667" layer="21"/>
<rectangle x1="35.941" y1="2.58241875" x2="36.36518125" y2="2.667" layer="21"/>
<rectangle x1="4.52881875" y1="2.667" x2="4.86918125" y2="2.75158125" layer="21"/>
<rectangle x1="8.001" y1="2.667" x2="8.33881875" y2="2.75158125" layer="21"/>
<rectangle x1="8.763" y1="2.667" x2="9.271" y2="2.75158125" layer="21"/>
<rectangle x1="10.87881875" y1="2.667" x2="11.21918125" y2="2.75158125" layer="21"/>
<rectangle x1="11.72718125" y1="2.667" x2="12.065" y2="2.75158125" layer="21"/>
<rectangle x1="12.827" y1="2.667" x2="13.16481875" y2="2.75158125" layer="21"/>
<rectangle x1="14.859" y1="2.667" x2="15.19681875" y2="2.75158125" layer="21"/>
<rectangle x1="17.907" y1="2.667" x2="18.24481875" y2="2.75158125" layer="21"/>
<rectangle x1="18.669" y1="2.667" x2="19.09318125" y2="2.75158125" layer="21"/>
<rectangle x1="20.701" y1="2.667" x2="21.03881875" y2="2.75158125" layer="21"/>
<rectangle x1="21.463" y1="2.667" x2="21.80081875" y2="2.75158125" layer="21"/>
<rectangle x1="23.66518125" y1="2.667" x2="24.08681875" y2="2.75158125" layer="21"/>
<rectangle x1="25.69718125" y1="2.667" x2="26.035" y2="2.75158125" layer="21"/>
<rectangle x1="26.797" y1="2.667" x2="27.13481875" y2="2.75158125" layer="21"/>
<rectangle x1="30.26918125" y1="2.667" x2="30.607" y2="2.75158125" layer="21"/>
<rectangle x1="30.94481875" y1="2.667" x2="31.45281875" y2="2.75158125" layer="21"/>
<rectangle x1="32.97681875" y1="2.667" x2="33.401" y2="2.75158125" layer="21"/>
<rectangle x1="33.909" y1="2.667" x2="34.33318125" y2="2.75158125" layer="21"/>
<rectangle x1="35.941" y1="2.667" x2="36.27881875" y2="2.75158125" layer="21"/>
<rectangle x1="4.52881875" y1="2.75158125" x2="4.86918125" y2="2.83641875" layer="21"/>
<rectangle x1="8.001" y1="2.75158125" x2="8.33881875" y2="2.83641875" layer="21"/>
<rectangle x1="8.763" y1="2.75158125" x2="9.271" y2="2.83641875" layer="21"/>
<rectangle x1="10.795" y1="2.75158125" x2="11.21918125" y2="2.83641875" layer="21"/>
<rectangle x1="11.72718125" y1="2.75158125" x2="12.065" y2="2.83641875" layer="21"/>
<rectangle x1="12.827" y1="2.75158125" x2="13.16481875" y2="2.83641875" layer="21"/>
<rectangle x1="14.859" y1="2.75158125" x2="15.19681875" y2="2.83641875" layer="21"/>
<rectangle x1="17.907" y1="2.75158125" x2="18.24481875" y2="2.83641875" layer="21"/>
<rectangle x1="18.75281875" y1="2.75158125" x2="19.09318125" y2="2.83641875" layer="21"/>
<rectangle x1="20.61718125" y1="2.75158125" x2="21.03881875" y2="2.83641875" layer="21"/>
<rectangle x1="21.463" y1="2.75158125" x2="21.80081875" y2="2.83641875" layer="21"/>
<rectangle x1="22.987" y1="2.75158125" x2="23.15718125" y2="2.83641875" layer="21"/>
<rectangle x1="23.749" y1="2.75158125" x2="24.17318125" y2="2.83641875" layer="21"/>
<rectangle x1="25.61081875" y1="2.75158125" x2="26.035" y2="2.83641875" layer="21"/>
<rectangle x1="26.797" y1="2.75158125" x2="27.13481875" y2="2.83641875" layer="21"/>
<rectangle x1="30.26918125" y1="2.75158125" x2="30.607" y2="2.83641875" layer="21"/>
<rectangle x1="30.94481875" y1="2.75158125" x2="31.45281875" y2="2.83641875" layer="21"/>
<rectangle x1="32.97681875" y1="2.75158125" x2="33.401" y2="2.83641875" layer="21"/>
<rectangle x1="33.909" y1="2.75158125" x2="34.33318125" y2="2.83641875" layer="21"/>
<rectangle x1="35.85718125" y1="2.75158125" x2="36.27881875" y2="2.83641875" layer="21"/>
<rectangle x1="4.52881875" y1="2.83641875" x2="4.86918125" y2="2.921" layer="21"/>
<rectangle x1="8.001" y1="2.83641875" x2="8.33881875" y2="2.921" layer="21"/>
<rectangle x1="8.763" y1="2.83641875" x2="9.35481875" y2="2.921" layer="21"/>
<rectangle x1="10.71118125" y1="2.83641875" x2="11.13281875" y2="2.921" layer="21"/>
<rectangle x1="11.72718125" y1="2.83641875" x2="12.065" y2="2.921" layer="21"/>
<rectangle x1="12.827" y1="2.83641875" x2="13.16481875" y2="2.921" layer="21"/>
<rectangle x1="14.859" y1="2.83641875" x2="15.19681875" y2="2.921" layer="21"/>
<rectangle x1="17.907" y1="2.83641875" x2="18.24481875" y2="2.921" layer="21"/>
<rectangle x1="18.75281875" y1="2.83641875" x2="19.177" y2="2.921" layer="21"/>
<rectangle x1="20.53081875" y1="2.83641875" x2="20.955" y2="2.921" layer="21"/>
<rectangle x1="21.463" y1="2.83641875" x2="21.80081875" y2="2.921" layer="21"/>
<rectangle x1="22.90318125" y1="2.83641875" x2="23.241" y2="2.921" layer="21"/>
<rectangle x1="23.749" y1="2.83641875" x2="24.257" y2="2.921" layer="21"/>
<rectangle x1="25.527" y1="2.83641875" x2="25.95118125" y2="2.921" layer="21"/>
<rectangle x1="26.797" y1="2.83641875" x2="27.13481875" y2="2.921" layer="21"/>
<rectangle x1="30.26918125" y1="2.83641875" x2="30.607" y2="2.921" layer="21"/>
<rectangle x1="30.94481875" y1="2.83641875" x2="31.53918125" y2="2.921" layer="21"/>
<rectangle x1="32.893" y1="2.83641875" x2="33.31718125" y2="2.921" layer="21"/>
<rectangle x1="33.99281875" y1="2.83641875" x2="34.417" y2="2.921" layer="21"/>
<rectangle x1="35.77081875" y1="2.83641875" x2="36.195" y2="2.921" layer="21"/>
<rectangle x1="4.52881875" y1="2.921" x2="4.86918125" y2="3.00558125" layer="21"/>
<rectangle x1="8.001" y1="2.921" x2="8.33881875" y2="3.00558125" layer="21"/>
<rectangle x1="8.763" y1="2.921" x2="9.44118125" y2="3.00558125" layer="21"/>
<rectangle x1="10.62481875" y1="2.921" x2="11.049" y2="3.00558125" layer="21"/>
<rectangle x1="11.72718125" y1="2.921" x2="12.065" y2="3.00558125" layer="21"/>
<rectangle x1="12.827" y1="2.921" x2="13.16481875" y2="3.00558125" layer="21"/>
<rectangle x1="14.859" y1="2.921" x2="15.19681875" y2="3.00558125" layer="21"/>
<rectangle x1="17.907" y1="2.921" x2="18.24481875" y2="3.00558125" layer="21"/>
<rectangle x1="18.83918125" y1="2.921" x2="19.26081875" y2="3.00558125" layer="21"/>
<rectangle x1="20.447" y1="2.921" x2="20.87118125" y2="3.00558125" layer="21"/>
<rectangle x1="21.463" y1="2.921" x2="21.88718125" y2="3.00558125" layer="21"/>
<rectangle x1="22.81681875" y1="2.921" x2="23.241" y2="3.00558125" layer="21"/>
<rectangle x1="23.83281875" y1="2.921" x2="24.34081875" y2="3.00558125" layer="21"/>
<rectangle x1="25.44318125" y1="2.921" x2="25.95118125" y2="3.00558125" layer="21"/>
<rectangle x1="26.797" y1="2.921" x2="27.22118125" y2="3.00558125" layer="21"/>
<rectangle x1="30.26918125" y1="2.921" x2="30.607" y2="3.00558125" layer="21"/>
<rectangle x1="30.94481875" y1="2.921" x2="31.623" y2="3.00558125" layer="21"/>
<rectangle x1="32.80918125" y1="2.921" x2="33.23081875" y2="3.00558125" layer="21"/>
<rectangle x1="34.07918125" y1="2.921" x2="34.50081875" y2="3.00558125" layer="21"/>
<rectangle x1="35.687" y1="2.921" x2="36.195" y2="3.00558125" layer="21"/>
<rectangle x1="4.52881875" y1="3.00558125" x2="4.86918125" y2="3.09041875" layer="21"/>
<rectangle x1="8.001" y1="3.00558125" x2="8.33881875" y2="3.09041875" layer="21"/>
<rectangle x1="8.763" y1="3.00558125" x2="9.60881875" y2="3.09041875" layer="21"/>
<rectangle x1="10.541" y1="3.00558125" x2="11.049" y2="3.09041875" layer="21"/>
<rectangle x1="11.72718125" y1="3.00558125" x2="12.065" y2="3.09041875" layer="21"/>
<rectangle x1="12.827" y1="3.00558125" x2="13.16481875" y2="3.09041875" layer="21"/>
<rectangle x1="14.859" y1="3.00558125" x2="15.19681875" y2="3.09041875" layer="21"/>
<rectangle x1="17.907" y1="3.00558125" x2="18.24481875" y2="3.09041875" layer="21"/>
<rectangle x1="18.83918125" y1="3.00558125" x2="19.431" y2="3.09041875" layer="21"/>
<rectangle x1="20.27681875" y1="3.00558125" x2="20.87118125" y2="3.09041875" layer="21"/>
<rectangle x1="21.54681875" y1="3.00558125" x2="21.971" y2="3.09041875" layer="21"/>
<rectangle x1="22.733" y1="3.00558125" x2="23.15718125" y2="3.09041875" layer="21"/>
<rectangle x1="23.91918125" y1="3.00558125" x2="24.42718125" y2="3.09041875" layer="21"/>
<rectangle x1="25.273" y1="3.00558125" x2="25.86481875" y2="3.09041875" layer="21"/>
<rectangle x1="26.88081875" y1="3.00558125" x2="27.22118125" y2="3.09041875" layer="21"/>
<rectangle x1="30.18281875" y1="3.00558125" x2="30.607" y2="3.09041875" layer="21"/>
<rectangle x1="30.94481875" y1="3.00558125" x2="31.79318125" y2="3.09041875" layer="21"/>
<rectangle x1="32.639" y1="3.00558125" x2="33.23081875" y2="3.09041875" layer="21"/>
<rectangle x1="34.163" y1="3.00558125" x2="34.671" y2="3.09041875" layer="21"/>
<rectangle x1="35.60318125" y1="3.00558125" x2="36.11118125" y2="3.09041875" layer="21"/>
<rectangle x1="4.52881875" y1="3.09041875" x2="4.86918125" y2="3.175" layer="21"/>
<rectangle x1="8.001" y1="3.09041875" x2="8.33881875" y2="3.175" layer="21"/>
<rectangle x1="8.763" y1="3.09041875" x2="9.10081875" y2="3.175" layer="21"/>
<rectangle x1="9.18718125" y1="3.09041875" x2="9.86281875" y2="3.175" layer="21"/>
<rectangle x1="10.20318125" y1="3.09041875" x2="10.96518125" y2="3.175" layer="21"/>
<rectangle x1="11.72718125" y1="3.09041875" x2="12.065" y2="3.175" layer="21"/>
<rectangle x1="12.827" y1="3.09041875" x2="13.16481875" y2="3.175" layer="21"/>
<rectangle x1="14.859" y1="3.09041875" x2="15.19681875" y2="3.175" layer="21"/>
<rectangle x1="17.907" y1="3.09041875" x2="18.24481875" y2="3.175" layer="21"/>
<rectangle x1="19.00681875" y1="3.09041875" x2="19.685" y2="3.175" layer="21"/>
<rectangle x1="20.02281875" y1="3.09041875" x2="20.78481875" y2="3.175" layer="21"/>
<rectangle x1="21.54681875" y1="3.09041875" x2="22.225" y2="3.175" layer="21"/>
<rectangle x1="22.56281875" y1="3.09041875" x2="23.15718125" y2="3.175" layer="21"/>
<rectangle x1="24.003" y1="3.09041875" x2="24.68118125" y2="3.175" layer="21"/>
<rectangle x1="25.10281875" y1="3.09041875" x2="25.781" y2="3.175" layer="21"/>
<rectangle x1="26.88081875" y1="3.09041875" x2="27.22118125" y2="3.175" layer="21"/>
<rectangle x1="30.18281875" y1="3.09041875" x2="30.52318125" y2="3.175" layer="21"/>
<rectangle x1="30.94481875" y1="3.09041875" x2="32.04718125" y2="3.175" layer="21"/>
<rectangle x1="32.385" y1="3.09041875" x2="33.06318125" y2="3.175" layer="21"/>
<rectangle x1="34.24681875" y1="3.09041875" x2="34.925" y2="3.175" layer="21"/>
<rectangle x1="35.26281875" y1="3.09041875" x2="36.02481875" y2="3.175" layer="21"/>
<rectangle x1="4.52881875" y1="3.175" x2="4.86918125" y2="3.25958125" layer="21"/>
<rectangle x1="8.001" y1="3.175" x2="8.33881875" y2="3.25958125" layer="21"/>
<rectangle x1="8.763" y1="3.175" x2="9.10081875" y2="3.25958125" layer="21"/>
<rectangle x1="9.271" y1="3.175" x2="10.87881875" y2="3.25958125" layer="21"/>
<rectangle x1="11.72718125" y1="3.175" x2="12.065" y2="3.25958125" layer="21"/>
<rectangle x1="12.40281875" y1="3.175" x2="14.26718125" y2="3.25958125" layer="21"/>
<rectangle x1="14.859" y1="3.175" x2="15.19681875" y2="3.25958125" layer="21"/>
<rectangle x1="17.907" y1="3.175" x2="18.24481875" y2="3.25958125" layer="21"/>
<rectangle x1="19.09318125" y1="3.175" x2="20.701" y2="3.25958125" layer="21"/>
<rectangle x1="21.63318125" y1="3.175" x2="23.07081875" y2="3.25958125" layer="21"/>
<rectangle x1="24.08681875" y1="3.175" x2="25.69718125" y2="3.25958125" layer="21"/>
<rectangle x1="26.88081875" y1="3.175" x2="27.22118125" y2="3.25958125" layer="21"/>
<rectangle x1="30.18281875" y1="3.175" x2="30.52318125" y2="3.25958125" layer="21"/>
<rectangle x1="30.94481875" y1="3.175" x2="31.28518125" y2="3.25958125" layer="21"/>
<rectangle x1="31.45281875" y1="3.175" x2="33.06318125" y2="3.25958125" layer="21"/>
<rectangle x1="34.33318125" y1="3.175" x2="35.941" y2="3.25958125" layer="21"/>
<rectangle x1="4.52881875" y1="3.25958125" x2="4.86918125" y2="3.34441875" layer="21"/>
<rectangle x1="8.001" y1="3.25958125" x2="8.33881875" y2="3.34441875" layer="21"/>
<rectangle x1="8.763" y1="3.25958125" x2="9.10081875" y2="3.34441875" layer="21"/>
<rectangle x1="9.35481875" y1="3.25958125" x2="10.71118125" y2="3.34441875" layer="21"/>
<rectangle x1="11.72718125" y1="3.25958125" x2="12.065" y2="3.34441875" layer="21"/>
<rectangle x1="12.319" y1="3.25958125" x2="14.26718125" y2="3.34441875" layer="21"/>
<rectangle x1="14.859" y1="3.25958125" x2="15.19681875" y2="3.34441875" layer="21"/>
<rectangle x1="17.82318125" y1="3.25958125" x2="18.161" y2="3.34441875" layer="21"/>
<rectangle x1="19.177" y1="3.25958125" x2="20.53081875" y2="3.34441875" layer="21"/>
<rectangle x1="21.717" y1="3.25958125" x2="22.987" y2="3.34441875" layer="21"/>
<rectangle x1="24.17318125" y1="3.25958125" x2="25.61081875" y2="3.34441875" layer="21"/>
<rectangle x1="26.88081875" y1="3.25958125" x2="27.305" y2="3.34441875" layer="21"/>
<rectangle x1="30.099" y1="3.25958125" x2="30.52318125" y2="3.34441875" layer="21"/>
<rectangle x1="30.94481875" y1="3.25958125" x2="31.28518125" y2="3.34441875" layer="21"/>
<rectangle x1="31.53918125" y1="3.25958125" x2="32.893" y2="3.34441875" layer="21"/>
<rectangle x1="34.417" y1="3.25958125" x2="35.77081875" y2="3.34441875" layer="21"/>
<rectangle x1="4.52881875" y1="3.34441875" x2="4.86918125" y2="3.429" layer="21"/>
<rectangle x1="8.001" y1="3.34441875" x2="8.33881875" y2="3.429" layer="21"/>
<rectangle x1="8.763" y1="3.34441875" x2="9.10081875" y2="3.429" layer="21"/>
<rectangle x1="9.525" y1="3.34441875" x2="10.541" y2="3.429" layer="21"/>
<rectangle x1="11.72718125" y1="3.34441875" x2="12.065" y2="3.429" layer="21"/>
<rectangle x1="12.40281875" y1="3.34441875" x2="14.26718125" y2="3.429" layer="21"/>
<rectangle x1="14.859" y1="3.34441875" x2="15.19681875" y2="3.429" layer="21"/>
<rectangle x1="17.82318125" y1="3.34441875" x2="18.161" y2="3.429" layer="21"/>
<rectangle x1="19.34718125" y1="3.34441875" x2="20.36318125" y2="3.429" layer="21"/>
<rectangle x1="21.88718125" y1="3.34441875" x2="22.81681875" y2="3.429" layer="21"/>
<rectangle x1="24.34081875" y1="3.34441875" x2="25.35681875" y2="3.429" layer="21"/>
<rectangle x1="26.96718125" y1="3.34441875" x2="27.305" y2="3.429" layer="21"/>
<rectangle x1="30.099" y1="3.34441875" x2="30.43681875" y2="3.429" layer="21"/>
<rectangle x1="30.94481875" y1="3.34441875" x2="31.28518125" y2="3.429" layer="21"/>
<rectangle x1="31.70681875" y1="3.34441875" x2="32.72281875" y2="3.429" layer="21"/>
<rectangle x1="34.58718125" y1="3.34441875" x2="35.60318125" y2="3.429" layer="21"/>
<rectangle x1="4.52881875" y1="3.429" x2="4.86918125" y2="3.51358125" layer="21"/>
<rectangle x1="8.001" y1="3.429" x2="8.33881875" y2="3.51358125" layer="21"/>
<rectangle x1="8.84681875" y1="3.429" x2="9.017" y2="3.51358125" layer="21"/>
<rectangle x1="9.779" y1="3.429" x2="10.287" y2="3.51358125" layer="21"/>
<rectangle x1="11.811" y1="3.429" x2="11.98118125" y2="3.51358125" layer="21"/>
<rectangle x1="12.40281875" y1="3.429" x2="14.18081875" y2="3.51358125" layer="21"/>
<rectangle x1="14.859" y1="3.429" x2="15.19681875" y2="3.51358125" layer="21"/>
<rectangle x1="17.82318125" y1="3.429" x2="18.161" y2="3.51358125" layer="21"/>
<rectangle x1="19.60118125" y1="3.429" x2="20.10918125" y2="3.51358125" layer="21"/>
<rectangle x1="22.14118125" y1="3.429" x2="22.56281875" y2="3.51358125" layer="21"/>
<rectangle x1="24.59481875" y1="3.429" x2="25.10281875" y2="3.51358125" layer="21"/>
<rectangle x1="26.96718125" y1="3.429" x2="27.38881875" y2="3.51358125" layer="21"/>
<rectangle x1="30.01518125" y1="3.429" x2="30.43681875" y2="3.51358125" layer="21"/>
<rectangle x1="31.03118125" y1="3.429" x2="31.19881875" y2="3.51358125" layer="21"/>
<rectangle x1="31.96081875" y1="3.429" x2="32.46881875" y2="3.51358125" layer="21"/>
<rectangle x1="34.84118125" y1="3.429" x2="35.34918125" y2="3.51358125" layer="21"/>
<rectangle x1="4.52881875" y1="3.51358125" x2="4.86918125" y2="3.59841875" layer="21"/>
<rectangle x1="8.001" y1="3.51358125" x2="8.33881875" y2="3.59841875" layer="21"/>
<rectangle x1="12.827" y1="3.51358125" x2="13.16481875" y2="3.59841875" layer="21"/>
<rectangle x1="14.859" y1="3.51358125" x2="15.19681875" y2="3.59841875" layer="21"/>
<rectangle x1="17.73681875" y1="3.51358125" x2="18.07718125" y2="3.59841875" layer="21"/>
<rectangle x1="27.051" y1="3.51358125" x2="27.47518125" y2="3.59841875" layer="21"/>
<rectangle x1="30.01518125" y1="3.51358125" x2="30.353" y2="3.59841875" layer="21"/>
<rectangle x1="4.52881875" y1="3.59841875" x2="4.86918125" y2="3.683" layer="21"/>
<rectangle x1="8.001" y1="3.59841875" x2="8.33881875" y2="3.683" layer="21"/>
<rectangle x1="11.811" y1="3.59841875" x2="11.98118125" y2="3.683" layer="21"/>
<rectangle x1="12.827" y1="3.59841875" x2="13.16481875" y2="3.683" layer="21"/>
<rectangle x1="14.859" y1="3.59841875" x2="15.19681875" y2="3.683" layer="21"/>
<rectangle x1="17.653" y1="3.59841875" x2="18.07718125" y2="3.683" layer="21"/>
<rectangle x1="27.13481875" y1="3.59841875" x2="27.47518125" y2="3.683" layer="21"/>
<rectangle x1="29.92881875" y1="3.59841875" x2="30.353" y2="3.683" layer="21"/>
<rectangle x1="4.52881875" y1="3.683" x2="4.86918125" y2="3.76758125" layer="21"/>
<rectangle x1="8.001" y1="3.683" x2="8.33881875" y2="3.76758125" layer="21"/>
<rectangle x1="11.72718125" y1="3.683" x2="12.065" y2="3.76758125" layer="21"/>
<rectangle x1="12.827" y1="3.683" x2="13.16481875" y2="3.76758125" layer="21"/>
<rectangle x1="14.859" y1="3.683" x2="15.19681875" y2="3.76758125" layer="21"/>
<rectangle x1="17.653" y1="3.683" x2="18.07718125" y2="3.76758125" layer="21"/>
<rectangle x1="27.13481875" y1="3.683" x2="27.559" y2="3.76758125" layer="21"/>
<rectangle x1="29.845" y1="3.683" x2="30.26918125" y2="3.76758125" layer="21"/>
<rectangle x1="4.52881875" y1="3.76758125" x2="4.86918125" y2="3.85241875" layer="21"/>
<rectangle x1="8.001" y1="3.76758125" x2="8.33881875" y2="3.85241875" layer="21"/>
<rectangle x1="11.72718125" y1="3.76758125" x2="12.065" y2="3.85241875" layer="21"/>
<rectangle x1="12.827" y1="3.76758125" x2="13.16481875" y2="3.85241875" layer="21"/>
<rectangle x1="14.859" y1="3.76758125" x2="15.19681875" y2="3.85241875" layer="21"/>
<rectangle x1="17.56918125" y1="3.76758125" x2="17.99081875" y2="3.85241875" layer="21"/>
<rectangle x1="27.22118125" y1="3.76758125" x2="27.72918125" y2="3.85241875" layer="21"/>
<rectangle x1="29.76118125" y1="3.76758125" x2="30.18281875" y2="3.85241875" layer="21"/>
<rectangle x1="4.52881875" y1="3.85241875" x2="4.86918125" y2="3.937" layer="21"/>
<rectangle x1="8.001" y1="3.85241875" x2="8.33881875" y2="3.937" layer="21"/>
<rectangle x1="11.72718125" y1="3.85241875" x2="12.065" y2="3.937" layer="21"/>
<rectangle x1="12.827" y1="3.85241875" x2="13.16481875" y2="3.937" layer="21"/>
<rectangle x1="14.859" y1="3.85241875" x2="15.19681875" y2="3.937" layer="21"/>
<rectangle x1="17.48281875" y1="3.85241875" x2="17.907" y2="3.937" layer="21"/>
<rectangle x1="27.305" y1="3.85241875" x2="27.813" y2="3.937" layer="21"/>
<rectangle x1="29.591" y1="3.85241875" x2="30.099" y2="3.937" layer="21"/>
<rectangle x1="4.52881875" y1="3.937" x2="4.86918125" y2="4.02158125" layer="21"/>
<rectangle x1="8.001" y1="3.937" x2="8.33881875" y2="4.02158125" layer="21"/>
<rectangle x1="11.72718125" y1="3.937" x2="12.065" y2="4.02158125" layer="21"/>
<rectangle x1="12.827" y1="3.937" x2="13.16481875" y2="4.02158125" layer="21"/>
<rectangle x1="14.859" y1="3.937" x2="15.19681875" y2="4.02158125" layer="21"/>
<rectangle x1="17.399" y1="3.937" x2="17.907" y2="4.02158125" layer="21"/>
<rectangle x1="27.38881875" y1="3.937" x2="27.89681875" y2="4.02158125" layer="21"/>
<rectangle x1="29.50718125" y1="3.937" x2="30.099" y2="4.02158125" layer="21"/>
<rectangle x1="4.52881875" y1="4.02158125" x2="4.86918125" y2="4.10641875" layer="21"/>
<rectangle x1="8.001" y1="4.02158125" x2="8.33881875" y2="4.10641875" layer="21"/>
<rectangle x1="11.811" y1="4.02158125" x2="11.98118125" y2="4.10641875" layer="21"/>
<rectangle x1="12.827" y1="4.02158125" x2="13.16481875" y2="4.10641875" layer="21"/>
<rectangle x1="14.859" y1="4.02158125" x2="15.19681875" y2="4.10641875" layer="21"/>
<rectangle x1="17.31518125" y1="4.02158125" x2="17.82318125" y2="4.10641875" layer="21"/>
<rectangle x1="27.47518125" y1="4.02158125" x2="28.067" y2="4.10641875" layer="21"/>
<rectangle x1="29.337" y1="4.02158125" x2="30.01518125" y2="4.10641875" layer="21"/>
<rectangle x1="4.52881875" y1="4.10641875" x2="4.86918125" y2="4.191" layer="21"/>
<rectangle x1="8.001" y1="4.10641875" x2="8.33881875" y2="4.191" layer="21"/>
<rectangle x1="12.827" y1="4.10641875" x2="13.16481875" y2="4.191" layer="21"/>
<rectangle x1="14.859" y1="4.10641875" x2="15.19681875" y2="4.191" layer="21"/>
<rectangle x1="17.22881875" y1="4.10641875" x2="17.73681875" y2="4.191" layer="21"/>
<rectangle x1="27.559" y1="4.10641875" x2="28.321" y2="4.191" layer="21"/>
<rectangle x1="29.083" y1="4.10641875" x2="29.845" y2="4.191" layer="21"/>
<rectangle x1="4.52881875" y1="4.191" x2="4.86918125" y2="4.27558125" layer="21"/>
<rectangle x1="8.001" y1="4.191" x2="8.33881875" y2="4.27558125" layer="21"/>
<rectangle x1="12.827" y1="4.191" x2="13.16481875" y2="4.27558125" layer="21"/>
<rectangle x1="14.859" y1="4.191" x2="15.19681875" y2="4.27558125" layer="21"/>
<rectangle x1="17.06118125" y1="4.191" x2="17.653" y2="4.27558125" layer="21"/>
<rectangle x1="27.64281875" y1="4.191" x2="29.76118125" y2="4.27558125" layer="21"/>
<rectangle x1="4.52881875" y1="4.27558125" x2="4.86918125" y2="4.36041875" layer="21"/>
<rectangle x1="8.001" y1="4.27558125" x2="8.33881875" y2="4.36041875" layer="21"/>
<rectangle x1="12.827" y1="4.27558125" x2="13.16481875" y2="4.36041875" layer="21"/>
<rectangle x1="14.94281875" y1="4.27558125" x2="15.28318125" y2="4.36041875" layer="21"/>
<rectangle x1="16.80718125" y1="4.27558125" x2="17.56918125" y2="4.36041875" layer="21"/>
<rectangle x1="27.813" y1="4.27558125" x2="29.591" y2="4.36041875" layer="21"/>
<rectangle x1="4.52881875" y1="4.36041875" x2="4.86918125" y2="4.445" layer="21"/>
<rectangle x1="8.001" y1="4.36041875" x2="8.33881875" y2="4.445" layer="21"/>
<rectangle x1="12.827" y1="4.36041875" x2="13.16481875" y2="4.445" layer="21"/>
<rectangle x1="14.859" y1="4.36041875" x2="17.399" y2="4.445" layer="21"/>
<rectangle x1="27.98318125" y1="4.36041875" x2="29.42081875" y2="4.445" layer="21"/>
<rectangle x1="4.52881875" y1="4.445" x2="4.86918125" y2="4.52958125" layer="21"/>
<rectangle x1="8.001" y1="4.445" x2="8.255" y2="4.52958125" layer="21"/>
<rectangle x1="12.827" y1="4.445" x2="13.081" y2="4.52958125" layer="21"/>
<rectangle x1="14.859" y1="4.445" x2="17.31518125" y2="4.52958125" layer="21"/>
<rectangle x1="28.321" y1="4.445" x2="29.16681875" y2="4.52958125" layer="21"/>
<rectangle x1="14.859" y1="4.52958125" x2="17.145" y2="4.61441875" layer="21"/>
<rectangle x1="14.94281875" y1="4.61441875" x2="16.891" y2="4.699" layer="21"/>
<rectangle x1="7.239" y1="6.477" x2="32.46881875" y2="6.56158125" layer="21"/>
<rectangle x1="6.64718125" y1="6.56158125" x2="33.147" y2="6.64641875" layer="21"/>
<rectangle x1="6.30681875" y1="6.64641875" x2="33.48481875" y2="6.731" layer="21"/>
<rectangle x1="5.969" y1="6.731" x2="33.82518125" y2="6.81558125" layer="21"/>
<rectangle x1="5.63118125" y1="6.81558125" x2="34.07918125" y2="6.90041875" layer="21"/>
<rectangle x1="5.37718125" y1="6.90041875" x2="34.33318125" y2="6.985" layer="21"/>
<rectangle x1="5.207" y1="6.985" x2="34.58718125" y2="7.06958125" layer="21"/>
<rectangle x1="4.953" y1="7.06958125" x2="34.75481875" y2="7.15441875" layer="21"/>
<rectangle x1="4.78281875" y1="7.15441875" x2="34.925" y2="7.239" layer="21"/>
<rectangle x1="4.61518125" y1="7.239" x2="35.179" y2="7.32358125" layer="21"/>
<rectangle x1="4.445" y1="7.32358125" x2="35.34918125" y2="7.40841875" layer="21"/>
<rectangle x1="4.27481875" y1="7.40841875" x2="35.51681875" y2="7.493" layer="21"/>
<rectangle x1="4.10718125" y1="7.493" x2="35.60318125" y2="7.57758125" layer="21"/>
<rectangle x1="4.02081875" y1="7.57758125" x2="7.15518125" y2="7.66241875" layer="21"/>
<rectangle x1="19.51481875" y1="7.57758125" x2="35.77081875" y2="7.66241875" layer="21"/>
<rectangle x1="3.85318125" y1="7.66241875" x2="6.64718125" y2="7.747" layer="21"/>
<rectangle x1="19.431" y1="7.66241875" x2="35.941" y2="7.747" layer="21"/>
<rectangle x1="3.76681875" y1="7.747" x2="6.30681875" y2="7.83158125" layer="21"/>
<rectangle x1="19.431" y1="7.747" x2="36.02481875" y2="7.83158125" layer="21"/>
<rectangle x1="3.59918125" y1="7.83158125" x2="6.05281875" y2="7.91641875" layer="21"/>
<rectangle x1="19.34718125" y1="7.83158125" x2="36.195" y2="7.91641875" layer="21"/>
<rectangle x1="3.51281875" y1="7.91641875" x2="5.79881875" y2="8.001" layer="21"/>
<rectangle x1="19.26081875" y1="7.91641875" x2="36.27881875" y2="8.001" layer="21"/>
<rectangle x1="3.34518125" y1="8.001" x2="5.54481875" y2="8.08558125" layer="21"/>
<rectangle x1="19.26081875" y1="8.001" x2="36.36518125" y2="8.08558125" layer="21"/>
<rectangle x1="3.25881875" y1="8.08558125" x2="5.37718125" y2="8.17041875" layer="21"/>
<rectangle x1="19.177" y1="8.08558125" x2="36.53281875" y2="8.17041875" layer="21"/>
<rectangle x1="3.175" y1="8.17041875" x2="5.207" y2="8.255" layer="21"/>
<rectangle x1="19.177" y1="8.17041875" x2="36.61918125" y2="8.255" layer="21"/>
<rectangle x1="3.00481875" y1="8.255" x2="5.03681875" y2="8.33958125" layer="21"/>
<rectangle x1="19.09318125" y1="8.255" x2="36.703" y2="8.33958125" layer="21"/>
<rectangle x1="3.00481875" y1="8.33958125" x2="4.86918125" y2="8.42441875" layer="21"/>
<rectangle x1="19.09318125" y1="8.33958125" x2="36.78681875" y2="8.42441875" layer="21"/>
<rectangle x1="2.83718125" y1="8.42441875" x2="4.699" y2="8.509" layer="21"/>
<rectangle x1="19.00681875" y1="8.42441875" x2="36.957" y2="8.509" layer="21"/>
<rectangle x1="2.75081875" y1="8.509" x2="4.61518125" y2="8.59358125" layer="21"/>
<rectangle x1="19.00681875" y1="8.509" x2="37.04081875" y2="8.59358125" layer="21"/>
<rectangle x1="2.667" y1="8.59358125" x2="4.445" y2="8.67841875" layer="21"/>
<rectangle x1="18.923" y1="8.59358125" x2="37.12718125" y2="8.67841875" layer="21"/>
<rectangle x1="2.58318125" y1="8.67841875" x2="4.27481875" y2="8.763" layer="21"/>
<rectangle x1="18.923" y1="8.67841875" x2="37.211" y2="8.763" layer="21"/>
<rectangle x1="2.49681875" y1="8.763" x2="4.191" y2="8.84758125" layer="21"/>
<rectangle x1="18.83918125" y1="8.763" x2="37.29481875" y2="8.84758125" layer="21"/>
<rectangle x1="2.413" y1="8.84758125" x2="4.02081875" y2="8.93241875" layer="21"/>
<rectangle x1="18.83918125" y1="8.84758125" x2="37.38118125" y2="8.93241875" layer="21"/>
<rectangle x1="2.32918125" y1="8.93241875" x2="3.937" y2="9.017" layer="21"/>
<rectangle x1="18.75281875" y1="8.93241875" x2="37.465" y2="9.017" layer="21"/>
<rectangle x1="2.24281875" y1="9.017" x2="3.85318125" y2="9.10158125" layer="21"/>
<rectangle x1="18.75281875" y1="9.017" x2="37.54881875" y2="9.10158125" layer="21"/>
<rectangle x1="2.159" y1="9.10158125" x2="3.76681875" y2="9.18641875" layer="21"/>
<rectangle x1="18.75281875" y1="9.10158125" x2="37.63518125" y2="9.18641875" layer="21"/>
<rectangle x1="2.07518125" y1="9.18641875" x2="3.683" y2="9.271" layer="21"/>
<rectangle x1="18.669" y1="9.18641875" x2="37.63518125" y2="9.271" layer="21"/>
<rectangle x1="2.07518125" y1="9.271" x2="3.51281875" y2="9.35558125" layer="21"/>
<rectangle x1="18.669" y1="9.271" x2="37.719" y2="9.35558125" layer="21"/>
<rectangle x1="1.98881875" y1="9.35558125" x2="3.429" y2="9.44041875" layer="21"/>
<rectangle x1="18.669" y1="9.35558125" x2="37.80281875" y2="9.44041875" layer="21"/>
<rectangle x1="1.905" y1="9.44041875" x2="3.34518125" y2="9.525" layer="21"/>
<rectangle x1="18.58518125" y1="9.44041875" x2="37.88918125" y2="9.525" layer="21"/>
<rectangle x1="1.82118125" y1="9.525" x2="3.25881875" y2="9.60958125" layer="21"/>
<rectangle x1="18.58518125" y1="9.525" x2="37.973" y2="9.60958125" layer="21"/>
<rectangle x1="1.73481875" y1="9.60958125" x2="3.175" y2="9.69441875" layer="21"/>
<rectangle x1="18.49881875" y1="9.60958125" x2="37.973" y2="9.69441875" layer="21"/>
<rectangle x1="1.73481875" y1="9.69441875" x2="3.09118125" y2="9.779" layer="21"/>
<rectangle x1="18.49881875" y1="9.69441875" x2="38.05681875" y2="9.779" layer="21"/>
<rectangle x1="1.651" y1="9.779" x2="3.00481875" y2="9.86358125" layer="21"/>
<rectangle x1="18.49881875" y1="9.779" x2="38.14318125" y2="9.86358125" layer="21"/>
<rectangle x1="1.56718125" y1="9.86358125" x2="2.921" y2="9.94841875" layer="21"/>
<rectangle x1="18.415" y1="9.86358125" x2="38.227" y2="9.94841875" layer="21"/>
<rectangle x1="1.56718125" y1="9.94841875" x2="2.921" y2="10.033" layer="21"/>
<rectangle x1="18.415" y1="9.94841875" x2="38.227" y2="10.033" layer="21"/>
<rectangle x1="1.48081875" y1="10.033" x2="2.83718125" y2="10.11758125" layer="21"/>
<rectangle x1="18.415" y1="10.033" x2="38.31081875" y2="10.11758125" layer="21"/>
<rectangle x1="1.397" y1="10.11758125" x2="2.75081875" y2="10.20241875" layer="21"/>
<rectangle x1="18.415" y1="10.11758125" x2="38.39718125" y2="10.20241875" layer="21"/>
<rectangle x1="1.397" y1="10.20241875" x2="2.667" y2="10.287" layer="21"/>
<rectangle x1="18.33118125" y1="10.20241875" x2="38.39718125" y2="10.287" layer="21"/>
<rectangle x1="1.31318125" y1="10.287" x2="2.58318125" y2="10.37158125" layer="21"/>
<rectangle x1="18.33118125" y1="10.287" x2="38.481" y2="10.37158125" layer="21"/>
<rectangle x1="1.22681875" y1="10.37158125" x2="2.58318125" y2="10.45641875" layer="21"/>
<rectangle x1="18.33118125" y1="10.37158125" x2="38.481" y2="10.45641875" layer="21"/>
<rectangle x1="1.22681875" y1="10.45641875" x2="2.49681875" y2="10.541" layer="21"/>
<rectangle x1="18.33118125" y1="10.45641875" x2="38.56481875" y2="10.541" layer="21"/>
<rectangle x1="1.143" y1="10.541" x2="2.413" y2="10.62558125" layer="21"/>
<rectangle x1="18.24481875" y1="10.541" x2="38.56481875" y2="10.62558125" layer="21"/>
<rectangle x1="1.143" y1="10.62558125" x2="2.413" y2="10.71041875" layer="21"/>
<rectangle x1="18.24481875" y1="10.62558125" x2="38.65118125" y2="10.71041875" layer="21"/>
<rectangle x1="1.05918125" y1="10.71041875" x2="2.32918125" y2="10.795" layer="21"/>
<rectangle x1="18.24481875" y1="10.71041875" x2="38.65118125" y2="10.795" layer="21"/>
<rectangle x1="1.05918125" y1="10.795" x2="2.24281875" y2="10.87958125" layer="21"/>
<rectangle x1="18.24481875" y1="10.795" x2="38.735" y2="10.87958125" layer="21"/>
<rectangle x1="0.97281875" y1="10.87958125" x2="2.24281875" y2="10.96441875" layer="21"/>
<rectangle x1="18.24481875" y1="10.87958125" x2="38.81881875" y2="10.96441875" layer="21"/>
<rectangle x1="0.97281875" y1="10.96441875" x2="2.159" y2="11.049" layer="21"/>
<rectangle x1="18.161" y1="10.96441875" x2="38.81881875" y2="11.049" layer="21"/>
<rectangle x1="0.889" y1="11.049" x2="2.159" y2="11.13358125" layer="21"/>
<rectangle x1="18.161" y1="11.049" x2="38.90518125" y2="11.13358125" layer="21"/>
<rectangle x1="0.889" y1="11.13358125" x2="2.07518125" y2="11.21841875" layer="21"/>
<rectangle x1="7.40918125" y1="11.13358125" x2="8.763" y2="11.21841875" layer="21"/>
<rectangle x1="11.557" y1="11.13358125" x2="13.92681875" y2="11.21841875" layer="21"/>
<rectangle x1="18.161" y1="11.13358125" x2="21.63318125" y2="11.21841875" layer="21"/>
<rectangle x1="23.07081875" y1="11.13358125" x2="33.06318125" y2="11.21841875" layer="21"/>
<rectangle x1="33.147" y1="11.13358125" x2="38.90518125" y2="11.21841875" layer="21"/>
<rectangle x1="0.80518125" y1="11.21841875" x2="2.07518125" y2="11.303" layer="21"/>
<rectangle x1="7.06881875" y1="11.21841875" x2="9.10081875" y2="11.303" layer="21"/>
<rectangle x1="11.47318125" y1="11.21841875" x2="14.18081875" y2="11.303" layer="21"/>
<rectangle x1="18.161" y1="11.21841875" x2="21.29281875" y2="11.303" layer="21"/>
<rectangle x1="23.32481875" y1="11.21841875" x2="26.45918125" y2="11.303" layer="21"/>
<rectangle x1="26.88081875" y1="11.21841875" x2="29.92881875" y2="11.303" layer="21"/>
<rectangle x1="30.43681875" y1="11.21841875" x2="32.639" y2="11.303" layer="21"/>
<rectangle x1="33.82518125" y1="11.21841875" x2="38.90518125" y2="11.303" layer="21"/>
<rectangle x1="0.80518125" y1="11.303" x2="1.98881875" y2="11.38758125" layer="21"/>
<rectangle x1="6.81481875" y1="11.303" x2="9.35481875" y2="11.38758125" layer="21"/>
<rectangle x1="11.38681875" y1="11.303" x2="14.43481875" y2="11.38758125" layer="21"/>
<rectangle x1="18.161" y1="11.303" x2="21.12518125" y2="11.38758125" layer="21"/>
<rectangle x1="23.57881875" y1="11.303" x2="26.37281875" y2="11.38758125" layer="21"/>
<rectangle x1="27.051" y1="11.303" x2="29.845" y2="11.38758125" layer="21"/>
<rectangle x1="30.52318125" y1="11.303" x2="32.385" y2="11.38758125" layer="21"/>
<rectangle x1="34.163" y1="11.303" x2="38.989" y2="11.38758125" layer="21"/>
<rectangle x1="0.80518125" y1="11.38758125" x2="1.98881875" y2="11.47241875" layer="21"/>
<rectangle x1="6.64718125" y1="11.38758125" x2="9.525" y2="11.47241875" layer="21"/>
<rectangle x1="11.38681875" y1="11.38758125" x2="14.605" y2="11.47241875" layer="21"/>
<rectangle x1="18.07718125" y1="11.38758125" x2="20.955" y2="11.47241875" layer="21"/>
<rectangle x1="23.749" y1="11.38758125" x2="26.289" y2="11.47241875" layer="21"/>
<rectangle x1="27.051" y1="11.38758125" x2="29.76118125" y2="11.47241875" layer="21"/>
<rectangle x1="30.52318125" y1="11.38758125" x2="32.21481875" y2="11.47241875" layer="21"/>
<rectangle x1="34.33318125" y1="11.38758125" x2="38.989" y2="11.47241875" layer="21"/>
<rectangle x1="0.71881875" y1="11.47241875" x2="1.905" y2="11.557" layer="21"/>
<rectangle x1="6.477" y1="11.47241875" x2="9.69518125" y2="11.557" layer="21"/>
<rectangle x1="11.303" y1="11.47241875" x2="14.859" y2="11.557" layer="21"/>
<rectangle x1="18.07718125" y1="11.47241875" x2="20.78481875" y2="11.557" layer="21"/>
<rectangle x1="23.91918125" y1="11.47241875" x2="26.20518125" y2="11.557" layer="21"/>
<rectangle x1="27.13481875" y1="11.47241875" x2="29.76118125" y2="11.557" layer="21"/>
<rectangle x1="30.607" y1="11.47241875" x2="32.04718125" y2="11.557" layer="21"/>
<rectangle x1="34.50081875" y1="11.47241875" x2="39.07281875" y2="11.557" layer="21"/>
<rectangle x1="0.71881875" y1="11.557" x2="1.905" y2="11.64158125" layer="21"/>
<rectangle x1="6.39318125" y1="11.557" x2="9.779" y2="11.64158125" layer="21"/>
<rectangle x1="11.303" y1="11.557" x2="14.94281875" y2="11.64158125" layer="21"/>
<rectangle x1="18.07718125" y1="11.557" x2="20.61718125" y2="11.64158125" layer="21"/>
<rectangle x1="24.08681875" y1="11.557" x2="26.20518125" y2="11.64158125" layer="21"/>
<rectangle x1="27.13481875" y1="11.557" x2="29.76118125" y2="11.64158125" layer="21"/>
<rectangle x1="30.607" y1="11.557" x2="31.96081875" y2="11.64158125" layer="21"/>
<rectangle x1="34.671" y1="11.557" x2="39.07281875" y2="11.64158125" layer="21"/>
<rectangle x1="0.635" y1="11.64158125" x2="1.82118125" y2="11.72641875" layer="21"/>
<rectangle x1="6.223" y1="11.64158125" x2="9.94918125" y2="11.72641875" layer="21"/>
<rectangle x1="11.303" y1="11.64158125" x2="15.02918125" y2="11.72641875" layer="21"/>
<rectangle x1="18.07718125" y1="11.64158125" x2="20.53081875" y2="11.72641875" layer="21"/>
<rectangle x1="24.17318125" y1="11.64158125" x2="26.20518125" y2="11.72641875" layer="21"/>
<rectangle x1="27.13481875" y1="11.64158125" x2="29.76118125" y2="11.72641875" layer="21"/>
<rectangle x1="30.607" y1="11.64158125" x2="31.877" y2="11.72641875" layer="21"/>
<rectangle x1="34.75481875" y1="11.64158125" x2="39.07281875" y2="11.72641875" layer="21"/>
<rectangle x1="0.635" y1="11.72641875" x2="1.82118125" y2="11.811" layer="21"/>
<rectangle x1="6.13918125" y1="11.72641875" x2="10.033" y2="11.811" layer="21"/>
<rectangle x1="11.303" y1="11.72641875" x2="15.19681875" y2="11.811" layer="21"/>
<rectangle x1="18.07718125" y1="11.72641875" x2="20.36318125" y2="11.811" layer="21"/>
<rectangle x1="24.34081875" y1="11.72641875" x2="26.20518125" y2="11.811" layer="21"/>
<rectangle x1="27.13481875" y1="11.72641875" x2="29.76118125" y2="11.811" layer="21"/>
<rectangle x1="30.607" y1="11.72641875" x2="31.79318125" y2="11.811" layer="21"/>
<rectangle x1="34.84118125" y1="11.72641875" x2="39.15918125" y2="11.811" layer="21"/>
<rectangle x1="0.635" y1="11.811" x2="1.73481875" y2="11.89558125" layer="21"/>
<rectangle x1="5.969" y1="11.811" x2="10.11681875" y2="11.89558125" layer="21"/>
<rectangle x1="11.38681875" y1="11.811" x2="15.28318125" y2="11.89558125" layer="21"/>
<rectangle x1="18.07718125" y1="11.811" x2="20.27681875" y2="11.89558125" layer="21"/>
<rectangle x1="24.42718125" y1="11.811" x2="26.20518125" y2="11.89558125" layer="21"/>
<rectangle x1="27.13481875" y1="11.811" x2="29.76118125" y2="11.89558125" layer="21"/>
<rectangle x1="30.607" y1="11.811" x2="31.623" y2="11.89558125" layer="21"/>
<rectangle x1="35.00881875" y1="11.811" x2="39.15918125" y2="11.89558125" layer="21"/>
<rectangle x1="0.55118125" y1="11.89558125" x2="1.73481875" y2="11.98041875" layer="21"/>
<rectangle x1="5.88518125" y1="11.89558125" x2="10.287" y2="11.98041875" layer="21"/>
<rectangle x1="11.38681875" y1="11.89558125" x2="15.367" y2="11.98041875" layer="21"/>
<rectangle x1="17.99081875" y1="11.89558125" x2="20.193" y2="11.98041875" layer="21"/>
<rectangle x1="24.511" y1="11.89558125" x2="26.20518125" y2="11.98041875" layer="21"/>
<rectangle x1="27.13481875" y1="11.89558125" x2="29.76118125" y2="11.98041875" layer="21"/>
<rectangle x1="30.607" y1="11.89558125" x2="31.623" y2="11.98041875" layer="21"/>
<rectangle x1="35.00881875" y1="11.89558125" x2="39.15918125" y2="11.98041875" layer="21"/>
<rectangle x1="0.55118125" y1="11.98041875" x2="1.73481875" y2="12.065" layer="21"/>
<rectangle x1="5.79881875" y1="11.98041875" x2="10.37081875" y2="12.065" layer="21"/>
<rectangle x1="11.47318125" y1="11.98041875" x2="15.45081875" y2="12.065" layer="21"/>
<rectangle x1="17.99081875" y1="11.98041875" x2="20.10918125" y2="12.065" layer="21"/>
<rectangle x1="24.59481875" y1="11.98041875" x2="26.20518125" y2="12.065" layer="21"/>
<rectangle x1="27.13481875" y1="11.98041875" x2="29.76118125" y2="12.065" layer="21"/>
<rectangle x1="30.607" y1="11.98041875" x2="31.53918125" y2="12.065" layer="21"/>
<rectangle x1="35.09518125" y1="11.98041875" x2="39.243" y2="12.065" layer="21"/>
<rectangle x1="0.55118125" y1="12.065" x2="1.651" y2="12.14958125" layer="21"/>
<rectangle x1="5.715" y1="12.065" x2="10.45718125" y2="12.14958125" layer="21"/>
<rectangle x1="11.557" y1="12.065" x2="15.53718125" y2="12.14958125" layer="21"/>
<rectangle x1="17.99081875" y1="12.065" x2="20.02281875" y2="12.14958125" layer="21"/>
<rectangle x1="24.68118125" y1="12.065" x2="26.20518125" y2="12.14958125" layer="21"/>
<rectangle x1="27.13481875" y1="12.065" x2="29.76118125" y2="12.14958125" layer="21"/>
<rectangle x1="30.607" y1="12.065" x2="31.45281875" y2="12.14958125" layer="21"/>
<rectangle x1="32.80918125" y1="12.065" x2="33.655" y2="12.14958125" layer="21"/>
<rectangle x1="35.179" y1="12.065" x2="39.243" y2="12.14958125" layer="21"/>
<rectangle x1="0.46481875" y1="12.14958125" x2="1.651" y2="12.23441875" layer="21"/>
<rectangle x1="5.63118125" y1="12.14958125" x2="7.91718125" y2="12.23441875" layer="21"/>
<rectangle x1="8.255" y1="12.14958125" x2="10.541" y2="12.23441875" layer="21"/>
<rectangle x1="13.335" y1="12.14958125" x2="15.621" y2="12.23441875" layer="21"/>
<rectangle x1="17.99081875" y1="12.14958125" x2="19.939" y2="12.23441875" layer="21"/>
<rectangle x1="22.225" y1="12.14958125" x2="22.56281875" y2="12.23441875" layer="21"/>
<rectangle x1="24.765" y1="12.14958125" x2="26.20518125" y2="12.23441875" layer="21"/>
<rectangle x1="27.13481875" y1="12.14958125" x2="29.76118125" y2="12.23441875" layer="21"/>
<rectangle x1="30.607" y1="12.14958125" x2="31.369" y2="12.23441875" layer="21"/>
<rectangle x1="32.639" y1="12.14958125" x2="33.909" y2="12.23441875" layer="21"/>
<rectangle x1="35.179" y1="12.14958125" x2="39.243" y2="12.23441875" layer="21"/>
<rectangle x1="0.46481875" y1="12.23441875" x2="1.651" y2="12.319" layer="21"/>
<rectangle x1="5.54481875" y1="12.23441875" x2="7.40918125" y2="12.319" layer="21"/>
<rectangle x1="8.763" y1="12.23441875" x2="10.62481875" y2="12.319" layer="21"/>
<rectangle x1="13.843" y1="12.23441875" x2="15.70481875" y2="12.319" layer="21"/>
<rectangle x1="17.99081875" y1="12.23441875" x2="19.85518125" y2="12.319" layer="21"/>
<rectangle x1="21.717" y1="12.23441875" x2="22.987" y2="12.319" layer="21"/>
<rectangle x1="24.84881875" y1="12.23441875" x2="26.20518125" y2="12.319" layer="21"/>
<rectangle x1="27.13481875" y1="12.23441875" x2="29.76118125" y2="12.319" layer="21"/>
<rectangle x1="30.607" y1="12.23441875" x2="31.369" y2="12.319" layer="21"/>
<rectangle x1="32.46881875" y1="12.23441875" x2="34.07918125" y2="12.319" layer="21"/>
<rectangle x1="35.179" y1="12.23441875" x2="39.32681875" y2="12.319" layer="21"/>
<rectangle x1="0.46481875" y1="12.319" x2="1.56718125" y2="12.40358125" layer="21"/>
<rectangle x1="5.54481875" y1="12.319" x2="7.239" y2="12.40358125" layer="21"/>
<rectangle x1="8.93318125" y1="12.319" x2="10.62481875" y2="12.40358125" layer="21"/>
<rectangle x1="14.097" y1="12.319" x2="15.79118125" y2="12.40358125" layer="21"/>
<rectangle x1="17.99081875" y1="12.319" x2="19.76881875" y2="12.40358125" layer="21"/>
<rectangle x1="21.463" y1="12.319" x2="23.241" y2="12.40358125" layer="21"/>
<rectangle x1="24.93518125" y1="12.319" x2="26.20518125" y2="12.40358125" layer="21"/>
<rectangle x1="27.13481875" y1="12.319" x2="29.76118125" y2="12.40358125" layer="21"/>
<rectangle x1="30.607" y1="12.319" x2="31.28518125" y2="12.40358125" layer="21"/>
<rectangle x1="32.385" y1="12.319" x2="34.24681875" y2="12.40358125" layer="21"/>
<rectangle x1="35.179" y1="12.319" x2="39.32681875" y2="12.40358125" layer="21"/>
<rectangle x1="0.46481875" y1="12.40358125" x2="1.56718125" y2="12.48841875" layer="21"/>
<rectangle x1="5.461" y1="12.40358125" x2="7.06881875" y2="12.48841875" layer="21"/>
<rectangle x1="9.10081875" y1="12.40358125" x2="10.71118125" y2="12.48841875" layer="21"/>
<rectangle x1="14.26718125" y1="12.40358125" x2="15.875" y2="12.48841875" layer="21"/>
<rectangle x1="17.99081875" y1="12.40358125" x2="19.685" y2="12.48841875" layer="21"/>
<rectangle x1="21.29281875" y1="12.40358125" x2="23.41118125" y2="12.48841875" layer="21"/>
<rectangle x1="25.019" y1="12.40358125" x2="26.20518125" y2="12.48841875" layer="21"/>
<rectangle x1="27.13481875" y1="12.40358125" x2="29.76118125" y2="12.48841875" layer="21"/>
<rectangle x1="30.607" y1="12.40358125" x2="31.28518125" y2="12.48841875" layer="21"/>
<rectangle x1="32.30118125" y1="12.40358125" x2="34.33318125" y2="12.48841875" layer="21"/>
<rectangle x1="35.09518125" y1="12.40358125" x2="39.32681875" y2="12.48841875" layer="21"/>
<rectangle x1="0.46481875" y1="12.48841875" x2="1.56718125" y2="12.573" layer="21"/>
<rectangle x1="5.37718125" y1="12.48841875" x2="6.90118125" y2="12.573" layer="21"/>
<rectangle x1="9.271" y1="12.48841875" x2="10.795" y2="12.573" layer="21"/>
<rectangle x1="14.351" y1="12.48841875" x2="15.875" y2="12.573" layer="21"/>
<rectangle x1="17.907" y1="12.48841875" x2="19.685" y2="12.573" layer="21"/>
<rectangle x1="21.209" y1="12.48841875" x2="23.495" y2="12.573" layer="21"/>
<rectangle x1="25.019" y1="12.48841875" x2="26.20518125" y2="12.573" layer="21"/>
<rectangle x1="27.13481875" y1="12.48841875" x2="29.76118125" y2="12.573" layer="21"/>
<rectangle x1="30.607" y1="12.48841875" x2="31.19881875" y2="12.573" layer="21"/>
<rectangle x1="32.21481875" y1="12.48841875" x2="34.417" y2="12.573" layer="21"/>
<rectangle x1="35.09518125" y1="12.48841875" x2="39.32681875" y2="12.573" layer="21"/>
<rectangle x1="0.381" y1="12.573" x2="1.48081875" y2="12.65758125" layer="21"/>
<rectangle x1="5.29081875" y1="12.573" x2="6.731" y2="12.65758125" layer="21"/>
<rectangle x1="9.35481875" y1="12.573" x2="10.87881875" y2="12.65758125" layer="21"/>
<rectangle x1="14.52118125" y1="12.573" x2="15.95881875" y2="12.65758125" layer="21"/>
<rectangle x1="17.907" y1="12.573" x2="19.60118125" y2="12.65758125" layer="21"/>
<rectangle x1="21.03881875" y1="12.573" x2="23.66518125" y2="12.65758125" layer="21"/>
<rectangle x1="25.10281875" y1="12.573" x2="26.20518125" y2="12.65758125" layer="21"/>
<rectangle x1="27.13481875" y1="12.573" x2="29.76118125" y2="12.65758125" layer="21"/>
<rectangle x1="30.607" y1="12.573" x2="31.19881875" y2="12.65758125" layer="21"/>
<rectangle x1="32.131" y1="12.573" x2="34.50081875" y2="12.65758125" layer="21"/>
<rectangle x1="35.00881875" y1="12.573" x2="39.32681875" y2="12.65758125" layer="21"/>
<rectangle x1="0.381" y1="12.65758125" x2="1.48081875" y2="12.74241875" layer="21"/>
<rectangle x1="5.29081875" y1="12.65758125" x2="6.64718125" y2="12.74241875" layer="21"/>
<rectangle x1="9.525" y1="12.65758125" x2="10.87881875" y2="12.74241875" layer="21"/>
<rectangle x1="14.605" y1="12.65758125" x2="16.04518125" y2="12.74241875" layer="21"/>
<rectangle x1="17.907" y1="12.65758125" x2="19.51481875" y2="12.74241875" layer="21"/>
<rectangle x1="20.955" y1="12.65758125" x2="23.749" y2="12.74241875" layer="21"/>
<rectangle x1="25.18918125" y1="12.65758125" x2="26.20518125" y2="12.74241875" layer="21"/>
<rectangle x1="27.13481875" y1="12.65758125" x2="29.76118125" y2="12.74241875" layer="21"/>
<rectangle x1="30.607" y1="12.65758125" x2="31.19881875" y2="12.74241875" layer="21"/>
<rectangle x1="32.131" y1="12.65758125" x2="39.41318125" y2="12.74241875" layer="21"/>
<rectangle x1="0.381" y1="12.74241875" x2="1.48081875" y2="12.827" layer="21"/>
<rectangle x1="5.207" y1="12.74241875" x2="6.56081875" y2="12.827" layer="21"/>
<rectangle x1="9.60881875" y1="12.74241875" x2="10.87881875" y2="12.827" layer="21"/>
<rectangle x1="14.68881875" y1="12.74241875" x2="16.04518125" y2="12.827" layer="21"/>
<rectangle x1="17.907" y1="12.74241875" x2="19.51481875" y2="12.827" layer="21"/>
<rectangle x1="20.87118125" y1="12.74241875" x2="23.83281875" y2="12.827" layer="21"/>
<rectangle x1="25.18918125" y1="12.74241875" x2="26.20518125" y2="12.827" layer="21"/>
<rectangle x1="27.13481875" y1="12.74241875" x2="29.76118125" y2="12.827" layer="21"/>
<rectangle x1="30.607" y1="12.74241875" x2="31.115" y2="12.827" layer="21"/>
<rectangle x1="32.04718125" y1="12.74241875" x2="39.41318125" y2="12.827" layer="21"/>
<rectangle x1="0.381" y1="12.827" x2="1.48081875" y2="12.91158125" layer="21"/>
<rectangle x1="5.207" y1="12.827" x2="6.477" y2="12.91158125" layer="21"/>
<rectangle x1="9.69518125" y1="12.827" x2="10.96518125" y2="12.91158125" layer="21"/>
<rectangle x1="14.77518125" y1="12.827" x2="16.129" y2="12.91158125" layer="21"/>
<rectangle x1="17.907" y1="12.827" x2="19.431" y2="12.91158125" layer="21"/>
<rectangle x1="20.701" y1="12.827" x2="23.91918125" y2="12.91158125" layer="21"/>
<rectangle x1="25.273" y1="12.827" x2="26.20518125" y2="12.91158125" layer="21"/>
<rectangle x1="27.13481875" y1="12.827" x2="29.76118125" y2="12.91158125" layer="21"/>
<rectangle x1="30.607" y1="12.827" x2="31.115" y2="12.91158125" layer="21"/>
<rectangle x1="32.04718125" y1="12.827" x2="39.41318125" y2="12.91158125" layer="21"/>
<rectangle x1="0.29718125" y1="12.91158125" x2="1.397" y2="12.99641875" layer="21"/>
<rectangle x1="5.12318125" y1="12.91158125" x2="6.39318125" y2="12.99641875" layer="21"/>
<rectangle x1="9.779" y1="12.91158125" x2="11.049" y2="12.99641875" layer="21"/>
<rectangle x1="14.859" y1="12.91158125" x2="16.129" y2="12.99641875" layer="21"/>
<rectangle x1="17.907" y1="12.91158125" x2="19.431" y2="12.99641875" layer="21"/>
<rectangle x1="20.701" y1="12.91158125" x2="24.003" y2="12.99641875" layer="21"/>
<rectangle x1="25.273" y1="12.91158125" x2="26.20518125" y2="12.99641875" layer="21"/>
<rectangle x1="27.13481875" y1="12.91158125" x2="29.76118125" y2="12.99641875" layer="21"/>
<rectangle x1="30.607" y1="12.91158125" x2="31.115" y2="12.99641875" layer="21"/>
<rectangle x1="35.179" y1="12.91158125" x2="39.41318125" y2="12.99641875" layer="21"/>
<rectangle x1="0.29718125" y1="12.99641875" x2="1.397" y2="13.081" layer="21"/>
<rectangle x1="5.12318125" y1="12.99641875" x2="6.30681875" y2="13.081" layer="21"/>
<rectangle x1="9.779" y1="12.99641875" x2="11.049" y2="13.081" layer="21"/>
<rectangle x1="14.94281875" y1="12.99641875" x2="16.21281875" y2="13.081" layer="21"/>
<rectangle x1="17.907" y1="12.99641875" x2="19.34718125" y2="13.081" layer="21"/>
<rectangle x1="20.61718125" y1="12.99641875" x2="24.08681875" y2="13.081" layer="21"/>
<rectangle x1="25.35681875" y1="12.99641875" x2="26.20518125" y2="13.081" layer="21"/>
<rectangle x1="27.13481875" y1="12.99641875" x2="29.76118125" y2="13.081" layer="21"/>
<rectangle x1="30.607" y1="12.99641875" x2="31.115" y2="13.081" layer="21"/>
<rectangle x1="35.26281875" y1="12.99641875" x2="39.41318125" y2="13.081" layer="21"/>
<rectangle x1="0.29718125" y1="13.081" x2="1.397" y2="13.16558125" layer="21"/>
<rectangle x1="5.03681875" y1="13.081" x2="6.30681875" y2="13.16558125" layer="21"/>
<rectangle x1="9.86281875" y1="13.081" x2="11.049" y2="13.16558125" layer="21"/>
<rectangle x1="15.02918125" y1="13.081" x2="16.21281875" y2="13.16558125" layer="21"/>
<rectangle x1="17.907" y1="13.081" x2="19.34718125" y2="13.16558125" layer="21"/>
<rectangle x1="20.53081875" y1="13.081" x2="24.17318125" y2="13.16558125" layer="21"/>
<rectangle x1="25.35681875" y1="13.081" x2="26.20518125" y2="13.16558125" layer="21"/>
<rectangle x1="27.13481875" y1="13.081" x2="29.76118125" y2="13.16558125" layer="21"/>
<rectangle x1="30.607" y1="13.081" x2="31.115" y2="13.16558125" layer="21"/>
<rectangle x1="35.34918125" y1="13.081" x2="39.41318125" y2="13.16558125" layer="21"/>
<rectangle x1="0.29718125" y1="13.16558125" x2="1.397" y2="13.25041875" layer="21"/>
<rectangle x1="5.03681875" y1="13.16558125" x2="6.223" y2="13.25041875" layer="21"/>
<rectangle x1="9.94918125" y1="13.16558125" x2="11.13281875" y2="13.25041875" layer="21"/>
<rectangle x1="15.02918125" y1="13.16558125" x2="16.21281875" y2="13.25041875" layer="21"/>
<rectangle x1="17.907" y1="13.16558125" x2="19.26081875" y2="13.25041875" layer="21"/>
<rectangle x1="20.53081875" y1="13.16558125" x2="24.17318125" y2="13.25041875" layer="21"/>
<rectangle x1="25.44318125" y1="13.16558125" x2="26.20518125" y2="13.25041875" layer="21"/>
<rectangle x1="27.13481875" y1="13.16558125" x2="29.76118125" y2="13.25041875" layer="21"/>
<rectangle x1="30.607" y1="13.16558125" x2="31.03118125" y2="13.25041875" layer="21"/>
<rectangle x1="35.34918125" y1="13.16558125" x2="39.41318125" y2="13.25041875" layer="21"/>
<rectangle x1="0.29718125" y1="13.25041875" x2="1.397" y2="13.335" layer="21"/>
<rectangle x1="5.03681875" y1="13.25041875" x2="6.13918125" y2="13.335" layer="21"/>
<rectangle x1="9.94918125" y1="13.25041875" x2="11.13281875" y2="13.335" layer="21"/>
<rectangle x1="15.113" y1="13.25041875" x2="16.29918125" y2="13.335" layer="21"/>
<rectangle x1="17.907" y1="13.25041875" x2="19.26081875" y2="13.335" layer="21"/>
<rectangle x1="20.447" y1="13.25041875" x2="24.257" y2="13.335" layer="21"/>
<rectangle x1="25.44318125" y1="13.25041875" x2="26.20518125" y2="13.335" layer="21"/>
<rectangle x1="27.13481875" y1="13.25041875" x2="29.76118125" y2="13.335" layer="21"/>
<rectangle x1="30.607" y1="13.25041875" x2="31.03118125" y2="13.335" layer="21"/>
<rectangle x1="35.433" y1="13.25041875" x2="39.41318125" y2="13.335" layer="21"/>
<rectangle x1="0.29718125" y1="13.335" x2="1.31318125" y2="13.41958125" layer="21"/>
<rectangle x1="4.953" y1="13.335" x2="6.13918125" y2="13.41958125" layer="21"/>
<rectangle x1="10.033" y1="13.335" x2="11.21918125" y2="13.41958125" layer="21"/>
<rectangle x1="15.19681875" y1="13.335" x2="16.29918125" y2="13.41958125" layer="21"/>
<rectangle x1="17.907" y1="13.335" x2="19.26081875" y2="13.41958125" layer="21"/>
<rectangle x1="20.36318125" y1="13.335" x2="24.34081875" y2="13.41958125" layer="21"/>
<rectangle x1="25.44318125" y1="13.335" x2="26.20518125" y2="13.41958125" layer="21"/>
<rectangle x1="27.13481875" y1="13.335" x2="29.76118125" y2="13.41958125" layer="21"/>
<rectangle x1="30.607" y1="13.335" x2="31.03118125" y2="13.41958125" layer="21"/>
<rectangle x1="35.433" y1="13.335" x2="39.41318125" y2="13.41958125" layer="21"/>
<rectangle x1="0.29718125" y1="13.41958125" x2="1.31318125" y2="13.50441875" layer="21"/>
<rectangle x1="4.953" y1="13.41958125" x2="6.05281875" y2="13.50441875" layer="21"/>
<rectangle x1="10.033" y1="13.41958125" x2="11.21918125" y2="13.50441875" layer="21"/>
<rectangle x1="15.19681875" y1="13.41958125" x2="16.29918125" y2="13.50441875" layer="21"/>
<rectangle x1="17.907" y1="13.41958125" x2="19.177" y2="13.50441875" layer="21"/>
<rectangle x1="20.36318125" y1="13.41958125" x2="24.34081875" y2="13.50441875" layer="21"/>
<rectangle x1="25.44318125" y1="13.41958125" x2="26.20518125" y2="13.50441875" layer="21"/>
<rectangle x1="27.13481875" y1="13.41958125" x2="29.76118125" y2="13.50441875" layer="21"/>
<rectangle x1="30.607" y1="13.41958125" x2="31.03118125" y2="13.50441875" layer="21"/>
<rectangle x1="35.433" y1="13.41958125" x2="39.41318125" y2="13.50441875" layer="21"/>
<rectangle x1="0.21081875" y1="13.50441875" x2="1.31318125" y2="13.589" layer="21"/>
<rectangle x1="4.953" y1="13.50441875" x2="6.05281875" y2="13.589" layer="21"/>
<rectangle x1="10.11681875" y1="13.50441875" x2="11.21918125" y2="13.589" layer="21"/>
<rectangle x1="15.19681875" y1="13.50441875" x2="16.383" y2="13.589" layer="21"/>
<rectangle x1="17.907" y1="13.50441875" x2="19.177" y2="13.589" layer="21"/>
<rectangle x1="20.36318125" y1="13.50441875" x2="24.34081875" y2="13.589" layer="21"/>
<rectangle x1="25.527" y1="13.50441875" x2="26.20518125" y2="13.589" layer="21"/>
<rectangle x1="27.13481875" y1="13.50441875" x2="29.67481875" y2="13.589" layer="21"/>
<rectangle x1="30.607" y1="13.50441875" x2="31.03118125" y2="13.589" layer="21"/>
<rectangle x1="35.433" y1="13.50441875" x2="39.41318125" y2="13.589" layer="21"/>
<rectangle x1="0.21081875" y1="13.589" x2="1.31318125" y2="13.67358125" layer="21"/>
<rectangle x1="4.86918125" y1="13.589" x2="6.05281875" y2="13.67358125" layer="21"/>
<rectangle x1="10.11681875" y1="13.589" x2="11.21918125" y2="13.67358125" layer="21"/>
<rectangle x1="15.28318125" y1="13.589" x2="16.383" y2="13.67358125" layer="21"/>
<rectangle x1="17.907" y1="13.589" x2="19.177" y2="13.67358125" layer="21"/>
<rectangle x1="20.27681875" y1="13.589" x2="24.42718125" y2="13.67358125" layer="21"/>
<rectangle x1="25.527" y1="13.589" x2="26.20518125" y2="13.67358125" layer="21"/>
<rectangle x1="27.13481875" y1="13.589" x2="29.67481875" y2="13.67358125" layer="21"/>
<rectangle x1="30.607" y1="13.589" x2="31.115" y2="13.67358125" layer="21"/>
<rectangle x1="35.433" y1="13.589" x2="39.41318125" y2="13.67358125" layer="21"/>
<rectangle x1="0.21081875" y1="13.67358125" x2="1.31318125" y2="13.75841875" layer="21"/>
<rectangle x1="4.86918125" y1="13.67358125" x2="5.969" y2="13.75841875" layer="21"/>
<rectangle x1="10.20318125" y1="13.67358125" x2="11.303" y2="13.75841875" layer="21"/>
<rectangle x1="15.28318125" y1="13.67358125" x2="16.383" y2="13.75841875" layer="21"/>
<rectangle x1="17.82318125" y1="13.67358125" x2="19.177" y2="13.75841875" layer="21"/>
<rectangle x1="20.27681875" y1="13.67358125" x2="24.42718125" y2="13.75841875" layer="21"/>
<rectangle x1="25.527" y1="13.67358125" x2="26.20518125" y2="13.75841875" layer="21"/>
<rectangle x1="27.13481875" y1="13.67358125" x2="29.67481875" y2="13.75841875" layer="21"/>
<rectangle x1="30.52318125" y1="13.67358125" x2="31.115" y2="13.75841875" layer="21"/>
<rectangle x1="35.433" y1="13.67358125" x2="39.41318125" y2="13.75841875" layer="21"/>
<rectangle x1="0.21081875" y1="13.75841875" x2="1.31318125" y2="13.843" layer="21"/>
<rectangle x1="4.86918125" y1="13.75841875" x2="5.969" y2="13.843" layer="21"/>
<rectangle x1="10.20318125" y1="13.75841875" x2="11.303" y2="13.843" layer="21"/>
<rectangle x1="15.28318125" y1="13.75841875" x2="16.383" y2="13.843" layer="21"/>
<rectangle x1="17.82318125" y1="13.75841875" x2="19.177" y2="13.843" layer="21"/>
<rectangle x1="20.27681875" y1="13.75841875" x2="24.42718125" y2="13.843" layer="21"/>
<rectangle x1="25.527" y1="13.75841875" x2="26.20518125" y2="13.843" layer="21"/>
<rectangle x1="27.22118125" y1="13.75841875" x2="29.67481875" y2="13.843" layer="21"/>
<rectangle x1="30.52318125" y1="13.75841875" x2="31.115" y2="13.843" layer="21"/>
<rectangle x1="32.04718125" y1="13.75841875" x2="34.417" y2="13.843" layer="21"/>
<rectangle x1="35.34918125" y1="13.75841875" x2="39.41318125" y2="13.843" layer="21"/>
<rectangle x1="0.21081875" y1="13.843" x2="1.31318125" y2="13.92758125" layer="21"/>
<rectangle x1="4.86918125" y1="13.843" x2="5.969" y2="13.92758125" layer="21"/>
<rectangle x1="10.20318125" y1="13.843" x2="11.303" y2="13.92758125" layer="21"/>
<rectangle x1="15.367" y1="13.843" x2="16.46681875" y2="13.92758125" layer="21"/>
<rectangle x1="17.82318125" y1="13.843" x2="19.09318125" y2="13.92758125" layer="21"/>
<rectangle x1="20.193" y1="13.843" x2="24.42718125" y2="13.92758125" layer="21"/>
<rectangle x1="25.61081875" y1="13.843" x2="26.20518125" y2="13.92758125" layer="21"/>
<rectangle x1="27.22118125" y1="13.843" x2="29.591" y2="13.92758125" layer="21"/>
<rectangle x1="30.52318125" y1="13.843" x2="31.115" y2="13.92758125" layer="21"/>
<rectangle x1="32.04718125" y1="13.843" x2="34.417" y2="13.92758125" layer="21"/>
<rectangle x1="35.34918125" y1="13.843" x2="39.41318125" y2="13.92758125" layer="21"/>
<rectangle x1="0.21081875" y1="13.92758125" x2="1.31318125" y2="14.01241875" layer="21"/>
<rectangle x1="4.86918125" y1="13.92758125" x2="5.969" y2="14.01241875" layer="21"/>
<rectangle x1="10.20318125" y1="13.92758125" x2="11.303" y2="14.01241875" layer="21"/>
<rectangle x1="15.367" y1="13.92758125" x2="16.46681875" y2="14.01241875" layer="21"/>
<rectangle x1="17.82318125" y1="13.92758125" x2="19.09318125" y2="14.01241875" layer="21"/>
<rectangle x1="20.193" y1="13.92758125" x2="24.511" y2="14.01241875" layer="21"/>
<rectangle x1="25.61081875" y1="13.92758125" x2="26.20518125" y2="14.01241875" layer="21"/>
<rectangle x1="27.22118125" y1="13.92758125" x2="29.591" y2="14.01241875" layer="21"/>
<rectangle x1="30.52318125" y1="13.92758125" x2="31.115" y2="14.01241875" layer="21"/>
<rectangle x1="32.04718125" y1="13.92758125" x2="34.417" y2="14.01241875" layer="21"/>
<rectangle x1="35.34918125" y1="13.92758125" x2="39.41318125" y2="14.01241875" layer="21"/>
<rectangle x1="0.21081875" y1="14.01241875" x2="1.31318125" y2="14.097" layer="21"/>
<rectangle x1="4.86918125" y1="14.01241875" x2="5.88518125" y2="14.097" layer="21"/>
<rectangle x1="10.20318125" y1="14.01241875" x2="11.303" y2="14.097" layer="21"/>
<rectangle x1="15.367" y1="14.01241875" x2="16.46681875" y2="14.097" layer="21"/>
<rectangle x1="17.82318125" y1="14.01241875" x2="19.09318125" y2="14.097" layer="21"/>
<rectangle x1="20.193" y1="14.01241875" x2="24.511" y2="14.097" layer="21"/>
<rectangle x1="25.61081875" y1="14.01241875" x2="26.20518125" y2="14.097" layer="21"/>
<rectangle x1="27.305" y1="14.01241875" x2="29.50718125" y2="14.097" layer="21"/>
<rectangle x1="30.52318125" y1="14.01241875" x2="31.19881875" y2="14.097" layer="21"/>
<rectangle x1="32.131" y1="14.01241875" x2="34.33318125" y2="14.097" layer="21"/>
<rectangle x1="35.26281875" y1="14.01241875" x2="39.41318125" y2="14.097" layer="21"/>
<rectangle x1="0.21081875" y1="14.097" x2="1.31318125" y2="14.18158125" layer="21"/>
<rectangle x1="4.78281875" y1="14.097" x2="5.88518125" y2="14.18158125" layer="21"/>
<rectangle x1="10.287" y1="14.097" x2="11.303" y2="14.18158125" layer="21"/>
<rectangle x1="15.367" y1="14.097" x2="16.46681875" y2="14.18158125" layer="21"/>
<rectangle x1="17.82318125" y1="14.097" x2="19.09318125" y2="14.18158125" layer="21"/>
<rectangle x1="20.193" y1="14.097" x2="24.511" y2="14.18158125" layer="21"/>
<rectangle x1="25.61081875" y1="14.097" x2="26.20518125" y2="14.18158125" layer="21"/>
<rectangle x1="27.38881875" y1="14.097" x2="29.50718125" y2="14.18158125" layer="21"/>
<rectangle x1="30.43681875" y1="14.097" x2="31.19881875" y2="14.18158125" layer="21"/>
<rectangle x1="32.21481875" y1="14.097" x2="34.33318125" y2="14.18158125" layer="21"/>
<rectangle x1="35.26281875" y1="14.097" x2="39.41318125" y2="14.18158125" layer="21"/>
<rectangle x1="0.21081875" y1="14.18158125" x2="1.31318125" y2="14.26641875" layer="21"/>
<rectangle x1="4.78281875" y1="14.18158125" x2="5.88518125" y2="14.26641875" layer="21"/>
<rectangle x1="10.287" y1="14.18158125" x2="11.303" y2="14.26641875" layer="21"/>
<rectangle x1="15.367" y1="14.18158125" x2="16.46681875" y2="14.26641875" layer="21"/>
<rectangle x1="17.82318125" y1="14.18158125" x2="19.09318125" y2="14.26641875" layer="21"/>
<rectangle x1="20.193" y1="14.18158125" x2="24.511" y2="14.26641875" layer="21"/>
<rectangle x1="25.61081875" y1="14.18158125" x2="26.20518125" y2="14.26641875" layer="21"/>
<rectangle x1="27.38881875" y1="14.18158125" x2="29.42081875" y2="14.26641875" layer="21"/>
<rectangle x1="30.43681875" y1="14.18158125" x2="31.19881875" y2="14.26641875" layer="21"/>
<rectangle x1="32.21481875" y1="14.18158125" x2="34.24681875" y2="14.26641875" layer="21"/>
<rectangle x1="35.26281875" y1="14.18158125" x2="39.41318125" y2="14.26641875" layer="21"/>
<rectangle x1="0.21081875" y1="14.26641875" x2="1.31318125" y2="14.351" layer="21"/>
<rectangle x1="4.78281875" y1="14.26641875" x2="5.88518125" y2="14.351" layer="21"/>
<rectangle x1="10.287" y1="14.26641875" x2="11.303" y2="14.351" layer="21"/>
<rectangle x1="15.367" y1="14.26641875" x2="16.46681875" y2="14.351" layer="21"/>
<rectangle x1="17.82318125" y1="14.26641875" x2="19.09318125" y2="14.351" layer="21"/>
<rectangle x1="20.193" y1="14.26641875" x2="24.511" y2="14.351" layer="21"/>
<rectangle x1="25.61081875" y1="14.26641875" x2="26.20518125" y2="14.351" layer="21"/>
<rectangle x1="27.47518125" y1="14.26641875" x2="29.337" y2="14.351" layer="21"/>
<rectangle x1="30.353" y1="14.26641875" x2="31.28518125" y2="14.351" layer="21"/>
<rectangle x1="32.30118125" y1="14.26641875" x2="34.163" y2="14.351" layer="21"/>
<rectangle x1="35.179" y1="14.26641875" x2="39.41318125" y2="14.351" layer="21"/>
<rectangle x1="0.21081875" y1="14.351" x2="1.31318125" y2="14.43558125" layer="21"/>
<rectangle x1="4.78281875" y1="14.351" x2="5.88518125" y2="14.43558125" layer="21"/>
<rectangle x1="10.287" y1="14.351" x2="11.303" y2="14.43558125" layer="21"/>
<rectangle x1="15.367" y1="14.351" x2="16.46681875" y2="14.43558125" layer="21"/>
<rectangle x1="17.82318125" y1="14.351" x2="19.09318125" y2="14.43558125" layer="21"/>
<rectangle x1="20.193" y1="14.351" x2="24.511" y2="14.43558125" layer="21"/>
<rectangle x1="25.61081875" y1="14.351" x2="26.20518125" y2="14.43558125" layer="21"/>
<rectangle x1="27.559" y1="14.351" x2="29.25318125" y2="14.43558125" layer="21"/>
<rectangle x1="30.353" y1="14.351" x2="31.28518125" y2="14.43558125" layer="21"/>
<rectangle x1="32.385" y1="14.351" x2="34.07918125" y2="14.43558125" layer="21"/>
<rectangle x1="35.179" y1="14.351" x2="39.41318125" y2="14.43558125" layer="21"/>
<rectangle x1="0.21081875" y1="14.43558125" x2="1.31318125" y2="14.52041875" layer="21"/>
<rectangle x1="4.78281875" y1="14.43558125" x2="5.88518125" y2="14.52041875" layer="21"/>
<rectangle x1="10.287" y1="14.43558125" x2="11.303" y2="14.52041875" layer="21"/>
<rectangle x1="15.367" y1="14.43558125" x2="16.46681875" y2="14.52041875" layer="21"/>
<rectangle x1="17.82318125" y1="14.43558125" x2="19.09318125" y2="14.52041875" layer="21"/>
<rectangle x1="20.193" y1="14.43558125" x2="24.511" y2="14.52041875" layer="21"/>
<rectangle x1="25.61081875" y1="14.43558125" x2="26.20518125" y2="14.52041875" layer="21"/>
<rectangle x1="27.72918125" y1="14.43558125" x2="29.083" y2="14.52041875" layer="21"/>
<rectangle x1="30.26918125" y1="14.43558125" x2="31.369" y2="14.52041875" layer="21"/>
<rectangle x1="32.55518125" y1="14.43558125" x2="33.909" y2="14.52041875" layer="21"/>
<rectangle x1="35.09518125" y1="14.43558125" x2="39.41318125" y2="14.52041875" layer="21"/>
<rectangle x1="0.21081875" y1="14.52041875" x2="1.31318125" y2="14.605" layer="21"/>
<rectangle x1="4.78281875" y1="14.52041875" x2="5.88518125" y2="14.605" layer="21"/>
<rectangle x1="10.287" y1="14.52041875" x2="11.303" y2="14.605" layer="21"/>
<rectangle x1="15.367" y1="14.52041875" x2="16.46681875" y2="14.605" layer="21"/>
<rectangle x1="17.82318125" y1="14.52041875" x2="19.09318125" y2="14.605" layer="21"/>
<rectangle x1="20.193" y1="14.52041875" x2="24.511" y2="14.605" layer="21"/>
<rectangle x1="25.61081875" y1="14.52041875" x2="26.20518125" y2="14.605" layer="21"/>
<rectangle x1="27.89681875" y1="14.52041875" x2="28.91281875" y2="14.605" layer="21"/>
<rectangle x1="30.26918125" y1="14.52041875" x2="31.45281875" y2="14.605" layer="21"/>
<rectangle x1="32.72281875" y1="14.52041875" x2="33.82518125" y2="14.605" layer="21"/>
<rectangle x1="35.00881875" y1="14.52041875" x2="39.41318125" y2="14.605" layer="21"/>
<rectangle x1="0.21081875" y1="14.605" x2="1.31318125" y2="14.68958125" layer="21"/>
<rectangle x1="4.78281875" y1="14.605" x2="5.88518125" y2="14.68958125" layer="21"/>
<rectangle x1="10.287" y1="14.605" x2="11.303" y2="14.68958125" layer="21"/>
<rectangle x1="15.367" y1="14.605" x2="16.46681875" y2="14.68958125" layer="21"/>
<rectangle x1="17.82318125" y1="14.605" x2="19.09318125" y2="14.68958125" layer="21"/>
<rectangle x1="20.193" y1="14.605" x2="24.511" y2="14.68958125" layer="21"/>
<rectangle x1="25.61081875" y1="14.605" x2="26.20518125" y2="14.68958125" layer="21"/>
<rectangle x1="28.15081875" y1="14.605" x2="28.65881875" y2="14.68958125" layer="21"/>
<rectangle x1="30.18281875" y1="14.605" x2="31.45281875" y2="14.68958125" layer="21"/>
<rectangle x1="32.97681875" y1="14.605" x2="33.48481875" y2="14.68958125" layer="21"/>
<rectangle x1="35.00881875" y1="14.605" x2="39.41318125" y2="14.68958125" layer="21"/>
<rectangle x1="0.21081875" y1="14.68958125" x2="1.31318125" y2="14.77441875" layer="21"/>
<rectangle x1="4.78281875" y1="14.68958125" x2="5.88518125" y2="14.77441875" layer="21"/>
<rectangle x1="10.287" y1="14.68958125" x2="11.303" y2="14.77441875" layer="21"/>
<rectangle x1="15.367" y1="14.68958125" x2="16.46681875" y2="14.77441875" layer="21"/>
<rectangle x1="17.82318125" y1="14.68958125" x2="19.09318125" y2="14.77441875" layer="21"/>
<rectangle x1="20.193" y1="14.68958125" x2="24.511" y2="14.77441875" layer="21"/>
<rectangle x1="25.61081875" y1="14.68958125" x2="26.20518125" y2="14.77441875" layer="21"/>
<rectangle x1="30.099" y1="14.68958125" x2="31.53918125" y2="14.77441875" layer="21"/>
<rectangle x1="34.925" y1="14.68958125" x2="39.41318125" y2="14.77441875" layer="21"/>
<rectangle x1="0.21081875" y1="14.77441875" x2="1.31318125" y2="14.859" layer="21"/>
<rectangle x1="4.78281875" y1="14.77441875" x2="5.88518125" y2="14.859" layer="21"/>
<rectangle x1="10.287" y1="14.77441875" x2="11.303" y2="14.859" layer="21"/>
<rectangle x1="15.367" y1="14.77441875" x2="16.46681875" y2="14.859" layer="21"/>
<rectangle x1="17.82318125" y1="14.77441875" x2="19.09318125" y2="14.859" layer="21"/>
<rectangle x1="20.193" y1="14.77441875" x2="24.42718125" y2="14.859" layer="21"/>
<rectangle x1="25.61081875" y1="14.77441875" x2="26.20518125" y2="14.859" layer="21"/>
<rectangle x1="30.01518125" y1="14.77441875" x2="31.623" y2="14.859" layer="21"/>
<rectangle x1="34.84118125" y1="14.77441875" x2="39.41318125" y2="14.859" layer="21"/>
<rectangle x1="0.21081875" y1="14.859" x2="1.31318125" y2="14.94358125" layer="21"/>
<rectangle x1="4.78281875" y1="14.859" x2="5.88518125" y2="14.94358125" layer="21"/>
<rectangle x1="10.287" y1="14.859" x2="11.303" y2="14.94358125" layer="21"/>
<rectangle x1="15.28318125" y1="14.859" x2="16.383" y2="14.94358125" layer="21"/>
<rectangle x1="17.82318125" y1="14.859" x2="19.09318125" y2="14.94358125" layer="21"/>
<rectangle x1="20.27681875" y1="14.859" x2="24.42718125" y2="14.94358125" layer="21"/>
<rectangle x1="25.527" y1="14.859" x2="26.20518125" y2="14.94358125" layer="21"/>
<rectangle x1="29.92881875" y1="14.859" x2="31.70681875" y2="14.94358125" layer="21"/>
<rectangle x1="34.75481875" y1="14.859" x2="39.41318125" y2="14.94358125" layer="21"/>
<rectangle x1="0.21081875" y1="14.94358125" x2="1.31318125" y2="15.02841875" layer="21"/>
<rectangle x1="4.78281875" y1="14.94358125" x2="5.88518125" y2="15.02841875" layer="21"/>
<rectangle x1="10.287" y1="14.94358125" x2="11.303" y2="15.02841875" layer="21"/>
<rectangle x1="15.28318125" y1="14.94358125" x2="16.383" y2="15.02841875" layer="21"/>
<rectangle x1="17.82318125" y1="14.94358125" x2="19.177" y2="15.02841875" layer="21"/>
<rectangle x1="20.27681875" y1="14.94358125" x2="24.42718125" y2="15.02841875" layer="21"/>
<rectangle x1="25.527" y1="14.94358125" x2="26.20518125" y2="15.02841875" layer="21"/>
<rectangle x1="29.845" y1="14.94358125" x2="31.79318125" y2="15.02841875" layer="21"/>
<rectangle x1="34.671" y1="14.94358125" x2="39.41318125" y2="15.02841875" layer="21"/>
<rectangle x1="0.21081875" y1="15.02841875" x2="1.31318125" y2="15.113" layer="21"/>
<rectangle x1="4.78281875" y1="15.02841875" x2="5.88518125" y2="15.113" layer="21"/>
<rectangle x1="10.287" y1="15.02841875" x2="11.303" y2="15.113" layer="21"/>
<rectangle x1="15.28318125" y1="15.02841875" x2="16.383" y2="15.113" layer="21"/>
<rectangle x1="17.82318125" y1="15.02841875" x2="19.177" y2="15.113" layer="21"/>
<rectangle x1="20.27681875" y1="15.02841875" x2="24.42718125" y2="15.113" layer="21"/>
<rectangle x1="25.527" y1="15.02841875" x2="26.20518125" y2="15.113" layer="21"/>
<rectangle x1="29.76118125" y1="15.02841875" x2="31.877" y2="15.113" layer="21"/>
<rectangle x1="34.58718125" y1="15.02841875" x2="39.41318125" y2="15.113" layer="21"/>
<rectangle x1="0.29718125" y1="15.113" x2="1.31318125" y2="15.19758125" layer="21"/>
<rectangle x1="4.78281875" y1="15.113" x2="5.88518125" y2="15.19758125" layer="21"/>
<rectangle x1="10.287" y1="15.113" x2="11.303" y2="15.19758125" layer="21"/>
<rectangle x1="15.19681875" y1="15.113" x2="16.383" y2="15.19758125" layer="21"/>
<rectangle x1="17.907" y1="15.113" x2="19.177" y2="15.19758125" layer="21"/>
<rectangle x1="20.36318125" y1="15.113" x2="24.34081875" y2="15.19758125" layer="21"/>
<rectangle x1="25.527" y1="15.113" x2="26.20518125" y2="15.19758125" layer="21"/>
<rectangle x1="27.051" y1="15.113" x2="27.22118125" y2="15.19758125" layer="21"/>
<rectangle x1="29.67481875" y1="15.113" x2="32.04718125" y2="15.19758125" layer="21"/>
<rectangle x1="34.50081875" y1="15.113" x2="39.41318125" y2="15.19758125" layer="21"/>
<rectangle x1="0.29718125" y1="15.19758125" x2="1.31318125" y2="15.28241875" layer="21"/>
<rectangle x1="4.78281875" y1="15.19758125" x2="5.88518125" y2="15.28241875" layer="21"/>
<rectangle x1="10.287" y1="15.19758125" x2="11.303" y2="15.28241875" layer="21"/>
<rectangle x1="15.19681875" y1="15.19758125" x2="16.29918125" y2="15.28241875" layer="21"/>
<rectangle x1="17.907" y1="15.19758125" x2="19.177" y2="15.28241875" layer="21"/>
<rectangle x1="20.36318125" y1="15.19758125" x2="24.34081875" y2="15.28241875" layer="21"/>
<rectangle x1="25.44318125" y1="15.19758125" x2="26.289" y2="15.28241875" layer="21"/>
<rectangle x1="27.051" y1="15.19758125" x2="27.305" y2="15.28241875" layer="21"/>
<rectangle x1="29.50718125" y1="15.19758125" x2="32.131" y2="15.28241875" layer="21"/>
<rectangle x1="34.33318125" y1="15.19758125" x2="39.41318125" y2="15.28241875" layer="21"/>
<rectangle x1="0.29718125" y1="15.28241875" x2="1.397" y2="15.367" layer="21"/>
<rectangle x1="4.78281875" y1="15.28241875" x2="5.88518125" y2="15.367" layer="21"/>
<rectangle x1="10.287" y1="15.28241875" x2="11.303" y2="15.367" layer="21"/>
<rectangle x1="15.113" y1="15.28241875" x2="16.29918125" y2="15.367" layer="21"/>
<rectangle x1="17.907" y1="15.28241875" x2="19.26081875" y2="15.367" layer="21"/>
<rectangle x1="20.36318125" y1="15.28241875" x2="24.257" y2="15.367" layer="21"/>
<rectangle x1="25.44318125" y1="15.28241875" x2="26.289" y2="15.367" layer="21"/>
<rectangle x1="27.051" y1="15.28241875" x2="27.47518125" y2="15.367" layer="21"/>
<rectangle x1="29.337" y1="15.28241875" x2="32.30118125" y2="15.367" layer="21"/>
<rectangle x1="34.163" y1="15.28241875" x2="39.41318125" y2="15.367" layer="21"/>
<rectangle x1="0.29718125" y1="15.367" x2="1.397" y2="15.45158125" layer="21"/>
<rectangle x1="4.78281875" y1="15.367" x2="5.88518125" y2="15.45158125" layer="21"/>
<rectangle x1="10.287" y1="15.367" x2="11.303" y2="15.45158125" layer="21"/>
<rectangle x1="15.113" y1="15.367" x2="16.29918125" y2="15.45158125" layer="21"/>
<rectangle x1="17.907" y1="15.367" x2="19.26081875" y2="15.45158125" layer="21"/>
<rectangle x1="20.447" y1="15.367" x2="24.257" y2="15.45158125" layer="21"/>
<rectangle x1="25.44318125" y1="15.367" x2="26.37281875" y2="15.45158125" layer="21"/>
<rectangle x1="26.96718125" y1="15.367" x2="27.64281875" y2="15.45158125" layer="21"/>
<rectangle x1="29.16681875" y1="15.367" x2="32.46881875" y2="15.45158125" layer="21"/>
<rectangle x1="33.99281875" y1="15.367" x2="39.41318125" y2="15.45158125" layer="21"/>
<rectangle x1="0.29718125" y1="15.45158125" x2="1.397" y2="15.53641875" layer="21"/>
<rectangle x1="4.78281875" y1="15.45158125" x2="5.88518125" y2="15.53641875" layer="21"/>
<rectangle x1="10.287" y1="15.45158125" x2="11.303" y2="15.53641875" layer="21"/>
<rectangle x1="15.02918125" y1="15.45158125" x2="16.21281875" y2="15.53641875" layer="21"/>
<rectangle x1="17.907" y1="15.45158125" x2="19.26081875" y2="15.53641875" layer="21"/>
<rectangle x1="20.53081875" y1="15.45158125" x2="24.17318125" y2="15.53641875" layer="21"/>
<rectangle x1="25.35681875" y1="15.45158125" x2="26.45918125" y2="15.53641875" layer="21"/>
<rectangle x1="26.88081875" y1="15.45158125" x2="27.98318125" y2="15.53641875" layer="21"/>
<rectangle x1="28.91281875" y1="15.45158125" x2="32.80918125" y2="15.53641875" layer="21"/>
<rectangle x1="33.655" y1="15.45158125" x2="39.41318125" y2="15.53641875" layer="21"/>
<rectangle x1="0.29718125" y1="15.53641875" x2="1.397" y2="15.621" layer="21"/>
<rectangle x1="4.78281875" y1="15.53641875" x2="5.88518125" y2="15.621" layer="21"/>
<rectangle x1="10.287" y1="15.53641875" x2="11.303" y2="15.621" layer="21"/>
<rectangle x1="15.02918125" y1="15.53641875" x2="16.21281875" y2="15.621" layer="21"/>
<rectangle x1="17.907" y1="15.53641875" x2="19.34718125" y2="15.621" layer="21"/>
<rectangle x1="20.53081875" y1="15.53641875" x2="24.17318125" y2="15.621" layer="21"/>
<rectangle x1="25.35681875" y1="15.53641875" x2="39.41318125" y2="15.621" layer="21"/>
<rectangle x1="0.29718125" y1="15.621" x2="1.397" y2="15.70558125" layer="21"/>
<rectangle x1="4.78281875" y1="15.621" x2="5.88518125" y2="15.70558125" layer="21"/>
<rectangle x1="10.287" y1="15.621" x2="11.303" y2="15.70558125" layer="21"/>
<rectangle x1="14.94281875" y1="15.621" x2="16.21281875" y2="15.70558125" layer="21"/>
<rectangle x1="17.907" y1="15.621" x2="19.34718125" y2="15.70558125" layer="21"/>
<rectangle x1="20.61718125" y1="15.621" x2="24.08681875" y2="15.70558125" layer="21"/>
<rectangle x1="25.35681875" y1="15.621" x2="39.41318125" y2="15.70558125" layer="21"/>
<rectangle x1="0.29718125" y1="15.70558125" x2="1.397" y2="15.79041875" layer="21"/>
<rectangle x1="4.78281875" y1="15.70558125" x2="5.88518125" y2="15.79041875" layer="21"/>
<rectangle x1="10.287" y1="15.70558125" x2="11.303" y2="15.79041875" layer="21"/>
<rectangle x1="14.859" y1="15.70558125" x2="16.129" y2="15.79041875" layer="21"/>
<rectangle x1="17.907" y1="15.70558125" x2="19.431" y2="15.79041875" layer="21"/>
<rectangle x1="20.701" y1="15.70558125" x2="24.003" y2="15.79041875" layer="21"/>
<rectangle x1="25.273" y1="15.70558125" x2="39.41318125" y2="15.79041875" layer="21"/>
<rectangle x1="0.381" y1="15.79041875" x2="1.48081875" y2="15.875" layer="21"/>
<rectangle x1="4.78281875" y1="15.79041875" x2="5.88518125" y2="15.875" layer="21"/>
<rectangle x1="10.287" y1="15.79041875" x2="11.303" y2="15.875" layer="21"/>
<rectangle x1="14.77518125" y1="15.79041875" x2="16.129" y2="15.875" layer="21"/>
<rectangle x1="17.907" y1="15.79041875" x2="19.431" y2="15.875" layer="21"/>
<rectangle x1="20.78481875" y1="15.79041875" x2="23.91918125" y2="15.875" layer="21"/>
<rectangle x1="25.273" y1="15.79041875" x2="39.41318125" y2="15.875" layer="21"/>
<rectangle x1="0.381" y1="15.875" x2="1.48081875" y2="15.95958125" layer="21"/>
<rectangle x1="4.78281875" y1="15.875" x2="5.88518125" y2="15.95958125" layer="21"/>
<rectangle x1="10.287" y1="15.875" x2="11.303" y2="15.95958125" layer="21"/>
<rectangle x1="14.68881875" y1="15.875" x2="16.04518125" y2="15.95958125" layer="21"/>
<rectangle x1="17.907" y1="15.875" x2="19.51481875" y2="15.95958125" layer="21"/>
<rectangle x1="20.87118125" y1="15.875" x2="23.83281875" y2="15.95958125" layer="21"/>
<rectangle x1="25.18918125" y1="15.875" x2="39.41318125" y2="15.95958125" layer="21"/>
<rectangle x1="0.381" y1="15.95958125" x2="1.48081875" y2="16.04441875" layer="21"/>
<rectangle x1="4.78281875" y1="15.95958125" x2="5.88518125" y2="16.04441875" layer="21"/>
<rectangle x1="10.287" y1="15.95958125" x2="11.303" y2="16.04441875" layer="21"/>
<rectangle x1="14.605" y1="15.95958125" x2="16.04518125" y2="16.04441875" layer="21"/>
<rectangle x1="17.907" y1="15.95958125" x2="19.51481875" y2="16.04441875" layer="21"/>
<rectangle x1="20.955" y1="15.95958125" x2="23.749" y2="16.04441875" layer="21"/>
<rectangle x1="25.10281875" y1="15.95958125" x2="39.41318125" y2="16.04441875" layer="21"/>
<rectangle x1="0.381" y1="16.04441875" x2="1.48081875" y2="16.129" layer="21"/>
<rectangle x1="4.78281875" y1="16.04441875" x2="5.88518125" y2="16.129" layer="21"/>
<rectangle x1="10.287" y1="16.04441875" x2="11.303" y2="16.129" layer="21"/>
<rectangle x1="14.52118125" y1="16.04441875" x2="15.95881875" y2="16.129" layer="21"/>
<rectangle x1="17.907" y1="16.04441875" x2="19.60118125" y2="16.129" layer="21"/>
<rectangle x1="21.03881875" y1="16.04441875" x2="23.66518125" y2="16.129" layer="21"/>
<rectangle x1="25.10281875" y1="16.04441875" x2="39.32681875" y2="16.129" layer="21"/>
<rectangle x1="0.381" y1="16.129" x2="1.56718125" y2="16.21358125" layer="21"/>
<rectangle x1="4.78281875" y1="16.129" x2="5.88518125" y2="16.21358125" layer="21"/>
<rectangle x1="10.287" y1="16.129" x2="11.303" y2="16.21358125" layer="21"/>
<rectangle x1="14.351" y1="16.129" x2="15.875" y2="16.21358125" layer="21"/>
<rectangle x1="17.907" y1="16.129" x2="19.685" y2="16.21358125" layer="21"/>
<rectangle x1="21.209" y1="16.129" x2="23.495" y2="16.21358125" layer="21"/>
<rectangle x1="25.019" y1="16.129" x2="39.32681875" y2="16.21358125" layer="21"/>
<rectangle x1="0.46481875" y1="16.21358125" x2="1.56718125" y2="16.29841875" layer="21"/>
<rectangle x1="4.78281875" y1="16.21358125" x2="5.88518125" y2="16.29841875" layer="21"/>
<rectangle x1="10.287" y1="16.21358125" x2="11.303" y2="16.29841875" layer="21"/>
<rectangle x1="14.18081875" y1="16.21358125" x2="15.875" y2="16.29841875" layer="21"/>
<rectangle x1="17.99081875" y1="16.21358125" x2="19.685" y2="16.29841875" layer="21"/>
<rectangle x1="21.29281875" y1="16.21358125" x2="23.41118125" y2="16.29841875" layer="21"/>
<rectangle x1="24.93518125" y1="16.21358125" x2="39.32681875" y2="16.29841875" layer="21"/>
<rectangle x1="0.46481875" y1="16.29841875" x2="1.56718125" y2="16.383" layer="21"/>
<rectangle x1="4.78281875" y1="16.29841875" x2="5.88518125" y2="16.383" layer="21"/>
<rectangle x1="10.287" y1="16.29841875" x2="11.303" y2="16.383" layer="21"/>
<rectangle x1="14.097" y1="16.29841875" x2="15.79118125" y2="16.383" layer="21"/>
<rectangle x1="17.99081875" y1="16.29841875" x2="19.76881875" y2="16.383" layer="21"/>
<rectangle x1="21.463" y1="16.29841875" x2="23.241" y2="16.383" layer="21"/>
<rectangle x1="24.93518125" y1="16.29841875" x2="39.32681875" y2="16.383" layer="21"/>
<rectangle x1="0.46481875" y1="16.383" x2="1.651" y2="16.46758125" layer="21"/>
<rectangle x1="4.78281875" y1="16.383" x2="5.88518125" y2="16.46758125" layer="21"/>
<rectangle x1="10.287" y1="16.383" x2="11.303" y2="16.46758125" layer="21"/>
<rectangle x1="13.843" y1="16.383" x2="15.70481875" y2="16.46758125" layer="21"/>
<rectangle x1="17.99081875" y1="16.383" x2="19.85518125" y2="16.46758125" layer="21"/>
<rectangle x1="21.80081875" y1="16.383" x2="22.987" y2="16.46758125" layer="21"/>
<rectangle x1="24.84881875" y1="16.383" x2="39.32681875" y2="16.46758125" layer="21"/>
<rectangle x1="0.46481875" y1="16.46758125" x2="1.651" y2="16.55241875" layer="21"/>
<rectangle x1="4.78281875" y1="16.46758125" x2="5.88518125" y2="16.55241875" layer="21"/>
<rectangle x1="10.287" y1="16.46758125" x2="11.47318125" y2="16.55241875" layer="21"/>
<rectangle x1="13.25118125" y1="16.46758125" x2="15.621" y2="16.55241875" layer="21"/>
<rectangle x1="17.99081875" y1="16.46758125" x2="19.939" y2="16.55241875" layer="21"/>
<rectangle x1="24.765" y1="16.46758125" x2="39.243" y2="16.55241875" layer="21"/>
<rectangle x1="0.55118125" y1="16.55241875" x2="1.651" y2="16.637" layer="21"/>
<rectangle x1="4.78281875" y1="16.55241875" x2="5.88518125" y2="16.637" layer="21"/>
<rectangle x1="10.287" y1="16.55241875" x2="15.53718125" y2="16.637" layer="21"/>
<rectangle x1="17.99081875" y1="16.55241875" x2="20.02281875" y2="16.637" layer="21"/>
<rectangle x1="24.68118125" y1="16.55241875" x2="39.243" y2="16.637" layer="21"/>
<rectangle x1="0.55118125" y1="16.637" x2="1.73481875" y2="16.72158125" layer="21"/>
<rectangle x1="4.78281875" y1="16.637" x2="5.88518125" y2="16.72158125" layer="21"/>
<rectangle x1="10.287" y1="16.637" x2="15.45081875" y2="16.72158125" layer="21"/>
<rectangle x1="17.99081875" y1="16.637" x2="20.10918125" y2="16.72158125" layer="21"/>
<rectangle x1="24.59481875" y1="16.637" x2="39.243" y2="16.72158125" layer="21"/>
<rectangle x1="0.55118125" y1="16.72158125" x2="1.73481875" y2="16.80641875" layer="21"/>
<rectangle x1="4.78281875" y1="16.72158125" x2="5.88518125" y2="16.80641875" layer="21"/>
<rectangle x1="10.287" y1="16.72158125" x2="15.367" y2="16.80641875" layer="21"/>
<rectangle x1="17.99081875" y1="16.72158125" x2="20.193" y2="16.80641875" layer="21"/>
<rectangle x1="24.511" y1="16.72158125" x2="39.15918125" y2="16.80641875" layer="21"/>
<rectangle x1="0.635" y1="16.80641875" x2="1.73481875" y2="16.891" layer="21"/>
<rectangle x1="4.78281875" y1="16.80641875" x2="5.88518125" y2="16.891" layer="21"/>
<rectangle x1="10.287" y1="16.80641875" x2="15.28318125" y2="16.891" layer="21"/>
<rectangle x1="18.07718125" y1="16.80641875" x2="20.27681875" y2="16.891" layer="21"/>
<rectangle x1="24.42718125" y1="16.80641875" x2="39.15918125" y2="16.891" layer="21"/>
<rectangle x1="0.635" y1="16.891" x2="1.82118125" y2="16.97558125" layer="21"/>
<rectangle x1="4.78281875" y1="16.891" x2="5.88518125" y2="16.97558125" layer="21"/>
<rectangle x1="10.287" y1="16.891" x2="15.19681875" y2="16.97558125" layer="21"/>
<rectangle x1="18.07718125" y1="16.891" x2="20.36318125" y2="16.97558125" layer="21"/>
<rectangle x1="24.34081875" y1="16.891" x2="39.15918125" y2="16.97558125" layer="21"/>
<rectangle x1="0.635" y1="16.97558125" x2="1.82118125" y2="17.06041875" layer="21"/>
<rectangle x1="4.78281875" y1="16.97558125" x2="5.88518125" y2="17.06041875" layer="21"/>
<rectangle x1="10.20318125" y1="16.97558125" x2="15.02918125" y2="17.06041875" layer="21"/>
<rectangle x1="18.07718125" y1="16.97558125" x2="20.53081875" y2="17.06041875" layer="21"/>
<rectangle x1="24.17318125" y1="16.97558125" x2="39.07281875" y2="17.06041875" layer="21"/>
<rectangle x1="0.71881875" y1="17.06041875" x2="1.905" y2="17.145" layer="21"/>
<rectangle x1="4.78281875" y1="17.06041875" x2="5.88518125" y2="17.145" layer="21"/>
<rectangle x1="10.287" y1="17.06041875" x2="14.94281875" y2="17.145" layer="21"/>
<rectangle x1="18.07718125" y1="17.06041875" x2="20.61718125" y2="17.145" layer="21"/>
<rectangle x1="24.08681875" y1="17.06041875" x2="39.07281875" y2="17.145" layer="21"/>
<rectangle x1="0.71881875" y1="17.145" x2="1.905" y2="17.22958125" layer="21"/>
<rectangle x1="4.86918125" y1="17.145" x2="5.88518125" y2="17.22958125" layer="21"/>
<rectangle x1="10.287" y1="17.145" x2="14.77518125" y2="17.22958125" layer="21"/>
<rectangle x1="18.07718125" y1="17.145" x2="20.78481875" y2="17.22958125" layer="21"/>
<rectangle x1="23.91918125" y1="17.145" x2="39.07281875" y2="17.22958125" layer="21"/>
<rectangle x1="0.80518125" y1="17.22958125" x2="1.98881875" y2="17.31441875" layer="21"/>
<rectangle x1="4.86918125" y1="17.22958125" x2="5.88518125" y2="17.31441875" layer="21"/>
<rectangle x1="10.287" y1="17.22958125" x2="14.605" y2="17.31441875" layer="21"/>
<rectangle x1="18.07718125" y1="17.22958125" x2="20.955" y2="17.31441875" layer="21"/>
<rectangle x1="23.749" y1="17.22958125" x2="38.989" y2="17.31441875" layer="21"/>
<rectangle x1="0.80518125" y1="17.31441875" x2="1.98881875" y2="17.399" layer="21"/>
<rectangle x1="4.86918125" y1="17.31441875" x2="5.79881875" y2="17.399" layer="21"/>
<rectangle x1="10.37081875" y1="17.31441875" x2="14.43481875" y2="17.399" layer="21"/>
<rectangle x1="18.161" y1="17.31441875" x2="21.12518125" y2="17.399" layer="21"/>
<rectangle x1="23.57881875" y1="17.31441875" x2="38.989" y2="17.399" layer="21"/>
<rectangle x1="0.80518125" y1="17.399" x2="2.07518125" y2="17.48358125" layer="21"/>
<rectangle x1="4.953" y1="17.399" x2="5.715" y2="17.48358125" layer="21"/>
<rectangle x1="10.37081875" y1="17.399" x2="14.18081875" y2="17.48358125" layer="21"/>
<rectangle x1="18.161" y1="17.399" x2="21.37918125" y2="17.48358125" layer="21"/>
<rectangle x1="23.32481875" y1="17.399" x2="38.90518125" y2="17.48358125" layer="21"/>
<rectangle x1="0.889" y1="17.48358125" x2="2.07518125" y2="17.56841875" layer="21"/>
<rectangle x1="5.12318125" y1="17.48358125" x2="5.63118125" y2="17.56841875" layer="21"/>
<rectangle x1="10.541" y1="17.48358125" x2="13.843" y2="17.56841875" layer="21"/>
<rectangle x1="18.161" y1="17.48358125" x2="21.717" y2="17.56841875" layer="21"/>
<rectangle x1="22.987" y1="17.48358125" x2="38.90518125" y2="17.56841875" layer="21"/>
<rectangle x1="0.97281875" y1="17.56841875" x2="2.159" y2="17.653" layer="21"/>
<rectangle x1="18.161" y1="17.56841875" x2="38.81881875" y2="17.653" layer="21"/>
<rectangle x1="0.97281875" y1="17.653" x2="2.159" y2="17.73758125" layer="21"/>
<rectangle x1="18.161" y1="17.653" x2="38.81881875" y2="17.73758125" layer="21"/>
<rectangle x1="0.97281875" y1="17.73758125" x2="2.24281875" y2="17.82241875" layer="21"/>
<rectangle x1="18.24481875" y1="17.73758125" x2="38.81881875" y2="17.82241875" layer="21"/>
<rectangle x1="1.05918125" y1="17.82241875" x2="2.24281875" y2="17.907" layer="21"/>
<rectangle x1="18.24481875" y1="17.82241875" x2="38.735" y2="17.907" layer="21"/>
<rectangle x1="1.05918125" y1="17.907" x2="2.32918125" y2="17.99158125" layer="21"/>
<rectangle x1="18.24481875" y1="17.907" x2="38.735" y2="17.99158125" layer="21"/>
<rectangle x1="1.143" y1="17.99158125" x2="2.413" y2="18.07641875" layer="21"/>
<rectangle x1="18.24481875" y1="17.99158125" x2="38.65118125" y2="18.07641875" layer="21"/>
<rectangle x1="1.143" y1="18.07641875" x2="2.413" y2="18.161" layer="21"/>
<rectangle x1="18.24481875" y1="18.07641875" x2="38.56481875" y2="18.161" layer="21"/>
<rectangle x1="1.22681875" y1="18.161" x2="2.49681875" y2="18.24558125" layer="21"/>
<rectangle x1="18.33118125" y1="18.161" x2="38.56481875" y2="18.24558125" layer="21"/>
<rectangle x1="1.22681875" y1="18.24558125" x2="2.58318125" y2="18.33041875" layer="21"/>
<rectangle x1="18.33118125" y1="18.24558125" x2="38.481" y2="18.33041875" layer="21"/>
<rectangle x1="1.31318125" y1="18.33041875" x2="2.667" y2="18.415" layer="21"/>
<rectangle x1="18.33118125" y1="18.33041875" x2="38.481" y2="18.415" layer="21"/>
<rectangle x1="1.397" y1="18.415" x2="2.667" y2="18.49958125" layer="21"/>
<rectangle x1="18.33118125" y1="18.415" x2="38.39718125" y2="18.49958125" layer="21"/>
<rectangle x1="1.397" y1="18.49958125" x2="2.75081875" y2="18.58441875" layer="21"/>
<rectangle x1="18.415" y1="18.49958125" x2="38.31081875" y2="18.58441875" layer="21"/>
<rectangle x1="1.48081875" y1="18.58441875" x2="2.83718125" y2="18.669" layer="21"/>
<rectangle x1="18.415" y1="18.58441875" x2="38.31081875" y2="18.669" layer="21"/>
<rectangle x1="1.56718125" y1="18.669" x2="2.921" y2="18.75358125" layer="21"/>
<rectangle x1="18.415" y1="18.669" x2="38.227" y2="18.75358125" layer="21"/>
<rectangle x1="1.56718125" y1="18.75358125" x2="3.00481875" y2="18.83841875" layer="21"/>
<rectangle x1="18.49881875" y1="18.75358125" x2="38.227" y2="18.83841875" layer="21"/>
<rectangle x1="1.651" y1="18.83841875" x2="3.00481875" y2="18.923" layer="21"/>
<rectangle x1="18.49881875" y1="18.83841875" x2="38.14318125" y2="18.923" layer="21"/>
<rectangle x1="1.73481875" y1="18.923" x2="3.09118125" y2="19.00758125" layer="21"/>
<rectangle x1="18.49881875" y1="18.923" x2="38.05681875" y2="19.00758125" layer="21"/>
<rectangle x1="1.73481875" y1="19.00758125" x2="3.175" y2="19.09241875" layer="21"/>
<rectangle x1="18.49881875" y1="19.00758125" x2="37.973" y2="19.09241875" layer="21"/>
<rectangle x1="1.82118125" y1="19.09241875" x2="3.25881875" y2="19.177" layer="21"/>
<rectangle x1="18.58518125" y1="19.09241875" x2="37.973" y2="19.177" layer="21"/>
<rectangle x1="1.905" y1="19.177" x2="3.34518125" y2="19.26158125" layer="21"/>
<rectangle x1="18.58518125" y1="19.177" x2="37.88918125" y2="19.26158125" layer="21"/>
<rectangle x1="1.98881875" y1="19.26158125" x2="3.429" y2="19.34641875" layer="21"/>
<rectangle x1="18.669" y1="19.26158125" x2="37.80281875" y2="19.34641875" layer="21"/>
<rectangle x1="2.07518125" y1="19.34641875" x2="3.51281875" y2="19.431" layer="21"/>
<rectangle x1="18.669" y1="19.34641875" x2="37.719" y2="19.431" layer="21"/>
<rectangle x1="2.07518125" y1="19.431" x2="3.683" y2="19.51558125" layer="21"/>
<rectangle x1="18.669" y1="19.431" x2="37.63518125" y2="19.51558125" layer="21"/>
<rectangle x1="2.159" y1="19.51558125" x2="3.76681875" y2="19.60041875" layer="21"/>
<rectangle x1="18.669" y1="19.51558125" x2="37.54881875" y2="19.60041875" layer="21"/>
<rectangle x1="2.24281875" y1="19.60041875" x2="3.85318125" y2="19.685" layer="21"/>
<rectangle x1="18.75281875" y1="19.60041875" x2="37.465" y2="19.685" layer="21"/>
<rectangle x1="2.32918125" y1="19.685" x2="3.937" y2="19.76958125" layer="21"/>
<rectangle x1="18.75281875" y1="19.685" x2="37.465" y2="19.76958125" layer="21"/>
<rectangle x1="2.413" y1="19.76958125" x2="4.10718125" y2="19.85441875" layer="21"/>
<rectangle x1="18.83918125" y1="19.76958125" x2="37.38118125" y2="19.85441875" layer="21"/>
<rectangle x1="2.49681875" y1="19.85441875" x2="4.191" y2="19.939" layer="21"/>
<rectangle x1="18.83918125" y1="19.85441875" x2="37.29481875" y2="19.939" layer="21"/>
<rectangle x1="2.58318125" y1="19.939" x2="4.27481875" y2="20.02358125" layer="21"/>
<rectangle x1="18.923" y1="19.939" x2="37.211" y2="20.02358125" layer="21"/>
<rectangle x1="2.667" y1="20.02358125" x2="4.445" y2="20.10841875" layer="21"/>
<rectangle x1="18.923" y1="20.02358125" x2="37.12718125" y2="20.10841875" layer="21"/>
<rectangle x1="2.75081875" y1="20.10841875" x2="4.52881875" y2="20.193" layer="21"/>
<rectangle x1="19.00681875" y1="20.10841875" x2="36.957" y2="20.193" layer="21"/>
<rectangle x1="2.83718125" y1="20.193" x2="4.699" y2="20.27758125" layer="21"/>
<rectangle x1="19.00681875" y1="20.193" x2="36.87318125" y2="20.27758125" layer="21"/>
<rectangle x1="3.00481875" y1="20.27758125" x2="4.86918125" y2="20.36241875" layer="21"/>
<rectangle x1="19.09318125" y1="20.27758125" x2="36.78681875" y2="20.36241875" layer="21"/>
<rectangle x1="3.09118125" y1="20.36241875" x2="5.03681875" y2="20.447" layer="21"/>
<rectangle x1="19.09318125" y1="20.36241875" x2="36.703" y2="20.447" layer="21"/>
<rectangle x1="3.175" y1="20.447" x2="5.207" y2="20.53158125" layer="21"/>
<rectangle x1="19.177" y1="20.447" x2="36.61918125" y2="20.53158125" layer="21"/>
<rectangle x1="3.25881875" y1="20.53158125" x2="5.37718125" y2="20.61641875" layer="21"/>
<rectangle x1="19.177" y1="20.53158125" x2="36.53281875" y2="20.61641875" layer="21"/>
<rectangle x1="3.429" y1="20.61641875" x2="5.54481875" y2="20.701" layer="21"/>
<rectangle x1="19.26081875" y1="20.61641875" x2="36.36518125" y2="20.701" layer="21"/>
<rectangle x1="3.51281875" y1="20.701" x2="5.79881875" y2="20.78558125" layer="21"/>
<rectangle x1="19.26081875" y1="20.701" x2="36.27881875" y2="20.78558125" layer="21"/>
<rectangle x1="3.683" y1="20.78558125" x2="6.05281875" y2="20.87041875" layer="21"/>
<rectangle x1="19.34718125" y1="20.78558125" x2="36.195" y2="20.87041875" layer="21"/>
<rectangle x1="3.76681875" y1="20.87041875" x2="6.30681875" y2="20.955" layer="21"/>
<rectangle x1="19.34718125" y1="20.87041875" x2="36.02481875" y2="20.955" layer="21"/>
<rectangle x1="3.85318125" y1="20.955" x2="6.731" y2="21.03958125" layer="21"/>
<rectangle x1="19.431" y1="20.955" x2="35.85718125" y2="21.03958125" layer="21"/>
<rectangle x1="4.02081875" y1="21.03958125" x2="7.239" y2="21.12441875" layer="21"/>
<rectangle x1="19.51481875" y1="21.03958125" x2="35.77081875" y2="21.12441875" layer="21"/>
<rectangle x1="4.191" y1="21.12441875" x2="35.60318125" y2="21.209" layer="21"/>
<rectangle x1="4.27481875" y1="21.209" x2="35.433" y2="21.29358125" layer="21"/>
<rectangle x1="4.445" y1="21.29358125" x2="35.26281875" y2="21.37841875" layer="21"/>
<rectangle x1="4.61518125" y1="21.37841875" x2="35.179" y2="21.463" layer="21"/>
<rectangle x1="4.78281875" y1="21.463" x2="34.925" y2="21.54758125" layer="21"/>
<rectangle x1="4.953" y1="21.54758125" x2="34.75481875" y2="21.63241875" layer="21"/>
<rectangle x1="5.207" y1="21.63241875" x2="34.58718125" y2="21.717" layer="21"/>
<rectangle x1="5.461" y1="21.717" x2="34.33318125" y2="21.80158125" layer="21"/>
<rectangle x1="5.715" y1="21.80158125" x2="34.07918125" y2="21.88641875" layer="21"/>
<rectangle x1="5.969" y1="21.88641875" x2="33.82518125" y2="21.971" layer="21"/>
<rectangle x1="6.30681875" y1="21.971" x2="33.48481875" y2="22.05558125" layer="21"/>
<rectangle x1="6.731" y1="22.05558125" x2="32.97681875" y2="22.14041875" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="UDO-LOGO-10MM" urn="urn:adsk.eagle:package:6649258/2" type="box">
<packageinstances>
<packageinstance name="UDO-LOGO-10MM"/>
</packageinstances>
</package3d>
<package3d name="UDO-LOGO-12MM" urn="urn:adsk.eagle:package:6649257/3" type="box">
<packageinstances>
<packageinstance name="UDO-LOGO-12MM"/>
</packageinstances>
</package3d>
<package3d name="UDO-LOGO-15MM" urn="urn:adsk.eagle:package:6649256/2" type="box">
<packageinstances>
<packageinstance name="UDO-LOGO-15MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="UDO-LOGO-20MM">
<rectangle x1="3.00481875" y1="0.29641875" x2="3.429" y2="0.381" layer="94"/>
<rectangle x1="14.351" y1="0.29641875" x2="14.68881875" y2="0.381" layer="94"/>
<rectangle x1="2.75081875" y1="0.381" x2="3.683" y2="0.46558125" layer="94"/>
<rectangle x1="4.445" y1="0.381" x2="4.52881875" y2="0.46558125" layer="94"/>
<rectangle x1="5.54481875" y1="0.381" x2="5.715" y2="0.46558125" layer="94"/>
<rectangle x1="5.969" y1="0.381" x2="6.05281875" y2="0.46558125" layer="94"/>
<rectangle x1="6.985" y1="0.381" x2="7.15518125" y2="0.46558125" layer="94"/>
<rectangle x1="7.57681875" y1="0.381" x2="8.42518125" y2="0.46558125" layer="94"/>
<rectangle x1="9.94918125" y1="0.381" x2="10.20318125" y2="0.46558125" layer="94"/>
<rectangle x1="11.21918125" y1="0.381" x2="11.38681875" y2="0.46558125" layer="94"/>
<rectangle x1="12.40281875" y1="0.381" x2="12.74318125" y2="0.46558125" layer="94"/>
<rectangle x1="14.097" y1="0.381" x2="14.94281875" y2="0.46558125" layer="94"/>
<rectangle x1="15.70481875" y1="0.381" x2="15.79118125" y2="0.46558125" layer="94"/>
<rectangle x1="16.80718125" y1="0.381" x2="16.891" y2="0.46558125" layer="94"/>
<rectangle x1="17.653" y1="0.381" x2="17.907" y2="0.46558125" layer="94"/>
<rectangle x1="2.667" y1="0.46558125" x2="3.85318125" y2="0.55041875" layer="94"/>
<rectangle x1="4.36118125" y1="0.46558125" x2="4.61518125" y2="0.55041875" layer="94"/>
<rectangle x1="5.54481875" y1="0.46558125" x2="5.715" y2="0.55041875" layer="94"/>
<rectangle x1="5.88518125" y1="0.46558125" x2="6.13918125" y2="0.55041875" layer="94"/>
<rectangle x1="6.731" y1="0.46558125" x2="7.239" y2="0.55041875" layer="94"/>
<rectangle x1="7.493" y1="0.46558125" x2="8.67918125" y2="0.55041875" layer="94"/>
<rectangle x1="9.69518125" y1="0.46558125" x2="10.37081875" y2="0.55041875" layer="94"/>
<rectangle x1="11.049" y1="0.46558125" x2="11.64081875" y2="0.55041875" layer="94"/>
<rectangle x1="12.23518125" y1="0.46558125" x2="12.91081875" y2="0.55041875" layer="94"/>
<rectangle x1="13.92681875" y1="0.46558125" x2="15.113" y2="0.55041875" layer="94"/>
<rectangle x1="15.621" y1="0.46558125" x2="15.875" y2="0.55041875" layer="94"/>
<rectangle x1="16.72081875" y1="0.46558125" x2="16.97481875" y2="0.55041875" layer="94"/>
<rectangle x1="17.399" y1="0.46558125" x2="18.07718125" y2="0.55041875" layer="94"/>
<rectangle x1="2.58318125" y1="0.55041875" x2="3.00481875" y2="0.635" layer="94"/>
<rectangle x1="3.51281875" y1="0.55041875" x2="3.937" y2="0.635" layer="94"/>
<rectangle x1="4.36118125" y1="0.55041875" x2="4.61518125" y2="0.635" layer="94"/>
<rectangle x1="5.54481875" y1="0.55041875" x2="5.715" y2="0.635" layer="94"/>
<rectangle x1="5.88518125" y1="0.55041875" x2="6.13918125" y2="0.635" layer="94"/>
<rectangle x1="6.64718125" y1="0.55041875" x2="7.239" y2="0.635" layer="94"/>
<rectangle x1="7.493" y1="0.55041875" x2="8.84681875" y2="0.635" layer="94"/>
<rectangle x1="9.60881875" y1="0.55041875" x2="10.45718125" y2="0.635" layer="94"/>
<rectangle x1="10.96518125" y1="0.55041875" x2="11.72718125" y2="0.635" layer="94"/>
<rectangle x1="12.14881875" y1="0.55041875" x2="12.99718125" y2="0.635" layer="94"/>
<rectangle x1="13.843" y1="0.55041875" x2="14.26718125" y2="0.635" layer="94"/>
<rectangle x1="14.77518125" y1="0.55041875" x2="15.19681875" y2="0.635" layer="94"/>
<rectangle x1="15.621" y1="0.55041875" x2="15.875" y2="0.635" layer="94"/>
<rectangle x1="16.72081875" y1="0.55041875" x2="16.97481875" y2="0.635" layer="94"/>
<rectangle x1="17.31518125" y1="0.55041875" x2="18.24481875" y2="0.635" layer="94"/>
<rectangle x1="2.49681875" y1="0.635" x2="2.83718125" y2="0.71958125" layer="94"/>
<rectangle x1="3.683" y1="0.635" x2="4.02081875" y2="0.71958125" layer="94"/>
<rectangle x1="4.36118125" y1="0.635" x2="4.61518125" y2="0.71958125" layer="94"/>
<rectangle x1="5.54481875" y1="0.635" x2="5.715" y2="0.71958125" layer="94"/>
<rectangle x1="5.88518125" y1="0.635" x2="6.13918125" y2="0.71958125" layer="94"/>
<rectangle x1="6.56081875" y1="0.635" x2="6.90118125" y2="0.71958125" layer="94"/>
<rectangle x1="7.493" y1="0.635" x2="7.747" y2="0.71958125" layer="94"/>
<rectangle x1="8.509" y1="0.635" x2="8.93318125" y2="0.71958125" layer="94"/>
<rectangle x1="9.525" y1="0.635" x2="9.86281875" y2="0.71958125" layer="94"/>
<rectangle x1="10.20318125" y1="0.635" x2="10.541" y2="0.71958125" layer="94"/>
<rectangle x1="10.87881875" y1="0.635" x2="11.13281875" y2="0.71958125" layer="94"/>
<rectangle x1="11.47318125" y1="0.635" x2="11.811" y2="0.71958125" layer="94"/>
<rectangle x1="12.065" y1="0.635" x2="12.40281875" y2="0.71958125" layer="94"/>
<rectangle x1="12.74318125" y1="0.635" x2="13.081" y2="0.71958125" layer="94"/>
<rectangle x1="13.75918125" y1="0.635" x2="14.097" y2="0.71958125" layer="94"/>
<rectangle x1="14.94281875" y1="0.635" x2="15.28318125" y2="0.71958125" layer="94"/>
<rectangle x1="15.621" y1="0.635" x2="15.875" y2="0.71958125" layer="94"/>
<rectangle x1="16.72081875" y1="0.635" x2="16.97481875" y2="0.71958125" layer="94"/>
<rectangle x1="17.22881875" y1="0.635" x2="17.56918125" y2="0.71958125" layer="94"/>
<rectangle x1="17.907" y1="0.635" x2="18.33118125" y2="0.71958125" layer="94"/>
<rectangle x1="2.413" y1="0.71958125" x2="2.667" y2="0.80441875" layer="94"/>
<rectangle x1="3.76681875" y1="0.71958125" x2="4.02081875" y2="0.80441875" layer="94"/>
<rectangle x1="4.36118125" y1="0.71958125" x2="4.61518125" y2="0.80441875" layer="94"/>
<rectangle x1="5.54481875" y1="0.71958125" x2="5.715" y2="0.80441875" layer="94"/>
<rectangle x1="5.88518125" y1="0.71958125" x2="6.13918125" y2="0.80441875" layer="94"/>
<rectangle x1="6.56081875" y1="0.71958125" x2="6.81481875" y2="0.80441875" layer="94"/>
<rectangle x1="7.493" y1="0.71958125" x2="7.747" y2="0.80441875" layer="94"/>
<rectangle x1="8.67918125" y1="0.71958125" x2="9.017" y2="0.80441875" layer="94"/>
<rectangle x1="9.44118125" y1="0.71958125" x2="9.69518125" y2="0.80441875" layer="94"/>
<rectangle x1="10.37081875" y1="0.71958125" x2="10.62481875" y2="0.80441875" layer="94"/>
<rectangle x1="10.87881875" y1="0.71958125" x2="11.049" y2="0.80441875" layer="94"/>
<rectangle x1="11.557" y1="0.71958125" x2="11.811" y2="0.80441875" layer="94"/>
<rectangle x1="11.98118125" y1="0.71958125" x2="12.23518125" y2="0.80441875" layer="94"/>
<rectangle x1="12.91081875" y1="0.71958125" x2="13.16481875" y2="0.80441875" layer="94"/>
<rectangle x1="13.67281875" y1="0.71958125" x2="14.01318125" y2="0.80441875" layer="94"/>
<rectangle x1="15.02918125" y1="0.71958125" x2="15.367" y2="0.80441875" layer="94"/>
<rectangle x1="15.621" y1="0.71958125" x2="15.875" y2="0.80441875" layer="94"/>
<rectangle x1="16.72081875" y1="0.71958125" x2="16.97481875" y2="0.80441875" layer="94"/>
<rectangle x1="17.145" y1="0.71958125" x2="17.399" y2="0.80441875" layer="94"/>
<rectangle x1="18.07718125" y1="0.71958125" x2="18.33118125" y2="0.80441875" layer="94"/>
<rectangle x1="2.32918125" y1="0.80441875" x2="2.58318125" y2="0.889" layer="94"/>
<rectangle x1="3.85318125" y1="0.80441875" x2="4.10718125" y2="0.889" layer="94"/>
<rectangle x1="4.36118125" y1="0.80441875" x2="4.61518125" y2="0.889" layer="94"/>
<rectangle x1="5.54481875" y1="0.80441875" x2="5.715" y2="0.889" layer="94"/>
<rectangle x1="5.88518125" y1="0.80441875" x2="6.13918125" y2="0.889" layer="94"/>
<rectangle x1="6.477" y1="0.80441875" x2="6.731" y2="0.889" layer="94"/>
<rectangle x1="7.493" y1="0.80441875" x2="7.747" y2="0.889" layer="94"/>
<rectangle x1="8.763" y1="0.80441875" x2="9.10081875" y2="0.889" layer="94"/>
<rectangle x1="9.44118125" y1="0.80441875" x2="9.69518125" y2="0.889" layer="94"/>
<rectangle x1="10.45718125" y1="0.80441875" x2="10.71118125" y2="0.889" layer="94"/>
<rectangle x1="11.557" y1="0.80441875" x2="11.811" y2="0.889" layer="94"/>
<rectangle x1="11.98118125" y1="0.80441875" x2="12.14881875" y2="0.889" layer="94"/>
<rectangle x1="12.99718125" y1="0.80441875" x2="13.16481875" y2="0.889" layer="94"/>
<rectangle x1="13.67281875" y1="0.80441875" x2="13.92681875" y2="0.889" layer="94"/>
<rectangle x1="15.113" y1="0.80441875" x2="15.367" y2="0.889" layer="94"/>
<rectangle x1="15.621" y1="0.80441875" x2="15.875" y2="0.889" layer="94"/>
<rectangle x1="16.72081875" y1="0.80441875" x2="16.97481875" y2="0.889" layer="94"/>
<rectangle x1="17.145" y1="0.80441875" x2="17.399" y2="0.889" layer="94"/>
<rectangle x1="18.161" y1="0.80441875" x2="18.33118125" y2="0.889" layer="94"/>
<rectangle x1="2.32918125" y1="0.889" x2="2.58318125" y2="0.97358125" layer="94"/>
<rectangle x1="3.937" y1="0.889" x2="4.191" y2="0.97358125" layer="94"/>
<rectangle x1="4.36118125" y1="0.889" x2="4.61518125" y2="0.97358125" layer="94"/>
<rectangle x1="5.54481875" y1="0.889" x2="5.715" y2="0.97358125" layer="94"/>
<rectangle x1="5.88518125" y1="0.889" x2="6.13918125" y2="0.97358125" layer="94"/>
<rectangle x1="6.477" y1="0.889" x2="6.731" y2="0.97358125" layer="94"/>
<rectangle x1="7.493" y1="0.889" x2="7.747" y2="0.97358125" layer="94"/>
<rectangle x1="8.84681875" y1="0.889" x2="9.10081875" y2="0.97358125" layer="94"/>
<rectangle x1="9.35481875" y1="0.889" x2="9.60881875" y2="0.97358125" layer="94"/>
<rectangle x1="10.45718125" y1="0.889" x2="10.71118125" y2="0.97358125" layer="94"/>
<rectangle x1="11.47318125" y1="0.889" x2="11.72718125" y2="0.97358125" layer="94"/>
<rectangle x1="11.89481875" y1="0.889" x2="12.14881875" y2="0.97358125" layer="94"/>
<rectangle x1="13.589" y1="0.889" x2="13.843" y2="0.97358125" layer="94"/>
<rectangle x1="15.19681875" y1="0.889" x2="15.45081875" y2="0.97358125" layer="94"/>
<rectangle x1="15.621" y1="0.889" x2="15.875" y2="0.97358125" layer="94"/>
<rectangle x1="16.72081875" y1="0.889" x2="16.97481875" y2="0.97358125" layer="94"/>
<rectangle x1="17.06118125" y1="0.889" x2="17.31518125" y2="0.97358125" layer="94"/>
<rectangle x1="2.32918125" y1="0.97358125" x2="2.49681875" y2="1.05841875" layer="94"/>
<rectangle x1="3.937" y1="0.97358125" x2="4.191" y2="1.05841875" layer="94"/>
<rectangle x1="4.36118125" y1="0.97358125" x2="4.61518125" y2="1.05841875" layer="94"/>
<rectangle x1="5.54481875" y1="0.97358125" x2="5.715" y2="1.05841875" layer="94"/>
<rectangle x1="5.88518125" y1="0.97358125" x2="6.13918125" y2="1.05841875" layer="94"/>
<rectangle x1="6.477" y1="0.97358125" x2="6.64718125" y2="1.05841875" layer="94"/>
<rectangle x1="7.493" y1="0.97358125" x2="7.747" y2="1.05841875" layer="94"/>
<rectangle x1="8.93318125" y1="0.97358125" x2="9.18718125" y2="1.05841875" layer="94"/>
<rectangle x1="9.35481875" y1="0.97358125" x2="9.60881875" y2="1.05841875" layer="94"/>
<rectangle x1="10.45718125" y1="0.97358125" x2="10.71118125" y2="1.05841875" layer="94"/>
<rectangle x1="11.21918125" y1="0.97358125" x2="11.72718125" y2="1.05841875" layer="94"/>
<rectangle x1="11.89481875" y1="0.97358125" x2="13.25118125" y2="1.05841875" layer="94"/>
<rectangle x1="13.589" y1="0.97358125" x2="13.843" y2="1.05841875" layer="94"/>
<rectangle x1="15.19681875" y1="0.97358125" x2="15.45081875" y2="1.05841875" layer="94"/>
<rectangle x1="15.621" y1="0.97358125" x2="15.875" y2="1.05841875" layer="94"/>
<rectangle x1="16.72081875" y1="0.97358125" x2="16.97481875" y2="1.05841875" layer="94"/>
<rectangle x1="17.06118125" y1="0.97358125" x2="18.415" y2="1.05841875" layer="94"/>
<rectangle x1="2.24281875" y1="1.05841875" x2="2.49681875" y2="1.143" layer="94"/>
<rectangle x1="3.937" y1="1.05841875" x2="4.191" y2="1.143" layer="94"/>
<rectangle x1="4.36118125" y1="1.05841875" x2="4.61518125" y2="1.143" layer="94"/>
<rectangle x1="5.54481875" y1="1.05841875" x2="5.715" y2="1.143" layer="94"/>
<rectangle x1="5.88518125" y1="1.05841875" x2="6.13918125" y2="1.143" layer="94"/>
<rectangle x1="6.477" y1="1.05841875" x2="6.64718125" y2="1.143" layer="94"/>
<rectangle x1="7.493" y1="1.05841875" x2="7.747" y2="1.143" layer="94"/>
<rectangle x1="8.93318125" y1="1.05841875" x2="9.18718125" y2="1.143" layer="94"/>
<rectangle x1="9.35481875" y1="1.05841875" x2="9.60881875" y2="1.143" layer="94"/>
<rectangle x1="10.45718125" y1="1.05841875" x2="10.71118125" y2="1.143" layer="94"/>
<rectangle x1="11.049" y1="1.05841875" x2="11.557" y2="1.143" layer="94"/>
<rectangle x1="11.89481875" y1="1.05841875" x2="13.25118125" y2="1.143" layer="94"/>
<rectangle x1="13.589" y1="1.05841875" x2="13.75918125" y2="1.143" layer="94"/>
<rectangle x1="15.28318125" y1="1.05841875" x2="15.53718125" y2="1.143" layer="94"/>
<rectangle x1="15.621" y1="1.05841875" x2="15.875" y2="1.143" layer="94"/>
<rectangle x1="16.72081875" y1="1.05841875" x2="16.97481875" y2="1.143" layer="94"/>
<rectangle x1="17.06118125" y1="1.05841875" x2="18.415" y2="1.143" layer="94"/>
<rectangle x1="2.24281875" y1="1.143" x2="2.49681875" y2="1.22758125" layer="94"/>
<rectangle x1="4.02081875" y1="1.143" x2="4.191" y2="1.22758125" layer="94"/>
<rectangle x1="4.36118125" y1="1.143" x2="4.61518125" y2="1.22758125" layer="94"/>
<rectangle x1="5.54481875" y1="1.143" x2="5.715" y2="1.22758125" layer="94"/>
<rectangle x1="5.88518125" y1="1.143" x2="6.13918125" y2="1.22758125" layer="94"/>
<rectangle x1="6.477" y1="1.143" x2="6.64718125" y2="1.22758125" layer="94"/>
<rectangle x1="7.493" y1="1.143" x2="7.747" y2="1.22758125" layer="94"/>
<rectangle x1="9.017" y1="1.143" x2="9.271" y2="1.22758125" layer="94"/>
<rectangle x1="9.35481875" y1="1.143" x2="9.60881875" y2="1.22758125" layer="94"/>
<rectangle x1="10.45718125" y1="1.143" x2="10.71118125" y2="1.22758125" layer="94"/>
<rectangle x1="10.87881875" y1="1.143" x2="11.38681875" y2="1.22758125" layer="94"/>
<rectangle x1="11.89481875" y1="1.143" x2="13.25118125" y2="1.22758125" layer="94"/>
<rectangle x1="13.50518125" y1="1.143" x2="13.75918125" y2="1.22758125" layer="94"/>
<rectangle x1="15.28318125" y1="1.143" x2="15.53718125" y2="1.22758125" layer="94"/>
<rectangle x1="15.621" y1="1.143" x2="15.875" y2="1.22758125" layer="94"/>
<rectangle x1="16.72081875" y1="1.143" x2="16.97481875" y2="1.22758125" layer="94"/>
<rectangle x1="17.06118125" y1="1.143" x2="18.415" y2="1.22758125" layer="94"/>
<rectangle x1="2.24281875" y1="1.22758125" x2="2.49681875" y2="1.31241875" layer="94"/>
<rectangle x1="4.02081875" y1="1.22758125" x2="4.191" y2="1.31241875" layer="94"/>
<rectangle x1="4.36118125" y1="1.22758125" x2="4.61518125" y2="1.31241875" layer="94"/>
<rectangle x1="5.461" y1="1.22758125" x2="5.715" y2="1.31241875" layer="94"/>
<rectangle x1="5.88518125" y1="1.22758125" x2="6.13918125" y2="1.31241875" layer="94"/>
<rectangle x1="6.477" y1="1.22758125" x2="6.64718125" y2="1.31241875" layer="94"/>
<rectangle x1="7.493" y1="1.22758125" x2="7.747" y2="1.31241875" layer="94"/>
<rectangle x1="9.017" y1="1.22758125" x2="9.271" y2="1.31241875" layer="94"/>
<rectangle x1="9.35481875" y1="1.22758125" x2="9.60881875" y2="1.31241875" layer="94"/>
<rectangle x1="10.45718125" y1="1.22758125" x2="10.71118125" y2="1.31241875" layer="94"/>
<rectangle x1="10.795" y1="1.22758125" x2="11.21918125" y2="1.31241875" layer="94"/>
<rectangle x1="11.89481875" y1="1.22758125" x2="12.14881875" y2="1.31241875" layer="94"/>
<rectangle x1="12.99718125" y1="1.22758125" x2="13.25118125" y2="1.31241875" layer="94"/>
<rectangle x1="13.50518125" y1="1.22758125" x2="13.75918125" y2="1.31241875" layer="94"/>
<rectangle x1="15.28318125" y1="1.22758125" x2="15.53718125" y2="1.31241875" layer="94"/>
<rectangle x1="15.621" y1="1.22758125" x2="15.875" y2="1.31241875" layer="94"/>
<rectangle x1="16.72081875" y1="1.22758125" x2="16.97481875" y2="1.31241875" layer="94"/>
<rectangle x1="17.06118125" y1="1.22758125" x2="17.31518125" y2="1.31241875" layer="94"/>
<rectangle x1="18.161" y1="1.22758125" x2="18.415" y2="1.31241875" layer="94"/>
<rectangle x1="2.24281875" y1="1.31241875" x2="2.49681875" y2="1.397" layer="94"/>
<rectangle x1="4.02081875" y1="1.31241875" x2="4.191" y2="1.397" layer="94"/>
<rectangle x1="4.36118125" y1="1.31241875" x2="4.699" y2="1.397" layer="94"/>
<rectangle x1="5.461" y1="1.31241875" x2="5.715" y2="1.397" layer="94"/>
<rectangle x1="5.88518125" y1="1.31241875" x2="6.13918125" y2="1.397" layer="94"/>
<rectangle x1="6.477" y1="1.31241875" x2="6.64718125" y2="1.397" layer="94"/>
<rectangle x1="7.493" y1="1.31241875" x2="7.747" y2="1.397" layer="94"/>
<rectangle x1="9.017" y1="1.31241875" x2="9.271" y2="1.397" layer="94"/>
<rectangle x1="9.44118125" y1="1.31241875" x2="9.69518125" y2="1.397" layer="94"/>
<rectangle x1="10.45718125" y1="1.31241875" x2="10.62481875" y2="1.397" layer="94"/>
<rectangle x1="10.795" y1="1.31241875" x2="11.049" y2="1.397" layer="94"/>
<rectangle x1="11.64081875" y1="1.31241875" x2="11.72718125" y2="1.397" layer="94"/>
<rectangle x1="11.98118125" y1="1.31241875" x2="12.23518125" y2="1.397" layer="94"/>
<rectangle x1="12.99718125" y1="1.31241875" x2="13.16481875" y2="1.397" layer="94"/>
<rectangle x1="13.50518125" y1="1.31241875" x2="13.75918125" y2="1.397" layer="94"/>
<rectangle x1="15.28318125" y1="1.31241875" x2="15.53718125" y2="1.397" layer="94"/>
<rectangle x1="15.621" y1="1.31241875" x2="15.875" y2="1.397" layer="94"/>
<rectangle x1="16.637" y1="1.31241875" x2="16.891" y2="1.397" layer="94"/>
<rectangle x1="17.145" y1="1.31241875" x2="17.399" y2="1.397" layer="94"/>
<rectangle x1="18.161" y1="1.31241875" x2="18.415" y2="1.397" layer="94"/>
<rectangle x1="2.24281875" y1="1.397" x2="2.49681875" y2="1.48158125" layer="94"/>
<rectangle x1="4.02081875" y1="1.397" x2="4.191" y2="1.48158125" layer="94"/>
<rectangle x1="4.36118125" y1="1.397" x2="4.78281875" y2="1.48158125" layer="94"/>
<rectangle x1="5.37718125" y1="1.397" x2="5.63118125" y2="1.48158125" layer="94"/>
<rectangle x1="5.88518125" y1="1.397" x2="6.13918125" y2="1.48158125" layer="94"/>
<rectangle x1="6.477" y1="1.397" x2="6.64718125" y2="1.48158125" layer="94"/>
<rectangle x1="7.493" y1="1.397" x2="7.747" y2="1.48158125" layer="94"/>
<rectangle x1="9.017" y1="1.397" x2="9.271" y2="1.48158125" layer="94"/>
<rectangle x1="9.44118125" y1="1.397" x2="9.69518125" y2="1.48158125" layer="94"/>
<rectangle x1="10.37081875" y1="1.397" x2="10.62481875" y2="1.48158125" layer="94"/>
<rectangle x1="10.795" y1="1.397" x2="11.049" y2="1.48158125" layer="94"/>
<rectangle x1="11.557" y1="1.397" x2="11.811" y2="1.48158125" layer="94"/>
<rectangle x1="11.98118125" y1="1.397" x2="12.319" y2="1.48158125" layer="94"/>
<rectangle x1="12.91081875" y1="1.397" x2="13.16481875" y2="1.48158125" layer="94"/>
<rectangle x1="13.50518125" y1="1.397" x2="13.75918125" y2="1.48158125" layer="94"/>
<rectangle x1="15.28318125" y1="1.397" x2="15.53718125" y2="1.48158125" layer="94"/>
<rectangle x1="15.621" y1="1.397" x2="15.95881875" y2="1.48158125" layer="94"/>
<rectangle x1="16.637" y1="1.397" x2="16.891" y2="1.48158125" layer="94"/>
<rectangle x1="17.145" y1="1.397" x2="17.48281875" y2="1.48158125" layer="94"/>
<rectangle x1="18.07718125" y1="1.397" x2="18.33118125" y2="1.48158125" layer="94"/>
<rectangle x1="2.24281875" y1="1.48158125" x2="2.49681875" y2="1.56641875" layer="94"/>
<rectangle x1="4.02081875" y1="1.48158125" x2="4.191" y2="1.56641875" layer="94"/>
<rectangle x1="4.36118125" y1="1.48158125" x2="4.953" y2="1.56641875" layer="94"/>
<rectangle x1="5.207" y1="1.48158125" x2="5.63118125" y2="1.56641875" layer="94"/>
<rectangle x1="5.88518125" y1="1.48158125" x2="6.13918125" y2="1.56641875" layer="94"/>
<rectangle x1="6.477" y1="1.48158125" x2="6.64718125" y2="1.56641875" layer="94"/>
<rectangle x1="7.493" y1="1.48158125" x2="7.747" y2="1.56641875" layer="94"/>
<rectangle x1="9.017" y1="1.48158125" x2="9.271" y2="1.56641875" layer="94"/>
<rectangle x1="9.525" y1="1.48158125" x2="9.86281875" y2="1.56641875" layer="94"/>
<rectangle x1="10.20318125" y1="1.48158125" x2="10.541" y2="1.56641875" layer="94"/>
<rectangle x1="10.87881875" y1="1.48158125" x2="11.21918125" y2="1.56641875" layer="94"/>
<rectangle x1="11.47318125" y1="1.48158125" x2="11.72718125" y2="1.56641875" layer="94"/>
<rectangle x1="12.065" y1="1.48158125" x2="12.40281875" y2="1.56641875" layer="94"/>
<rectangle x1="12.74318125" y1="1.48158125" x2="13.081" y2="1.56641875" layer="94"/>
<rectangle x1="13.589" y1="1.48158125" x2="13.75918125" y2="1.56641875" layer="94"/>
<rectangle x1="15.28318125" y1="1.48158125" x2="15.53718125" y2="1.56641875" layer="94"/>
<rectangle x1="15.621" y1="1.48158125" x2="16.129" y2="1.56641875" layer="94"/>
<rectangle x1="16.46681875" y1="1.48158125" x2="16.80718125" y2="1.56641875" layer="94"/>
<rectangle x1="17.22881875" y1="1.48158125" x2="17.653" y2="1.56641875" layer="94"/>
<rectangle x1="17.907" y1="1.48158125" x2="18.24481875" y2="1.56641875" layer="94"/>
<rectangle x1="2.24281875" y1="1.56641875" x2="2.49681875" y2="1.651" layer="94"/>
<rectangle x1="4.02081875" y1="1.56641875" x2="4.191" y2="1.651" layer="94"/>
<rectangle x1="4.36118125" y1="1.56641875" x2="5.54481875" y2="1.651" layer="94"/>
<rectangle x1="5.88518125" y1="1.56641875" x2="6.13918125" y2="1.651" layer="94"/>
<rectangle x1="6.223" y1="1.56641875" x2="7.239" y2="1.651" layer="94"/>
<rectangle x1="7.493" y1="1.56641875" x2="7.747" y2="1.651" layer="94"/>
<rectangle x1="9.017" y1="1.56641875" x2="9.271" y2="1.651" layer="94"/>
<rectangle x1="9.60881875" y1="1.56641875" x2="10.45718125" y2="1.651" layer="94"/>
<rectangle x1="10.87881875" y1="1.56641875" x2="11.72718125" y2="1.651" layer="94"/>
<rectangle x1="12.14881875" y1="1.56641875" x2="12.99718125" y2="1.651" layer="94"/>
<rectangle x1="13.589" y1="1.56641875" x2="13.843" y2="1.651" layer="94"/>
<rectangle x1="15.19681875" y1="1.56641875" x2="15.45081875" y2="1.651" layer="94"/>
<rectangle x1="15.621" y1="1.56641875" x2="16.72081875" y2="1.651" layer="94"/>
<rectangle x1="17.31518125" y1="1.56641875" x2="18.161" y2="1.651" layer="94"/>
<rectangle x1="2.24281875" y1="1.651" x2="2.49681875" y2="1.73558125" layer="94"/>
<rectangle x1="4.02081875" y1="1.651" x2="4.191" y2="1.73558125" layer="94"/>
<rectangle x1="4.36118125" y1="1.651" x2="4.61518125" y2="1.73558125" layer="94"/>
<rectangle x1="4.78281875" y1="1.651" x2="5.37718125" y2="1.73558125" layer="94"/>
<rectangle x1="5.88518125" y1="1.651" x2="6.13918125" y2="1.73558125" layer="94"/>
<rectangle x1="6.223" y1="1.651" x2="7.239" y2="1.73558125" layer="94"/>
<rectangle x1="7.493" y1="1.651" x2="7.747" y2="1.73558125" layer="94"/>
<rectangle x1="9.017" y1="1.651" x2="9.18718125" y2="1.73558125" layer="94"/>
<rectangle x1="9.69518125" y1="1.651" x2="10.37081875" y2="1.73558125" layer="94"/>
<rectangle x1="11.049" y1="1.651" x2="11.557" y2="1.73558125" layer="94"/>
<rectangle x1="12.23518125" y1="1.651" x2="12.91081875" y2="1.73558125" layer="94"/>
<rectangle x1="13.589" y1="1.651" x2="13.843" y2="1.73558125" layer="94"/>
<rectangle x1="15.19681875" y1="1.651" x2="15.45081875" y2="1.73558125" layer="94"/>
<rectangle x1="15.621" y1="1.651" x2="15.79118125" y2="1.73558125" layer="94"/>
<rectangle x1="15.95881875" y1="1.651" x2="16.637" y2="1.73558125" layer="94"/>
<rectangle x1="17.399" y1="1.651" x2="18.07718125" y2="1.73558125" layer="94"/>
<rectangle x1="2.24281875" y1="1.73558125" x2="2.49681875" y2="1.82041875" layer="94"/>
<rectangle x1="4.02081875" y1="1.73558125" x2="4.191" y2="1.82041875" layer="94"/>
<rectangle x1="4.445" y1="1.73558125" x2="4.52881875" y2="1.82041875" layer="94"/>
<rectangle x1="4.953" y1="1.73558125" x2="5.12318125" y2="1.82041875" layer="94"/>
<rectangle x1="5.969" y1="1.73558125" x2="6.05281875" y2="1.82041875" layer="94"/>
<rectangle x1="6.30681875" y1="1.73558125" x2="7.15518125" y2="1.82041875" layer="94"/>
<rectangle x1="7.493" y1="1.73558125" x2="7.747" y2="1.82041875" layer="94"/>
<rectangle x1="8.93318125" y1="1.73558125" x2="9.18718125" y2="1.82041875" layer="94"/>
<rectangle x1="9.94918125" y1="1.73558125" x2="10.11681875" y2="1.82041875" layer="94"/>
<rectangle x1="11.21918125" y1="1.73558125" x2="11.38681875" y2="1.82041875" layer="94"/>
<rectangle x1="12.48918125" y1="1.73558125" x2="12.65681875" y2="1.82041875" layer="94"/>
<rectangle x1="13.67281875" y1="1.73558125" x2="13.92681875" y2="1.82041875" layer="94"/>
<rectangle x1="15.113" y1="1.73558125" x2="15.367" y2="1.82041875" layer="94"/>
<rectangle x1="16.21281875" y1="1.73558125" x2="16.383" y2="1.82041875" layer="94"/>
<rectangle x1="17.653" y1="1.73558125" x2="17.82318125" y2="1.82041875" layer="94"/>
<rectangle x1="2.24281875" y1="1.82041875" x2="2.49681875" y2="1.905" layer="94"/>
<rectangle x1="4.02081875" y1="1.82041875" x2="4.191" y2="1.905" layer="94"/>
<rectangle x1="5.88518125" y1="1.82041875" x2="6.13918125" y2="1.905" layer="94"/>
<rectangle x1="6.477" y1="1.82041875" x2="6.64718125" y2="1.905" layer="94"/>
<rectangle x1="7.493" y1="1.82041875" x2="7.747" y2="1.905" layer="94"/>
<rectangle x1="8.84681875" y1="1.82041875" x2="9.18718125" y2="1.905" layer="94"/>
<rectangle x1="13.67281875" y1="1.82041875" x2="14.01318125" y2="1.905" layer="94"/>
<rectangle x1="15.02918125" y1="1.82041875" x2="15.367" y2="1.905" layer="94"/>
<rectangle x1="2.24281875" y1="1.905" x2="2.49681875" y2="1.98958125" layer="94"/>
<rectangle x1="4.02081875" y1="1.905" x2="4.191" y2="1.98958125" layer="94"/>
<rectangle x1="5.88518125" y1="1.905" x2="6.13918125" y2="1.98958125" layer="94"/>
<rectangle x1="6.477" y1="1.905" x2="6.64718125" y2="1.98958125" layer="94"/>
<rectangle x1="7.493" y1="1.905" x2="7.747" y2="1.98958125" layer="94"/>
<rectangle x1="8.84681875" y1="1.905" x2="9.10081875" y2="1.98958125" layer="94"/>
<rectangle x1="13.75918125" y1="1.905" x2="14.097" y2="1.98958125" layer="94"/>
<rectangle x1="14.94281875" y1="1.905" x2="15.28318125" y2="1.98958125" layer="94"/>
<rectangle x1="2.24281875" y1="1.98958125" x2="2.49681875" y2="2.07441875" layer="94"/>
<rectangle x1="4.02081875" y1="1.98958125" x2="4.27481875" y2="2.07441875" layer="94"/>
<rectangle x1="5.88518125" y1="1.98958125" x2="6.05281875" y2="2.07441875" layer="94"/>
<rectangle x1="6.477" y1="1.98958125" x2="6.64718125" y2="2.07441875" layer="94"/>
<rectangle x1="7.493" y1="1.98958125" x2="7.747" y2="2.07441875" layer="94"/>
<rectangle x1="8.67918125" y1="1.98958125" x2="9.017" y2="2.07441875" layer="94"/>
<rectangle x1="13.843" y1="1.98958125" x2="14.26718125" y2="2.07441875" layer="94"/>
<rectangle x1="14.77518125" y1="1.98958125" x2="15.19681875" y2="2.07441875" layer="94"/>
<rectangle x1="2.24281875" y1="2.07441875" x2="2.49681875" y2="2.159" layer="94"/>
<rectangle x1="4.02081875" y1="2.07441875" x2="4.27481875" y2="2.159" layer="94"/>
<rectangle x1="6.477" y1="2.07441875" x2="6.64718125" y2="2.159" layer="94"/>
<rectangle x1="7.493" y1="2.07441875" x2="7.747" y2="2.159" layer="94"/>
<rectangle x1="8.59281875" y1="2.07441875" x2="8.93318125" y2="2.159" layer="94"/>
<rectangle x1="13.92681875" y1="2.07441875" x2="15.113" y2="2.159" layer="94"/>
<rectangle x1="2.24281875" y1="2.159" x2="2.49681875" y2="2.24358125" layer="94"/>
<rectangle x1="4.02081875" y1="2.159" x2="4.191" y2="2.24358125" layer="94"/>
<rectangle x1="6.477" y1="2.159" x2="6.64718125" y2="2.24358125" layer="94"/>
<rectangle x1="7.493" y1="2.159" x2="8.84681875" y2="2.24358125" layer="94"/>
<rectangle x1="14.097" y1="2.159" x2="14.94281875" y2="2.24358125" layer="94"/>
<rectangle x1="2.24281875" y1="2.24358125" x2="2.413" y2="2.32841875" layer="94"/>
<rectangle x1="4.02081875" y1="2.24358125" x2="4.191" y2="2.32841875" layer="94"/>
<rectangle x1="6.477" y1="2.24358125" x2="6.64718125" y2="2.32841875" layer="94"/>
<rectangle x1="7.493" y1="2.24358125" x2="8.763" y2="2.32841875" layer="94"/>
<rectangle x1="14.26718125" y1="2.24358125" x2="14.77518125" y2="2.32841875" layer="94"/>
<rectangle x1="7.493" y1="2.32841875" x2="8.509" y2="2.413" layer="94"/>
<rectangle x1="3.34518125" y1="3.25958125" x2="16.80718125" y2="3.34441875" layer="94"/>
<rectangle x1="3.00481875" y1="3.34441875" x2="17.145" y2="3.429" layer="94"/>
<rectangle x1="2.667" y1="3.429" x2="17.399" y2="3.51358125" layer="94"/>
<rectangle x1="2.49681875" y1="3.51358125" x2="17.56918125" y2="3.59841875" layer="94"/>
<rectangle x1="2.32918125" y1="3.59841875" x2="17.82318125" y2="3.683" layer="94"/>
<rectangle x1="2.159" y1="3.683" x2="17.99081875" y2="3.76758125" layer="94"/>
<rectangle x1="1.98881875" y1="3.76758125" x2="18.07718125" y2="3.85241875" layer="94"/>
<rectangle x1="1.905" y1="3.85241875" x2="3.51281875" y2="3.937" layer="94"/>
<rectangle x1="9.779" y1="3.85241875" x2="18.24481875" y2="3.937" layer="94"/>
<rectangle x1="1.73481875" y1="3.937" x2="3.175" y2="4.02158125" layer="94"/>
<rectangle x1="9.779" y1="3.937" x2="18.33118125" y2="4.02158125" layer="94"/>
<rectangle x1="1.651" y1="4.02158125" x2="2.83718125" y2="4.10641875" layer="94"/>
<rectangle x1="9.69518125" y1="4.02158125" x2="18.49881875" y2="4.10641875" layer="94"/>
<rectangle x1="1.56718125" y1="4.10641875" x2="2.667" y2="4.191" layer="94"/>
<rectangle x1="9.60881875" y1="4.10641875" x2="18.58518125" y2="4.191" layer="94"/>
<rectangle x1="1.397" y1="4.191" x2="2.49681875" y2="4.27558125" layer="94"/>
<rectangle x1="9.60881875" y1="4.191" x2="18.669" y2="4.27558125" layer="94"/>
<rectangle x1="1.31318125" y1="4.27558125" x2="2.32918125" y2="4.36041875" layer="94"/>
<rectangle x1="9.525" y1="4.27558125" x2="18.75281875" y2="4.36041875" layer="94"/>
<rectangle x1="1.22681875" y1="4.36041875" x2="2.159" y2="4.445" layer="94"/>
<rectangle x1="9.525" y1="4.36041875" x2="18.83918125" y2="4.445" layer="94"/>
<rectangle x1="1.143" y1="4.445" x2="2.07518125" y2="4.52958125" layer="94"/>
<rectangle x1="9.44118125" y1="4.445" x2="18.923" y2="4.52958125" layer="94"/>
<rectangle x1="1.05918125" y1="4.52958125" x2="1.98881875" y2="4.61441875" layer="94"/>
<rectangle x1="9.44118125" y1="4.52958125" x2="19.00681875" y2="4.61441875" layer="94"/>
<rectangle x1="0.97281875" y1="4.61441875" x2="1.905" y2="4.699" layer="94"/>
<rectangle x1="9.44118125" y1="4.61441875" x2="19.09318125" y2="4.699" layer="94"/>
<rectangle x1="0.97281875" y1="4.699" x2="1.73481875" y2="4.78358125" layer="94"/>
<rectangle x1="9.35481875" y1="4.699" x2="19.177" y2="4.78358125" layer="94"/>
<rectangle x1="0.889" y1="4.78358125" x2="1.651" y2="4.86841875" layer="94"/>
<rectangle x1="9.35481875" y1="4.78358125" x2="19.26081875" y2="4.86841875" layer="94"/>
<rectangle x1="0.80518125" y1="4.86841875" x2="1.56718125" y2="4.953" layer="94"/>
<rectangle x1="9.35481875" y1="4.86841875" x2="19.26081875" y2="4.953" layer="94"/>
<rectangle x1="0.71881875" y1="4.953" x2="1.48081875" y2="5.03758125" layer="94"/>
<rectangle x1="9.271" y1="4.953" x2="19.34718125" y2="5.03758125" layer="94"/>
<rectangle x1="0.71881875" y1="5.03758125" x2="1.48081875" y2="5.12241875" layer="94"/>
<rectangle x1="9.271" y1="5.03758125" x2="19.431" y2="5.12241875" layer="94"/>
<rectangle x1="0.635" y1="5.12241875" x2="1.397" y2="5.207" layer="94"/>
<rectangle x1="9.271" y1="5.12241875" x2="19.431" y2="5.207" layer="94"/>
<rectangle x1="0.55118125" y1="5.207" x2="1.31318125" y2="5.29158125" layer="94"/>
<rectangle x1="9.271" y1="5.207" x2="19.51481875" y2="5.29158125" layer="94"/>
<rectangle x1="0.55118125" y1="5.29158125" x2="1.22681875" y2="5.37641875" layer="94"/>
<rectangle x1="9.18718125" y1="5.29158125" x2="19.51481875" y2="5.37641875" layer="94"/>
<rectangle x1="0.46481875" y1="5.37641875" x2="1.22681875" y2="5.461" layer="94"/>
<rectangle x1="9.18718125" y1="5.37641875" x2="19.60118125" y2="5.461" layer="94"/>
<rectangle x1="0.46481875" y1="5.461" x2="1.143" y2="5.54558125" layer="94"/>
<rectangle x1="9.18718125" y1="5.461" x2="19.685" y2="5.54558125" layer="94"/>
<rectangle x1="0.381" y1="5.54558125" x2="1.05918125" y2="5.63041875" layer="94"/>
<rectangle x1="3.85318125" y1="5.54558125" x2="4.27481875" y2="5.63041875" layer="94"/>
<rectangle x1="5.88518125" y1="5.54558125" x2="6.81481875" y2="5.63041875" layer="94"/>
<rectangle x1="9.18718125" y1="5.54558125" x2="19.685" y2="5.63041875" layer="94"/>
<rectangle x1="0.381" y1="5.63041875" x2="1.05918125" y2="5.715" layer="94"/>
<rectangle x1="3.51281875" y1="5.63041875" x2="4.61518125" y2="5.715" layer="94"/>
<rectangle x1="5.79881875" y1="5.63041875" x2="7.239" y2="5.715" layer="94"/>
<rectangle x1="9.10081875" y1="5.63041875" x2="10.96518125" y2="5.715" layer="94"/>
<rectangle x1="11.64081875" y1="5.63041875" x2="19.685" y2="5.715" layer="94"/>
<rectangle x1="0.381" y1="5.715" x2="0.97281875" y2="5.79958125" layer="94"/>
<rectangle x1="3.34518125" y1="5.715" x2="4.78281875" y2="5.79958125" layer="94"/>
<rectangle x1="5.715" y1="5.715" x2="7.40918125" y2="5.79958125" layer="94"/>
<rectangle x1="9.10081875" y1="5.715" x2="10.71118125" y2="5.79958125" layer="94"/>
<rectangle x1="11.89481875" y1="5.715" x2="13.335" y2="5.79958125" layer="94"/>
<rectangle x1="13.67281875" y1="5.715" x2="15.113" y2="5.79958125" layer="94"/>
<rectangle x1="15.367" y1="5.715" x2="16.383" y2="5.79958125" layer="94"/>
<rectangle x1="17.22881875" y1="5.715" x2="19.76881875" y2="5.79958125" layer="94"/>
<rectangle x1="0.29718125" y1="5.79958125" x2="0.97281875" y2="5.88441875" layer="94"/>
<rectangle x1="3.175" y1="5.79958125" x2="4.953" y2="5.88441875" layer="94"/>
<rectangle x1="5.715" y1="5.79958125" x2="7.57681875" y2="5.88441875" layer="94"/>
<rectangle x1="9.10081875" y1="5.79958125" x2="10.541" y2="5.88441875" layer="94"/>
<rectangle x1="12.065" y1="5.79958125" x2="13.335" y2="5.88441875" layer="94"/>
<rectangle x1="13.67281875" y1="5.79958125" x2="15.113" y2="5.88441875" layer="94"/>
<rectangle x1="15.45081875" y1="5.79958125" x2="16.21281875" y2="5.88441875" layer="94"/>
<rectangle x1="17.48281875" y1="5.79958125" x2="19.76881875" y2="5.88441875" layer="94"/>
<rectangle x1="0.29718125" y1="5.88441875" x2="0.889" y2="5.969" layer="94"/>
<rectangle x1="3.09118125" y1="5.88441875" x2="5.12318125" y2="5.969" layer="94"/>
<rectangle x1="5.715" y1="5.88441875" x2="7.66318125" y2="5.969" layer="94"/>
<rectangle x1="9.10081875" y1="5.88441875" x2="10.37081875" y2="5.969" layer="94"/>
<rectangle x1="12.23518125" y1="5.88441875" x2="13.335" y2="5.969" layer="94"/>
<rectangle x1="13.67281875" y1="5.88441875" x2="15.113" y2="5.969" layer="94"/>
<rectangle x1="15.45081875" y1="5.88441875" x2="16.129" y2="5.969" layer="94"/>
<rectangle x1="17.56918125" y1="5.88441875" x2="19.85518125" y2="5.969" layer="94"/>
<rectangle x1="0.21081875" y1="5.969" x2="0.889" y2="6.05358125" layer="94"/>
<rectangle x1="2.921" y1="5.969" x2="5.207" y2="6.05358125" layer="94"/>
<rectangle x1="5.715" y1="5.969" x2="7.747" y2="6.05358125" layer="94"/>
<rectangle x1="9.10081875" y1="5.969" x2="10.287" y2="6.05358125" layer="94"/>
<rectangle x1="12.319" y1="5.969" x2="13.335" y2="6.05358125" layer="94"/>
<rectangle x1="13.67281875" y1="5.969" x2="15.113" y2="6.05358125" layer="94"/>
<rectangle x1="15.45081875" y1="5.969" x2="16.04518125" y2="6.05358125" layer="94"/>
<rectangle x1="17.653" y1="5.969" x2="19.85518125" y2="6.05358125" layer="94"/>
<rectangle x1="0.21081875" y1="6.05358125" x2="0.889" y2="6.13841875" layer="94"/>
<rectangle x1="2.83718125" y1="6.05358125" x2="5.29081875" y2="6.13841875" layer="94"/>
<rectangle x1="5.79881875" y1="6.05358125" x2="7.83081875" y2="6.13841875" layer="94"/>
<rectangle x1="9.10081875" y1="6.05358125" x2="10.11681875" y2="6.13841875" layer="94"/>
<rectangle x1="12.40281875" y1="6.05358125" x2="13.335" y2="6.13841875" layer="94"/>
<rectangle x1="13.67281875" y1="6.05358125" x2="15.113" y2="6.13841875" layer="94"/>
<rectangle x1="15.45081875" y1="6.05358125" x2="15.95881875" y2="6.13841875" layer="94"/>
<rectangle x1="16.55318125" y1="6.05358125" x2="17.06118125" y2="6.13841875" layer="94"/>
<rectangle x1="17.73681875" y1="6.05358125" x2="19.85518125" y2="6.13841875" layer="94"/>
<rectangle x1="0.21081875" y1="6.13841875" x2="0.80518125" y2="6.223" layer="94"/>
<rectangle x1="2.75081875" y1="6.13841875" x2="5.37718125" y2="6.223" layer="94"/>
<rectangle x1="5.88518125" y1="6.13841875" x2="7.91718125" y2="6.223" layer="94"/>
<rectangle x1="9.017" y1="6.13841875" x2="10.11681875" y2="6.223" layer="94"/>
<rectangle x1="10.96518125" y1="6.13841875" x2="11.64081875" y2="6.223" layer="94"/>
<rectangle x1="12.48918125" y1="6.13841875" x2="13.335" y2="6.223" layer="94"/>
<rectangle x1="13.67281875" y1="6.13841875" x2="15.02918125" y2="6.223" layer="94"/>
<rectangle x1="15.45081875" y1="6.13841875" x2="15.875" y2="6.223" layer="94"/>
<rectangle x1="16.46681875" y1="6.13841875" x2="17.22881875" y2="6.223" layer="94"/>
<rectangle x1="17.73681875" y1="6.13841875" x2="19.939" y2="6.223" layer="94"/>
<rectangle x1="0.21081875" y1="6.223" x2="0.80518125" y2="6.30758125" layer="94"/>
<rectangle x1="2.75081875" y1="6.223" x2="3.683" y2="6.30758125" layer="94"/>
<rectangle x1="4.445" y1="6.223" x2="5.37718125" y2="6.30758125" layer="94"/>
<rectangle x1="7.06881875" y1="6.223" x2="8.001" y2="6.30758125" layer="94"/>
<rectangle x1="9.017" y1="6.223" x2="10.033" y2="6.30758125" layer="94"/>
<rectangle x1="10.795" y1="6.223" x2="11.811" y2="6.30758125" layer="94"/>
<rectangle x1="12.573" y1="6.223" x2="13.335" y2="6.30758125" layer="94"/>
<rectangle x1="13.67281875" y1="6.223" x2="15.02918125" y2="6.30758125" layer="94"/>
<rectangle x1="15.45081875" y1="6.223" x2="15.875" y2="6.30758125" layer="94"/>
<rectangle x1="16.29918125" y1="6.223" x2="17.399" y2="6.30758125" layer="94"/>
<rectangle x1="17.73681875" y1="6.223" x2="19.939" y2="6.30758125" layer="94"/>
<rectangle x1="0.127" y1="6.30758125" x2="0.80518125" y2="6.39241875" layer="94"/>
<rectangle x1="2.667" y1="6.30758125" x2="3.51281875" y2="6.39241875" layer="94"/>
<rectangle x1="4.61518125" y1="6.30758125" x2="5.461" y2="6.39241875" layer="94"/>
<rectangle x1="7.239" y1="6.30758125" x2="8.08481875" y2="6.39241875" layer="94"/>
<rectangle x1="9.017" y1="6.30758125" x2="9.94918125" y2="6.39241875" layer="94"/>
<rectangle x1="10.62481875" y1="6.30758125" x2="11.98118125" y2="6.39241875" layer="94"/>
<rectangle x1="12.65681875" y1="6.30758125" x2="13.335" y2="6.39241875" layer="94"/>
<rectangle x1="13.67281875" y1="6.30758125" x2="15.113" y2="6.39241875" layer="94"/>
<rectangle x1="15.45081875" y1="6.30758125" x2="15.79118125" y2="6.39241875" layer="94"/>
<rectangle x1="16.21281875" y1="6.30758125" x2="17.48281875" y2="6.39241875" layer="94"/>
<rectangle x1="17.653" y1="6.30758125" x2="19.939" y2="6.39241875" layer="94"/>
<rectangle x1="0.127" y1="6.39241875" x2="0.71881875" y2="6.477" layer="94"/>
<rectangle x1="2.58318125" y1="6.39241875" x2="3.34518125" y2="6.477" layer="94"/>
<rectangle x1="4.78281875" y1="6.39241875" x2="5.54481875" y2="6.477" layer="94"/>
<rectangle x1="7.32281875" y1="6.39241875" x2="8.17118125" y2="6.477" layer="94"/>
<rectangle x1="9.017" y1="6.39241875" x2="9.86281875" y2="6.477" layer="94"/>
<rectangle x1="10.541" y1="6.39241875" x2="12.065" y2="6.477" layer="94"/>
<rectangle x1="12.65681875" y1="6.39241875" x2="13.335" y2="6.477" layer="94"/>
<rectangle x1="13.67281875" y1="6.39241875" x2="15.113" y2="6.477" layer="94"/>
<rectangle x1="15.45081875" y1="6.39241875" x2="15.79118125" y2="6.477" layer="94"/>
<rectangle x1="16.21281875" y1="6.39241875" x2="19.939" y2="6.477" layer="94"/>
<rectangle x1="0.127" y1="6.477" x2="0.71881875" y2="6.56158125" layer="94"/>
<rectangle x1="2.58318125" y1="6.477" x2="3.25881875" y2="6.56158125" layer="94"/>
<rectangle x1="4.86918125" y1="6.477" x2="5.54481875" y2="6.56158125" layer="94"/>
<rectangle x1="7.493" y1="6.477" x2="8.17118125" y2="6.56158125" layer="94"/>
<rectangle x1="9.017" y1="6.477" x2="9.86281875" y2="6.56158125" layer="94"/>
<rectangle x1="10.45718125" y1="6.477" x2="12.14881875" y2="6.56158125" layer="94"/>
<rectangle x1="12.74318125" y1="6.477" x2="13.335" y2="6.56158125" layer="94"/>
<rectangle x1="13.67281875" y1="6.477" x2="15.113" y2="6.56158125" layer="94"/>
<rectangle x1="15.45081875" y1="6.477" x2="15.79118125" y2="6.56158125" layer="94"/>
<rectangle x1="16.21281875" y1="6.477" x2="19.939" y2="6.56158125" layer="94"/>
<rectangle x1="0.127" y1="6.56158125" x2="0.71881875" y2="6.64641875" layer="94"/>
<rectangle x1="2.49681875" y1="6.56158125" x2="3.175" y2="6.64641875" layer="94"/>
<rectangle x1="4.953" y1="6.56158125" x2="5.63118125" y2="6.64641875" layer="94"/>
<rectangle x1="7.493" y1="6.56158125" x2="8.255" y2="6.64641875" layer="94"/>
<rectangle x1="9.017" y1="6.56158125" x2="9.779" y2="6.64641875" layer="94"/>
<rectangle x1="10.37081875" y1="6.56158125" x2="12.23518125" y2="6.64641875" layer="94"/>
<rectangle x1="12.74318125" y1="6.56158125" x2="13.335" y2="6.64641875" layer="94"/>
<rectangle x1="13.67281875" y1="6.56158125" x2="15.02918125" y2="6.64641875" layer="94"/>
<rectangle x1="15.45081875" y1="6.56158125" x2="15.79118125" y2="6.64641875" layer="94"/>
<rectangle x1="17.82318125" y1="6.56158125" x2="19.939" y2="6.64641875" layer="94"/>
<rectangle x1="0.127" y1="6.64641875" x2="0.71881875" y2="6.731" layer="94"/>
<rectangle x1="2.49681875" y1="6.64641875" x2="3.175" y2="6.731" layer="94"/>
<rectangle x1="4.953" y1="6.64641875" x2="5.63118125" y2="6.731" layer="94"/>
<rectangle x1="7.57681875" y1="6.64641875" x2="8.255" y2="6.731" layer="94"/>
<rectangle x1="9.017" y1="6.64641875" x2="9.779" y2="6.731" layer="94"/>
<rectangle x1="10.287" y1="6.64641875" x2="12.319" y2="6.731" layer="94"/>
<rectangle x1="12.827" y1="6.64641875" x2="13.335" y2="6.731" layer="94"/>
<rectangle x1="13.67281875" y1="6.64641875" x2="15.02918125" y2="6.731" layer="94"/>
<rectangle x1="15.45081875" y1="6.64641875" x2="15.70481875" y2="6.731" layer="94"/>
<rectangle x1="17.907" y1="6.64641875" x2="19.939" y2="6.731" layer="94"/>
<rectangle x1="0.04318125" y1="6.731" x2="0.71881875" y2="6.81558125" layer="94"/>
<rectangle x1="2.49681875" y1="6.731" x2="3.09118125" y2="6.81558125" layer="94"/>
<rectangle x1="5.03681875" y1="6.731" x2="5.715" y2="6.81558125" layer="94"/>
<rectangle x1="7.66318125" y1="6.731" x2="8.255" y2="6.81558125" layer="94"/>
<rectangle x1="9.017" y1="6.731" x2="9.779" y2="6.81558125" layer="94"/>
<rectangle x1="10.287" y1="6.731" x2="12.319" y2="6.81558125" layer="94"/>
<rectangle x1="12.827" y1="6.731" x2="13.335" y2="6.81558125" layer="94"/>
<rectangle x1="13.67281875" y1="6.731" x2="15.113" y2="6.81558125" layer="94"/>
<rectangle x1="15.45081875" y1="6.731" x2="15.70481875" y2="6.81558125" layer="94"/>
<rectangle x1="17.907" y1="6.731" x2="19.939" y2="6.81558125" layer="94"/>
<rectangle x1="0.04318125" y1="6.81558125" x2="0.71881875" y2="6.90041875" layer="94"/>
<rectangle x1="2.413" y1="6.81558125" x2="3.09118125" y2="6.90041875" layer="94"/>
<rectangle x1="5.03681875" y1="6.81558125" x2="5.715" y2="6.90041875" layer="94"/>
<rectangle x1="7.66318125" y1="6.81558125" x2="8.33881875" y2="6.90041875" layer="94"/>
<rectangle x1="9.017" y1="6.81558125" x2="9.69518125" y2="6.90041875" layer="94"/>
<rectangle x1="10.20318125" y1="6.81558125" x2="12.319" y2="6.90041875" layer="94"/>
<rectangle x1="12.827" y1="6.81558125" x2="13.335" y2="6.90041875" layer="94"/>
<rectangle x1="13.67281875" y1="6.81558125" x2="15.02918125" y2="6.90041875" layer="94"/>
<rectangle x1="15.45081875" y1="6.81558125" x2="15.70481875" y2="6.90041875" layer="94"/>
<rectangle x1="17.907" y1="6.81558125" x2="19.939" y2="6.90041875" layer="94"/>
<rectangle x1="0.04318125" y1="6.90041875" x2="0.635" y2="6.985" layer="94"/>
<rectangle x1="2.413" y1="6.90041875" x2="3.00481875" y2="6.985" layer="94"/>
<rectangle x1="5.12318125" y1="6.90041875" x2="5.715" y2="6.985" layer="94"/>
<rectangle x1="7.66318125" y1="6.90041875" x2="8.33881875" y2="6.985" layer="94"/>
<rectangle x1="9.017" y1="6.90041875" x2="9.69518125" y2="6.985" layer="94"/>
<rectangle x1="10.20318125" y1="6.90041875" x2="12.40281875" y2="6.985" layer="94"/>
<rectangle x1="12.91081875" y1="6.90041875" x2="13.335" y2="6.985" layer="94"/>
<rectangle x1="13.67281875" y1="6.90041875" x2="15.02918125" y2="6.985" layer="94"/>
<rectangle x1="15.45081875" y1="6.90041875" x2="15.79118125" y2="6.985" layer="94"/>
<rectangle x1="16.21281875" y1="6.90041875" x2="17.399" y2="6.985" layer="94"/>
<rectangle x1="17.907" y1="6.90041875" x2="19.939" y2="6.985" layer="94"/>
<rectangle x1="0.04318125" y1="6.985" x2="0.635" y2="7.06958125" layer="94"/>
<rectangle x1="2.413" y1="6.985" x2="3.00481875" y2="7.06958125" layer="94"/>
<rectangle x1="5.12318125" y1="6.985" x2="5.715" y2="7.06958125" layer="94"/>
<rectangle x1="7.747" y1="6.985" x2="8.33881875" y2="7.06958125" layer="94"/>
<rectangle x1="9.017" y1="6.985" x2="9.69518125" y2="7.06958125" layer="94"/>
<rectangle x1="10.20318125" y1="6.985" x2="12.40281875" y2="7.06958125" layer="94"/>
<rectangle x1="12.91081875" y1="6.985" x2="13.335" y2="7.06958125" layer="94"/>
<rectangle x1="13.75918125" y1="6.985" x2="15.02918125" y2="7.06958125" layer="94"/>
<rectangle x1="15.45081875" y1="6.985" x2="15.79118125" y2="7.06958125" layer="94"/>
<rectangle x1="16.21281875" y1="6.985" x2="17.48281875" y2="7.06958125" layer="94"/>
<rectangle x1="17.82318125" y1="6.985" x2="19.939" y2="7.06958125" layer="94"/>
<rectangle x1="0.04318125" y1="7.06958125" x2="0.635" y2="7.15441875" layer="94"/>
<rectangle x1="2.413" y1="7.06958125" x2="3.00481875" y2="7.15441875" layer="94"/>
<rectangle x1="5.12318125" y1="7.06958125" x2="5.715" y2="7.15441875" layer="94"/>
<rectangle x1="7.747" y1="7.06958125" x2="8.33881875" y2="7.15441875" layer="94"/>
<rectangle x1="9.017" y1="7.06958125" x2="9.69518125" y2="7.15441875" layer="94"/>
<rectangle x1="10.20318125" y1="7.06958125" x2="12.40281875" y2="7.15441875" layer="94"/>
<rectangle x1="12.91081875" y1="7.06958125" x2="13.335" y2="7.15441875" layer="94"/>
<rectangle x1="13.75918125" y1="7.06958125" x2="14.94281875" y2="7.15441875" layer="94"/>
<rectangle x1="15.367" y1="7.06958125" x2="15.79118125" y2="7.15441875" layer="94"/>
<rectangle x1="16.21281875" y1="7.06958125" x2="17.399" y2="7.15441875" layer="94"/>
<rectangle x1="17.82318125" y1="7.06958125" x2="19.939" y2="7.15441875" layer="94"/>
<rectangle x1="0.04318125" y1="7.15441875" x2="0.635" y2="7.239" layer="94"/>
<rectangle x1="2.413" y1="7.15441875" x2="3.00481875" y2="7.239" layer="94"/>
<rectangle x1="5.12318125" y1="7.15441875" x2="5.715" y2="7.239" layer="94"/>
<rectangle x1="7.747" y1="7.15441875" x2="8.33881875" y2="7.239" layer="94"/>
<rectangle x1="9.017" y1="7.15441875" x2="9.69518125" y2="7.239" layer="94"/>
<rectangle x1="10.20318125" y1="7.15441875" x2="12.40281875" y2="7.239" layer="94"/>
<rectangle x1="12.91081875" y1="7.15441875" x2="13.335" y2="7.239" layer="94"/>
<rectangle x1="13.843" y1="7.15441875" x2="14.94281875" y2="7.239" layer="94"/>
<rectangle x1="15.367" y1="7.15441875" x2="15.875" y2="7.239" layer="94"/>
<rectangle x1="16.29918125" y1="7.15441875" x2="17.31518125" y2="7.239" layer="94"/>
<rectangle x1="17.82318125" y1="7.15441875" x2="19.939" y2="7.239" layer="94"/>
<rectangle x1="0.04318125" y1="7.239" x2="0.635" y2="7.32358125" layer="94"/>
<rectangle x1="2.413" y1="7.239" x2="3.00481875" y2="7.32358125" layer="94"/>
<rectangle x1="5.12318125" y1="7.239" x2="5.715" y2="7.32358125" layer="94"/>
<rectangle x1="7.747" y1="7.239" x2="8.33881875" y2="7.32358125" layer="94"/>
<rectangle x1="9.017" y1="7.239" x2="9.69518125" y2="7.32358125" layer="94"/>
<rectangle x1="10.20318125" y1="7.239" x2="12.40281875" y2="7.32358125" layer="94"/>
<rectangle x1="12.91081875" y1="7.239" x2="13.335" y2="7.32358125" layer="94"/>
<rectangle x1="13.92681875" y1="7.239" x2="14.859" y2="7.32358125" layer="94"/>
<rectangle x1="15.28318125" y1="7.239" x2="15.875" y2="7.32358125" layer="94"/>
<rectangle x1="16.383" y1="7.239" x2="17.22881875" y2="7.32358125" layer="94"/>
<rectangle x1="17.73681875" y1="7.239" x2="19.939" y2="7.32358125" layer="94"/>
<rectangle x1="0.04318125" y1="7.32358125" x2="0.635" y2="7.40841875" layer="94"/>
<rectangle x1="2.413" y1="7.32358125" x2="3.00481875" y2="7.40841875" layer="94"/>
<rectangle x1="5.12318125" y1="7.32358125" x2="5.715" y2="7.40841875" layer="94"/>
<rectangle x1="7.747" y1="7.32358125" x2="8.33881875" y2="7.40841875" layer="94"/>
<rectangle x1="9.017" y1="7.32358125" x2="9.69518125" y2="7.40841875" layer="94"/>
<rectangle x1="10.20318125" y1="7.32358125" x2="12.40281875" y2="7.40841875" layer="94"/>
<rectangle x1="12.91081875" y1="7.32358125" x2="13.335" y2="7.40841875" layer="94"/>
<rectangle x1="14.01318125" y1="7.32358125" x2="14.68881875" y2="7.40841875" layer="94"/>
<rectangle x1="15.28318125" y1="7.32358125" x2="15.95881875" y2="7.40841875" layer="94"/>
<rectangle x1="16.46681875" y1="7.32358125" x2="17.145" y2="7.40841875" layer="94"/>
<rectangle x1="17.73681875" y1="7.32358125" x2="19.939" y2="7.40841875" layer="94"/>
<rectangle x1="0.04318125" y1="7.40841875" x2="0.635" y2="7.493" layer="94"/>
<rectangle x1="2.413" y1="7.40841875" x2="3.00481875" y2="7.493" layer="94"/>
<rectangle x1="5.12318125" y1="7.40841875" x2="5.715" y2="7.493" layer="94"/>
<rectangle x1="7.747" y1="7.40841875" x2="8.33881875" y2="7.493" layer="94"/>
<rectangle x1="9.017" y1="7.40841875" x2="9.69518125" y2="7.493" layer="94"/>
<rectangle x1="10.20318125" y1="7.40841875" x2="12.40281875" y2="7.493" layer="94"/>
<rectangle x1="12.91081875" y1="7.40841875" x2="13.335" y2="7.493" layer="94"/>
<rectangle x1="14.26718125" y1="7.40841875" x2="14.43481875" y2="7.493" layer="94"/>
<rectangle x1="15.19681875" y1="7.40841875" x2="15.95881875" y2="7.493" layer="94"/>
<rectangle x1="16.72081875" y1="7.40841875" x2="16.891" y2="7.493" layer="94"/>
<rectangle x1="17.653" y1="7.40841875" x2="19.939" y2="7.493" layer="94"/>
<rectangle x1="0.04318125" y1="7.493" x2="0.635" y2="7.57758125" layer="94"/>
<rectangle x1="2.413" y1="7.493" x2="3.00481875" y2="7.57758125" layer="94"/>
<rectangle x1="5.12318125" y1="7.493" x2="5.715" y2="7.57758125" layer="94"/>
<rectangle x1="7.66318125" y1="7.493" x2="8.33881875" y2="7.57758125" layer="94"/>
<rectangle x1="9.017" y1="7.493" x2="9.69518125" y2="7.57758125" layer="94"/>
<rectangle x1="10.20318125" y1="7.493" x2="12.40281875" y2="7.57758125" layer="94"/>
<rectangle x1="12.91081875" y1="7.493" x2="13.335" y2="7.57758125" layer="94"/>
<rectangle x1="15.113" y1="7.493" x2="16.04518125" y2="7.57758125" layer="94"/>
<rectangle x1="17.56918125" y1="7.493" x2="19.939" y2="7.57758125" layer="94"/>
<rectangle x1="0.04318125" y1="7.57758125" x2="0.71881875" y2="7.66241875" layer="94"/>
<rectangle x1="2.413" y1="7.57758125" x2="3.00481875" y2="7.66241875" layer="94"/>
<rectangle x1="5.12318125" y1="7.57758125" x2="5.715" y2="7.66241875" layer="94"/>
<rectangle x1="7.66318125" y1="7.57758125" x2="8.33881875" y2="7.66241875" layer="94"/>
<rectangle x1="9.017" y1="7.57758125" x2="9.69518125" y2="7.66241875" layer="94"/>
<rectangle x1="10.20318125" y1="7.57758125" x2="12.40281875" y2="7.66241875" layer="94"/>
<rectangle x1="12.91081875" y1="7.57758125" x2="13.335" y2="7.66241875" layer="94"/>
<rectangle x1="15.02918125" y1="7.57758125" x2="16.21281875" y2="7.66241875" layer="94"/>
<rectangle x1="17.48281875" y1="7.57758125" x2="19.939" y2="7.66241875" layer="94"/>
<rectangle x1="0.04318125" y1="7.66241875" x2="0.71881875" y2="7.747" layer="94"/>
<rectangle x1="2.413" y1="7.66241875" x2="3.00481875" y2="7.747" layer="94"/>
<rectangle x1="5.12318125" y1="7.66241875" x2="5.715" y2="7.747" layer="94"/>
<rectangle x1="7.66318125" y1="7.66241875" x2="8.255" y2="7.747" layer="94"/>
<rectangle x1="9.017" y1="7.66241875" x2="9.779" y2="7.747" layer="94"/>
<rectangle x1="10.287" y1="7.66241875" x2="12.319" y2="7.747" layer="94"/>
<rectangle x1="12.827" y1="7.66241875" x2="13.335" y2="7.747" layer="94"/>
<rectangle x1="13.67281875" y1="7.66241875" x2="13.843" y2="7.747" layer="94"/>
<rectangle x1="14.859" y1="7.66241875" x2="16.29918125" y2="7.747" layer="94"/>
<rectangle x1="17.31518125" y1="7.66241875" x2="19.939" y2="7.747" layer="94"/>
<rectangle x1="0.127" y1="7.747" x2="0.71881875" y2="7.83158125" layer="94"/>
<rectangle x1="2.413" y1="7.747" x2="3.00481875" y2="7.83158125" layer="94"/>
<rectangle x1="5.12318125" y1="7.747" x2="5.715" y2="7.83158125" layer="94"/>
<rectangle x1="7.57681875" y1="7.747" x2="8.255" y2="7.83158125" layer="94"/>
<rectangle x1="9.017" y1="7.747" x2="9.779" y2="7.83158125" layer="94"/>
<rectangle x1="10.287" y1="7.747" x2="12.319" y2="7.83158125" layer="94"/>
<rectangle x1="12.827" y1="7.747" x2="13.335" y2="7.83158125" layer="94"/>
<rectangle x1="13.589" y1="7.747" x2="14.097" y2="7.83158125" layer="94"/>
<rectangle x1="14.68881875" y1="7.747" x2="16.55318125" y2="7.83158125" layer="94"/>
<rectangle x1="17.06118125" y1="7.747" x2="19.939" y2="7.83158125" layer="94"/>
<rectangle x1="0.127" y1="7.83158125" x2="0.71881875" y2="7.91641875" layer="94"/>
<rectangle x1="2.413" y1="7.83158125" x2="3.00481875" y2="7.91641875" layer="94"/>
<rectangle x1="5.12318125" y1="7.83158125" x2="5.715" y2="7.91641875" layer="94"/>
<rectangle x1="7.57681875" y1="7.83158125" x2="8.255" y2="7.91641875" layer="94"/>
<rectangle x1="9.017" y1="7.83158125" x2="9.779" y2="7.91641875" layer="94"/>
<rectangle x1="10.37081875" y1="7.83158125" x2="12.23518125" y2="7.91641875" layer="94"/>
<rectangle x1="12.827" y1="7.83158125" x2="19.939" y2="7.91641875" layer="94"/>
<rectangle x1="0.127" y1="7.91641875" x2="0.71881875" y2="8.001" layer="94"/>
<rectangle x1="2.413" y1="7.91641875" x2="3.00481875" y2="8.001" layer="94"/>
<rectangle x1="5.12318125" y1="7.91641875" x2="5.715" y2="8.001" layer="94"/>
<rectangle x1="7.493" y1="7.91641875" x2="8.17118125" y2="8.001" layer="94"/>
<rectangle x1="9.017" y1="7.91641875" x2="9.86281875" y2="8.001" layer="94"/>
<rectangle x1="10.45718125" y1="7.91641875" x2="12.14881875" y2="8.001" layer="94"/>
<rectangle x1="12.74318125" y1="7.91641875" x2="19.939" y2="8.001" layer="94"/>
<rectangle x1="0.127" y1="8.001" x2="0.71881875" y2="8.08558125" layer="94"/>
<rectangle x1="2.413" y1="8.001" x2="3.00481875" y2="8.08558125" layer="94"/>
<rectangle x1="5.12318125" y1="8.001" x2="5.715" y2="8.08558125" layer="94"/>
<rectangle x1="7.40918125" y1="8.001" x2="8.17118125" y2="8.08558125" layer="94"/>
<rectangle x1="9.017" y1="8.001" x2="9.86281875" y2="8.08558125" layer="94"/>
<rectangle x1="10.45718125" y1="8.001" x2="12.065" y2="8.08558125" layer="94"/>
<rectangle x1="12.74318125" y1="8.001" x2="19.939" y2="8.08558125" layer="94"/>
<rectangle x1="0.127" y1="8.08558125" x2="0.80518125" y2="8.17041875" layer="94"/>
<rectangle x1="2.413" y1="8.08558125" x2="3.00481875" y2="8.17041875" layer="94"/>
<rectangle x1="5.12318125" y1="8.08558125" x2="5.715" y2="8.17041875" layer="94"/>
<rectangle x1="7.32281875" y1="8.08558125" x2="8.08481875" y2="8.17041875" layer="94"/>
<rectangle x1="9.017" y1="8.08558125" x2="9.94918125" y2="8.17041875" layer="94"/>
<rectangle x1="10.62481875" y1="8.08558125" x2="11.98118125" y2="8.17041875" layer="94"/>
<rectangle x1="12.65681875" y1="8.08558125" x2="19.939" y2="8.17041875" layer="94"/>
<rectangle x1="0.127" y1="8.17041875" x2="0.80518125" y2="8.255" layer="94"/>
<rectangle x1="2.413" y1="8.17041875" x2="3.00481875" y2="8.255" layer="94"/>
<rectangle x1="5.12318125" y1="8.17041875" x2="5.715" y2="8.255" layer="94"/>
<rectangle x1="7.15518125" y1="8.17041875" x2="8.001" y2="8.255" layer="94"/>
<rectangle x1="9.017" y1="8.17041875" x2="9.94918125" y2="8.255" layer="94"/>
<rectangle x1="10.71118125" y1="8.17041875" x2="11.89481875" y2="8.255" layer="94"/>
<rectangle x1="12.573" y1="8.17041875" x2="19.939" y2="8.255" layer="94"/>
<rectangle x1="0.21081875" y1="8.255" x2="0.80518125" y2="8.33958125" layer="94"/>
<rectangle x1="2.413" y1="8.255" x2="3.00481875" y2="8.33958125" layer="94"/>
<rectangle x1="5.12318125" y1="8.255" x2="5.715" y2="8.33958125" layer="94"/>
<rectangle x1="6.90118125" y1="8.255" x2="8.001" y2="8.33958125" layer="94"/>
<rectangle x1="9.017" y1="8.255" x2="10.033" y2="8.33958125" layer="94"/>
<rectangle x1="10.87881875" y1="8.255" x2="11.72718125" y2="8.33958125" layer="94"/>
<rectangle x1="12.573" y1="8.255" x2="19.939" y2="8.33958125" layer="94"/>
<rectangle x1="0.21081875" y1="8.33958125" x2="0.889" y2="8.42441875" layer="94"/>
<rectangle x1="2.413" y1="8.33958125" x2="3.00481875" y2="8.42441875" layer="94"/>
<rectangle x1="5.12318125" y1="8.33958125" x2="7.91718125" y2="8.42441875" layer="94"/>
<rectangle x1="9.10081875" y1="8.33958125" x2="10.11681875" y2="8.42441875" layer="94"/>
<rectangle x1="11.21918125" y1="8.33958125" x2="11.38681875" y2="8.42441875" layer="94"/>
<rectangle x1="12.48918125" y1="8.33958125" x2="19.85518125" y2="8.42441875" layer="94"/>
<rectangle x1="0.21081875" y1="8.42441875" x2="0.889" y2="8.509" layer="94"/>
<rectangle x1="2.413" y1="8.42441875" x2="3.00481875" y2="8.509" layer="94"/>
<rectangle x1="5.12318125" y1="8.42441875" x2="7.83081875" y2="8.509" layer="94"/>
<rectangle x1="9.10081875" y1="8.42441875" x2="10.20318125" y2="8.509" layer="94"/>
<rectangle x1="12.40281875" y1="8.42441875" x2="19.85518125" y2="8.509" layer="94"/>
<rectangle x1="0.29718125" y1="8.509" x2="0.889" y2="8.59358125" layer="94"/>
<rectangle x1="2.413" y1="8.509" x2="3.00481875" y2="8.59358125" layer="94"/>
<rectangle x1="5.12318125" y1="8.509" x2="7.747" y2="8.59358125" layer="94"/>
<rectangle x1="9.10081875" y1="8.509" x2="10.287" y2="8.59358125" layer="94"/>
<rectangle x1="12.23518125" y1="8.509" x2="19.85518125" y2="8.59358125" layer="94"/>
<rectangle x1="0.29718125" y1="8.59358125" x2="0.97281875" y2="8.67841875" layer="94"/>
<rectangle x1="2.413" y1="8.59358125" x2="3.00481875" y2="8.67841875" layer="94"/>
<rectangle x1="5.12318125" y1="8.59358125" x2="7.57681875" y2="8.67841875" layer="94"/>
<rectangle x1="9.10081875" y1="8.59358125" x2="10.45718125" y2="8.67841875" layer="94"/>
<rectangle x1="12.14881875" y1="8.59358125" x2="19.76881875" y2="8.67841875" layer="94"/>
<rectangle x1="0.29718125" y1="8.67841875" x2="0.97281875" y2="8.763" layer="94"/>
<rectangle x1="2.413" y1="8.67841875" x2="3.00481875" y2="8.763" layer="94"/>
<rectangle x1="5.12318125" y1="8.67841875" x2="7.493" y2="8.763" layer="94"/>
<rectangle x1="9.10081875" y1="8.67841875" x2="10.62481875" y2="8.763" layer="94"/>
<rectangle x1="11.98118125" y1="8.67841875" x2="19.76881875" y2="8.763" layer="94"/>
<rectangle x1="0.381" y1="8.763" x2="1.05918125" y2="8.84758125" layer="94"/>
<rectangle x1="2.413" y1="8.763" x2="2.921" y2="8.84758125" layer="94"/>
<rectangle x1="5.207" y1="8.763" x2="7.32281875" y2="8.84758125" layer="94"/>
<rectangle x1="9.10081875" y1="8.763" x2="10.795" y2="8.84758125" layer="94"/>
<rectangle x1="11.72718125" y1="8.763" x2="19.76881875" y2="8.84758125" layer="94"/>
<rectangle x1="0.381" y1="8.84758125" x2="1.05918125" y2="8.93241875" layer="94"/>
<rectangle x1="2.58318125" y1="8.84758125" x2="2.83718125" y2="8.93241875" layer="94"/>
<rectangle x1="5.29081875" y1="8.84758125" x2="7.06881875" y2="8.93241875" layer="94"/>
<rectangle x1="9.18718125" y1="8.84758125" x2="19.685" y2="8.93241875" layer="94"/>
<rectangle x1="0.46481875" y1="8.93241875" x2="1.143" y2="9.017" layer="94"/>
<rectangle x1="9.18718125" y1="8.93241875" x2="19.685" y2="9.017" layer="94"/>
<rectangle x1="0.46481875" y1="9.017" x2="1.143" y2="9.10158125" layer="94"/>
<rectangle x1="9.18718125" y1="9.017" x2="19.60118125" y2="9.10158125" layer="94"/>
<rectangle x1="0.55118125" y1="9.10158125" x2="1.22681875" y2="9.18641875" layer="94"/>
<rectangle x1="9.18718125" y1="9.10158125" x2="19.60118125" y2="9.18641875" layer="94"/>
<rectangle x1="0.55118125" y1="9.18641875" x2="1.31318125" y2="9.271" layer="94"/>
<rectangle x1="9.18718125" y1="9.18641875" x2="19.51481875" y2="9.271" layer="94"/>
<rectangle x1="0.635" y1="9.271" x2="1.397" y2="9.35558125" layer="94"/>
<rectangle x1="9.271" y1="9.271" x2="19.51481875" y2="9.35558125" layer="94"/>
<rectangle x1="0.71881875" y1="9.35558125" x2="1.397" y2="9.44041875" layer="94"/>
<rectangle x1="9.271" y1="9.35558125" x2="19.431" y2="9.44041875" layer="94"/>
<rectangle x1="0.71881875" y1="9.44041875" x2="1.48081875" y2="9.525" layer="94"/>
<rectangle x1="9.271" y1="9.44041875" x2="19.34718125" y2="9.525" layer="94"/>
<rectangle x1="0.80518125" y1="9.525" x2="1.56718125" y2="9.60958125" layer="94"/>
<rectangle x1="9.35481875" y1="9.525" x2="19.34718125" y2="9.60958125" layer="94"/>
<rectangle x1="0.889" y1="9.60958125" x2="1.651" y2="9.69441875" layer="94"/>
<rectangle x1="9.35481875" y1="9.60958125" x2="19.26081875" y2="9.69441875" layer="94"/>
<rectangle x1="0.889" y1="9.69441875" x2="1.73481875" y2="9.779" layer="94"/>
<rectangle x1="9.35481875" y1="9.69441875" x2="19.177" y2="9.779" layer="94"/>
<rectangle x1="0.97281875" y1="9.779" x2="1.82118125" y2="9.86358125" layer="94"/>
<rectangle x1="9.44118125" y1="9.779" x2="19.09318125" y2="9.86358125" layer="94"/>
<rectangle x1="1.05918125" y1="9.86358125" x2="1.905" y2="9.94841875" layer="94"/>
<rectangle x1="9.44118125" y1="9.86358125" x2="19.00681875" y2="9.94841875" layer="94"/>
<rectangle x1="1.143" y1="9.94841875" x2="2.07518125" y2="10.033" layer="94"/>
<rectangle x1="9.44118125" y1="9.94841875" x2="18.923" y2="10.033" layer="94"/>
<rectangle x1="1.22681875" y1="10.033" x2="2.159" y2="10.11758125" layer="94"/>
<rectangle x1="9.525" y1="10.033" x2="18.923" y2="10.11758125" layer="94"/>
<rectangle x1="1.31318125" y1="10.11758125" x2="2.24281875" y2="10.20241875" layer="94"/>
<rectangle x1="9.525" y1="10.11758125" x2="18.75281875" y2="10.20241875" layer="94"/>
<rectangle x1="1.397" y1="10.20241875" x2="2.413" y2="10.287" layer="94"/>
<rectangle x1="9.60881875" y1="10.20241875" x2="18.669" y2="10.287" layer="94"/>
<rectangle x1="1.48081875" y1="10.287" x2="2.58318125" y2="10.37158125" layer="94"/>
<rectangle x1="9.60881875" y1="10.287" x2="18.58518125" y2="10.37158125" layer="94"/>
<rectangle x1="1.56718125" y1="10.37158125" x2="2.75081875" y2="10.45641875" layer="94"/>
<rectangle x1="9.69518125" y1="10.37158125" x2="18.49881875" y2="10.45641875" layer="94"/>
<rectangle x1="1.73481875" y1="10.45641875" x2="3.00481875" y2="10.541" layer="94"/>
<rectangle x1="9.69518125" y1="10.45641875" x2="18.415" y2="10.541" layer="94"/>
<rectangle x1="1.82118125" y1="10.541" x2="3.34518125" y2="10.62558125" layer="94"/>
<rectangle x1="9.779" y1="10.541" x2="18.24481875" y2="10.62558125" layer="94"/>
<rectangle x1="1.98881875" y1="10.62558125" x2="9.69518125" y2="10.71041875" layer="94"/>
<rectangle x1="9.779" y1="10.62558125" x2="18.161" y2="10.71041875" layer="94"/>
<rectangle x1="2.07518125" y1="10.71041875" x2="17.99081875" y2="10.795" layer="94"/>
<rectangle x1="2.24281875" y1="10.795" x2="17.82318125" y2="10.87958125" layer="94"/>
<rectangle x1="2.413" y1="10.87958125" x2="17.653" y2="10.96441875" layer="94"/>
<rectangle x1="2.58318125" y1="10.96441875" x2="17.48281875" y2="11.049" layer="94"/>
<rectangle x1="2.83718125" y1="11.049" x2="17.22881875" y2="11.13358125" layer="94"/>
<rectangle x1="3.175" y1="11.13358125" x2="16.891" y2="11.21841875" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="UDO-LOGO-" prefix="LOGO" uservalue="yes">
<gates>
<gate name="LOGO" symbol="UDO-LOGO-20MM" x="0" y="0"/>
</gates>
<devices>
<device name="10MM" package="UDO-LOGO-10MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6649258/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="12MM" package="UDO-LOGO-12MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6649257/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="15MM" package="UDO-LOGO-15MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6649256/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="20MM" package="UDO-LOGO-20MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="30MM" package="UDO-LOGO-30MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="40MM" package="UDO-LOGO-40MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-panasonic-relay">
<packages>
<package name="PA1A-PS-HORIZONTAL" urn="urn:adsk.eagle:footprint:6226245/2" locally_modified="yes">
<pad name="1" x="-8.89" y="0" drill="0.7"/>
<pad name="2" x="-6.35" y="0" drill="0.7"/>
<pad name="NO" x="8.89" y="0" drill="1"/>
<pad name="COM" x="3.81" y="0" drill="1"/>
<wire x1="-11.2" y1="19.1" x2="11.2" y2="19.1" width="0.2032" layer="21"/>
<wire x1="11.2" y1="19.1" x2="11.2" y2="1" width="0.2032" layer="21"/>
<wire x1="11.2" y1="1" x2="-11.2" y2="1" width="0.2032" layer="21"/>
<wire x1="-11.2" y1="1" x2="-11.2" y2="19.1" width="0.2032" layer="21"/>
<wire x1="11.3" y1="15.595240625" x2="11.3" y2="3.295240625" width="0.1524" layer="51"/>
<wire x1="11.3" y1="3.295240625" x2="11.3" y2="2.995240625" width="0.1524" layer="51"/>
<wire x1="11.3" y1="2.995240625" x2="11.3" y2="1.295240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="15.595240625" x2="-11.3" y2="3.295240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="3.295240625" x2="-11.3" y2="2.995240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="2.995240625" x2="-11.3" y2="1.295240625" width="0.1524" layer="51"/>
<wire x1="-9.6" y1="6.595240625" x2="-5.21" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="-5.21" y1="6.595240625" x2="-2.41" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="-2.41" y1="6.595240625" x2="4.95" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="4.95" y1="6.595240625" x2="7.75" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="7.75" y1="6.595240625" x2="9.6" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="1.295240625" x2="-10.6" y2="1.295240625" width="0.1524" layer="51"/>
<wire x1="-10.6" y1="1.595240625" x2="-10.6" y2="1.295240625" width="0.1524" layer="51"/>
<wire x1="10.6" y1="1.595240625" x2="10.6" y2="1.295240625" width="0.1524" layer="51"/>
<wire x1="10.6" y1="1.295240625" x2="11.3" y2="1.295240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="15.595240625" x2="-10.200009375" y2="15.595240625" width="0.1524" layer="51"/>
<wire x1="-10.2" y1="15.595240625" x2="-9.8" y2="14.995240625" width="0.1524" layer="51"/>
<wire x1="10.2" y1="15.595240625" x2="11.3" y2="15.595240625" width="0.1524" layer="51"/>
<wire x1="9.8" y1="14.995240625" x2="10.074571875" y2="6.60833125" width="0.1524" layer="51"/>
<wire x1="-10.074578125" y1="6.60833125" x2="-9.8" y2="14.995240625" width="0.1524" layer="51"/>
<wire x1="9.6" y1="6.595240625" x2="9.6" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="-5.11" y1="6.195240625" x2="-2.51" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="-5.21" y1="6.595240625" x2="-5.11" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="-2.51" y1="6.195240625" x2="-2.41" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="9.6" y1="6.195240625" x2="9.67478125" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="-10.6" y1="1.595240625" x2="-9.14" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="-9.14" y1="1.595240625" x2="-8.19" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="-8.19" y1="1.595240625" x2="-6.6" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="-6.6" y1="1.595240625" x2="-5.65" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="-5.65" y1="1.595240625" x2="3.41" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="3.41" y1="1.595240625" x2="4.51" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="4.51" y1="1.595240625" x2="8.49" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="8.49" y1="1.595240625" x2="9.59" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="9.59" y1="1.595240625" x2="10.6" y2="1.595240625" width="0.1524" layer="51"/>
<wire x1="-9.6" y1="6.595240625" x2="-9.6" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="-9.6747875" y1="6.195240625" x2="-9.6" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="4.95" y1="6.595240625" x2="5.05" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="7.65" y1="6.195240625" x2="7.75" y2="6.595240625" width="0.1524" layer="51"/>
<wire x1="5.05" y1="6.195240625" x2="7.65" y2="6.195240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="3.295240625" x2="11.3" y2="3.295240625" width="0.1524" layer="51"/>
<wire x1="-11.3" y1="2.995240625" x2="11.3" y2="2.995240625" width="0.1524" layer="51"/>
<wire x1="9.8" y1="14.995240625" x2="10.2" y2="15.595240625" width="0.1524" layer="51"/>
<wire x1="-8.69" y1="-2.704759375" x2="-9.09" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="-9.14" y1="1.595240625" x2="-9.14" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="-8.64" y1="1.345240625" x2="-8.64" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="-8.64" y1="-2.104759375" x2="-8.69" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="-9.09" y1="-2.704759375" x2="-9.14" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="-8.19" y1="1.595240625" x2="-8.64" y2="1.345240625" width="0.1524" layer="51"/>
<wire x1="-6.15" y1="-2.704759375" x2="-6.55" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="-6.6" y1="1.595240625" x2="-6.6" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="-6.1" y1="1.345240625" x2="-6.1" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="-6.1" y1="-2.104759375" x2="-6.15" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="-6.55" y1="-2.704759375" x2="-6.6" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="-5.65" y1="1.595240625" x2="-6.1" y2="1.345240625" width="0.1524" layer="51"/>
<wire x1="4.11" y1="-2.704759375" x2="3.51" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="3.41" y1="1.595240625" x2="3.41" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="4.21" y1="1.345240625" x2="4.21" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="4.21" y1="-2.104759375" x2="4.11" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="3.509990625" y1="-2.704759375" x2="3.41" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="4.51" y1="1.595240625" x2="4.21" y2="1.345240625" width="0.1524" layer="51"/>
<wire x1="9.19" y1="-2.704759375" x2="8.59" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="8.49" y1="1.595240625" x2="8.49" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="9.29" y1="1.345240625" x2="9.29" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="9.29" y1="-2.104759375" x2="9.19" y2="-2.704759375" width="0.1524" layer="51"/>
<wire x1="8.59" y1="-2.704759375" x2="8.49" y2="-2.104759375" width="0.1524" layer="51"/>
<wire x1="9.59" y1="1.595240625" x2="9.29" y2="1.345240625" width="0.1524" layer="51"/>
<wire x1="9.67478125" y1="6.195240625" x2="10.074565625" y2="6.60833125" width="0.1524" layer="51" curve="91.875435"/>
<wire x1="-10.07456875" y1="6.60833125" x2="-9.6747875" y2="6.195240625" width="0.1524" layer="51" curve="91.874988"/>
<text x="-12" y="2" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="13.0266" y="2.0984" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<circle x="11" y="5" radius="1" width="0" layer="35"/>
<circle x="-11" y="5" radius="1" width="0" layer="35"/>
</package>
<package name="PA1A-PS-VERTICAL" urn="urn:adsk.eagle:footprint:6395346/2" locally_modified="yes">
<pad name="1" x="-8.89" y="0" drill="0.7"/>
<pad name="2" x="-6.35" y="0" drill="0.7"/>
<pad name="NO" x="8.89" y="0" drill="1"/>
<pad name="COM" x="3.81" y="0" drill="1"/>
<wire x1="11.3" y1="-3.77" x2="-11.3" y2="-3.77" width="0.2032" layer="21"/>
<wire x1="11.3" y1="1.23" x2="-11.3" y2="1.23" width="0.2032" layer="21"/>
<wire x1="-11.3" y1="1.23" x2="-11.3" y2="-3.77" width="0.2032" layer="21"/>
<wire x1="11.3" y1="1.23" x2="11.3" y2="-3.77" width="0.2032" layer="21"/>
<text x="-11" y="1.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-11" y="-4" size="1.27" layer="27" align="top-left">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="PA1A-PS-HORIZONTAL" urn="urn:adsk.eagle:package:6226248/5" locally_modified="yes" type="model">
<packageinstances>
<packageinstance name="PA1A-PS-HORIZONTAL"/>
</packageinstances>
</package3d>
<package3d name="PA1A-PS-VERTICAL" urn="urn:adsk.eagle:package:6395349/4" locally_modified="yes" type="model">
<packageinstances>
<packageinstance name="PA1A-PS-VERTICAL"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="K">
<wire x1="-3.81" y1="-1.905" x2="-1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.905" x2="1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="1.905" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<text x="1.27" y="2.921" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.27" y="5.08" size="1.778" layer="95">&gt;PART</text>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<text x="-2.54" y="-2.54" size="1.27" layer="94" rot="R180">(+)</text>
</symbol>
<symbol name="S">
<wire x1="0" y1="3.175" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-3.175" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="95" rot="R90">&gt;PART</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="0.762" y="3.81" size="1.27" layer="94" rot="R270">(+)</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PA1A-PS" prefix="REL">
<gates>
<gate name="K" symbol="K" x="-7.62" y="0"/>
<gate name="S" symbol="S" x="7.62" y="0"/>
</gates>
<devices>
<device name="(HORIZONTAL)" package="PA1A-PS-HORIZONTAL">
<connects>
<connect gate="K" pin="1" pad="1"/>
<connect gate="K" pin="2" pad="2"/>
<connect gate="S" pin="P" pad="COM"/>
<connect gate="S" pin="S" pad="NO"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6226248/5"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Relay socket" constant="no"/>
<attribute name="MF" value="PANASONIC"/>
<attribute name="MPN" value="PA1A-PS" constant="no"/>
</technology>
</technologies>
</device>
<device name="(VERTICAL)" package="PA1A-PS-VERTICAL">
<connects>
<connect gate="K" pin="1" pad="1"/>
<connect gate="K" pin="2" pad="2"/>
<connect gate="S" pin="P" pad="COM"/>
<connect gate="S" pin="S" pad="NO"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6395349/4"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Relay socket" constant="no"/>
<attribute name="MF" value="PANASONIC" constant="no"/>
<attribute name="MPN" value="PA1A-PS" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-diode">
<packages>
<package name="MINIMELF" urn="urn:adsk.eagle:footprint:43207/1">
<description>&lt;b&gt;Mini Melf Diode&lt;/b&gt;</description>
<wire x1="1.3208" y1="0.7874" x2="-1.3208" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.7874" x2="-1.3208" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="0.5" y1="0.5" x2="-0.5" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-0.5" x2="0.5" y2="0.5" width="0.2032" layer="21"/>
<smd name="C" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="A" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8636" x2="-1.2954" y2="0.8636" layer="51"/>
<rectangle x1="1.2954" y1="-0.8636" x2="1.8542" y2="0.8636" layer="51"/>
<rectangle x1="-0.8636" y1="-0.7874" x2="-0.254" y2="0.7874" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="MINIMELF" urn="urn:adsk.eagle:package:7567381/2" type="model">
<description>Mini Melf Diode</description>
<packageinstances>
<packageinstance name="MINIMELF"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
<text x="2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LL4148" prefix="D">
<gates>
<gate name="D" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MINIMELF">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7567381/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Diode, generic, 100V/0.15A, MiniMELF"/>
<attribute name="MF" value="any" constant="no"/>
<attribute name="MPN" value="LL4148" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+24V" urn="urn:adsk.eagle:symbol:26935/1" library_version="1">
<wire x1="1.27" y1="-0.635" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+24V" urn="urn:adsk.eagle:component:26964/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+24V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="blebox-dummy">
<packages>
<package name="EXTRA-BOM-LINE">
<text x="0" y="0" size="1.778" layer="48">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="48">&gt;VALUE</text>
<circle x="-2.54" y="0" radius="2.300065625" width="0.127" layer="48"/>
<wire x1="-4.064" y1="0" x2="-1.016" y2="0" width="0.127" layer="48"/>
<wire x1="-2.54" y1="1.524" x2="-2.54" y2="-1.27" width="0.127" layer="48"/>
</package>
</packages>
<symbols>
<symbol name="BOM">
<text x="2.54" y="2.54" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="2.54" y="0" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<circle x="0" y="0" radius="2.0478125" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="-2.54" size="1.778" layer="97" align="center-left">&gt;DESCRIPTION</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BOM" prefix="X" uservalue="yes">
<description>Dummy symbol for inserting extra BOM lines.</description>
<gates>
<gate name="X" symbol="BOM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EXTRA-BOM-LINE">
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DGND" urn="urn:adsk.eagle:symbol:27019/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-2.667" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="DGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DGND" urn="urn:adsk.eagle:component:27076/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-wago-terminal-blocks">
<packages>
<package name="P-2059-302/998-403" urn="urn:adsk.eagle:footprint:8559212/1">
<wire x1="-3.475" y1="2.15" x2="2.325" y2="2.15" width="0.01" layer="51"/>
<wire x1="-3.125" y1="1.1" x2="-3.125" y2="1.9" width="0.01" layer="51"/>
<wire x1="-3.825" y1="1.1" x2="-3.825" y2="1.9" width="0.2" layer="51"/>
<wire x1="3.825" y1="1.1" x2="3.825" y2="1.9" width="0.01" layer="51"/>
<wire x1="3.125" y1="1.1" x2="3.125" y2="1.9" width="0.01" layer="51"/>
<wire x1="-3.475" y1="-0.85" x2="2.325" y2="-0.85" width="0.01" layer="51"/>
<wire x1="-3.475" y1="0.85" x2="2.325" y2="0.85" width="0.01" layer="51"/>
<wire x1="-3.125" y1="-1.9" x2="-3.125" y2="-1.1" width="0.01" layer="51"/>
<wire x1="-3.825" y1="-1.9" x2="-3.825" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-3.475" y1="-2.15" x2="2.325" y2="-2.15" width="0.01" layer="51"/>
<wire x1="3.825" y1="-1.9" x2="3.825" y2="-1.1" width="0.01" layer="51"/>
<wire x1="3.125" y1="-1.9" x2="3.125" y2="-1.1" width="0.01" layer="51"/>
<wire x1="2.9166" y1="1.1" x2="2.9166" y2="1.9" width="0.01" layer="51"/>
<wire x1="2.9166" y1="-1.9" x2="2.9166" y2="-1.1" width="0.01" layer="51"/>
<wire x1="-2.7656" y1="1.1" x2="-2.7656" y2="1.9" width="0.01" layer="51"/>
<wire x1="-2.7656" y1="-1.9" x2="-2.7656" y2="-1.1" width="0.01" layer="51"/>
<wire x1="2.6292" y1="1.1" x2="2.6292" y2="1.9" width="0.01" layer="51"/>
<wire x1="2.6292" y1="-1.9" x2="2.6292" y2="-1.1" width="0.01" layer="51"/>
<wire x1="2.525" y1="1.1" x2="2.525" y2="1.9" width="0.01" layer="51"/>
<wire x1="1.325" y1="1.1" x2="1.325" y2="1.9" width="0.01" layer="51"/>
<wire x1="2.525" y1="-1.9" x2="2.525" y2="-1.1" width="0.01" layer="51"/>
<wire x1="1.325" y1="-1.9" x2="1.325" y2="-1.1" width="0.01" layer="51"/>
<wire x1="1.231" y1="1.1" x2="1.231" y2="1.9" width="0.01" layer="51"/>
<wire x1="1.231" y1="-1.9" x2="1.231" y2="-1.1" width="0.01" layer="51"/>
<wire x1="-2.6712" y1="1.9" x2="-2.6712" y2="1.1" width="0.01" layer="51"/>
<wire x1="3.925" y1="1.9" x2="1.0163" y2="1.9" width="0.01" layer="51"/>
<wire x1="-3.475" y1="2.0057" x2="2.325" y2="2.0057" width="0.01" layer="51"/>
<wire x1="3.925" y1="1.1" x2="1.0163" y2="1.1" width="0.01" layer="51"/>
<wire x1="1.0163" y1="1.1" x2="1.0163" y2="1.9" width="0.01" layer="51"/>
<wire x1="-3.475" y1="0.9943" x2="2.325" y2="0.9943" width="0.01" layer="51"/>
<wire x1="2.325" y1="1.0336" x2="3.725" y2="1.0336" width="0.01" layer="51"/>
<wire x1="2.325" y1="1.9664" x2="3.725" y2="1.9664" width="0.01" layer="51"/>
<wire x1="-2.6712" y1="-1.1" x2="-2.6712" y2="-1.9" width="0.01" layer="51"/>
<wire x1="-3.475" y1="-0.9943" x2="2.325" y2="-0.9943" width="0.01" layer="51"/>
<wire x1="3.925" y1="-1.1" x2="1.0163" y2="-1.1" width="0.01" layer="51"/>
<wire x1="3.925" y1="-1.9" x2="1.0163" y2="-1.9" width="0.01" layer="51"/>
<wire x1="1.0163" y1="-1.9" x2="1.0163" y2="-1.1" width="0.01" layer="51"/>
<wire x1="-3.475" y1="-2.0057" x2="2.325" y2="-2.0057" width="0.01" layer="51"/>
<wire x1="2.325" y1="-1.0336" x2="3.725" y2="-1.0336" width="0.01" layer="51"/>
<wire x1="2.325" y1="-1.9664" x2="3.725" y2="-1.9664" width="0.01" layer="51"/>
<wire x1="3.725" y1="1.0336" x2="3.725" y2="1.9664" width="0.01" layer="51"/>
<wire x1="3.725" y1="-1.9664" x2="3.725" y2="-1.0336" width="0.01" layer="51"/>
<wire x1="2.325" y1="1.9664" x2="2.325" y2="2.8538" width="0.01" layer="51"/>
<wire x1="2.325" y1="2.8538" x2="2.325" y2="2.95" width="0.01" layer="21"/>
<wire x1="2.325" y1="-2.95" x2="2.325" y2="-2.8538" width="0.01" layer="21"/>
<wire x1="2.325" y1="-2.8538" x2="2.325" y2="-1.9664" width="0.01" layer="51"/>
<wire x1="2.325" y1="-0.0448" x2="3.925" y2="-0.0448" width="0.01" layer="51"/>
<wire x1="2.325" y1="-1.0336" x2="2.325" y2="0.0448" width="0.01" layer="51"/>
<wire x1="2.325" y1="0.0448" x2="2.325" y2="1.0336" width="0.01" layer="51"/>
<wire x1="2.325" y1="0.0448" x2="3.925" y2="0.0448" width="0.01" layer="51"/>
<wire x1="2.325" y1="-2.8538" x2="3.925" y2="-2.8538" width="0.01" layer="21"/>
<wire x1="2.325" y1="2.8538" x2="3.925" y2="2.8538" width="0.01" layer="21"/>
<wire x1="1.925" y1="2.3" x2="1.925" y2="0.7" width="0.01" layer="51"/>
<wire x1="1.925" y1="0.7" x2="3.7565" y2="0.7" width="0.01" layer="51"/>
<wire x1="3.7565" y1="2.3" x2="1.925" y2="2.3" width="0.01" layer="51"/>
<wire x1="1.925" y1="-0.7" x2="1.925" y2="-2.3" width="0.01" layer="51"/>
<wire x1="1.925" y1="-2.3" x2="3.7565" y2="-2.3" width="0.01" layer="51"/>
<wire x1="3.7565" y1="-0.7" x2="1.925" y2="-0.7" width="0.01" layer="51"/>
<wire x1="-1.31" y1="-1.5" x2="0.16" y2="-1.5" width="0.01" layer="51"/>
<wire x1="-0.575" y1="-2.235" x2="-0.575" y2="-0.765" width="0.01" layer="51"/>
<wire x1="-1.31" y1="1.5" x2="0.16" y2="1.5" width="0.01" layer="51"/>
<wire x1="-0.575" y1="0.765" x2="-0.575" y2="2.235" width="0.01" layer="51"/>
<wire x1="-2.6712" y1="1.1" x2="-3.475" y2="1.1" width="0.01" layer="51"/>
<wire x1="-3.525" y1="1.9" x2="-2.6712" y2="1.9" width="0.01" layer="51"/>
<wire x1="-3.925" y1="1.1" x2="-3.925" y2="1.9" width="0.2" layer="51"/>
<wire x1="-3.475" y1="-1.1" x2="-2.6712" y2="-1.1" width="0.01" layer="51"/>
<wire x1="-2.6712" y1="-1.9" x2="-3.475" y2="-1.9" width="0.01" layer="51"/>
<wire x1="-3.925" y1="-1.9" x2="-3.925" y2="-1.1" width="0.2" layer="51"/>
<wire x1="3.925" y1="-2.95" x2="3.925" y2="2.95" width="0.2" layer="51"/>
<wire x1="3.858" y1="-2.95" x2="3.858" y2="-2.2804" width="0.01" layer="51"/>
<wire x1="3.858" y1="-0.7196" x2="3.858" y2="0.7196" width="0.01" layer="51"/>
<wire x1="3.858" y1="2.2804" x2="3.858" y2="2.95" width="0.01" layer="51"/>
<wire x1="3.208" y1="-2.95" x2="3.208" y2="2.95" width="0.01" layer="51"/>
<wire x1="3.925" y1="2.95" x2="-3.475" y2="2.95" width="0.2" layer="21"/>
<wire x1="-3.475" y1="-2.95" x2="-3.475" y2="-1.9" width="0.2" layer="51"/>
<wire x1="-3.475" y1="-1.9" x2="-3.475" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-3.475" y1="-1.1" x2="-3.475" y2="1.1" width="0.2" layer="51"/>
<wire x1="-3.475" y1="1.1" x2="-3.475" y2="2.95" width="0.2" layer="51"/>
<wire x1="3.925" y1="-2.95" x2="-3.475" y2="-2.95" width="0.2" layer="21"/>
<wire x1="-2.475" y1="-2.95" x2="-2.475" y2="2.95" width="0.01" layer="51"/>
<wire x1="2.775" y1="-2.95" x2="2.775" y2="2.95" width="0.01" layer="51"/>
<wire x1="-3.925" y1="1.9" x2="-3.525" y2="1.9" width="0.2" layer="51"/>
<wire x1="-3.925" y1="1.1" x2="-3.475" y2="1.1" width="0.2" layer="51"/>
<wire x1="-3.925" y1="-1.1" x2="-3.475" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-3.925" y1="-1.9" x2="-3.475" y2="-1.9" width="0.2" layer="51"/>
<circle x="-0.575" y="1.5" radius="0.55" width="0.01" layer="51"/>
<circle x="-0.575" y="-1.5" radius="0.55" width="0.01" layer="51"/>
<circle x="-0.575" y="-1.5" radius="0.7" width="0.01" layer="51"/>
<circle x="-0.575" y="1.5" radius="0.7" width="0.01" layer="51"/>
<smd name="1@2" x="2.775" y="1.5" dx="2.5" dy="1.4" layer="1"/>
<smd name="1@1" x="-2.625" y="1.5" dx="2.5" dy="1.4" layer="1"/>
<smd name="2@2" x="2.765" y="-1.5" dx="2.5" dy="1.4" layer="1"/>
<smd name="2@1" x="-2.625" y="-1.5" dx="2.5" dy="1.4" layer="1"/>
<text x="-3.695" y="4.22" size="2" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.695" y="-5.94" size="2" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="7.735" y="1.68" size="2" layer="51" font="vector" ratio="10" rot="R90">1</text>
</package>
</packages>
<packages3d>
<package3d name="P-2059-302/998-403" urn="urn:adsk.eagle:package:8559218/2" type="model">
<packageinstances>
<packageinstance name="P-2059-302/998-403"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="S-2-POL-S">
<wire x1="-3.81" y1="-1.27" x2="3.81" y2="-1.27" width="0.2" layer="97"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="1.27" width="0.2" layer="97"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="3.81" width="0.2" layer="97"/>
<wire x1="3.81" y1="3.81" x2="-3.81" y2="3.81" width="0.2" layer="97"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="1.27" width="0.2" layer="97"/>
<text x="5.08" y="2.54" size="1.778" layer="95" font="vector" ratio="10" rot="MR180" align="center-left">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96" font="vector" ratio="10" rot="MR180" align="center-left">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pin" length="short" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pin" length="short" function="dot"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.2" layer="97"/>
<wire x1="3.81" y1="1.27" x2="-3.81" y2="1.27" width="0.2" layer="97"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2059-302/998-403" prefix="CN">
<description>&lt;b&gt;Serie 2059,  SMD-Leiterplattenklemme im Gurt 2-polig / Series 2059,  SMD Terminal block in tape-and-reel packing 2-pole&lt;/b&gt;&lt;br&gt;&lt;br&gt;Polzahl / Pole No.: 2 &lt;br&gt;Rastermaß / Pitch: 3  mm&lt;br&gt;Bemessungsspannung / Rated Voltage: 160 V&lt;br&gt;Nennstrom / Nominal Current: 3 A&lt;br&gt;Leiterquerschnitt / Conductor Size: 0.14 - 0.34 mm2&lt;br&gt;Anschlusstechnik / Connection Technology: PUSH WIRE&lt;sup&gt;®&lt;/sup&gt; &lt;br&gt;Leitereinführung (zur Platine) / Conductor entry angle (to PCB): 0 °&lt;br&gt;Farbe / Color: weiß / weiß&lt;br&gt;&lt;br&gt; Stand: 01.01.2014. Die jeweils aktuellen Daten zu diesem Artikel finden Sie in unserem Produktkatalog, den Sie unter &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt; einsehen können. &lt;br&gt; As of: 01/01/2014. Please find the newest data for this article in our product catalogue, which can be viewed under &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt;.&lt;br&gt;</description>
<gates>
<gate name="CN" symbol="S-2-POL-S" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P-2059-302/998-403">
<connects>
<connect gate="CN" pin="1" pad="1@1 1@2"/>
<connect gate="CN" pin="2" pad="2@1 2@2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8559218/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Series 2059, SMD Terminal block in tape-and-reel packing 2-pole"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="2059-302/998-403"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-idc">
<packages>
<package name="3M_20" urn="urn:adsk.eagle:footprint:6230384/1" locally_modified="yes">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<wire x1="22.225" y1="-4.2418" x2="22.225" y2="4.3" width="0.3048" layer="21"/>
<wire x1="-22.225" y1="4.3" x2="-22.225" y2="-4.2418" width="0.3048" layer="21"/>
<wire x1="-22.225" y1="-4.3" x2="-2.54" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-4.3" x2="-2.54" y2="-3.048" width="0.3048" layer="21"/>
<wire x1="2.54" y1="-3.048" x2="2.54" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="2.54" y1="-4.3" x2="21.971" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="22.225" y1="4.3" x2="-22.225" y2="4.3" width="0.3048" layer="21"/>
<wire x1="15.24" y1="-3" x2="2.54" y2="-3" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-3" x2="-15.24" y2="-3" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="-3" x2="-15.24" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="15.113" y1="3" x2="-15.24" y2="3" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="1.27" x2="-15.24" y2="3" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="-1.27" x2="-22.098" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="1.27" x2="-22.098" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-4.318" x2="2.54" y2="-4.318" width="0.3048" layer="21"/>
<wire x1="15.24" y1="-3" x2="15.24" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="15.24" y1="1.27" x2="15.24" y2="3" width="0.3048" layer="21"/>
<wire x1="15.24" y1="-1.27" x2="22.098" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="15.24" y1="1.27" x2="22.098" y2="1.27" width="0.3048" layer="21"/>
<pad name="1" x="-11.43" y="-1.27" drill="1" shape="square"/>
<pad name="2" x="-11.43" y="1.27" drill="1" shape="square"/>
<pad name="3" x="-8.89" y="-1.27" drill="1" shape="square"/>
<pad name="4" x="-8.89" y="1.27" drill="1" shape="square"/>
<pad name="5" x="-6.35" y="-1.27" drill="1" shape="square"/>
<pad name="6" x="-6.35" y="1.27" drill="1" shape="square"/>
<pad name="8" x="-3.81" y="1.27" drill="1" shape="square"/>
<pad name="9" x="-1.27" y="-1.27" drill="1" shape="square"/>
<pad name="10" x="-1.27" y="1.27" drill="1" shape="square"/>
<pad name="11" x="1.27" y="-1.27" drill="1" shape="square"/>
<pad name="12" x="1.27" y="1.27" drill="1" shape="square"/>
<pad name="13" x="3.81" y="-1.27" drill="1" shape="square"/>
<pad name="14" x="3.81" y="1.27" drill="1" shape="square"/>
<pad name="15" x="6.35" y="-1.27" drill="1" shape="square"/>
<pad name="16" x="6.35" y="1.27" drill="1" shape="square"/>
<pad name="17" x="8.89" y="-1.27" drill="1" shape="square"/>
<pad name="18" x="8.89" y="1.27" drill="1" shape="square"/>
<pad name="19" x="11.43" y="-1.27" drill="1" shape="square"/>
<pad name="20" x="11.43" y="1.27" drill="1" shape="square"/>
<pad name="7" x="-3.81" y="-1.27" drill="1" shape="square"/>
<text x="-8.89" y="-7.62" size="2.54" layer="25">&gt;NAME</text>
<text x="8.89" y="-7.62" size="2.54" layer="27">&gt;VALUE</text>
</package>
<package name="3M_20L" urn="urn:adsk.eagle:footprint:6230383/1" locally_modified="yes">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<wire x1="-22.225" y1="-6.0198" x2="-19.685" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="22.225" y1="-6.0198" x2="22.225" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-22.225" y1="2.54" x2="-22.225" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-19.685" y1="-6.0198" x2="-19.685" y2="-2.032" width="0.3048" layer="21"/>
<wire x1="-19.685" y1="-6.0198" x2="-15.0114" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-19.685" y1="-2.032" x2="-17.3482" y2="-0.4572" width="0.3048" layer="21"/>
<wire x1="-17.3482" y1="-0.4572" x2="-15.0114" y2="-2.032" width="0.3048" layer="21"/>
<wire x1="-15.0114" y1="-2.032" x2="-15.0114" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-15.0114" y1="-6.0198" x2="15.0114" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="15.0114" y1="-6.0198" x2="15.0114" y2="-2.0574" width="0.3048" layer="21"/>
<wire x1="15.0114" y1="-2.0574" x2="17.3482" y2="-0.4572" width="0.3048" layer="21"/>
<wire x1="17.3482" y1="-0.4572" x2="19.685" y2="-2.0574" width="0.3048" layer="21"/>
<wire x1="19.685" y1="-2.0574" x2="19.685" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="15.0114" y1="-6.0198" x2="19.685" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="19.685" y1="-6.0198" x2="22.225" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-22.225" y1="2.54" x2="-19.3548" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="22.225" y1="2.54" x2="19.3548" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="10.9982" x2="-19.3548" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="8.89" x2="-15.24" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="6.6802" width="0.3048" layer="21"/>
<wire x1="-3.81" y1="7.874" x2="-2.54" y2="6.604" width="0.3048" layer="21" curve="-90"/>
<wire x1="-15.24" y1="8.89" x2="-14.224" y2="7.874" width="0.3048" layer="21" curve="90"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="6.6802" width="0.3048" layer="21"/>
<wire x1="2.54" y1="6.604" x2="3.81" y2="7.874" width="0.3048" layer="21" curve="-90"/>
<wire x1="-15.24" y1="1.27" x2="-15.24" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="5.08" x2="-15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.27" x2="-12.7" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.27" x2="-7.62" y2="1.27" width="0.1524" layer="21"/>
<wire x1="15.24" y1="10.9982" x2="19.3548" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-14.224" y1="7.874" x2="14.224" y2="7.874" width="0.3048" layer="21"/>
<wire x1="15.24" y1="8.89" x2="15.24" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="14.224" y1="7.874" x2="15.24" y2="8.89" width="0.3048" layer="21" curve="90"/>
<wire x1="0" y1="1.27" x2="2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.27" x2="15.24" y2="3.81" width="0.1524" layer="21"/>
<wire x1="15.24" y1="5.08" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.27" x2="12.7" y2="1.27" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.27" x2="7.62" y2="1.27" width="0.1524" layer="21"/>
<circle x="-18.0848" y="3.81" radius="0.9398" width="0.1524" layer="21"/>
<circle x="18.0848" y="3.81" radius="0.9398" width="0.1524" layer="21"/>
<pad name="1" x="-11.43" y="-5.08" drill="1" shape="square"/>
<pad name="2" x="-11.43" y="-2.54" drill="1" shape="square"/>
<pad name="3" x="-8.89" y="-5.08" drill="1" shape="square"/>
<pad name="4" x="-8.89" y="-2.54" drill="1" shape="square"/>
<pad name="5" x="-6.35" y="-5.08" drill="1" shape="square"/>
<pad name="6" x="-6.35" y="-2.54" drill="1" shape="square"/>
<pad name="8" x="-3.81" y="-2.54" drill="1" shape="square"/>
<pad name="9" x="-1.27" y="-5.08" drill="1" shape="square"/>
<pad name="10" x="-1.27" y="-2.54" drill="1" shape="square"/>
<pad name="11" x="1.27" y="-5.08" drill="1" shape="square"/>
<pad name="12" x="1.27" y="-2.54" drill="1" shape="square"/>
<pad name="13" x="3.81" y="-5.08" drill="1" shape="square"/>
<pad name="14" x="3.81" y="-2.54" drill="1" shape="square"/>
<pad name="15" x="6.35" y="-5.08" drill="1" shape="square"/>
<pad name="16" x="6.35" y="-2.54" drill="1" shape="square"/>
<pad name="17" x="8.89" y="-5.08" drill="1" shape="square"/>
<pad name="18" x="8.89" y="-2.54" drill="1" shape="square"/>
<pad name="19" x="11.43" y="-5.08" drill="1" shape="square"/>
<pad name="20" x="11.43" y="-2.54" drill="1" shape="square"/>
<pad name="7" x="-3.81" y="-5.08" drill="1" shape="square"/>
<text x="-21.59" y="-10.16" size="2.54" layer="25">&gt;NAME</text>
<text x="7.62" y="-10.16" size="2.54" layer="27">&gt;VALUE</text>
<polygon width="0.3048" layer="21">
<vertex x="-12.7" y="5.35"/>
<vertex x="-10.16" y="5.35"/>
<vertex x="-11.43" y="2.81"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="3M_20" urn="urn:adsk.eagle:package:6230386/2" locally_modified="yes" type="model">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="3M_20"/>
</packageinstances>
</package3d>
<package3d name="3M_20L" urn="urn:adsk.eagle:package:6230385/2" locally_modified="yes" type="model">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="3M_20L"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="20P">
<wire x1="3.81" y1="-15.24" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-1.27" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-1.27" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-12.7" x2="-1.27" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-1.27" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="10.16" x2="-1.27" y2="10.16" width="0.6096" layer="94"/>
<text x="-3.81" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="13" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="15" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="17" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="19" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="12" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="14" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="16" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="18" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="20" x="-7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AWP-20" prefix="CN">
<gates>
<gate name="CN" symbol="20P" x="0" y="0"/>
</gates>
<devices>
<device name="P" package="3M_20">
<connects>
<connect gate="CN" pin="1" pad="1"/>
<connect gate="CN" pin="10" pad="10"/>
<connect gate="CN" pin="11" pad="11"/>
<connect gate="CN" pin="12" pad="12"/>
<connect gate="CN" pin="13" pad="13"/>
<connect gate="CN" pin="14" pad="14"/>
<connect gate="CN" pin="15" pad="15"/>
<connect gate="CN" pin="16" pad="16"/>
<connect gate="CN" pin="17" pad="17"/>
<connect gate="CN" pin="18" pad="18"/>
<connect gate="CN" pin="19" pad="19"/>
<connect gate="CN" pin="2" pad="2"/>
<connect gate="CN" pin="20" pad="20"/>
<connect gate="CN" pin="3" pad="3"/>
<connect gate="CN" pin="4" pad="4"/>
<connect gate="CN" pin="5" pad="5"/>
<connect gate="CN" pin="6" pad="6"/>
<connect gate="CN" pin="7" pad="7"/>
<connect gate="CN" pin="8" pad="8"/>
<connect gate="CN" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6230386/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="IDC socket, 2x10, straight, with latch" constant="no"/>
<attribute name="MF" value="CONNFLY" constant="no"/>
<attribute name="MPN" value="DS1011-20SBSIB7" constant="no"/>
</technology>
</technologies>
</device>
<device name="K" package="3M_20L">
<connects>
<connect gate="CN" pin="1" pad="1"/>
<connect gate="CN" pin="10" pad="10"/>
<connect gate="CN" pin="11" pad="11"/>
<connect gate="CN" pin="12" pad="12"/>
<connect gate="CN" pin="13" pad="13"/>
<connect gate="CN" pin="14" pad="14"/>
<connect gate="CN" pin="15" pad="15"/>
<connect gate="CN" pin="16" pad="16"/>
<connect gate="CN" pin="17" pad="17"/>
<connect gate="CN" pin="18" pad="18"/>
<connect gate="CN" pin="19" pad="19"/>
<connect gate="CN" pin="2" pad="2"/>
<connect gate="CN" pin="20" pad="20"/>
<connect gate="CN" pin="3" pad="3"/>
<connect gate="CN" pin="4" pad="4"/>
<connect gate="CN" pin="5" pad="5"/>
<connect gate="CN" pin="6" pad="6"/>
<connect gate="CN" pin="7" pad="7"/>
<connect gate="CN" pin="8" pad="8"/>
<connect gate="CN" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6230385/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="IDC socket, 2x10, right angle, with latch" constant="no"/>
<attribute name="MF" value="CONNFLY" constant="no"/>
<attribute name="MPN" value="DS1011-20RBSIB7" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<modules>
<module name="RELAY" prefix="M" dx="20.32" dy="15.24">
<ports>
<port name="GND" side="right" coord="-2.54" direction="pwr"/>
<port name="OUT" side="right" coord="0" direction="out"/>
<port name="+24V" side="right" coord="2.54" direction="pwr"/>
<port name="IN+" side="left" coord="2.54" direction="in"/>
<port name="IN-" side="left" coord="-2.54" direction="in"/>
</ports>
<variantdefs>
</variantdefs>
<parts>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="REL1" library="udo-panasonic-relay" deviceset="PA1A-PS" device="(VERTICAL)" package3d_urn="urn:adsk.eagle:package:6395349/4" value="PA1A-PS"/>
<part name="D1" library="udo-diode" deviceset="LL4148" device="" package3d_urn="urn:adsk.eagle:package:7567381/2"/>
<part name="D2" library="udo-diode" deviceset="LL4148" device="" package3d_urn="urn:adsk.eagle:package:7567381/2"/>
<part name="X8" library="blebox-dummy" deviceset="BOM" device="" value="APAN3124">
<attribute name="DESCRIPTION" value="Form A relay, 24V, 5A"/>
<attribute name="MF" value="PANASONIC"/>
<attribute name="MPN" value="APAN3124"/>
</part>
</parts>
<sheets>
<sheet>
<plain>
<text x="167.64" y="27.94" size="2.54" layer="94">&gt;MODULE</text>
<text x="259.08" y="12.7" size="2.54" layer="94" align="center-left">A</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME1" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="REL1" gate="K" x="109.22" y="109.22" smashed="yes" rot="R180">
<attribute name="VALUE" x="111.76" y="96.52" size="1.778" layer="96" rot="R180"/>
<attribute name="PART" x="111.76" y="99.06" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="REL1" gate="S" x="127" y="109.22" smashed="yes">
<attribute name="PART" x="124.46" y="106.68" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="D1" gate="D" x="127" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="126.5174" y="91.44" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="129.3114" y="91.44" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D2" gate="D" x="99.06" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="98.5774" y="111.76" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="101.3714" y="111.76" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X8" gate="X" x="109.22" y="129.54" smashed="yes" rot="MR0">
<attribute name="NAME" x="106.68" y="132.08" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="106.68" y="129.54" size="1.778" layer="96" rot="MR0" align="center-left"/>
<attribute name="DESCRIPTION" x="106.68" y="127" size="1.778" layer="97" rot="MR0" align="center-left"/>
<attribute name="MF" x="109.22" y="129.54" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
<attribute name="MPN" x="109.22" y="129.54" size="1.778" layer="96" rot="MR0" align="top-left" display="off"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="OUT" class="0">
<segment>
<pinref part="REL1" gate="S" pin="P"/>
<wire x1="127" y1="104.14" x2="127" y2="101.6" width="0.1524" layer="91"/>
<wire x1="127" y1="101.6" x2="147.32" y2="101.6" width="0.1524" layer="91"/>
<pinref part="D1" gate="D" pin="C"/>
<wire x1="127" y1="91.44" x2="127" y2="101.6" width="0.1524" layer="91"/>
<junction x="127" y="101.6"/>
<label x="147.32" y="101.6" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="D1" gate="D" pin="A"/>
<wire x1="127" y1="86.36" x2="127" y2="83.82" width="0.1524" layer="91"/>
<wire x1="127" y1="83.82" x2="147.32" y2="83.82" width="0.1524" layer="91"/>
<label x="147.32" y="83.82" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<pinref part="REL1" gate="S" pin="S"/>
<wire x1="127" y1="114.3" x2="127" y2="116.84" width="0.1524" layer="91"/>
<wire x1="127" y1="116.84" x2="147.32" y2="116.84" width="0.1524" layer="91"/>
<label x="147.32" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="IN-" class="0">
<segment>
<pinref part="REL1" gate="K" pin="1"/>
<wire x1="109.22" y1="104.14" x2="109.22" y2="101.6" width="0.1524" layer="91"/>
<wire x1="109.22" y1="101.6" x2="99.06" y2="101.6" width="0.1524" layer="91"/>
<pinref part="D2" gate="D" pin="A"/>
<wire x1="99.06" y1="101.6" x2="78.74" y2="101.6" width="0.1524" layer="91"/>
<wire x1="99.06" y1="106.68" x2="99.06" y2="101.6" width="0.1524" layer="91"/>
<junction x="99.06" y="101.6"/>
<label x="78.74" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="IN+" class="0">
<segment>
<pinref part="REL1" gate="K" pin="2"/>
<wire x1="109.22" y1="114.3" x2="109.22" y2="116.84" width="0.1524" layer="91"/>
<wire x1="109.22" y1="116.84" x2="99.06" y2="116.84" width="0.1524" layer="91"/>
<pinref part="D2" gate="D" pin="C"/>
<wire x1="99.06" y1="116.84" x2="78.74" y2="116.84" width="0.1524" layer="91"/>
<wire x1="99.06" y1="111.76" x2="99.06" y2="116.84" width="0.1524" layer="91"/>
<junction x="99.06" y="116.84"/>
<label x="78.74" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</module>
</modules>
<parts>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="LOGO1" library="udo-logo" deviceset="UDO-LOGO-" device="10MM" package3d_urn="urn:adsk.eagle:package:6649258/2"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+24V" device=""/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="DGND" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="CN1" library="udo-wago-terminal-blocks" deviceset="2059-302/998-403" device="" package3d_urn="urn:adsk.eagle:package:8559218/2"/>
<part name="CN2" library="udo-wago-terminal-blocks" deviceset="2059-302/998-403" device="" package3d_urn="urn:adsk.eagle:package:8559218/2"/>
<part name="CN3" library="udo-wago-terminal-blocks" deviceset="2059-302/998-403" device="" package3d_urn="urn:adsk.eagle:package:8559218/2"/>
<part name="CN4" library="udo-wago-terminal-blocks" deviceset="2059-302/998-403" device="" package3d_urn="urn:adsk.eagle:package:8559218/2"/>
<part name="CN5" library="udo-wago-terminal-blocks" deviceset="2059-302/998-403" device="" package3d_urn="urn:adsk.eagle:package:8559218/2"/>
<part name="CN6" library="udo-wago-terminal-blocks" deviceset="2059-302/998-403" device="" package3d_urn="urn:adsk.eagle:package:8559218/2"/>
<part name="CN7" library="udo-wago-terminal-blocks" deviceset="2059-302/998-403" device="" package3d_urn="urn:adsk.eagle:package:8559218/2"/>
<part name="CN8" library="udo-wago-terminal-blocks" deviceset="2059-302/998-403" device="" package3d_urn="urn:adsk.eagle:package:8559218/2"/>
<part name="CN9" library="udo-wago-terminal-blocks" deviceset="2059-302/998-403" device="" package3d_urn="urn:adsk.eagle:package:8559218/2"/>
<part name="CN10" library="udo-wago-terminal-blocks" deviceset="2059-302/998-403" device="" package3d_urn="urn:adsk.eagle:package:8559218/2"/>
<part name="CN17" library="udo-idc" deviceset="AWP-20" device="P" package3d_urn="urn:adsk.eagle:package:6230386/2"/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="DGND" device=""/>
<part name="SUPPLY3" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="DGND" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+24V" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+24V" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="259.08" y="12.7" size="2.54" layer="94" align="center-left">A</text>
<text x="167.64" y="30.48" size="2.54" layer="94" align="top-left">OUTPUTS</text>
</plain>
<moduleinsts>
<moduleinst name="M1" module="RELAY" x="17.78" y="91.44" rot="R90">
<attribute name="NAME" value="M1" x="17.78" y="91.44" size="2.032" layer="95" rot="R90" align="bottom-center"/>
</moduleinst>
<moduleinst name="M2" module="RELAY" x="38.1" y="91.44" rot="R90">
<attribute name="NAME" value="M2" x="38.1" y="91.44" size="2.032" layer="95" rot="R90" align="bottom-center"/>
</moduleinst>
<moduleinst name="M3" module="RELAY" x="58.42" y="91.44" rot="R90">
<attribute name="NAME" value="M3" x="58.42" y="91.44" size="2.032" layer="95" rot="R90" align="bottom-center"/>
</moduleinst>
<moduleinst name="M4" module="RELAY" x="78.74" y="91.44" rot="R90">
<attribute name="NAME" value="M4" x="78.74" y="91.44" size="2.032" layer="95" rot="R90" align="bottom-center"/>
</moduleinst>
<moduleinst name="M5" module="RELAY" x="99.06" y="91.44" rot="R90">
<attribute name="NAME" value="M5" x="99.06" y="91.44" size="2.032" layer="95" rot="R90" align="bottom-center"/>
</moduleinst>
<moduleinst name="M6" module="RELAY" x="119.38" y="91.44" rot="R90">
<attribute name="NAME" value="M6" x="119.38" y="91.44" size="2.032" layer="95" rot="R90" align="bottom-center"/>
</moduleinst>
<moduleinst name="M7" module="RELAY" x="139.7" y="91.44" rot="R90">
<attribute name="NAME" value="M7" x="139.7" y="91.44" size="2.032" layer="95" rot="R90" align="bottom-center"/>
</moduleinst>
<moduleinst name="M8" module="RELAY" x="160.02" y="91.44" rot="R90">
<attribute name="NAME" value="M8" x="160.02" y="91.44" size="2.032" layer="95" rot="R90" align="bottom-center"/>
</moduleinst>
<moduleinst name="M9" module="RELAY" x="180.34" y="91.44" rot="R90">
<attribute name="NAME" value="M9" x="180.34" y="91.44" size="2.032" layer="95" rot="R90" align="bottom-center"/>
</moduleinst>
<moduleinst name="M10" module="RELAY" x="200.66" y="91.44" rot="R90">
<attribute name="NAME" value="M10" x="200.66" y="91.44" size="2.032" layer="95" rot="R90" align="bottom-center"/>
</moduleinst>
</moduleinsts>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME1" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="LOGO1" gate="LOGO" x="243.332" y="23.114" smashed="yes"/>
<instance part="P+1" gate="1" x="10.16" y="116.84" smashed="yes">
<attribute name="VALUE" x="7.62" y="114.3" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="215.9" y="66.04" smashed="yes">
<attribute name="VALUE" x="213.233" y="62.865" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="215.9" y="101.6" smashed="yes">
<attribute name="VALUE" x="213.36" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="CN1" gate="CN" x="20.32" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="17.78" y="149.86" size="2" layer="95" font="vector" ratio="10" rot="MR90" align="center-left"/>
<attribute name="VALUE" x="20.32" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR90" align="center-left"/>
</instance>
<instance part="CN2" gate="CN" x="40.64" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="38.1" y="149.86" size="2" layer="95" font="vector" ratio="10" rot="MR90" align="center-left"/>
<attribute name="VALUE" x="40.64" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR90" align="center-left"/>
</instance>
<instance part="CN3" gate="CN" x="60.96" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="58.42" y="149.86" size="2" layer="95" font="vector" ratio="10" rot="MR90" align="center-left"/>
<attribute name="VALUE" x="60.96" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR90" align="center-left"/>
</instance>
<instance part="CN4" gate="CN" x="81.28" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="78.74" y="149.86" size="2" layer="95" font="vector" ratio="10" rot="MR90" align="center-left"/>
<attribute name="VALUE" x="81.28" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR90" align="center-left"/>
</instance>
<instance part="CN5" gate="CN" x="101.6" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="99.06" y="149.86" size="2" layer="95" font="vector" ratio="10" rot="MR90" align="center-left"/>
<attribute name="VALUE" x="101.6" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR90" align="center-left"/>
</instance>
<instance part="CN6" gate="CN" x="121.92" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="119.38" y="149.86" size="2" layer="95" font="vector" ratio="10" rot="MR90" align="center-left"/>
<attribute name="VALUE" x="121.92" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR90" align="center-left"/>
</instance>
<instance part="CN7" gate="CN" x="142.24" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="139.7" y="149.86" size="2" layer="95" font="vector" ratio="10" rot="MR90" align="center-left"/>
<attribute name="VALUE" x="142.24" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR90" align="center-left"/>
</instance>
<instance part="CN8" gate="CN" x="162.56" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="160.02" y="149.86" size="2" layer="95" font="vector" ratio="10" rot="MR90" align="center-left"/>
<attribute name="VALUE" x="162.56" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR90" align="center-left"/>
</instance>
<instance part="CN9" gate="CN" x="182.88" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="180.34" y="149.86" size="2" layer="95" font="vector" ratio="10" rot="MR90" align="center-left"/>
<attribute name="VALUE" x="182.88" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR90" align="center-left"/>
</instance>
<instance part="CN10" gate="CN" x="203.2" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="200.66" y="149.86" size="2" layer="95" font="vector" ratio="10" rot="MR90" align="center-left"/>
<attribute name="VALUE" x="203.2" y="149.86" size="2" layer="96" font="vector" ratio="10" rot="MR90" align="center-left"/>
</instance>
<instance part="CN17" gate="CN" x="76.2" y="22.86" smashed="yes">
<attribute name="VALUE" x="72.39" y="2.54" size="1.778" layer="96"/>
<attribute name="NAME" x="72.39" y="5.842" size="1.778" layer="95"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="66.04" y="5.08" smashed="yes">
<attribute name="VALUE" x="63.373" y="1.905" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="86.36" y="5.08" smashed="yes">
<attribute name="VALUE" x="83.693" y="1.905" size="1.778" layer="96"/>
</instance>
<instance part="P+2" gate="1" x="63.5" y="40.64" smashed="yes">
<attribute name="VALUE" x="60.96" y="35.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+3" gate="1" x="88.9" y="40.64" smashed="yes" rot="MR0">
<attribute name="VALUE" x="91.44" y="35.56" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="GND2" gate="1" x="40.64" y="30.48" smashed="yes" rot="R270">
<attribute name="VALUE" x="38.1" y="33.02" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND3" gate="1" x="109.22" y="30.48" smashed="yes" rot="R90">
<attribute name="VALUE" x="111.76" y="27.94" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="VALVE_MOVE_TOWER_RETRACT" class="0">
<segment>
<portref moduleinst="M3" port="OUT"/>
<wire x1="58.42" y1="106.68" x2="58.42" y2="142.24" width="0.1524" layer="91"/>
<label x="58.42" y="114.3" size="1.27" layer="95" rot="R90"/>
<pinref part="CN3" gate="CN" pin="1"/>
</segment>
</net>
<net name="VALVE_MOVE_TOWER_EXTEND" class="0">
<segment>
<portref moduleinst="M4" port="OUT"/>
<wire x1="78.74" y1="142.24" x2="78.74" y2="106.68" width="0.1524" layer="91"/>
<label x="78.74" y="114.3" size="1.27" layer="95" rot="R90"/>
<pinref part="CN4" gate="CN" pin="1"/>
</segment>
</net>
<net name="VALVE_SUCKER_PUSH_RETRACT" class="0">
<segment>
<portref moduleinst="M5" port="OUT"/>
<wire x1="99.06" y1="142.24" x2="99.06" y2="106.68" width="0.1524" layer="91"/>
<label x="99.06" y="114.3" size="1.27" layer="95" rot="R90"/>
<pinref part="CN5" gate="CN" pin="1"/>
</segment>
</net>
<net name="VALVE_UPPER_PUSH_RETRACT" class="0">
<segment>
<portref moduleinst="M7" port="OUT"/>
<wire x1="139.7" y1="142.24" x2="139.7" y2="106.68" width="0.1524" layer="91"/>
<label x="139.7" y="114.3" size="1.27" layer="95" rot="R90"/>
<pinref part="CN7" gate="CN" pin="1"/>
</segment>
</net>
<net name="VALVE_GRIPPERS_OPENER" class="0">
<segment>
<portref moduleinst="M1" port="OUT"/>
<wire x1="17.78" y1="106.68" x2="17.78" y2="142.24" width="0.1524" layer="91"/>
<label x="17.78" y="114.3" size="1.27" layer="95" rot="R90"/>
<pinref part="CN1" gate="CN" pin="1"/>
</segment>
</net>
<net name="VALVE_GRIPPERS_ACTUATOR" class="0">
<segment>
<portref moduleinst="M2" port="OUT"/>
<wire x1="38.1" y1="106.68" x2="38.1" y2="142.24" width="0.1524" layer="91"/>
<label x="38.1" y="114.3" size="1.27" layer="95" rot="R90"/>
<pinref part="CN2" gate="CN" pin="1"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<pinref part="P+1" gate="1" pin="+24V"/>
<wire x1="10.16" y1="114.3" x2="10.16" y2="111.76" width="0.1524" layer="91"/>
<portref moduleinst="M1" port="+24V"/>
<wire x1="15.24" y1="106.68" x2="15.24" y2="111.76" width="0.1524" layer="91"/>
<portref moduleinst="M10" port="+24V"/>
<wire x1="15.24" y1="111.76" x2="35.56" y2="111.76" width="0.1524" layer="91"/>
<wire x1="35.56" y1="111.76" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
<wire x1="55.88" y1="111.76" x2="76.2" y2="111.76" width="0.1524" layer="91"/>
<wire x1="76.2" y1="111.76" x2="96.52" y2="111.76" width="0.1524" layer="91"/>
<wire x1="96.52" y1="111.76" x2="116.84" y2="111.76" width="0.1524" layer="91"/>
<wire x1="116.84" y1="111.76" x2="137.16" y2="111.76" width="0.1524" layer="91"/>
<wire x1="137.16" y1="111.76" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
<wire x1="157.48" y1="111.76" x2="177.8" y2="111.76" width="0.1524" layer="91"/>
<wire x1="177.8" y1="111.76" x2="198.12" y2="111.76" width="0.1524" layer="91"/>
<wire x1="198.12" y1="111.76" x2="198.12" y2="106.68" width="0.1524" layer="91"/>
<portref moduleinst="M9" port="+24V"/>
<wire x1="177.8" y1="106.68" x2="177.8" y2="111.76" width="0.1524" layer="91"/>
<junction x="177.8" y="111.76"/>
<portref moduleinst="M8" port="+24V"/>
<wire x1="157.48" y1="106.68" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
<junction x="157.48" y="111.76"/>
<portref moduleinst="M7" port="+24V"/>
<wire x1="137.16" y1="106.68" x2="137.16" y2="111.76" width="0.1524" layer="91"/>
<junction x="137.16" y="111.76"/>
<portref moduleinst="M6" port="+24V"/>
<wire x1="116.84" y1="106.68" x2="116.84" y2="111.76" width="0.1524" layer="91"/>
<junction x="116.84" y="111.76"/>
<portref moduleinst="M5" port="+24V"/>
<wire x1="96.52" y1="106.68" x2="96.52" y2="111.76" width="0.1524" layer="91"/>
<junction x="96.52" y="111.76"/>
<portref moduleinst="M4" port="+24V"/>
<wire x1="76.2" y1="106.68" x2="76.2" y2="111.76" width="0.1524" layer="91"/>
<junction x="76.2" y="111.76"/>
<portref moduleinst="M3" port="+24V"/>
<wire x1="55.88" y1="106.68" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
<junction x="55.88" y="111.76"/>
<portref moduleinst="M2" port="+24V"/>
<wire x1="35.56" y1="106.68" x2="35.56" y2="111.76" width="0.1524" layer="91"/>
<junction x="35.56" y="111.76"/>
<wire x1="10.16" y1="111.76" x2="15.24" y2="111.76" width="0.1524" layer="91"/>
<junction x="15.24" y="111.76"/>
</segment>
<segment>
<pinref part="CN17" gate="CN" pin="20"/>
<wire x1="68.58" y1="33.02" x2="63.5" y2="33.02" width="0.1524" layer="91"/>
<pinref part="P+2" gate="1" pin="+24V"/>
<wire x1="63.5" y1="33.02" x2="63.5" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CN17" gate="CN" pin="19"/>
<wire x1="83.82" y1="33.02" x2="88.9" y2="33.02" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+24V"/>
<wire x1="88.9" y1="33.02" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DGND" class="0">
<segment>
<portref moduleinst="M1" port="IN-"/>
<wire x1="20.32" y1="76.2" x2="20.32" y2="73.66" width="0.1524" layer="91"/>
<wire x1="20.32" y1="73.66" x2="40.64" y2="73.66" width="0.1524" layer="91"/>
<wire x1="40.64" y1="73.66" x2="60.96" y2="73.66" width="0.1524" layer="91"/>
<wire x1="60.96" y1="73.66" x2="81.28" y2="73.66" width="0.1524" layer="91"/>
<wire x1="81.28" y1="73.66" x2="101.6" y2="73.66" width="0.1524" layer="91"/>
<wire x1="101.6" y1="73.66" x2="121.92" y2="73.66" width="0.1524" layer="91"/>
<wire x1="121.92" y1="73.66" x2="142.24" y2="73.66" width="0.1524" layer="91"/>
<wire x1="142.24" y1="73.66" x2="162.56" y2="73.66" width="0.1524" layer="91"/>
<wire x1="162.56" y1="73.66" x2="182.88" y2="73.66" width="0.1524" layer="91"/>
<wire x1="182.88" y1="73.66" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<wire x1="203.2" y1="73.66" x2="215.9" y2="73.66" width="0.1524" layer="91"/>
<wire x1="215.9" y1="73.66" x2="215.9" y2="68.58" width="0.1524" layer="91"/>
<portref moduleinst="M10" port="IN-"/>
<wire x1="203.2" y1="76.2" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<junction x="203.2" y="73.66"/>
<portref moduleinst="M9" port="IN-"/>
<wire x1="182.88" y1="76.2" x2="182.88" y2="73.66" width="0.1524" layer="91"/>
<junction x="182.88" y="73.66"/>
<portref moduleinst="M8" port="IN-"/>
<wire x1="162.56" y1="76.2" x2="162.56" y2="73.66" width="0.1524" layer="91"/>
<junction x="162.56" y="73.66"/>
<portref moduleinst="M7" port="IN-"/>
<wire x1="142.24" y1="76.2" x2="142.24" y2="73.66" width="0.1524" layer="91"/>
<junction x="142.24" y="73.66"/>
<portref moduleinst="M6" port="IN-"/>
<wire x1="121.92" y1="76.2" x2="121.92" y2="73.66" width="0.1524" layer="91"/>
<junction x="121.92" y="73.66"/>
<portref moduleinst="M5" port="IN-"/>
<wire x1="101.6" y1="76.2" x2="101.6" y2="73.66" width="0.1524" layer="91"/>
<junction x="101.6" y="73.66"/>
<portref moduleinst="M4" port="IN-"/>
<wire x1="81.28" y1="76.2" x2="81.28" y2="73.66" width="0.1524" layer="91"/>
<junction x="81.28" y="73.66"/>
<portref moduleinst="M3" port="IN-"/>
<wire x1="60.96" y1="76.2" x2="60.96" y2="73.66" width="0.1524" layer="91"/>
<junction x="60.96" y="73.66"/>
<portref moduleinst="M2" port="IN-"/>
<wire x1="40.64" y1="76.2" x2="40.64" y2="73.66" width="0.1524" layer="91"/>
<junction x="40.64" y="73.66"/>
<pinref part="SUPPLY1" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="83.82" y1="25.4" x2="88.9" y2="25.4" width="0.1524" layer="91"/>
<label x="88.9" y="25.4" size="1.27" layer="95" xref="yes"/>
<pinref part="CN17" gate="CN" pin="13"/>
</segment>
<segment>
<wire x1="68.58" y1="25.4" x2="63.5" y2="25.4" width="0.1524" layer="91"/>
<label x="63.5" y="25.4" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CN17" gate="CN" pin="14"/>
</segment>
<segment>
<pinref part="SUPPLY2" gate="G$1" pin="DGND"/>
<wire x1="66.04" y1="7.62" x2="66.04" y2="10.16" width="0.1524" layer="91"/>
<pinref part="CN17" gate="CN" pin="2"/>
<wire x1="66.04" y1="10.16" x2="68.58" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CN17" gate="CN" pin="1"/>
<wire x1="83.82" y1="10.16" x2="86.36" y2="10.16" width="0.1524" layer="91"/>
<pinref part="SUPPLY3" gate="G$1" pin="DGND"/>
<wire x1="86.36" y1="10.16" x2="86.36" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SIG_OUT_VALVE_GRIPPERS_ACTUATOR" class="0">
<segment>
<portref moduleinst="M2" port="IN+"/>
<wire x1="35.56" y1="76.2" x2="35.56" y2="71.12" width="0.1524" layer="91"/>
<label x="35.56" y="71.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="83.82" y1="22.86" x2="88.9" y2="22.86" width="0.1524" layer="91"/>
<label x="88.9" y="22.86" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN17" gate="CN" pin="11"/>
</segment>
</net>
<net name="SIG_OUT_VALVE_UPPER_PUSH_RETRACT" class="0">
<segment>
<portref moduleinst="M7" port="IN+"/>
<wire x1="137.16" y1="76.2" x2="137.16" y2="71.12" width="0.1524" layer="91"/>
<label x="137.16" y="71.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="68.58" y1="15.24" x2="63.5" y2="15.24" width="0.1524" layer="91"/>
<label x="63.5" y="15.24" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN17" gate="CN" pin="6"/>
</segment>
</net>
<net name="SIG_OUT_VALVE_SUCKER_PUSH_RETRACT" class="0">
<segment>
<portref moduleinst="M5" port="IN+"/>
<wire x1="96.52" y1="76.2" x2="96.52" y2="71.12" width="0.1524" layer="91"/>
<label x="96.52" y="71.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="68.58" y1="17.78" x2="63.5" y2="17.78" width="0.1524" layer="91"/>
<label x="63.5" y="17.78" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN17" gate="CN" pin="8"/>
</segment>
</net>
<net name="SIG_OUT_VALVE_MOVE_TOWER_EXTEND" class="0">
<segment>
<portref moduleinst="M4" port="IN+"/>
<wire x1="76.2" y1="76.2" x2="76.2" y2="71.12" width="0.1524" layer="91"/>
<label x="76.2" y="71.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="83.82" y1="20.32" x2="88.9" y2="20.32" width="0.1524" layer="91"/>
<label x="88.9" y="20.32" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN17" gate="CN" pin="9"/>
</segment>
</net>
<net name="SIG_OUT_VALVE_MOVE_TOWER_RETRACT" class="0">
<segment>
<portref moduleinst="M3" port="IN+"/>
<wire x1="55.88" y1="76.2" x2="55.88" y2="71.12" width="0.1524" layer="91"/>
<label x="55.88" y="71.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="68.58" y1="20.32" x2="63.5" y2="20.32" width="0.1524" layer="91"/>
<label x="63.5" y="20.32" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN17" gate="CN" pin="10"/>
</segment>
</net>
<net name="SIG_OUT_VALVE_GRIPPERS_OPENER" class="0">
<segment>
<portref moduleinst="M1" port="IN+"/>
<wire x1="15.24" y1="76.2" x2="15.24" y2="71.12" width="0.1524" layer="91"/>
<label x="15.24" y="71.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="68.58" y1="22.86" x2="63.5" y2="22.86" width="0.1524" layer="91"/>
<label x="63.5" y="22.86" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN17" gate="CN" pin="12"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="215.9" y1="109.22" x2="215.9" y2="104.14" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<portref moduleinst="M10" port="GND"/>
<wire x1="203.2" y1="142.24" x2="203.2" y2="109.22" width="0.1524" layer="91"/>
<wire x1="203.2" y1="109.22" x2="203.2" y2="106.68" width="0.1524" layer="91"/>
<wire x1="215.9" y1="109.22" x2="203.2" y2="109.22" width="0.1524" layer="91"/>
<junction x="203.2" y="109.22"/>
<portref moduleinst="M9" port="GND"/>
<wire x1="182.88" y1="142.24" x2="182.88" y2="109.22" width="0.1524" layer="91"/>
<wire x1="182.88" y1="109.22" x2="182.88" y2="106.68" width="0.1524" layer="91"/>
<wire x1="203.2" y1="109.22" x2="182.88" y2="109.22" width="0.1524" layer="91"/>
<junction x="182.88" y="109.22"/>
<portref moduleinst="M8" port="GND"/>
<wire x1="162.56" y1="142.24" x2="162.56" y2="109.22" width="0.1524" layer="91"/>
<wire x1="162.56" y1="109.22" x2="162.56" y2="106.68" width="0.1524" layer="91"/>
<wire x1="182.88" y1="109.22" x2="162.56" y2="109.22" width="0.1524" layer="91"/>
<junction x="162.56" y="109.22"/>
<portref moduleinst="M7" port="GND"/>
<wire x1="142.24" y1="142.24" x2="142.24" y2="109.22" width="0.1524" layer="91"/>
<wire x1="142.24" y1="109.22" x2="142.24" y2="106.68" width="0.1524" layer="91"/>
<wire x1="162.56" y1="109.22" x2="142.24" y2="109.22" width="0.1524" layer="91"/>
<junction x="142.24" y="109.22"/>
<portref moduleinst="M6" port="GND"/>
<wire x1="121.92" y1="142.24" x2="121.92" y2="109.22" width="0.1524" layer="91"/>
<wire x1="121.92" y1="109.22" x2="121.92" y2="106.68" width="0.1524" layer="91"/>
<wire x1="142.24" y1="109.22" x2="121.92" y2="109.22" width="0.1524" layer="91"/>
<junction x="121.92" y="109.22"/>
<portref moduleinst="M5" port="GND"/>
<wire x1="101.6" y1="142.24" x2="101.6" y2="109.22" width="0.1524" layer="91"/>
<wire x1="101.6" y1="109.22" x2="101.6" y2="106.68" width="0.1524" layer="91"/>
<wire x1="121.92" y1="109.22" x2="101.6" y2="109.22" width="0.1524" layer="91"/>
<junction x="101.6" y="109.22"/>
<portref moduleinst="M4" port="GND"/>
<wire x1="81.28" y1="142.24" x2="81.28" y2="109.22" width="0.1524" layer="91"/>
<wire x1="81.28" y1="109.22" x2="81.28" y2="106.68" width="0.1524" layer="91"/>
<wire x1="101.6" y1="109.22" x2="81.28" y2="109.22" width="0.1524" layer="91"/>
<junction x="81.28" y="109.22"/>
<portref moduleinst="M3" port="GND"/>
<wire x1="60.96" y1="142.24" x2="60.96" y2="109.22" width="0.1524" layer="91"/>
<wire x1="60.96" y1="109.22" x2="60.96" y2="106.68" width="0.1524" layer="91"/>
<wire x1="81.28" y1="109.22" x2="60.96" y2="109.22" width="0.1524" layer="91"/>
<junction x="60.96" y="109.22"/>
<portref moduleinst="M1" port="GND"/>
<wire x1="20.32" y1="106.68" x2="20.32" y2="109.22" width="0.1524" layer="91"/>
<portref moduleinst="M2" port="GND"/>
<wire x1="20.32" y1="109.22" x2="20.32" y2="142.24" width="0.1524" layer="91"/>
<wire x1="40.64" y1="106.68" x2="40.64" y2="109.22" width="0.1524" layer="91"/>
<wire x1="40.64" y1="109.22" x2="40.64" y2="142.24" width="0.1524" layer="91"/>
<wire x1="20.32" y1="109.22" x2="40.64" y2="109.22" width="0.1524" layer="91"/>
<junction x="20.32" y="109.22"/>
<junction x="40.64" y="109.22"/>
<wire x1="60.96" y1="109.22" x2="40.64" y2="109.22" width="0.1524" layer="91"/>
<pinref part="CN1" gate="CN" pin="2"/>
<pinref part="CN2" gate="CN" pin="2"/>
<pinref part="CN3" gate="CN" pin="2"/>
<pinref part="CN4" gate="CN" pin="2"/>
<pinref part="CN5" gate="CN" pin="2"/>
<pinref part="CN6" gate="CN" pin="2"/>
<pinref part="CN7" gate="CN" pin="2"/>
<pinref part="CN8" gate="CN" pin="2"/>
<pinref part="CN9" gate="CN" pin="2"/>
<pinref part="CN10" gate="CN" pin="2"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="CN17" gate="CN" pin="17"/>
<wire x1="106.68" y1="30.48" x2="83.82" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CN17" gate="CN" pin="18"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="68.58" y1="30.48" x2="43.18" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VALVE_SUCKER_PUSH_EXTEND" class="0">
<segment>
<portref moduleinst="M6" port="OUT"/>
<wire x1="119.38" y1="142.24" x2="119.38" y2="106.68" width="0.1524" layer="91"/>
<label x="119.38" y="114.3" size="1.27" layer="95" rot="R90"/>
<pinref part="CN6" gate="CN" pin="1"/>
</segment>
</net>
<net name="VALVE_UPPER_PUSH_EXTEND" class="0">
<segment>
<portref moduleinst="M8" port="OUT"/>
<wire x1="160.02" y1="142.24" x2="160.02" y2="106.68" width="0.1524" layer="91"/>
<label x="160.02" y="114.3" size="1.27" layer="95" rot="R90"/>
<pinref part="CN8" gate="CN" pin="1"/>
</segment>
</net>
<net name="VALVE_BOLT_RETRACT" class="0">
<segment>
<portref moduleinst="M9" port="OUT"/>
<wire x1="180.34" y1="142.24" x2="180.34" y2="106.68" width="0.1524" layer="91"/>
<label x="180.34" y="114.3" size="1.27" layer="95" rot="R90"/>
<pinref part="CN9" gate="CN" pin="1"/>
</segment>
</net>
<net name="VALVE_BOLT_EXTEND" class="0">
<segment>
<portref moduleinst="M10" port="OUT"/>
<wire x1="200.66" y1="142.24" x2="200.66" y2="106.68" width="0.1524" layer="91"/>
<label x="200.66" y="114.3" size="1.27" layer="95" rot="R90"/>
<pinref part="CN10" gate="CN" pin="1"/>
</segment>
</net>
<net name="SIG_OUT_VALVE_SUCKER_PUSH_EXTEND" class="0">
<segment>
<portref moduleinst="M6" port="IN+"/>
<wire x1="116.84" y1="76.2" x2="116.84" y2="71.12" width="0.1524" layer="91"/>
<label x="116.84" y="71.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="83.82" y1="17.78" x2="88.9" y2="17.78" width="0.1524" layer="91"/>
<label x="88.9" y="17.78" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN17" gate="CN" pin="7"/>
</segment>
</net>
<net name="SIG_OUT_VALVE_UPPER_PUSH_EXTEND" class="0">
<segment>
<portref moduleinst="M8" port="IN+"/>
<wire x1="157.48" y1="76.2" x2="157.48" y2="71.12" width="0.1524" layer="91"/>
<label x="157.48" y="71.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="83.82" y1="15.24" x2="88.9" y2="15.24" width="0.1524" layer="91"/>
<label x="88.9" y="15.24" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN17" gate="CN" pin="5"/>
</segment>
</net>
<net name="SIG_OUT_VALVE_BOLT_RETRACT" class="0">
<segment>
<portref moduleinst="M9" port="IN+"/>
<wire x1="177.8" y1="76.2" x2="177.8" y2="71.12" width="0.1524" layer="91"/>
<label x="177.8" y="71.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="68.58" y1="12.7" x2="63.5" y2="12.7" width="0.1524" layer="91"/>
<label x="63.5" y="12.7" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="CN17" gate="CN" pin="4"/>
</segment>
</net>
<net name="SIG_OUT_VALVE_BOLT_EXTEND" class="0">
<segment>
<portref moduleinst="M10" port="IN+"/>
<wire x1="198.12" y1="76.2" x2="198.12" y2="71.12" width="0.1524" layer="91"/>
<label x="198.12" y="71.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="83.82" y1="12.7" x2="88.9" y2="12.7" width="0.1524" layer="91"/>
<label x="88.9" y="12.7" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="CN17" gate="CN" pin="3"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
