<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="yes" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="yes" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="yes" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="yes" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="yes" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="yes" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="yes" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="yes" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="yes" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="yes" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="yes" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="yes" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="yes" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="yes" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="yes" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="yes" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="yes" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Warning" color="59" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DGND" urn="urn:adsk.eagle:symbol:27019/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-2.667" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="DGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DGND" urn="urn:adsk.eagle:component:27076/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+24V" urn="urn:adsk.eagle:symbol:26935/1" library_version="1">
<wire x1="1.27" y1="-0.635" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+24V" urn="urn:adsk.eagle:component:26964/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+24V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-wago-terminal-blocks" urn="urn:adsk.eagle:library:9251134">
<packages>
<package name="P-2059-303/998-403" urn="urn:adsk.eagle:footprint:9251142/1" library_version="1">
<wire x1="-3.475" y1="3.65" x2="2.325" y2="3.65" width="0.01" layer="51"/>
<wire x1="-3.125" y1="2.6" x2="-3.125" y2="3.4" width="0.01" layer="51"/>
<wire x1="-3.825" y1="2.6" x2="-3.825" y2="3.4" width="0.2" layer="51"/>
<wire x1="3.825" y1="2.6" x2="3.825" y2="3.4" width="0.01" layer="51"/>
<wire x1="3.125" y1="2.6" x2="3.125" y2="3.4" width="0.01" layer="51"/>
<wire x1="-3.475" y1="0.65" x2="2.325" y2="0.65" width="0.01" layer="51"/>
<wire x1="-3.475" y1="2.35" x2="2.325" y2="2.35" width="0.01" layer="51"/>
<wire x1="-3.125" y1="-0.4" x2="-3.125" y2="0.4" width="0.01" layer="51"/>
<wire x1="-3.825" y1="-0.4" x2="-3.825" y2="0.4" width="0.2" layer="51"/>
<wire x1="3.825" y1="-0.4" x2="3.825" y2="0.4" width="0.01" layer="51"/>
<wire x1="3.125" y1="-0.4" x2="3.125" y2="0.4" width="0.01" layer="51"/>
<wire x1="-3.475" y1="-2.35" x2="2.325" y2="-2.35" width="0.01" layer="51"/>
<wire x1="-3.475" y1="-0.65" x2="2.325" y2="-0.65" width="0.01" layer="51"/>
<wire x1="-3.125" y1="-3.4" x2="-3.125" y2="-2.6" width="0.01" layer="51"/>
<wire x1="-3.825" y1="-3.4" x2="-3.825" y2="-2.6" width="0.2" layer="51"/>
<wire x1="-3.475" y1="-3.65" x2="2.325" y2="-3.65" width="0.01" layer="51"/>
<wire x1="3.825" y1="-3.4" x2="3.825" y2="-2.6" width="0.01" layer="51"/>
<wire x1="3.125" y1="-3.4" x2="3.125" y2="-2.6" width="0.01" layer="51"/>
<wire x1="2.9166" y1="2.6" x2="2.9166" y2="3.4" width="0.01" layer="51"/>
<wire x1="2.9166" y1="-0.4" x2="2.9166" y2="0.4" width="0.01" layer="51"/>
<wire x1="2.9166" y1="-3.4" x2="2.9166" y2="-2.6" width="0.01" layer="51"/>
<wire x1="-2.7656" y1="2.6" x2="-2.7656" y2="3.4" width="0.01" layer="51"/>
<wire x1="-2.7656" y1="-0.4" x2="-2.7656" y2="0.4" width="0.01" layer="51"/>
<wire x1="-2.7656" y1="-3.4" x2="-2.7656" y2="-2.6" width="0.01" layer="51"/>
<wire x1="2.6292" y1="2.6" x2="2.6292" y2="3.4" width="0.01" layer="51"/>
<wire x1="2.6292" y1="-0.4" x2="2.6292" y2="0.4" width="0.01" layer="51"/>
<wire x1="2.6292" y1="-3.4" x2="2.6292" y2="-2.6" width="0.01" layer="51"/>
<wire x1="1.325" y1="2.6" x2="1.325" y2="3.4" width="0.01" layer="51"/>
<wire x1="2.525" y1="2.6" x2="2.525" y2="3.4" width="0.01" layer="51"/>
<wire x1="1.325" y1="-0.4" x2="1.325" y2="0.4" width="0.01" layer="51"/>
<wire x1="2.525" y1="-0.4" x2="2.525" y2="0.4" width="0.01" layer="51"/>
<wire x1="2.525" y1="-3.4" x2="2.525" y2="-2.6" width="0.01" layer="51"/>
<wire x1="1.325" y1="-3.4" x2="1.325" y2="-2.6" width="0.01" layer="51"/>
<wire x1="1.231" y1="2.6" x2="1.231" y2="3.4" width="0.01" layer="51"/>
<wire x1="1.231" y1="-0.4" x2="1.231" y2="0.4" width="0.01" layer="51"/>
<wire x1="1.231" y1="-3.4" x2="1.231" y2="-2.6" width="0.01" layer="51"/>
<wire x1="-2.6712" y1="3.4" x2="-2.6712" y2="2.6" width="0.01" layer="51"/>
<wire x1="-3.475" y1="3.5057" x2="2.325" y2="3.5057" width="0.01" layer="51"/>
<wire x1="3.925" y1="3.4" x2="1.0163" y2="3.4" width="0.01" layer="51"/>
<wire x1="3.925" y1="2.6" x2="1.0163" y2="2.6" width="0.01" layer="51"/>
<wire x1="1.0163" y1="2.6" x2="1.0163" y2="3.4" width="0.01" layer="51"/>
<wire x1="2.325" y1="2.5336" x2="3.725" y2="2.5336" width="0.01" layer="51"/>
<wire x1="2.325" y1="3.4664" x2="3.725" y2="3.4664" width="0.01" layer="51"/>
<wire x1="-3.475" y1="2.4943" x2="2.325" y2="2.4943" width="0.01" layer="51"/>
<wire x1="-2.6712" y1="0.4" x2="-2.6712" y2="-0.4" width="0.01" layer="51"/>
<wire x1="-3.475" y1="0.5057" x2="2.325" y2="0.5057" width="0.01" layer="51"/>
<wire x1="3.925" y1="-0.4" x2="1.0163" y2="-0.4" width="0.01" layer="51"/>
<wire x1="1.0163" y1="-0.4" x2="1.0163" y2="0.4" width="0.01" layer="51"/>
<wire x1="3.925" y1="0.4" x2="1.0163" y2="0.4" width="0.01" layer="51"/>
<wire x1="-3.475" y1="-0.5057" x2="2.325" y2="-0.5057" width="0.01" layer="51"/>
<wire x1="2.325" y1="-0.4664" x2="3.725" y2="-0.4664" width="0.01" layer="51"/>
<wire x1="2.325" y1="0.4664" x2="3.725" y2="0.4664" width="0.01" layer="51"/>
<wire x1="-2.6712" y1="-2.6" x2="-2.6712" y2="-3.4" width="0.01" layer="51"/>
<wire x1="3.925" y1="-2.6" x2="1.0163" y2="-2.6" width="0.01" layer="51"/>
<wire x1="-3.475" y1="-2.4943" x2="2.325" y2="-2.4943" width="0.01" layer="51"/>
<wire x1="1.0163" y1="-3.4" x2="1.0163" y2="-2.6" width="0.01" layer="51"/>
<wire x1="3.925" y1="-3.4" x2="1.0163" y2="-3.4" width="0.01" layer="51"/>
<wire x1="-3.475" y1="-3.5057" x2="2.325" y2="-3.5057" width="0.01" layer="51"/>
<wire x1="2.325" y1="-2.5336" x2="3.725" y2="-2.5336" width="0.01" layer="51"/>
<wire x1="2.325" y1="-3.4664" x2="3.725" y2="-3.4664" width="0.01" layer="51"/>
<wire x1="3.725" y1="2.5336" x2="3.725" y2="3.4664" width="0.01" layer="51"/>
<wire x1="3.725" y1="-0.4664" x2="3.725" y2="0.4664" width="0.01" layer="51"/>
<wire x1="3.725" y1="-3.4664" x2="3.725" y2="-2.5336" width="0.01" layer="51"/>
<wire x1="2.325" y1="3.4664" x2="2.325" y2="4.3538" width="0.01" layer="51"/>
<wire x1="2.325" y1="4.3538" x2="2.325" y2="4.45" width="0.01" layer="21"/>
<wire x1="2.325" y1="-4.45" x2="2.325" y2="-4.3538" width="0.01" layer="21"/>
<wire x1="2.325" y1="-4.3538" x2="2.325" y2="-3.4664" width="0.01" layer="51"/>
<wire x1="2.325" y1="1.4552" x2="3.925" y2="1.4552" width="0.01" layer="51"/>
<wire x1="2.325" y1="-1.5448" x2="3.925" y2="-1.5448" width="0.01" layer="51"/>
<wire x1="2.325" y1="-2.5336" x2="2.325" y2="-1.4552" width="0.01" layer="51"/>
<wire x1="2.325" y1="-1.4552" x2="2.325" y2="-0.4664" width="0.01" layer="51"/>
<wire x1="2.325" y1="-1.4552" x2="3.925" y2="-1.4552" width="0.01" layer="51"/>
<wire x1="2.325" y1="1.5448" x2="3.925" y2="1.5448" width="0.01" layer="51"/>
<wire x1="2.325" y1="0.4664" x2="2.325" y2="2.5336" width="0.01" layer="51"/>
<wire x1="2.325" y1="-4.3538" x2="3.925" y2="-4.3538" width="0.01" layer="21"/>
<wire x1="2.325" y1="4.3538" x2="3.925" y2="4.3538" width="0.01" layer="21"/>
<wire x1="1.925" y1="3.8" x2="1.925" y2="2.2" width="0.01" layer="51"/>
<wire x1="1.925" y1="2.2" x2="3.7565" y2="2.2" width="0.01" layer="51"/>
<wire x1="3.7565" y1="3.8" x2="1.925" y2="3.8" width="0.01" layer="51"/>
<wire x1="1.925" y1="0.8" x2="1.925" y2="-0.8" width="0.01" layer="51"/>
<wire x1="1.925" y1="-0.8" x2="3.7565" y2="-0.8" width="0.01" layer="51"/>
<wire x1="3.7565" y1="0.8" x2="1.925" y2="0.8" width="0.01" layer="51"/>
<wire x1="1.925" y1="-2.2" x2="1.925" y2="-3.8" width="0.01" layer="51"/>
<wire x1="1.925" y1="-3.8" x2="3.7565" y2="-3.8" width="0.01" layer="51"/>
<wire x1="3.7565" y1="-2.2" x2="1.925" y2="-2.2" width="0.01" layer="51"/>
<wire x1="-1.31" y1="-3" x2="0.16" y2="-3" width="0.01" layer="51"/>
<wire x1="-0.575" y1="-3.735" x2="-0.575" y2="-2.265" width="0.01" layer="51"/>
<wire x1="-1.31" y1="3" x2="0.16" y2="3" width="0.01" layer="51"/>
<wire x1="-0.575" y1="2.265" x2="-0.575" y2="3.735" width="0.01" layer="51"/>
<wire x1="-1.31" y1="0" x2="0.16" y2="0" width="0.01" layer="51"/>
<wire x1="-0.575" y1="-0.735" x2="-0.575" y2="0.735" width="0.01" layer="51"/>
<wire x1="-2.6712" y1="2.6" x2="-3.475" y2="2.6" width="0.01" layer="51"/>
<wire x1="-3.475" y1="3.4" x2="-2.6712" y2="3.4" width="0.01" layer="51"/>
<wire x1="-3.925" y1="2.6" x2="-3.925" y2="3.4" width="0.2" layer="51"/>
<wire x1="-2.6712" y1="-0.4" x2="-3.475" y2="-0.4" width="0.01" layer="51"/>
<wire x1="-3.475" y1="0.4" x2="-2.6712" y2="0.4" width="0.01" layer="51"/>
<wire x1="-3.925" y1="-0.4" x2="-3.925" y2="0.4" width="0.2" layer="51"/>
<wire x1="-2.6712" y1="-3.4" x2="-3.475" y2="-3.4" width="0.01" layer="51"/>
<wire x1="-3.475" y1="-2.6" x2="-2.6712" y2="-2.6" width="0.01" layer="51"/>
<wire x1="-3.925" y1="-3.4" x2="-3.925" y2="-2.6" width="0.2" layer="51"/>
<wire x1="3.925" y1="-4.45" x2="3.925" y2="4.45" width="0.2" layer="51"/>
<wire x1="3.858" y1="-4.45" x2="3.858" y2="-3.7804" width="0.01" layer="51"/>
<wire x1="3.858" y1="-2.2196" x2="3.858" y2="-0.7804" width="0.01" layer="51"/>
<wire x1="3.858" y1="0.7804" x2="3.858" y2="2.2196" width="0.01" layer="51"/>
<wire x1="3.858" y1="3.7804" x2="3.858" y2="4.45" width="0.01" layer="51"/>
<wire x1="3.208" y1="-4.45" x2="3.208" y2="4.45" width="0.01" layer="51"/>
<wire x1="3.925" y1="-4.45" x2="-3.475" y2="-4.45" width="0.2" layer="21"/>
<wire x1="3.925" y1="4.45" x2="-3.475" y2="4.45" width="0.2" layer="21"/>
<wire x1="-3.475" y1="-4.45" x2="-3.475" y2="-3.4" width="0.2" layer="51"/>
<wire x1="-3.475" y1="-3.4" x2="-3.475" y2="-2.6" width="0.2" layer="51"/>
<wire x1="-3.475" y1="-2.6" x2="-3.475" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-3.475" y1="-0.4" x2="-3.475" y2="0.4" width="0.2" layer="51"/>
<wire x1="-3.475" y1="0.4" x2="-3.475" y2="2.6" width="0.2" layer="51"/>
<wire x1="-3.475" y1="2.6" x2="-3.475" y2="3.4" width="0.2" layer="51"/>
<wire x1="-3.475" y1="3.4" x2="-3.475" y2="4.45" width="0.2" layer="51"/>
<wire x1="-2.475" y1="-4.45" x2="-2.475" y2="4.45" width="0.01" layer="51"/>
<wire x1="2.775" y1="-4.45" x2="2.775" y2="4.45" width="0.01" layer="51"/>
<wire x1="-3.475" y1="3.4" x2="-3.925" y2="3.4" width="0.2" layer="51"/>
<wire x1="-3.475" y1="2.6" x2="-3.925" y2="2.6" width="0.2" layer="51"/>
<wire x1="-3.475" y1="0.4" x2="-3.925" y2="0.4" width="0.2" layer="51"/>
<wire x1="-3.475" y1="-0.4" x2="-3.925" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-3.475" y1="-2.6" x2="-3.925" y2="-2.6" width="0.2" layer="51"/>
<wire x1="-3.475" y1="-3.4" x2="-3.925" y2="-3.4" width="0.2" layer="51"/>
<circle x="-0.575" y="3" radius="0.55" width="0.01" layer="51"/>
<circle x="-0.575" y="-3" radius="0.55" width="0.01" layer="51"/>
<circle x="-0.575" y="-3" radius="0.7" width="0.01" layer="51"/>
<circle x="-0.575" y="3" radius="0.7" width="0.01" layer="51"/>
<circle x="-0.575" y="0" radius="0.55" width="0.01" layer="51"/>
<circle x="-0.575" y="0" radius="0.7" width="0.01" layer="51"/>
<smd name="1@2" x="2.775" y="3" dx="2.5" dy="1.4" layer="1"/>
<smd name="1@1" x="-2.625" y="3" dx="2.5" dy="1.4" layer="1"/>
<smd name="2@2" x="2.775" y="0" dx="2.5" dy="1.4" layer="1"/>
<smd name="2@1" x="-2.625" y="0" dx="2.5" dy="1.4" layer="1"/>
<smd name="3@2" x="2.775" y="-3" dx="2.5" dy="1.4" layer="1"/>
<smd name="3@1" x="-2.625" y="-3" dx="2.5" dy="1.4" layer="1"/>
<text x="-3.695" y="5.72" size="2" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.695" y="-6.98" size="2" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="7.215" y="3.18" size="2" layer="51" font="vector" ratio="10" rot="R90">1</text>
</package>
</packages>
<packages3d>
<package3d name="P-2059-303/998-403" urn="urn:adsk.eagle:package:9251146/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="P-2059-303/998-403"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="S-3-POL-S" urn="urn:adsk.eagle:symbol:9251137/1" library_version="1">
<wire x1="-3.81" y1="-3.81" x2="3.81" y2="-3.81" width="0.2" layer="97"/>
<wire x1="3.81" y1="-3.81" x2="3.81" y2="-1.27" width="0.2" layer="97"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="1.27" width="0.2" layer="97"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="3.81" width="0.2" layer="97"/>
<wire x1="3.81" y1="3.81" x2="-3.81" y2="3.81" width="0.2" layer="97"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="1.27" width="0.2" layer="97"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.2" layer="97"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="-3.81" width="0.2" layer="97"/>
<wire x1="-3.81" y1="-1.27" x2="3.81" y2="-1.27" width="0.2" layer="97"/>
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.2" layer="97"/>
<text x="5.08" y="2.54" size="1.778" layer="95" font="vector" ratio="10" rot="MR180" align="center-left">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96" font="vector" ratio="10" rot="MR180" align="center-left">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pin" length="short" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pin" length="short" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pin" length="short" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2059-303/998-403" urn="urn:adsk.eagle:component:9251154/1" prefix="CN" library_version="1">
<description>&lt;b&gt;Serie 2059,  SMD-Leiterplattenklemme im Gurt 3-polig / Series 2059,  SMD Terminal block in tape-and-reel packing 3-pole&lt;/b&gt;&lt;br&gt;&lt;br&gt;Polzahl / Pole No.: 3 &lt;br&gt;Rastermaß / Pitch: 3  mm&lt;br&gt;Bemessungsspannung / Rated Voltage: 160 V&lt;br&gt;Nennstrom / Nominal Current: 3 A&lt;br&gt;Leiterquerschnitt / Conductor Size: 0.14 - 0.34 mm2&lt;br&gt;Anschlusstechnik / Connection Technology: PUSH WIRE&lt;sup&gt;®&lt;/sup&gt; &lt;br&gt;Leitereinführung (zur Platine) / Conductor entry angle (to PCB): 0 °&lt;br&gt;Farbe / Color: weiß / weiß&lt;br&gt;&lt;br&gt; Stand: 01.01.2014. Die jeweils aktuellen Daten zu diesem Artikel finden Sie in unserem Produktkatalog, den Sie unter &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt; einsehen können. &lt;br&gt; As of: 01/01/2014. Please find the newest data for this article in our product catalogue, which can be viewed under &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt;.&lt;br&gt;</description>
<gates>
<gate name="CN" symbol="S-3-POL-S" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P-2059-303/998-403">
<connects>
<connect gate="CN" pin="1" pad="1@1 1@2"/>
<connect gate="CN" pin="2" pad="2@1 2@2"/>
<connect gate="CN" pin="3" pad="3@1 3@2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9251146/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Series 2059, SMD Terminal block in tape-and-reel packing 3-pole"/>
<attribute name="MF" value="WAGO"/>
<attribute name="MPN" value="2059-303/998-403"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="udo-idc">
<packages>
<package name="3M_10" urn="urn:adsk.eagle:footprint:6910/1" locally_modified="yes">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<wire x1="15.875" y1="-4.2418" x2="15.875" y2="4.3" width="0.3048" layer="21"/>
<wire x1="-15.875" y1="4.3" x2="-15.875" y2="-4.2418" width="0.3048" layer="21"/>
<wire x1="-15.875" y1="-4.3" x2="-2.54" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-4.3" x2="-2.54" y2="-3.048" width="0.3048" layer="21"/>
<wire x1="2.54" y1="-3.048" x2="2.54" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="2.54" y1="-4.3" x2="15.621" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="15.875" y1="4.3" x2="-15.875" y2="4.3" width="0.3048" layer="21"/>
<wire x1="8.89" y1="-3" x2="2.54" y2="-3" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-3" x2="-8.89" y2="-3" width="0.3048" layer="21"/>
<wire x1="-8.89" y1="-3" x2="-8.89" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="8.763" y1="3" x2="-8.89" y2="3" width="0.3048" layer="21"/>
<wire x1="-8.89" y1="1.27" x2="-8.89" y2="3" width="0.3048" layer="21"/>
<wire x1="-8.89" y1="-1.27" x2="-15.748" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-8.89" y1="1.27" x2="-15.748" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-4.318" x2="2.54" y2="-4.318" width="0.3048" layer="21"/>
<wire x1="8.89" y1="-3" x2="8.89" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="8.89" y1="1.27" x2="8.89" y2="3" width="0.3048" layer="21"/>
<wire x1="8.89" y1="-1.27" x2="15.748" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="8.89" y1="1.27" x2="15.748" y2="1.27" width="0.3048" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1" shape="square"/>
<pad name="2" x="-5.08" y="1.27" drill="1" shape="square"/>
<pad name="3" x="-2.54" y="-1.27" drill="1" shape="square"/>
<pad name="4" x="-2.54" y="1.27" drill="1" shape="square"/>
<pad name="5" x="0" y="-1.27" drill="1" shape="square"/>
<pad name="6" x="0" y="1.27" drill="1" shape="square"/>
<pad name="8" x="2.54" y="1.27" drill="1" shape="square"/>
<pad name="9" x="5.08" y="-1.27" drill="1" shape="square"/>
<pad name="10" x="5.08" y="1.27" drill="1" shape="square"/>
<pad name="7" x="2.54" y="-1.27" drill="1" shape="square"/>
<text x="-15.24" y="5.08" size="2.54" layer="25">&gt;NAME</text>
<text x="2.54" y="5.08" size="2.54" layer="27">&gt;VALUE</text>
</package>
<package name="3M_10L" urn="urn:adsk.eagle:footprint:6911/1" locally_modified="yes">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<wire x1="-15.875" y1="-6.0198" x2="-13.335" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="15.875" y1="-6.0198" x2="15.875" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-15.875" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-13.335" y1="-6.0198" x2="-13.335" y2="-2.032" width="0.3048" layer="21"/>
<wire x1="-13.335" y1="-6.0198" x2="-8.6614" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-13.335" y1="-2.032" x2="-10.9982" y2="-0.4572" width="0.3048" layer="21"/>
<wire x1="-10.9982" y1="-0.4572" x2="-8.6614" y2="-2.032" width="0.3048" layer="21"/>
<wire x1="-8.6614" y1="-2.032" x2="-8.6614" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-8.6614" y1="-6.0198" x2="8.6614" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="8.6614" y1="-6.0198" x2="8.6614" y2="-2.0574" width="0.3048" layer="21"/>
<wire x1="8.6614" y1="-2.0574" x2="10.9982" y2="-0.4572" width="0.3048" layer="21"/>
<wire x1="10.9982" y1="-0.4572" x2="13.335" y2="-2.0574" width="0.3048" layer="21"/>
<wire x1="13.335" y1="-2.0574" x2="13.335" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="8.6614" y1="-6.0198" x2="13.335" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="13.335" y1="-6.0198" x2="15.875" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-13.0048" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="15.875" y1="2.54" x2="13.0048" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-8.89" y1="10.9982" x2="-13.0048" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-8.89" y1="8.89" x2="-8.89" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="6.6802" width="0.3048" layer="21"/>
<wire x1="-3.81" y1="7.874" x2="-2.54" y2="6.604" width="0.3048" layer="21" curve="-90"/>
<wire x1="-8.89" y1="8.89" x2="-7.874" y2="7.874" width="0.3048" layer="21" curve="90"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="6.6802" width="0.3048" layer="21"/>
<wire x1="2.54" y1="6.604" x2="3.81" y2="7.874" width="0.3048" layer="21" curve="-90"/>
<wire x1="-8.89" y1="1.27" x2="-8.89" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="5.08" x2="-8.89" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="1.27" x2="-6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.89" y1="10.9982" x2="13.0048" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-7.874" y1="7.874" x2="7.874" y2="7.874" width="0.3048" layer="21"/>
<wire x1="8.89" y1="8.89" x2="8.89" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="7.874" y1="7.874" x2="8.89" y2="8.89" width="0.3048" layer="21" curve="90"/>
<wire x1="0" y1="1.27" x2="2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.89" y1="1.27" x2="8.89" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.89" y1="5.08" x2="8.89" y2="7.62" width="0.1524" layer="21"/>
<wire x1="8.89" y1="1.27" x2="6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.27" x2="2.54" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="-5.08" drill="1" shape="square"/>
<pad name="2" x="-5.08" y="-2.54" drill="1" shape="square"/>
<pad name="3" x="-2.54" y="-5.08" drill="1" shape="square"/>
<pad name="4" x="-2.54" y="-2.54" drill="1" shape="square"/>
<pad name="5" x="0" y="-5.08" drill="1" shape="square"/>
<pad name="6" x="0" y="-2.54" drill="1" shape="square"/>
<pad name="8" x="2.54" y="-2.54" drill="1" shape="square"/>
<pad name="9" x="5.08" y="-5.08" drill="1" shape="square"/>
<pad name="10" x="5.08" y="-2.54" drill="1" shape="square"/>
<pad name="7" x="2.54" y="-5.08" drill="1" shape="square"/>
<text x="-15.24" y="-10.16" size="2.54" layer="25">&gt;NAME</text>
<text x="2.54" y="-10.16" size="2.54" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="3M_10" urn="urn:adsk.eagle:package:6397704/2" locally_modified="yes" type="model">
<description>3M</description>
<packageinstances>
<packageinstance name="3M_10"/>
</packageinstances>
</package3d>
<package3d name="3M_10L" urn="urn:adsk.eagle:package:6397703/3" locally_modified="yes" type="model">
<description>3M</description>
<packageinstances>
<packageinstance name="3M_10L"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="10P">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="9" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AWP-10" prefix="CN">
<gates>
<gate name="CN" symbol="10P" x="0" y="0"/>
</gates>
<devices>
<device name="P" package="3M_10">
<connects>
<connect gate="CN" pin="1" pad="1"/>
<connect gate="CN" pin="10" pad="10"/>
<connect gate="CN" pin="2" pad="2"/>
<connect gate="CN" pin="3" pad="3"/>
<connect gate="CN" pin="4" pad="4"/>
<connect gate="CN" pin="5" pad="5"/>
<connect gate="CN" pin="6" pad="6"/>
<connect gate="CN" pin="7" pad="7"/>
<connect gate="CN" pin="8" pad="8"/>
<connect gate="CN" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6397704/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="IDC socket, 2x5, straight, with latch" constant="no"/>
<attribute name="MF" value="CONNFLY" constant="no"/>
<attribute name="MPN" value="DS1011-10SBSIB7" constant="no"/>
</technology>
</technologies>
</device>
<device name="K" package="3M_10L">
<connects>
<connect gate="CN" pin="1" pad="1"/>
<connect gate="CN" pin="10" pad="10"/>
<connect gate="CN" pin="2" pad="2"/>
<connect gate="CN" pin="3" pad="3"/>
<connect gate="CN" pin="4" pad="4"/>
<connect gate="CN" pin="5" pad="5"/>
<connect gate="CN" pin="6" pad="6"/>
<connect gate="CN" pin="7" pad="7"/>
<connect gate="CN" pin="8" pad="8"/>
<connect gate="CN" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6397703/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="IDC socket, 2x5, right angle, with latch" constant="no"/>
<attribute name="MF" value="CONNFLY" constant="no"/>
<attribute name="MPN" value="AWP-10K" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="AIR_LIM_+" library="udo-wago-terminal-blocks" library_urn="urn:adsk.eagle:library:9251134" deviceset="2059-303/998-403" device="" package3d_urn="urn:adsk.eagle:package:9251146/1"/>
<part name="AIR_LIM_-" library="udo-wago-terminal-blocks" library_urn="urn:adsk.eagle:library:9251134" deviceset="2059-303/998-403" device="" package3d_urn="urn:adsk.eagle:package:9251146/1"/>
<part name="INS_SENSOR" library="udo-wago-terminal-blocks" library_urn="urn:adsk.eagle:library:9251134" deviceset="2059-303/998-403" device="" package3d_urn="urn:adsk.eagle:package:9251146/1"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+24V" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+24V" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+24V" device=""/>
<part name="SUPPLY5" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="DGND" device=""/>
<part name="CN1" library="udo-idc" deviceset="AWP-10" device="P" package3d_urn="urn:adsk.eagle:package:6397704/2"/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="DGND" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+24V" device=""/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="DGND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-40.64" y="96.52" size="1.778" layer="97">INPUT</text>
<wire x1="-33.02" y1="99.06" x2="-33.02" y2="58.42" width="0.1524" layer="97"/>
<wire x1="-33.02" y1="48.26" x2="-33.02" y2="7.62" width="0.1524" layer="97"/>
<text x="-43.18" y="45.72" size="1.778" layer="97">OUTPUT</text>
</plain>
<instances>
<instance part="AIR_LIM_+" gate="CN" x="-2.54" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="-7.62" y="86.36" size="1.778" layer="95" font="vector" ratio="10" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="-7.62" y="88.9" size="1.778" layer="96" font="vector" ratio="10" rot="MR0" align="center-left"/>
</instance>
<instance part="AIR_LIM_-" gate="CN" x="-2.54" y="78.74" smashed="yes" rot="R180">
<attribute name="NAME" x="-7.62" y="76.2" size="1.778" layer="95" font="vector" ratio="10" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="-7.62" y="78.74" size="1.778" layer="96" font="vector" ratio="10" rot="MR0" align="center-left"/>
</instance>
<instance part="INS_SENSOR" gate="CN" x="-2.54" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="-7.62" y="66.04" size="1.778" layer="95" font="vector" ratio="10" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="-7.62" y="68.58" size="1.778" layer="96" font="vector" ratio="10" rot="MR0" align="center-left"/>
</instance>
<instance part="P+1" gate="1" x="7.62" y="88.9" smashed="yes" rot="R270">
<attribute name="VALUE" x="10.16" y="88.9" size="1.778" layer="96"/>
</instance>
<instance part="P+4" gate="1" x="7.62" y="78.74" smashed="yes" rot="R270">
<attribute name="VALUE" x="10.16" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="P+6" gate="1" x="7.62" y="68.58" smashed="yes" rot="R270">
<attribute name="VALUE" x="10.16" y="68.58" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY5" gate="G$1" x="30.48" y="63.5" smashed="yes">
<attribute name="VALUE" x="27.813" y="60.325" size="1.778" layer="96"/>
</instance>
<instance part="CN1" gate="CN" x="0" y="30.48" smashed="yes" rot="R180">
<attribute name="VALUE" x="3.81" y="40.64" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="3.81" y="22.098" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="12.7" y="15.24" smashed="yes">
<attribute name="VALUE" x="10.033" y="12.065" size="1.778" layer="96"/>
</instance>
<instance part="P+3" gate="1" x="-12.7" y="17.78" smashed="yes" rot="R90">
<attribute name="VALUE" x="-12.7" y="15.24" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="-17.78" y="25.4" smashed="yes">
<attribute name="VALUE" x="-20.447" y="22.225" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="DGND" class="0">
<segment>
<pinref part="AIR_LIM_+" gate="CN" pin="3"/>
<wire x1="0" y1="91.44" x2="30.48" y2="91.44" width="0.1524" layer="91"/>
<wire x1="30.48" y1="91.44" x2="30.48" y2="81.28" width="0.1524" layer="91"/>
<pinref part="AIR_LIM_-" gate="CN" pin="3"/>
<wire x1="30.48" y1="81.28" x2="30.48" y2="71.12" width="0.1524" layer="91"/>
<wire x1="30.48" y1="71.12" x2="30.48" y2="66.04" width="0.1524" layer="91"/>
<wire x1="0" y1="81.28" x2="30.48" y2="81.28" width="0.1524" layer="91"/>
<junction x="30.48" y="81.28"/>
<pinref part="INS_SENSOR" gate="CN" pin="3"/>
<wire x1="0" y1="71.12" x2="30.48" y2="71.12" width="0.1524" layer="91"/>
<junction x="30.48" y="71.12"/>
<pinref part="SUPPLY5" gate="G$1" pin="DGND"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="8"/>
<wire x1="7.62" y1="27.94" x2="12.7" y2="27.94" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="G$1" pin="DGND"/>
<wire x1="12.7" y1="27.94" x2="12.7" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="7"/>
<wire x1="-7.62" y1="27.94" x2="-17.78" y2="27.94" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="G$1" pin="DGND"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<pinref part="AIR_LIM_+" gate="CN" pin="2"/>
<wire x1="0" y1="88.9" x2="5.08" y2="88.9" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+24V"/>
</segment>
<segment>
<pinref part="INS_SENSOR" gate="CN" pin="2"/>
<pinref part="P+6" gate="1" pin="+24V"/>
<wire x1="0" y1="68.58" x2="5.08" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="AIR_LIM_-" gate="CN" pin="2"/>
<pinref part="P+4" gate="1" pin="+24V"/>
<wire x1="0" y1="78.74" x2="5.08" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="10"/>
<wire x1="7.62" y1="25.4" x2="7.62" y2="17.78" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+24V"/>
<wire x1="7.62" y1="17.78" x2="-7.62" y2="17.78" width="0.1524" layer="91"/>
<pinref part="CN1" gate="CN" pin="9"/>
<wire x1="-7.62" y1="17.78" x2="-10.16" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="25.4" x2="-7.62" y2="17.78" width="0.1524" layer="91"/>
<junction x="-7.62" y="17.78"/>
</segment>
</net>
<net name="AIR_LIM+" class="0">
<segment>
<pinref part="AIR_LIM_+" gate="CN" pin="1"/>
<wire x1="0" y1="86.36" x2="7.62" y2="86.36" width="0.1524" layer="91"/>
<label x="7.62" y="86.36" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="5"/>
<wire x1="-7.62" y1="30.48" x2="-10.16" y2="30.48" width="0.1524" layer="91"/>
<label x="-10.16" y="30.48" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AIR_LIM-" class="0">
<segment>
<pinref part="AIR_LIM_-" gate="CN" pin="1"/>
<wire x1="0" y1="76.2" x2="7.62" y2="76.2" width="0.1524" layer="91"/>
<label x="7.62" y="76.2" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="3"/>
<wire x1="-7.62" y1="33.02" x2="-10.16" y2="33.02" width="0.1524" layer="91"/>
<label x="-10.16" y="33.02" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="INSIDE_SENSOR" class="0">
<segment>
<pinref part="INS_SENSOR" gate="CN" pin="1"/>
<wire x1="0" y1="66.04" x2="7.62" y2="66.04" width="0.1524" layer="91"/>
<label x="7.62" y="66.04" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CN1" gate="CN" pin="6"/>
<wire x1="7.62" y1="30.48" x2="10.16" y2="30.48" width="0.1524" layer="91"/>
<label x="10.16" y="30.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
